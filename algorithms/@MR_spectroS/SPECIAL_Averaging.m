%% ISIS scheme averaging
function [this] = SPECIAL_Averaging(this)

if this.Parameter.ReconFlags.isSPECIALAveraged
    warning('The data are already averaged')
end

nSamples      = size(this.Data{1},this.kx_dim);
nCoils        = size(this.Data{1},this.coil_dim);
nAverages     = size(this.Data{1},this.meas_dim);   

Data       = this.Data{1};       %Columns, Lines, Partitions, Coils, Slices, Cardiac Phases...
                                 %Echos, NaN, NaN (New Mixes),Repetitions, Sets, Averages
tmp        = squeeze(Data);
if nCoils == 1
    tmp = permute(tmp,[1 3 2]);
end


nSPECIAL  = 2;
nSpec  = nAverages/nSPECIAL;
tmp22  = zeros(nSamples,nCoils,nSPECIAL);
tmp22Old = zeros(nSamples,nCoils,nSPECIAL);
%average measurements
for idxOfCh = 1:nCoils
    for idxOfSPECIAL = 0:nSPECIAL-1
        for idxOfSpec = 0:nSpec-1
            tmpData = tmp(:,idxOfCh,1+idxOfSPECIAL+idxOfSpec*2);
            tmp22(:,idxOfCh,idxOfSPECIAL+1) = tmp22Old(:,idxOfCh,idxOfSPECIAL+1) + tmpData;
            tmp22Old = tmp22;
        end
    end
end


this.Data{1}                                   = double(permute(tmp22,[1,4,5,2,6,7,8,9,10,11,12,3]));
this.Parameter.ReconFlags.isSPECIALAveraged    = true;
