classdef Parameter
% version: 26 Feb 2014 
    
    properties
        AverageSettings
        fMRSAveSettings
        fMRSDiffSettings
        CoilCombSettings
        FreqAlignSettings
        EddyCurrSettings
        ZeroFillSettings
        FilterSettings
        PhaseCorrSettings
        Phase0RemovalSettings
        PhaseCorr31PSettings
        HsvdSettings
        FreqAlignFreqDomainSettings
        DeleteMovedAveragesSettings
        MissingPointPredictionSettings
        ISISSettings
        Recon
        ReconFlags
        Headers
        Filepath
        Filename
       
    end    
    
    methods 
        
        function this=Parameter()
        this.AverageSettings            = AverageSettings();
        this.fMRSAveSettings            = fMRSAveSettings();
        this.fMRSDiffSettings           = fMRSDiffSettings();
        this.CoilCombSettings           = CoilCombSettings();
        this.FreqAlignSettings          = FreqAlignSettings();
        this.Recon                      = Recon();
        this.ReconFlags                 = ReconFlags();
        this.ZeroFillSettings           = ZeroFillSettings();
        this.HsvdSettings               = HsvdSettings();
        this.FilterSettings             = FilterSettings();
        this.PhaseCorrSettings          = PhaseCorrSettings();
        this.Phase0RemovalSettings      = Phase0RemovalSettings();
        this.PhaseCorr31PSettings       = PhaseCorr31PSettings();
        this.EddyCurrSettings           = EddyCurrSettings();
        this.FreqAlignFreqDomainSettings = FreqAlignFreqDomainSettings();
        this.DeleteMovedAveragesSettings = DeleteMovedAveragesSettings();
        this.MissingPointPredictionSettings = MissingPointPredictionSettings();
        this.ISISSettings               = ISISSettings();
        %this.Headers= Headers();
        end
     
    end
    
end
