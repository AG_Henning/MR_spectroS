function [ tableConcentrations, tableT2s ] = calculate_T2( FileNames,PathName, displaySilent, availableTEs, downField_MM_Met)
%CALCULATE_T2 Summary of this function goes here
%   Detailed explanation goes here

if (~exist('FileNames', 'var'))
    [FileNames,PathName] = uigetfile('*.table','Please select .table files from LCModel','Multiselect','on');
end

if ~exist('displaySilent', 'var')
    displaySilent = false;
end

if ~exist('availableTEs', 'var')
    availableTEs = {'24' '32' '40' '52' '60'};
end

if ~exist('downField_MM_Met', 'var')
    downField_MM_Met = 'MM';
end

[mapMet_Names] = getMetaboliteNames(downField_MM_Met);

numTEs = length(availableTEs);
%threshold for crlbs
threshold_CRLB = 900;

%% do the absolute quantification on all files
if ~iscell(FileNames)
    FileNames= {FileNames};
end

tableConcentrations = [];
isFirstIter = 1;
for index =1:length(FileNames)
    %% retrieve parameters
    [c1 c2 c3 c4] = textread(strcat(PathName,FileNames{index}),'%s %s %s %s');
    
    if isFirstIter
        start  = find(strcmp(c1,'Conc.'));
        start  = start + 1;
        finish = find(strcmpi(c1,'$$MISC')) - 1;
        tableConcentrations{1,1} = 'ID';
        
        s = 2;
        for j=start:finish
            if(strcmp(c4{j},'')==1)
                c3_split = strsplit(c3{j},'-');
                c3_split = strsplit(c3_split{end},'+');
                tableConcentrations{1,s} = c3_split{end};
            else
                tableConcentrations{1,s} = c4{j};
            end
            tableConcentrations{1,s+1} = 'CRLB %';
            tableConcentrations{1,s+2} = 'CRLB (rel)';
            s          = s +3;
        end
        tableConcentrations{1,s+1} = 'water concentration';
        %extract parameters from the LCModel fit
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        index_atth2o = find(strcmp(c1,'atth2o='));
        atth2o_LCModel = str2num(c2{index_atth2o});
    end
    if strcmp(downField_MM_Met,'DF')
        %rename Cr to Naa
%         tableConcentrations{1,2} = 'Cr';
    else
        if strcmp(downField_MM_Met,'MM')
            %rename Cr to MM09
            %TODO temp disabled
%             tableConcentrations{1,2} = 'MM09';
            tableConcentrations{1,2} = 'NAA';
        else
            % nothing to change
        end
    end
    
    s = 2;
    loc = 1;
    position = find(strcmpi(FileNames{index}(end-7:end-6),availableTEs));
    posTable = position + 1; %position in the table
    
    tableConcentrations{posTable,1}= FileNames{index}(1:end-6);
    
    %% get each metabolite concentration and add it to the table
    for j=start:finish
        %add quantified metabolite
        tableConcentrations{posTable,s} = str2num( c1{j});
        %add CRLB
        tableConcentrations{posTable,s+1} = str2num(  c2{j}(1:end-1));
        
        %check for reasonable CRLB values
        if (tableConcentrations{posTable,s+1}>threshold_CRLB) tableConcentrations{posTable,s}= NaN; end
        if (tableConcentrations{posTable,s+1}>threshold_CRLB) tableConcentrations{posTable,s+1}= NaN; end
        %add CRLB (rel)
        tableConcentrations{posTable,s+2} = tableConcentrations{posTable,s}*0.01*tableConcentrations{posTable,s+1};
        s          = s +3;
    end
    
    isFirstIter = false;
end

tableT2s = [];
x = str2double(availableTEs(:));
numOfMetabolite =  finish - start + 1;
tableConcentrations{8,1} = 'T2';
tableConcentrations{9,1} = 'RSquare';
tableT2s{2,1} = 'T2';
tableT2s{3,1} = 'RSquare';
for iMetabolite = 1:numOfMetabolite
    indexMetabolite = iMetabolite * 3 - 1;
    metaboliteName = tableConcentrations(1,indexMetabolite);
    y = cell2mat(tableConcentrations(2:numTEs+1,indexMetabolite));

    [calculatedT2, gof] = calculateT2Fit(x,y, iMetabolite, metaboliteName, displaySilent, PathName);
    %saving results
    tableConcentrations{8,indexMetabolite} = calculatedT2;
    tableConcentrations{9,indexMetabolite} = gof.rsquare;
    metaboliteName{1};
    tableT2s{1,iMetabolite+1} = mapMet_Names(metaboliteName{1});
    tableT2s{2,iMetabolite+1} = num2str(calculatedT2,'%.2f');
    tableT2s{3,iMetabolite+1} = num2str(gof.rsquare,'%.2f');

end

if strcmp(downField_MM_Met, 'DF')
    % scale Cr with the number of H (protons) contributing to the peak)
    for indexTe = 1: numTEs
        tableConcentrations{1+indexTe,2} = tableConcentrations{1+indexTe,2} ./ 3;
    end
end

%% save table to file
exportFileXlsx = [PathName ,'Quantification_Results.xlsx'];
xlswrite(exportFileXlsx, {PathName}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileXlsx = [PathName ,'T2_Results.xlsx'];
xlswrite(exportFileXlsx, {PathName}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableT2s, 1, cellPosition);

end

function [calculatedT2, gof] = calculateT2Fit(x, y, iMetabolite, metaboliteName, displaySilent, PathName)
    orderOfMagn = floor(log(abs(mean(y,'omitnan')))./log(10));
    tempX = x(~(isnan(y)));
    tempY = y(~(isnan(y))) * 10^(orderOfMagn+1); %values scaled to have the same order of magnitude as the echo times ( 10 -100 ms)
    if length(tempX) >= 3
        % write equation y = a * x +b for 2nd and 3rd point to check for an
        % improved starting value
        a = ((tempY(3)-tempY(2))/tempX(3)) / (1 + tempX(2)/tempX(3));
        b = tempY(2) - a * tempX(2);
        firstValueOk = tempY(1) > (a * tempX(1) + b);
        if (firstValueOk)
            options = fitoptions('exp1', 'Upper', [Inf 0]);
        else
            options = fitoptions('exp1', 'Upper', [Inf 0]);
            [expofitTemp] = fit(tempX(2:end),tempY(2:end), 'exp1', options);
            p0 = [expofitTemp.a * 0.5 expofitTemp.b * 1.5]; %it's a try to give a better starting value, but it actually doesn't really work
            options = fitoptions('exp1', 'Upper', [Inf 0], 'StartPoint', p0);
        end
        [expofit, gof] = fit(tempX,tempY, 'exp1', options);
        calculatedT2 = 1.0/(-1.0*expofit.b);
        if length(tempX) == 2
            if isnan(y(1)) || isnan(y(2))
                thisValue = [metaboliteName ' ' num2str(calculatedT2) ' ' num2str(gof.rsquare)]
                calculatedT2 = NaN;
                gof.rsquare = 0;
            end
        end
        if displaySilent == false
            % plotting
            figure(iMetabolite);
            plot(expofit,tempX,tempY);
%             set(gca, 'YScale', 'log')
            title(metaboliteName);
            mkdir([PathName 'T2_plots\'])
            savefig([PathName 'T2_plots\' metaboliteName{1} '.fig'])
        end
    else
        calculatedT2 = NaN;
        gof.rsquare = 0;
    end
    
end
