
pathName = '31P data path';
pathBase = pathToDataFolder(pathName, {''});
pathBase = strcat(pathBase, 'ISIS\');
folders = {'3iso_TR5s', '5iso_TR5s', '5iso_TR25s'};
folders = {'noP1'};

for indexFolder = 1:length(folders)
    pathFolder = strcat(pathBase, folders{indexFolder});
    files = ls([pathFolder '\meas*.mat']);
    for indexFile = 1:size(files,1)
        this = load([pathFolder '\' files(indexFile,:)]);
        try
            xx = strsplit(files(indexFile,:),'.mat');
            this2 = eval(['this.',xx{1}]);
            this = this2;
        catch
        end
        try
            this2 = this.this;
            this = this2;
        catch
        end
        data = this.Data{1};
        spectrum = fftshift(fft(data));
        maxSpectrum = max(real(spectrum));
        scaledData = data ./ maxSpectrum;
        if indexFile == 1
            summedSpectra = this;
            summedSpectra.Data{1} = scaledData ;
        else
            summedSpectra.Data{1} = summedSpectra.Data{1} + scaledData;
        end
        figure;
        plot(real(spectrum ./ maxSpectrum))
        title([folders{indexFolder}, ' ', this.Parameter.Filename])
    end
    spectrum  = fftshift(fft(summedSpectra.Data{1}));
    figure;
    plot(real(spectrum))
    title(['Summed: ' folders{indexFolder}])
    save([pathFolder, '\summedSpectrum.mat'], 'summedSpectra');
    
    if indexFolder == 1
        summedAllSpectra = summedSpectra;
    else
        summedAllSpectra.Data{1} = summedAllSpectra.Data{1} + summedSpectra.Data{1};
    end
end

summedSpectrum = fftshift(fft(summedAllSpectra.Data{1}));
figure;
plot(real(summedSpectrum))
title('Summed all spectra')
save([pathBase, 'summedAllSpectra.mat'], 'summedAllSpectra')