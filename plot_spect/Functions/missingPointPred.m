function varargout = missingPointPred(varargin)
% missingPointPred MATLAB code for missingPointPred.fig
%      missingPointPred, by itself, creates a new missingPointPred or raises the existing
%      singleton*.
%
%      H = missingPointPred returns the handle to a new missingPointPred or the handle to
%      the existing singleton*.
%
%      missingPointPred('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in missingPointPred.M with the given input arguments.
%
%      missingPointPred('Property','Value',...) creates a new missingPointPred or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before missingPointPred_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to missingPointPred_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help missingPointPred

% Last Modified by GUIDE v2.5 21-Aug-2018 11:22:10



% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @missingPointPred_OpeningFcn, ...
                   'gui_OutputFcn',  @missingPointPred_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before missingPointPred is made visible.
function missingPointPred_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to missingPointPred (see VARARGIN)

% Choose default command line output for missingPointPred
handles.output = hObject;



%---------------Initialise the shown values, sliders etc-------------------
if nargin == 4
    handles.hInput= varargin{1};
end

this      =  handles.hInput.MRSpectroMatrix.getCurrent();
imiss     =  this.Parameter.MissingPointPredictionSettings.imiss;
T_factor     = this.Parameter.MissingPointPredictionSettings.T_factor;
polyOrder     = this.Parameter.MissingPointPredictionSettings.polyOrder;

set(handles.edit_imiss,'String',imiss);
set(handles.slider_imiss,'Value',imiss);
set(handles.edit_T,'String',T_factor);
set(handles.slider_T,'Value',T_factor);
set(handles.edit_polyOrder,'String',polyOrder);
set(handles.slider_polyOrder,'Value',polyOrder);

set(handles.slider_imiss,'Max', 40); 
set(handles.slider_imiss,'Min', 0);
set(handles.slider_imiss,'SliderStep',[1/40 1/40]);

set(handles.slider_T,'Max',6); 
set(handles.slider_T,'Min', 1);
set(handles.slider_T,'SliderStep',[1/5 1/5]);

set(handles.slider_polyOrder,'Max', 100); 
set(handles.slider_polyOrder,'Min', 1);
set(handles.slider_polyOrder,'SliderStep',[1/99 1/99]);

 updatePlot(hObject, eventdata, handles);
%--------------------------------------------------------------------------

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes missingPointPred wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = missingPointPred_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function updatePlot(hObject, eventdata, handles)
imiss     =  get(handles.slider_imiss,'Value');
T_factor     =  get(handles.slider_T,'Value');
polyOrder     =  get(handles.slider_polyOrder,'Value');
set(handles.hInput.SelectedProcessingValues,'MissingPointPred_imiss',imiss);
set(handles.hInput.SelectedProcessingValues,'MissingPointPred_T_factor',T_factor);
set(handles.hInput.SelectedProcessingValues,'MissingPointPred_polyOrder',polyOrder);

displaySpect(handles.hInput);

%##########################################################################
%                             MISSING POINTS
%##########################################################################

% --- Executes on slider movement.
function slider_imiss_Callback(hObject, eventdata, handles)
% hObject    handle to slider_imiss (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
current_value=get(hObject,'Value');
current_value = round(current_value);
set(handles.edit_imiss,'String',current_value);
set(handles.slider_imiss,'Value',current_value);
updatePlot(hObject, eventdata, handles);



function edit_imiss_Callback(hObject, eventdata, handles)
% hObject    handle to edit_imiss (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
current_value=get(hObject,'String');
current_value = round(str2double(current_value));
set(handles.edit_imiss,'String',current_value);
set(handles.slider_imiss,'Value',current_value);
updatePlot(hObject, eventdata, handles);


%##########################################################################
%                           T
%##########################################################################

% --- Executes on slider movement.
function slider_T_Callback(hObject, eventdata, handles)
% hObject    handle to slider_T (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
current_value=get(hObject,'Value');
current_value = round(current_value);
set(handles.edit_T,'String',current_value);
set(handles.slider_T,'Value',current_value);
updatePlot(hObject, eventdata, handles);


function edit_T_Callback(hObject, eventdata, handles)
% hObject    handle to edit_T (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
current_value=get(hObject,'String');
current_value = round(str2double(current_value));
set(handles.edit_T,'String',current_value);
set(handles.slider_T,'Value',current_value);
updatePlot(hObject, eventdata, handles);



% --- Executes on slider movement.
function slider_polyOrder_Callback(hObject, eventdata, handles)
% hObject    handle to slider_polyOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
current_value=get(hObject,'Value');
current_value = round(current_value);
set(handles.edit_polyOrder,'String',current_value);
set(handles.slider_polyOrder,'Value',current_value);
updatePlot(hObject, eventdata, handles);

function edit_polyOrder_Callback(hObject, eventdata, handles)
% hObject    handle to edit_polyOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value=get(hObject,'String');
current_value=round(str2double(current_value));
set(handles.edit_polyOrder,'String',current_value);
set(handles.slider_polyOrder,'Value',current_value);
updatePlot(hObject, eventdata, handles);

% Hints: get(hObject,'String') returns contents of edit_polyOrder as text
%        str2double(get(hObject,'String')) returns contents of edit_polyOrder as a double

%##########################################################################
%                              BUTTONS
%##########################################################################


% --- Executes on button press in pushbutton_ok.
function pushbutton_ok_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
imiss = 0;
T_factor = 0;
polyOrder = 0;
set(handles.edit_imiss,'String',imiss);
set(handles.slider_imiss,'Value',imiss);
set(handles.edit_T,'String',T_factor);
set(handles.slider_T,'Value',T_factor);
set(handles.edit_polyOrder,'String',polyOrder);
set(handles.slider_polyOrder,'Value',polyOrder);
updatePlot(hObject, eventdata, handles);
close missingPointPred;


% --- Executes on button press in pushbuttonApply.
function pushbuttonApply_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonApply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


this                                        =  handles.hInput.MRSpectroMatrix.getCurrent();
this.Parameter.MissingPointPredictionSettings.imiss     =  get(handles.slider_imiss,'Value');
this.Parameter.MissingPointPredictionSettings.T_factor     =  get(handles.slider_T,'Value');
this.Parameter.MissingPointPredictionSettings.polyOrder     =  get(handles.slider_polyOrder,'Value');
this                                         = this.MissingPointPrediction;
imiss = 0;
T_factor = 0;
polyOrder = 0;
set(handles.edit_imiss,'String',imiss);
set(handles.slider_imiss,'Value',imiss);
set(handles.edit_T,'String',T_factor);
set(handles.slider_T,'Value',T_factor);
set(handles.edit_polyOrder,'String',polyOrder);
set(handles.slider_polyOrder,'Value',polyOrder);
handles.hInput.MRSpectroMatrix.Save(this);
displaySpect(handles.hInput);


%##########################################################################
%##########################################################################

% --- Executes during object creation, after setting all properties.
function edit_imiss_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_imiss (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function slider_T_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_T (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function edit_T_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_T (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function slider_imiss_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_imiss (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes during object creation, after setting all properties.
function slider_polyOrder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_polyOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function edit_polyOrder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_polyOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
