function reconstruct_all_MM()

pathName = 'MM WM T1 data path';
subjects = { ...
        '1717' ...
        '2016' ...
        '9810' ...
        '3490' ...
        '2017' ...
        '7338'...
        '1658'...
        '2349'...
        '2020'...
      };       %  '7782'...
      %  '3373'...

pathBase = pathToDataFolder(pathName, subjects);

%TI1 = [24; 32; 40; 52; 60];
TI1 =  [2360; 1800; 1900; 2000; 2150; 1200; 1250; 1300; 1300; 1300; 1050]; 
TI2 =  [625;  525; 550; 575; 600; 20; 20; 80; 60; 20; 238];

numberOfSubjects = length(subjects);
paths = cell(1,numberOfSubjects);
for indexSubj = 1:numberOfSubjects
    paths{indexSubj} = [pathBase subjects{indexSubj} '\\'];
end

files_TI_2360_625 = cell(1, numberOfSubjects);
files_TI_1800_525 = cell(1, numberOfSubjects);
files_TI_1900_550 = cell(1, numberOfSubjects);
files_TI_2000_575 = cell(1, numberOfSubjects);
files_TI_2150_600 = cell(1, numberOfSubjects);
files_TI_1200_20 = cell(1, numberOfSubjects);
files_TI_1250_20 = cell(1, numberOfSubjects);
files_TI_1300_80 = cell(1, numberOfSubjects);
files_TI_1300_60 = cell(1, numberOfSubjects);
files_TI_1300_20 = cell(1, numberOfSubjects);
files_TI_1050_238 = cell(1, numberOfSubjects);
 water_files = cell(1, numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    files_TI_2360_625{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_2360_625_*.dat']);
    files_TI_1800_525{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_1800_525_*.dat']);
    files_TI_1900_550{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_1900_550_*.dat']);
    files_TI_2000_575{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_2000_575_*.dat']);
    files_TI_2150_600{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_2150_600_*.dat']);
    files_TI_1200_20{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_1200_20_*.dat']);
    files_TI_1250_20{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_1250_20_*.dat']);
    files_TI_1300_80{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_1300_80_*.dat']);
    files_TI_1300_60{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_1300_60_*.dat']);
    files_TI_1300_20{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_1300_20_*.dat']);
    files_TI_1050_238{indexSubject} = ls([paths{indexSubject}, '*_semiLASER_1050_238_*.dat']);
      water_files{indexSubject} = ls([paths{indexSubject}, '*_scout_wref_*.dat']);
end

addSinglet0ppm = 'Yes';
numberOfTIs = length(TI1);

 filesPath = cell(numberOfTIs,numberOfSubjects);
waterFilesPath = cell(1,numberOfSubjects);
files = [files_TI_2360_625; files_TI_1800_525; files_TI_1900_550;files_TI_2000_575;...
        files_TI_2150_600; files_TI_1200_20; files_TI_1250_20; files_TI_1300_80;...
        files_TI_1300_60; files_TI_1300_20; files_TI_1050_238 ];
for indexSubj = 1:numberOfSubjects
    for indexTI = 1:numberOfTIs
        filesPath{indexTI, indexSubj} = [paths{indexSubj} files{indexTI, indexSubj}];
    end
waterFilesPath{indexSubj} = [paths{indexSubj} water_files{indexSubj}];
end

summedSpectra = cell(numberOfTIs,1);
indexSubjTIs = zeros(numberOfTIs,1);
for indexSubj = 1:numberOfSubjects
    %% water data
    [a] = reconstructWater(waterFilesPath{indexSubj});
    filename = [paths{indexSubj} subjects{indexSubj} '_water.mat'];
    save(filename, 'a');
    a.ExportLcmRaw(paths{indexSubj}, strcat(subjects{indexSubj}, '_water'), addSinglet0ppm);
    %calculate scaling factor the summedSpectra individually according to water peak
    waterData = a.Data{1};
    maxWaterPeak = max(real(fftshift(fft(waterData))));     
    %scale the data for equalizing between subjects
    a.Data{1} = a.Data{1}./ maxWaterPeak;
    %create summed spectra
    if(indexSubj == 1)
        summedWater = a;
        newDataWater= zeros(length(a.Data{1}),1,1,1,1,1,1,1,1,1,1,numberOfSubjects);
    else
        oldDataWater = summedWater.Data{1};
        if (ndims(oldDataWater) ~= 12)
            newDataWater(:,1,1,1,1,1,1,1,1,1,1,1) = oldDataWater;
        else
            newDataWater(:,1,1,1,1,1,1,1,1,1,1,:) = oldDataWater;
        end
        newDataWater(:,1,1,1,1,1,1,1,1,1,1,indexSubj) = a.Data{1};
        summedWater.Data{1} = newDataWater;
    end
    

    
    %% reconstruct Macromolecule data
    for indexTI = 1:numberOfTIs
        if((TI1(indexTI) == 1300)| (TI1(indexTI) ==1250) | (TI1(indexTI)==1200))
            doReverse = false;
        else
            doReverse = true;
        end
        a = reconstruct_MM(filesPath{indexTI, indexSubj}, 0, 0, doReverse);
        filename = [paths{indexSubj} subjects{indexSubj} '_TI1_' num2str(TI1(indexTI)) '_TI2_' num2str(TI2(indexTI)) '.mat'];
        save(filename, 'a');
        a.ExportLcmRaw(paths{indexSubj}, strcat(subjects{indexSubj}, '_TI1_', num2str(TI1(indexTI)),'_TI2_', num2str(TI2(indexTI))) , addSinglet0ppm);
        %scale the data for equalizing between subjects
        a.Data{1} = a.Data{1}./ maxWaterPeak;
        
        % eliminate data with too many deleted averages from the summedSpectra
        
        deletedAverages = a.Parameter.DeleteMovedAveragesSettings.deletedAverages;
        if ~isempty(deletedAverages)
            numDeletedAverages = length(deletedAverages);
        else
            numDeletedAverages = 0;
        end
        
        if numDeletedAverages <= 2 
            indexSubjTIs(indexTI) = indexSubjTIs(indexTI) + 1;
            %create summed spectra
            if(indexSubjTIs(indexTI) == 1)
                summedSpectra{indexTI} = a;
                newData= zeros(length(a.Data{1}),1,1,1,1,1,1,1,2,1,1,numberOfSubjects);
            else
                oldData = summedSpectra{indexTI}.Data{1};
                if (ndims(oldData) ~= 12)
                    newData(:,1,1,1,1,1,1,1,:,1,1,1) = oldData;
                else
                    newData(:,1,1,1,1,1,1,1,:,1,1,:) = oldData;
                end
                newData(:,1,1,1,1,1,1,1,:,1,1,indexSubjTIs(indexTI)) = a.Data{1};
                summedSpectra{indexTI}.Data{1} = newData;
            end
        end
    end    
end

%

% filename = [pathBase, 'Summed_water.mat'];
% save(filename, 'summedWater');
% summedWater = summedWater.AverageData;
% save([pathBase, 'Summed_Averaged_water.mat'], 'summedWater');
% summedWater.ExportLcmRaw(pathBase, 'Summed_Averaged_water', addSinglet0ppm);
for indexTI = 1:numberOfTIs
    filename = [pathBase, 'Summed_spectra_TI' num2str(TI1(indexTI)) '.mat'];
    summedSpectraTI = summedSpectra{indexTI};
    %store only the data, which was kept after eliminating the deleted ones
    actualData = summedSpectraTI.Data{1}./indexSubjTIs(indexTI);
    summedSpectraTI.Data{1} = actualData(:,1,1,1,1,1,1,1,:,1,1,1:indexSubjTIs(indexTI));
    save(filename, 'summedSpectraTI');
    %average across subjects
    summedSpectraTI = summedSpectraTI.AverageData;
    filename = [pathBase, 'Summed_Averaged_TI1_' num2str(TI1(indexTI)) '_TI2_' num2str(TI2(indexTI)) '.mat'];
    save(filename, 'summedSpectraTI');   
    summedSpectraTI.ExportLcmRaw(pathBase, strcat('Summed_Averaged_TI1_', num2str(TI1(indexTI)), '_TI2_', num2str(TI2(indexTI))), addSinglet0ppm);
end

%summarize_Deleted_Averages(subjects, pathBase, TI1)
close all
