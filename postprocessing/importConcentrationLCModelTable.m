function [ tableConcentrations, wconc_LCModel, atth2o_LCModel, PathName] = importConcentrationLCModelTable(FileNames,PathName, availableTEs, downField_MM_Met)
%importConcentrationLCModelTable Summary of this function goes here
%   Detailed explanation goes here

if (~exist('FileNames', 'var'))
    [FileNames,PathName] = uigetfile('*.table','Please select .table files from LCModel','Multiselect','on');
end

if ~exist('availableTEs', 'var')
    availableTEs = {'24' '32' '40' '52' '60'};
end

if ~exist('downField_MM_Met', 'var')
    downField_MM_Met = 'MM';
end

%threshold for crlbs
threshold_CRLB = 900;

%% do the absolute quantification on all files
if ~iscell(FileNames)
    FileNames= {FileNames};
end

tableConcentrations = [];
isFirstIter = 1;
for index =1:length(FileNames)
    %% retrieve parameters
    [c1 c2 c3 c4] = textread(strcat(PathName,FileNames{index}),'%s %s %s %s');
    
    if isFirstIter
        start  = find(strcmp(c1,'Conc.'));
        start  = start + 1;
        finish = find(strcmpi(c1,'$$MISC')) - 1;
        tableConcentrations{1,1} = 'ID';
        
        s = 2;
        for j=start:finish
            if(strcmp(c4{j},'')==1)
                c3_split = strsplit(c3{j},'+');
                if length(c3_split) == 1
                    c3_split = strsplit(c3{j},'-');
                end
                tableConcentrations{1,s} = c3_split{2};
            else
                tableConcentrations{1,s} = c4{j};
            end
            tableConcentrations{1,s+1} = 'CRLB %';
            tableConcentrations{1,s+2} = 'CRLB (rel)';
            s          = s +3;
        end
        tableConcentrations{1,s+1} = 'water concentration';
        %extract parameters from the LCModel fit
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        index_atth2o = find(strcmp(c1,'atth2o='));
        atth2o_LCModel = str2num(c2{index_atth2o});
    end
    if strcmp(downField_MM_Met, 'DF')
        %rename Cr to Naa
        tableConcentrations{1,2} = 'Naa';
    else
        if strcmp(downField_MM_Met, 'MM')
            %rename Cr to MM09
            tableConcentrations{1,2} = 'MM09';
        else
            %Met -> do nothing
        end
    end
    
    s = 2;
    loc = 1;
    position = find(strcmpi(FileNames{index}(end-7:end-6),availableTEs));
    if isempty(position)
            warning('TE not found in file name. Assuming first entry');
            position = 1;
    end
    posTable = position + 1; %position in the table

    tableConcentrations{posTable,1}= FileNames{index}(1:end-6);

    %% absolutely quantify each metabolite and add it to the table
    for j=start:finish
        %add quantified metabolite 
        tableConcentrations{posTable,s} = str2num( c1{j});
        %add CRLB
        tableConcentrations{posTable,s+1} = str2num(  c2{j}(1:end-1));
        
        %check for reasonable CRLB values
        if (tableConcentrations{posTable,s+1}>threshold_CRLB) tableConcentrations{posTable,s}= NaN; end
        if (tableConcentrations{posTable,s+1}>threshold_CRLB) tableConcentrations{posTable,s+1}= NaN; end
        %add CRLB (rel)
        tableConcentrations{posTable,s+2} = tableConcentrations{posTable,s}*0.01*tableConcentrations{posTable,s+1};
        s          = s +3;
    end
        
    isFirstIter = false;
end


%% save table to file
exportFileXlsx = [PathName ,'Quantification_Results.xlsx'];
xlswrite(exportFileXlsx, {PathName}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);
end

