classdef fMRSAveSettings
  
    properties
        paradigm % should be in this  format
                 % [0/1 #averages 0/1 #averages etc]
                 % for instance [1 40 0 20 1 40]
                 % ON(40ave)-OFF(20ave)-ON(40ave)
        plotParadigm
        excludeBlocks
        % for sliding averaging
        shiftedAverages
        numberOfAverages
        numberOfNewPoints
    end
       
    methods
        function this = fMRSAveSettings()
            this.paradigm       = [];
            this.plotParadigm   = true;
            this.excludeBlocks  = [];
            
            %for sliding averaging
            this.shiftedAverages      = [];
            this.numberOfAverages     = [];
            this.numberOfNewPoints  = [];
            
        end

    end
    
end

