%% SPECIAL ordering scheme 
function [this] = SPECIAL_Scheme(this)

if ~this.Parameter.ReconFlags.isSPECIALAveraged
    warning('The data should be generally SPECIAL averaged first')
end

if this.Parameter.ReconFlags.isSPECIALOrdered
    warning('The data are already ordered according to the SPECIAL scheme')
end

nSamples      = size(this.Data{1},this.kx_dim);
nCoils        = size(this.Data{1},this.coil_dim);
nAverages     = size(this.Data{1},this.meas_dim);   

Data       = this.Data{1};     
tmp        = squeeze(Data);
if nCoils == 1
    tmp = permute(tmp,[1 3 2]);
end

nSPECIAL  = 2;
nSpec  = nAverages/nSPECIAL;
tmp2       = zeros(nSamples,nCoils,nSpec);
for idxOfSpec = 0:nSpec-1
    offsetISIS = idxOfSpec*nSPECIAL;
    for idxOfCh = 1:nCoils
        tmp2(:,idxOfCh, idxOfSpec+1)    = tmp(:,idxOfCh,1+offsetISIS) + tmp(:,idxOfCh,2+offsetISIS);
    end
end


this.Data{1}                                = double(permute(tmp2,[1,4,5,2,6,7,8,9,10,11,12,3]));
this.Parameter.ReconFlags.isSPECIALOrdered     = true;
