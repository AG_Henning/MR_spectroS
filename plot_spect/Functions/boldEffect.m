function boldEffect( this, selectedProcessingValues, selectedCoil, selectedMix)
%BOLDEFFECT Summary of this function goes here
%   Detailed explanation goes here

TR                       = this.Parameter.Headers.TR_us * 1E-6; %in secs
data                     = this.Data{1};
bandwidth                = this.Parameter.Headers.Bandwidth_Hz;
paradigm                 = this.Parameter.fMRSAveSettings.paradigm;
nAcquisitions            = this.Parameter.Headers.NAveMeas*this.Parameter.Headers.NRepMeas;
nAverages                = size(data,this.meas_dim);

data = squeeze(data(:,1,1,selectedCoil,1,1,1,1,selectedMix,:,1,:))           ;

clear  selectedCoil selectedMix  handles this
 

%//////////////////////////////////////////////////////////////////////////
%                      CALCULATE SNR AND FWHM
%//////////////////////////////////////////////////////////////////////////

amplitudeArray   = zeros(1,nAverages);
fwhmArray = zeros(1,nAverages);
for iAverage = 1:nAverages
try    
 fwhmArray(iAverage) = linewidth( squeeze(data(:,iAverage)), selectedProcessingValues,bandwidth);
catch
    
end
 %snrArray(iAverage) = 20*log10(snr( squeeze(data(:,iAverage)),selectedProcessingValues )); %dB units
 %snrArray(iAverage) = max(real(squeeze(data(1:100,iAverage))));
%  snrArray(iAverage) = snr( squeeze(data(:,iAverage)),selectedProcessingValues );
 peakRange        = get(selectedProcessingValues, 'PeakRange');
 fftsReal =  real(fftshift(fft(data(:,iAverage))));
 amplitudeArray(iAverage) = max(abs(fftsReal(peakRange(1):peakRange(2))));
end

clear data bandwidth SelectedProcessingValues iAverage

%%  paradigm
%  snr1 =  mean(snrArray(1:40))/1000
%  std1 =  std(snrArray(1:40))/1000
% 
%   snr2 =  mean(snrArray(40:80))/1000
%   std2 =  std(snrArray(40:40))/1000
% 
%   snr3 =  mean(snrArray(80:120))/1000
%   std3 =  std(snrArray(80:120))/1000
%  
%   snr4 =  mean(snrArray(120:160))/1000
%  std4 =  std(snrArray(120:160))/1000


%%
figure

subplot(2,1,1)

plot((amplitudeArray(1:end)));
xlabel('Time (sec)')
title('Amplitude')


subplot(2,1,2)

plot((fwhmArray(1:end)));
%plot([1:nAverages]*TR*(nAcquisitions/nAverages),fwhmArray);
xlabel('Time (sec)')
title('FWHM')
mean(fwhmArray(1:end))
std(fwhmArray(1:end))




end

