function [figureIds, sortedIndeces] = evaluateFWHMT2Fits(downField_MM_Met, offsetPlot, figureIds, sortedIndeces)

if ~exist('downField_MM_Met','var')
    downField_MM_Met = 'DF';
end

if ~exist('offsetPlot','var')
    offsetPlot = 0;
end
if ~exist('figureIds','var')
    figureIds = {};
end

useTotalNAA = false;
doMetaboliteFWHMs = false;

if offsetPlot <= 0
    plotColors = {[0 0 1], [1 0 0]};
    plotColors2 = {[0.0 0.4 0.0], [1 0.5 0.3]};
    plotColors2 = {[0.0 0.4 0.0], [0 0 0]};
%     plotColors2 = {[0.3 0.3 0.1], [0.2 0.5 0.05]};
else
    plotColors = {[0.0 0.7 0.9], [1 0 1]};
    plotColors2 = {[0.1 0.9 0.1], [0.9 0.6 0]};
    plotColors2 = {[0.1 0.9 0.1], [0.5 0.5 0.5]};
end

usePpm = false;

if isempty(figureIds)
    emptyFigureIds = true;
else
    emptyFigureIds = false;
end

pathNameMM = 'DF data path';
outputFolder = 'Output\';

sampleCriteriaMM = {outputFolder};
data_path = pathToDataFolder(pathNameMM, sampleCriteriaMM);

load([data_path outputFolder 'tableT2sSubjects.mat'],'tableT2sSubjects')
load([data_path outputFolder 'tableConcentrationsSubjects.mat'],'tableConcentrationsSubjects')

doFWHMevalutation = true;
if doFWHMevalutation
    load([data_path outputFolder 'tableFWHMSubjects.mat'],'tableFWHMSubjects')
    load([data_path outputFolder 'tableFWHMSubjects.mat'],'orderMetFWHMSubjects')
end


metaboliteNameOrder = {'Cr-CH3' 'Cr-CH2' 'NAA-ace' 'tCho_PE'};
metaboliteNameOrderConc = {'Cr' 'Cr_CH2' 'NAA_ac'  'tCho_P'};
metaboliteNameOrderFWHM = metaboliteNameOrder;
metaboliteNamesDisplay = {'tCr(CH3)';'tCr(CH2)';'NAA(CH3)';'tCho+';};
subjects = {'1658';'1706';'1717';'2017';'3373';'3490';'5771';'6249';'6971';'7338';'7782';'9810'};
yAxisLim_FWHM = [-3 28];
yAxisLim_Res = [-5 5];
        
numberOfMet = length(metaboliteNameOrder);
TEs = [24 32 40 52 60];
numberOfTEs = 5;
scanFrequency = 399.719;
numOfSubjects = length(tableT2sSubjects);
T2s = cell(numOfSubjects,numberOfMet);
R2s = cell(numOfSubjects,numberOfMet);
concentrations = cell(numOfSubjects, numberOfMet, numberOfTEs);
for indexSubject = 1:numOfSubjects        
    T2times = tableT2sSubjects{indexSubject}(2,2:end);
    R2times = tableT2sSubjects{indexSubject}(3,2:end);
    concentrationsSubject = tableConcentrationsSubjects{indexSubject};
    for indexMetabolite = 1:numberOfMet
        index = find(strcmp(metaboliteNameOrder(indexMetabolite), tableT2sSubjects{indexSubject}(1,2:end)));
        T2s(indexSubject,indexMetabolite) = T2times(index);
        R2s(indexSubject,indexMetabolite) = R2times(index);
        indexConc = find(strcmp(metaboliteNameOrderConc(indexMetabolite),concentrationsSubject(1,:))); 
        concentrations(indexSubject, indexMetabolite,:) = concentrationsSubject(2:6,indexConc);
    end
end

concentrations = cell2mat(concentrations);
T2sNumeric = str2double(T2s);
R2sNumeric = str2double(R2s);
nonNegT2s = T2sNumeric;
nonNegT2s(nonNegT2s<0) = NaN;
reshape(nonNegT2s,numberOfMet, numOfSubjects);

acceptableT2s = nonNegT2s;
acceptableR2s = R2sNumeric;

acceptableT2s(R2sNumeric<=0.51) = NaN;
acceptableR2s(R2sNumeric<=0.51) = NaN;

reshape(acceptableT2s,numberOfMet, numOfSubjects);
acceptableMeanT2s = mean(acceptableT2s,1,'omitNaN');
acceptableStdT2s = std(acceptableT2s,1,'omitNaN');
acceptableMeanR2s = mean(acceptableR2s,1,'omitNaN');
acceptableStdR2s = std(acceptableR2s,1,'omitNaN');



plusMinusSign = [' ' char(177) ' '];
tableT2Summarized = cell(numberOfMet+1,4);
tableT2Summarized(2:end,1) = metaboliteNamesDisplay;
tableT2Summarized(1,3) = {'Std'};
tableT2Summarized(1,4) = {['T2' plusMinusSign 'Std']};
tableT2Summarized(1,5) = {['R2' plusMinusSign 'Std']};
tableT2Summarized(2:end,2) = cellstr(num2str(acceptableMeanT2s','%.2f'));
tableT2Summarized(2:end,3) = cellstr(num2str(acceptableStdT2s','%.2f'));
tableT2Summarized(1,2) = {'T2 Summed'};
tableT2Summarized(1,3) = {['T2' plusMinusSign 'Std']};
tableT2Summarized(1,4) = {['R2' plusMinusSign 'Std']};
for index=1:numberOfMet
    tableT2Summarized{index+1,4} = [num2str(acceptableMeanT2s(index), '%.2f') plusMinusSign  num2str(acceptableStdT2s(index), '%.2f')];
    tableT2Summarized{index+1,5} = [num2str(acceptableMeanR2s(index), '%.2f') plusMinusSign  num2str(acceptableStdR2s(index), '%.2f')];
    tableT2Summarized{index+1,3} = [num2str(acceptableMeanT2s(index), '%.1f') plusMinusSign  num2str(acceptableStdT2s(index), '%.1f')];
    tableT2Summarized{index+1,4} = [num2str(acceptableMeanR2s(index), '%.2f') plusMinusSign  num2str(acceptableStdR2s(index), '%.2f')];
end

save([data_path , outputFolder, 'meanT2s_met.mat'],'acceptableMeanT2s', 'acceptableStdT2s', 'metaboliteNamesDisplay');
exportFileXlsx = [data_path , outputFolder, 'T2_Results_Mean_std_met.xlsx'];
xlswrite(exportFileXlsx, tableT2Summarized, 1, 'A1');

%% plot the T2 Box plots
if emptyFigureIds
    figureIds{1} = figure;
else
    figure(figureIds{1});
end

positionsTicks = [2.2:1.5:1.5*numberOfMet+1.5];
positions1 = positionsTicks + offsetPlot;
boxPlotT2 = boxplot(acceptableT2s, metaboliteNamesDisplay,'colors',plotColors{1},'symbol','+','positions',positions1,'width',0.18);
ylabel('T_2   (ms)')
axT2 = get(figureIds{1},'CurrentAxes');
axT2.XAxis.TickLength = [0,0];
axT2.YColor = 'k';

set(boxPlotT2(:,:),'linewidth',1);

set(gca, 'YLim', [-5,200]);
set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);

set(gca, 'YGrid', 'on');
set(gca, 'XTickMode', 'manual');
set(gca, 'XTick', positionsTicks);
set(gca, 'XTickLabelRotation', 90.0);
set(gca, 'FontSize', 16);
% title('T_2^\rho Relaxation Times');
title('T_2 Relaxation Times');

%% FWHM
fwhmReshaped = zeros(size(tableFWHMSubjects,1)*size(tableFWHMSubjects,2), numberOfMet);
for indexMetabolite = 1 :numberOfMet
    
    indexTable = find(strcmp(orderMetFWHMSubjects(:), metaboliteNameOrderFWHM(indexMetabolite)));
    
    fwhms = tableFWHMSubjects(:,:,indexTable);
    fwhms(fwhms>100)= NaN;
    fwhmReshaped(:,indexMetabolite) = fwhms(:);
end

%% T2 corrected FWHM
for i = 1:size(fwhmReshaped,1)
    fwhmReshaped3(i,:) = fwhmReshaped(i,:) - 1./(pi*acceptableMeanT2s*1e-3);
end

fwhmReshaped3 = fwhmReshaped3 - fwhmReshaped3(:,1);

T2RelaxationLinewidth = 1./(pi*acceptableT2s*1e-3);

%% plotting of FWHM
if emptyFigureIds
    figureIds{2} = plotFWHMs(fwhmReshaped, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors, '\Delta\nu_{1/2}', usePpm, yAxisLim_FWHM, offsetPlot);
    figureIds{3} = plotFWHMs(fwhmReshaped3, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, '\Delta\nu_{residual}', usePpm, yAxisLim_Res, offsetPlot);
else
    figureIds{2} = plotFWHMs(fwhmReshaped, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors, '\Delta \nu_{1/2}', usePpm, yAxisLim_FWHM, offsetPlot, figureIds{2});
    figureIds{3} = plotFWHMs(fwhmReshaped3, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, '\Delta\nu_{residual}', usePpm, yAxisLim_Res, offsetPlot, figureIds{3});
end

%%
if emptyFigureIds
    figureIds{4} = plotFWHMs(T2RelaxationLinewidth, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, '\Delta\nu_{1/2} vs. (\pi T_2^{app})^{-1}', usePpm, yAxisLim_FWHM, offsetPlot);
else
    figureIds{4} = plotFWHMs(T2RelaxationLinewidth, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, '\Delta\nu_{1/2} vs. (\pi T_2^{app})^{-1}', usePpm, yAxisLim_FWHM, offsetPlot, figureIds{4});
end
figureIds{4} = plotFWHMs(fwhmReshaped, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors, '\Delta\nu_{1/2} vs. (\pi T_2^{app})^{-1}', usePpm, yAxisLim_FWHM, offsetPlot, figureIds{4});


tableFWHMSummarized = cell(numberOfMet+1,4);
tableFWHMSummarized(2:end,1) = metaboliteNamesDisplay;
tableFWHMSummarized(1,2) = {['FWHM' plusMinusSign 'Std']};
if usePpm
    meanFwhmReshaped = mean(T2RelaxationLinewidth./scanFrequency, 'omitnan');
    stdFwhmReshaped = std(T2RelaxationLinewidth./scanFrequency, 'omitnan');
    writePrecision = '%.3f';
else
    meanFwhmReshaped = mean(T2RelaxationLinewidth, 'omitnan');
    stdFwhmReshaped = std(T2RelaxationLinewidth, 'omitnan');
    writePrecision = '%.2f';
    writePrecision = '%.1f';
end
for index=1:numberOfMet
    tableFWHMSummarized{index+1,2} = [num2str(meanFwhmReshaped(index), writePrecision) plusMinusSign  num2str(stdFwhmReshaped(index), writePrecision)];
end
exportFileXlsx = [data_path ,'Output\FWHM_Results_Mean_std_met.xlsx'];
xlswrite(exportFileXlsx, tableFWHMSummarized, 1, 'A1');


end

function [figId] = plotFWHMs(fwhmReshaped, FWHMSummedNumeric, metaboliteNames, numberOfMet, scanFrequency, ...
    plotColors, titleName, usePpm, yAxisLim, offsetPlot, figId)
%% plot the FWHM Box plots
if ~exist('figId', 'var')
    figId = figure;
else
    figure(figId);
    hold on
end
positionsTicks = 2.2:1.5:1.5*numberOfMet+1.5;
positions = positionsTicks + offsetPlot;
fwhm_plot_limit = 150;

if (usePpm)
    fwhmReshapedPpm = fwhmReshaped ./ scanFrequency;
    fwhm_plot_limit_ppm = fwhm_plot_limit ./ scanFrequency;
    %set yyaxis left active, hence both
    yyaxis left
end

boxPlotFWHM = boxplot(fwhmReshaped, metaboliteNames,'colors',plotColors{1},'symbol','+','positions',positions);

if (usePpm)
    yyaxis right
    boxPlotFWHMPpm = boxplot(fwhmReshapedPpm, metaboliteNames,'colors',plotColors{1},'symbol','+','positions',positions);
    set(gca,'YColor','k');
    yyaxis left
end

ylabel('\Delta\nu (Hz)')
if usePpm
    yyaxis right
    ylabel('\Delta\nu (ppm)', 'Color', 'k')
    yyaxis left
end

ax = get(figId,'CurrentAxes');
ax.XAxis.TickLength = [0,0];
ax.YColor = 'k';

set(boxPlotFWHM(:,:),'linewidth',1);
set(gca, 'YGrid', 'on');
set(gca, 'XTickMode', 'manual');
set(gca, 'XTick', positionsTicks);
set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
set(gca, 'XTickLabelRotation', 90.0);
set(gca, 'FontSize', 16);
title(titleName);

if usePpm
    set(gca, 'YLim', [yAxisLim(1),yAxisLim(2)]);
    yyaxis right
    set(gca, 'YLim', [yAxisLim(1)/scanFrequency,yAxisLim(2)./scanFrequency]);
else
    set(gca, 'YLim', [0,yAxisLim(2)]);
end
end
