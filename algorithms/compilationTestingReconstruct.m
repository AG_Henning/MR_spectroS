function [a] = compilationTestingReconstruct(fid_id, isMC, isInVivo, weights, saveResults)

if ~exist('fid_id','var')
    fid_id = 135;
else
    if ~isnan(str2double(fid_id))
        fid_id = str2double(fid_id);
    end
end

if ~exist('isMC', 'var')
    isMC = true;
else
    if ischar(isMC)
        if isnan(str2double(isMC))
            isMC = strcmpi(isMC,'true');
        else
            isMC = str2double(isMC);
        end
    end
end

if ~exist('isInVivo', 'var')
    isInVivo = true;
else
    if ischar(isInVivo)
        if isnan(str2double(isInVivo))
            isInVivo = strcmpi(isInVivo,'true');
        else
            isInVivo = str2double(isInVivo);
        end
    end
end

if ~exist('saveResults', 'var')
    saveResults = true;
else
    if ischar(saveResults)
        if isnan(str2double(saveResults))
            saveResults = strcmpi(saveResults,'true');
        else
            saveResults = str2double(saveResults);
        end
    end
end

if isMC
    a = MR_spectroS(fid_id,1);
else
    a = MR_spectroS(fid_id);
end

if isInVivo
    %% truncate before anything
    trunc_ms = 250;
    truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
    a = a.Truncate(truncPoint);

    a = a.FrequencyAlign;
end
if isMC
    if isInVivo
        a = a.Rescale;
    end
    a = a.ReconData;
end

a = a.AverageData;

if isMC
    a = a.EddyCurrentCorrection;
else
    [FileName, PathName] = uigetfile('*.dat',['Select water reference for :' a.Parameter.Filename]);
    path = strcat(PathName,FileName);
    tmpData      = mapVBVD(path);
    tmpWaterReferenceData = tmpData.image(''); %Read the data
    waterReferenceData = double(permute(tmpWaterReferenceData,[1 3 4 2 5 7 8 11 12 9 10 6]));
    a = a.EddyCurrentCorrection(waterReferenceData);
end
if exist('weights','var')
    if ~isempty(weights)
        a = a.CombineCoils(weights);
    else
        a = a.CombineCoils;
    end
else
    a = a.CombineCoils;
end

%% setup the settings for the FrequencyAlignFreqDomain
a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;

%indices of Interest: given in ppm
% water peak
a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = [2.008];

%set zeroFillingParameter to get smooth approximations
a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;

%search area (+-) in ppm
if isInVivo
    a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.1;
else
    a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.2;
end
%spline filtering is not needed if the spectra are not too noisy.
%flag to do spline filtering
a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
%spline filtering coefficient
a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;
% do the actual Frequency Alignment
a = a.FrequencyAlignFreqDomain;
%%
if isInVivo
    a.Parameter.HsvdSettings.bound = [-160 160];
    a.Parameter.HsvdSettings.bound = [-100 100];
else
    a.Parameter.HsvdSettings.bound = [-100 100];
end
a.Parameter.HsvdSettings.n = size(a.Data{1},1);
a.Parameter.HsvdSettings.p = 25;
% [~, a] = a.Hsvd;
% [~, a] = a.Hsvd;
% [~, a] = a.Hsvd;

if isMC
    %% Reverse MC
    a = a.ReverseMC;
%     [~, a] = a.Hsvd;
end

%
if isInVivo
    trunc_ms = 200;
    truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
    a = a.Truncate(truncPoint);
end

if saveResults
    %saving the file
    save([a.Parameter.Filename(1:end-4) '.mat'], 'a')
    filename = a.Parameter.Filename(1:end-4);
    %export to LCModel
    addSinglet0ppm = true;
    a.ExportLcmRaw('',filename, addSinglet0ppm);
end
end