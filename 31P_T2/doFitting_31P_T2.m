function doFitting_31P_T2()
clear; clc; close all;

%%%%%%%%
%check TruncToFit, controlFilesBase, STEAMVersion

pathName = 'T2 31P Paper Data';
[localFilePathBase] = pathToDataFolder(pathName);

subjects = {'9873';'1090';'3490';'9810';'1658';'2017';'6524';'3009';...
    '6260';'2823';'2455';'4085'};
prefixes = {'2019-09-30_';'2019-10-10_';'2019-10-14_';'2019-10-14_';...
    '2019-10-22_';'2019-11-07_';'2019-11-07_';'2019-11-20_';...
    '2019-11-20_';'2019-12-05_';'2020-07-22_';'2020-07-23_';};
suffixFolder = '_SurfCoil';
STEAMVersion = '';      %'Ideal_JD'

%%%%%%%%%%%%%%
%Select
trunc_ms = 90;
TruncToFit = 'noTrunc';        %'noTrunc';   %'Trunc90ms';


% controlFilesBase = 'fitsettings_ZZZZ_STEAM_Ideal_JD_XXXX_TM5ms_LW_Trunc90ms';
% controlFilesBase = 'fitsettings_ZZZZ_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms';
% % % % controlFilesBase = 'fitsettings_ZZZZ_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noBCoupl';
% % % % controlFilesBase = 'fitsettings_ZZZZ_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noAGCoupl';
% % % % controlFilesBase = 'fitsettings_ZZZZ_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noBCoupl_noAGCoupl';
controlFilesBase = 'fitsettings_ZZZZ_STEAM_Real_XXXX_TM5ms_LW_Trunc90ms';
%%%%%%%%%%%%%%


%be careful with Pi intra/extra, NADH and bATP!
metaboliteNames = {'PE', 'PC', 'Pi_int', 'GPE', 'GPC', 'PCr', 'gATP', 'aATP', 'NADH'};
centerPpm =       [ 6.77, 6.23, 4.83,     3.51,   2.96,  0,     -2.48, -7.62,  -8.16];

TEs = {'6'; '8'; '11'; '15'; '20'; '30'; '50'; '80'; '150'};  %ms
numberOfSubjects = length(subjects);
paths = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    paths{indexSubject} = [localFilePathBase prefixes{indexSubject} subjects{indexSubject} suffixFolder '\'];
end

files_TE6 = cell(1, numberOfSubjects);
files_TE8 = cell(1, numberOfSubjects);
files_TE11 = cell(1, numberOfSubjects);
files_TE15 = cell(1, numberOfSubjects);
files_TE20 = cell(1, numberOfSubjects);
files_TE30 = cell(1, numberOfSubjects);
files_TE50 = cell(1, numberOfSubjects);
files_TE80 = cell(1, numberOfSubjects);
files_TE150 = cell(1, numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    files_TE6{indexSubject} = ls([paths{indexSubject}, '*_TE6ms_*.dat']);       %TE6ms x subjects
    files_TE8{indexSubject} = ls([paths{indexSubject}, '*_TE8ms_*.dat']);
    files_TE11{indexSubject} = ls([paths{indexSubject}, '*_TE11ms_*.dat']);
    files_TE15{indexSubject} = ls([paths{indexSubject}, '*_TE15ms_*.dat']);
    files_TE20{indexSubject} = ls([paths{indexSubject}, '*_TE20ms_*.dat']);
    files_TE30{indexSubject} = ls([paths{indexSubject}, '*_TE30ms_*.dat']);
    files_TE50{indexSubject} = ls([paths{indexSubject}, '*_TE50ms_*.dat']);
    files_TE80{indexSubject} = ls([paths{indexSubject}, '*_TE80ms_*.dat']);
    files_TE150{indexSubject} = ls([paths{indexSubject}, '*_TE150ms_*.dat']);
end

numberOfTEs = length(TEs);

filesPath = cell(numberOfTEs,numberOfSubjects);
files = [files_TE6; files_TE8; files_TE11; files_TE15; files_TE20; files_TE30; files_TE50; files_TE80; files_TE150];    %TE x subjects
for indexSubject = 1:numberOfSubjects
    for indexTE = 1:numberOfTEs
        filesPath{indexTE, indexSubject} = [paths{indexSubject} files{indexTE, indexSubject}];   %TE x subjects
    end
end

%% process T2 data and save .RAW file
%check TEs, was TE = [6; 8; ...] before, now it is a cell

% for indexSubject = 1:numberOfSubjects
%     for indexTE = 1:numberOfTEs
%         currentFile = filesPath{indexTE, indexSubject};
% 
%         reconstruct_31P_T2(currentFile,TEs,prefixes,subjects,paths,metaboliteNames,centerPpm,trunc_ms,indexTE,indexSubject)
%         close all
%     end
% end


%% LCModel Fitting
extensionTable = '.table';
extensionCoord = '.coord';
defaultTE = 'XXXX';
defaultSubjectsPath = 'YYYY';
defaultSubject ='ZZZZ';
defaultTrunc = 'Trunc90ms';
defaultLCModelUser = 'jdorst';

controlFilesBaseSuffix = '.control';
outputFileNameBase = strcat(defaultTE, '_');

%% file paths setup
controlFilesPathRemote = '/Desktop/31P_T2_Paper/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/31P_T2_Paper/Output/';
LCModelOutputPath = [localFilePathBase, 'Output/'];

defaultControlFile = strcat(controlFilesBase, controlFilesBaseSuffix);

subjectsPath = strcat(prefixes, subjects, suffixFolder);

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath, '/');
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath, '/');
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath, '/');
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath, '/');
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath, '/');

%% basic configurations
if ~isempty(strfind(controlFilesBase,'_noBCoupl')) && isempty(strfind(controlFilesBase,'_noAGCoupl'))
    LCModelOutputFiles = strcat(subjects, '_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noBCoupl');
elseif ~isempty(strfind(controlFilesBase,'_noAGCoupl')) && isempty(strfind(controlFilesBase,'_noBCoupl'))
    LCModelOutputFiles = strcat(subjects, '_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noAGCoupl');
elseif ~isempty(strfind(controlFilesBase,'_noBCoupl_noAGCoupl'))
    LCModelOutputFiles = strcat(subjects, '_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noBCoupl_noAGCoupl');
else
    if ~isempty(strfind(controlFilesBase,'_Ideal'))
        LCModelOutputFiles = strcat(subjects, '_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms');
    elseif ~isempty(strfind(controlFilesBase,'_Real'))
        LCModelOutputFiles = strcat(subjects, '_STEAM_Real_XXXX_TM5ms_LW_Trunc90ms');
    end
end
%
numberOfSubjects = length(subjects);
% TEsCell = num2cell(TEs);
%% do the LCModel fitting
% % % LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

% do the actual LCModel fitting
parfor indexCurrentSubject = 1:numberOfSubjects
% for indexCurrentSubject = 1:numberOfSubjects
    %     for indexCurrentTE = 1:numberOfTEs
    %% do the LCModel fitting
    LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
    
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    
    %% create the control file series
    for indexCurrentTE = 1:numberOfTEs
        if ~isempty(strfind(STEAMVersion,'Ideal_JD'))
            currentTE = strcat('JD_TE',num2str((TEs{indexCurrentTE})),'ms');
        else
            currentTE = strcat('TE',num2str((TEs{indexCurrentTE})),'ms');
        end
        currentOutputFile = strrep(LCModelOutputFiles{indexCurrentSubject}, defaultTE, currentTE);
        defaultOutputFile = 'defaultOutputFile';
        createLCModelConfig_31P_T2(controlFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, defaultControlFile, ...
            subjectsPath{indexCurrentSubject}, defaultSubjectsPath, subjects{indexCurrentSubject}, defaultSubject,...
            currentTE,defaultTE,currentOutputFile,defaultOutputFile,TruncToFit,defaultTrunc);
    end
    if ~isempty(strfind(STEAMVersion,'Ideal_JD'))
        currentControlFiles = strrep(strrep(strrep(defaultControlFile,defaultTE,strcat('JD_TE',TEs,'ms')),defaultSubject,subjects{indexCurrentSubject}),defaultTrunc,TruncToFit);
        currentOutputFiles  = strrep(strrep(LCModelOutputFiles{indexCurrentSubject}, defaultTE, strcat('JD_TE',TEs,'ms')),defaultTrunc,TruncToFit);
    else
        currentControlFiles = strrep(strrep(strrep(defaultControlFile,defaultTE,strcat('TE',TEs,'ms')),defaultSubject,subjects{indexCurrentSubject}),defaultTrunc,TruncToFit);
        currentOutputFiles  = strrep(strrep(LCModelOutputFiles{indexCurrentSubject}, defaultTE, strcat('TE',TEs,'ms')),defaultTrunc,TruncToFit);
    end
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
end

createConcTable(numberOfSubjects,LCModelOutputFiles,LCModelOutputPath,defaultTE,TEs,prefixes,subjects,subjectsPath,defaultTrunc,TruncToFit,STEAMVersion)


end


%% create concentration table
function createConcTable(numberOfSubjects,LCModelOutputFiles,LCModelOutputPath,defaultTE,TEs,prefixes,subjects,subjectsPath,defaultTrunc,TruncToFit,STEAMVersion)
tableConcentrations = [];
isFirstIter = 1;
s = 1;
for indexCurrentTE = 1:length(TEs)
    for indexCurrentSubject = 1:numberOfSubjects
        % retrieve parameters
        if ~isempty(strfind(STEAMVersion,'Ideal_JD'))
            currentTE = strcat('JD_TE',num2str((TEs{indexCurrentTE})),'ms');
        else
            currentTE = strcat('TE',num2str((TEs{indexCurrentTE})),'ms');
        end
%         currentTE = strcat('TE',num2str((TEs{indexCurrentTE})),'ms');
        currentOutputFiles = strrep(strrep(LCModelOutputFiles, defaultTE, currentTE),defaultTrunc,TruncToFit);
        currentPath = strrep(strcat(LCModelOutputPath,subjectsPath,'/',currentOutputFiles,'.table'),defaultTrunc,TruncToFit);
        [c1 c2 c3 c4] = textread(currentPath{indexCurrentSubject},'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
%             tableConcentrations{1,s} = 'water concentration';
        end
        
        % add ID and Water conc to table
        tableConcentrations{((indexCurrentTE-1)*12+2)+indexCurrentSubject-1,1} = strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject}, '_', strcat('TE',num2str((TEs{indexCurrentTE})),'ms'));
%         index_wconc = find(strcmp(c1,'wconc='));
%         wconc_LCModel = str2num(c2{index_wconc});
%         tableConcentrations{indexCurrentSubject*2+indexorderFMRS-1,end} = wconc_LCModel;
        % add metabolites to table
        s = 2;
        for j=start:finish
            %add quantified metabolite
            tableConcentrations{((indexCurrentTE-1)*12+2)+indexCurrentSubject-1,s} = str2num( c1{j});
            %add CRLB
            tableConcentrations{((indexCurrentTE-1)*12+2)+indexCurrentSubject-1,s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end
end

% save table to file
if ~isempty(strfind(STEAMVersion,'Ideal_JD'))
    ExpName = strrep(strcat('LCModelFit_', strrep(LCModelOutputFiles{1}(6:end),strcat(defaultTE,'_'),'JD_')),defaultTrunc,TruncToFit);
else
    ExpName = strrep(strcat('LCModelFit_', strrep(LCModelOutputFiles{1}(6:end),strcat(defaultTE,'_'),'')),defaultTrunc,TruncToFit);
end
exportFileXlsx = [LCModelOutputPath ,strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)

close all
end

