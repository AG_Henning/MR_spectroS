%% ISIS scheme averaging
function [this] = ISIS_Averaging(this)

if this.Parameter.ReconFlags.isISISAveraged
    warning('The data are already averaged')
end

scheme = this.Parameter.ISISSettings.scheme;
nSamples      = size(this.Data{1},this.kx_dim);
nCoils        = size(this.Data{1},this.coil_dim);
nAverages     = size(this.Data{1},this.meas_dim);   

Data       = this.Data{1};       %Columns, Lines, Partitions, Coils, Slices, Cardiac Phases...
                                 %Echos, NaN, NaN (New Mixes),Repetitions, Sets, Averages
tmp        = squeeze(Data);
if nCoils == 1
    tmp = permute(tmp,[1 3 2]);
end

if strcmp(scheme,'ISIS-orig')
    nISIS  = 8;
    nSpec  = nAverages/nISIS;
    tmp22  = zeros(nSamples,nCoils,8);
    tmp22Old = zeros(nSamples,nCoils,8);
    %average measurements
    for idxOfCh = 1:nCoils
        for idxOfISIS = 0:nISIS-1
            for idxOfSpec = 0:nSpec-1
                tmpData = tmp(:,idxOfCh,1+idxOfISIS+idxOfSpec*8);
                tmp22(:,idxOfCh,idxOfISIS+1) = tmp22Old(:,idxOfCh,idxOfISIS+1) + tmpData;
                tmp22Old = tmp22;
            end
        end
    end

elseif strcmp(scheme,'EISIS-full')
    nISIS  = 36;
    nSpec  = nAverages/nISIS;
    tmp22  = zeros(nSamples,nCoils,nISIS);
    tmp22Old = zeros(nSamples,nCoils,nISIS);
    for idxOfCh = 1:nCoils
        for idxOfISIS = 1:nISIS
            for idxOfSpec = 0:nSpec-1
                tmpData = tmp(:,idxOfCh,idxOfISIS+nISIS*idxOfSpec);
                tmp22(:,idxOfCh,idxOfISIS) = tmp22Old(:,idxOfCh,idxOfISIS) + tmpData;
                tmp22Old = tmp22;
            end
        end
    end
else
    error('ISIS scheme should be ISIS-orig or EISIS-full')
end

this.Data{1}                                = double(permute(tmp22,[1,4,5,2,6,7,8,9,10,11,12,3]));
this.Parameter.ReconFlags.isISISAveraged    = true;
