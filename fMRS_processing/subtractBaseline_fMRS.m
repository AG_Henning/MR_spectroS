function subtractBaseline_fMRS(currentDataPath,subjectsLCModelOutputPath,currentLCModelFit,currentLCModelFitWithoutMetabolite, Met);

currentLCModelMatFit = strrep(strcat(currentLCModelFit, '.mat'),'#','_');

indexStart = strfind(currentLCModelFitWithoutMetabolite,'_woBaseline');
if ~isempty(indexStart)
    currentLCModelOutputFit = currentLCModelFitWithoutMetabolite(1:indexStart-1);
    currentLCModelCoordFit = strcat(currentLCModelOutputFit, '.coord');
else
    currentLCModelCoordFit = strcat(currentLCModelFitWithoutMetabolite, '.coord');
end

if ~isempty(strfind(currentLCModelFitWithoutMetabolite, '_v2'))
    currentRawOutputFile = strcat(strrep(currentLCModelFit,'_Av','_v2_Av'), '_woBaseline');
else
    currentRawOutputFile = strcat(currentLCModelFit, '_woBaseline');
end

if Met == 'Lac'
    Met = 1.314;
elseif Met == 'NAA'
    Met = 2.008;
else
    error('Met needs to be Lac or NAA');
end
% metabolitesToSubtract = {'Glu'};
doNullingOfDownField = false;
if ~isempty(strfind(currentLCModelFit,'#'))
    reconstructedSpectra = strrep(strcat(currentDataPath,strrep(currentLCModelMatFit,'_1','#1')),'_v2','');
else
    reconstructedSpectra = strrep(strcat(currentDataPath,currentLCModelMatFit),'_v2','');
end
reconstructedSpectraWithoutMetabolite = strcat(currentDataPath, currentRawOutputFile);

phase90 = -90 * pi/180;
zeroFillFactor = 2;

%plot configs
FontSize = 14;
LineWidth = 1.5;

% load(reconstructedSpectra, 'a');
a = load(reconstructedSpectra);
fields = fieldnames(a);
a = a.(fields{1});
[nOfPoints, ppmVector, phasedData, fitData, baselineData, residualData, ...
    metaboliteNames, metaboliteData, tableConcentrations, ppmStart, ppmEnd, ...
    scanFrequency,  frequencyShift, ppmGap] = ...
    extractFits(subjectsLCModelOutputPath,currentLCModelCoordFit);

% align the two spectra
fid = a.Data{1};
spectrum = fftshift(fft(fid(:,1,1,1,1,1,1,1,1,1,1,1),size(fid,1)*zeroFillFactor));
ppmSpectrum = a.getPpmVector(zeroFillFactor);

[~, maxPpmValueSpectrum] = getMaxPeak(ppmSpectrum', real(spectrum), Met, 0.1);
[~, maxPpmValueLCModel] = getMaxPeak(ppmVector, phasedData, Met, 0.1);

frequencyShiftDiff = maxPpmValueSpectrum - maxPpmValueLCModel;
fMRSData_withBaseline = a.ApplyFrequencyShift(frequencyShiftDiff);
%get the aligned reconstructed spectra
fid = fMRSData_withBaseline.Data{1};
spectrum = fftshift(fft(fid(:,1,1,1,1,1,1,1,1,1,1,1),size(fid,1)*zeroFillFactor));
% max of the reconstructed spectrum
if isempty(ppmGap)
    ppmSpectrumMask  = (ppmSpectrum < ppmStart) & (ppmSpectrum > ppmEnd);
    [~,idx] = find(ppmSpectrumMask == 1);
    ppmSpectrumMask(idx(1)-1) = 1;
%     ppmSpectrumMask(idx(end)+1) = 1;
    ppmMetaboliteMask  = (ppmVector < ppmStart) & (ppmVector > ppmEnd);
else
    ppmSpectrumMask  = (ppmSpectrum < ppmGap) & (ppmSpectrum > ppmEnd);
    ppmMetaboliteMask  = (ppmVector < ppmGap) & (ppmVector > ppmEnd);
end
maxSpectrum = max(real(spectrum(ppmSpectrumMask)));     %spectrum between 0.6 and 4.1 ppm
% max of the LCModel fit
maxPhasedData = max(phasedData);
% calculate scaling
scalingFactor = maxSpectrum/maxPhasedData;
oldSpectrum = spectrum;
% for indexMetabolite = 1:length(metabolitesToSubtract)
%     metaboliteToSubtract = metabolitesToSubtract{indexMetabolite};
%     indexMetaboliteToSubtract = find(strcmp(metaboliteNames,metaboliteToSubtract));
%     if isempty(indexMetaboliteToSubtract)
%         error('Metabolite %s not found in fit',metaboliteToSubtract);
%     end
    
%     spectrumToSubtract = metaboliteData{indexMetaboliteToSubtract}.metaboliteSpectrum(ppmMetaboliteMask);
    spectrumToSubtract = baselineData;
    spectrumToSubtract(1) = 0;
    %eliminate first point, which is anyway an error; Also scale it
    spectrumToSubtractFlipped = flipud(spectrumToSubtract).*scalingFactor;
    
    spectrumToSubtractReal = zeros(size(oldSpectrum));
    spectrumToSubtractReal(ppmSpectrumMask) = spectrumToSubtractReal(ppmSpectrumMask) + spectrumToSubtractFlipped;
    fidToSubtract = ifft(ifftshift(complex(spectrumToSubtractReal)));
    fidToSubtract(length(fidToSubtract)/2:end)=0;
    spectrumToSubtractPhased90 = fftshift(fft(fidToSubtract .* exp(1i*phase90))).*2; %multiply by 2 to account for nulling the half of the FID
    spectrumToSubtractImag = real(spectrumToSubtractPhased90);
    newSpectrumReal = real(oldSpectrum) - spectrumToSubtractReal;
    newSpectrumImag = imag(oldSpectrum) - spectrumToSubtractImag;
    newSpectrum = complex(newSpectrumReal, newSpectrumImag);
    
    if (doNullingOfDownField)
        newSpectrum(length(newSpectrum)/2:end) = 0;
    end
    
    oldSpectrum = newSpectrum;
% end

figure
% plot(ppmVector, phasedData.*scalingFactor)
% hold on
plot(ppmSpectrum, real(spectrum),'r')
hold on
plot(ppmSpectrum, real(newSpectrum),'k')
set(gca,'xDir','reverse')
xlim([0 4.2])
title(sprintf('Difference spectrum with and w/o baseline'))
legend('Before subtraction','After subtration')

fig2save_Conc = fullfile(currentDataPath, strcat(strrep(currentRawOutputFile,'#','_'), '.fig'));
savefig(fig2save_Conc)

close all

% figure(101)
% hold on
% p1 = plot(ppmSpectrum, real(spectrum), 'r');
% hold on
% p2 = plot(ppmSpectrum, real(newSpectrum),'k');
% xlim([0 4.2])
% xlabel('[ppm]');
% %     ylim([(indexTE+1) * offsetPlot-offsetPlot*0.5 5e-5]);
% set(gca,'xDir','reverse')
% set(gca,'ytick',[]);
% set(gca,'fontsize',FontSize);
% set(p1,'LineWidth',LineWidth);
% set(p2,'LineWidth',LineWidth);
% title('Macromolecular baseline with and w/o the Cr peak')

fMRSData_woBaseline = fMRSData_withBaseline;
newFid = ifft(ifftshift(newSpectrum));
fMRSData_woBaseline.Data{1} = newFid;
% trunc_ms = 250;
% truncPoint = floor( trunc_ms/(fMRSData_woBaseline.Parameter.Headers.DwellTimeSig_ns*1e-6));
% fMRSData_woBaseline = fMRSData_woBaseline.Truncate(truncPoint);

ExpName = fullfile(currentDataPath, strcat(currentRawOutputFile, '.fig'));
% eval([currentLCModelFitWithoutMetabolite '=fMRSData_woBaseline'])
% save(reconstructedSpectraWithoutMetabolite,currentRawOutputFile);
save(strrep(reconstructedSpectraWithoutMetabolite,'#','_'),'fMRSData_woBaseline');
fMRSData_woBaseline.ExportLcmRaw(currentDataPath, strrep(currentRawOutputFile,'#','_'), 'Yes', 'Yes');
end

function [maxValue, maxPpmValue] = getMaxPeak(ppmVector, spectrum, peakPpm, searchAreaPpm)
minPpm = peakPpm - searchAreaPpm;
maxPpm = peakPpm + searchAreaPpm;
ppmMask = (ppmVector > minPpm) & (ppmVector < maxPpm);
tempSpectrum = spectrum .* ppmMask;
[maxValue, maxIndex] = max(real(tempSpectrum));
maxPpmValue = ppmVector(maxIndex);
end
