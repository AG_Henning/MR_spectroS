function this = ApplyFrequencyShift(this, frequencyShiftsPpm)

fprintf('Reconstrustion of the data. Please wait...\n')
 
%retrieve general information from the dataset
oldData     = this.Data{1};
newData     = zeros(size(oldData));
nCoils      = size(oldData, this.coil_dim);
nAverages   = size(oldData,this.meas_dim);
samples     = size(oldData, this.kx_dim);
nMix        = size(this.Data{1}, this.mix_dim);
dwellTime   = this.Parameter.Headers.DwellTimeSig_ns*1e-9;
scanFreq    = this.Parameter.Headers.ScanFrequency;
%time Vector for frequency shift calculations
timeVector    = (0:samples-1)*dwellTime;

if (length(frequencyShiftsPpm) == 1)
    frequencyShiftsPpm(1:nAverages) = frequencyShiftsPpm;
end
frequencyShifts = frequencyShiftsPpm *scanFreq*1e-6;

for indexOfAverage=1:nAverages
  for indexOfChannel=1:nCoils
      for indexMix = 1:nMix
          signal         = oldData(:, 1, 1, indexOfChannel, 1, 1, 1, 1, indexMix, 1, 1, indexOfAverage);
          %apply the frequency shift on the time domain vector
          signalAligned  = signal.* exp(1i*2*pi*frequencyShifts(indexOfAverage)*timeVector )';
          %1:NCol, 1, 1, 1:NCha, 1, 1, 1, 1, 1, 1:NRep, 1, 1:NSet
          newData(:, 1, 1, indexOfChannel, 1, 1, 1, 1, indexMix, 1, 1, indexOfAverage)=signalAligned;
      end
  end
end

this.Data{1}                                    = newData;
% this.Parameter.ReconFlags.isFrequencyAligned    = true;

fprintf('Frequency alignment...FINISHED\n')     

end