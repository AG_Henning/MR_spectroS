function missingPointPredArrray    = missingPointPredictionArray(data, imiss, T_factor, polyOrder)

if (imiss <= 0)
    missingPointPredArrray = data; % do nothing to the data
else
    comb = data(imiss+1:end);
    missingPointPredArrray( imiss+1:imiss+size(comb,1),:,: ) = comb;
    if (imiss > 0) && (T_factor > 0)
        T = round(imiss/T_factor);
        L = size( missingPointPredArrray, 1 ) - imiss;
        signal = flip( missingPointPredArrray(imiss+1:imiss+L) );
        acoeffs = aryule( signal( L-floor(length(signal)/T-1)*T:T:L, : ), polyOrder );
        pred = zeros( imiss, size(missingPointPredArrray,2) );
        L = size( acoeffs,2 );
        for k = 1 : imiss
            pred(k,:) = sum(-flip(acoeffs(:,2:end),1)'.*signal(end-(L-2)*T:T:end,:));
            signal(end+1,:) = pred(k,:);
        end
        missingPointPredArrray( 1:imiss ) = flip( pred, 1 );
    end
end