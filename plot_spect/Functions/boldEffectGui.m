function boldEffectGui( handles )
this                     = handles.MRSpectroMatrix.getCurrent;
selectedProcessingValues = handles.SelectedProcessingValues;

%Selected Display Values
selectedCoil      = get(handles.SelectedDisplayValues, 'selectedCoil')       ;
selectedMix       = get(handles.SelectedDisplayValues, 'selectedMix')        ;
boldEffect(this, selectedProcessingValues, selectedCoil, selectedMix);