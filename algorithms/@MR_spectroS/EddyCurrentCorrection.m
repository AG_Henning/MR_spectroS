function this = EddyCurrentCorrection(this, waterReferenceData)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

correctedWater              = this.Parameter.EddyCurrSettings.correctedWater;

if this.Parameter.ReconFlags.isEddyCurrentCorrected
    error('The data is already EddyCurrentCorrected')
end

 if (~this.Parameter.ReconFlags.isEddyCurrentCorrected && this.Parameter.ReconFlags.isDataPhased)
    warning('data was already phased in the post-processing. eddy current correction may spoil the final reconstructed signal');
 end 
       
 data = this.Data{1};
 if ~exist('waterReferenceData', 'var')
    %% Eddy Current Correction without an external water reference data
     %there is no water reference data, so performing ECC using the own
     %data (metabolite cycled water or own phase
     if correctedWater         
         if (size(data,this.mix_dim) == 2)
             display('Eddy Current Correction using the metabolite cycled water signal as water reference');
             waterRef = repmat(data(:,:,:,:,:,:,:,:,2,:,:,:),[1 1 1 1 1 1 1 1 2 1 1]);
         else
             display('Eddy Current Correction using the own phase of the data');
             waterRef = repmat(data(:,:,:,:,:,:,:,:,1,:,:,:),[1 1 1 1 1 1 1 1 1 1 1]);
         end         
         data     = data.*exp(-1j*angle(waterRef));
     else
         display('Hi, can you tell me what are you doing here? What kind of Eddy Current Correction is this you are applying? Thanks, Tamas');
         
         waterRef = cat(this.mix_dim,data(:,:,:,:,:,:,:,:,2,:,:,:),ones(size(data)));
         data     = data.*exp(-1j*angle(waterRef));
         
     end
 else
     %% TODO: rethink, whether one shouldn't use the waterReferenceData as an MR_SpectroS object
     %% Eddy Current Correction using an external file
     if ~this.Parameter.ReconFlags.isAveraged
         error('The data has to be averaged to apply external water reference for Eddy Current Correction')
     end
     %% Sorting of water reference for the case it isn't
     % If NRep == 1 then the data does not have to be sorted
     if size(waterReferenceData,this.dyn_dim)~=1;
         NDyns      = size(waterReferenceData,this.dyn_dim);
         nAverages  = size(waterReferenceData,this.meas_dim);
         newDataSize= size(waterReferenceData);
         newDataSize(this.dyn_dim)       = 1;
         newDataSize(this.extr2_dim)     = 1;
         newDataSize(this.meas_dim)      = NDyns*nAverages;
         newData    = zeros(newDataSize);
         oldData    = waterReferenceData;
         for i = 1:NDyns
             for j = 1: nAverages
                 newData(:,:,:,:,:,:,:,:,:,1,:,j+(i-1)*nAverages)  =  oldData(:,:,:,:,:,:,:,:,:,i,:,j);
             end
         end
         waterReferenceData     = newData;
     end
     %% Zerofilling of water reference data
     if this.Parameter.ReconFlags.isZeroFilled
         % ZeroFilling the spectrum
         zeroFillFactor =  this.Parameter.ZeroFillSettings.ZerroFillFactor;
         % we use the parameters from this, since we assume an identical structure of the water reference
         initialVectorSize = size(waterReferenceData, this.kx_dim);
         newWaterReferenceSize = size(waterReferenceData);
         newWaterReferenceVectorSize  =  zeroFillFactor * initialVectorSize;
         newWaterReferenceSize(this.kx_dim) =  newWaterReferenceVectorSize;
         newWaterReferenceData =  zeros(newWaterReferenceSize);
         newWaterReferenceData(1:initialVectorSize,:,:,:,:,:,:,:,:,:,:,:) =  waterReferenceData(:,:,:,:,:,:,:,:,:,:,:,:);
         waterReferenceData = newWaterReferenceData;
     else
         % no zerofilling of the water spectrum is required
     end
     if (size(waterReferenceData, this.kx_dim) ~= size(data, this.kx_dim))
         error('Size of water reference  and data not equal ');
     end
     %% Averaging the water reference data
     % Get the number of blocks of the data (only important for external water references. If one uses metabolite cycling, like above, the
     % number of water signals is identical to the number of metabolite signals)
     nBlocks = this.Parameter.AverageSettings.nBlocks;
     % average the data based on the number of blocks
      nAverages     = size(waterReferenceData, this.meas_dim);
%      if mod(nAverages, nBlocks)~=0
%          error('The number of measurements of the water reference should be multiple of the number of blocks')
%      end
     averagesPerBlock            = nAverages/nBlocks;
     
     for indexOfBlocks=1:nBlocks
         %TODO: this line is wrong...
         averagedWaterReferenceData(:,1,1,:,1,1,1,1,1,1,1,indexOfBlocks)     =...
             sum(waterReferenceData(:,1,1,:,1,1,1,1,1,1,1,(1:averagesPerBlock)+(indexOfBlocks-1)*averagesPerBlock),this.meas_dim);
     end
     
     %% TODO: add combine coils with the weights - one has to add the weights as a parameter
     %% Apply the actual Eddy Current Correction using the phase of the water reference (Klose correction)
     data     = data.*exp(-1*j*angle(averagedWaterReferenceData)); %
 end
 this.Data{1} = data;
 this.Parameter.ReconFlags.isEddyCurrentCorrected= true;
                 
end









% %% OLD CODE  
%        howManyAveragedWaterSignals = this.Parameter.EddyCurrSettings.howManyAveragedWaterSignals;
%        everyHowManyAcquisitions    = this.Parameter.EddyCurrSettings.everyHowManyAcquisitions;
%        correctedWater              = this.Parameter.EddyCurrSettings.correctedWater;
%        numberOfWaterSignals        = this.Parameter.EddyCurrSettings.numberOfWaterSignals;
%        
%        nCoils                      = size(this.Data{1},this.coil_dim);
%        nDyns                       = size(this.Data{1},this.dyn_dim);
%        nMixes                      = size(this.Data{1},this.mix_dim);
%        nAverages                   = size(this.Data{1},this.meas_dim);
%        newData                     = this.Data{1};
%                
%        
%        waterSignalsSpacing        = numberOfWaterSignals/howManyAveragedWaterSignals;
%        aquisitionsSignalsSpacing  = nAverages/everyHowManyAcquisitions;
%        
%        
% if (size(this.Data{1},this.mix_dim)==2 )       
%        
%       if mod(numberOfWaterSignals,howManyAveragedWaterSignals)
%          error('Number of water signals should be multiple of averaged water signals');
%       end
%       
%       if mod(nAverages,everyHowManyAcquisitions)
%          error('Number of averages should be everyHowManyAquisitions');
%       end
%       
%       if waterSignalsSpacing ~=aquisitionsSignalsSpacing
%          error('The spacing between water signals and FID signals should be equal');
%       end
%       
%       for indexWaterLoop= 0:waterSignalsSpacing-1
%           
%            referenceWaterSignal= sum(newData(:,1,1,:,1,1,1,1,2,1,1,[1:howManyAveragedWaterSignals]+indexWaterLoop*howManyAveragedWaterSignals),this.meas_dim);
%             %={'kx','ky','kz','coil','5','6','7','8','mix','dyn',11,'meas')
%           
%           for indexAquisitionsLoop= 1:everyHowManyAcquisitions
%             
%               
%               
%               newData(:,1,1,:,1,1,1,1,1,1,1,indexAquisitionsLoop+indexWaterLoop*everyHowManyAcquisitions)=...
%                   newData(:,1,1,:,1,1,1,1,1,1,1,indexAquisitionsLoop+indexWaterLoop*everyHowManyAcquisitions)...
%                   .*exp(-1j*angle(referenceWaterSignal));
%               
%               if correctedWater
%                   
%                   newData(:,1,1,:,1,1,1,1,2,1,1,indexAquisitionsLoop+indexWaterLoop*everyHowManyAcquisitions)=...
%                   newData(:,1,1,:,1,1,1,1,2,1,1,indexAquisitionsLoop+indexWaterLoop*everyHowManyAcquisitions)...
%                   .*exp(-1j*angle(referenceWaterSignal));
%               
%               end
%                   
%                   
%               
%            end
%       end
%       
%       
%  else
%       
%       warning('No ref scan can be found. Only one mix is present in the data!');
%       newData = newData.*exp(-1i*angle(newData));
%      
%       
%  end
%       this.Data{1}= newData;


%%
%{
OLD CODE for 2D JPRESS
function this = EddyCurrentCorrection (this, waterReferenceData)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



if this.Parameter.ReconFlags.isEddyCurrentCorrected
    error('The data is already EddyCurrentCorrected')
end

 if (~this.Parameter.ReconFlags.isEddyCurrentCorrected && this.Parameter.ReconFlags.isDataPhased)
    warning('data was already phased in the post-processing. eddy current correction may spoil the final reconstructed signal');
 end
 
 
       
 
       howManyAveragedWaterSignals = this.Parameter.EddyCurrSettings.howManyAveragedWaterSignals;
       everyHowManyAcquisitions    = this.Parameter.EddyCurrSettings.everyHowManyAcquisitions;
       correctedWater              = this.Parameter.EddyCurrSettings.correctedWater;
       numberOfWaterSignals        = this.Parameter.EddyCurrSettings.numberOfWaterSignals;
       
       nCoils                      = size(this.Data{1},this.coil_dim);
       nDyns                       = size(this.Data{1},this.dyn_dim);
       nMixes                      = size(this.Data{1},this.mix_dim);
       nAverages                   = size(this.Data{1},this.meas_dim);
       newData                     = this.Data{1};
               
       
       waterSignalsSpacing        = numberOfWaterSignals/howManyAveragedWaterSignals;
       aquisitionsSignalsSpacing  = nAverages/everyHowManyAcquisitions;
       
       
if (size(this.Data{1},this.mix_dim)==2 )       
       
      if mod(numberOfWaterSignals,howManyAveragedWaterSignals)
         error('Number of water signals should be multiple of averaged water signals');
      end
      
      if mod(nAverages,everyHowManyAcquisitions)
         error('Number of averages should be everyHowManyAquisitions');
      end
      
      if waterSignalsSpacing ~=aquisitionsSignalsSpacing
         error('The spacing between water signals and FID signals should be equal');
      end
      phasesToPlot = zeros(8192,waterSignalsSpacing); % changed by Tamas
      for indexWaterLoop= 0:waterSignalsSpacing-1
          
           referenceWaterSignal= sum(newData(:,1,1,:,1,1,1,1,2,1,1,[1:howManyAveragedWaterSignals]+indexWaterLoop*howManyAveragedWaterSignals),this.meas_dim);
            %={'kx','ky','kz','coil','5','6','7','8','mix','dyn',11,'meas')
%          phasesToPlot(:,indexWaterLoop+1) = angle(referenceWaterSignal(:,1,1,1)); % changed by Tamas
          for indexAquisitionsLoop= 1:everyHowManyAcquisitions
          
              newData(:,1,1,:,1,1,1,1,1,1,1,indexAquisitionsLoop+indexWaterLoop*everyHowManyAcquisitions)=...
                  newData(:,1,1,:,1,1,1,1,1,1,1,indexAquisitionsLoop+indexWaterLoop*everyHowManyAcquisitions)...
                  .*exp(-1j*angle(referenceWaterSignal));
              
              if correctedWater
                  
                  newData(:,1,1,:,1,1,1,1,2,1,1,indexAquisitionsLoop+indexWaterLoop*everyHowManyAcquisitions)=...
                  newData(:,1,1,:,1,1,1,1,2,1,1,indexAquisitionsLoop+indexWaterLoop*everyHowManyAcquisitions)...
                  .*exp(-1j*angle(referenceWaterSignal));
              
              end

           end
      end
      
      
else
    % changed by Tamas
    useWaterReference = (waterReferenceData ~= 0);
    zerroFillFactor = 1; %this.Parameter.ZeroFillSettings.ZerroFillFactor;
    if (useWaterReference)
       if (size(waterReferenceData, this.kx_dim) * zerroFillFactor ~= size(newData, this.kx_dim))
           error('Size of water reference data not equal ');
       else
           numberOfTESteps = nAverages;
           %iterate for each TE step   
           for indexWaterLoop= 1:numberOfTESteps
               %extract the water reference corresponding to the current TE step
               referenceWaterSignal = waterReferenceData(:,1,1,:,1,1,1,1,1,indexWaterLoop); % TODO add the extra 2 dimensions up to 12D array if needed
               %zerofill the water reference data if needed
               initialVectorSize        = size(referenceWaterSignal, this.kx_dim);
               newDataSize              = size(referenceWaterSignal);
               newDataSize(this.kx_dim) = newDataSize(this.kx_dim) * zerroFillFactor;
               newReferenceWaterSignal =  zeros(newDataSize);
               newReferenceWaterSignal(1:initialVectorSize,:,:,:) =  referenceWaterSignal(:,:,:,:);
                                  
               %do the actual phase correction for each TE step
               newData(:,1,1,:,1,1,1,1,1,1,1,indexWaterLoop)=...
                       newData(:,1,1,:,1,1,1,1,1,1,1,indexWaterLoop)...
                       .*exp(-1j*angle(newReferenceWaterSignal));                  
           end
       end
    else
        warning('No ref scan can be found. Only one mix is present in the data!'); %original code
        newData = newData.*exp(-1i*angle(newData)); %original code
    end
end
%end changed by Tamas

this.Data{1}= newData;
this.Parameter.ReconFlags.isEddyCurrentCorrected= true;

end
%}