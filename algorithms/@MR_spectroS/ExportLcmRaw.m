function ExportLcmRaw(this, path, name, addSinglet, makeBasisExport, referencePeakPpm, scalingFactorRefPeak, MC_mix)

% Define the fileptath where the .RAW files will be saved
if exist('path','var')
    filepath=path;
else
    filepath=pwd;
end
if exist('name','var')
    filename = name;
else
    filename = '';
end

if exist('makeBasisExport','var')
    makeBasis = makeBasisExport;
else
    makeBasis = 'No';
end

if ~exist('referencePeakPpm','var')
    referencePeakPpm = -4.7;
end

if ~exist('scalingFactorRefPeak','var')
    scalingFactorRefPeak = 0.05;
end

if ~exist('MC_mix','var')
    MC_mix = 1; %1 = normal case, export spectra. 2 = export MC water
end

if exist('addSinglet','var')
    choice = addSinglet;
    if choice == true
        choice = 'Yes';
    else
        if choice == false
            choice ='No';
        end
    end
else
    choice = 'NotDefined';
end

if strcmp(choice,'NotDefined')
    %Question dialog box
    choice = questdlg('Add signlet at 0 PPM ?', ...
        'LCModel Extract', ...
        'Yes','No','No');
end

% Read the data
data             =  this.Data{1};
NMeas            =  size( data, this.meas_dim); % # Averages
%NCha            =  size( data, this.coil_dim); % # Channels
%NRep            =  size( data, this.dyn_dim);  % # Repetitions
NMix            =  size( data, this.mix_dim);  % # Mixes
isAveraged       =  this.Parameter.ReconFlags.isAveraged;
isMovingAveraged =  this.Parameter.ReconFlags.isfMRSMovingAveraged;
isISISAveraged =  this.Parameter.ReconFlags.isISISAveraged;
isCoilsCombined =  this.Parameter.ReconFlags.isCoilsCombined;

dwellTime =  this.Parameter.Headers.DwellTimeSig_ns *1e-6; %ms
scanFrequency = get(this.Parameter.Headers,'ScanFrequency')*1e-6;

%Read the values will be later written in the .RAW namelists
TE=          get(this.Parameter.Headers,'TE_us')/1E3; %in ms

sequenceName = get(this.Parameter.Headers,'SequenceFileName');
if strfind(sequenceName,'semiLASER')
    sequenceName = 'sLASER';
else
    sequenceName = 'STEAM'; %this is not really true
end

PatientName = get(this.Parameter.Headers,'PatientName');
PatientID   = get(this.Parameter.Headers,'PatientID');
PatientID  = num2str(PatientID);
if strfind(PatientID,':') % if the ID is not something usable, replace it
    PatientID = PatientName;
    PatientID  = num2str(PatientID);
end
Sequence    = get(this.Parameter.Headers,'SequenceDescription');

if ~isAveraged && ~isMovingAveraged && ~isISISAveraged
    error('The data has to be averaged!!!')
end

if ~isCoilsCombined
    error('The data has to be combined first!!!')
end

dyn_char=   strcat( '_',num2str(NMeas),'Averages');
if isunix
    slash='/';
else
    slash='\';
end
if strcmp(filename,'')
    filename = strcat(slash,PatientID,'_',Sequence, dyn_char);
end

switch makeBasis
    case 'No'
        metaboliteName = PatientID;
    case 'Yes'
        metaboliteName = 'Leu';
end

dataExport = data(:,1,1,1,1,1,1,1,MC_mix,1,1,1);
brukerFormat = true;
ExportLCModelBasis(dataExport, dwellTime, scanFrequency, filepath, filename, metaboliteName, choice, ...
    brukerFormat, referencePeakPpm, scalingFactorRefPeak, makeBasis, TE, sequenceName)

end

