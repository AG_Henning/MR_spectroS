function [diff_conc, diff_em, diff_gm, diff_pc0, diff_df2, diff_pc12, diff_T2] = compareFitInput_Output(dataExportPath, filenameData, iteration)

saveFigures = false;

fileNameOff = strrep(filenameData,'On', 'Off');
xx = ls([dataExportPath, fileNameOff '*']);

load([dataExportPath, fileNameOff, '.mat'], 'summedFidWithNoise', 'current_em', 'currentConcentrationRm', ...
    'current_pc0', 'current_pc1', 'current_df2', 'current_gm', 'current_em_std_factor', 'conc_std_factor');

fileNameProFit = [filenameData '_profit'];
% close all;
load(strcat(dataExportPath,fileNameProFit, '.mat'),'fitresult');

currentFitresult = fitresult{1,iteration};

% metabolites and results have to be first sorted
metabolites     = {'Asp', 'tCr(CH2)', 'tCr(CH3)', 'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA(CH3)', 'NAA(CH2)', 'NAAG', 'tCho+',  'Scy',    'Tau', 'MMB'};
metabolitesName = {'Asp', 'Cr_CH2',   'Cr',       'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA_ac',   'NAA_as',   'NAAG', 'tCho_P', 'Scyllo', 'Tau', 'Leu'};

active_met_names = currentFitresult.met(currentFitresult.active_mets);
sortedIndeces = zeros(length(metabolitesName),1);
for indexMet = 1: length(metabolitesName)
    indexX = find(strcmp(metabolitesName{indexMet}, active_met_names));
    sortedIndeces(indexX) = indexMet;
end

currentConcentrationRm(end) = currentConcentrationRm(end)*1e-8;
diff_conc = currentFitresult.fitted_values.conc * currentFitresult.sub_mask.conc.full - currentConcentrationRm(sortedIndeces);
diff_conc(currentFitresult.fitted_values.conc==0)=NaN;
diff_em   = currentFitresult.fitted_values.em * currentFitresult.sub_mask.em.full - current_em(sortedIndeces);
diff_gm   = currentFitresult.fitted_values.gm * currentFitresult.sub_mask.gm.full- current_gm;
diff_pc0  = currentFitresult.fitted_values.pc0 * currentFitresult.sub_mask.pc0.full - current_pc0;
diff_df2  = currentFitresult.fitted_values.df2 * currentFitresult.sub_mask.df2.full - current_df2(sortedIndeces);
diff_pc12 = currentFitresult.fitted_values.pc12 * currentFitresult.sub_mask.pc12.full - current_pc1;
diff_T2   = 1./(pi*currentFitresult.fitted_values.em * currentFitresult.sub_mask.em.full) *1e3 - ...
    1./(pi*current_em(sortedIndeces)) * 1e3;

figId2=figure;
set(figId2,...
    'Position',[350 400 890 600],'Color',[1 1 1]);
crlb_print =currentFitresult.crlb;
crlb_print(abs(crlb_print)>=1000) = 999.9999;
dataOfTable={};
columnname =  {'met','conc', '<html>crlb<br />[%]</html>', ...
    '<html>T2<br />[ms]</html>','<html>em<br />[Hz]</html>',...
    '<html>gm<br />[Hz]</html>', '<html>em_g<br />[Hz]</html>', ...
    '<html>pc0<br />[deg]</html>', '<html>df2<br />[Hz]</html>', ...
    '<html>pc12<br />[deg/ppm]</html>'};
columnformat={'char',[],[],[],[],[],[],[],[],[]};
concentrations = zeros(1,currentFitresult.nr_act_mets);

for met_cnt=1:currentFitresult.nr_act_mets
    dataOfTable=[dataOfTable;{...
        active_met_names{met_cnt},...
        num2str(diff_conc(met_cnt),'%6.4f'),...
        num2str(crlb_print(currentFitresult.sub_mask.conc.full(:,met_cnt)'),'%6.4f'),...
        num2str(diff_T2(met_cnt),'%6.4f'),...
        num2str(diff_em(met_cnt),'%6.4f'),...
        num2str(diff_gm(met_cnt),'%6.4f'),...
        num2str(diff_pc0(met_cnt),'%6.4f'),...
        num2str(diff_df2(met_cnt),'%6.4f'),...
        num2str(diff_pc12(met_cnt),'%6.4f')...
        }];
    concentrations(met_cnt) = currentFitresult.fitted_values.conc(currentFitresult.sub_mask.conc.full(:,met_cnt)');
end
tableProfit = uitable(...%'Units','normalized',...%
    'Position',[20 20 850 580],'Data', dataOfTable,...
    'ColumnName', columnname,'ColumnFormat',columnformat,...
    'RowName',[],'ColumnWidth',{65});

if saveFigures
    saveas(figId2, [dataExportPath,fileNameProFit, '_compare_iter', num2str(iteration), '_table.tif']);
    savefig(figId2, [dataExportPath,fileNameProFit, '_compare_iter', num2str(iteration), '_table.fig'],'compact');
end
end
