function doFitting_fMRS_Met_v2()
clear; clc; close all;
%modulus is not needed, was tested but didn't work for fMRS data. No
%gradient modulations, but phase jittering

%check pathName, StimType, BOLDType, MMBType
%check nBlocks in reconstruct!

%for v2 (+Asc, -Glyc, PCh&GPC -> tCh), check MMBType, defaultMMB, currentControlFiles

pathName = 'fMRS data path';    %first version, no modulus correction for gradient sideband removal
% sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName);
StimType = {'StimON', 'StimOFF'};      %'StimON', 'StimOFF', 'RestON', 'RestOFF'
if ~isempty(strfind(StimType{1},'Stim'))
    visualType = 'Stim';
elseif ~isempty(strfind(StimType{1},'Rest'))
    visualType = 'Rest';
else
    error('StimType needs to be Stim or Rest');
end
BOLDTypes = {'_woBOLDCorr128', '_withBOLDCorr128','_woBOLDCorr64', '_woBOLDCorr64#1', '_withBOLDCorr64', '_withBOLDCorr64#1'};  %_woBOLDCorr128, _withBOLDCorr128, _woBOLDCorr64, _woBOLDCorr64#1, _withBOLDCorr64, _withBOLDCorr64#1
% BOLDTypes = {'_woBOLDCorr64#1'};  %_woBOLDCorr128, _withBOLDCorr128, _woBOLDCorr64, _woBOLDCorr64#1, _withBOLDCorr64, _withBOLDCorr64#1
for idxBOLDType = 1:length(BOLDTypes)
    BOLDType = BOLDTypes(idxBOLDType);
orderFMRS = strcat(StimType,BOLDType);%
MMBType = '_MMBSummed_v2';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated
subjects = {'3333';};
% subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
%     '4085';'3333';'4012';'4085';'6524'};
prefixes = {'2020-03-10_';};
% prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
%     '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
%     '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
excludeSubjects = {'5166';'9810';'5269'};
suffixFolder = '_fMRS';
if strcmp(MMBType,'_MMBSummed')
    excludeMet = {'NAA';'GABA';'Gln';'Glyc';'GSH';'mI';'NAAG';'PCh';'GPC';'PE';'Scyllo';'Tau';'Glc';'NAA_NAAG';'Glu_Gln';'Glyc';'mI_Glyc';'PCh'};  %for v1
elseif strcmp(MMBType,'_MMBSummed_v2')
    excludeMet = {'NAA';'Asc';'GABA';'Glc';'Gln';'GSH';'mI';'NAAG';'tCh';'PE';'Scyllo';'Tau';'NAA_NAAG';'Glu_Gln'};  %for v2
end

extensionTable = '.table';
extensionCoord = '.coord';
defaultfMRS = 'StimOFF';
defaultSubject = 'XXXX';
defaultSubjectsPath = 'YYYY';
% defaultMMB = '_MMBSummed';
defaultMMB = '_MMBSummed_v2';
defaultLCModelUser = 'jdorst';

% controlFilesBase = 'fitsettings_fMRS_XXXX_';
controlFilesBase = 'fitsettings_fMRS_v2_XXXX_';
controlFilesBaseSuffix = '.control';
outputFileNameBase = strcat(defaultSubject, '_');

%% file paths setup
controlFilesPathRemote = '/Desktop/1H_fMRS/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/1H_fMRS/Output/';
if ~isempty(strfind(localFilePathBase,'Modulus'))
    LCModelOutputPath = [localFilePathBase, 'Output_v2_MMBSummed_Modulus/'];
    modulus = true;
elseif ~isempty(strfind(localFilePathBase,'06-38ppm'))
    LCModelOutputPath = [localFilePathBase, 'Output_v2_MMBSummed_06-38ppm/'];
    modulus = false;
else
    LCModelOutputPath = [localFilePathBase, 'Output_v2_MMBSummed/'];
    modulus = false;
end
defaultControlFile = strcat(controlFilesBase, defaultfMRS, controlFilesBaseSuffix);

subjectsPath = strcat(prefixes, subjects, suffixFolder);

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath, '/');
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath, '/');
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath, '/');
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath, '/');
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath, '/');


%% process fMRS data and save .RAW file
dataPath = strcat(localFilePathBase,subjectsPath, '/');
if ~isempty(strfind(BOLDType{1},'wo'))
    for i = 1:length(dataPath)
        currentFolder = num2str(cell2mat(dataPath(i)));
        cd (currentFolder);
        subFolderName = dir(dataPath{i});
        if ~isempty(strfind(StimType{1},'Stim'))
            dataFile = dir('*Stim*.dat');
        elseif ~isempty(strfind(StimType{1},'Rest'))
            dataFile = dir('*Rest*.dat');
        else
            error('StimType needs to be Stim or Rest');
        end
        if ~isempty(strfind(BOLDType{1},'128'))
            nBlocks = 5;
        else
            nBlocks = 10;
        end
        PCSteps = 16;
        reconstruct_1H_fMRS(currentFolder,dataFile.name,1,1,nBlocks,false,PCSteps,[],1,visualType,BOLDType{1}, modulus); %currentFolder, fid_id, isMC, isInVivo, nBlocks, nOnAverages, PCSteps, weights, saveResults
        close all; cd ..;
    end
end
% % % % end
%% basic configurations
LCModelOutputFiles = strcat(outputFileNameBase, orderFMRS, MMBType);
LCModelOutputFileswoBaseline = strcat(outputFileNameBase, orderFMRS, MMBType, '_woBaseline');
LCModelControlFiles = strcat(controlFilesBase, orderFMRS, MMBType, controlFilesBaseSuffix);
LCModelControlFileswoBaseline = strcat(controlFilesBase, orderFMRS, MMBType, '_woBaseline', controlFilesBaseSuffix);
defaultWaterRef = strcat(outputFileNameBase, 'WRef');

%
numberOfFMRS = length(orderFMRS);
numberOfSubjects = length(subjects);

%% do the LCModel fitting
% % % LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

% % do the actual LCModel fitting
parfor indexCurrentSubject = 1:numberOfSubjects
    %% do the LCModel fitting
    LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
    
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    
%     currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
    currentControlFiles = strrep(LCModelControlFiles, strcat('v2_',defaultSubject), subjects{indexCurrentSubject});
    currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
    waterRefFileName = strrep(defaultWaterRef, defaultSubject, subjects{indexCurrentSubject});
    
    %% create the control file series
    for indexFMRS = 1:length(currentControlFiles)
        currentOutputFile = currentOutputFiles{indexFMRS};
        currentFMRS = orderFMRS{indexFMRS};
        currentLCModelOutputFile = LCModelOutputFiles{indexFMRS};
        createLCModelConfigfMRS('fMRS', controlFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, defaultControlFile, ...
            currentFMRS, defaultfMRS, defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFile, currentLCModelOutputFile,...
            waterRefFileName, defaultWaterRef, subjectsPath{indexCurrentSubject}, defaultSubjectsPath, subjects{indexCurrentSubject},...
            defaultSubject, MMBType, defaultMMB);
    end
    
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
end
if exist('excludeSubjects')
    createConcTable(numberOfSubjects,orderFMRS,LCModelOutputFiles,LCModelOutputPath,defaultSubject,prefixes,subjects,subjectsPath,...
        visualType,BOLDType,MMBType,excludeSubjects,excludeMet)
else
    createConcTable(numberOfSubjects,orderFMRS,LCModelOutputFiles,LCModelOutputPath,defaultSubject,prefixes,subjects,subjectsPath,...
        visualType,BOLDType,MMBType,'',excludeMet)
end

%% subtract baseline from difference spectrum and fit again
% only useful for MMBSummed
if strcmp(MMBType(1:10),'_MMBSummed')
    %     LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
    parfor indexCurrentSubject = 1:numberOfSubjects
        %% do the LCModel fitting
                LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
        
                LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
                    subjectsLCModelOutputPath{indexCurrentSubject});
        
        currentControlFileswoBaseline = strrep(LCModelControlFileswoBaseline, defaultSubject, subjects{indexCurrentSubject});
        currentControlFileswoBaseline = strrep(currentControlFileswoBaseline,'fMRS_v2_','fMRS_');
        currentControlFileswoBaseline = strrep(currentControlFileswoBaseline,'#1','_1');
        if ~isempty(strfind(MMBType,'_v2'))
            currentDataFiles = strrep(strcat(outputFileNameBase, orderFMRS, '_v2'), defaultSubject, subjects{indexCurrentSubject});
        else
            currentDataFiles = strrep(strcat(outputFileNameBase, visualType, BOLDType), defaultSubject, subjects{indexCurrentSubject});
        end
        
        %         currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
        waterRefFileName = strrep(defaultWaterRef, defaultSubject, subjects{indexCurrentSubject});
        currentDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject}, '/');
        for indexFMRS = 1:length(currentDataFiles)
            %         currentOutputFileswoBaseline = currentOutputFileswoBaseline{indexFMRS};
            currentFMRS = orderFMRS{indexFMRS};
            currentFMRS = strrep(currentFMRS,'#1','_1');
            currentLCModelOutputFile = LCModelOutputFiles{indexFMRS};
            currentLCModelOutputFile = strrep(currentLCModelOutputFile,'#1','_1');
            %check save option for .mat file in subtractBaseline_fMRS
            currentOutputFileswoBaseline = strrep(LCModelOutputFileswoBaseline, defaultSubject, subjects{indexCurrentSubject});
            subtractBaseline_fMRS(currentDataPath,subjectsLCModelOutputPath{indexCurrentSubject},currentDataFiles{indexFMRS},currentOutputFileswoBaseline{indexFMRS},'NAA')
            currentOutputFileswoBaseline = strrep(currentOutputFileswoBaseline,'#1','_1');
            createLCModelConfigfMRS('fMRS', controlFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, defaultControlFile, ...
                currentFMRS, defaultfMRS, defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFileswoBaseline{indexFMRS}, currentLCModelOutputFile,...
                waterRefFileName, defaultWaterRef, subjectsPath{indexCurrentSubject}, defaultSubjectsPath, subjects{indexCurrentSubject},...
                defaultSubject, MMBType, defaultMMB,'');
        end
        fittingLCModel(LCModelCallerInstance, currentControlFileswoBaseline, currentOutputFileswoBaseline, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
            subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
        
    end
    if exist('excludeSubjects')
        createConcTable(numberOfSubjects,orderFMRS,LCModelOutputFiles,LCModelOutputPath,defaultSubject,prefixes,subjects,subjectsPath,...
            visualType,BOLDType,MMBType,excludeSubjects,excludeMet,1)
    else
        createConcTable(numberOfSubjects,orderFMRS,LCModelOutputFiles,LCModelOutputPath,defaultSubject,prefixes,subjects,subjectsPath,...
            visualType,BOLDType,MMBType,'',excludeMet,1)
    end
end
end
end


%% create concentration table
function createConcTable(numberOfSubjects,orderFMRS,LCModelOutputFiles,LCModelOutputPath,defaultSubject,prefixes,subjects,subjectsPath,...
        visualType,BOLDType,MMBType,excludeSubjects,excludeMet,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
    LCModelOutputFiles = strrep(LCModelOutputFiles,'#1','_1');
else
    woBaseline = '';
end
tableConcentrations = [];
isFirstIter = 1;
s = 1;
for indexCurrentSubject = 1:numberOfSubjects
    for indexorderFMRS = 1:length(orderFMRS)
        % retrieve parameters
        currentOutputFiles = strrep(strcat(LCModelOutputFiles,woBaseline), defaultSubject, subjects{indexCurrentSubject});
        currentPath = strcat(LCModelOutputPath,subjectsPath{indexCurrentSubject},'/',currentOutputFiles,'.table');
        [c1 c2 c3 c4] = textread(currentPath{indexorderFMRS},'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
            tableConcentrations{1,s} = 'water concentration';
        end
        
        % add ID and Water conc to table
        tableConcentrations{indexCurrentSubject*2+indexorderFMRS-1,1} = strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject}, '_', orderFMRS{indexorderFMRS});
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        tableConcentrations{indexCurrentSubject*2+indexorderFMRS-1,end} = wconc_LCModel;
        % add metabolites to table
        s = 2;
        for j=start:finish
            %add quantified metabolite
            tableConcentrations{indexCurrentSubject*2+indexorderFMRS-1,s} = str2num( c1{j});
            %add CRLB
            tableConcentrations{indexCurrentSubject*2+indexorderFMRS-1,s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end
end

% save table to file
ExpName = strcat('LCModelFit_', num2str(visualType), BOLDType{1}, MMBType, woBaseline);
ExpName = strrep(ExpName,'#1','_1');
exportFileXlsx = [LCModelOutputPath ,strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)

%% calculate percent changes Stim OFF vs Stim ON, save result tables
load(exportFileMat)
[~, rawFile, ~] = fileparts(exportFileMat);
LCModelFitTable = eval(rawFile);
metaboliteNames = LCModelFitTable(1,4:2:end-2);
metaboliteNames = strrep(metaboliteNames,'+','_');

if exist('excludeSubjects', 'var') & ~isempty(excludeSubjects)
    [n,~] = size(LCModelFitTable);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 2:n
            xx = find(~cellfun(@isempty,strfind(LCModelFitTable(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    LCModelFitTable([excludeIdx],:) = [];
end

ON = cell2mat(LCModelFitTable(2:2:end,4:2:end-2));
OFF = cell2mat(LCModelFitTable(3:2:end,4:2:end-2));
PercDiff = 100./OFF.*ON-100;
PercDiffMean = mean(PercDiff);
PercDiffStd = std(PercDiff);

%% two-sided Wilcoxon signed rank test
for indexOfMetabolite = 1:length(metaboliteNames)
    if isempty(find(~isnan(PercDiff(:,indexOfMetabolite))))
        p_Wilc(indexOfMetabolite) = NaN;
        h_Wilc(indexOfMetabolite) = 0;
    else
        [p_Wilc(indexOfMetabolite),h_Wilc(indexOfMetabolite)] = signrank(PercDiff(:,indexOfMetabolite));
    end
end

%check histogram of p-values
edges = linspace(0,1,length(p_Wilc));
histogram(p_Wilc,edges)

%Benjamini-Hochberg correction for multiple testing
[h_BH_Wilc, crit_p_Wilc, adj_ci_cvrg_Wilc, adj_p_Wilc]=fdr_bh(p_Wilc,0.05,'pdep','yes');
%h=1: reject H0, h=0: H0 not rejected
%crit_p: max p-value that is significant
%adj_ci_cvrg: BH adjusted confidence interval coverage
%adj_p: N pj / j, all adjusted p-values less than or equal to q are significant


if exist('excludeMet')
    [~,n] = size(metaboliteNames);
    m = length(excludeMet);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = strcmp(metaboliteNames(1,i),excludeMet{j});
%             xx = find(~cellfun(@isempty,strcmp(metaboliteNames(1,i),excludeMet{j})));
%             if ~isempty(xx)
            if xx
                excludeMetIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    pExclMet_Wilc = p_Wilc;
    pExclMet_Wilc(excludeMetIdx) = [];
    [h_BH_ExclMet_Wilc, crit_p_ExclMet_Wilc, adj_ci_cvrg_ExclMet_Wilc, adj_p_ExclMet_Wilc]=fdr_bh(pExclMet_Wilc,0.05,'pdep','yes');
    temph = ones(1,length(h_Wilc))*NaN; temp_p = ones(1,length(p_Wilc))*NaN; tempidx = [0 sort(excludeMetIdx)]; nstep = 1;
    for j = 1:length(tempidx)-1
        temp_p((tempidx(j)+1):(tempidx(j+1)-1)) = adj_p_ExclMet_Wilc((tempidx(j)+2-nstep):(tempidx(j+1)-nstep));
        temph((tempidx(j)+1):(tempidx(j+1)-1)) = h_BH_ExclMet_Wilc((tempidx(j)+2-nstep):(tempidx(j+1)-nstep));
        nstep = nstep + 1;
    end
    h_BH_ExclMet_Wilc = temph;
    adj_p_ExclMet_Wilc = temp_p;
end

%% paired two-tailed t-test (data need to be normally distributed, which they aren't here! -> use nonparametric Wilcoxon)
for indexOfMetabolite = 1:length(metaboliteNames)
    if isempty(find(~isnan(PercDiff(:,indexOfMetabolite))))
        h_ttest(indexOfMetabolite) = NaN;
        p_ttest(indexOfMetabolite) = 0;
    else
        [h_ttest(indexOfMetabolite),p_ttest(indexOfMetabolite)] = ttest(PercDiff(:,indexOfMetabolite));
    end
end

%check histogram of p-values
edges = linspace(0,1,length(p_ttest));
histogram(p_ttest,edges)

%Benjamini-Hochberg correction for multiple testing
[h_BH_ttest, crit_p_ttest, adj_ci_cvrg_ttest, adj_p_ttest]=fdr_bh(p_ttest,0.05,'pdep','yes');
%h=1: reject H0, h=0: H0 not rejected
%crit_p: max p-value that is significant
%adj_ci_cvrg: BH adjusted confidence interval coverage
%adj_p: N pj / j, all adjusted p-values less than or equal to q are significant

if exist('excludeMet', 'var')
    [~,n] = size(metaboliteNames);
    m = length(excludeMet);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = strcmp(metaboliteNames(1,i),excludeMet{j});
%             xx = find(~cellfun(@isempty,strcmp(metaboliteNames(1,i),excludeMet{j})));
%             if ~isempty(xx)
            if xx
                excludeMetIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    pExclMet_ttest = p_ttest;
    pExclMet_ttest(excludeMetIdx) = [];
    [h_BH_ExclMet_ttest, crit_p_ExclMet_ttest, adj_ci_cvrg_ExclMet_ttest, adj_p_ExclMet_ttest]=fdr_bh(pExclMet_ttest,0.05,'pdep','yes');
    temph = ones(1,length(h_ttest))*NaN; temp_p = ones(1,length(p_ttest))*NaN; tempidx = [0 sort(excludeMetIdx)]; nstep = 1;
    for j = 1:length(tempidx)-1
        temp_p((tempidx(j)+1):(tempidx(j+1)-1)) = adj_p_ExclMet_ttest((tempidx(j)+2-nstep):(tempidx(j+1)-nstep));
        temph((tempidx(j)+1):(tempidx(j+1)-1)) = h_BH_ExclMet_ttest((tempidx(j)+2-nstep):(tempidx(j+1)-nstep));
        nstep = nstep + 1;
    end
    h_BH_ExclMet_ttest = temph;
    adj_p_ExclMet_ttest = temp_p;
end
if exist('excludeSubjects', 'var') & ~isempty(excludeSubjects)
    m = length(excludeSubjects);
    for k = 1:m
        [~, idx(k)] = ismember(excludeSubjects(k), subjects);
    end
    prefixes([idx],:) = [];
    subjects([idx],:) = [];
end
rowNames = strcat(prefixes, subjects);
if exist('excludeMet', 'var')
    rowNames = [rowNames; 'Mean'; 'Std'; 'p-Value_Wilc'; 'h-Value_Wilc'; 'p_BH_Wilc'; 'h_BH_Wilc'; 'p_BH_Excl_Wilc'; 'h_BH_Excl_Wilc'; 'p-Value_ttest'; 'h-Value_ttest'; 'p_BH_ttest'; 'h_BH_ttest'; 'p_BH_Excl_ttest'; 'h_BH_Excl_ttest'];
else
    rowNames = [rowNames; 'Mean'; 'Std'; 'p-Value_Wilc'; 'h-Value_Wilc'; 'p_BH_Wilc'; 'h_BH_Wilc'; 'p-Value_ttest'; 'h-Value_ttest'; 'p_BH_ttest'; 'h_BH_ttest'];
end
if exist('excludeSubjects', 'var') & ~isempty(excludeSubjects)
    ExpNameDiff = strcat('ConcDiff_', num2str(visualType), BOLDType, MMBType, '_Excluded_woBaseline');
else
    ExpNameDiff = strcat('ConcDiff_', num2str(visualType), BOLDType, MMBType, '_woBaseline');
end
ExpNameDiff = ExpNameDiff{1};   %convert 1x1cell to string
ExpNameDiff = strrep(ExpNameDiff,'#1','_1');
if exist ('excludeMet')
    ConcentrationsDiff_xx = array2table([PercDiff; PercDiffMean; PercDiffStd; p_Wilc; h_Wilc; adj_p_Wilc; h_BH_Wilc; adj_p_ExclMet_Wilc; h_BH_ExclMet_Wilc; p_ttest; h_ttest; adj_p_ttest; h_BH_ttest; adj_p_ExclMet_ttest; h_BH_ExclMet_ttest],'rowNames',rowNames,'VariableNames',metaboliteNames);
else
    ConcentrationsDiff_xx = array2table([PercDiff; PercDiffMean; PercDiffStd; p_Wilc; h_Wilc; adj_p_Wilc; h_BH_Wilc; p_ttest; h_ttest; adj_p_ttest; h_BH_ttest],'rowNames',rowNames,'VariableNames',metaboliteNames);
end

% ConcentrationsDiff_xx = array2table([PercDiff; PercDiffMean; PercDiffStd; p; h; adj_p; h_BH],'rowNames',rowNames,'VariableNames',metaboliteNames);
eval([ExpNameDiff '=ConcentrationsDiff_xx']);
exportFileMatDiff = [LCModelOutputPath ,strcat(ExpNameDiff,'.mat')];
save(exportFileMatDiff,ExpNameDiff)

close all
end

