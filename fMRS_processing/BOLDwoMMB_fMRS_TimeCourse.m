function BOLDwoMMB_fMRS_TimeCourse()
clear; clc; close all;
woBaseline = true;
allVisTypes = {'Stim', 'Rest'};      %'Stim', 'Rest'

for i = 1:length(allVisTypes)
    pathName = 'fMRS TimeCourse data path';
    sampleCriteria = {'Output', 'LCModelConfig'};
    [localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
    visualType = allVisTypes{i};      %'Stim', 'Rest'
    BOLDType = '_woBOLDCorr';  %only _woBOLDCorr reasonable since block wise fitting for metabolite time courses
    MMBType = '_MMBSummed_v2';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated
    %     nBlocks = 5;
    nBlocks = 10;
    
    BlockNo = cell(1,nBlocks);
    for k = 1:nBlocks
        BlockNo{k} = strcat('_Block', num2str(k));
    end
    
    %     subjects = {'2823';};
    subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
        '4085';'3333';'4012';'4085';'6524'};
    %     prefixes = {'2020-02-18_';};
    prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
        '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
        '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
    suffixFolder = '_fMRS';
    excludeSubjects = {'5166';'9810';'5269'};
    
    defaultSubject = 'XXXX';
    outputFileNameBase = strcat(defaultSubject, '_');
    
    %% file paths setup
    if ~isempty(strfind(MMBType,'_v2'))
        LCModelOutputPath = [localFilePathBase, 'Output_v2_MMBSummed/'];
    else
        LCModelOutputPath = [localFilePathBase, 'Output/'];
    end
    subjectsPath = strcat(prefixes, subjects, suffixFolder);
    subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath, '/');
    preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath, '/');
    OutputFilesPathLocal = strcat(LCModelOutputPath, 'BOLD/');
    %% basic configurations
    LCModelOutputFileswoBaseline = strcat(outputFileNameBase, visualType, BOLDType, MMBType, BlockNo, '_woBaseline');
    numberOfSubjects = length(subjects);
    
%     %% subtract LCModel baseline and Macromolecular Baseline from preprocessed .mat file of individual subjects
%     for indexCurrentSubject = 1:numberOfSubjects
%         if ~isempty(strfind(MMBType,'_v2'))
%             currentDataFiles = strrep(strcat(outputFileNameBase, visualType, BOLDType, '_v2', BlockNo), defaultSubject, subjects{indexCurrentSubject});
%         else
%             currentDataFiles = strrep(strcat(outputFileNameBase, visualType, BOLDType, BlockNo), defaultSubject, subjects{indexCurrentSubject});
%         end
%         currentOutputFileswoBaseline = strrep(LCModelOutputFileswoBaseline, defaultSubject, subjects{indexCurrentSubject});
%         currentDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject}, '/');
%         for nBl = 1:nBlocks
%             %check save option for .mat file in subtractLCMB_MMB_fMRS
%             subtractLCMB_MMB_fMRS(currentDataPath,subjectsLCModelOutputPath{indexCurrentSubject},currentDataFiles{nBl},currentOutputFileswoBaseline{nBl},'NAA')
%         end
%     end
%     %% calculate BOLD effect for individual subjects, mean, std
%     if ~isempty(strfind(MMBType,'v2'))
%         BOLD_MMBType = strcat(BOLDType,'_v2');
%     else
%         BOLD_MMBType = BOLDType;
%     end
%     %for Water
%     PeakRange = [4 5];  %ppm where to find the peak
%     metabType = 'Water';
%     if exist('excludeSubjects')
%         CalcBOLDEffect(subjects,BlockNo,visualType,BOLD_MMBType,preprocessedFilesPathLocal,...
%             OutputFilesPathLocal,nBlocks,PeakRange,metabType,excludeSubjects)
%     else
%         CalcBOLDEffect(subjects,BlockNo,visualType,BOLD_MMBType,preprocessedFilesPathLocal,...
%             OutputFilesPathLocal,nBlocks,PeakRange,metabType)
%     end
%     %for NAA (2.008 ppm)
%     PeakRange = [1.8 2.3];  %ppm where to find the peak
%     metabType = 'NAA';
%     if exist('excludeSubjects')
%         CalcBOLDEffect(subjects,BlockNo,visualType,BOLD_MMBType,preprocessedFilesPathLocal,...
%             OutputFilesPathLocal,nBlocks,PeakRange,metabType,excludeSubjects)
%     else
%         CalcBOLDEffect(subjects,BlockNo,visualType,BOLD_MMBType,preprocessedFilesPathLocal,...
%             OutputFilesPathLocal,nBlocks,PeakRange,metabType)
%     end
%     %for Cr (methyl signal of tCr: 3.027ppm)
%     PeakRange = [2.9 3.15];  %ppm where to find the peak
%     metabType = 'Cr';
%     if exist('excludeSubjects')
%         CalcBOLDEffect(subjects,BlockNo,visualType,BOLD_MMBType,preprocessedFilesPathLocal,...
%             OutputFilesPathLocal,nBlocks,PeakRange,metabType,excludeSubjects)
%     else
%         CalcBOLDEffect(subjects,BlockNo,visualType,BOLD_MMBType,preprocessedFilesPathLocal,...
%             OutputFilesPathLocal,nBlocks,PeakRange,metabType)
%     end
    
    
    %% sum spectra of of all volunteers, weighted with water fit (need to run doFitting_fMRS_Water.m first) (LCModel baseline & MMB subtracted)
    WaterReferenceTable = (readtable(strcat(OutputFilesPathLocal,'WaterReference_Fit.xlsx')));
    WaterReferenceID = table2cell(WaterReferenceTable(2:end,1));
    WaterReference = str2double(table2array(WaterReferenceTable(2:end,2:end)));
    fMRSDataScaled = zeros(8192,numberOfSubjects,2,length(BlockNo));    %samples, subjects, data/water
    if ~isempty(strfind(MMBType,'v2'))
        BOLD_MMBType = strcat(BOLDType,'_v2');
    else
        BOLD_MMBType = BOLDType;
    end
    if exist('excludeSubjects')
        n = length(subjects);
        m = length(excludeSubjects);
        nstep = 1;
        for j = 1:m
            for i = 1:n
                xx = find(~cellfun(@isempty,strfind(subjects(i,1),excludeSubjects{j})));
                if ~isempty(xx)
                    excludeIdx(nstep) = i;
                    nstep = nstep + 1;
                end
            end
        end
        subjects([excludeIdx],:) = [];
        prefixes([excludeIdx],:) = [];
    end
    numberOfSubjects = length(subjects);
    defaultSubject = 'XXXX';
    subjectsPath = strcat(prefixes, subjects, suffixFolder);
    preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath, '/');
    OutputFilesPathLocal = strcat(localFilePathBase, 'Output_v2_MMBSummed/BOLD/');
    outputFileNameBase = strcat(defaultSubject, '_', num2str(visualType), BOLD_MMBType);
    extensionMat   = '.mat';
    for indexCurrentSubject = 1:numberOfSubjects
        for indexBlock = 1:length(BlockNo)
            idx = find(ismember(WaterReferenceID, strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject})));
            currentScalingFactor = WaterReference(idx,1);
            currentfMRSDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject});
            load(strrep(strcat(currentfMRSDataPath, '\',outputFileNameBase,BlockNo{indexBlock},'_woMMB.mat'),defaultSubject,subjects{indexCurrentSubject}));
            fMRSData_woMMB.Parameter.FreqAlignFreqDomainSettings.peaksInPpm = 2.008;    %NAA
            fMRSData_woMMB = fMRSData_woMMB.FrequencyAlignFreqDomain;
            currentfMRSData = squeeze(fMRSData_woMMB.Data{1});
            currentfMRSDataScaled = currentfMRSData./currentScalingFactor;
            fMRSDataScaled(:,indexCurrentSubject,:,indexBlock) = currentfMRSDataScaled;
        end
    end
    cd ('D:\PAPER\1H_fMRS\Data_TimeCourse\SummedData')
    fMRSData_Summed_Path = strcat(localFilePathBase,'SummedData\');
    fMRSData_woMMB.Parameter.Filepath = fMRSData_Summed_Path;
    fMRSData_Summed = squeeze(sum(fMRSDataScaled,2));
    fMRSData_woMMB.Parameter.Filename = 'fMRSData_Summed_Block1_v2_MMBSummed_Excluded_woMMB';
    fMRSData_Summed_Block1 = fMRSData_Summed(:,:,1); fMRSData_Summed_Block1 = permute(fMRSData_Summed_Block1,[1 3 4 5 6 7 8 9 2]);
    fMRSData_woMMB.Data{1} = fMRSData_Summed_Block1; save(strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block1_woMMB'),'fMRSData_woMMB'); fMRSData_woMMB.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block1_woMMB'),'Yes', 'Yes');
    fMRSData_woMMB.Parameter.Filename = 'fMRSData_Summed_Block2_v2_MMBSummed_Excluded_woMMB';
    fMRSData_Summed_Block2 = fMRSData_Summed(:,:,2); fMRSData_Summed_Block2 = permute(fMRSData_Summed_Block2,[1 3 4 5 6 7 8 9 2]);
    fMRSData_woMMB.Data{1} = fMRSData_Summed_Block2; save(strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block2_woMMB'),'fMRSData_woMMB'); fMRSData_woMMB.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block2_woMM'),'Yes', 'Yes');
    fMRSData_woMMB.Parameter.Filename = 'fMRSData_Summed_Block3_v2_MMBSummed_Excluded_woMMB';
    fMRSData_Summed_Block3 = fMRSData_Summed(:,:,3); fMRSData_Summed_Block3 = permute(fMRSData_Summed_Block3,[1 3 4 5 6 7 8 9 2]);
    fMRSData_woMMB.Data{1} = fMRSData_Summed_Block3; save(strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block3_woMMB'),'fMRSData_woMMB'); fMRSData_woMMB.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block3_woMM'),'Yes', 'Yes');
    fMRSData_woMMB.Parameter.Filename = 'fMRSData_Summed_Block4_v2_MMBSummed_Excluded_woMMB';
    fMRSData_Summed_Block4 = fMRSData_Summed(:,:,4); fMRSData_Summed_Block4 = permute(fMRSData_Summed_Block4,[1 3 4 5 6 7 8 9 2]);
    fMRSData_woMMB.Data{1} = fMRSData_Summed_Block4; save(strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block4_woMMB'),'fMRSData_woMMB'); fMRSData_woMMB.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block4_woMM'),'Yes', 'Yes');
    fMRSData_woMMB.Parameter.Filename = 'fMRSData_Summed_Block5_v2_MMBSummed_Excluded_woMMB';
    fMRSData_Summed_Block5 = fMRSData_Summed(:,:,5); fMRSData_Summed_Block5 = permute(fMRSData_Summed_Block5,[1 3 4 5 6 7 8 9 2]);
    fMRSData_woMMB.Data{1} = fMRSData_Summed_Block5; save(strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block5_woMMB'),'fMRSData_woMMB'); fMRSData_woMMB.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block5_woMM'),'Yes', 'Yes');
    fMRSData_woMMB.Parameter.Filename = 'fMRSData_Summed_Block6_v2_MMBSummed_Excluded_woMMB';
    fMRSData_Summed_Block6 = fMRSData_Summed(:,:,6); fMRSData_Summed_Block6 = permute(fMRSData_Summed_Block6,[1 3 4 5 6 7 8 9 2]);
    fMRSData_woMMB.Data{1} = fMRSData_Summed_Block6; save(strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block6_woMMB'),'fMRSData_woMMB'); fMRSData_woMMB.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block6_woMM'),'Yes', 'Yes');
    fMRSData_woMMB.Parameter.Filename = 'fMRSData_Summed_Block7_v2_MMBSummed_Excluded_woMMB';
    fMRSData_Summed_Block7 = fMRSData_Summed(:,:,7); fMRSData_Summed_Block7 = permute(fMRSData_Summed_Block7,[1 3 4 5 6 7 8 9 2]);
    fMRSData_woMMB.Data{1} = fMRSData_Summed_Block7; save(strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block7_woMMB'),'fMRSData_woMMB'); fMRSData_woMMB.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block7_woMM'),'Yes', 'Yes');
    fMRSData_woMMB.Parameter.Filename = 'fMRSData_Summed_Block8_v2_MMBSummed_Excluded_woMMB';
    fMRSData_Summed_Block8 = fMRSData_Summed(:,:,8); fMRSData_Summed_Block8 = permute(fMRSData_Summed_Block8,[1 3 4 5 6 7 8 9 2]);
    fMRSData_woMMB.Data{1} = fMRSData_Summed_Block8; save(strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block8_woMMB'),'fMRSData_woMMB'); fMRSData_woMMB.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block8_woMM'),'Yes', 'Yes');
    fMRSData_woMMB.Parameter.Filename = 'fMRSData_Summed_Block9_v2_MMBSummed_Excluded_woMMB';
    fMRSData_Summed_Block9 = fMRSData_Summed(:,:,9); fMRSData_Summed_Block9 = permute(fMRSData_Summed_Block9,[1 3 4 5 6 7 8 9 2]);
    fMRSData_woMMB.Data{1} = fMRSData_Summed_Block9; save(strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block9_woMMB'),'fMRSData_woMMB'); fMRSData_woMMB.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block9_woMM'),'Yes', 'Yes');
    fMRSData_woMMB.Parameter.Filename = 'fMRSData_Summed_Block10_v2_MMBSummed_Excluded_woMMB';
    fMRSData_Summed_Block10 = fMRSData_Summed(:,:,10); fMRSData_Summed_Block10 = permute(fMRSData_Summed_Block10,[1 3 4 5 6 7 8 9 2]);
    fMRSData_woMMB.Data{1} = fMRSData_Summed_Block10; save(strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block10_woMMB'),'fMRSData_woMMB'); fMRSData_woMMB.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLD_MMBType,'_Block10_woMM'),'Yes', 'Yes');
    
    %% BOLD FWHM / Amplitude
% for Water (not useful w/o baseline, since it doesn't have a baseline)
% BUT: do it because better frequency aligned
PeakRange = [4 5];  %ppm where to find the peak
metabType = 'Water';
if exist('excludeSubjects')
    CalcBOLDEffect_Summed(BlockNo,visualType,BOLDType,localFilePathBase,...
        OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
else
    CalcBOLDEffect_Summed(BlockNo,visualType,BOLDType,localFilePathBase,...
        OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
end

%for NAA (2.008 ppm)
PeakRange = [1.8 2.3];  %ppm where to find the peak
metabType = 'NAA';
if exist('excludeSubjects')
    CalcBOLDEffect_Summed(BlockNo,visualType,BOLDType,localFilePathBase,...
        OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
else
    CalcBOLDEffect_Summed(BlockNo,visualType,BOLDType,localFilePathBase,...
        OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
end

%for Cr (methyl signal of tCr: 3.027ppm)
PeakRange = [2.9 3.15];  %ppm where to find the peak
metabType = 'Cr';
if exist('excludeSubjects')
    CalcBOLDEffect_Summed(BlockNo,visualType,BOLDType,localFilePathBase,...
        OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
else
    CalcBOLDEffect_Summed(BlockNo,visualType,BOLDType,localFilePathBase,...
        OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
end
end
end



function CalcBOLDEffect_Summed(BlockNo,visualType,BOLDType,preprocessedFilesPathLocal,...
    OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woMMB';
    fitVersion = '_v2';
else
    woBaseline = '';
    fitVersion = '';
end
SelProcValues = SelectedProcessingValues();
fwhmArray = zeros(1,length(BlockNo));
amplitudeArray = zeros(1,length(BlockNo));
for nBl = 1:length(BlockNo)
    impName = strcat('fMRSData_Summed_',visualType,BOLDType,fitVersion, BlockNo{nBl},woBaseline);
    impFileMat = strcat(preprocessedFilesPathLocal,'SummedData\',impName,'.mat');
    dataPerBlock = load(impFileMat);
    fields = fieldnames(dataPerBlock);
    dataPerBlock = dataPerBlock.(fields{1});
    SelProcValues.PeakRange = PeakRange;  %ppm
    currentValue = SelProcValues.PeakRange;
    currentValue = convertPPMtoIndex(dataPerBlock, currentValue,1);
    currentValue =[find(currentValue==1,1,'first') find(currentValue==1,1,'last')];
    SelProcValues.PeakRange = [currentValue(1) currentValue(2)];
    bandwidth = dataPerBlock.Parameter.Headers.Bandwidth_Hz;
    datafid = squeeze(dataPerBlock.Data{1});
    peakRange = get(SelProcValues, 'PeakRange');
    
    if strcmp(metabType(1:2),'Wa')
        datafid = datafid(:,2);
        dataSpecReal = real(fftshift(fft(datafid)));
    else
        datafid = datafid(:,1);
        dataSpecReal = real(fftshift(fft(datafid)));
    end
    
    fwhmArray(1,nBl) = linewidth(datafid, SelProcValues, bandwidth);
    amplitudeArray(1,nBl) = max(abs(dataSpecReal(peakRange(1):peakRange(2))));
end

file2save = strcat('BOLD_',metabType,'_',visualType,BOLDType,woBaseline,'_Excluded_SummedData');

BOLD.fwhm = fwhmArray; BOLD.amplitude = amplitudeArray;
eval([file2save '= BOLD']);
exportFileMat = [OutputFilesPathLocal, strcat(file2save,'.mat')];
% save(exportFileMat,file2save)

x = linspace(320/60/4,(320*5/60-320/60/4),10);
figure
plot(x,fwhmArray,'o-')

title ([num2str(file2save),' fwhm'], 'Interpreter', 'none');
xlabel 't / min';
yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]);set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
set(gca, 'FontWeight','bold');
fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhm.fig'));
% savefig(fig2save)

figure
plot(x,amplitudeArray./10,'o-')

title ([num2str(file2save),' amplitude'], 'Interpreter', 'none');
xlabel 't / min';
yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]);set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
set(gca, 'FontWeight','bold');
fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_amplitude.fig'));
% savefig(fig2save)
close all
end



function CalcBOLDEffect(subjects,BlockNo,visualType,BOLDType,preprocessedFilesPathLocal,...
    OutputFilesPathLocal,nBlocks,PeakRange,metabType,excludeSubjects)
SelProcValues = SelectedProcessingValues();
fwhmArray = zeros(length(subjects),length(BlockNo));
amplitudeArray = zeros(length(subjects),length(BlockNo));
for idxSubject = 1:length(subjects)
    for nBl = 1:length(BlockNo)
        impName = strcat(subjects{idxSubject},'_',visualType,BOLDType,BlockNo{nBl},'_woMMB');
        impFileMat = strcat(preprocessedFilesPathLocal{idxSubject},impName,'.mat');
        dataPerBlock = load(impFileMat);
        fields = fieldnames(dataPerBlock);
        dataPerBlock = dataPerBlock.(fields{1});
        SelProcValues.PeakRange = PeakRange;  %ppm
        currentValue = SelProcValues.PeakRange;
        currentValue = convertPPMtoIndex(dataPerBlock, currentValue,1);
        currentValue =[find(currentValue==1,1,'first') find(currentValue==1,1,'last')];
        SelProcValues.PeakRange = [currentValue(1) currentValue(2)];
        bandwidth = dataPerBlock.Parameter.Headers.Bandwidth_Hz;
        datafid = squeeze(dataPerBlock.Data{1});
        peakRange = get(SelProcValues, 'PeakRange');
        
        if strcmp(metabType(1:2),'Wa')
            datafid = datafid(:,2);
            dataSpecReal = real(fftshift(fft(datafid)));
        else
            datafid = datafid(:,1);
            dataSpecReal = real(fftshift(fft(datafid)));
        end
        
        fwhmArray(idxSubject,nBl) = linewidth(datafid, SelProcValues, bandwidth);
        amplitudeArray(idxSubject,nBl) = max(abs(dataSpecReal(peakRange(1):peakRange(2))));
    end
end

if exist('excludeSubjects')
    n = length(subjects);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(subjects(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    fwhmArray([excludeIdx],:) = [];
    amplitudeArray([excludeIdx],:) = [];
    subjects([excludeIdx],:) = [];
end

if exist('excludeSubjects')
    file2save = strcat('BOLD_',metabType,'_',visualType,BOLDType,'_woMMB','_Excluded');
else
    file2save = strcat('BOLD_',metabType,'_',visualType,BOLDType,'_woMMB');
end
BOLD.fwhm = fwhmArray; BOLD.amplitude = amplitudeArray;
eval([file2save '= BOLD']);
exportFileMat = [OutputFilesPathLocal, strcat(file2save,'.mat')];
save(exportFileMat,file2save)

x = linspace(320/60/4,(320*5/60-320/60/4),10);
figure
hold on
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 15, 30]);
for idxSubject = 1:length(subjects)
    plot(x,(fwhmArray(idxSubject,:)+(idxSubject-1)*1.5),'o-')
end
title ([num2str(file2save),' fwhm'], 'Interpreter', 'none');
xlabel 't / min';
yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]); set(gca, 'FontWeight','bold');
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhm.fig'));
% savefig(fig2save)

figure
hold on
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 15, 30]);
for idxSubject = 1:length(subjects)
    if strcmp(metabType(1:2),'Cr')
        plot(x,(amplitudeArray(idxSubject,:)+(idxSubject-1)*0.001),'o-')
    elseif strcmp(metabType(1:3),'NAA')
        plot(x,(amplitudeArray(idxSubject,:)+(idxSubject-1)*0.002),'o-')
    else strcmp(metabType(1:3),'Wat')
        plot(x,(amplitudeArray(idxSubject,:)+(idxSubject-1)*2),'o-')
    end
end
title ([num2str(file2save),' amplitude'], 'Interpreter', 'none');
xlabel 't / min';
yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]); set(gca, 'FontWeight','bold');
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_amplitude.fig'));
% savefig(fig2save)
close all

%plot mean, std of BOLD
n = length(fwhmArray);
for indexOfBlock = 1:nBlocks
    fwhmArray_mean{1,indexOfBlock} = mean(fwhmArray(:,indexOfBlock));
    fwhmArray_std{1,indexOfBlock} = std(fwhmArray(:,indexOfBlock));
    amplitudeArray_mean{1,indexOfBlock} = mean(amplitudeArray(:,indexOfBlock));
    amplitudeArray_std{1,indexOfBlock} = std(amplitudeArray(:,indexOfBlock));
end

figure
subplot(2,1,1)
hold on
x = linspace(320/60/4,(320*5/60-320/60/4),10);
errorbar(x,cell2mat(fwhmArray_mean),cell2mat(fwhmArray_std))
title ([num2str(file2save),' fwhm, mean'], 'Interpreter', 'none');
ylabel 'FWHM / Hz';

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]);

subplot(2,1,2)
hold on
x = linspace(320/60/4,(320*5/60-320/60/4),10);
errorbar(x,cell2mat(amplitudeArray_mean),cell2mat(amplitudeArray_std))
title ([num2str(file2save),' amplitude, mean'], 'Interpreter', 'none');
xlabel 't / min';ylabel 'Amplitude / a.u.';

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]); set(gca, 'FontWeight','bold');
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 12, 18]);
fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_Mean.fig'));
savefig(fig2save)
end