classdef ISISSettings 
% version: 22 Aug 2018
% define ISIS scheme (ISIS-orig, EISIS-full)

    properties
        scheme;   
    end
    
    methods
        function this=ISISSettings()
            this.scheme   = 'ISIS-orig';
        end
    end
    
end

