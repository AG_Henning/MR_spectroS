function rand_indeces = reconstruct_all_test_data_profit(subjects, reconstruct_water_references, reconOption, truncOn, rand_indeces)
% reconstruct test data to 
%input arguments:
% subjects - mandatory parameter. Should be a cell array of the subjectIDs
% reconstruct_water_references = true or false: to reconstruct water reference data
% reconOption: 
%     0 = Water References
%     1 = Normal recon
%     2 = Removed averages
%     3 = Removed coils
%     4 = Removed averages and coils
%     5 = Water Supp Off
%     6 = Frequency Alignment Off
%     7 = Rescale off
%     8 = Rescale Water Supp Off
% truncOn = true or false: to truncate or not the data: both as first and last step of the data reconstruction
% rand_indeces = random indeces permutating the subjects. Should not be stored to assure correct anonimization

if ~exist('reconstruct_water_references','var')
    reconstruct_water_references =  false;
end
if ~exist('reconOption','var')
    reconOption = 1;
end
if ~exist('truncOn','var')
    truncOn = false;
end

exportFolders = {'0 Water References',...
    '1 Normal recon', '2 Removed averages ','3 Removed coils',...
    '4 Removed averages and coils', '5 Water Supp Off', '6 Frequency Alignment Off', ...
    '7 Rescale off', '8 Rescale Water Supp Off'};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolders);
waterExportPathBase = strcat(pathBaseExportFiles, exportFolders{1}, '\');
dataExportPathBase = strcat(pathBaseExportFiles, exportFolders{reconOption+1}, '\');

%% flags setup
flagsReconstruct.averageAll=true;
flagsReconstruct.truncStep1=true;
flagsReconstruct.frequencyAlign=true;
flagsReconstruct.rescale=true;
flagsReconstruct.eddyCurrentCorrection=true; %not yet included into recon options
flagsReconstruct.coilCombineAll=true;
flagsReconstruct.frequencyAlignFreqDomain=true;
flagsReconstruct.waterSuppress=true;
flagsReconstruct.waterSuppress2=true;
flagsReconstruct.reverseMC=true; %not yet included into recon options
flagsReconstruct.truncStepLast=true;

switch reconOption
    case 1 % 1 Normal recon
        %do nothing
    case 2 % 2 Removed averages
        flagsReconstruct.averageAll = false;
    case 3 % 3 Removed coils
        flagsReconstruct.coilCombineAll=false;
    case 4 % 4 Removed averages and coils
        flagsReconstruct.averageAll = false;
        flagsReconstruct.coilCombineAll=false;
    case 5 % 5 Water Supp Off
        flagsReconstruct.waterSuppress=false;
        flagsReconstruct.waterSuppress2=false;
    case 6 % 6 Frequency Alignment Off
        flagsReconstruct.frequencyAlign=false;
    case 7 % 7 Rescale Off
        flagsReconstruct.rescale=false;
    case 8 % 8 Rescale Water Supp Off
        flagsReconstruct.rescale=false;
        flagsReconstruct.waterSuppress=false;
        flagsReconstruct.waterSuppress2=false;
end

if truncOn == false
    flagsReconstruct.truncStep1=false;
    flagsReconstruct.truncStepLast=false;
    truncSuffix = '_truncOff';
else
    truncSuffix = '';
end
%% configurations for averages and coils

% 48 number of availble averages in the DATA_DF; phase cycling steps of 16
if  flagsReconstruct.averageAll == false
    averages2KeepOptions = {...
        1:16, 17:32, 33:48, ...
        1:32, 17:48, [1:16 33:48]};
    namingSuffixesAve = {...
        '_16ave_1', '_16ave_2', '_16ave_3', ...
        '_32ave_1', '_32ave_2', '_32ave_3'};
else
    averages2KeepOptions = {1:48};
    namingSuffixesAve = {''};
end

% 8/16 Channel 1H coil has been used. 
if  flagsReconstruct.coilCombineAll == false
    coil2DeleteOptions = {...
        1:2:16, 2:2:16, ...
        1:8, 9:16, ...
        1:2:8, 9:2:16, 2:2:8, 10:2:16, ...
        1:4, 5:8, 9:12, 13:16,...
        [1:2:8 9:16], [1:8 9:2:16], [2:2:8 9:16], [1:8 10:2:16], ...
        5:12, [1:4 9:16], [1:8 13:16], 1:12};
    namingSuffixesCoil = {...
        '_8coil_vert', '_8coil_horiz', ...
        '_8coil_1', '_8coil_2', ...
        '_12coil_vert_1', '_12coil_vert_2', '_12coil_horiz_1', '_12coil_horiz_2', ...
        '_12coil_1', '_12coil_2','_12coil_3', '_12coil_4',...
        '_4coil_vert_1', '_4coil_vert_2', '_4coil_horiz_1', '_4coil_horiz_2', ...
        '_4coil_1', '_4coil_2','_4coil_3', '_4coil_4'};
else
    coil2DeleteOptions = {[]};
    namingSuffixesCoil = {''};
end

%% import file setup
pathName = 'DF data path';

numberOfSubjects = length(subjects);
if ~exist('rand_indeces','var')
    [~,rand_indeces] = sort(rand(1,numberOfSubjects));
end
anonimizedSubjects = cell(1,numberOfSubjects);
for indexSubj = 1 : numberOfSubjects
    anonimizedSubjects{indexSubj} = strcat('Subj_', num2str(rand_indeces(indexSubj)));
end

pathBaseRawFiles = pathToDataFolder(pathName, subjects);

TEs = [24; 32; 40; 52; 60];

paths = cell(1,numberOfSubjects);
waterExportPath = cell(1,numberOfSubjects);
dataExportPath = cell(1,numberOfSubjects);
for indexSubj = 1:numberOfSubjects
    paths{indexSubj} = [pathBaseRawFiles subjects{indexSubj} '\\'];
    waterExportPath{indexSubj} = [waterExportPathBase anonimizedSubjects{indexSubj} '\\'];
    dataExportPath{indexSubj} = [dataExportPathBase anonimizedSubjects{indexSubj} '\\'];
end

files_TE24 = cell(1, numberOfSubjects);
files_TE32 = cell(1, numberOfSubjects);
files_TE40 = cell(1, numberOfSubjects);
files_TE52 = cell(1, numberOfSubjects);
files_TE60 = cell(1, numberOfSubjects);
water_files = cell(1, numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    files_TE24{indexSubject} = ls([paths{indexSubject}, '*_df_24*.dat']);
    files_TE32{indexSubject} = ls([paths{indexSubject}, '*_df_32*.dat']);
    files_TE40{indexSubject} = ls([paths{indexSubject}, '*_df_40*.dat']);
    files_TE52{indexSubject} = ls([paths{indexSubject}, '*_df_52*.dat']);
    files_TE60{indexSubject} = ls([paths{indexSubject}, '*_df_60*.dat']);
    water_files{indexSubject} = ls([paths{indexSubject}, '*_scout_wref_*.dat']);
end

addSinglet0ppm = 'Yes';
numberOfTEs = length(TEs);

filesPath = cell(numberOfTEs,numberOfSubjects);
waterFilesPath = cell(1,numberOfSubjects);
files = [files_TE24; files_TE32; files_TE40; files_TE52; files_TE60];
for indexSubj = 1:numberOfSubjects
    for indexTE = 1:numberOfTEs
        filesPath{indexTE, indexSubj} = [paths{indexSubj} files{indexTE, indexSubj}];
    end
    waterFilesPath{indexSubj} = [paths{indexSubj} water_files{indexSubj}];
end
    
%% do the actual reconstruction
parfor indexSubj = 1:numberOfSubjects
    filenameWater = [waterExportPath{indexSubj} anonimizedSubjects{indexSubj} '_water.mat'];
    if reconstruct_water_references
        %% water data
        [aWater] = reconstructWater(waterFilesPath{indexSubj});
        mkdir(waterExportPath{indexSubj});
        aWater = aWater.Anonimize_MR_spectroS(rand_indeces(indexSubj));
        aWater.ExportLcmRaw(waterExportPath{indexSubj}, [anonimizedSubjects{indexSubj}, '_water'], addSinglet0ppm);
        %calculate scaling factor the summedSpectra individually according to water peak
        waterData = aWater.Data{1};
        maxWaterPeak = max(real(fftshift(fft(waterData))));      
        parsaveWater(filenameWater, aWater, maxWaterPeak);
    else
        if exist(filenameWater,'file')
% does not work in parfor            load(filenameWater,'maxWaterPeak')
            maxWaterPeak = parloadWater(filenameWater);
        else
            maxWaterPeak = 1;
        end
    end
    
    stats = cell(numberOfTEs,length(averages2KeepOptions));
    %% reconstruct data
    for indexTE = 1:numberOfTEs
        for indexAverageDelete = 1: length(averages2KeepOptions)
            averages2Keep = averages2KeepOptions{indexAverageDelete};
            namingSuffixAve = namingSuffixesAve{indexAverageDelete};
            for indexCoilDelete = 1: length(coil2DeleteOptions)
                coilChannels2Delete = coil2DeleteOptions{indexCoilDelete};
                namingSuffixCoil = namingSuffixesCoil{indexCoilDelete};
                try
                    [a, stats{indexTE,indexAverageDelete, indexCoilDelete}] = ...
                        reconstruct_testData(filesPath{indexTE, indexSubj}, flagsReconstruct, ...
                        averages2Keep, coilChannels2Delete);
                    a = a.Anonimize_MR_spectroS(rand_indeces(indexSubj));
                    TE_string = ['TE', num2str(TEs(indexTE))];
                    filenameData = [ anonimizedSubjects{indexSubj}, '_', TE_string namingSuffixAve namingSuffixCoil truncSuffix];
                    mkdir(dataExportPath{indexSubj});
                    dataExportPathTE = [dataExportPath{indexSubj}, '\\', TE_string, '\\'];
                    mkdir(dataExportPathTE);
                    parsave([dataExportPathTE, filenameData, '.mat'], a);
                    a.ExportLcmRaw(dataExportPathTE, filenameData, addSinglet0ppm);
                catch
                    % do nothing, just continue
                    continue;
                end
                close all
            end
        end
    end
    
    parsaveStats([dataExportPath{indexSubj}, 'stats', truncSuffix, '.mat'], stats);
end

%%
close all

end

function parsaveWater(fname, aWater, maxWaterPeak)
save(fname, 'aWater', 'maxWaterPeak')
end

function maxWaterPeak = parloadWater(fname)
load(fname, 'maxWaterPeak')
end

function parsave(fname, a)
save(fname, 'a')
end

function parsaveStats(fname, stats)
save(fname, 'stats')
end
