function this = fMRSDifferenceGUI( this )

if this.Parameter.ReconFlags.isfMRSDifference
    error('The data are already fMRSDifference')
end

if ~this.Parameter.ReconFlags.isfMRSAveraged
    error('The data has to fMRSAveraged first')
end


lorenzianFactor = this.Parameter.fMRSDiffSettings.lorenzianFactor;
phase           = this.Parameter.fMRSDiffSettings.PhaseCorrection;
frequency       = this.Parameter.fMRSDiffSettings.frequencyShift;
offset          = this.Parameter.fMRSDiffSettings.OffsetCorrection;
automatic       = this.Parameter.fMRSDiffSettings.Automatic;

indecesRegion   = this.Parameter.fMRSDiffSettings.indecesRegion;
dwellTime       = this.Parameter.Headers.DwellTimeSig_ns * 1e-9; %in secs
data            = this.Data{1};

if isempty(lorenzianFactor)
    if isempty(indecesRegion)
        error('You have to specify the indeces region first')
    end
    
     %find the expLorentzian factor that you will use for the multiplication
     x1 = -1.5; x2 = 1.5; 
     options = optimset('Display','iter','TolX',1E-9,'TolFun',1E-9); 
     expFactor = fminbnd(@(expFactor) costFunction1( expFactor, data, dwellTime,indecesRegion),x1,x2,options); 
     
else
    
    expFactor = lorenzianFactor;
end
   
   %now try to phase them as well
    
     x = [expFactor phase frequency offset];
 
     
if automatic 
%      x = fmincon(fun,x0,A,b,Aeq,beq,lb,ub);
       x0 = x;
       A = []; b=[]; Aeq = []; beq = [];
       lb = -[2 4 4]; ub = [2 4 4];
       options = optimset('Display','iter','TolX',1E-18,'TolFun',1E-18,'TolCon',1E-18);
       x = fmincon(@(x) costFunction2( x, data, dwellTime,indecesRegion),x0,A,b,Aeq,beq,lb,ub,[],options);

end    
   
   
   
    [~, newData] = costFunction2(x,data,dwellTime,indecesRegion);
   
    this.Data{1} = newData;
    
    
    
    this.Parameter.fMRSDiffSettings.lorenzianFactor  = x(1);
    this.Parameter.fMRSDiffSettings.PhaseCorrection  = x(2);
    this.Parameter.fMRSDiffSettings.frequencyShift   = x(3);
    this.Parameter.fMRSDiffSettings.OffsetCorrection = x(4);
    this.Parameter.fMRSDiffSettings.Automatic        = false;
    this.Parameter.ReconFlags.isfMRSDifference       = true;


end


%Cost functions
function [residual, difference] = costFunction1(expFactor,data,dwellTime,indecesRegion)

selectedMix     = 1;  %use the Cr or NAA peak!
timePoints      = size(data,1);
sizeDiff        = size(data);
sizeDiff(12)    = 1;
difference      = zeros(sizeDiff);

timeVector      = linspace(0,(timePoints-1)*dwellTime,timePoints)';

residual  = 0;
if ~isempty(indecesRegion)
    fidON           = squeeze(data(:,:,:,:,:,:,:,:,selectedMix,:,:,1));
    fidOFF          = squeeze(data(:,:,:,:,:,:,:,:,selectedMix,:,:,2)); 
    fidONCorr       = fidON.*exp(pi*expFactor*timeVector);
    residual        = fidONCorr - fidOFF;
    residual        = fftshift(fft(residual));
    residual        = residual(indecesRegion(1):indecesRegion(2));
    residual        = sum(abs(residual).^2);
end

for mix=1:2
    difference(:,:,:,:,:,:,:,:,mix,:,:,1) = data(:,:,:,:,:,:,:,:,mix,:,:,1).*exp(pi*expFactor*timeVector)-...
                                            data(:,:,:,:,:,:,:,:,mix,:,:,2);
end

end



function [residual, newData] = costFunction2(x,data,dwellTime,indecesRegion)

selectedMix     = 1;  %use the Cr or NAA peak!
timePoints      = size(data,1);
sizeDiff        = size(data);
sizeDiff(12)    = 1;
difference      = zeros(sizeDiff);
%phase0          = x(1); 
%expFactor       = x(2);
expFactor       = x(1);
phase           = x(2)*pi/180;  %convert it in rads
frequency       = x(3);
offset          = x(4);

timeVector      = linspace(0,(timePoints-1)*dwellTime,timePoints)';

residual  = 0;


corFactor = exp(pi*expFactor*timeVector).*exp(1i*phase).*exp(1i*2*pi*frequency*timeVector);
for mix=1:2
    
      onDataFFTs              = fftshift(fft(data(:,:,:,:,:,:,:,:,mix,:,:,1)));
      onDataFFTsReal          = real(onDataFFTs) + offset;
      onDataFFTsImag          = imag(onDataFFTs);
      onDataTemp              = ifft(ifftshift(onDataFFTsReal + 1j*onDataFFTsImag));
      
      %onData(:,:,:,:,:,:,:,:,mix,:,:,1)     = data(:,:,:,:,:,:,:,:,mix,:,:,1).*corFactor;
      onData(:,:,:,:,:,:,:,:,mix,:,:,1)      = onDataTemp.*corFactor;
      
      offData(:,:,:,:,:,:,:,:,mix,:,:,1)    = data(:,:,:,:,:,:,:,:,mix,:,:,2);
      difference(:,:,:,:,:,:,:,:,mix,:,:,1) = onData(:,:,:,:,:,:,:,:,mix,:,:,1)-offData(:,:,:,:,:,:,:,:,mix,:,:,1) ;
      
end      

residual = difference(:,:,:,:,:,:,:,:,1,:,:,1);
residual = fftshift(fft(residual));
residual = residual(indecesRegion);
residual = sum(abs(residual).^2);

newData = cat(12,onData,offData,difference);
end





%% old code







