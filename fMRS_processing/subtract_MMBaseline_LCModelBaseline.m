function subtract_MMBaseline_LCModelBaseline()
pathName = 'fMRS data path';    %first version, no modulus correction for gradient sideband removal
% pathName = 'fMRS data path modulus';    %second version, with modulus correction for gradient sideband, only UF can be processed
% sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName);
StimType = {'RestON', 'RestOFF'};      %'StimON', 'StimOFF', 'RestON', 'RestOFF'
if ~isempty(strfind(StimType{1},'Stim'))
    visualType = 'Stim';
elseif ~isempty(strfind(StimType{1},'Rest'))
    visualType = 'Rest';
else
    error('StimType needs to be Stim or Rest');
end
Met = 'NAA';
if Met == 'Lac'
    Met = 1.314;
elseif Met == 'NAA'
    Met = 2.008;
else
    error('Met needs to be Lac or NAA');
end
BOLDTypes = {'_woBOLDCorr128', '_withBOLDCorr128', '_woBOLDCorr64', '_woBOLDCorr64#1', '_withBOLDCorr64', '_withBOLDCorr64#1'};  %_woBOLDCorr128, _withBOLDCorr128, _woBOLDCorr64, _woBOLDCorr64#1, _withBOLDCorr64, _withBOLDCorr64#1
for idxBOLDType = 1:length(BOLDTypes)
    BOLDType = BOLDTypes(idxBOLDType);
    ordersFMRS = strcat(StimType,BOLDType);%
    for idxOrderFMRS = 1:length(ordersFMRS)
        orderFMRS = ordersFMRS{idxOrderFMRS};
        MMBType = '_MMBSummed_v2';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated
        % subjects = {'3333';};
        subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
            '4085';'3333';'4012';'4085';'6524'};
        % prefixes = {'2020-03-10_';};
        prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
            '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
            '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
        excludeSubjects = {'5166';'9810';'5269'};
        numberOfSubjects = length(subjects);
        suffixFolder = '_fMRS';
        if strcmp(MMBType,'_MMBSummed')
            excludeMet = {'NAA';'GABA';'Gln';'Glyc';'GSH';'mI';'NAAG';'PCh';'GPC';'PE';'Scyllo';'Tau';'Glc';'NAA_NAAG';'Glu_Gln';'Glyc';'mI_Glyc';'PCh'};  %for v1
        elseif strcmp(MMBType,'_MMBSummed_v2')
            excludeMet = {'NAA';'Asc';'GABA';'Glc';'Gln';'GSH';'mI';'NAAG';'tCh';'PE';'Scyllo';'Tau';'NAA_NAAG';'Glu_Gln'};  %for v2
        end
        
        extensionTable = '.table';
        extensionCoord = '.coord';
        defaultfMRS = 'StimOFF';
        defaultSubject = 'XXXX';
        defaultSubjectsPath = 'YYYY';
        % defaultMMB = '_MMBSummed';
        defaultMMB = '_MMBSummed_v2';
        defaultLCModelUser = 'jdorst';
        
        % controlFilesBase = 'fitsettings_fMRS_XXXX_';
        controlFilesBase = 'fitsettings_fMRS_v2_XXXX_';
        controlFilesBaseSuffix = '.control';
        outputFileNameBase = strcat(defaultSubject, '_');
        
        %% file paths setup
        controlFilesPathRemote = '/Desktop/1H_fMRS/LCModelConfig/';
        controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
        LCModelOutputFilesPathRemote = '/Desktop/1H_fMRS/Output/';
        if ~isempty(strfind(localFilePathBase,'Modulus'))
            LCModelOutputPath = [localFilePathBase, 'Output_v2_MMBSummed_Modulus/'];
        else
            LCModelOutputPath = [localFilePathBase, 'Output_v2_MMBSummed/'];
        end
        defaultControlFile = strcat(controlFilesBase, defaultfMRS, controlFilesBaseSuffix);
        
        subjectsPath = strcat(prefixes, subjects, suffixFolder);
        
        subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath, '/');
        subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath, '/');
        preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath, '/');
        subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath, '/');
        subjectsLCModelOutputPaths = strcat(LCModelOutputPath, subjectsPath, '/');
        LCModelOutputFiles = strcat(outputFileNameBase, orderFMRS, MMBType);
        LCModelOutputFileswoBaseline = strcat(outputFileNameBase, orderFMRS, MMBType, '_woBaseline');
        
        %% process fMRS data and save .RAW file
        dataPath = strcat(localFilePathBase,subjectsPath, '/');
        
        for indexCurrentSubject = 1:numberOfSubjects
            subjectsLCModelOutputPath = subjectsLCModelOutputPaths{indexCurrentSubject};
            currentOutputFileswoBaseline = strrep(LCModelOutputFileswoBaseline, defaultSubject, subjects{indexCurrentSubject});
            
            currentDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject}, '/');
            currentLCModelFit = strrep(strcat(outputFileNameBase, orderFMRS, '_v2'), defaultSubject, subjects{indexCurrentSubject});
            currentLCModelMatFit = strrep(strcat(currentLCModelFit, '.mat'),'#','_');
            
            
            indexStart = strfind(currentOutputFileswoBaseline,'_woBaseline');
            if ~isempty(indexStart)
                currentLCModelOutputFit = currentOutputFileswoBaseline(1:indexStart-1);
                currentLCModelCoordFit = strcat(currentLCModelOutputFit, '.coord');
            else
                currentLCModelCoordFit = strcat(currentOutputFileswoBaseline, '.coord');
            end
            
            if ~isempty(strfind(currentOutputFileswoBaseline, '_v2'))
                currentRawOutputFile = strcat(strrep(currentLCModelFit,'_Av','_v2_Av'), '_woBaseline_MMB');
            else
                currentRawOutputFile = strcat(currentLCModelFit, '_woBaseline_MMB');
            end
            
            
            % metabolitesToSubtract = {'Glu'};
            doNullingOfDownField = false;
            if ~isempty(strfind(currentLCModelFit,'#'))
                reconstructedSpectra = strrep(strcat(currentDataPath,strrep(currentLCModelMatFit,'_1','#1')),'_v2','');
            else
                reconstructedSpectra = strrep(strcat(currentDataPath,currentLCModelMatFit),'_v2','');
            end
            reconstructedSpectraWithoutMetabolite = strcat(strcat(currentDataPath,'MMBSubtracted/'), currentRawOutputFile);
            
            phase90 = -90 * pi/180;
            zeroFillFactor = 2;
            
            %plot configs
            FontSize = 14;
            LineWidth = 1.5;
            
            % load(reconstructedSpectra, 'a');
            a = load(reconstructedSpectra);
            fields = fieldnames(a);
            a = a.(fields{1});
            [nOfPoints, ppmVector, phasedData, fitData, baselineData, residualData, ...
                metaboliteNames, metaboliteData, tableConcentrations, ppmStart, ppmEnd, ...
                scanFrequency,  frequencyShift, ppmGap] = ...
                extractFits(subjectsLCModelOutputPath,currentLCModelCoordFit);
            
            % align the two spectra
            fid = a.Data{1};
            spectrum = fftshift(fft(fid(:,1,1,1,1,1,1,1,1,1,1,1),size(fid,1)*zeroFillFactor));
            ppmSpectrum = a.getPpmVector(zeroFillFactor);
            
            [~, maxPpmValueSpectrum] = getMaxPeak(ppmSpectrum', real(spectrum), Met, 0.1);
            [~, maxPpmValueLCModel] = getMaxPeak(ppmVector, phasedData, Met, 0.1);
            
            frequencyShiftDiff = maxPpmValueSpectrum - maxPpmValueLCModel;
            fMRSData_withBaseline = a.ApplyFrequencyShift(frequencyShiftDiff);
            %get the aligned reconstructed spectra
            fid = fMRSData_withBaseline.Data{1};
            spectrum = fftshift(fft(fid(:,1,1,1,1,1,1,1,1,1,1,1),size(fid,1)*zeroFillFactor));
            % max of the reconstructed spectrum
            if isempty(ppmGap)
                ppmSpectrumMask  = (ppmSpectrum < ppmStart) & (ppmSpectrum > ppmEnd);
                [~,idx] = find(ppmSpectrumMask == 1);
                ppmSpectrumMask(idx(1)-1) = 1;
                %     ppmSpectrumMask(idx(end)+1) = 1;
                ppmMetaboliteMask  = (ppmVector < ppmStart) & (ppmVector > ppmEnd);
            else
                ppmSpectrumMask  = (ppmSpectrum < ppmGap) & (ppmSpectrum > ppmEnd);
                ppmMetaboliteMask  = (ppmVector < ppmGap) & (ppmVector > ppmEnd);
            end
            maxSpectrum = max(real(spectrum(ppmSpectrumMask)));     %spectrum between 0.6 and 4.1 ppm
            % max of the LCModel fit
            maxPhasedData = max(phasedData);
            % calculate scaling
            scalingFactor = maxSpectrum/maxPhasedData;
            oldSpectrum = spectrum;
            % for indexMetabolite = 1:length(metabolitesToSubtract)
            metaboliteToSubtract = 'Leu';
            indexMetaboliteToSubtract = find(strcmp(metaboliteNames,metaboliteToSubtract));
            if isempty(indexMetaboliteToSubtract)
                error('Metabolite %s not found in fit',metaboliteToSubtract);
            end
            ppmMetaboliteMask(1) = 1;ppmMetaboliteMask(end) = 1;
            spectrumToSubtract = metaboliteData{indexMetaboliteToSubtract}.metaboliteSpectrum(ppmMetaboliteMask);
            spectrumToSubtract = spectrumToSubtract + baselineData;
            spectrumToSubtract(1) = 0;
            %eliminate first point, which is anyway an error; Also scale it
            spectrumToSubtractFlipped = flipud(spectrumToSubtract).*scalingFactor;
            
            spectrumToSubtractReal = zeros(size(oldSpectrum));
            spectrumToSubtractReal(ppmSpectrumMask) = spectrumToSubtractReal(ppmSpectrumMask) + spectrumToSubtractFlipped;
            fidToSubtract = ifft(ifftshift(complex(spectrumToSubtractReal)));
            fidToSubtract(length(fidToSubtract)/2:end)=0;
            spectrumToSubtractPhased90 = fftshift(fft(fidToSubtract .* exp(1i*phase90))).*2; %multiply by 2 to account for nulling the half of the FID
            spectrumToSubtractImag = real(spectrumToSubtractPhased90);
            newSpectrumReal = real(oldSpectrum) - spectrumToSubtractReal;
            newSpectrumImag = imag(oldSpectrum) - spectrumToSubtractImag;
            newSpectrum = complex(newSpectrumReal, newSpectrumImag);
            
            if (doNullingOfDownField)
                newSpectrum(length(newSpectrum)/2:end) = 0;
            end
            
            oldSpectrum = newSpectrum;
            % end
            
            figure
            % plot(ppmVector, phasedData.*scalingFactor)
            % hold on
            plot(ppmSpectrum, real(spectrum),'r')
            hold on
            plot(ppmSpectrum, real(newSpectrum),'k')
            set(gca,'xDir','reverse')
            xlim([0 4.2])
            title(sprintf('Difference spectrum with and w/o baseline + MM'))
            legend('Before subtraction','After subtration')
            
            fig2save_Conc = fullfile(strcat(currentDataPath,'MMBSubtracted/'), strcat(strrep(currentRawOutputFile,'#','_'), '.fig'));
            savefig(fig2save_Conc)
            
            close all
            
            % figure(101)
            % hold on
            % p1 = plot(ppmSpectrum, real(spectrum), 'r');
            % hold on
            % p2 = plot(ppmSpectrum, real(newSpectrum),'k');
            % xlim([0 4.2])
            % xlabel('[ppm]');
            % %     ylim([(indexTE+1) * offsetPlot-offsetPlot*0.5 5e-5]);
            % set(gca,'xDir','reverse')
            % set(gca,'ytick',[]);
            % set(gca,'fontsize',FontSize);
            % set(p1,'LineWidth',LineWidth);
            % set(p2,'LineWidth',LineWidth);
            % title('Macromolecular baseline with and w/o the Cr peak')
            
            fMRSData_woBaseline = fMRSData_withBaseline;
            newFid = ifft(ifftshift(newSpectrum));
            fMRSData_woBaseline.Data{1} = newFid;
            % trunc_ms = 250;
            % truncPoint = floor( trunc_ms/(fMRSData_woBaseline.Parameter.Headers.DwellTimeSig_ns*1e-6));
            % fMRSData_woBaseline = fMRSData_woBaseline.Truncate(truncPoint);
            
            ExpName = fullfile(strcat(currentDataPath,'MMBSubtracted/'), strcat(currentRawOutputFile, '.fig'));
            % % eval([currentOutputFileswoBaseline '=fMRSData_woBaseline'])
            % % save(reconstructedSpectraWithoutMetabolite,currentRawOutputFile);
            save(strrep(reconstructedSpectraWithoutMetabolite,'#','_'),'fMRSData_woBaseline');
            fMRSData_woBaseline.ExportLcmRaw(strcat(currentDataPath,'MMBSubtracted/'), strrep(currentRawOutputFile,'#','_'), 'Yes', 'Yes');
            
        end
    end
end
end

function [maxValue, maxPpmValue] = getMaxPeak(ppmVector, spectrum, peakPpm, searchAreaPpm)
minPpm = peakPpm - searchAreaPpm;
maxPpm = peakPpm + searchAreaPpm;
ppmMask = (ppmVector > minPpm) & (ppmVector < maxPpm);
tempSpectrum = spectrum .* ppmMask;
[maxValue, maxIndex] = max(real(tempSpectrum));
maxPpmValue = ppmVector(maxIndex);
end
