    %function [e, fid_aligned] = align_fid(x, fid_ref, fid, idx)
    function [e, G, fidAligned] = AlignFid(x, fidRef, fid, idx)
        
  
        fidAligned = fid.*exp(1i*x*2*pi*linspace(0,1,length(fid))).';
        
        
        
        % I just changed the residual part in order to correspond to the FrequencyAlign_fMRS
        % for the FrequencyAlign delete the fftshift command 
        
        residual = [ real(fftshift (fft(fidRef)))-real(fftshift((fft(fidAligned)))); imag( fftshift( (fft(fidRef) ) ))- imag(fftshift (fft(fidAligned)))];
        e = sum([residual(idx); residual(idx+length(fid))].^2);
         
        
       gr_pc1 = -2*repmat(fftshift(  fft(1i*2*pi*linspace(0,1,length(fid)).'.*fidAligned)), [2 1]).*residual;
       G = sum([real(gr_pc1(idx)); imag(gr_pc1(idx+length(fid)))]);
    end