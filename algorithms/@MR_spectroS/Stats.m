function stats = Stats(this,list)
%29 March 2017
% list: a list containing ppm range and name of met! {[2.0 2.1] "NAA";[4.18 4.22] "mI";...}
%
%
% retrieve SNR values for NAA, Cr(CH3), Glu (2.4ppm), tCho
% the noise is calculated from the noise window of the first 500 points (-5 to -3 ppm)
% subtracting a 1st order polynomial from the points
% The peak amplitude is calculated from the real spectra

noiseRangePpm = [-4 -1];
ppmVector = this.getPpmVector;
indexNoiseStart = find(ppmVector>noiseRangePpm(1),1,'first');
indexNoiseEnd = find(ppmVector>noiseRangePpm(2),1,'first');
noiseRange = [indexNoiseStart indexNoiseEnd];
if ~exist('list','var')
    list = {[1.9 2.1] 'NAA'; [2.9 3.1] 'Cr';[2.25 2.5] 'Glu'; [3.1 3.3] 'tCho'};
end
waterRange = {[3.7 5.7] 'H2O'};
if ~this.Parameter.ReconFlags.isAveraged & ~this.Parameter.ReconFlags.isCoilsCombined
    warning('Skip...');

else
    %retrieve metabolites
    mix = 1;
    data = this.Data{1};
    data = data(:,:,:,:,:,:,:,:,mix,:,:,:);
    data = real(fftshift(fft(data)));

    %calculate noise
    [noiseMetaboliteSignal] = noiseCalc(data, noiseRange);

    for i=1:size(list,1)
        metRange = list{i,1};
        metRange = this.convertPPMtoIndex(metRange,1);
        metRange =[find(metRange==1,1,'first') find(metRange==1,1,'last')];

        [snrV, amplitude] = snr(data,metRange,noiseMetaboliteSignal);
        lineWidthValue = linewidth( data,metRange,this);
        %area          = sum(data(metRange(1):metRange(2)));
        stats{i,1} = list{i,2};
        stats{i,2} = snrV;
        stats{i,3} = lineWidthValue;
        stats{i,4} = amplitude;
        %stats{i,4} = area;
    end
    i= size(list,1);
    
    %retrieve water
    mix = 2;
    data = this.Data{1};
    if size(data,9) == 1
        warning('No mix 2 found. Assuming non metabolite cycled water data');
        mix = 1;
    end
    data = data(:,:,:,:,:,:,:,:,mix,:,:,:);
    data = real(fftshift(fft(data)));
    metRange = waterRange{1};
    metRange = this.convertPPMtoIndex(metRange,1);
    metRange =[find(metRange==1,1,'first') find(metRange==1,1,'last')];

    [snrV, amplitude] = snr(data,metRange,noiseMetaboliteSignal);
    lineWidthValue = linewidth( data,metRange,this);

    %area          = sum(data(metRange(1):metRange(2)));
    stats{i+1,1} = waterRange{2};
    stats{i+1,2} = snrV;
    stats{i+1,3} = lineWidthValue;
    stats{i+1,4} = amplitude;
end
end

function [noise] = noiseCalc(data, noiseRange)
%version 29 March 2017
fftsReal =  data;
% noise calculation
noise     = (fftsReal(noiseRange(1):noiseRange(2)));
fitobject = fit((0:length(noise)-1)',noise,'poly1'); %fit a poly2 to noise
y         = feval(fitobject,(0:length(noise)-1)');   % substract poly1 from noise
noise     = noise - y;% now the noise does not have linear component
noise     = std(noise);
end

function [snrValue, amplitude] = snr( data,peakRange,noise)
%version 29 March 2017
fftsReal =  data;
%amplitude = sum(fftsReal(peakRange(1):peakRange(2)));
amplitude = max(abs(fftsReal(peakRange(1):peakRange(2))));
snrValue  = amplitude/noise;
%snrValue  = 20*log(amplitude/noise); %in dB
end

function lineWidthValue = linewidth( data,metRange,this )
peakRange     = metRange;
fftsReal      = data;
peak          = fftsReal(peakRange(1):peakRange(2));

interpFactor  = 100; %
indeces       = [];
i = 1;
while (length(indeces)<2 && i <4)
    peakTmp         = interp(peak,interpFactor*i);
    [~,  maxPos] = max(abs(peakTmp));
    normValues   = peakTmp/(peakTmp(maxPos)/2);
    indeces      = find(normValues> 0.995 & normValues< 1.005);
    i            = i+1;
end

bandwidth       = this.Parameter.Headers.Bandwidth_Hz;
interpFactor    = interpFactor*(i-1);
freqResolution  = bandwidth/(length(data)*interpFactor);
try
    lineWidthValue  = (indeces(end) - indeces(1))*freqResolution;
catch err
    lineWidthValue = [];
end
end







