function BoldEffectfMRS_SlidingAverage()
clear; clc; close all;

pathName = 'fMRS SlidingAverage data path';
[localFilePathBase] = pathToDataFolder(pathName);
visualType = 'Stim';      %'Stim', 'Rest'
BOLDType = '_woBOLDCorr';
nBlocks = 10;
nAv = 37;
AvNo = cell(1,nAv);
for k = 1:nAv
    AvNo{k} = strcat('_Av', num2str(k));
end
woBaseline = false;

subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
    '4085';'3333';'4012';'4085';'6524'};
% prefixes = {'2020-02-18_';};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
    '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
    '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
suffixFolder = '_fMRS';
excludeSubjects = {'5166';'9810';'5269'};

subjectsPath = strcat(prefixes, subjects, suffixFolder);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath, '/');
OutputFilesPathLocal = strcat(localFilePathBase, 'Output/BOLD/');

% % %for Water
% PeakRange = [4 5];  %ppm where to find the peak
% metabType = 'Water';
% if exist('excludeSubjects')
%     CalcBOLDEffect(subjects,AvNo,visualType,BOLDType,preprocessedFilesPathLocal,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType,'',excludeSubjects)
% else
%     CalcBOLDEffect(subjects,AvNo,visualType,BOLDType,preprocessedFilesPathLocal,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType)
% end
% %for NAA (2.008 ppm)
% PeakRange = [1.8 2.3];  %ppm where to find the peak
% metabType = 'NAA';
% if exist('excludeSubjects')
%     CalcBOLDEffect(subjects,AvNo,visualType,BOLDType,preprocessedFilesPathLocal,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline,excludeSubjects)
% else
%     CalcBOLDEffect(subjects,AvNo,visualType,BOLDType,preprocessedFilesPathLocal,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
% end
% %for Cr (methyl signal of tCr: 3.027ppm)
% PeakRange = [2.9 3.15];  %ppm where to find the peak
% metabType = 'Cr';
% if exist('excludeSubjects')
%     CalcBOLDEffect(subjects,AvNo,visualType,BOLDType,preprocessedFilesPathLocal,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline,excludeSubjects)
% else
%     CalcBOLDEffect(subjects,AvNo,visualType,BOLDType,preprocessedFilesPathLocal,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
% end
% 
% %correlation FWHM, Amplitude
% metabType = {'NAA', 'Cr'};
% if exist('excludeSubjects')
%     CorrBOLDSpearman(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects)
% else
%     CorrBOLDSpearman(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline)
% end
% % CorrBOLDCategorical(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline)

%correlation Water, Metabolite Concentration
%%%%% didn't check this for metabolites from sliding average %%%%%
metabType = {'Lac'};
if exist('excludeSubjects')
    CorrBOLDSpearman_MetConc(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects)
else
    CorrBOLDSpearman_MetConc(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline)
end

end

function CorrBOLDSpearman_MetConc(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end

if exist('excludeSubjects')
    impName = strcat('BOLD_Water_',visualType,BOLDType,'_SlidingAverage_Excluded');
else
    impName = strcat('BOLD_Water_',visualType,BOLDType,'_SlidingAverage');
end
impFileMat = strcat(OutputFilesPathLocal,impName,'.mat');
BOLDData = load(impFileMat);
fields = fieldnames(BOLDData);
BOLDDataXY(1) = BOLDData.(fields{1});

%import Met Conc
impNameMetConc = strcat('BlockConc_Stim_woBOLDCorr_MMBSummed',woBaseline)
impFileMatMetConc = strcat(OutputFilesPathLocal(1:end-5),impNameMetConc,'.mat');
BOLDData = load(impNameMetConc);
fields = fieldnames(BOLDData);
BOLDData_temp = BOLDData.(fields{1});
if exist('excludeSubjects')
    n = length(BOLDData_temp);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(BOLDData_temp(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    BOLDData_temp([excludeIdx],:) = [];
end
indexMet = strcmp(BOLDData_temp(1,2:end),metabType);
idxMet = find(indexMet);
BOLDData_temp = BOLDData_temp(2:end-4,2:end);
for i = 1:length(BOLDData_temp)
    BOLDData_Met{i} = BOLDData_temp{i,idxMet};
end

if exist('excludeSubjects')
    file2save = strcat('Corr_Water_',metabType{1},'_Conc_',visualType,BOLDType,woBaseline,'_Excluded');
else
    file2save = strcat('Corr_Water_',metabType{1},'_Conc_',visualType,BOLDType,woBaseline);
end

subjLength = length(BOLDDataXY(1).fwhm);
subjects = ones(subjLength*10,1);
for i = 1:subjLength
    subjects((1+(i-1)*10):(10+(i-1)*10)) = subjects((1+(i-1)*10):(10+(i-1)*10)).*i;
end

%for FWHM
fwhmTab = table(BOLDDataXY(1).fwhm(:),cell2mat(BOLDData_Met'),subjects);
BOLDData_FWHM = table2array(fwhmTab);
%Spearman correlation
[RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Spearman');
% [RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Pearson');
%linear regression
fit = fitlm(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2));
figure
hold on
% plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
plot(fit)
xlabel(['FWHM Water / Hz'])
ylabel(['Concentration ',metabType, ' / a.u.'])
title(['Correlation FWHM Water & concentration', metabType, woBaseline], 'Interpreter', 'none')
txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
text(max(BOLDData_FWHM(:,1)-0.5),max(BOLDData_FWHM(:,2)-1),txt)

fig2saveFWHM = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhmConc.fig'));
savefig(fig2saveFWHM)

%for amplitude
ampTab = table(BOLDDataXY(1).amplitude(:),cell2mat(BOLDData_Met'),subjects);
BOLDData_amp = table2array(ampTab);
%Spearman correlation
[RHO,PVAL] = corr(BOLDData_amp(:,1),BOLDData_amp(:,2),'Type','Spearman');
%linear regression
fit = fitlm(BOLDData_amp(:,1),BOLDData_amp(:,2));
figure
hold on
% plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
plot(fit)
xlabel(['Amplitude Water / a.u.'])
ylabel(['Concentration ',metabType, ' / a.u.'])
title(['Correlation Amplitude Water & concentration', metabType, woBaseline], 'Interpreter', 'none')
txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
text((min(BOLDData_amp(:,1))+min(BOLDData_amp(:,1))*0.05),(max(BOLDData_amp(:,2))-max(BOLDData_amp(:,2))*0.05),txt)
fig2saveAmplitude = fullfile(OutputFilesPathLocal, strcat(file2save,'_ampConc.fig'));
savefig(fig2saveAmplitude)
end

function CorrBOLDSpearman(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end

for idxmetab = 1:length(metabType)
    if strcmp(metabType{idxmetab}(1:2),'Wa')
        if exist('excludeSubjects')
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,'_Excluded');
        else
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType);
        end
    else
        if exist('excludeSubjects')
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,woBaseline,'_Excluded');
        else
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,woBaseline);
        end
    end
    impFileMat = strcat(OutputFilesPathLocal,impName,'.mat');
    BOLDData = load(impFileMat);
    fields = fieldnames(BOLDData);
    BOLDDataXY(idxmetab) = BOLDData.(fields{1});
end
if exist('excludeSubjects')
    file2save = strcat('Corr_',metabType{1},'_',metabType{2},'_',visualType,BOLDType,woBaseline,'_Excluded');
else
    file2save = strcat('Corr_',metabType{1},'_',metabType{2},'_',visualType,BOLDType,woBaseline);
end

subjLength = length(BOLDDataXY(1).fwhm);
subjects = ones(subjLength*10,1);
for i = 1:subjLength
    subjects((1+(i-1)*10):(10+(i-1)*10)) = subjects((1+(i-1)*10):(10+(i-1)*10)).*i;
end

%for FWHM
fwhmTab = table(BOLDDataXY(1).fwhm(:),BOLDDataXY(2).fwhm(:),subjects);
BOLDData_FWHM = table2array(fwhmTab);
%Spearman correlation
[RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Spearman');
% [RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Pearson');
%linear regression
fit = fitlm(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2));
figure
hold on
% plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
plot(fit)
xlabel(['FWHM ',num2str(metabType{1}), ' / Hz'])
ylabel(['FWHM ',num2str(metabType{2}), ' / Hz'])
title(['Correlation FWHM ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
text(min(BOLDData_FWHM(:,1)+1),max(BOLDData_FWHM(:,2)-1),txt)

fig2saveFWHM = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhm.fig'));
savefig(fig2saveFWHM)

%for amplitude
ampTab = table(BOLDDataXY(1).amplitude(:),BOLDDataXY(2).amplitude(:),subjects);
BOLDData_amp = table2array(ampTab);
%Spearman correlation
[RHO,PVAL] = corr(BOLDData_amp(:,1),BOLDData_amp(:,2),'Type','Spearman');
%linear regression
fit = fitlm(BOLDData_amp(:,1),BOLDData_amp(:,2));
figure
hold on
% plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
plot(fit)
xlabel(['Amplitude ',num2str(metabType{1}), ' / a.u.'])
ylabel(['Amplitude ',num2str(metabType{2}), ' / a.u.'])
title(['Correlation Amplitude ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
text((min(BOLDData_amp(:,1))+min(BOLDData_amp(:,1))*0.05),(max(BOLDData_amp(:,2))-max(BOLDData_amp(:,2))*0.05),txt)
fig2saveAmplitude = fullfile(OutputFilesPathLocal, strcat(file2save,'_amp.fig'));
savefig(fig2saveAmplitude)
end




function CalcBOLDEffect(subjects,AvNo,visualType,BOLDType,preprocessedFilesPathLocal,...
    OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline,excludeSubjects)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
SelProcValues = SelectedProcessingValues();
fwhmArray = zeros(length(subjects),length(AvNo));
amplitudeArray = zeros(length(subjects),length(AvNo));
for idxSubject = 1:length(subjects)
    for nBl = 1:length(AvNo)
        impName = strcat(subjects{idxSubject},'_',visualType,BOLDType,AvNo{nBl},woBaseline);
        impFileMat = strcat(preprocessedFilesPathLocal{idxSubject},impName,'.mat');
        dataPerBlock = load(impFileMat);
        fields = fieldnames(dataPerBlock);
        dataPerBlock = dataPerBlock.(fields{1});
        SelProcValues.PeakRange = PeakRange;  %ppm
        currentValue = SelProcValues.PeakRange;
        currentValue = convertPPMtoIndex(dataPerBlock, currentValue,1);
        currentValue =[find(currentValue==1,1,'first') find(currentValue==1,1,'last')];
        SelProcValues.PeakRange = [currentValue(1) currentValue(2)];
        bandwidth = dataPerBlock.Parameter.Headers.Bandwidth_Hz;
        datafid = squeeze(dataPerBlock.Data{1});
        peakRange = get(SelProcValues, 'PeakRange');
        
        if strcmp(metabType(1:2),'Wa')
            datafid = datafid(:,2);
            dataSpecReal = real(fftshift(fft(datafid)));
        else
            datafid = datafid(:,1);
            dataSpecReal = real(fftshift(fft(datafid)));
        end
        
        fwhmArray(idxSubject,nBl) = linewidth(datafid, SelProcValues, bandwidth);
        amplitudeArray(idxSubject,nBl) = max(abs(dataSpecReal(peakRange(1):peakRange(2))));
    end
end

if exist('excludeSubjects')
    n = length(subjects);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(subjects(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    fwhmArray([excludeIdx],:) = [];
    amplitudeArray([excludeIdx],:) = [];
    subjects([excludeIdx],:) = [];
end

if exist('excludeSubjects')
    file2save = strcat('BOLD_',metabType,'_',visualType,BOLDType,woBaseline,'_SlidingAverage_Excluded');
else
    file2save = strcat('BOLD_',metabType,'_',visualType,BOLDType,woBaseline,'_SlidingAverage');
end
BOLD.fwhm = fwhmArray; BOLD.amplitude = amplitudeArray;
eval([file2save '= BOLD']);
exportFileMat = [OutputFilesPathLocal, strcat(file2save,'.mat')];
save(exportFileMat,file2save)

x = linspace(16*5,(320*5-16*5),37)/60;
figure
hold on
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 15, 30]);
for idxSubject = 1:length(subjects)
    plot(x,(fwhmArray(idxSubject,:)+(idxSubject-1)*1.3),'o-')
end
title ([num2str(file2save),' fwhm'], 'Interpreter', 'none');
xlabel 't / min';
yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]);
fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhm.fig'));
savefig(fig2save)

figure
hold on
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 15, 30]);
for idxSubject = 1:length(subjects)
    if strcmp(metabType(1:2),'Cr')
        plot(x,(amplitudeArray(idxSubject,:)+(idxSubject-1)*0.001),'o-')
    elseif strcmp(metabType(1:3),'NAA')
        plot(x,(amplitudeArray(idxSubject,:)+(idxSubject-1)*0.002),'o-')
    else strcmp(metabType(1:3),'Wat')
        plot(x,(amplitudeArray(idxSubject,:)+(idxSubject-1)*2),'o-')
    end
end
title ([num2str(file2save),' amplitude'], 'Interpreter', 'none');
xlabel 't / min';
yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]);
fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_amplitude.fig'));
savefig(fig2save)
close all

%plot mean, std of BOLD
n = length(fwhmArray);
for indexOfBlock = 1:length(AvNo)
    fwhmArray_mean{1,indexOfBlock} = mean(fwhmArray(:,indexOfBlock));
    fwhmArray_std{1,indexOfBlock} = std(fwhmArray(:,indexOfBlock));
    amplitudeArray_mean{1,indexOfBlock} = mean(amplitudeArray(:,indexOfBlock));
    amplitudeArray_std{1,indexOfBlock} = std(amplitudeArray(:,indexOfBlock));
end

figure
subplot(2,1,1)
hold on
x = linspace(16*5,(320*5-16*5),37)/60;
errorbar(x,cell2mat(fwhmArray_mean),cell2mat(fwhmArray_std))
title ([num2str(file2save),' fwhm, mean'], 'Interpreter', 'none');
ylabel 'FWHM / Hz';

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]);

subplot(2,1,2)
hold on
errorbar(x,cell2mat(amplitudeArray_mean),cell2mat(amplitudeArray_std))
title ([num2str(file2save),' amplitude, mean'], 'Interpreter', 'none');
xlabel 't / min';ylabel 'Amplitude / a.u.';

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]);
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 12, 18]);
fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_Mean.fig'));
savefig(fig2save)
end

function CorrBOLDCategorical(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end

for idxmetab = 1:length(metabType)
    if strcmp(metabType{idxmetab}(1:2),'Wa')
        impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType);
    else
        impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,woBaseline);
    end
    impFileMat = strcat(OutputFilesPathLocal,impName,'.mat');
    BOLDData = load(impFileMat);
    fields = fieldnames(BOLDData);
    BOLDDataXY(idxmetab) = BOLDData.(fields{1});
end
file2save = strcat('Corr_',metabType{1},'_',metabType{2},'_',visualType,BOLDType,woBaseline);
figure
subjects = ones(130,1);
for i = 1:13
    subjects((1+(i-1)*10):(10+(i-1)*10)) = subjects((1+(i-1)*10):(10+(i-1)*10)).*i;
end
fwhmTab = table(BOLDDataXY(1).fwhm(:),BOLDDataXY(2).fwhm(:),subjects);
fwhmTab.subjects = categorical(fwhmTab.subjects);
fit = fitlm(fwhmTab,'Var2~Var1*subjects');
w = linspace(min(BOLDDataXY(1).fwhm(:)),max(BOLDDataXY(1).fwhm(:)));

plot(BOLDDataXY(1).fwhm,BOLDDataXY(2).fwhm,'o')
xlabel(['FWHM ',num2str(metabType{1}), ' / Hz'])
ylabel(['FWHM ',num2str(metabType{2}), ' / Hz'])
title(['Correlation FWHM ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
for i = 1:size(BOLDDataXY(1).fwhm,1)
    line(w,feval(fit,w,num2str(i)))
end
fig2saveFWHM = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhm.fig'));
savefig(fig2saveFWHM)

figure
fwhmAmp = table(BOLDDataXY(1).amplitude(:),BOLDDataXY(2).amplitude(:),subjects);
fwhmAmp.subjects = categorical(fwhmAmp.subjects);
fit = fitlm(fwhmAmp,'Var2~Var1*subjects');
w = linspace(min(BOLDDataXY(1).amplitude(:)),max(BOLDDataXY(1).amplitude(:)));

plot(BOLDDataXY(1).amplitude,BOLDDataXY(2).amplitude,'o')
xlabel(['Amplitude ',num2str(metabType{1}), ' / a.u.'])
ylabel(['Amplitude ',num2str(metabType{2}), ' / a.u.'])
title(['Correlation Amplitude ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
for i = 1:size(BOLDDataXY(1).fwhm,1)
    line(w,feval(fit,w,num2str(i)))
end
fig2saveAmplitude = fullfile(OutputFilesPathLocal, strcat(file2save,'_amp.fig'));
savefig(fig2saveAmplitude)

% close all
end