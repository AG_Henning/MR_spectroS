function doFittingAndT2Calculations_UF(fitAllSubjects)

pathName = 'DF data path';
sampleCriteria = {'Output_UF', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
fitAllSubjects = true;
orderTEs = {'24' '32' '40' '52' '60'};
subjects = {'1658';'1717';'2017';'3373';'3490';'5771';'6249';'6971'; '7338'; '7782';'9810'};

extensionTable = '.table';
extensionCoord = '.coord';
defaultTE = 'TE24';
defaultSubject = 'XXXX';
defaultLCModelUser = 'tborbath';

useFinal = true;
moietyFit = true;
if moietyFit == false
    orderTEs = {'24'};
end

exportSuffix = '';

if fitAllSubjects
    controlFilesBase = 'fitsettings_Subject_XXXX_';
    controlFilesBaseTE = strcat(controlFilesBase, 'TE');
    %%UF fitsettings
    controlFilesBaseSuffix = '_uf_test.control';
    controlFilesBaseSuffix = '_uf_Ref7ppm_moiety.control';
    controlFilesBaseSuffix = '_Metabolite_testing.control';
    if moietyFit
        controlFilesBaseSuffix = '_Metabolite_final.control';
%         controlFilesBaseSuffix = '_Metabolite_deGraaf.control';
    else
        controlFilesBaseSuffix = '_Metabolite_final_abs_quantif.control';
        controlFilesBaseSuffix = '_Metabolite_deGraaf_abs_quantif.control';
        exportSuffix = '_abs_quantif';
    end

    outputFileNameBase = strcat(defaultSubject, '_TE');
else
    controlFilesBase = 'fitsettings_Summed_Averaged_';
    %%UF fitsettings
    controlFilesBaseSuffix = '_uf_test.control';
    outputFileNameBase = 'Summed_Averaged_TE';
    
%     controlFilesBaseSuffix = '_uf_Ref7ppm.control';
%     outputFileNameBase = 'Summed_Averaged_Ref_7ppm_TE';

    controlFilesBaseSuffix = '_uf_Ref7ppm_moiety.control';
    outputFileNameBase = 'Summed_Averaged_Ref_7ppm_moiety_TE';
    
    controlFilesBase = 'fitsettings_testing_UF_Summed_Averaged_';
%     controlFilesBase = 'fitsettings_deGraaf_UF_Summed_Averaged_';
    controlFilesBaseSuffix = '_moiety.control';
    outputFileNameBase = 'Summed_Averaged_final_UF_moiety_TE';
    if useFinal
        if moietyFit
            controlFilesBase = 'fitsettings_final_UF_Summed_Averaged_';
            controlFilesBaseSuffix = '_moiety.control';
            outputFileNameBase = 'Summed_Averaged_final_UF_moiety_TE';
        else
            controlFilesBase = 'fitsettings_final_UF_Summed_Averaged_';
            controlFilesBaseSuffix = '_abs_quantif.control';
            outputFileNameBase = 'Summed_Averaged_final_UF_abs_quantif_TE';
            exportSuffix = '_abs_quantif';
        end
    end
    controlFilesBaseTE = strcat(controlFilesBase, 'TE');
    
end

%% file paths setup
controlFilesPathRemote = '/Desktop/DATA_df/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/DATA_df/Output_UF/';
LCModelOutputPath = [localFilePathBase, 'Output_UF/'];
defaultControlFile = strcat(controlFilesBase, 'Default', controlFilesBaseSuffix);


if fitAllSubjects
    subjectsPath = strcat(subjects, '/');
else
    subjects = {'Summed'};
    subjectsPath = {''};
end

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath);
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath);
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath);
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath);

%% basic configurations
LCModelTableFits = strcat(outputFileNameBase, orderTEs, extensionTable);
LCModelTableFitsCoord = strcat(outputFileNameBase, orderTEs, extensionCoord);
LCModelOutputFiles = strcat(outputFileNameBase, orderTEs);
LCModelControlFiles = strcat(controlFilesBaseTE, orderTEs, controlFilesBaseSuffix);

numberOfTes = length(orderTEs);
numberOfSubjects = length(subjects);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

%% create the control file series
if fitAllSubjects
    createLCModelConfigsTEseries('Met', controlFilesPathLocal, defaultControlFile, orderTEs, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser, subjects, defaultSubject);
else
    % call the Configuration file creator without subject configuration
    createLCModelConfigsTEseries('Met', controlFilesPathLocal, defaultControlFile, orderTEs, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser);
end

%% load the table with the deleted averages
% load([localFilePathBase, 'tableDeletedAverages.mat'], 'tableDeletedAverages');
tableDeletedAverages = [];

%% do the actual LCModel fitting and T2 calculation
parfor indexCurrentSubject = 1:numberOfSubjects
    %% do the LCModel fitting
    LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    if fitAllSubjects
        currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
    else
        currentControlFiles = LCModelControlFiles;
        currentOutputFiles = LCModelOutputFiles;
    end
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
    %% do the actual LCModel fitting and T2 calculation
end

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

for indexCurrentSubject = 1:numberOfSubjects
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    if fitAllSubjects
        currentLCModelTableFits = strrep(LCModelTableFits, defaultSubject, subjects{indexCurrentSubject});
        currentLCModelTableFitsCoord = strrep(LCModelTableFitsCoord, defaultSubject, subjects{indexCurrentSubject});
    else
        currentLCModelTableFits = LCModelTableFits;
        currentLCModelTableFitsCoord = LCModelTableFitsCoord;
    end
    
    %%extract the TEs to eliminate
    eliminateTEs = {};
    if ~isempty(tableDeletedAverages)
        indexTableSubject = find(strcmp(subjects{indexCurrentSubject}, tableDeletedAverages(:,1)), 1);
        %if the index is empty, then we don't eliminate any TE
        if (~isempty(indexTableSubject))
            % we assume that
            for indexTableDeleteTE = 1:numberOfTes
                indexCurrentTE = tableDeletedAverages{indexTableSubject+indexTableDeleteTE-1,2};
                if strcmp(indexCurrentTE,['TE' orderTEs{indexTableDeleteTE}]) == 1
                    deletedAverages = tableDeletedAverages(indexTableSubject+indexTableDeleteTE-1,3:end);
                    deletedAveragesNum = cell2mat(deletedAverages);
                    if (length(deletedAveragesNum) > 2) %TODO check what is best 2 or 0
                        eliminateTEs{end+1} = orderTEs{indexTableDeleteTE};
                    end
                else
                    warning('Subject %s does not have the matching TEs in the table of deleted averages.',subjects{indexCurrentSubject});
                end
            end
        else
            warning('Subject %s not found in the table of deleted averages.',subjects{indexCurrentSubject});
        end
    end

    %% calculate the available TEs
    availableTEs = {};
    availableLCModelTableFits = {};
    availableLCModelTableFitsCoord = {};
    indexTE = 0;
    for indexCurrentTE = 1:numberOfTes
        foundTE  = find(strcmp(orderTEs{indexCurrentTE}, eliminateTEs), 1);
        if isempty(foundTE)
            indexTE = indexTE +1;
            availableTEs{indexTE} = orderTEs{indexCurrentTE};
            availableLCModelTableFits{indexTE} = currentLCModelTableFits{indexCurrentTE};
            availableLCModelTableFitsCoord{indexTE} = currentLCModelTableFitsCoord{indexCurrentTE};
        end
    end
    numOfAvailableTEs = length(availableTEs);
    %% do the T2 exponential fits
    [tableConcentrations{indexCurrentSubject}, tableT2s{indexCurrentSubject}] = calculate_T2(availableLCModelTableFits, ...
        subjectsLCModelOutputPath{indexCurrentSubject}, false, availableTEs, 'UF');
    
    %% mean Rsquared
    Rsquares = str2double(tableT2s{indexCurrentSubject}(3,2:end));
    T2_values = str2double(tableT2s{indexCurrentSubject}(2,2:end));
    numNegT2s = sum(T2_values<0);
    meanRsquared =  mean(Rsquares(T2_values>0));
    
    %% mean CRLB
    CRLBs = cell2mat((tableConcentrations{indexCurrentSubject}(2:numberOfTes + 1,3:3:end)));
    meanCrlbTEs = mean(CRLBs,2, 'omitnan');
    meanCrlb = mean(meanCrlbTEs);
    
    %% T2 of NAA
    metNames = tableT2s{indexCurrentSubject}(1,2:end);
    indexes = strfind(metNames,'NAA');
%     indexCr = find(not(cellfun('isempty', indexes))) + 1;
%     T2NAA = tableT2s{indexCurrentSubject}(2,indexCr);
    
    %% display means
    table(1,1) = {'TE'};
    table(1,2:numOfAvailableTEs+1) = availableTEs(1:end);
    table(2,1) = {'CRLB'};
    table(2,2:numOfAvailableTEs+1) = num2cell(meanCrlbTEs(1:end));
    table(3,1) = {'meanCRLB'};
    table(3,2) = num2cell(meanCrlb);
    table(4,1) = {'meanR2'};
    table(4,2) = num2cell(meanRsquared);
    table(5,1) = {'num Neg T2s'};
    table(5,2) = num2cell(numNegT2s);
    display(table)
    
    %% save table to file
    exportFileXlsx = [LCModelOutputPath ,'Quantification_Summaries.xlsx'];
    startIndex = 1+ 7 *(indexCurrentSubject-1);
    cellPositionTitle = ['A' num2str(startIndex)];
    xlswrite(exportFileXlsx, {['Subject: ' subjects{indexCurrentSubject}]}, 1, cellPositionTitle);
    cellPosition = ['A' num2str(startIndex + 1)];
    xlswrite(exportFileXlsx, table, 1, cellPosition);
    
    %% calculate FWHMs
    if indexCurrentSubject == 1
        [fwhms, orderMetFWHM] =calculate_FWHM(currentLCModelTableFitsCoord, subjectsLCModelOutputPath{indexCurrentSubject}, false, 'Met');
        tableFWHM = zeros(numberOfSubjects, size(fwhms,1), size(fwhms,2));
        tableFWHM(indexCurrentSubject,:,:) = fwhms;
    else
        tableFWHM(indexCurrentSubject,:,:) = calculate_FWHM(currentLCModelTableFitsCoord, subjectsLCModelOutputPath{indexCurrentSubject}, false, 'Met');
    end
end


% [tableT2sAll] = calculate_T2s_all(tableConcentrations, availableTEs, LCModelOutputPath, 'UF', false);

if fitAllSubjects
    fileNameSuffix = ['Subjects' exportSuffix];
else
    fileNameSuffix = ['Summed' exportSuffix];
end
tableFWHM_name = 'tableFWHM';
tableT2s_name = 'tableT2s';
orderMet_name = 'orderMetFWHM';
tableConcentrations_name = 'tableConcentrations';
tableFWHM_nameSuffixed = [tableFWHM_name fileNameSuffix];
tableT2s_nameSuffixed = [tableT2s_name fileNameSuffix];
orderMet_nameSuffixed = [orderMet_name fileNameSuffix];
tableConcentrations_nameSuffixed = [tableConcentrations_name fileNameSuffix];
eval( [tableFWHM_nameSuffixed, '=',  tableFWHM_name]);
eval( [tableT2s_nameSuffixed, '=',  tableT2s_name]);
eval( [orderMet_nameSuffixed, '=',  orderMet_name]);
eval( [tableConcentrations_nameSuffixed, '=',  tableConcentrations_name]);
save([LCModelOutputPath, tableFWHM_nameSuffixed, '.mat'], tableFWHM_nameSuffixed, orderMet_nameSuffixed);
save([LCModelOutputPath, tableT2s_nameSuffixed, '.mat'], tableT2s_nameSuffixed)
save([LCModelOutputPath, tableConcentrations_nameSuffixed, '.mat'], tableConcentrations_nameSuffixed)
end