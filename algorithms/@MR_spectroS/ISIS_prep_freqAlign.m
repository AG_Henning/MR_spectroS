function [this] = ISIS_prep_freqAlign(this, this2, weights)

%Coil combination
oldData = this.Data{1};
oldData = squeeze(oldData);
nSamples      = size(this.Data{1},this.kx_dim);
nAverages     = size(this.Data{1},this.meas_dim); 

comb = zeros(nSamples,nAverages);
for iAverage = 1:nAverages
    comb(:,iAverage) = oldData(:,:,iAverage)*weights;
end
this.Data{1}                                = double(permute(comb,[1,3,4,5,6,7,8,9,10,11,12,2]));

%% pc0 correction, cc vor phase
phases = this2.Parameter.PhaseCorr31PSettings.phases;

oldData                 = this.Data{1};
newDataSize             = size(oldData);
newData                 = zeros(newDataSize);
nChannels = size(this.Data{1},this.coil_dim);
if length(phases) == 1
    for iAverage = 1:nAverages
        for indexOfChannel=1:nChannels
            currentDataToPhase =oldData(:,1,1,indexOfChannel,1,1,1,1,1,1,1,iAverage);
            newData(:,1,1,indexOfChannel,1,1,1,1,1,1,1,iAverage) = currentDataToPhase .*exp(1i*-phases *pi/180);
        end
    end
else
    for iAverage = 1:nAverages
        for indexOfChannel=1:nChannels
            currentDataToPhase =oldData(:,1,1,indexOfChannel,1,1,1,1,1,1,1,iAverage);
            newData(:,1,1,indexOfChannel,1,1,1,1,1,1,1,iAverage) = currentDataToPhase .*exp(1i*-phases(indexOfChannel) *pi/180);
        end
    end
end

this.Data{1} = newData;

%% apply the previously calculated values
this.Parameter.PhaseCorrSettings = this2.Parameter.PhaseCorrSettings;
this = this.PhaseCorrection;


%% truncate data

trunc_ms = 100;
truncPoint = floor( trunc_ms/(this.Parameter.Headers.DwellTimeSig_ns*1e-6));
this = this.Truncate(truncPoint);

end