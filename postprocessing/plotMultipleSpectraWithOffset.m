[FileName,PathName] = uigetfile('*.coord','Please select .coord files from LCModel','Multiselect','on');
FitMethod = strsplit(PathName,'\\');
FitMethod = FitMethod(length(FitMethod)-1);
%
if ~iscell(FileName)
    FileName= {FileName};
end
%
ppm = {};
pData = {};
fData = {};
bData = {};
mmData = {};
rData = {};
% close all;
figure;
offset = 0.2;
maxValueFirstSample = 0;
for index =1:length(FileName)
    %[c1 c2 c3 c4] = textread(strcat(PathName,FileName{i}),'%s %s %s %s');
    c          = textread(strcat(PathName,FileName{index}),'%s');
    nOfPoints  = find(strcmp(c ,'points'),1);
    nOfPoints  = str2num (c{nOfPoints-1});
    
    %ppm axis
    indexOfMet   = find(strcmp(c,'ppm-axis'))+3;
    endOfMet     = nOfPoints+indexOfMet-1;
    ppm{index}= str2double( c(indexOfMet:endOfMet,1));
    
    %phaseData
    indexOfMet   = find(strcmp(c,'phased'))+4;
    endOfMet     = nOfPoints+indexOfMet-1;
    pData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %background
    indexOfMet   = find(strcmp(c,'background'))+3;
    endOfMet     = nOfPoints+indexOfMet-1;
    bData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %Macromolecular baseline
    indexOfMet   = find(strcmp(c,'Leu'),1,'last')+3;
    endOfMet     = nOfPoints+indexOfMet-1;
    mmData{index}= str2double( c(indexOfMet:endOfMet,1));
    
    if(index == 1)
        maxValueFirstSample = max(pData{1});
    end
    
    %plotting
    hold on
    p = plot(ppm{index}, pData{index} / maxValueFirstSample + (length(FileName)-index+1) * offset - 0.2);
    
    set(p(1),'LineWidth',1);
    set(gca,'fontsize',14);
end


xlim([0.5 4.2]);
ylim([-0.15 length(FileName)*offset+ 0.9]);
set(gca,'xDir','reverse')
set(p(1),'LineWidth',1);

set(gca,'ytick',[])
% legend('TE = 24ms','TE = 32ms','TE = 40ms', 'TE = 52ms', 'TE = 60ms');
xlabel('[ppm]');
title('Summed spectra at different echo times')