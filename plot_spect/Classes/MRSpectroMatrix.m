classdef MRSpectroMatrix <hgsetget
    
    properties
        Matrix
        n
        currentPosition
        occupiedPosition
    end
    
    methods
        function this=MRSpectroMatrix(nInput)
           
            if nargin <1
                
                nInput     = 3; %default
            end
            
            set(this,'n',nInput);
            
                
            this.Matrix           = [];
            mrSpectroObject       = MR_spectroS(0);
            for i=1:nInput+1   %n+1 position is a position for temporary save   
                this.Matrix       = [this.Matrix mrSpectroObject];
            end
            
            set(this,'currentPosition',0);
            set(this,'occupiedPosition',0);
            
        end
          
        function Save(this,MRSpectroObj)
            
            occupiedPosition = get(this,'occupiedPosition');
            currentPosition  = get(this,'currentPosition');
            n                = get(this,'n');
            
            
            if occupiedPosition < n
                
                occupiedPosition              = occupiedPosition +1;
                currentPosition               = occupiedPosition ;
                
                set(this,'occupiedPosition',occupiedPosition);
                set(this,'currentPosition',currentPosition);
                this.Matrix(occupiedPosition) = MRSpectroObj;
                
            else
                
                
                
                for i=2:n-1
                    this.Matrix(i) = this.Matrix(i+1);
                end
                    this.Matrix(n) = MRSpectroObj;
                    
                    occupiedPosition = n;
                    currentPosition  = n;
                    set(this,'occupiedPosition',occupiedPosition);
                    set(this,'currentPosition',currentPosition);
            end
            
        end
        
        
        
        function this =getCurrent(this)
            
           currentPosition = get(this,'currentPosition');
           this            = this.Matrix(currentPosition);
            
        end
        
        function  Reset(this) 
                set(this,'currentPosition',1);
        end
        
        
        function Back(this)
            
            currentPosition = get(this,'currentPosition');
   
            
            if currentPosition >1    
                newPosition     = currentPosition -1;
            else
                 newPosition     = currentPosition  ; 
            end
                
                set(this,'currentPosition',newPosition);
        end
        
        
       
        
        function Forward(this)
            
            currentPosition   = get(this,'currentPosition');
            occupiedPosition  = get(this,'occupiedPosition');
            
            
            if currentPosition < occupiedPosition 
                
                newPosition     = currentPosition +1;
            else
                newPosition     = currentPosition   ;
            end
            
            set(this,'currentPosition',newPosition);
        end
        
        
        function setTemporary(this,MRSpectroObj)
            
          n = get(this,'n');
          this.Matrix(n+1) = MRSpectroObj;
               
        end
        
        
         function this =getTemporary(this)
            
          n      =  get(this,'n') ;
          this   =  this.Matrix(n+1);
                      
        end
        
    end
    
end

