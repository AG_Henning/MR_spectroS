function this = ReverseMC(this)

oldData                = this.Data{1};
nPoints                 = size(this.Data{1}, this.kx_dim);
nCoils                 = size(this.Data{1}, this.coil_dim);
nAverages              = size(this.Data{1}, this. meas_dim);
bandwidth = this.Parameter.Headers.Bandwidth_Hz;

newData                            = zeros(size(oldData));

% inversion pulse frequency offset
% The frequency offset of the inversion pulse is set to 350 Hz. 
% This leads to the steep inversion pulse edge to start at 200 Hz
invPulsFreqOffset = 200; % Not used, see comments below
% The metabolite cycling inverts during one average the upfield metabolite
% spectra starting 200Hz as the inversion pulse dictates, while keeping the
% water peak and the downfield non-inverted.
% In the paired average the downfield metabolite spectra starting -200Hz is
% inverted, while the upfield and the water is non-inverted.
% The inversion pulse has furthermore a transition bandwidth of 10s of Hz,
% where a transition of Mz = 0 is traversed.
% The spectra at acquisition should have the following magnetizations,
% where | stands for the +-200Hz transition point
% Acquisition 1:
% Mz:  ++++++++++++ | +++++ | ------------
%      down-field   |  H2O  | up-field
% Acquisition 2:
% Mz:  ------------ | +++++ | ++++++++++++
%      down-field   |  H2O  | up-field
% Using the MC-Recon function ideally one should get the following
% magnetizations:
% Mix 1 (difference spectra):
% Mz:  ------------ | 00000 | ++++++++++++
%      down-field   |  H2O  | up-field
% Mix 2:
% Mz:  000000000000 | +++++ | 000000000000
%      down-field   |  H2O  | up-field
% Hence there are two possible options to generate an positive metabolite
% spectra, by inverting at the pulse inversion point at -200 Hz or at the
% water resonance symmetrically at 0 Hz
% Mix 1 (difference spectra):
% Mz:  ++++++++++++ | 00000 | ++++++++++++
% So here           ^ or^  
%
% I did intensive evaluation and there is no significant difference between
% inverting the spectra at -200Hz or 0Hz (used TE series, 11 subjects etc).
% Hence, out of simplicity I recommend inverting the spectra symmetrically,
% meaning invPulsFreqOffset = 0.
invPulsFreqOffset = 0;
invPulsPoints = floor(invPulsFreqOffset / bandwidth * nPoints);

for indexOfAverage=1:nAverages
  for indexOfChannel=1:nCoils
	 %find the water peak frequency (in points)
     waterMix = 2;
     waterData = oldData(:, 1, 1, indexOfChannel, 1, 1, 1, 1, waterMix, 1, 1, indexOfAverage);
     fftsWater = fftshift(fft(waterData));
	 [~, maxIndex] = max(real(fftsWater));

	 %reverse the spectrum according to the water reference and the inversion pulse frequency offset
	 dataMix = 1;
     metaboliteData = oldData(:, 1, 1, indexOfChannel, 1, 1, 1, 1, dataMix, 1, 1, indexOfAverage);
     ffts        = fftshift(fft(metaboliteData));
	 reversePoint = maxIndex + invPulsPoints ;% was nPoints/2
     ffts(reversePoint:end) = (-1) * ffts(reversePoint:end);
     tmpFID          = ifft(ifftshift(ffts));

	 %1:NCol, 1, 1, 1:NCha, 1, 1, 1, 1, 1, 1:NRep, 1, 1:NSet
     newData(:, 1, 1, indexOfChannel, 1, 1, 1, 1, waterMix, 1, 1, indexOfAverage) = waterData; %keep it unchanged
     newData(:, 1, 1, indexOfChannel, 1, 1, 1, 1, dataMix, 1, 1, indexOfAverage) = tmpFID; %update with the reversed downfield
  end
end

this.Data{1} = newData;