function makeBasis

pathName = 'DF data path';
sampleCriteria = {'Basis_sets'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
orderTEs = {'24' '32' '40' '52' '60'};

defaultTE = '24';
defaultLCModelUser = 'tborbath@mpi.localnet';

final_UF_version = true;
moietyFit = false;
if final_UF_version
    if moietyFit
        makeBasisFilesBase = 'final_sLASER_moiety_UF_';
        makeBasisFilesBase = 'sLASER_7ppm_moiety_UF_';
%         makeBasisFilesBase = 'sLASER_2.4ppm_short_moiety_UF_';
    else
        makeBasisFilesBase = 'final_sLASER_abs_quantif_UF_';
        makeBasisFilesBase = 'sLASER_7ppm_abs_quantif_UF_';
        makeBasisFilesBase = 'sLASER_2.4ppm_abs_quantif_UF_';
    end
else
    makeBasisFilesBase = 'sLASER_moiety_7_ppm_';
    makeBasisFilesBase = 'noCreMMB_sLASER_moiety_UF_';
    makeBasisFilesBase = 'noCreMMB_sLASER_testing_moiety_UF_';
    %DF
    makeBasisFilesBase = 'sLASER_7ppm_moiety_DF_';
end
%%UF fitsettings
makeBasisFilesBaseSuffix = '_make_basis.in';

%% file paths setup

if final_UF_version
    makeBasisFilesPathRemote = '/Desktop/DATA_df/Basis_sets/final_T2_met_MM_paper/';
    makeBasisFilesPathLocal =  [localFilePathBase, 'Basis_sets/final_T2_met_MM_paper/'];
    LCModelBasisOutputFilesPathRemote = '/Desktop/DATA_df/Basis_sets/final_T2_met_MM_paper/';
    LCModelBasisOutputPath = [localFilePathBase, 'Basis_sets/final_T2_met_MM_paper//'];
else
    %UF
    makeBasisFilesPathRemote = '/Desktop/DATA_df/Basis_sets/sLASER_UF/';
    makeBasisFilesPathLocal =  [localFilePathBase, 'Basis_sets/sLASER_UF/'];
    LCModelBasisOutputFilesPathRemote = '/Desktop/DATA_df/Basis_sets/sLASER_UF/';
    LCModelBasisOutputPath = [localFilePathBase, 'Basis_sets/sLASER_UF//'];
    %DF
    makeBasisFilesPathRemote = '/Desktop/DATA_df/Basis_sets/sLASER_DF/';
    makeBasisFilesPathLocal =  [localFilePathBase, 'Basis_sets/sLASER_DF/'];
    LCModelBasisOutputFilesPathRemote = '/Desktop/DATA_df/Basis_sets/sLASER_DF/';
    LCModelBasisOutputPath = [localFilePathBase, 'Basis_sets/sLASER_DF//'];
end
defaultMakeBasisFile = strcat(makeBasisFilesBase, 'Default', makeBasisFilesBaseSuffix);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(makeBasisFilesPathRemote, LCModelBasisOutputFilesPathRemote, LCModelBasisOutputPath);

%% create the make basis file series
[makeBasisFiles, basisFileNames] = createLCModelMakeBasisTEseries(makeBasisFilesPathLocal, defaultMakeBasisFile, orderTEs, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser);

% do LCModel stuff
copyFileLocally = true;
for index = 1 : length(makeBasisFiles)
    currentMakeBasisFile = makeBasisFiles{index};
    currentBasisFileName = basisFileNames{index};
    LCModelCallerInstance.CreateBasisSet(makeBasisFilesPathLocal, makeBasisFilesPathRemote, currentMakeBasisFile, copyFileLocally, currentBasisFileName);
end

fclose all;
end