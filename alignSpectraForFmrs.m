function alignSpectraForFmrs
load('N:\AGHE\To Tamas Borbath\test.mat', 'test')
a = test;
ppmVector = a.getPpmVector;
doHSVD = true;
while doHSVD
    data = squeeze(a.Data{1});
    spectra = fftshift(fft(data,[],1),1);
    
    %first remove all the residual water befor adjusting baseline
    waterPeak = 4.7; %ppm
    searchArea = 0.5; %ppm
    
    ppmMask = ppmVector > waterPeak-searchArea & ppmVector < waterPeak+searchArea;
    spectraMasked = spectra(ppmMask,:,:);
    waterPeakHeight = max(real(spectraMasked));
    waterPeakHeight = squeeze(waterPeakHeight(1,1,:));
    
    %check NAA peak
    naaPeak = 2.008; %ppm
    searchArea = 0.2; %ppm
    
    ppmMask = ppmVector > naaPeak-searchArea & ppmVector < naaPeak+searchArea;
    spectraMasked = spectra(ppmMask,:,:);
    NaaPeakHeight = max(real(spectraMasked));
    NaaPeakHeight = squeeze(NaaPeakHeight(1,1,:));
    
    peakRatio = waterPeakHeight ./ NaaPeakHeight;
    if size(find(peakRatio > 1)) > size(peakRatio)/2
        display('Applying HSVD');
        a.Parameter.HsvdSettings.bound = [-100 100];
        a.Parameter.HsvdSettings.n = size(a.Data{1},1);
        a.Parameter.HsvdSettings.p = 25;
        [~, a] = a.Hsvd;
    else
        doHSVD = false;
    end
    
    fig1 = figure;
    hold on
    for index = 1:size(spectra,3)
        plot(ppmVector, real(spectra(:,1,index)));
    end
    set(gca, 'XDir','reverse');
end

%first remove all the residual water befor adjusting baseline
refPeak1 = 0.948; %ppm
searchArea = 0.05; %ppm

ppmMask = ppmVector > refPeak1-searchArea & ppmVector < refPeak1+searchArea;
spectraMaskedRef1 = spectra(ppmMask,:,:);
refPeak1Height = max(real(spectraMaskedRef1));
refPeak1Height = squeeze(refPeak1Height(1,1,:));

%check NAA peak
refPeak2 = 4.10; %ppm
searchArea = 0.05; %ppm

ppmMask = ppmVector > refPeak2-searchArea & ppmVector < refPeak2+searchArea;
spectraMaskedRef2 = spectra(ppmMask,:,:);
refPeak2Height = max(real(spectraMaskedRef2));
refPeak2Height = squeeze(refPeak2Height(1,1,:));


ppmMask = ppmVector > refPeak1 & ppmVector < refPeak2;

%% % checking if adding/subtracting a simple linear baseline fixes the issue. 
%% It doesn't! Not worth to check extrapolation of the line

% ppmVectorMasked = ppmVector(ppmMask);
% spectraAdjusted = spectra;
% fig2 = figure;
% for index = 1:size(spectra,3)
%     xx = linspace(refPeak1Height(index), refPeak2Height(index), length(ppmVectorMasked));
%     spectraAdjusted(ppmMask,1,index) = spectra(ppmMask,1,index) - xx(:);
%     figure(fig1);
%     hold on
%     plot(ppmVectorMasked, xx);
%     figure(fig2);
%     hold on
%     plot(ppmVector, real(spectraAdjusted(:,1,index)));
% end
% set(gca, 'XDir','reverse');

%% trying phase frequency alignment
oldData =  a.Data{1};
newData = zeros(size(oldData));
dwellTime = a.Parameter.Headers.DwellTimeSig_ns*1e-9;
nAverages = size(a.Data{1}, a.meas_dim);
xFreAlgnMatrix = [];

tmpData =  squeeze(oldData(:,1,1,:,1,1,1,1,1,1,1,:));
referenceFID =  tmpData(:,1);
% One can try specific areas of the spectrum... but it is neither a simple phase problem either.
refPeak1 = 3.7; %ppm
refPeak2 = 4.1; %ppm
ppmMask = ppmVector > refPeak1 & ppmVector < refPeak2;
indecesOfInterest = ppmMask;

f0 = 0;

for indexOfAverage=1:nAverages
    f = fminsearch(@(f) sumOfSquaresFD2( f, referenceFID, tmpData(:,indexOfAverage), dwellTime, indecesOfInterest),[0 f0])
    xFreAlgnMatrix{indexOfAverage}= f;
end

for indexOfAverage=1:nAverages
    selectedData         = oldData(:, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, indexOfAverage);
    [~, tmpFID]          = sumOfSquaresFD2(xFreAlgnMatrix{indexOfAverage}, referenceFID, selectedData, dwellTime, indecesOfInterest);
    %1:NCol, 1, 1, 1:NCha, 1, 1, 1, 1, 1, 1:NRep, 1, 1:NSet
    newData(:, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, indexOfAverage) = tmpFID;
end

a.Data{1} = newData;

spectra = fftshift(fft(squeeze(newData),[],1),1);
figure;
for index = 1:size(spectra,3)
    hold on
    plot(ppmVector, real(spectra(:,1,index)));
end
set(gca, 'XDir','reverse');
end

function [d, signalAligned] = sumOfSquaresFD2( f, refSignal1, signal,dwellTime, indeces )

timeVector     = (0:length(signal)-1)*dwellTime;
signalAligned  = signal.* exp(1i*(f(1) + 2*pi*f(2)*timeVector))';


ffts1        = fftshift(fft(refSignal1));
ffts2        = fftshift(fft(signalAligned));

d            = ffts1(indeces)-ffts2(indeces);
d            = sum(abs(d).^2);


end