function this = FitSpectrum( this, currentControlFile, currentOutputFiles, copyFileLocally)
% Fit a given spectrum by a control file with LCModel
% - currentControlFile: specifies the control file used for the fit. The
% file has to be in the 'controlFilesPathRemote' folder on the remote server
% - currentOutputFiles:  specifies the name of the output files. The
% function checks for all possible output extensions created by LCModel:
% fileExtensions = {'.coord', '.csv', '.ps', '.table'};
% - copyFileLocally: is a boolean flag specifying, whether we will transfer
% the data to the local computer.
%
% % Sample declarations would be
% currentControlFile = 'fitsettings_Summed_Averaged_TE24_Cre_PpmPfeuffer.control';
% currentOutputFiles = 'Summed_Averaged_TE24';
% copyFilesLocally = true;
%
% Author: Tamas Borbath
% version: 20 July 2018
if(nargin  <  3)
    error('Error: FitSpectrum requires minimum 2 input arguments: currentControlFile, currentOutputFiles, copyFileLocally .');
end
if ~(exist('copyFileLocally', 'var'))
    copyFileLocally = false;
end

% store the selected setup
this.currentControlFile = currentControlFile;
this.currentOutputFiles = currentOutputFiles;

this.currentControlFilePath = [this.homePath this.controlFilesPathRemote this.currentControlFile];

remoteFilePath = [this.homePath this.outputFilesPathRemote];
[this.connection, res] = sshfrommatlabissue(this.connection, ['mkdir ' remoteFilePath]);

[this.connection, res] = sshfrommatlabissue(this.connection, [this.homePath this.LCModelPath ' < ' this.currentControlFilePath]);

if (copyFileLocally)
    fileExtensions = {'.coord', '.csv', '.ps', '.table'};
    for extension = fileExtensions
        scptomatlab(this.LCModelUser, this.LCModelIP, this.LCModelPassword, this.outputFilesPathLocal, [this.homePath this.outputFilesPathRemote currentOutputFiles extension{1}])
    end
end

end

