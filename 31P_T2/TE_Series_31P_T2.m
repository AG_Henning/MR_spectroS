function TE_Series_31P_T2()
clear; clc; close all;

%%%%%%%%
% calculates mean and std of raw data per TE


pathName = 'T2 31P Paper Data';
% sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName);

subjects = {'9873';'1090';'3490';'9810';'1658';'2017';'6524';'3009';...
    '6260';'2823';'2455';'4085'};
prefixes = {'2019-09-30_';'2019-10-10_';'2019-10-14_';'2019-10-14_';...
    '2019-10-22_';'2019-11-07_';'2019-11-07_';'2019-11-20_';...
    '2019-11-20_';'2019-12-05_';'2020-07-22_';'2020-07-23_';};
% excludeSubjects = {'5166';'9810';'5269'};
suffixFolder = '_SurfCoil';

%be careful with Pi intra/extra, NADH and bATP!
metaboliteNames = {'PE', 'PC', 'Pi_int', 'GPE', 'GPC', 'PCr', 'gATP', 'aATP', 'NADH'};
centerPpm =       [ 6.77, 6.23, 4.83,     3.51,   2.96,  0,     -2.48, -7.62,  -8.16];

trunc_ms = 90;
TruncToFit = 'noTrunc';        %'noTrunc';   %'Trunc90ms';
numberOfSubjects = length(subjects);
TEs = {'6'; '8'; '11'; '15'; '20'; '30'; '50'; '80'; '150'};  %ms
numberOfTEs = length(TEs);

paths = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    paths{indexSubject} = [localFilePathBase prefixes{indexSubject} subjects{indexSubject} suffixFolder '\'];
end
outputPath = 'D:\PAPER\31P_T2_RelaxationTimes\Data\Summed';

%read MR_SpectroS files of all subjects and TEs, normalize to PCr, write
%into cell array SpecTE
SpecTE = cell(numberOfSubjects,numberOfTEs);    %subjects x TEs
MaxPeak = cell(numberOfSubjects,numberOfTEs);    %subjects x TEs
for indexSubject = 1:numberOfSubjects
    if strcmp(TruncToFit,'Trunc90ms')
        files = ls([paths{indexSubject} '*_Trunc90ms.mat*']);
    elseif strcmp(TruncToFit,'noTrunc')
        files = ls([paths{indexSubject} '*_noTrunc.mat*']);
    end
    for indexTE = 1:numberOfTEs
        temp = strfind(cellstr(files),strcat('TE',num2str(TEs{indexTE}),'ms'));
        indexFile = find(~cellfun(@isempty,temp));
        load([paths{indexSubject} '\' files(indexFile,:)]);     %MR_SpectroS files saved as currentData
        if strcmp(TruncToFit,'Trunc90ms')
            currentData = currentDataTrunc;
        elseif strcmp(TruncToFit,'noTrunc')
            currentData = currentData;
        end
        tempFID = currentData.Data{1};
        tempSpec = real(fftshift(fft(tempFID)));
        tempSpecNorm = tempSpec./max(tempSpec);
        SpecTE{indexSubject,indexTE} = tempSpecNorm;
        MaxPeak{indexSubject,indexTE} = max(tempSpec);
    end
end

%calculate mean and std per TE, scale to mean max peak to see peak height
%decrease with increasing TE
Data_mean = cell(numberOfTEs,1);
Data_std = cell(numberOfTEs,1);
for indexTE = 1:numberOfTEs
    currentData_temp = cell2mat(SpecTE(:,indexTE))';
    currentData_temp = reshape(currentData_temp,4096,numberOfSubjects);
    currentMax_temp = mean(cell2mat(MaxPeak(:,indexTE))');
    Data_mean{indexTE} = mean(currentMax_temp.*currentData_temp,2);
    Data_std{indexTE} = std(currentMax_temp.*currentData_temp');
end


nTimepoints     = size(currentData.Data{1},currentData.kx_dim);
bandwidth       = currentData.Parameter.Headers.Bandwidth_Hz;
freq    = bandwidth/nTimepoints*(-(nTimepoints/2-1):1:nTimepoints/2)';
time    = 1/bandwidth*(0:nTimepoints-1)'*1000;      %ms
scanFreq_MHz = currentData.Parameter.Headers.ScanFrequency/1E6;
x = 1:4096;
x = freq./scanFreq_MHz;

colors = {[0 0.7 0]; [0 0.4 0.8]; [0.8 0 0.8]; [0.8 0.4 0];[0 0.5 0.5]; [0.4 0 0.8]; [1 0 0]; [0 0.6 0.3]; [1 0 0.5];};

%create std shadow
for indexTE = 1:numberOfTEs
    lo = Data_mean{indexTE} - Data_std{indexTE}'; hi = Data_mean{indexTE} + Data_std{indexTE}';
    
%     idx = 1;
    figure
    hp = patch([x; x(end:-1:1); x(1)], [lo; hi(end:-1:1); lo(1)], 'r');
    hold on;
    hl = line(x,Data_mean{indexTE});
    % set(hp, 'facecolor', [1 0.65 0.8], 'edgecolor', [1 0.65 0.8]);
    % set(hp, 'facecolor', [1 0 0],'FaceAlpha',.3, 'edgecolor', [1 0 0],'EdgeAlpha',.2);
    set(hp, 'facecolor', colors{indexTE},'FaceAlpha',.3, 'edgecolor', colors{indexTE},'EdgeAlpha',.2);
    set(hl, 'color', colors{indexTE});
    set(gca,'xdir','reverse')
    set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 40, 20]);
    set(gca, 'fontSize', 40);set(gca, 'FontWeight','bold');
%     if strcmp(TruncToFit,'Trunc90ms')
        axis([-10 8 -0.003 0.06])
%     elseif strcmp(TruncToFit,'noTrunc')
%         axis([-10 8 -0.003 0.06])
%     end
    
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
        lines(i).LineWidth = 1.5;
    end
    
    % % remove top x axis and right y axis
    % set(gca,'box','off')
    % % removes y axes
    % h = gca; h.YAxis.Visible = 'off';
    % % removes all axes
    % axis off ;
    fig2save = fullfile(localFilePathBase, strcat('Output\TE_Series\','TE', num2str(TEs{indexTE}), 'ms_',TruncToFit, '.fig'));
    savefig(fig2save)
end
end