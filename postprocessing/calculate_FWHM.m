function [ tableFWHM, orderMet] = calculate_FWHM( FileNames,PathName, displaySilent, downField_MM_Met)
%CALCULATE_T2 Summary of this function goes here
%   Detailed explanation goes here

if (~exist('FileNames', 'var'))
    [FileNames,PathName] = uigetfile('*.coord','Please select .coord files from LCModel','Multiselect','on');
end

if ~exist('displaySilent', 'var')
    displaySilent = false;
end


if ~exist('downField_MM_Met', 'var')
    downField_MM_Met = 'MM';
end

switch downField_MM_Met
    case 'DF'
        keyMM_ppmNaming = {'DF58' 'DF60' 'DF61' 'DF68' 'DF70' 'DF73' 'DF75' 'DF82' 'DF83' 'DF835' 'DF85' 'NAAB'      'hCs' 'NAA_DF' 'NAD' 'Cr'};%'hist' 
        orderMet =        {'DF58';'DF60';'DF61';'DF68';'DF70';'DF73';'DF75';'DF82';'DF83';'DF835';'DF85';'NAA Broad';'hCs';'NAA'; 'NAD'; 'Cr'};%;'hist'
    case 'MM'
%         keyMM_ppmNaming = {'MM09'     'MM12'     'MM14'     'MM17'     'MM20'     'MM22'     'MM26'     'MM27' ...
%             'MM30'     'MM32'     'MM36'     'MM37'     'MM38'     'Cr39'    'Cr30'    'NAA'};
%         orderMet = {       'M_{0.92}';'M_{1.21}';'M_{1.39}';'M_{1.67}';'M_{2.04}';'M_{2.26}';'M_{2.56}';'M_{2.70}';...
%             'M_{2.99}';'M_{3.21}';'M_{3.62}';'M_{3.75}';'M_{3.86}'; 'tCr(CH_2)'; 'tCr(CH_3)'; 'NAA'};  

%           keyMM_ppmNaming = {'MM09'     'MM12'     'MM14'     'MM17'     'MM20'     'MM22'     'MM26'     'MM27' ...
%             'MM30'     'MM32'     'MM36'     'MM37'     'MM38'    'NAA'    'Creat'      'Cho'     'Glu'    'Tau'   'Glycin'    'PCr'};
%         orderMet = {       'M_{0.92}';'M_{1.21}';'M_{1.39}';'M_{1.67}';'M_{2.04}';'M_{2.26}';'M_{2.56}';'M_{2.70}';...
%             'M_{2.99}';'M_{3.21}';'M_{3.62}';'M_{3.75}';'M_{3.86}'; 'NAA'; 'tCr(CH_3)'; 'tCho'; 'Glu'; 'Tau'; 'Glyc'; 'tCr(CH_2)'}; 

        keyMM_ppmNaming = {'MM09'     'MM12'     'MM14'     'MM17'     'MM20'     'MM22'     'MM26'     'MM27' ...
            'MM30'     'MM32'     'MM36'     'MM37'     'MM38'   'MM39'   'Cr39'    'NAA'    'NAA_as'    'Cho'    'mI'};
        orderMet = {       'M_{0.92}';'M_{1.21}';'M_{1.39}';'M_{1.67}';'M_{2.04}';'M_{2.26}';'M_{2.56}';'M_{2.70}';...
            'M_{2.99}';'M_{3.21}';'M_{3.62}';'M_{3.75}';'M_{3.86}'; 'M_{4.03}'; 'tCr(CH_2)'; 'NAA'; 'NAA_{asp}';'tCho'; 'mI'}  
        
    case 'MM WM'
        keyMM_ppmNaming = {'MM09'     'MM12'     'MM14'     'MM17'     'MM20'     'MM22'     'MM26'     'MM27' ...
            'MM30'     'MM32'     'MM36'     'MM37'     'MM38'     'MM40'     'Cre'};
        orderMet = {       'M_{0.92}';'M_{1.21}';'M_{1.39}';'M_{1.67}';'M_{2.04}';'M_{2.26}';'M_{2.56}';'M_{2.70}';...
            'M_{2.99}';'M_{3.21}';'M_{3.62}';'M_{3.75}';'M_{3.86}';'M_{4.03}';'tCr(CH_2)'};
    case 'Met'
        keyMM_ppmNaming = {'Cr' 'Cr_CH2' 'NAA_ac' 'tCho_P' 'Scyllo'};
        orderMet = {'Cr-CH3';'Cr-CH2';'NAA-ace';'tCho_PE';'Scyllo';};
%         keyMM_ppmNaming = {'Cr' 'Cr_CH2' 'NAA' 'PCh' 'Scy'};
%         orderMet = {'Cr-CH3';'Cr-CH2';'NAA';'PCh';'Scy';};
    otherwise
end

mapMM_Names = containers.Map(keyMM_ppmNaming,orderMet);
numberOfMet = length(orderMet);

%threshold for crlbs
threshold_CRLB = 900;
FontSize = 14;
LineWidth = 2.5;
ppmEnd = 4.2;
ppmStart = 0.2;
interpolationFactor = 20;

%plotting offsets
offsetMetabolite = 0.085;
offsetResidual = offsetMetabolite * (numberOfMet+1);
offsetBaseline = offsetResidual + 0.042;

%% do the absolute quantification on all files
if ~iscell(FileNames)
    FileNames= {FileNames};
end

ppmVector = {};
pData = {};
fData = {};
bData = {};
rData = {};
close all;
tableFWHM = zeros(length(FileNames), numberOfMet);

for index =1:length(FileNames)
    figure(index);
    %[c1 c2 c3 c4] = textread(strcat(PathName,FileName{i}),'%s %s %s %s');
    
    [nOfPoints, ppmVector{index}, pData{index}, fData{index}, bData{index}, rData{index}, ...
        metaboliteNames, metaboliteData, tableConcentrations{index}, ppmStart, ppmEnd, scanFrequency, ~, ppmGap] = ...
        extractFits(PathName,FileNames{index});
    
    %scaling calculation
    scale = max(pData{1});
    
    %plotting
    hold on
    p = plot(ppmVector{index}, (pData{index} - bData{index}) ./ scale, ...
        ppmVector{index},(fData{index} - bData{index}) ./ scale, ...
        ppmVector{index},bData{index} ./ scale - offsetBaseline, ...
        ppmVector{index},rData{index} ./ scale - offsetResidual);
    text(0.47,0.05,'Data + Fit', 'FontSize', FontSize);
    if strcmp(downField_MM_Met,'Met')
        text(0.47,-0.03,'MM', 'FontSize', FontSize);
    end
    text(0.47,-offsetResidual,'Residual', 'FontSize', FontSize);
    text(0.47,-offsetBaseline-0.06,'Baseline', 'FontSize', FontSize);
    set(gca,'xDir','reverse')  
    if strcmp(downField_MM_Met,'DF')
        xlim([5.5 9.4]);
    else
        xlim([0.5 4.02]);
    end
    for plots = 1:length(p)
        set(p(plots),'LineWidth',LineWidth);
    end
    
    for indexMetabolite = 1:numberOfMet
        %evaluate each metabolite
        indexCurrentMetabolite = find(strcmp(metaboliteNames, keyMM_ppmNaming{indexMetabolite}));
        if ~isempty(indexCurrentMetabolite)
            metaboliteSpectrum = metaboliteData{indexCurrentMetabolite}.metaboliteSpectrum;
        else
            metaboliteSpectrum = zeros(length(ppmVector{index}),1);
        end
        
        metaboliteSpectrum(1) = 0;
        pMetabolite = plot(ppmVector{index}, metaboliteSpectrum ./ scale - indexMetabolite * offsetMetabolite);
        text(0.47, -indexMetabolite * offsetMetabolite,orderMet{indexMetabolite}, 'FontSize', FontSize);
        set(pMetabolite,'LineWidth',LineWidth);
                    
        if strcmp(downField_MM_Met,'DF')
            switch keyMM_ppmNaming{indexMetabolite}
                case 'Cr'
                    % 3.12 ppm is where upfield starts in fit config
                    indexDF = find((ppmVector{index} < 3.12), 1, 'first');
                    metaboliteSpectrum(1:indexDF) = 0;
                case 'hCs'
                    searchArea = 0.2;
                    % 8.08 ppm peak
                    indexStart  = find((ppmVector{index} > 8.08+searchArea), 1, 'last');
                    indexEnd    = find((ppmVector{index} < 8.08-searchArea), 1, 'first');
                    metaboliteSpectrum(1:indexStart) = 0;
                    metaboliteSpectrum(indexEnd:end) = 0;
                case 'ATP'
                    searchArea = 0.2;
                    % 8.514 ppm peak
                    indexStart  = find((ppmVector{index} > 8.514+searchArea), 1, 'last');
                    indexEnd    = find((ppmVector{index} < 8.514-searchArea), 1, 'first');
                    metaboliteSpectrum(1:indexStart) = 0;
                    metaboliteSpectrum(indexEnd:end) = 0;
                case 'hist'
                    searchArea = 0.3;
                    % 7.06 ppm peak
                    indexStart  = find((ppmVector{index} > 7.06+searchArea), 1, 'last');
                    indexEnd    = find((ppmVector{index} < 7.06-searchArea), 1, 'first');
                    metaboliteSpectrum(1:indexStart) = 0;
                    metaboliteSpectrum(indexEnd:end) = 0;
                case 'NAD'
                    NAD_peak = 9.334; %H2 behaves like a singlet [de Graaf, NMRB 2014] % 8.415 singlet adenine, 9.158 multiplet nicotinamide
                    indexStart  = find((ppmVector{index} > NAD_peak+0.1), 1, 'last');
                    indexEnd    = find((ppmVector{index} < NAD_peak-0.1), 1, 'first');
                    metaboliteSpectrum(1:indexStart) = 0;
                    metaboliteSpectrum(indexEnd:end) = 0;
                otherwise
                    indexDF = find((ppmVector{index} < 5.6), 1, 'first');
                    metaboliteSpectrum(indexDF:end) = 0;
            end
        else % assuming no downfield peaks       
            if ~isempty(ppmGap)
                % make the downfield 0, if there was a ppmGap
                indexDF = find((ppmVector{index} < ppmGap), 1, 'first');
                metaboliteSpectrum(1:indexDF) = 0;
            end
        end
        
        dataPoints = length(metaboliteSpectrum);
        [fwhm, warningFlag] = fwhm_Hz(metaboliteSpectrum(2:end), ppmStart, ppmEnd, dataPoints, scanFrequency, interpolationFactor);
        if(warningFlag == true)
            warning('Metabolite: %s, in File: %s' , keyMM_ppmNaming{indexMetabolite}, FileNames{index})
        end
        tableFWHM(index,indexMetabolite) = fwhm;
    end
    

    xlabel('\delta [ppm]');
    ylim([-1.9 1.2]);
    set(gca,'xDir','reverse')
    set(gca,'ytick',[]);
    
    title('Sample spectrum with fitted metabolites')
    set(gca,'fontsize',FontSize);
    clear   nOfPoints indexOfMet i
end



%% save table to file
exportFileXlsx = [PathName ,'FWHM_Results.xlsx'];
xlswrite(exportFileXlsx, {PathName}, 1, 'A1');
cellPosition = 'A3';
xlswrite(exportFileXlsx, FileNames', 1, cellPosition);
cellPosition = 'B2';
xlswrite(exportFileXlsx, orderMet', 1, cellPosition);
cellPosition = 'B3';
xlswrite(exportFileXlsx, tableFWHM, 1, cellPosition);

end

