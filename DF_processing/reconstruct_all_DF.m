function reconstruct_all_DF()

pathName = 'DF data path';
subjects = { ...
    '1658' ...
    '1717' ...
    '2017' ...
    '3373' ...
    '3490' ...
    '5771' ...
    '6249' ...
    '6971' ...
    '7338' ...
    '7782' ...
    '9810' ...
    };

pathBase = pathToDataFolder(pathName, subjects);

TEs = [24; 32; 40; 52; 60];

numberOfSubjects = length(subjects);
paths = cell(1,numberOfSubjects);
for indexSubj = 1:numberOfSubjects
    paths{indexSubj} = [pathBase subjects{indexSubj} '\\'];
end

files_TE24 = cell(1, numberOfSubjects);
files_TE32 = cell(1, numberOfSubjects);
files_TE40 = cell(1, numberOfSubjects);
files_TE52 = cell(1, numberOfSubjects);
files_TE60 = cell(1, numberOfSubjects);
water_files = cell(1, numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    files_TE24{indexSubject} = ls([paths{indexSubject}, '*_df_24*.dat']);
    files_TE32{indexSubject} = ls([paths{indexSubject}, '*_df_32*.dat']);
    files_TE40{indexSubject} = ls([paths{indexSubject}, '*_df_40*.dat']);
    files_TE52{indexSubject} = ls([paths{indexSubject}, '*_df_52*.dat']);
    files_TE60{indexSubject} = ls([paths{indexSubject}, '*_df_60*.dat']);
    water_files{indexSubject} = ls([paths{indexSubject}, '*_scout_wref_*.dat']);
end

addSinglet0ppm = 'Yes';
numberOfTEs = length(TEs);

filesPath = cell(numberOfTEs,numberOfSubjects);
waterFilesPath = cell(1,numberOfSubjects);
files = [files_TE24; files_TE32; files_TE40; files_TE52; files_TE60];
for indexSubj = 1:numberOfSubjects
    for indexTE = 1:numberOfTEs
        filesPath{indexTE, indexSubj} = [paths{indexSubj} files{indexTE, indexSubj}];
    end
    waterFilesPath{indexSubj} = [paths{indexSubj} water_files{indexSubj}];
end

%% load phase correction factors
fileNamePhaseCorrection = 'PhaseCorrectionFromLCM.xlsx';
[~, ~, phaseCorrectionFactors] = xlsread([pathBase fileNamePhaseCorrection]);
% check the phase correction data
for indexSubj = 1:numberOfSubjects
    if (phaseCorrectionFactors{indexSubj+1,1} ~= str2num(subjects{indexSubj}))
        error('The file %s does not contain the subjects and the phase correction elements in the order specified', fileNamePhaseCorrection);
    end
end
    
%% do the actual reconstruction

summedSpectra = cell(numberOfTEs,1);
stats = cell(numberOfSubjects, numberOfTEs);
statsWater = cell(numberOfSubjects, 1);
indexSubjTEs = zeros(numberOfTEs,1);
for indexSubj = 1:numberOfSubjects
    %% water data
    [a, statsWater{indexSubj}] = reconstructWater(waterFilesPath{indexSubj});
    filename = [paths{indexSubj} subjects{indexSubj} '_water.mat'];
    save(filename, 'a');
    a.ExportLcmRaw(paths{indexSubj}, strcat(subjects{indexSubj}, '_water'), addSinglet0ppm);
    %calculate scaling factor the summedSpectra individually according to water peak
    waterData = a.Data{1};
    maxWaterPeak = max(real(fftshift(fft(waterData))));     
    %scale the data for equalizing between subjects
    a.Data{1} = a.Data{1}./ maxWaterPeak;
    %create summed spectra
    if(indexSubj == 1)
        summedWater = a;
        newDataWater= zeros(length(a.Data{1}),1,1,1,1,1,1,1,1,1,1,numberOfSubjects);
    else
        oldDataWater = summedWater.Data{1};
        if (ndims(oldDataWater) ~= 12)
            newDataWater(:,1,1,1,1,1,1,1,1,1,1,1) = oldDataWater;
        else
            newDataWater(:,1,1,1,1,1,1,1,1,1,1,:) = oldDataWater;
        end
        newDataWater(:,1,1,1,1,1,1,1,1,1,1,indexSubj) = a.Data{1};
        summedWater.Data{1} = newDataWater;
    end
    

    %% get the phase values from the table
    phase0 = phaseCorrectionFactors{indexSubj+1,2};
    phase1Hz = phaseCorrectionFactors{indexSubj+1,4}; 
    %% reconstruct data
    for indexTE = 1:numberOfTEs
        [a, stats{indexSubj, indexTE}] = reconstruct_DF(filesPath{indexTE, indexSubj}, phase0, phase1Hz);
        filename = [paths{indexSubj} subjects{indexSubj} '_TE' num2str(TEs(indexTE)) '.mat'];
        save(filename, 'a');
        a.ExportLcmRaw(paths{indexSubj}, strcat(subjects{indexSubj}, '_TE', num2str(TEs(indexTE))), addSinglet0ppm);
        
        %% create the TE series data
        if(indexTE == 1)
            mruiSpectra = a;
            newDataMrui= zeros(length(a.Data{1}),1,1,1,1,1,1,1,2,1,1,numberOfTEs);
        else
            oldData = mruiSpectra.Data{1};
            if (ndims(oldData) ~= 12)
                newDataMrui(:,1,1,1,1,1,1,1,:,1,1,1) = oldData;
            else
                newDataMrui(:,1,1,1,1,1,1,1,:,1,1,:) = oldData;
            end
            newDataMrui(:,1,1,1,1,1,1,1,:,1,1,indexTE) = a.Data{1};
            mruiSpectra.Data{1} = newDataMrui;
        end
            
        %% calculations for subjects summed spectra
        %scale the data for equalizing between subjects
        a.Data{1} = a.Data{1}./ maxWaterPeak;
        
        % eliminate data with too many deleted averages from the summedSpectra
        deletedAverages = a.Parameter.DeleteMovedAveragesSettings.deletedAverages;
        if ~isempty(deletedAverages)
            numDeletedAverages = length(deletedAverages);
        else
            numDeletedAverages = 0;
        end
        
        if numDeletedAverages <= 2 
            indexSubjTEs(indexTE) = indexSubjTEs(indexTE) + 1;
            %create summed spectra
            if(indexSubjTEs(indexTE) == 1)
                summedSpectra{indexTE} = a;
                newData= zeros(length(a.Data{1}),1,1,1,1,1,1,1,2,1,1,numberOfSubjects);
            else
                oldData = summedSpectra{indexTE}.Data{1};
                if (ndims(oldData) ~= 12)
                    newData(:,1,1,1,1,1,1,1,:,1,1,1) = oldData;
                else
                    newData(:,1,1,1,1,1,1,1,:,1,1,:) = oldData;
                end
                newData(:,1,1,1,1,1,1,1,:,1,1,indexSubjTEs(indexTE)) = a.Data{1};
                summedSpectra{indexTE}.Data{1} = newData;
            end
        end
    end

    fileNameMRUI = [subjects{indexSubj} '_mrui'];
    mruiSpectra.ExportMruiText(paths{indexSubj}, fileNameMRUI);
end

save([pathBase, 'stats.mat'],'stats');
save([pathBase, 'statsWater.mat'],'statsWater');
%%

filename = [pathBase, 'Summed_water.mat'];
save(filename, 'summedWater');
summedWater = summedWater.AverageData;
save([pathBase, 'Summed_Averaged_water.mat'], 'summedWater');
summedWater.ExportLcmRaw(pathBase, 'Summed_Averaged_water', addSinglet0ppm);
for indexTE = 1:numberOfTEs
    filename = [pathBase, 'Summed_spectra_TE' num2str(TEs(indexTE)) '.mat'];
    summedSpectraTE = summedSpectra{indexTE};
    %store only the data, which was kept after eliminating the deleted ones
    actualData = summedSpectraTE.Data{1}./indexSubjTEs(indexTE);
    summedSpectraTE.Data{1} = actualData(:,1,1,1,1,1,1,1,:,1,1,1:indexSubjTEs(indexTE));
    save(filename, 'summedSpectraTE');
    %average across subjects
    summedSpectraTE = summedSpectraTE.AverageData;
    filename = [pathBase, 'Summed_Averaged_TE' num2str(TEs(indexTE)) '.mat'];
    save(filename, 'summedSpectraTE');   
    summedSpectraTE.ExportLcmRaw(pathBase, strcat('Summed_Averaged_TE', num2str(TEs(indexTE))), addSinglet0ppm);
    
    %% create the TE series data
    if(indexTE == 1)
        mruiSpectra = summedSpectraTE;
        newDataMrui= zeros(length(summedSpectraTE.Data{1}),1,1,1,1,1,1,1,2,1,1,numberOfTEs);
    else
        oldData = mruiSpectra.Data{1};
        if (ndims(oldData) ~= 12)
            newDataMrui(:,1,1,1,1,1,1,1,:,1,1,1) = oldData;
        else
            newDataMrui(:,1,1,1,1,1,1,1,:,1,1,:) = oldData;
        end
        newDataMrui(:,1,1,1,1,1,1,1,:,1,1,indexTE) = summedSpectraTE.Data{1};
        mruiSpectra.Data{1} = newDataMrui;
    end
end
%% save MRUI file of summed spectra
fileNameMRUI = 'SummedAveraged_mrui';
mruiSpectra.ExportMruiText(pathBase, fileNameMRUI);

summarize_Deleted_Averages(subjects, pathBase, TEs, 'Downfield')
close all
