function plot_all_test_data_profit()


reconOption = 5;
truncOn = false;
offsetScatter = 0.3;
markerScatter = 'o';
markerColor = [1 0 0];

reconOption = 6;
truncOn = false;
offsetScatter = 0;
markerScatter = 'd';
markerColor = [0 1 0];
%
reconOption = 2;
truncOn = true;
offsetScatter = 0.15;
markerScatter = 'x';
markerColor = [1 0 0];


exportFolders = {'0 Water References',...
    '1 Normal recon', '2 Removed averages ','3 Removed coils',...
    '4 Removed averages and coils', '5 Water Supp Off', '6 Frequency Alignment Off', ...
    '7 Rescale off', '8 Rescale Water Supp Off'};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolders);

pathBaseExportFiles = pathBaseExportFiles(1:end-1);
pathBaseExportFiles = [pathBaseExportFiles ' - R3_new\'];

dataExportPathBaseRef = strcat(pathBaseExportFiles, exportFolders{2}, '\');
dataExportPathBase = strcat(pathBaseExportFiles, exportFolders{reconOption+1}, '\');

if truncOn == false
    truncSuffix = '_truncOff';
else
    truncSuffix = '';
end
%% configurations for averages and coils
totalAverages = 48; % NEX = 96 / 2 (Metabolite cyclcing)
if  (reconOption == 2) || (reconOption == 4)
    namingSuffixesAve = {...
        '_16ave_1', '_16ave_2', '_16ave_3', ...
        '_32ave_1', '_32ave_2', '_32ave_3'};
else
    namingSuffixesAve = {''};
end

% 8/16 Channel 1H coil has been used.
if  (reconOption == 3) || (reconOption == 4)
    namingSuffixesCoil = {...
        '_8coil_vert', '_8coil_horiz', ...
        '_8coil_1', '_8coil_2', ...
        '_12coil_vert_1', '_12coil_vert_2', '_12coil_horiz_1', '_12coil_horiz_2', ...
        '_12coil_1', '_12coil_2','_12coil_3', '_12coil_4',...
        '_4coil_vert_1', '_4coil_vert_2', '_4coil_horiz_1', '_4coil_horiz_2', ...
        '_4coil_1', '_4coil_2','_4coil_3', '_4coil_4'};
else
    namingSuffixesCoil = {''};
end

subjects = { ...
    'Subj_1' ...
    'Subj_2' ...
    'Subj_3' ...
    'Subj_4' ...
    'Subj_5' ...
    'Subj_6' ...
    'Subj_7' ...
    'Subj_8' ...
    'Subj_9' ...
    'Subj_10' ...
    'Subj_11' ...
    };

TEs = [24;];% 32; 40; 52; 60];

% name of metabolites in LCModel
metabolitesLCModel     = {'Asp', 'Cr_CH2', 'Cr', 'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA_as', 'NAA_ac', 'NAAG', 'tCho_P',  'Scyllo',    'Tau', 'Leu'};

% name of metabolites in ProFit
metabolitesProFit     = {'Asp', 'Cr_CH2', 'Cr', 'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA_as', 'NAA_ac', 'NAAG', 'tCho_P',  'Scyllo',    'Tau', 'Leu'};

% name of metabolites to display
metabolitesToDisplay  = {'Asp', 'tCr(CH_2)', 'tCr(CH_3)', 'GABA', 'Gln', 'Glu', 'Gly', 'GSH', 'Lac', 'mI', 'NAA(CH_2)', 'NAA(CH_3)', 'NAAG', 'tCho+', 'sI', 'Tau', 'MM spectrum'};
metabolitesLabels     = {'Asp', 'tCr(CH2)', 'tCr(CH3)', 'GABA', 'Gln', 'Glu', 'Gly', 'GSH', 'Lac', 'mI', 'NAA(CH2)', 'NAA(CH3)', 'NAAG', 'tCho+', 'sI', 'Tau', 'MM spectrum'};

numberOfMet = length(metabolitesLabels);
indecesMainMet = zeros(1,numberOfMet);
mainMetabolites = { 'tCr(CH3)', 'tCr(CH2)', 'Glu', 'mI', 'NAA(CH2)', 'NAA(CH3)', 'tCho+'};
for indexMet = 1:numberOfMet
    indecesMainMet(indexMet) = ~isempty(find(strcmp(mainMetabolites,metabolitesLabels{indexMet}), 1));
end
indecesMainMet = logical(indecesMainMet);

fitLCModel = true; %false fits profit
if fitLCModel
    SoftwareName = 'LCModel';
else
    SoftwareName = 'ProFit';
end
[metConcAllSubjLCModel, metConcAllSubjRefLCModel, metConcAllSubjNormLCModel, MC_H2O_ConcAllSubjLCModel, MC_H2O_ConcAllSubjRefLCModel] = ...
    getAllMetaboliteConc(dataExportPathBase, dataExportPathBaseRef, truncSuffix, subjects, TEs, ...
    namingSuffixesAve, namingSuffixesCoil, fitLCModel, reconOption, ...
    numberOfMet, metabolitesLCModel, metabolitesProFit, metabolitesLabels, indecesMainMet, ...
    offsetScatter, markerScatter, markerColor);

fitLCModel = false; %false fits profit
if fitLCModel
    SoftwareName = 'LCModel';
else
    SoftwareName = 'ProFit';
end
[metConcAllSubjProFit, metConcAllSubjRefProFit, metConcAllSubjNormProFit, MC_H2O_ConcAllSubjProFit, MC_H2O_ConcAllSubjRefProFit] = ...
    getAllMetaboliteConc(dataExportPathBase, dataExportPathBaseRef, truncSuffix, subjects, TEs, ...
    namingSuffixesAve, namingSuffixesCoil, fitLCModel, reconOption, ...
    numberOfMet, metabolitesLCModel, metabolitesProFit, metabolitesLabels, indecesMainMet, ...
    offsetScatter, markerScatter, markerColor);

metaboliteRef = 14;%'tCho+' 3;%'tCr(CH3)' 12;%'NAA(CH3)'
metConcAllSubjProFitScaled = metConcAllSubjProFit ./ metConcAllSubjProFit(:,:,metaboliteRef);
metConcAllSubjRefProFitScaled = metConcAllSubjRefProFit ./ metConcAllSubjRefProFit(:,metaboliteRef);
metConcAllSubjLCModelScaled = metConcAllSubjLCModel ./ metConcAllSubjLCModel(:,:,metaboliteRef);
metConcAllSubjRefLCModelScaled = metConcAllSubjRefLCModel ./ metConcAllSubjRefLCModel(:,metaboliteRef);

metaboliteRef = 0;
metConcAllSubjProFitScaled = metConcAllSubjProFit;
metConcAllSubjRefProFitScaled = metConcAllSubjRefProFit;
metConcAllSubjLCModelScaled = metConcAllSubjLCModel;
metConcAllSubjRefLCModelScaled = metConcAllSubjRefLCModel;

% BA plot paramters
tit = 'Repeatability '; % figure title
subsets = {'32 ave.1','32 ave.2','32 ave.3', '64 ave.1', '64 ave.2', '64 ave.3'};
subsets_refit = {'32 ave.', '64 ave.'};
softwareNames = {'ProFit', 'LCModel'};
gnames = {subsets, softwareNames}; % names of groups in data {dimension 1 and 2}
label = {'Ref. Fits','Subset Fits','ratio'}; % Names of data sets
label_refit = {'c^{fits1}_{i,k}','c^{fits2}_{i,k}','arb.u.'}; % Names of data sets
label_SW_comp = {'LCModel','ProFit','arb.u.'}; % Names of data sets
corrinfo = {'n','SSE','r2','eq'}; % stats to display of correlation scatter plot
BAinfo = {'RPC','ks'}; % stats to display on Bland-ALtman plot
BAinfo_refit = {''}; % stats to display on Bland-ALtman plot
limits = 'auto'; % how to set the axes limits
if 1 % colors for the data sets may be set as:
    colors = 'br';      % character codes
else
    colors = [0 0 1;... % or RGB triplets
        1 0 0];
end

paramsBA.data1TreatmentMode = 'Compare';
paramsBA.diffValueMode = 'percent';

numOfSimulations = length(namingSuffixesAve)*length(namingSuffixesCoil);

%there seems to be a fit error in the fitting of even the MC water data.
% BlandAltman(MC_H2O_ConcAllSubjLCModel, MC_H2O_ConcAllSubjProFit*1e-6, label_SW_comp,[tit 'Water'],subsets,...
%     'corrInfo',corrinfo,'baInfo',BAinfo,'axesLimits',limits,'showFitCI',' on',...
%     'baStatsMode','Gaussian','forceZeroIntercept','on', 'diffValueMode', 'percent');
% 
% BlandAltman(MC_H2O_ConcAllSubjRefLCModel, MC_H2O_ConcAllSubjRefProFit*1e-6, label_SW_comp,[tit 'Water'],subsets,...
%     'corrInfo',corrinfo,'baInfo',BAinfo,'axesLimits',limits,'showFitCI',' on',...
%     'baStatsMode','Gaussian','forceZeroIntercept','on', 'diffValueMode', 'percent');

rpc_table = cell(numberOfMet+6,5);
rpc_table{1,2} = softwareNames{1};
rpc_table{1,4} = softwareNames{2};
rpc_table{2,2} = subsets_refit{1};
rpc_table{2,3} = subsets_refit{2};
rpc_table{2,4} = subsets_refit{1};
rpc_table{2,5} = subsets_refit{2};
rpc_table(3:end-4,1) = metabolitesToDisplay;
rpc_table{end-2,1} = 'Mean';
rpc_table{end-1,1} = 'Mean Main';
rpc_table{end,1} = 'Mean Small';
    
%title('Reproducibility');
subplotsX = 4;
subplotsY = 4;
totalSubplots1 = subplotsX * subplotsY;
rpcSum = zeros(1,numberOfMet);
diffMedian = zeros(1,numberOfMet);
for indexMet = 1: numberOfMet
    if indexMet == metaboliteRef
        continue;
    end
    if (indexMet == 1) || (indexMet == totalSubplots1/2+1)
        figure;
        if indexMet >= 8 
            subplotsX = 6;
            subplotsY = 3;
            totalSubplots = subplotsX * subplotsY;
        end
    end
    if indexMet <= totalSubplots1/2
        runningIndexPlot = (indexMet-1)+1+floor((indexMet-1)/subplotsY)*subplotsY-floor((indexMet-1)/totalSubplots1*2)*totalSubplots1;
    else
        indexMet2 = indexMet - totalSubplots1/2;
        runningIndexPlot = (indexMet2-1)+1+floor((indexMet2-1)/subplotsY)*subplotsY-floor((indexMet2-1)/totalSubplots*2)*totalSubplots;
    end

    %ProFit Concentrations
    metConcAllSubjProFitScaled_ = metConcAllSubjProFitScaled(:, :, indexMet);
    metConcAllSubjProFitScaled_ = reshape(metConcAllSubjProFitScaled_,11*3,2);%group same SNR
    metConcAllSubjRefProFitScaled_ = metConcAllSubjRefProFitScaled(:, indexMet);
    metConcAllSubjRefProFitScaled_ = repmat(metConcAllSubjRefProFitScaled_,1,6);
    metConcAllSubjRefProFitScaled_ = reshape(metConcAllSubjRefProFitScaled_,11*3,2);%group same SNR
    
    %LCModel Concentrations
    metConcAllSubjLCModelScaled_ = metConcAllSubjLCModelScaled(:, :, indexMet);
    metConcAllSubjLCModelScaled_ = reshape(metConcAllSubjLCModelScaled_,11*3,2);%group same SNR
    metConcAllSubjRefLCModelScaled_ = metConcAllSubjRefLCModelScaled(:, indexMet);
    metConcAllSubjRefLCModelScaled_ = repmat(metConcAllSubjRefLCModelScaled_,1,6);
    metConcAllSubjRefLCModelScaled_ = reshape(metConcAllSubjRefLCModelScaled_,11*3,2);%group same SNR
    
    %ProFit&LCModel Concentrations
    metConcAllSubjScaled_ = cat(3, metConcAllSubjProFitScaled_, metConcAllSubjLCModelScaled_);
    metConcAllSubjRefScaled_ = cat(3, metConcAllSubjRefProFitScaled_, metConcAllSubjRefLCModelScaled_);
%% ref Fits vs subfits: Code not used anymore in this style
%     BlandAltman(metConcAllSubjRefScaled_, metConcAllSubjScaled_, label,[tit metabolitesLCModel{indexMet}],gnames,...
%         'corrInfo',corrinfo,'baInfo',BAinfo,'axesLimits',limits,'colors',colors, 'showFitCI',' on',...
%         'baStatsMode','non-parametric','forceZeroIntercept','on');
% % Reference fits vs all per Software
%     %ProFit ref Fits vs subfits
%     rpcProFit = BlandAltman(metConcAllSubjRefProFitScaled_, metConcAllSubjProFitScaled_, label,[tit softwareNames{1} ' ' metabolitesLCModel{indexMet}],subsets,...
%         'corrInfo',corrinfo,'baInfo',BAinfo,'axesLimits',limits,'colors',[0.75 0.5 0; 0.5 0.2 0], 'showFitCI',' on',...
%         'baStatsMode','Gaussian','forceZeroIntercept','on', 'diffValueMode', 'percent');
%     %LCModel ref Fits vs subfits
%     rpcLCModel = BlandAltman(metConcAllSubjRefLCModelScaled_, metConcAllSubjLCModelScaled_, label,[tit softwareNames{2} ' ' metabolitesLCModel{indexMet}],subsets,...
%         'corrInfo',corrinfo,'baInfo',BAinfo,'axesLimits',limits,'colors',[0, 0.5, 0.75; 0, 0.25 0.5], 'showFitCI',' on',...
%         'baStatsMode','Gaussian','forceZeroIntercept','on', 'diffValueMode', 'percent');
%     %rpc_table{indexMet+1,2} = rpcProFit;
%     %rpc_table{indexMet+1,3} = rpcLCModel;
%% LCModel vs ProFit
%     BlandAltman(metConcAllSubjLCModelScaled_, metConcAllSubjProFitScaled_, label_SW_comp,[tit metabolitesLCModel{indexMet}],subsets,...
%         'corrInfo',corrinfo,'baInfo',BAinfo,'axesLimits',limits,'colors',colors, 'showFitCI',' on',...
%         'baStatsMode','non-parametric','forceZeroIntercept','on', 'diffValueMode', 'percent');
%     [rpc, ~, stat] = BlandAltman(metConcAllSubjRefLCModelScaled_, metConcAllSubjRefProFitScaled_, label_SW_comp,[tit metabolitesLCModel{indexMet}],subsets,...
%         'corrInfo',corrinfo,'baInfo',BAinfo,'axesLimits',limits,'colors',colors, 'showFitCI',' on',...
%         'baStatsMode','non-parametric','forceZeroIntercept','on', 'diffValueMode', 'percent');
%     rpcs(indexMet) = rpc;
%     diffMedian(indexMet) = stat.differenceMedian;
%% Subfits vs subfits per Software
    %ProFit ref Fits vs subfits    
    rpc_ProFit_32_ave = calculate_rpc_percent(metConcAllSubjProFitScaled_(1:11,1), metConcAllSubjProFitScaled_(12:22,1));
    rpc_ProFit_64_ave = calculate_rpc_percent(metConcAllSubjProFitScaled_(1:11,2), metConcAllSubjProFitScaled_(12:22,2));
    legendText = {['{\color{blue}\bf32-ave: RPC=' mynum2str(rpc_ProFit_32_ave,2) '%}'];['{\color{red}\bf64-ave: RPC=' mynum2str(rpc_ProFit_64_ave,2) '%}']};

    sProFit = subplot(subplotsX,subplotsY,runningIndexPlot);
    [rpcProFit, fig, stats] = BlandAltmanOnly(sProFit, metConcAllSubjProFitScaled_(1:11,:), metConcAllSubjProFitScaled_(12:22,:),...
        label_refit,[metabolitesToDisplay{indexMet}],legendText,...
        'corrInfo',corrinfo,'baInfo',BAinfo_refit,'axesLimits',limits,'colors','br', 'showFitCI',' on',...
        'baStatsMode','Gaussian','forceZeroIntercept','on', 'diffValueMode', 'percent');
    
    %LCModel ref Fits vs subfits
    rpc_LCModel_32_ave = calculate_rpc_percent(metConcAllSubjLCModelScaled_(1:11,1), metConcAllSubjLCModelScaled_(12:22,1));
    rpc_LCModel_64_ave = calculate_rpc_percent(metConcAllSubjLCModelScaled_(1:11,2), metConcAllSubjLCModelScaled_(12:22,2));
    legendText = {['{\color{blue}\bf32-ave: RPC=' mynum2str(rpc_LCModel_32_ave,2) '%}'];['{\color{red}\bf64-ave: RPC=' mynum2str(rpc_LCModel_64_ave,2) '%}']};

    sLCModel = subplot(subplotsX,subplotsY,runningIndexPlot+subplotsY); 
    [rpcLCModel, fig, stats] = BlandAltmanOnly(sLCModel, metConcAllSubjRefLCModelScaled_(1:11,:), metConcAllSubjLCModelScaled_(12:22,:), ...
        label_refit,[metabolitesToDisplay{indexMet}], legendText,...
        'corrInfo',corrinfo,'baInfo',BAinfo_refit,'axesLimits',limits,'colors','br', 'showFitCI',' on',...
        'baStatsMode','Gaussian','forceZeroIntercept','on', 'diffValueMode', 'percent');
    
    if mod(indexMet,subplotsY)==1
        pos1 = sProFit.Position;
        annotation('textarrow', [pos1(1), 0], [pos1(2)+pos1(4)/2, 0],'String',softwareNames{1}, ...
            'HeadStyle','none','LineStyle', 'none', 'TextRotation',90,'units','normalized', 'FontSize', 16, ...
            'FontWeight','bold','HorizontalAlignment','center', 'VerticalAlignment','middle');
        pos2 = sLCModel.Position;
        annotation('textarrow', [pos1(1), 0], [pos2(2)+pos2(4)/2, 0],'String',softwareNames{2}, ...
            'HeadStyle','none','LineStyle', 'none', 'TextRotation',90,'units','normalized', 'FontSize', 16, ...
            'FontWeight','bold','HorizontalAlignment','center', 'VerticalAlignment','middle');
    end
    rpc_table{indexMet+2,2} = rpc_ProFit_32_ave;
    rpc_table{indexMet+2,3} = rpc_ProFit_64_ave;
    rpc_table{indexMet+2,4} = rpc_LCModel_32_ave;
    rpc_table{indexMet+2,5} = rpc_LCModel_64_ave;
end

rpc_values = cell2mat(rpc_table(3:end-3,2:5));
% extract summaries main met
rpc_meanProFit_32ave_main = mean(rpc_values(indecesMainMet,1));
rpc_meanProFit_64ave_main = mean(rpc_values(indecesMainMet,2));
rpc_meanLCModel_32ave_main = mean(rpc_values(indecesMainMet,3));
rpc_meanLCModel_64ave_main = mean(rpc_values(indecesMainMet,4));
% extract summaries small met
rpc_meanProFit_32ave_small = mean(rpc_values(~indecesMainMet,1));
rpc_meanProFit_64ave_small = mean(rpc_values(~indecesMainMet,2));
rpc_meanLCModel_32ave_small = mean(rpc_values(~indecesMainMet,3));
rpc_meanLCModel_64ave_small = mean(rpc_values(~indecesMainMet,4));
% extract summaries all met
rpc_meanProFit_32ave = mean(rpc_values(:,1));
rpc_meanProFit_64ave = mean(rpc_values(:,2));
rpc_meanLCModel_32ave = mean(rpc_values(:,3));
rpc_meanLCModel_64ave = mean(rpc_values(:,4));
%% Add to RPC_table: all met
rpc_table(3:end-4,2:5) = sprintfc('%.0f',rpc_values);
% Add to RPC_table: mean values
rpc_table{end-2,2} = num2str(rpc_meanProFit_32ave, '%.0f');
rpc_table{end-2,3} = num2str(rpc_meanProFit_64ave, '%.0f');
rpc_table{end-2,4} = num2str(rpc_meanLCModel_32ave, '%.0f');
rpc_table{end-2,5} = num2str(rpc_meanLCModel_64ave, '%.0f');
% Add to RPC_table: mean main met
rpc_table{end-1,2} = num2str(rpc_meanProFit_32ave_main, '%.0f');
rpc_table{end-1,3} = num2str(rpc_meanProFit_64ave_main, '%.0f');
rpc_table{end-1,4} = num2str(rpc_meanLCModel_32ave_main, '%.0f');
rpc_table{end-1,5} = num2str(rpc_meanLCModel_64ave_main, '%.0f');
% Add to RPC_table: mean small met
rpc_table{end,2} = num2str(rpc_meanProFit_32ave_small, '%.0f');
rpc_table{end,3} = num2str(rpc_meanProFit_64ave_small, '%.0f');
rpc_table{end,4} = num2str(rpc_meanLCModel_32ave_small, '%.0f');
rpc_table{end,5} = num2str(rpc_meanLCModel_64ave_small, '%.0f');
%write file
xlswrite([pathBaseExportFiles, 'InVivoResults_RPC.xlsx'], rpc_table)

%% code will break here. This used to be the old style plotting/results without Bland-Altman plots.
meanMetDeviation = mean(abs(metaboliteConcentrationsAllSubjNorm), 'omitnan');
stdMetDeviation  = std(abs(metaboliteConcentrationsAllSubjNorm), 'omitnan');

meanAllMetDeviation = mean(abs(metaboliteConcentrationsAllSubjNorm(:)), 'omitnan');
stdAllMetDeviation  = std(abs(metaboliteConcentrationsAllSubjNorm(:)), 'omitnan');

mainMetaboliteConcentrationsAllSubj = metaboliteConcentrationsAllSubjNorm(:, indecesMainMet);
meanMainMetDeviation = mean(abs(mainMetaboliteConcentrationsAllSubj(:)), 'omitnan');
stdMainMetDeviation  = std(abs(mainMetaboliteConcentrationsAllSubj(:)), 'omitnan');

smallMetaboliteConcentrationsAllSubj = metaboliteConcentrationsAllSubjNorm(:, ~indecesMainMet);
meanSmallMetDeviation = mean(abs(smallMetaboliteConcentrationsAllSubj(:)), 'omitnan');
stdSmallMetDeviation  = std(abs(smallMetaboliteConcentrationsAllSubj(:)), 'omitnan');

metaboliteTable = cell(numberOfMet+3,2);
metaboliteTable(2:numberOfMet+1,1) = metabolitesLabels;
metaboliteTable{numberOfMet+2,1}   = 'Mean';
metaboliteTable{numberOfMet+3,1}   = 'Mean Main Metabolites';
metaboliteTable{numberOfMet+4,1}   = 'Mean Small Metabolites';

plusMinusSign = char(177);
metaboliteTable{1,2} = SoftwareName;

for indexMet = 1:numberOfMet
    metaboliteTable{indexMet+1,2}   = [num2str(meanMetDeviation(indexMet),'%.1f'),  plusMinusSign, num2str(stdMetDeviation(indexMet),'%.1f')];
end

metaboliteTable{numberOfMet+2,2}   = [num2str(meanAllMetDeviation,'%.1f'),  plusMinusSign, num2str(stdAllMetDeviation,'%.1f')];
metaboliteTable{numberOfMet+3,2}   = [num2str(meanMainMetDeviation,'%.1f'),  plusMinusSign, num2str(stdMainMetDeviation,'%.1f')];
metaboliteTable{numberOfMet+3,2}   = [num2str(meanSmallMetDeviation,'%.1f'),  plusMinusSign, num2str(stdSmallMetDeviation,'%.1f')];

xlswrite([pathBaseExportFiles, 'InVivoResults' SoftwareName, '.xlsx'], metaboliteTable)
%
% mean(cell2mat(NAA_SNR_all(:,1)))
% std(cell2mat(NAA_SNR_all(:,1)))
% mean(cell2mat(NAA_SNR_all(:,2)))
% mean(cell2mat(NAA_SNR_all(:,3)))
% mean(cell2mat(NAA_SNR_all(:,4)))
% mean(cell2mat(NAA_SNR_all(:,5)))
% std(cell2mat(NAA_SNR_all(:,5)))

end

function metaboliteConcentrations = matchFittedMetabolites(fitLCModel, currentConcentration, metabolitesNames,  proFitIteration, activeMetabolites)
metaboliteConcentrations = zeros(size(metabolitesNames));

if fitLCModel
    activeMetabolites = currentConcentration(1,:);
    currentConcentration = currentConcentration(2,:);
else
    activeMetabolites = activeMetabolites{proFitIteration};
    currentConcentration = currentConcentration{proFitIteration};
end
for indexMet = 1 : length(metabolitesNames)
    indexMetFit = find(strcmp(activeMetabolites,metabolitesNames(indexMet)));
    if fitLCModel
        metaboliteConcentrations(indexMet) = currentConcentration{indexMetFit};
    else
        metaboliteConcentrations(indexMet) = currentConcentration(indexMetFit);
    end
end
%     metaboliteConcentrations(1) = metaboliteConcentrations(1) / 1e6; % adjust the MMB to be within range
end

function [metaboliteConcentrationsAllSubj, metaboliteConcentrationsAllSubjRef, metaboliteConcentrationsAllSubjNorm,...
    MC_H2O_ConcentrationsAllSubj, MC_H2O_ConcentrationsAllSubjRef] = ...
    getAllMetaboliteConc(dataExportPathBase, dataExportPathBaseRef, truncSuffix, subjects, TEs, ...
    namingSuffixesAve, namingSuffixesCoil, fitLCModel, reconOption, ...
    numberOfMet, metabolitesLCModel, metabolitesProFit, metabolitesLabels, indecesMainMet, ...
    offsetScatter, markerScatter, markerColor)
% settings
plotInSameFigureAllSubjects = true;
scalingFactorColor = 1;
yLimValue = 50;
NAA_SNR_global_ref = 700;% 650; for truncOn true


if fitLCModel
    SoftwareName = 'LCModel';
    indexFigSoftware = 0;
else
    SoftwareName = 'ProFit';
    indexFigSoftware = 50;
    proFitIteration =4;
end

% prepare arrays etc
numberOfSubjects = length(subjects);
dataExportPathRef = cell(1,numberOfSubjects);
dataExportPath = cell(1,numberOfSubjects);
for indexSubj = 1:numberOfSubjects
    dataExportPathRef{indexSubj} = [dataExportPathBaseRef subjects{indexSubj} '\\'];
    dataExportPath{indexSubj} = [dataExportPathBase subjects{indexSubj} '\\'];
end
figId = figure();
colormapVect = colormap();
close(figId);
colormapVect = colormapVect(end:-1:1, :);
numberOfTEs = length(TEs);
numberOfColors = size(colormapVect,1);
NAA_SNR_all = cell(numberOfSubjects, numberOfTEs);
meanPercentualChange = 0;
iterator = 0;
nansIterator = 0;
meanFQN = 0;
meanFQN2 = 0;
numOfSimulations = length(namingSuffixesAve)*length(namingSuffixesCoil);
metaboliteConcentrationsAllSubjNorm = zeros(numberOfSubjects * numOfSimulations, numberOfMet);
MC_H2O_ConcentrationsAllSubj = zeros(numberOfSubjects, numOfSimulations);
metaboliteConcentrationsAllSubj = zeros(numberOfSubjects, numOfSimulations, numberOfMet);
metaboliteConcentrationsAllSubjRef = zeros(numberOfSubjects, numberOfMet);
MC_H2O_ConcentrationsAllSubjRef = zeros(numberOfSubjects, 1);
%% do the actual plotting
for indexSubj = 1:numberOfSubjects
    if fitLCModel
        load([dataExportPathRef{indexSubj}, 'concentrations' truncSuffix '.mat'], 'concentrations');
        load([dataExportPathRef{indexSubj}, 'concentrations', truncSuffix '_MC_H2O.mat'], 'concentrations_MC_H2O');
        concentrationsRef = concentrations;
        concentrations_MC_H2ORef = concentrations_MC_H2O;
        load([dataExportPath{indexSubj}, 'concentrations' truncSuffix '.mat'], 'concentrations');
        load([dataExportPath{indexSubj}, 'concentrations', truncSuffix '_MC_H2O.mat'], 'concentrations_MC_H2O');
    else
        load([dataExportPathRef{indexSubj}, 'concentrations' truncSuffix '_profit.mat'], 'concentrations', 'activeMetabolites', 'FQNs', 'FQNs2');
        load([dataExportPathRef{indexSubj}, 'concentrations', truncSuffix '_H2O_profit.mat'], 'concentrations_H2O');
        load([dataExportPathRef{indexSubj}, 'concentrations', truncSuffix '_MC_H2O_profit.mat'], 'concentrations_MC_H2O');
        concentrationsRef = concentrations;
        concentrations_H2ORef = cell2mat(concentrations_H2O);
        concentrations_MC_H2ORef = cell2mat(concentrations_MC_H2O);
        activeMetabolitesRef = activeMetabolites;
        FQNsRef =  FQNs;
        FQNsRef2 =  FQNs2;
        load([dataExportPath{indexSubj}, 'concentrations' truncSuffix '_profit.mat'], 'concentrations', 'activeMetabolites', 'FQNs', 'FQNs2'); 
        load([dataExportPath{indexSubj}, 'concentrations', truncSuffix '_H2O_profit.mat'], 'concentrations_H2O');
        load([dataExportPath{indexSubj}, 'concentrations', truncSuffix '_MC_H2O_profit.mat'], 'concentrations_MC_H2O');
        concentrations_H2O = cell2mat(concentrations_H2O);
        concentrations_MC_H2O = cell2mat(concentrations_MC_H2O);
    end
    % load stats (SNR, FWHM, etc)
    load([dataExportPathRef{indexSubj}, 'stats' truncSuffix '.mat'], 'stats');
    statsRef = stats;
    load([dataExportPath{indexSubj}, 'stats' truncSuffix '.mat'], 'stats');
    %%
    for indexTE = 1:numberOfTEs
        if plotInSameFigureAllSubjects
            indexFigure = TEs(indexTE)+indexFigSoftware;
        else
            indexFigure = str2double(subjects{indexSubj})*100 + TEs(indexTE)+indexFigSoftware;
            meanPercentualChange = 0;
            iterator = 0;
            nansIterator = 0;
            meanFQN = 0;
            meanFQN2 = 0;
        end
        %get Reference
        figure(indexFigure);
        hold on
        currentStatsRef = statsRef{indexTE,1, 1};
        NAA_ref_SNR = currentStatsRef{1,2};
        NAA_SNR_all{indexSubj,indexTE} = NAA_ref_SNR;
        water_ref_amplitude = currentStatsRef{end,4};
        currentColorRef = colormapVect(round(NAA_ref_SNR/NAA_ref_SNR/scalingFactorColor * numberOfColors),:);
        currentConcentrationRef = concentrationsRef{indexTE,1, 1};
        
        if fitLCModel
%             MC_H2O_ConcentrationsRef = concentrations_MC_H2ORef{indexTE,1, 1};
%             MC_H2O_ConcentrationsAllSubjRef(indexSubj) = MC_H2O_ConcentrationsRef{2,2};
            metaboliteConcentrationsRef = matchFittedMetabolites(fitLCModel, currentConcentrationRef, metabolitesLCModel, [], []);
            metaboliteConcentrationsRef(end) = metaboliteConcentrationsRef(end) .* 1e-8;
        else
            metaboliteConcentrationsRef_notScaled = matchFittedMetabolites(fitLCModel, currentConcentrationRef, metabolitesProFit, proFitIteration, activeMetabolitesRef);
            %scale the concentations similarly as LCModel
            metaboliteConcentrationsRef = metaboliteConcentrationsRef_notScaled * 40873 / concentrations_H2ORef * 2;
%             MC_H2O_ConcentrationsRef = concentrations_MC_H2ORef * 40873 / concentrations_H2ORef * 2;
%             MC_H2O_ConcentrationsAllSubjRef(indexSubj) = MC_H2O_ConcentrationsRef;
        end
        if ~fitLCModel
            currentFQNRef = FQNsRef{indexTE,1, 1};
            FQN_Ref = currentFQNRef{proFitIteration};
            currentFQNRef2 = FQNsRef2{indexTE,1, 1};
            FQN_Ref2 = currentFQNRef2{proFitIteration};
        end
        %scale with the MC water reference amplitude
        metaboliteConcentrationsRefNormalized = (metaboliteConcentrationsRef-metaboliteConcentrationsRef) ./ metaboliteConcentrationsRef *100;
        scatter([1:numberOfMet]-offsetScatter,metaboliteConcentrationsRefNormalized,[],'d', 'MarkerEdgeColor', currentColorRef);
        
        metaboliteConcentrationsAllSubjRef(indexSubj, :)  = metaboliteConcentrationsRef;
        %         cb = colorbar;
        %         cb.Limits = [0 NAA_ref_SNR*scalingFactorColor];
        %         cb.Color = colormapVect
        
        metaboliteConcentrationsAll = zeros(numOfSimulations, numberOfMet);
        % do plotting for each individual case
        for indexAverageDelete = 1: length(namingSuffixesAve)
            for indexCoilDelete = 1: length(namingSuffixesCoil)
                runningIndex = (indexAverageDelete-1) * length(namingSuffixesCoil) + indexCoilDelete;
                currentStats = stats{indexTE,indexAverageDelete, indexCoilDelete};
                currentConcentration = concentrations{indexTE,indexAverageDelete, indexCoilDelete};
                if ~isempty(currentConcentration)
                    NAA_SNR = currentStats{1,2};
                    water_amplitude = currentStats{end,4};
                    figure(indexFigure);
                    hold on
                    if fitLCModel
%                         MC_H2O_Concentrations = concentrations_MC_H2O{indexTE,indexAverageDelete, indexCoilDelete};
%                         MC_H2O_ConcentrationsAllSubj(indexSubj, runningIndex) = MC_H2O_Concentrations{2,2};
                        metaboliteConcentrations = matchFittedMetabolites(fitLCModel, currentConcentration, metabolitesLCModel, [], []);
                        metaboliteConcentrations(end) = metaboliteConcentrations(end) .* 1e-8;
                    else
                        metaboliteConcentrations_notScaled = matchFittedMetabolites(fitLCModel, currentConcentration, metabolitesProFit, proFitIteration, activeMetabolites);
                        %scale the concentations similarly as LCModel
                        metaboliteConcentrations = metaboliteConcentrations_notScaled * 40873 / concentrations_H2O(runningIndex) * 2;
%                         MC_H2O_Concentrations = concentrations_MC_H2O(runningIndex) * 40873 / concentrations_H2O(runningIndex) * 2;
%                         MC_H2O_ConcentrationsAllSubj(indexSubj, runningIndex) = MC_H2O_Concentrations;
                    end
                    if runningIndex <= 3 %scale with coresponding averages
                        metaboliteConcentrations = metaboliteConcentrations * 3;
                        MC_H2O_ConcentrationsAllSubj(indexSubj, runningIndex) = MC_H2O_ConcentrationsAllSubj(indexSubj, runningIndex) * 3;
                    else
                        metaboliteConcentrations = metaboliteConcentrations *3/2;
                        MC_H2O_ConcentrationsAllSubj(indexSubj, runningIndex) = MC_H2O_ConcentrationsAllSubj(indexSubj, runningIndex) *3/2;
                    end
                    if ~fitLCModel
                        currentFQN = FQNs{indexTE,indexAverageDelete, indexCoilDelete};
                        FQN = currentFQN{proFitIteration};
                        meanFQN = meanFQN + FQN;
                        currentFQN2 = FQNs2{indexTE,indexAverageDelete, indexCoilDelete};
                        FQN2 = currentFQN2{proFitIteration};
                        meanFQN2 = meanFQN2 + FQN2;
                    end                    
                    metaboliteConcentrationsNormalized = (metaboliteConcentrations-metaboliteConcentrationsRef) ...
                        ./ metaboliteConcentrationsRef *100;
                    metaboliteConcentrationsAll(runningIndex, :) = metaboliteConcentrationsNormalized;
                    metaboliteConcentrationsNormalized(abs(metaboliteConcentrationsNormalized) > yLimValue*2) = NaN; %TODO change! This is actually stupid
                    metaboliteConcentrationsAllSubjNorm(runningIndex + (indexSubj-1)*numOfSimulations, :) = metaboliteConcentrationsNormalized;
                    metaboliteConcentrationsAllSubj(indexSubj, runningIndex, :) = metaboliteConcentrations;
                    if reconOption>4
                        %use this scatter if you want to check if there are structural offsets
%                         scatter([1:numberOfMet]+offsetScatter,metaboliteConcentrationsNormalized, markerScatter, 'MarkerEdgeColor', markerColor, 'LineWidth', 1);
                    else
                        % SNR matched
                        currentColor = colormapVect(round(NAA_SNR/NAA_SNR_global_ref/scalingFactorColor * numberOfColors),:);
%                         scatter([1:numberOfMet]+offsetScatter,metaboliteConcentrationsNormalized, markerScatter, 'MarkerEdgeColor',currentColor, 'LineWidth', 2);
                    end
                    currentNans = isnan(metaboliteConcentrationsNormalized);
                    nansIterator = nansIterator + currentNans;
                    meanPercentualChange = meanPercentualChange + abs(metaboliteConcentrationsNormalized);
                    iterator = iterator + ~(currentNans);
                end
            end
        end
        xticksSNR = linspace(NAA_SNR_global_ref,0,11);
        cb = colorbar('XTickLabel',num2str(round(xticksSNR)'), 'Direction','reverse');
        ylabel(cb,'SNR_{NAA(CH3)}');
        
        %         boxplot(metaboliteConcentrationsAll);
        xticks(1:numberOfMet);
        xticklabels(metabolitesLabels);
        xtickangle(45)
        ylim([-yLimValue,yLimValue]);
        xlim([0.5,17.5])
        ylabel('Concentration change c_{k,%} (%)')
        
        mainMetMeanChange = meanPercentualChange(indecesMainMet);
        mainMetNaNs = nansIterator(indecesMainMet);
        mainMetIterator = iterator(indecesMainMet);
        
        title(SoftwareName)
        if fitLCModel
            title([SoftwareName  ... ' Subject: ', subjects{indexSubj}, ' TE ', num2str(TEs(indexTE)) ' ms'...
                ' - mean change (main mets): ', num2str(mean(meanPercentualChange./iterator,'omitnan'),3) '%; '...
                ' (', num2str(mean(mainMetMeanChange./mainMetIterator,'omitnan'),3) '%); '...
                num2str(sum(nansIterator)), ' (', num2str(sum(mainMetNaNs)), ') NaNs'...
                ]);
        else
            title([SoftwareName  ... ' Subject: ', subjects{indexSubj}, ' TE ', num2str(TEs(indexTE)) ' ms'...
                ' - mean change (main mets): ', num2str(mean(meanPercentualChange./iterator,'omitnan'),3) '%; '...
                ' (', num2str(mean(mainMetMeanChange./mainMetIterator,'omitnan'),3) '%); '...
                num2str(sum(nansIterator)), ' (', num2str(sum(mainMetNaNs)), ') NaNs'...
                ', FQN_{Ref}:' num2str(FQN_Ref/numberOfSubjects,3), ', FQN_{mean}:' num2str(meanFQN/max(iterator),3)...
                ', FQN_{Ref}:' num2str(FQN_Ref2/numberOfSubjects,3), ', FQN_{mean}:' num2str(meanFQN2/max(iterator),3)...
                ]);
        end
    end
    
end
end

function rpc = calculate_rpc_percent(dataS1, dataS2)
    data.set1 = dataS1;
    data.set2 = dataS2;
    s = size(data.set1);
    if ~isequal(s,size(data.set2))
        error('data1 and data2 must have the same size');
    end
    
    data.set1 = reshape(data.set1, [numel(data.set1),1]);
    data.set2 = reshape(data.set2, [numel(data.set2),1]);
    data.mask = isfinite(data.set1) & isnumeric(data.set1) & isfinite(data.set2) & isnumeric(data.set2);
    data.maskedSet1 = data.set1(data.mask);
    data.maskedSet2 = data.set2(data.mask);
    
	data.maskedBaRefData = mean([data.maskedSet1,data.maskedSet2],2);
    data.maskedDifferences = (data.maskedSet2-data.maskedSet1) ./ data.maskedBaRefData*100;
    
    differenceSTD = std(data.maskedDifferences, 'omitnan');
    rpc = 1.96*differenceSTD;
end