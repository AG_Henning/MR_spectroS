classdef CoilCombSettings 
    
    properties
        algorithm
        indecesOfPeakOfInterest
        indecesOfNoiseArea
        snrDefinition
        weights
    end
    
    
    methods
        
        function this=CoilCombSettings()
            this.algorithm                  = 'svd';
            this.indecesOfPeakOfInterest    =  [];
            this.indecesOfNoiseArea         =  [1 1000];
            this.snrDefinition              =  []; % for future use
            this.weights                    =  []; 
        end
    end
    
end