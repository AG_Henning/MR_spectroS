function this = Filtering(this)

        
  
        gaussFilter = this.Parameter.FilterSettings.Gaussian;
        expFilter   = this.Parameter.FilterSettings.Exponential;
        dwellTime   = this.Parameter.Headers.DwellTimeSig_ns * 1e-9; %in sec
        data        =this.Data{1};
        
        if (~gaussFilter) && (~expFilter)
             
            warning(' exp_filter= gauss_filter= 0, The data will remain unaffected' )
            
        else
            readout = size(data,this.kx_dim)*dwellTime * 1e+3;
            %readout = this.Parameter.Headers.AcquisitionTime_ms;
          
            if this.Parameter.ReconFlags.isDataFiltered
                warning ('The data is already filtered!!!')
            end 

             % NCol, 1, 1, NCha, 1, 1, 1, 1, n_mix, NRep, 1, NMeas  
              
              data=this.Data{1};

              %NCol=  size( data, this.kx_dim);   % # timepoints
              NMeas= size( data, this.meas_dim); % # Averages
              NCha=  size( data, this.coil_dim); % # Channels
              NRep=  size( data, this.dyn_dim);  % # Repetitions
              NMix=  size( data, this.mix_dim);  % # Mixes


    %------------------------------Voigt Filter -------------------------------
    %       exp(-t*exp_filter)*exp(-t^2/(2*gauss_filter^2))

  
         expFilter    = expFilter*pi; %In order to be in Hz exp(-At)--> linewidth= A/pi
         gaussFilter  = gaussFilter/readout; %normalised % of aquisition time
   
         
   
   if gaussFilter==0
       gaussPart=1;
   else
       gaussPart=(exp(-linspace(0,1,length(data)).^2/(2*gaussFilter^2))');
   end
   
   
       expPart=(exp(((0:length(data)-1)*dwellTime)*expFilter)');
       voigt=expPart.*gaussPart;
   

              for ncha=1:NCha
                  for nrep=1:NRep
                    for nmeas=1:NMeas
                        for nmix=1:NMix
                            data(:,1,1,ncha,1,1,1,1,nmix,nrep,1,nmeas)=data(:,1,1,ncha,1,1,1,1,nmix,nrep,1,nmeas).*voigt;
                        end
                    end
                  end
              end

              this.Data{1}=data;
              this.Parameter.ReconFlags.isDataFiltered= true;
        end





end

