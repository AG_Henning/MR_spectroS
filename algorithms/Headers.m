classdef Headers < hgsetget
    
    
    properties 
        
            
            PatientName
            PatientID
            BirthDay
            InstitutionName
            SoftwareVersion
            SequenceDescription
            SequenceFileName
            TR_us
            TE_us
            TM_us
            NAveMeas
            NRepMeas
            %NChannels
            Nucleus
            ScanFrequency
            VectorSize
            VectorSize_OS
            DwellTimeSig_ns
            SagPos
            CorPos
            TraPos
            SagRot_deg
            CorRot_deg
            TraRot_deg
            %AdjSagPos
            %AdjCorPos
            %AdjTraPos
            %AdjSagRot_deg
            %AdjCorRot_deg
            %AdjTraRot_deg
            %InPlaneRot_deg
            Thickness_mm
            PhaseFOV_mm
            ReadoutFOV_mm
            PatientPosition
            FlipAngle_deg
            TransmittingCoil
            %ReceivingCoil
            %CoilText
            SamplesBeforeEcho
            AfterHowManyAverages
            WaterSpectra
            RemoveOversampling
            AcquisitionTime_ms
            Bandwidth_Hz

            
    end
    
    
    
    methods
    
        
        function this= Headers()
  
        end
        
        
      
    function hdrs=readHeaders(hdrs,mrprot)
    %%
    %##########################################################################

        if nargin<2
            [filename,pathname] = uigetfile({'*.hdr;*.HDR;*.dat','Dat or Hdr files (*.dat, *.hdr)'},'Please select the dat or hdr file');
            filepath=strcat(pathname,filename);
        end
        
        hdrs.PatientName         = mrprot.Dicom.tPatientName;
        hdrs.PatientID           = mrprot.Config.PatientID;
        hdrs.BirthDay            = mrprot.Config.PatientBirthDay;
        hdrs.InstitutionName     = mrprot.Dicom.InstitutionName;
        hdrs.SoftwareVersion     = mrprot.Dicom.SoftwareVersions;
        hdrs.SequenceDescription = mrprot.Config.SequenceDescription;
        hdrs.SequenceFileName    = mrprot.Config.SequenceFileName;
        hdrs.TR_us               = mrprot.MeasYaps.alTR{1};
        hdrs.TE_us               = mrprot.MeasYaps.alTE{1};
        try %for CSI
            hdrs.TM_us               = mrprot.MeasYaps.alTD{1}; 
        catch
            hdrs.TM_us               = []; 
        end
        hdrs.NAveMeas            = mrprot.Config.NAveMeas;
        hdrs.NRepMeas            = mrprot.Config.NRepMeas;
        hdrs.Nucleus             = mrprot.Config.Nucleus;
        try
            hdrs.ScanFrequency       = mrprot.Config.ScanFrequency;
        catch %3T Berlin
            hdrs.ScanFrequency       = mrprot.Config.Frequency;
        end
        hdrs.VectorSize          = mrprot.Config.VectorSize;
        hdrs.DwellTimeSig_ns     = mrprot.Config.DwellTimeSig;
        
          try 
           hdrs.SagPos           = mrprot.MeasYaps.sSpecPara.sVoI.sPosition.dSag;
        catch err
             hdrs.SagPos         = 0;
          end
          
       try
        hdrs.CorPos              = mrprot.MeasYaps.sSpecPara.sVoI.sPosition.dCor;
       catch err
        hdrs.CorPos              = 0;
       end
        
        try 
            hdrs.TraPos          = mrprot.MeasYaps.sSpecPara.sVoI.sPosition.dTra;
        catch err
            hdrs.TraPos          = 0;
        end
            
        %hdrs.TraRot_deg          = mrprot.MeasYaps.sSpecPara.sVoI.sNormal.dTra;
        hdrs.Thickness_mm        = mrprot.MeasYaps.sSpecPara.sVoI.dThickness;
        hdrs.PhaseFOV_mm         = mrprot.MeasYaps.sSpecPara.sVoI.dPhaseFOV;
        hdrs.ReadoutFOV_mm       = mrprot.MeasYaps.sSpecPara.sVoI.dReadoutFOV;    
%         hdrs.PatientPosition     = mrprot.Meas.tPatientPosition;
%         hdrs.FlipAngle_deg       = mrprot.Meas.adFlipAngleDegree;
%         hdrs.TransmittingCoil    = mrprot.Meas.TransmittingCoil;
        
            %VectorSize: # timepoints after removing of OS.
            %True "VectorSize is the 2*VectorSize in the raw
            %dara. However, NColumns is not always equal to
            %2*VectorSize since NColmns= (SamplesBeforeEcho +
            %VectorSize +...) x 2 !!!
                        
         hdrs.VectorSize_OS=2*hdrs.VectorSize;
                        
         %Create additional fields that are not included directly in the hdrs         

         hdrs.AcquisitionTime_ms= hdrs.VectorSize_OS * hdrs.DwellTimeSig_ns *10^(-6);
         %  Bandwidth= [-Freq,+Freq]
         hdrs.Bandwidth_Hz= 2*(1/2)*(1/ hdrs.DwellTimeSig_ns) *10^9;

                        
    end          

    
    end
    
end
                                       
   
  

    %#####################################################
    function degrees= rads2deg(rad)
    
    degrees= rad *180/pi;
    
    end
    
    
    
        
    









        