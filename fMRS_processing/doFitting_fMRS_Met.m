function doFitting_fMRS_Met()
clear; clc; close all;

%check StimType, BOLDType, MMBType
%check nBlocks in reconstruct!


pathName = 'fMRS data path';
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
StimType = {'StimON', 'StimOFF'};      %'StimON', 'StimOFF', 'RestON', 'RestOFF'
if ~isempty(strfind(StimType{1},'Stim'))
    visualType = 'Stim';
elseif ~isempty(strfind(StimType{1},'Rest'))
    visualType = 'Rest';
else
    error('StimType needs to be Stim or Rest');
end
BOLDType = {'_withBOLDCorr128'};  %_woBOLDCorr128, _withBOLDCorr128, _woBOLDCorr64, _woBOLDCorr64#1, _withBOLDCorr64, _withBOLDCorr64#1
orderFMRS = strcat(StimType,BOLDType);%
% orderFMRS = {'StimON_woBOLDCorr128', 'StimOFF_woBOLDCorr128'};
MMBType = '_MMBSummed';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated
subjects = {'3333';};
% subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
%     '4085';'3333';'4012';'4085';'6524'};
prefixes = {'2020-03-10_';};
% prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
%     '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
%     '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
suffixFolder = '_fMRS';

extensionTable = '.table';
extensionCoord = '.coord';
defaultfMRS = 'StimOFF';
defaultSubject = 'XXXX';
defaultSubjectsPath = 'YYYY';
defaultMMB = '_MMBSummed';
defaultLCModelUser = 'jdorst';
outputFileNameBaseWithoutMetabolite_OFF = strcat(defaultSubject, '_StimOFF_woBOLDCorr128');
outputFileNameBaseWithoutMetabolite_ON  = strcat(defaultSubject, '_StimON_woBOLDCorr128');

controlFilesBase = 'fitsettings_fMRS_XXXX_';
controlFilesBaseSuffix = '.control';
outputFileNameBase = strcat(defaultSubject, '_');

%% file paths setup
controlFilesPathRemote = '/Desktop/1H_fMRS/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/1H_fMRS/Output/';
LCModelOutputPath = [localFilePathBase, 'Output/'];
defaultControlFile = strcat(controlFilesBase, defaultfMRS, controlFilesBaseSuffix);

subjectsPath = strcat(prefixes, subjects, suffixFolder);

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath, '/');
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath, '/');
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath, '/');
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath, '/');
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath, '/');


%% process fMRS data and save .RAW file
dataPath = strcat(localFilePathBase,subjectsPath, '/');
for i = 1:length(dataPath)
    currentFolder = num2str(cell2mat(dataPath(i)));
    cd (currentFolder);
    subFolderName = dir(dataPath{i});
    if ~isempty(strfind(StimType{1},'Stim'))
        dataFile = dir('*Stim*.dat');
    elseif ~isempty(strfind(StimType{1},'Rest'))
        dataFile = dir('*Rest*.dat');
    else
        error('StimType needs to be Stim or Rest');
    end
    nBlocks = 5;
%     nBlocks = 10;
    PCSteps = 16;
    reconstruct_1H_fMRS(currentFolder,dataFile.name,1,1,nBlocks,false,PCSteps,[],1,visualType); %currentFolder, fid_id, isMC, isInVivo, nBlocks, nOnAverages, PCSteps, weights, saveResults
    close all; cd ..;
end

%% basic configurations
% LCModelTableFits = strcat(outputFileNameBase, orderFMRS, extensionTable);
% LCModelTableFitsCoord = strcat(outputFileNameBase, orderFMRS, extensionCoord);
LCModelOutputFiles = strcat(outputFileNameBase, orderFMRS, MMBType);
LCModelControlFiles = strcat(controlFilesBase, orderFMRS, MMBType, controlFilesBaseSuffix);
defaultWaterRef = strcat(outputFileNameBase, 'WRef');

%
numberOfFMRS = length(orderFMRS);
numberOfSubjects = length(subjects);

%% do the LCModel fitting
% LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

% do the actual LCModel fitting
parfor indexCurrentSubject = 1:numberOfSubjects
    %% do the LCModel fitting
    LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
    
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    
    currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
    currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
    waterRefFileName = strrep(defaultWaterRef, defaultSubject, subjects{indexCurrentSubject});
    
    %% create the control file series
    for indexFMRS = 1:length(currentControlFiles)
        currentOutputFile = currentOutputFiles{indexFMRS};
        currentFMRS = orderFMRS{indexFMRS};
        currentLCModelOutputFile = LCModelOutputFiles{indexFMRS};
        createLCModelConfigfMRS('fMRS', controlFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, defaultControlFile, ...
            currentFMRS, defaultfMRS, defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFile, currentLCModelOutputFile,...
            waterRefFileName, defaultWaterRef, subjectsPath{indexCurrentSubject}, defaultSubjectsPath, subjects{indexCurrentSubject},...
            defaultSubject, MMBType, defaultMMB);
    end
    
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
end


%% create concentration table
tableConcentrations = [];
isFirstIter = 1;
s = 1;
for indexCurrentSubject = 1:numberOfSubjects
    for indexorderFMRS = 1:length(orderFMRS)
        % retrieve parameters
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
        currentPath = strcat(LCModelOutputPath,subjectsPath{indexCurrentSubject},'/',currentOutputFiles,'.table');
        [c1 c2 c3 c4] = textread(currentPath{indexorderFMRS},'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
            tableConcentrations{1,s} = 'water concentration';
        end
        
        % add ID and Water conc to table
        tableConcentrations{indexCurrentSubject*2+indexorderFMRS-1,1} = strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject}, '_', orderFMRS{indexorderFMRS});
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        tableConcentrations{indexCurrentSubject*2+indexorderFMRS-1,end} = wconc_LCModel;
        % add metabolites to table
        s = 2;
        for j=start:finish
            %add quantified metabolite
            tableConcentrations{indexCurrentSubject*2+indexorderFMRS-1,s} = str2num( c1{j});
            %add CRLB
            tableConcentrations{indexCurrentSubject*2+indexorderFMRS-1,s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end
end

% save table to file
ExpName = strcat('LCModelFit_', num2str(visualType), BOLDType{1}, MMBType);
ExpName = strrep(ExpName,'#1','_1');
exportFileXlsx = [LCModelOutputPath ,strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)

%% calculate percent changes Stim OFF vs Stim ON, save result tables
load(exportFileMat)
[~, rawFile, ~] = fileparts(exportFileMat);
LCModelFitTable = eval(rawFile);
metaboliteNames = LCModelFitTable(1,4:2:end-2);
metaboliteNames = strrep(metaboliteNames,'+','_');

ON = cell2mat(LCModelFitTable(2:2:end,4:2:end-2));
OFF = cell2mat(LCModelFitTable(3:2:end,4:2:end-2));
PercDiff = 100./OFF.*ON-100;
PercDiffMean = mean(PercDiff);
PercDiffStd = std(PercDiff);

%two-sided Wilcoxon signed rank test
for indexOfMetabolite = 1:length(metaboliteNames)
    if isempty(find(~isnan(PercDiff(:,indexOfMetabolite))))
        p(indexOfMetabolite) = NaN;
        h(indexOfMetabolite) = 0;
    else
        [p(indexOfMetabolite),h(indexOfMetabolite)] = signrank(PercDiff(:,indexOfMetabolite));
    end
end

rowNames = strcat(prefixes, subjects);
rowNames = [rowNames; 'Mean'; 'Std'; 'p-Value'; 'h-Value'];
ExpNameDiff = strcat('ConcDiff_', num2str(visualType), BOLDType{1}, MMBType);
ExpNameDiff = strrep(ExpNameDiff,'#1','_1');
ConcentrationsDiff_xx = array2table([PercDiff; PercDiffMean; PercDiffStd; p; h],'rowNames',rowNames,'VariableNames',metaboliteNames);
eval([ExpNameDiff '=ConcentrationsDiff_xx']);
exportFileMatDiff = [LCModelOutputPath ,strcat(ExpNameDiff,'.mat')];
save(exportFileMatDiff,ExpNameDiff)


end