function outputFile = createLCModelMakeBasis_31P_T2(makeBasisFilesPathLocal, subjectsControlFilesPathLocal, ...
        makeBasisFilesBase, defaultLCModelUser, currentLCModelUser, TE, defaultTE);
% Creates the make basis files, given a default make basis file.
% the newly generated files will be placed in the same path as indicated by
% the makeBasisFilesPath.
% The defaultMakeBasisFile has to include the keyword "Default" in the file name.
% A sample input:
% makeBasisFilesPathLocal = 'D:\PAPER\1H_fMRS\Data\Basis_sets/';
% makeBasisFilesBase = 'makeBasis_MMBInd_XXXX.in';

makeBasisDefaultFileID = fopen([makeBasisFilesPathLocal makeBasisFilesBase], 'r');

mkdir(subjectsControlFilesPathLocal);
outputFile = strcat(subjectsControlFilesPathLocal, strrep(makeBasisFilesBase,defaultTE,TE));

makeBasisFileID = fopen(outputFile, 'w');
display(outputFile);

while(~feof(makeBasisDefaultFileID))
    s = fgetl(makeBasisDefaultFileID);
    s = strrep(s, defaultLCModelUser, currentLCModelUser);
%     s = strrep(s, defaultSubjectsPath, subjectsPath);
    s = strrep(s, defaultTE, TE);

    fprintf(makeBasisFileID,'%s\n', s);
end

fclose(makeBasisFileID);
fclose(makeBasisDefaultFileID);

end