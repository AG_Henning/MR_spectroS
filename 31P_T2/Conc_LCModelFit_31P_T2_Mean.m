function Conc_LCModelFit_31P_T2_Mean()

%%%%%%%%%%
% need to run Conc_LCModelFit_31P_T2.m first
% check PathName
%%%%%%%%%%

PathName = 'D:\PAPER\31P_T2_RelaxationTimes\Data\Output\';
% PathName = 'D:\PAPER\31P_T2_RelaxationTimes\Data\Output_Real_Vespa_Original\';
subjects = {'9873';'1090';'3490';'9810';'1658';'2017';'6524';'3009';...
    '6260';'2823';'2455';'4085';'Summed'};
prefixes = {'2019-09-30_';'2019-10-10_';'2019-10-14_';'2019-10-14_';...
    '2019-10-22_';'2019-11-07_';'2019-11-07_';'2019-11-20_';...
    '2019-11-20_';'2019-12-05_';'2020-07-22_';'2020-07-23_';''};
suffixFolder = '_SurfCoil';

numberOfSubjects = length(subjects);
TEs = {'6'; '8'; '11'; '15'; '20'; '30'; '50'; '80'; '150'};  %ms
numberOfTEs = length(TEs);

paths = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    if strcmp(subjects{indexSubject},'Summed')
        paths{indexSubject} = [PathName prefixes{indexSubject} subjects{indexSubject}  '\'];
        filesConc{indexSubject} = strcat(subjects{indexSubject},'_STEAM_Real_noTrunc_Conc_LCModelFit.mat');
        filesConcSTEAM{indexSubject} = strcat(subjects{indexSubject},'_STEAM_Real_noTrunc_Conc_LCModelFit_STEAM.mat');
        filesFWHM{indexSubject} = strcat(subjects{indexSubject},'_STEAM_Real_noTrunc_FWHM_Freq_Fit.mat');
    else
        paths{indexSubject} = [PathName prefixes{indexSubject} subjects{indexSubject} suffixFolder '\'];
        filesConc{indexSubject} = strcat(subjects{indexSubject},'_STEAM_Real_T_noTrunc_Conc_LCModelFit.mat');
        filesConcSTEAM{indexSubject} = strcat(subjects{indexSubject},'_STEAM_Real_T_noTrunc_Conc_LCModelFit_STEAM.mat');
        filesFWHM{indexSubject} = strcat(subjects{indexSubject},'_STEAM_Real_T_noTrunc_FWHM_Freq_Fit.mat');
    end
end

for indexSubject = 1:numberOfSubjects
    load([paths{indexSubject} filesConc{indexSubject}])
    Conc_Real_TE6ms(indexSubject,:) = table2array(Conc_LCModelFit(1,:));
    Conc_Abs_TE6ms(indexSubject,:) = table2array(Conc_LCModelFit(2,:));
    load([paths{indexSubject} filesConcSTEAM{indexSubject}])
    Conc_Real_TE6ms_STEAM(indexSubject,:) = table2array(Conc_LCModelFit_STEAM(1,:));
    Conc_Abs_TE6ms_STEAM(indexSubject,:) = table2array(Conc_LCModelFit_STEAM(2,:));
    load([paths{indexSubject} filesFWHM{indexSubject}])
    FWHM_TE6ms_STEAM(indexSubject,:) = table2array(FWHM(1,:));
    Freq_TE6ms_STEAM(indexSubject,:) = table2array(FWHM(2,:));
    pH_TE6ms_STEAM(indexSubject,:) = abs(table2array(FWHM(3,:)));
end

metaboliteNames = Conc_LCModelFit.Properties.VariableNames;
Conc_Real_TE6ms(Conc_Real_TE6ms==0) = NaN; Conc_Abs_TE6ms(Conc_Abs_TE6ms==0) = NaN;
Conc_Real_TE6ms_STEAM(Conc_Real_TE6ms_STEAM==0) = NaN; Conc_Abs_TE6ms_STEAM(Conc_Abs_TE6ms_STEAM==0) = NaN;
FWHM_TE6ms_STEAM(FWHM_TE6ms_STEAM==0) = NaN; Freq_TE6ms_STEAM(Freq_TE6ms_STEAM==0) = NaN; pH_TE6ms_STEAM(pH_TE6ms_STEAM==0) = NaN;

for indexMet = 1:length(metaboliteNames)
    Conc_Real_TE6ms_Mean(1,indexMet) = nanmean(Conc_Real_TE6ms(1:end-1,indexMet));
    Conc_Real_TE6ms_Std(1,indexMet) = nanstd(Conc_Real_TE6ms(1:end-1,indexMet));
    Conc_Abs_TE6ms_Mean(1,indexMet) = nanmean(Conc_Abs_TE6ms(1:end-1,indexMet));
    Conc_Abs_TE6ms_Std(1,indexMet) = nanstd(Conc_Abs_TE6ms(1:end-1,indexMet));
    Conc_Real_TE6ms_STEAM_Mean(1,indexMet) = nanmean(Conc_Real_TE6ms_STEAM(1:end-1,indexMet));
    Conc_Real_TE6ms_STEAM_Std(1,indexMet) = nanstd(Conc_Real_TE6ms_STEAM(1:end-1,indexMet));
    Conc_Abs_TE6ms_STEAM_Mean(1,indexMet) = nanmean(Conc_Abs_TE6ms_STEAM(1:end-1,indexMet));
    Conc_Abs_TE6ms_STEAM_Std(1,indexMet) = nanstd(Conc_Abs_TE6ms_STEAM(1:end-1,indexMet));
    FWHM_TE6ms_STEAM_Mean(1,indexMet) = nanmean(FWHM_TE6ms_STEAM(1:end-1,indexMet));
    FWHM_TE6ms_STEAM_Std(1,indexMet) = nanstd(FWHM_TE6ms_STEAM(1:end-1,indexMet));
    Freq_TE6ms_STEAM_Mean(1,indexMet) = nanmean(Freq_TE6ms_STEAM(1:end-1,indexMet));
    Freq_TE6ms_STEAM_Std(1,indexMet) = nanstd(Freq_TE6ms_STEAM(1:end-1,indexMet));
    pH_TE6ms_STEAM_Mean(1,indexMet) = nanmean(pH_TE6ms_STEAM(1:end-1,indexMet));
    pH_TE6ms_STEAM_Std(1,indexMet) = nanstd(pH_TE6ms_STEAM(1:end-1,indexMet));
end

Conc_Real_TE6ms(numberOfSubjects+1,:) = Conc_Real_TE6ms_Mean; Conc_Real_TE6ms(numberOfSubjects+2,:) = Conc_Real_TE6ms_Std;
Conc_Abs_TE6ms(numberOfSubjects+1,:) = Conc_Abs_TE6ms_Mean; Conc_Abs_TE6ms(numberOfSubjects+2,:) = Conc_Abs_TE6ms_Std;
Conc_Real_TE6ms_STEAM(numberOfSubjects+1,:) = Conc_Real_TE6ms_STEAM_Mean; Conc_Real_TE6ms_STEAM(numberOfSubjects+2,:) = Conc_Real_TE6ms_STEAM_Std;
Conc_Abs_TE6ms_STEAM(numberOfSubjects+1,:) = Conc_Abs_TE6ms_STEAM_Mean; Conc_Abs_TE6ms_STEAM(numberOfSubjects+2,:) = Conc_Abs_TE6ms_STEAM_Std;
FWHM_TE6ms_STEAM(numberOfSubjects+1,:) = FWHM_TE6ms_STEAM_Mean; FWHM_TE6ms_STEAM(numberOfSubjects+2,:) = FWHM_TE6ms_STEAM_Std;
Freq_TE6ms_STEAM(numberOfSubjects+1,:) = Freq_TE6ms_STEAM_Mean; Freq_TE6ms_STEAM(numberOfSubjects+2,:) = Freq_TE6ms_STEAM_Std;
pH_TE6ms_STEAM(numberOfSubjects+1,:) = pH_TE6ms_STEAM_Mean; pH_TE6ms_STEAM(numberOfSubjects+2,:) = pH_TE6ms_STEAM_Std;

rowNames = {subjects{:},'Mean','Std'};
colNames = metaboliteNames;
Conc_Real_TE6ms = array2table(Conc_Real_TE6ms,'RowNames',rowNames,'VariableNames',colNames);
Conc_Abs_TE6ms = array2table(Conc_Abs_TE6ms,'RowNames',rowNames,'VariableNames',colNames);
Conc_Real_TE6ms_STEAM = array2table(Conc_Real_TE6ms_STEAM,'RowNames',rowNames,'VariableNames',colNames);
Conc_Abs_TE6ms_STEAM = array2table(Conc_Abs_TE6ms_STEAM,'RowNames',rowNames,'VariableNames',colNames);
FWHM_TE6ms_STEAM = array2table(FWHM_TE6ms_STEAM,'RowNames',rowNames,'VariableNames',colNames);
Freq_TE6ms_STEAM = array2table(Freq_TE6ms_STEAM,'RowNames',rowNames,'VariableNames',colNames);
pH_TE6ms_STEAM = array2table(pH_TE6ms_STEAM,'RowNames',rowNames,'VariableNames',colNames);

fileNameConc_Real_TE6ms = fullfile(PathName, 'Conc_Real_TE6ms_LCModelFit');
save(fileNameConc_Real_TE6ms, 'Conc_Real_TE6ms');
fileNameConc_Abs_TE6ms = fullfile(PathName, 'Conc_Abs_TE6ms_LCModelFit');
save(fileNameConc_Abs_TE6ms, 'Conc_Abs_TE6ms');
fileNameConc_Real_TE6ms_STEAM = fullfile(PathName, 'Conc_Real_TE6ms_STEAM_LCModelFit');
save(fileNameConc_Real_TE6ms_STEAM, 'Conc_Real_TE6ms_STEAM');
fileNameConc_Abs_TE6ms_STEAM = fullfile(PathName, 'Conc_Abs_TE6ms_STEAM_LCModelFit');
save(fileNameConc_Abs_TE6ms_STEAM, 'Conc_Abs_TE6ms_STEAM');
fileNameFWHM_TE6ms_STEAM = fullfile(PathName, 'FWHM_TE6ms_STEAM_LCModelFit');
save(fileNameFWHM_TE6ms_STEAM, 'FWHM_TE6ms_STEAM');
fileNameFreq_TE6ms_STEAM = fullfile(PathName, 'Freq_TE6ms_STEAM_LCModelFit');
save(fileNameFreq_TE6ms_STEAM, 'Freq_TE6ms_STEAM');
fileNamepH_TE6ms_STEAM = fullfile(PathName, 'pH_TE6ms_STEAM_LCModelFit');
save(fileNamepH_TE6ms_STEAM, 'pH_TE6ms_STEAM');
end