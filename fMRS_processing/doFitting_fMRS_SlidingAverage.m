function doFitting_fMRS_SlidingAverage()
clear; clc; close all;

% f=get(gca,'Children');
% legend([f(1),f(4)],'All subjects','w/o 5166, 9810, 5269')

fitversion = 'v2';
pathName = 'fMRS SlidingAverage data path';
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);

visualType = 'Rest';      %'Stim', 'Rest'
BOLDType = '_woBOLDCorr';  %_woBOLDCorr128, _withBOLDCorr128, _woBOLDCorr64, _withBOLDCorr64
if fitversion == 'v2'
    MMBType = '_MMBSummed_v2';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated
else
    MMBType = '_MMBSummed';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated
end
nOfSpectra = 32;   %how many spectra should be averaged? Need full PC+MC
nslidingAv = 8;    %how many spectra should be skipped before next averaging
nBlocks = (320 - nOfSpectra)/nslidingAv + 1;    %here 320 averages per .dat file acquired. nBlocks = #spectra after sliding average

BlockNo = cell(1,nBlocks);
for k = 1:nBlocks
    BlockNo{k} = strcat('_Av', num2str(k));
end

% subjects = {'2823';};
subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
    '4085';'3333';'4012';'4085';'6524'};
% prefixes = {'2020-02-18_';};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
    '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
    '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
excludeSubjects = {'5166';'9810';'5269'};
suffixFolder = '_fMRS';

extensionTable = '.table';
extensionCoord = '.coord';
defaultVisual = 'Stim';
defaultSubject = 'XXXX';
defaultSubjectsPath = 'YYYY';
if fitversion == 'v2'
    defaultMMB = '_MMBSummed_v2';
else
    defaultMMB = '_MMBSummed';
end
defaultLCModelUser = 'jdorst';
outputFileNameBaseWithoutMetabolite_OFF = strcat(defaultSubject, '_StimOFF_woBOLDCorr128');
outputFileNameBaseWithoutMetabolite_ON  = strcat(defaultSubject, '_StimON_woBOLDCorr128');

if fitversion == 'v2'
    controlFilesBase = 'fitsettings_v2_XXXX_';
else
    controlFilesBase = 'fitsettings_XXXX_';
end
controlFilesBaseSuffix = '.control';
outputFileNameBase = strcat(defaultSubject, '_');


pathNameBaseline = 'fMRS TimeCourse data path';
sampleCriteriaBaseline = {'Output_v2_MMBSummed', 'Output', 'LCModelConfig'};
[localFilePathBaseBaseline] = pathToDataFolder(pathNameBaseline, sampleCriteriaBaseline);
if fitversion == 'v2'
    localBaselinePath = [localFilePathBaseBaseline, 'Output_v2_MMBSummed/'];
    BaselineFiles = strcat('BaselineConc_',num2str(visualType), BOLDType, MMBType);
else
    localBaselinePath = [localFilePathBaseBaseline, 'Output/'];
    BaselineFiles = strcat('BaselineConc_',num2str(visualType), BOLDType, MMBType);
end

%% file paths setup
controlFilesPathRemote = '/Desktop/1H_fMRS/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/1H_fMRS/Output/';
LCModelOutputPath = [localFilePathBase, 'Output/'];
defaultControlFile = strcat(controlFilesBase, defaultVisual, controlFilesBaseSuffix);

subjectsPath = strcat(prefixes, subjects, suffixFolder);

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath, '/');
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath, '/');
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath, '/');
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath, '/');
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath, '/');


%% process fMRS data and save .RAW file
dataPath = strcat(localFilePathBase,subjectsPath, '/');
% for i = 1:length(dataPath)
%     currentFolder = num2str(cell2mat(dataPath(i)));
%     cd (currentFolder);
%     subFolderName = dir(dataPath{i});
%     if strcmp(visualType,'Stim')
%         dataFile = dir('*Stim*.dat');
%     elseif strcmp(visualType,'Rest')
%         dataFile = dir('*Rest*.dat');
%     else
%         error('visualType needs to be Stim or Rest');
%     end
%     PCSteps = 16;
%     reconstruct_1H_fMRS_SlidingAverage(currentFolder,dataFile.name,1,1,[],1,visualType,nOfSpectra,nslidingAv); %currentFolder, fid_id, isMC, isInVivo, weights, saveResults, visualType, nOfSpectra, nslidingAv
%     close all; cd ..;
% end

%% basic configurations
LCModelOutputFiles = strcat(outputFileNameBase, visualType, BOLDType, MMBType, BlockNo);
LCModelOutputFileswoBaseline = strcat(outputFileNameBase, visualType, BOLDType, MMBType, BlockNo, '_woBaseline');
LCModelControlFiles = strcat(controlFilesBase, visualType, BOLDType, MMBType, BlockNo, controlFilesBaseSuffix);
LCModelControlFileswoBaseline = strcat(controlFilesBase, visualType, BOLDType, MMBType, BlockNo, '_woBaseline', controlFilesBaseSuffix);
defaultWaterRef = strcat(outputFileNameBase, 'WRef');

numberOfSubjects = length(subjects);

%% do the LCModel fitting
% LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

% do the actual LCModel fitting
% for indexCurrentSubject = 1:numberOfSubjects
%     %% do the LCModel fitting
%     LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
%     
%     LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
%         subjectsLCModelOutputPath{indexCurrentSubject});
%     
%     if fitversion == 'v2'
%         currentControlFiles = strrep(LCModelControlFiles, strcat('v2_',defaultSubject), subjects{indexCurrentSubject});
%     else
%         currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
%     end
%     currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
%     waterRefFileName = strrep(defaultWaterRef, defaultSubject, subjects{indexCurrentSubject});
% 
%     %% create the control file series
%     for nBl = 1:nBlocks
%         currentOutputFile = currentOutputFiles{nBl};
%         currentFMRS = strcat(visualType,BOLDType);
% %         MMBType_temp = strrep(MMBType,'_v2','');
%         createLCModelConfigfMRSTimeCourse(controlFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, defaultControlFile, ...
%             currentFMRS, defaultVisual, currentOutputFile,...
%             waterRefFileName, defaultWaterRef, subjectsPath{indexCurrentSubject}, defaultSubjectsPath, subjects{indexCurrentSubject},...
%             defaultSubject, MMBType, defaultMMB, BlockNo{nBl});
%     end
%     
%     fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
%         subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
% end

%%% not useful here, take it from Data_TimeCourse
% % % Create and save table with baseline concentrations fitted with baseline, taken from second block of first REST
% % createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
% %     LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,0)

% % Create and save table from fitted concentrations with baseline, taken from
% % all blocks
% for indexCurrentSubject = 1:numberOfSubjects
% createConcTableBlock(BlockNo,LCModelOutputFiles,defaultSubject,subjects{indexCurrentSubject},...
%     LCModelOutputPath,subjectsPath{indexCurrentSubject},prefixes{indexCurrentSubject},visualType,BOLDType,MMBType,0)
% end

%% subtract baseline from difference spectrum and fit again
%only useful for MMBSummed
if strcmp(MMBType(1:10),'_MMBSummed')
%     LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
%     for indexCurrentSubject = 1:numberOfSubjects
%         %% do the LCModel fitting
%         LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
%         
%         LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
%             subjectsLCModelOutputPath{indexCurrentSubject});
% %         
%         if fitversion == 'v2'
%             currentControlFileswoBaseline = strrep(LCModelControlFileswoBaseline, strcat('v2_',defaultSubject), subjects{indexCurrentSubject});
%         else
%             currentControlFileswoBaseline = strrep(LCModelControlFileswoBaseline, defaultSubject, subjects{indexCurrentSubject});
%         end
%         currentDataFiles = strrep(strcat(outputFileNameBase, visualType, BOLDType, BlockNo), defaultSubject, subjects{indexCurrentSubject});
%         currentOutputFileswoBaseline = strrep(LCModelOutputFileswoBaseline, defaultSubject, subjects{indexCurrentSubject});
%         currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
%         waterRefFileName = strrep(defaultWaterRef, defaultSubject, subjects{indexCurrentSubject});
%         currentDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject}, '/');
%         for nBl = 1:nBlocks
%             %check save option for .mat file in subtractBaseline_fMRS
%             subtractBaseline_fMRS(currentDataPath,subjectsLCModelOutputPath{indexCurrentSubject},currentDataFiles{nBl},currentOutputFileswoBaseline{nBl},'NAA')
%             currentFMRS = strcat(visualType,BOLDType);
%             createLCModelConfigfMRSTimeCourse(controlFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, defaultControlFile, ...
%                 currentFMRS, defaultVisual, currentOutputFileswoBaseline{nBl},...
%                 waterRefFileName, defaultWaterRef, subjectsPath{indexCurrentSubject}, defaultSubjectsPath, subjects{indexCurrentSubject},...
%                 defaultSubject, MMBType, defaultMMB, strcat(BlockNo{nBl}, '_woBaseline'));
%         end
%         fittingLCModel(LCModelCallerInstance, currentControlFileswoBaseline, currentOutputFileswoBaseline, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
%             subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
%         
%     end
    %%% not useful here, take it from Data_TimeCourse
%     % Create and save table with baseline concentrations fitted w/o baseline, taken from second block of first REST
%     % createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
%     %     LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,1)
    
    % Create and save table from fitted concentrations w/o baseline, taken from all blocks
%     for indexCurrentSubject = 1:numberOfSubjects
%         createConcTableBlock(BlockNo,LCModelOutputFiles,defaultSubject,subjects{indexCurrentSubject},...
%             LCModelOutputPath,subjectsPath{indexCurrentSubject},prefixes{indexCurrentSubject},visualType,BOLDType,MMBType,1)
%     end
end


%% create concentration time course from fit with and w/o Baseline
ExpName = strcat('BlockConc_', subjects, '_', num2str(visualType), BOLDType, MMBType);
if exist('excludeSubjects')
%     createConcTimeCourseBoxplot(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'Lac',excludeSubjects,0)
%     createConcTimeCourseBoxplot(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'Glu',excludeSubjects,0)
    %     createConcTimeCourseFigExcluded(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,excludeSubjects,'Lac',0)
    %     createConcTimeCourseFigExcluded(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,excludeSubjects,'Glu',0)
%         createConcTimeCourseFigExcluded(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,excludeSubjects,'Cr',0)
%         createConcTimeCourseFigExcluded(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,excludeSubjects,'PCr',0)
%     createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'Glu',excludeSubjects,0)
%     createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'GSH',excludeSubjects,0)
%     createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'Cr',excludeSubjects,0)
    createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'PCr',excludeSubjects,0)
    if strcmp(MMBType(1:10),'_MMBSummed')
%         createConcTimeCourseFigExcluded(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,excludeSubjects,'Glu',1)
        createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'Glu',excludeSubjects,1)
        createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'Lac',excludeSubjects,1)
        createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'Cr',excludeSubjects,1)
        createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'PCr',excludeSubjects,1)
    end
else
    % createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,'Lac',0)
    % createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,'Glu',0)
    % createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,'Cr',0)
%     createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,'PCr',0)
createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,'Glu','',0)
%     if strcmp(MMBType(1:10),'_MMBSummed')
%         createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,'Lac',1)
%         createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,'Glu',1)
%     end
end
close all

end

function createConcTimeCourseBoxplot(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,metabToFig,excludeSubjects,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
ExpName = strcat(ExpName, woBaseline);
exportFileMat = strcat(subjectsLCModelOutputPath,ExpName,'.mat');

if exist('excludeSubjects') & ~isempty(excludeSubjects)
    n = length(ExpName);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(exportFileMat(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    ExpName([excludeIdx],:) = [];
    exportFileMat([excludeIdx],:) = [];
end

for k = 1:numel(exportFileMat)
    tmp = load(exportFileMat{k});
    dataTimeCourse(:,:,k) = tmp.(ExpName{k});
end

metaboliteNames = dataTimeCourse(1,:,1);
indexMet = strcmp(metaboliteNames,metabToFig);
% idxMet = find(not(cellfun('isempty',indexMet)));
idxMet = find(indexMet == true);

%Baseline concentration
if exist('excludeSubjects') & ~isempty(excludeSubjects)
    BaselineFiles = strcat(BaselineFiles,'_Excluded');
end
baselineConcFile = strcat(localBaselinePath,BaselineFiles);

baselineConcTemp = load(strcat(baselineConcFile,'.mat'));
baselineConc = baselineConcTemp.(BaselineFiles);
[o,~] = size(baselineConc);
for q = 2:o-2
    baselineConcIdxMet(1,q-1) = baselineConc{q,idxMet};
end

% MetConc = cell2mat(squeeze(dataTimeCourse(2:end,idxMet,:)));
MetConc = squeeze(dataTimeCourse(2:end,idxMet,:));

%calculate concentration change of data points with reference to baseline
[a,b] = size(MetConc);
for av = 1:a
    for sub = 1:b
        PercChange(av,sub) = (MetConc{av,sub}-baselineConcIdxMet(1,sub))/baselineConcIdxMet(1,sub)*100;
    end
end

% for k = 1:size(MetConc,1)
%     PercChange_Mean(k) = mean(PercChange(k,:));
%     PercChange_Std(k)  = std(PercChange(k,:));
% end

figure
x = linspace(16*5,(320*5-16*5),37)/60;
positions = 1:length(x)+0.5;
hold on
boxplot(PercChange','Labels',x,'positions',positions)
hold on
% plot([0,320*5/60],[0 0])
plot([0,38],[0 0])
xlabel 't / min'; ylabel 'Concentration change / %.'
if ~isempty(find(~cellfun(@isempty,strfind(ExpName,'Rest'))))
    visualType = 'Rest';
else
    visualType = 'Stim'
end
title ([metabToFig, ' ', visualType,' time course, subjects excluded'])
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
set(gca,'FontWeight','bold');
% xlim([0 320*5/60]);
xlim([0 38])

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:5
%         boxoff  = [boxoff 320/60*i 320/60*i];
        boxoff  = [boxoff 38/5*i 38/5*i];
        if i < 5
%         boxon   = [boxon 320/60*i 320/60*i];
        boxon   = [boxon 38/5*i 38/5*i];
        end
end
for ii = 1:2.5
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
set(gca,'XTick',[])
set(gca, 'XTick', [0 320/60*1.34 320/60*1.34*2 320/60*1.34*3 320/60*1.34*4 320/60*1.34*5]);
set(gca,'XTickLabel',['0 ';'5 ';'10';'15';'20';'25']);
PathName = strcat(LCModelOutputPath,'SlAvTimeCourse/');
if ~isempty(find(~cellfun(@isempty,strfind(ExpName,'Rest'))))
    metabToFig = strcat(metabToFig,'_Rest');
else
    metabToFig = strcat(metabToFig,'_Stim');
end
if ~isempty(find(~cellfun(@isempty,strfind(ExpName,'_v2'))))
    if exist('excludeSubjects') & ~isempty(excludeSubjects)
        fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '_PercChange_Excluded_Boxplot.fig'));
    else
        fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '_PercChange_Boxplot.fig'));
    end
else
    if exist('excludeSubjects') & ~isempty(excludeSubjects)
        fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '_PercChange_Excluded_Boxplot.fig'));
    else
        fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '_PercChange_Boxplot.fig'));
    end
end
savefig(fig2save)
end

function createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,localBaselinePath,BaselineFiles,subjects,metabToFig,excludeSubjects,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
ExpName = strcat(ExpName, woBaseline);
exportFileMat = strcat(subjectsLCModelOutputPath,ExpName,'.mat');

if exist('excludeSubjects') & ~isempty(excludeSubjects)
    n = length(ExpName);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(exportFileMat(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    ExpName([excludeIdx],:) = [];
    exportFileMat([excludeIdx],:) = [];
end

for k = 1:numel(exportFileMat)
    tmp = load(exportFileMat{k});
    dataTimeCourse(:,:,k) = tmp.(ExpName{k});
end

metaboliteNames = dataTimeCourse(1,:,1);
indexMet = strcmp(metaboliteNames,metabToFig);
% idxMet = find(not(cellfun('isempty',indexMet)));
idxMet = find(indexMet == true);

%Baseline concentration
if exist('excludeSubjects') & ~isempty(excludeSubjects)
    BaselineFiles = strcat(BaselineFiles,'_Excluded');
end
baselineConcFile = strcat(localBaselinePath,BaselineFiles);

baselineConcTemp = load(strcat(baselineConcFile,'.mat'));
baselineConc = baselineConcTemp.(BaselineFiles);
[o,~] = size(baselineConc);
for q = 2:o-2
    baselineConcIdxMet(1,q-1) = baselineConc{q,idxMet};
end

[n,~,m] = size(dataTimeCourse);
for k = 2:n
    for l = 1:m
        MetConc(k-1,l) = dataTimeCourse(k,idxMet(1),l);
    end
end

%calculate concentration change of data points with reference to baseline
[a,b] = size(MetConc);
for av = 1:a
    for sub = 1:b
        PercChange(av,sub) = (MetConc{av,sub}-baselineConcIdxMet(1,sub))/baselineConcIdxMet(1,sub)*100;
    end
end

for k = 1:size(MetConc,1)
    PercChange_Mean(k) = mean(PercChange(k,:));
    PercChange_Std(k)  = std(PercChange(k,:));
end

figure
x = linspace(16*5,(320*5-16*5),37)/60;
hold on
errorbar(x,PercChange_Mean,PercChange_Std)
hold on
plot([0,320*5/60],[0 0])
xlabel 't / min'; ylabel 'Concentration change / %.'
if ~isempty(find(~cellfun(@isempty,strfind(ExpName,'Rest'))))
    visualType = 'Rest';
else
    visualType = 'Stim'
end
title ([metabToFig, ' ', visualType,' time course, subjects excluded'])
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
set(gca,'FontWeight','bold');
xlim([0 320*5/60]);

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:5
        boxoff  = [boxoff 320/60*i 320/60*i];
        if i < 5
        boxon   = [boxon 320/60*i 320/60*i];
        end
end
for ii = 1:2.5
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
PathName = strcat(LCModelOutputPath,'SlAvTimeCourse/');
if ~isempty(find(~cellfun(@isempty,strfind(ExpName,'Rest'))))
    metabToFig = strcat(metabToFig,'_Rest');
else
    metabToFig = strcat(metabToFig,'_Stim');
end
if ~isempty(find(~cellfun(@isempty,strfind(ExpName,'_v2'))))
    if exist('excludeSubjects') & ~isempty(excludeSubjects)
        fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '_PercChange_Excluded.fig'));
    else
        fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '_PercChange.fig'));
    end
else
    if exist('excludeSubjects') & ~isempty(excludeSubjects)
        fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '_PercChange_Excluded.fig'));
    else
        fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '_PercChange.fig'));
    end
end
savefig(fig2save)
end

function createConcTimeCourseFigExcluded(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,excludeSubjects,metabToFig,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
ExpName = strcat(ExpName, woBaseline);
exportFileMat = strcat(subjectsLCModelOutputPath,ExpName,'.mat');

n = length(ExpName);
m = length(excludeSubjects);
nstep = 1;
for j = 1:m
    for i = 1:n
        xx = find(~cellfun(@isempty,strfind(exportFileMat(i,1),excludeSubjects{j})));
        if ~isempty(xx)
            excludeIdx(nstep) = i;
            nstep = nstep + 1;
        end
    end
end
ExpName([excludeIdx],:) = [];
exportFileMat([excludeIdx],:) = [];

for k = 1:numel(exportFileMat)
    tmp = load(exportFileMat{k});
    dataTimeCourse(:,:,k) = tmp.(ExpName{k});
end

metaboliteNames = dataTimeCourse(1,:,1);
indexMet = strcmp(metaboliteNames,metabToFig);
% idxMet = find(not(cellfun('isempty',indexMet)));
idxMet = find(indexMet == true);

[n,~,m] = size(dataTimeCourse);
for k = 2:n
    for l = 1:m
        MetConc(k-1,l) = dataTimeCourse(k,idxMet(1),l);
    end
end

for k = 1:size(MetConc,1)
    MetConc_Mean(k) = mean(cell2mat(MetConc(k,:)));
    MetConc_Std(k)  = std(cell2mat(MetConc(k,:)));
end

figure
x = linspace(16*5,(320*5-16*5),37)/60;
hold on
errorbar(x,MetConc_Mean,MetConc_Std)
xlabel 't / min'; ylabel 'Concentration / a.u.'
title ([metabToFig,' time course, subjects excluded'])
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
set(gca,'FontWeight','bold');
xlim([0 320*5/60]);

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:5
        boxoff  = [boxoff 320/60*i 320/60*i];
        if i < 5
        boxon   = [boxon 320/60*i 320/60*i];
        end
end
for ii = 1:2.5
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
PathName = strcat(LCModelOutputPath,'SlAvTimeCourse/');
if ~isempty(find(~cellfun(@isempty,strfind(ExpName,'Rest'))))
    metabToFig = strcat(metabToFig,'_Rest');
end
if ~isempty(find(~cellfun(@isempty,strfind(ExpName,'_v2'))))
    fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '_Excluded.fig'));
else
    fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '_Excluded.fig'));
end
savefig(fig2save)
end

function createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,metabToFig,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
ExpName = strcat(ExpName, woBaseline);

exportFileMat = strcat(subjectsLCModelOutputPath,ExpName,'.mat');

for k = 1:numel(exportFileMat)
    tmp = load(exportFileMat{k});
    dataTimeCourse(:,:,k) = tmp.(ExpName{k});
end

metaboliteNames = dataTimeCourse(1,:,1);
indexMet = strcmp(metaboliteNames,metabToFig);
% idxMet = find(not(cellfun('isempty',indexMet)));
idxMet = find(indexMet == true);

[n,~,m] = size(dataTimeCourse);
for k = 2:n
    for l = 1:m
        MetConc(k-1,l) = dataTimeCourse(k,idxMet(1),l);
    end
end

for k = 1:size(MetConc,1)
    MetConc_Mean(k) = mean(cell2mat(MetConc(k,:)));
    MetConc_Std(k)  = std(cell2mat(MetConc(k,:)));
end

figure
x = linspace(16*5,(320*5-16*5),37)/60;
hold on;
errorbar(x,MetConc_Mean,MetConc_Std)
xlabel 't / min'; ylabel 'Concentration / a.u.'
title ([metabToFig,' time course'])
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
set(gca,'FontWeight','bold');
xlim([0 320*5/60]);

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:5
        boxoff  = [boxoff 320/60*i 320/60*i];
        if i < 5
        boxon   = [boxon 320/60*i 320/60*i];
        end
end
for ii = 1:2.5
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
PathName = strcat(LCModelOutputPath,'SlAvTimeCourse/');
if ~isempty(find(~cellfun(@isempty,strfind(ExpName,'v2'))))
    fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '.fig'));
else
    fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '.fig'));
end
savefig(fig2save)
end

function createConcTableBlock(BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
    LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
tableConcentrations = [];
isFirstIter = 1;
s = 1;
% for indexCurrentSubject = 1:numberOfSubjects
    for nBl = 1:length(BlockNo)     
        %         retrieve parameters
        currentOutputFiles = strrep(strcat(LCModelOutputFiles,woBaseline), defaultSubject, subjects);
        currentPath = strcat(LCModelOutputPath,subjectsPath,'/',currentOutputFiles{nBl},'.table');
        [c1 c2 c3 c4] = textread(currentPath,'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
            tableConcentrations{1,s} = 'water concentration';
        end
        
        %         add ID and Water conc to table
        tableConcentrations{nBl+1,1} = strcat(prefixes, subjects, BlockNo{nBl});
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        tableConcentrations{nBl+1,end} = wconc_LCModel;
        %         add metabolites to table
        s = 2;
        for j=start:finish
            %             add quantified metabolite
            tableConcentrations{nBl+1,s} = str2num( c1{j});
            %             add CRLB
            tableConcentrations{nBl+1,s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end
% end

% % calculate mean, std
% [n,m] = size(tableConcentrations);
% subArray = cell2mat(tableConcentrations(2:end,2:end));
% subArrayON1 = subArray(3:10:end,:);
% subArrayON2 = subArray(4:10:end,:);
% subArrayON3 = subArray(7:10:end,:);
% subArrayON4 = subArray(8:10:end,:);
% subArrayON = [subArrayON1; subArrayON2; subArrayON3; subArrayON4];
% meanValueON = mean(subArrayON);
% stdValueON = std(subArrayON);
% tableConcentrations{n+1,1} = 'meanON';
% tableConcentrations{n+2,1} = 'stdON';
% for indexOfMetab = 1:m-1
%     tableConcentrations{n+1,indexOfMetab+1} = meanValueON(1,indexOfMetab);
%     tableConcentrations{n+2,indexOfMetab+1} = stdValueON(1,indexOfMetab);
% end
% subArrayOFF1 = subArray(5:10:end,:);
% subArrayOFF2 = subArray(6:10:end,:);
% subArrayOFF3 = subArray(9:10:end,:);
% subArrayOFF4 = subArray(10:10:end,:);
% subArrayOFF = [subArrayOFF1; subArrayOFF2; subArrayOFF3; subArrayOFF4];
% meanValueOFF = mean(subArrayOFF);
% stdValueOFF = std(subArrayOFF);
% tableConcentrations{n+3,1} = 'meanOFF';
% tableConcentrations{n+4,1} = 'stdOFF';
% for indexOfMetab = 1:m-1
%     tableConcentrations{n+3,indexOfMetab+1} = meanValueOFF(1,indexOfMetab);
%     tableConcentrations{n+4,indexOfMetab+1} = stdValueOFF(1,indexOfMetab);
% end

% save table to file
ExpName = strcat('BlockConc_', subjects, '_', num2str(visualType), BOLDType, MMBType, woBaseline);
exportFileXlsx = [LCModelOutputPath,subjectsPath,'/',strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath,subjectsPath,'/',strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)
end

function createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
    LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
tableConcentrations = [];
isFirstIter = 1;
s = 1;
for indexCurrentSubject = 1:numberOfSubjects
    for nBl = 2     %take baseline from second block of first REST
        % retrieve parameters
        currentOutputFiles = strrep(strcat(LCModelOutputFiles,woBaseline), defaultSubject, subjects{indexCurrentSubject});
        currentPath = strcat(LCModelOutputPath,subjectsPath{indexCurrentSubject},'/',currentOutputFiles{nBl},'.table');
        [c1 c2 c3 c4] = textread(currentPath,'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
            tableConcentrations{1,s} = 'water concentration';
        end
        
        % add ID and Water conc to table
        tableConcentrations{indexCurrentSubject+1,1} = strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject}, BlockNo{nBl});
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        tableConcentrations{indexCurrentSubject+1,end} = wconc_LCModel;
        % add metabolites to table
        s = 2;
        for j=start:finish
            %add quantified metabolite
            tableConcentrations{indexCurrentSubject+1,s} = str2num( c1{j});
            %add CRLB
            tableConcentrations{indexCurrentSubject+1,s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end
end

%calculate mean, std
[n,m] = size(tableConcentrations);
subArray = cell2mat(tableConcentrations(2:end,2:end));
meanValue = mean(subArray);
stdValue = std(subArray);
tableConcentrations{n+1,1} = 'mean';
tableConcentrations{n+2,1} = 'std';
for indexOfMetab = 1:m-1
    tableConcentrations{n+1,indexOfMetab+1} = meanValue(1,indexOfMetab);
    tableConcentrations{n+2,indexOfMetab+1} = stdValue(1,indexOfMetab);
end

% save table to file
ExpName = strcat('BaselineConc_', num2str(visualType), BOLDType, MMBType, woBaseline);
exportFileXlsx = [LCModelOutputPath ,strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)
end