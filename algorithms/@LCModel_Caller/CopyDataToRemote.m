function this = CopyDataToRemote( this, localPath, remoteDirectory, fileName)
% Copy a given file from the local computer to the remote LCModel computer
% - localPath: the path on the local computer where the desired file is
% - remoteDirectory:  the path on the remote computer where the desired
% file should be stored, relative to the 'homePath'
% - fileName: the file which should be moved. Indicate also the extension.
%
% % Sample declarations would be
% fileName = 'fitsettings_Summed_Averaged_TE24_Cre.control';
% localPath =  'D:/Software/Spectro Data/MM/LCModelConfig/';
% remoteDirectory = '/Desktop/MM/LCModelConfig/';
%
% Author: Tamas Borbath
% version: 20 July 2018
if(nargin  ~=  4)
    error('Error: FitSpectrum requires 3 input arguments: localPath, remoteDirectory, fileName .');
end

% create the folder if it does not exist. 
% The command does nothing if the folder exists.
[this.connection, res] = sshfrommatlabissue(this.connection, ['mkdir ' this.homePath remoteDirectory]);

sftpfrommatlab(this.LCModelUser, this.LCModelIP, this.LCModelPassword, [localPath fileName], [this.homePath remoteDirectory fileName])

end

