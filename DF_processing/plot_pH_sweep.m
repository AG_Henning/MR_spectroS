function plot_pH_sweep()

use_hCs = false;

outputFolder = 'Basis_sets\sLASER_new_DF_pH_sweep\TE24\';
pathName = 'DF data path';
sampleCriteria = {outputFolder};
data_path = pathToDataFolder(pathName, sampleCriteria);

filepath = [data_path outputFolder];
if use_hCs
    figure;
    subplot(1,2,1)
    metaboliteName = 'Homocarnosine';
    metaboliteAbbr = 'hCs';
else
    hold on
    subplot(1,2,2)
    metaboliteName = 'Histidine';
    metaboliteAbbr ='hist';
end
pH_values =6.91:0.04:7.15;

hold on
for pH_value = pH_values
    filename = [metaboliteAbbr '_imidazole_pH_' sprintf('%.2f', pH_value) '.RAW'];
    [dataComplex, bandwidth, scanFrequency, ~, ~] = ImportLCModelBasis(filepath, filename, false, '1H');
    
    bw     = linspace(-bandwidth/2,bandwidth/2,length(dataComplex)*8);
    ppmVector    = bw/(scanFrequency) + 4.7;
    plot(ppmVector,real(fftshift(fft(dataComplex,length(dataComplex)*8))),'LineWidth',1);
end

% deltaChar = char(948);
% it doesn't take the ASCII character
xlabel('\delta (ppm)');
xlim([6.9 8.2])
set(gca,'xDir','reverse');
title([metaboliteName ' pH Dependence']);
set(gca,'LineWidth',1);
yticks([])
ylabel('(arb. unit)')
% set(gca,'ytick',[]);
set(gca,'fontsize',14);
set(gca,'FontWeight','bold');

pH_valueString = {};
for index_pH = 1: length(pH_values)
    pH_valueString{index_pH} = sprintf('pH = %.2f', pH_values(index_pH));
end
legend(pH_valueString,'Location', 'best', 'fontsize',12)