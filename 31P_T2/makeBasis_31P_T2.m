function makeBasis_31P_T2
clear; clc; close all;

%%%%%%%%%
%Check TEPath, makeBasisFilesBase
%%%%%%%%%


pathName = 'T2 31P Paper Data';
sampleCriteria = {'Basis_sets'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);

TEs = {6; 8; 11; 15; 20; 30; 50; 80; 150};  %ms

extensionIn = '.in';
defaultTE = 'XXXX';
defaultLCModelUser = 'jdorst';

% Apply individual linewidths to VeSPA metabolite simulations
% basisSetApplyLineWidth_31P_T2(TEs)

%% file paths setup
makeBasisFilesPathRemote = '/Desktop/31P_T2_Paper/Basis_sets/';
makeBasisFilesPathLocal =  [localFilePathBase, 'Basis_sets/'];

%%%%%%%%%%%%%%
%%%%%%%%%%%%%%
% TEPath = strcat('STEAM_Ideal_JD_TE',cellfun(@num2str,TEs,'un',0), 'ms_TM5ms/');
% TEPath = strcat('STEAM_Ideal_TE',cellfun(@num2str,TEs,'un',0), 'ms_TM5ms/');
TEPath = strcat('STEAM_Real_TE',cellfun(@num2str,TEs,'un',0), 'ms_TM5ms/');
LCModelBasisOutputPath = strcat(localFilePathBase, 'Basis_sets/',TEPath,'makeBasisFiles/');

% makeBasisFilesBase = 'makeBasis_STEAM_Ideal_JD_XXXX_TM5ms_LW';
% makeBasisFilesBase = 'makeBasis_STEAM_Ideal_XXXX_TM5ms_LW';
% % % % makeBasisFilesBase = 'makeBasis_STEAM_Ideal_XXXX_TM5ms_LW_Summed';
% % % % makeBasisFilesBase = 'makeBasis_STEAM_Ideal_XXXX_TM5ms_LW_noBCoupl';
% % % % makeBasisFilesBase = 'makeBasis_STEAM_Ideal_XXXX_TM5ms_LW_noAGCoupl';
% % % % makeBasisFilesBase = 'makeBasis_STEAM_Ideal_XXXX_TM5ms_LW_noBCoupl_noAGCoupl';
makeBasisFilesBase = 'makeBasis_STEAM_Real_XXXX_TM5ms_LW';
%%%%%%%%%%%%%%
%%%%%%%%%%%%%%

TEControlFilesPathRemote = strcat(makeBasisFilesPathRemote, TEPath,'makeBasisFiles/');
TEControlFilesPathLocal = strcat(makeBasisFilesPathLocal, TEPath,'makeBasisFiles/');

numberOfTEs = length(TEs);

%% do the LCModel fitting
% % % LCModelCallerInstance = startLCModel(makeBasisFilesPathRemote, TEControlFilesPathRemote, LCModelBasisOutputPath);

%% create the make basis file series
for indexCurrentTE = 1:numberOfTEs
    %give subject specific paths to LCModelCallerInstance
    LCModelCallerInstance = startLCModel(makeBasisFilesPathRemote, TEControlFilesPathRemote{indexCurrentTE}, LCModelBasisOutputPath{indexCurrentTE});
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(makeBasisFilesPathRemote, TEControlFilesPathRemote{indexCurrentTE}, ...
        LCModelBasisOutputPath{indexCurrentTE});
    
    %create makeBasisFilesBase.in and save them locally
    createLCModelMakeBasis_31P_T2(makeBasisFilesPathLocal, TEControlFilesPathLocal{indexCurrentTE}, ...
        strcat(makeBasisFilesBase,extensionIn), defaultLCModelUser, ...
        LCModelCallerInstance.LCModelUser, strcat('TE',num2str((TEs{indexCurrentTE})),'ms'), defaultTE);
    
    % save makeBasis in LCModel Basis_sets Folder, create Basis sets and
    % save created .basis, .ps files locally
    copyFileLocally = true;
    currentMakeBasisFile = strrep(strcat(makeBasisFilesBase,extensionIn),defaultTE,strcat('TE',num2str((TEs{indexCurrentTE})),'ms'));
    currentBasisFileName = strrep(makeBasisFilesBase,defaultTE,strcat('TE',num2str((TEs{indexCurrentTE})),'ms'));
    LCModelCallerInstance.CreateBasisSet(TEControlFilesPathLocal{indexCurrentTE}, TEControlFilesPathRemote{indexCurrentTE}, ...
        currentMakeBasisFile, copyFileLocally, currentBasisFileName);
    
end

fclose all;
end