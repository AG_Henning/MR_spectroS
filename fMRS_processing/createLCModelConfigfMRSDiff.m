function outputFile = createLCModelConfigfMRSDiff(downField_MM_Met, controlFilesPath, outputControlFilesPath, defaultControlFile, ...
    currentfMRS, defaultfMRS, defaultLCModelUser, currentLCModelUser, spectrumFileName, defaultSpectrum,...
    waterRefFileName, defaultWaterRef, subjectsPath, defaultSubjectsPath, subjectID, defaultSubject, MMBType, defaultMMB, fMRSDataSuff)
% Intro comments need to be rewritten
% Creates the control files for the TE series, given a default control file.
% the newly generated files will be placed in the same path as indicated by
% the controlFilesPath.
% downField_MM_Met can be: 'DF' 'MM', 'Met' or 'pH'
% A sample input:
% controlFilesPath = 'Fitsettings/MMocc_noExtraMM_06knots/';
% defaultControlFile = 'fitsettingsDefault.control'

controlFileSuffix = '.control';

fitSettingsDefaultFileID = fopen([controlFilesPath defaultControlFile], 'r');

mkdir(outputControlFilesPath);
if defaultControlFile(1:29) == 'fitsettings_fMRS_XXXX_StimOFF'
    %     if ~isempty(strfind(MMBType,'_Excluded'))
    %         outputFile = strcat(outputControlFilesPath, 'fitsettings_fMRS_', spectrumFileName, '_Excluded', controlFileSuffix);
    %     else
    outputFile = strcat(outputControlFilesPath, 'fitsettings_fMRS_', spectrumFileName, controlFileSuffix);
    %     end
elseif defaultControlFile(1:32) == 'fitsettings_fMRS_v2_XXXX_StimOFF'
    %     if ~isempty(strfind(MMBType,'_Excluded'))
    %         outputFile = strcat(outputControlFilesPath, 'fitsettings_fMRS_', spectrumFileName, '_Excluded', controlFileSuffix);
    %     else
    outputFile = strcat(outputControlFilesPath, 'fitsettings_fMRS_', spectrumFileName, controlFileSuffix);
    %     end
else
    outputFile = strcat(outputControlFilesPath, defaultControlFile);
    outputFile = strrep(outputFile, 'Difference_XXXX_StimOFF', spectrumFileName);
end

if exist('fMRSDataSuff', 'var')
    spectrumFileName = strcat(fMRSDataSuff, spectrumFileName);
end

fitSettingsFileID = fopen(outputFile, 'w');
display(outputFile);

while(~feof(fitSettingsDefaultFileID))
    s = fgetl(fitSettingsDefaultFileID);
    k = strfind(s,'FILBAS');
    currentfMRSextended = strcat(currentfMRS, MMBType);
    if ~isempty(k) %&& (strcmp(subjectID(end-3:end),'Diff') || strcmp(subjectID(end-12:end),'Diff_Filtered'))
        if strcmp(subjectID(end-3:end),'Diff')
            subjectIDtemp = strcat(subjectID, 'erence');
        elseif strcmp(subjectID(end-12:end),'Diff_Filtered')
            subjectIDtemp = strcat(subjectID(1:end-13), 'Difference');
        else
            subjectIDtemp = subjectID;
        end
        s = strrep(s, defaultLCModelUser, currentLCModelUser);
        s = strrep(s, defaultfMRS, currentfMRS);
        s = strrep(s, defaultSubjectsPath, subjectsPath);
        s = strrep(s, defaultSpectrum, spectrumFileName);
        s = strrep(s, defaultWaterRef, waterRefFileName);
        s = strrep(s, defaultSubject, subjectIDtemp);
    else
        s = strrep(s, defaultLCModelUser, currentLCModelUser);
        s = strrep(s, defaultfMRS, currentfMRSextended);
        s = strrep(s, defaultSubjectsPath, subjectsPath);
        if ~isempty(strfind(s,'FILRAW'))
            s = strrep(s, defaultSpectrum, strrep(spectrumFileName,'#','_'));
        else
            s = strrep(s, defaultSpectrum, spectrumFileName);
        end
        s = strrep(s, defaultWaterRef, waterRefFileName);
        s = strrep(s, defaultSubject, subjectID);
        s = strrep(s, defaultMMB, MMBType);
    end
    
    fprintf(fitSettingsFileID,'%s\n', s);
end
fclose(fitSettingsFileID);
fclose(fitSettingsDefaultFileID);
end