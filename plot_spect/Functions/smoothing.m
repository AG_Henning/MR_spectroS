function varargout = smoothing(varargin)
% SMOOTHING MATLAB code for smoothing.fig
%      SMOOTHING, by itself, creates a new SMOOTHING or raises the existing
%      singleton*.
%
%      H = SMOOTHING returns the handle to a new SMOOTHING or the handle to
%      the existing singleton*.
%
%      SMOOTHING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SMOOTHING.M with the given input arguments.
%
%      SMOOTHING('Property','Value',...) creates a new SMOOTHING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before smoothing_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to smoothing_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help smoothing

% Last Modified by GUIDE v2.5 08-Dec-2016 00:06:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @smoothing_OpeningFcn, ...
                   'gui_OutputFcn',  @smoothing_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before smoothing is made visible.
function smoothing_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to smoothing (see VARARGIN)

% Choose default command line output for smoothing
handles.output = hObject;

f = figure('Visible','off');
handles.figure = f;
handles.signal = [];

if nargin == 4
    handles.hInput= varargin{1};
end


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes smoothing wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = smoothing_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function smoothingSlider_Callback(hObject, eventdata, handles)
% hObject    handle to smoothingSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

p    =  get(hObject,'Value');
this =  handles.hInput.MRSpectroMatrix.getCurrent;

nAve = handles.hInput.SelectedDisplayValues.selectedAverage;
nMix = handles.hInput.SelectedDisplayValues.selectedMix;

data = this.Data{1};
data = squeeze(data(:,1,1,1,1,1,1,1,nMix,1,1,nAve));

signal = MR_spectroS.smoothArray(data, p , true);
%save it to handles in order to use it later! 
handles.signal = signal;
guidata(handles.figure1,handles)   
        

function smoothingEdit_Callback(hObject, eventdata, handles)
% hObject    handle to smoothingEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of smoothingEdit as text
%        str2double(get(hObject,'String')) returns contents of smoothingEdit as a double

%Get a copy of the data with the command data = guidata(object_handle).
%Make the desired changes to data.
%Save the changed version of data with the command guidata(object_handle,data).

p = str2double(get(hObject,'String'));
set(handles.smoothingSlider,'Value',p);
smoothingSlider_Callback(handles.smoothingSlider,[],handles);



% --- Executes on button press in applyButton.
function applyButton_Callback(hObject, eventdata, handles)
% hObject    handle to applyButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

this         = handles.hInput.MRSpectroMatrix.getCurrent;
p            = get(handles.smoothingSlider,'Value');
this         = this.SplineSmoothing(p);

this.Parameter.ReconFlags.isSplinedSmoothed = p;
handles.hInput.MRSpectroMatrix.Save(this);
displaySpect(handles.hInput);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% --- Executes during object creation, after setting all properties.
function smoothingEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to smoothingEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function smoothingSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to smoothingSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
