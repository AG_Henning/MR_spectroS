function plotFitDFWithOffset()

pathNameMM = 'DF data path';
sampleCriteriaMM = {'OutputAminoAcids\New_WideRange\'};
[localFilePathBaseMM] = pathToDataFolder(pathNameMM, sampleCriteriaMM);
pathNameDF = 'DF data path';
sampleCriteriaDF = {'Output'};
[localFilePathBaseDF] = pathToDataFolder(pathNameDF, sampleCriteriaDF);

filenameDF= 'Summed_Averaged_TE24.coord';
filenameMM= 'Summed_Averaged_MMBaseline_TE24.coord';

fullPathMMFit = [localFilePathBaseMM 'OutputAminoAcids\New_WideRange\' filenameMM];
fullPathDF = [localFilePathBaseDF 'Output\' filenameDF];

% close all;
figure;
offset = 0.5;

colors = {[0 0.7 0]; [0 0 0]; [0 0 1]; [1 0 0]; [0 0.5 0.5]; };
plotId1 = plotSpectrum(fullPathMMFit, 0, colors(1:2), false);
plotId2 = plotSpectrum(fullPathDF, offset, colors(3:4), true);


xlim([5.5 9.4]);
ylim([-0.4 1.1+offset]);
set(gca,'xDir','reverse')

set(gca,'ytick',[])
xlabel('[ppm]');
title('Spectrum with Fit vs. Double Inversion Spectrum')
end

function plotId = plotSpectrum(FileNameFull, offset, colors, plotFit)
    %[c1 c2 c3 c4] = textread(strcat(PathName,FileName{i}),'%s %s %s %s');
    c          = textread(strcat(FileNameFull),'%s');
    nOfPoints  = find(strcmp(c ,'points'),1);
    nOfPoints  = str2num (c{nOfPoints-1});
    
    %ppm axis
    indexCoordinates   = find(strcmp(c,'ppm-axis'))+3;
    endOfCoordinates     = nOfPoints+indexCoordinates-1;
    ppmVector = str2double( c(indexCoordinates:endOfCoordinates,1));
    
    %phaseData
    indexCoordinates   = find(strcmp(c,'phased'))+4;
    endOfCoordinates     = nOfPoints+indexCoordinates-1;
    phasedData = str2double( c(indexCoordinates:endOfCoordinates,1));
    
    %fit data
    indexCoordinates   = find(strcmp(c,'fit'))+5;
    endOfCoordinates     = nOfPoints+indexCoordinates-1;
    fitData = str2double( c(indexCoordinates:endOfCoordinates,1));
    
    %background
    indexCoordinates   = find(strcmp(c,'background'))+3;
    endOfCoordinates     = nOfPoints+indexCoordinates-1;
    baselineData = str2double( c(indexCoordinates:endOfCoordinates,1));
    
    
    indexDF = find((ppmVector > 5.5), 1, 'last');
    % search only based on DF
    maxValueSample = max(phasedData(1:indexDF));
    
    %plotting
    hold on
    if plotFit
        plotId = plot(ppmVector, phasedData / maxValueSample + offset, 'color', colors{1});
        plotId = plot(ppmVector, fitData / maxValueSample + offset, 'color', colors{2});
    else
        plotId = plot(ppmVector(1:indexDF), phasedData(1:indexDF) / maxValueSample + offset, 'color', colors{1});
    end
    
    set(plotId(1),'LineWidth',1);
    set(gca,'fontsize',14);
end