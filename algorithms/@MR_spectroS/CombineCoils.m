function this =  CombineCoils(this, weights)

if this.Parameter.ReconFlags.isCoilsCombined
    error('The coils are already combined')
end

if size(this.Data{1},this.coil_dim)==1
    error('The data contains only 1 coil')
end

if ~this.Parameter.ReconFlags.isAveraged
    warning('The data should be averaged fisrt')
end;

if ~this.Parameter.ReconFlags.isSorted
    error('The data has to be sorted first')
end

if strcmp(this.Parameter.CoilCombSettings.algorithm,'none')
    error('Algorithm for coil combination should be set')
else
    selectedAlgorithm         = this.Parameter.CoilCombSettings.algorithm;
end

if exist('weights', 'var')
    if length(weights) == size(this.Data{1},this.coil_dim)
        weightsAvailable = true;
    else
        weightsAvailable = false;
    end
else
    weightsAvailable = false;
end

oldData                  = this.Data{1};
nCoils                   = size(this.Data{1}, this.coil_dim);
nAverages                = size(this.Data{1}, this. meas_dim);


% if ref scans are available try to determine the weighting factors
% on the water signal
if size(oldData,this.mix_dim)==2
    selectedMix = 2;
    
else
    selectedMix = 1; % hopefully SNR of mix one FIDs is good enough
end

if weightsAvailable
    % do nothing and set selectedAlgorithm to 'svd'
    selectedAlgorithm = 'svd';
else
    %calculate weights
    switch selectedAlgorithm
        %//////////////////////////////////////////////////////////////////////////
        case 'gls'
            %//////////////////////////////////////////////////////////////////////////
            if isempty(indecesOfPeakOfInterest)
                error(' indecesOfPeakOfInterest should be set ');
            end
            
            if isempty(indecesOfNoiseArea)
                error(' indecesOfNoiseArea should be set ');
            end
            
            
            if isempty(this.Parameter.CoilCombSettings.snrDefinition)
                error('Please select a snrDefinition')
            end
            
            indecesOfPeakOfInterest   = this.Parameter.CoilCombSettings.indecesOfPeakOfInterest;
            indecesOfNoiseArea        = this.Parameter.CoilCombSettings.indecesOfNoiseArea;
            snrDefinition             = this.Parameter.CoilCombSettings.snrDefinition;
            
            
            % Generalized Least Squares approach
            %####################################################################################
            %An, L.; Willem van der Veen, J.; Li, S.; Thomasson, D. M. & Shen, J. (2012),
            %'Combination of multichannel single-voxel MRS signals using generalized least squares',
            %Journal of Magnetic Resonance Imaging.
            %####################################################################################
            
            % S( #coils, #nAverages): sensitivity of each coil, in the original paper is the area of
            % PSI (#coils, #coils, #nAverages): correlation noise matrix
            % The weights of each coil are measured for every average
            
            
            S= zeros ( nCoils, nAverages);
            PSI=zeros( nCoils, nCoils, nAverages);
            
            
            for indexOfAverage=1:nAverages
                
                fidsTD= squeeze(oldData(:, 1, 1, :, 1, 1, 1, 1, selectedMix, 1, 1, indexOfAverage));
                fidsFD= fftshift( fft(fidsTD, [], 1), 1)'; %(#coils, #timepoints)
                
                fidsTDPSI= squeeze(oldData(:, 1, 1, :, 1, 1, 1, 1, 1, 1, 1, indexOfAverage));
                fidsFDPSI= fftshift( fft(fidsTD, [], 1), 1)'; %(#coils, #timepoints)
                
                PSI(:,:,indexOfAverage)= fidsFDPSI(:,indecesOfNoiseArea)*fidsFDPSI(:,indecesOfNoiseArea)'; %PSI= e* conj.trans(e)
                
                switch snrDefinition
                    
                    case 1
                        realS(:,indexOfAverage)= sum(real(fidsFD(:,indecesOfPeakOfInterest)),2)./(std(real(fidsFDPSI(:,indecesOfNoiseArea))',1))';
                        imagS(:,indexOfAverage)= sum(imag(fidsFD(:,indecesOfPeakOfInterest)),2)./(std(imag(fidsFDPSI(:,indecesOfNoiseArea))',1))';
                        S=realS+i*imagS;
                        
                    case 2
                        
                        S(:,indexOfAverage)    =  sum(fidsFD(:,indecesOfPeakOfInterest),2);
                end
                
            end
            
            %//////////////////////////////////////////////////////////////////////////
        case 'svd'
            %//////////////////////////////////////////////////////////////////////////
            % this can be found in Bydder et al. MRI (26), 847-850, 2008
            
            
            weights = zeros(nCoils,1); %the weighting factor has to be
            %the same for all the averages
            
            %         weights = zeros(nCoils,nAverages);
            %         if this.Parameter.AverageSettings.nBlocks==1
            %          weights = zeros(nCoils,nAverages+1);
            %         end
            
            %for indexOfAverage=1:nAverages
            
            H= squeeze(sum(oldData(:, 1, 1, :, 1, 1, 1, 1, selectedMix, 1, 1, :),12));
            
            HH=H'*H;
            
            [~, ~, V]            = svd(HH);
            
            weights(:,1) = -V(:,1)';
            weights      = weights/sum(abs(weights))  %normalize them
            weights = weights*ones(1,nAverages);
            %end
    end
end


%//////////////////////////////////////////////////////////////////////////
%              Reconstruction
%//////////////////////////////////////////////////////////////////////////
switch selectedAlgorithm
    
    
    %//////////////////////////////////////////////////////////////////////////
    case 'gls'
        %//////////////////////////////////////////////////////////////////////////
        % C= inv( S'*inv(PSI)*S )* S'inv(PSI)* D
        
        newDataSize                 = size(oldData);
        newDataSize(this.coil_dim)  = 1;
        newData                     = zeros(newDataSize);
        
        for indexOfAverage=1:nAverages
            W= inv( S(:,indexOfAverage)'* inv(PSI(:,:,indexOfAverage))*S(:,indexOfAverage)) * S(:,indexOfAverage)'*inv(PSI(:,:,indexOfAverage));
            
            for indexOfMix=1:size(oldData,this.mix_dim)
                D= squeeze(oldData(:, 1, 1, :, 1, 1, 1, 1, indexOfMix, 1, 1,indexOfAverage));
                D= fftshift( fft(D, [], 1), 1)';
                C=W*D;
                %C=conj(C); %The fid should be shifted by Pi/2, I do not know why!
                
                newData(:, 1, 1, 1, 1, 1, 1, 1, indexOfMix, 1, 1,indexOfAverage)=...
                    ifft (ifftshift(C));
            end         
        end
        
        %//////////////////////////////////////////////////////////////////////////
    case 'svd'
        %//////////////////////////////////////////////////////////////////////////
        
        newDataSize                 = size(oldData);
        newDataSize(this.coil_dim)  = 1;
        newData                     = zeros(newDataSize);
        
        for indexOfAverage=1:nAverages
            for indexOfMix=1:size(oldData,this.mix_dim) 
                newData(:, 1, 1, 1, 1, 1, 1, 1, indexOfMix, 1, 1,indexOfAverage)=...
                    squeeze(oldData(:, 1, 1, :, 1, 1, 1, 1, indexOfMix, 1, 1,indexOfAverage))*weights(:,indexOfAverage);
            end 
        end     
        
end

this.Parameter.CoilCombSettings.weights = weights;
this.Data{1}                                = newData;
this.Parameter.ReconFlags.isCoilsCombined   = true;



