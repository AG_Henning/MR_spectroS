function plot_All_Fit_Metabolites(FileName, PathName, downField_MM_Met, saveFigures, displayBaseline)

if ~exist('FileName', 'var')
    [FileName,PathName] = uigetfile('*.coord','Please select .coord files from LCModel','Multiselect','on');
end
%
if ~iscell(FileName)
    FileName= {FileName};
end



if ~exist('downField_MM_Met','var')
%     downField_MM_Met = 'Met moiety';
    downField_MM_Met = 'Met moiety fMRS';
%     downField_MM_Met = 'DF';
%     downField_MM_Met = 'MM';
% 	downField_MM_Met = 'fit';
end

overviewPlots = false;

if ~exist('saveFigures','var')
    saveFigures = true;
end

if ~exist('displayBaseline','var')
    displayBaseline = false;
end

switch downField_MM_Met
    case 'Met moiety'
        metaboliteNames = {'Asp' 'Cr' 'Cr_CH2' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'Lac' 'mI' 'NAA_as' 'NAA_ac' 'NAAG' 'Scyllo' 'Tau' 'tCho_P'};
        metaboliteLabels = {'Asp';'tCr(CH_3)';'tCr(CH_2)';'GABA';'Glu';'Gln';'GSH';'Glyc';'Lac';'mI';'NAA(CH_2)';'NAA(CH_3)';'NAAG';'Scyllo';'Tau';'tCho+';};
        xLimits = [0.5 4.097];
        offsetMetabolite = 0.1;
    case 'Met moiety fMRS'
%         metaboliteNames = {'Asp' 'Cr' 'PCr' 'GABA' 'Gln' 'Glu' 'GPC' 'GSH' 'Glyc' 'Lac' 'mI' 'NAA' 'NAAG' 'PE' 'Scyllo' 'Tau'};
%         metaboliteLabels = {'Asp';'Cr';'PCr';'GABA';'Gln';'Glu';'GPC';'GSH';'Glyc';'Lac';'mI';'NAA';'NAAG';'PE';'Scyllo';'Tau';};
        metaboliteNames = {'Asc' 'Asp' 'Cr' 'PCr' 'GABA' 'Gln' 'Glu' 'GSH' 'Lac' 'mI' 'NAA' 'NAAG' 'tCh' 'PE' 'Scyllo' 'Tau'};
        metaboliteLabels = {'Asc';'Asp';'Cr';'PCr';'GABA';'Gln';'Glu';'GSH';'Lac';'mI';'NAA';'NAAG';'tCh';'PE';'Scyllo';'Tau';};
        xLimits = [0.5 4.097];
        offsetMetabolite = 0.1;
    case 'Met'
        metaboliteNames = {'NAA' 'NAAG' 'Asp' 'Glu' 'Gln' 'GABA' 'Lac' ...
            'Cr+PCr' 'Cho+GPC+PCh' 'mI' 'sI' 'Gly' 'Glc' 'GSH' 'PE' 'Tau'};
        
        metaboliteLabels = {'NAA' 'NAAG' 'Asp' 'Glu' 'Gln' 'GABA' 'Lac'  ...
            'tCr' 'tCho'  'mI' 'sI' 'Gly' 'Glc' 'GSH' 'PE' 'Tau'};
        
        metaboliteNames = {'Asp' 'Cr' 'PCr' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'Lac' 'mI' 'NAA_as' 'NAA_ac' 'NAAG' 'Scyllo' 'Tau' 'tCho' 'PE'};
        metaboliteLabels = {'Asp';'Cr';'PCr';'GABA';'Glu';'Gln';'GSH';'Glyc';'Lac';'mI';'NAA(CH_2)';'NAA(CH_3)';'NAAG';'Scyllo';'Tau';'tCho';'PE'};
        metaboliteNames = {'Asp' 'Cr' 'PCr' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'Lac' 'mI' 'NAA' 'NAAG' 'Scyllo' 'Tau' 'PCh' 'GPC' 'PE'};
        metaboliteLabels = {'Asp';'Cr';'PCr';'GABA';'Glu';'Gln';'GSH';'Glyc';'Lac';'mI';'NAA';'NAAG';'Scyllo';'Tau';'PCh';'GPC';'PE'};
        
        %         %Braino
        %         metaboliteNames = {'Cr_CH3' 'Cr_CH2' 'Glu' 'Lac' 'mI' 'NAA_as' 'NAA_ac' 'Cho'};
        %         metaboliteLabels = {'Cr_CH3';'Cr_CH2';'Glu';'Lac';'mI';'NAA(CH_2)';'NAA(CH_3)';'Cho'};
        
        xLimits = [0.5 4.097];
        offsetMetabolite = 0.1;
        
    case 'DF'
        metaboliteNames = {'Asp';'Cr';'Cr_CH2';           'GABA';'Glu';'Gln';'GSH';'Glyc';'Lac';'mI';'NAA_ac';   'NAA_as';   'NAAG';'Scyllo';'Tau';'tCho_P';...
            'hCs';'hist';'ATP';'NAA_DF';'NAD';'DF58';'DF60';'DF68';'DF73';'DF75';'DF82';'DF83';'DF835';'NAAB'};
        metaboliteLabels = {'Asp';'tCr(CH_3)';'tCr(CH_2)';'GABA';'Glu';'Gln';'GSH';'Glyc';'Lac';'mI';'NAA(CH_3)';'NAA(CH_2)';'NAAG';'Scy';   'Tau';'tCho+';...
            'hCs';'hist';'ATP';'NAA_DF';'NAD';'DF58';'DF60';'DF68';'DF73';'DF75';'DF82';'DF83';'DF835';'NAAB'};
        xLimits = [0.5 9];
        metaboliteNames = {'hCs';'DF70';'ATP';'NAA_DF';'NAD';'DF58';'DF60';'DF68';'DF73';'DF75';'DF82';'DF83';'DF835';'NAAB';'DF61';'DF85'};
        metaboliteLabels = {'hCs';'DF70';'ATP';'NAA_DF';'NAD';'DF58';'DF60';'DF68';'DF73';'DF75';'DF82';'DF83';'DF835';'NAAB';'DF61';'DF85'};
        
        metaboliteNames = {'hCs';'NAA_DF';'NAD'; 'DF58';     'DF60';     'DF61';     'DF68';     'DF70';     'DF73';     'DF75';    'NAAB';      'DF82';     'DF83';     'DF835';    'DF85'};
        metaboliteLabels = {'hCs';'NAA';  'NAD^+';'DF_{5.75}';'DF_{5.97}';'DF_{6.12}';'DF_{6.83}';'DF_{7.04}';'DF_{7.30}';'DF_{7.48}';'NAA Broad';'DF_{8.18}';'DF_{8.24}';'DF_{8.37}';'DF_{8.49}'};
        xLimits = [5.5 9.4];
        %         xLimits = [6.95 8.1];
        if overviewPlots
            offsetMetabolite = 0.1;
        else
            offsetMetabolite = 0.007;
            offsetMetabolite = 0.01;
        end
    case '31P'
        

        metaboliteNames = {'Cr'   'ATP-g'     'ATP-a'     'GPC' 'GPE' 'Pi_in'    'Pi_ext'   'PC' 'PE' 'NADH' 'NAD'  'UDPG'};
        metaboliteLabels = {'PCr' 'ATP-gamma' 'ATP-alpha' 'GPC' 'GPE' 'Pi intra' 'Pi extra' 'PC' 'PE' 'NADH' 'NAD+' 'UDPG'};
        xLimits = [-10 10];
    case 'fit'
        
        metaboliteNames = {}; %, 'NAA', 'Cr39',  'MMB_si', 'NAA', 'Cr39', 'Cr30', 'mI'  'combin'
        metaboliteLabels = {}; %, 'NAA', 'Cr', 'MM' , 'NAA', 'Cr-CH_2', 'Cr-CH_3', 'mI'   'MM'
        xLimits = [0.5 4.05]
        offsetMetabolite = 0.15; %0.25
        
    case 'MM' 
        
        metaboliteNames = {'MM09', 'MM12', 'MM14', 'MM17', 'MM20', 'MM22', 'MM26', 'MM27', 'MM30', 'MM32', 'MM36', 'MM37', 'MM38', 'Cr39', 'NAA', 'mI' ,'GPC', 'Glu', 'NAA_as'};%  %, 'Cho', 'Glu', 'Gln', 'Tau', 'Glycin' 'PCr'    %, 'NAA_si', 'Creat', 'PCreat', 'GPC','Glu', 'Gln', 'GABA','NAA_as','Asp','Tau', 'mI', 'Glycin'   
        metaboliteLabels = {'M_{0.92}', 'M_{1.21}', 'M_{1.39}', 'M_{1.67}', 'M_{2.04}', 'M_{2.26}', 'M_{2.56}', 'M_{2.70}', 'M_{2.99}', 'M_{3.21}', 'M_{3.62}', 'M_{3.75}', 'M_{3.86}' ,  'tCr(CH_2)', 'NAA(CH_3)', 'mI', 'GPC', 'Glu', 'NAA_(asp)'}; %%'NAA', 'tCr(CH_{3})',  'tCho', 'Glu', 'Gln', 'Tau', 'Glyc',   %,   , 'NAA(CH_3)','tCr(CH_3)', 'tCr(CH_2)','GPC','Glu','Gln','GABA','NAA_{asp}', 'Asp', 'Tau', 'mI', 'Glycin'
        xLimits = [0.5 4.097]
        offsetMetabolite = 0.60; %0.25

   
    otherwise
        error('Not known downField_MM_Met');
end

xLimitsText = xLimits(1) -0.03;

if overviewPlots && strcmp(downField_MM_Met, 'DF')
    xLimitsText = xLimits(2) +0.63;
end
numberOfMet = length(metaboliteNames);

%plotting offsets

offsetResidual = offsetMetabolite * (numberOfMet+1);
offsetBaseline = offsetResidual + offsetMetabolite;

FontSize = 18;
% LineWidth = 1.5;
FontSize = 18;
LineWidth = 2;

%
ppm = {};
pData = {};
fData = {};
bData = {};
mmData = {};
rData = {};
% close all;

for index =1:length(FileName)
    figId = figure;%(index+10); %I am not sure if we need this indexing anymore. It is useful to have it like this for ProFit comparison
    %[c1 c2 c3 c4] = textread(strcat(PathName,FileName{i}),'%s %s %s %s');
    c          = textread(strcat(PathName,FileName{index}),'%s');
    nOfPoints  = find(strcmp(c ,'points'),1);
    nOfPoints  = str2num (c{nOfPoints-1});
    
    %ppm axis
    indexOfPpm   = find(strcmp(c,'ppm-axis'))+3;
    endOfMet     = nOfPoints+indexOfPpm-1;
    ppm{index}= str2double( c(indexOfPpm:endOfMet,1));
    
    %phaseData
    indexOfMet   = find(strcmp(c,'phased'))+4;
    endOfMet     = nOfPoints+indexOfMet-1;
    pData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %fit data
    indexOfMet   = find(strcmp(c,'fit'))+5;
    endOfMet     = nOfPoints+indexOfMet-1;
    fData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %background
    indexOfMet   = find(strcmp(c,'background'))+3;
    endOfMet     = nOfPoints+indexOfMet-1;
    bData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    if strcmp(downField_MM_Met,'Met') || strcmp(downField_MM_Met,'Met moiety') ||  strcmp(downField_MM_Met,'Met moiety fMRS')
        %Macromolecular baseline
        indexOfMet   = find(strcmp(c,'Leu'),1,'last')+3;
        endOfMet     = nOfPoints+indexOfMet-1;
        mmData{index}= str2double( c(indexOfMet:endOfMet,1));
        if ~isempty(indexOfMet)
            MMB_available = true;
        else
            MMB_available = false;
        end
    else
        MMB_available = false;
    end
    
    
    %residual
    rData{index} = pData{index} - fData{index};
    
    %scaling calculation
    scale = max(pData{index});
    
    %plotting
    hold on
    if displayBaseline
        if MMB_available
            p = plot(ppm{index}, pData{index} ./ scale , ...
                ppm{index},fData{index} ./ scale , ...
                ppm{index},(mmData{index}- bData{index}) ./ scale - 0.03 , ...
                ppm{index},bData{index} ./ scale - offsetBaseline, ...
                ppm{index},rData{index} ./ scale - offsetResidual);
            text(xLimitsText,-0.03,'MMB', 'FontSize', FontSize, 'FontWeight', 'bold');
        else
            p = plot(ppm{index}, pData{index} ./ scale, ...
                ppm{index},fData{index} ./ scale, ...
                ppm{index},bData{index} ./ scale - offsetBaseline, ...
                ppm{index},rData{index} ./ scale - offsetResidual);
        end
    else
        if MMB_available
            p = plot(ppm{index}, (pData{index} - bData{index}) ./ scale , ...
                ppm{index},(fData{index} - bData{index}) ./ scale , ...
                ppm{index},(mmData{index}- bData{index} ) ./ scale - 0.03 , ...
                ppm{index},bData{index} ./ scale - offsetBaseline, ...
                ppm{index},rData{index} ./ scale - offsetResidual);
            text(xLimitsText,-0.02,'MMB', 'FontSize', FontSize, 'FontWeight', 'bold');
        else
            p = plot(ppm{index}, (pData{index}- bData{index}) ./ scale, ...
                ppm{index},(fData{index}- bData{index}) ./ scale, ...
                ppm{index},bData{index} ./ scale - offsetBaseline, ...
                ppm{index},rData{index} ./ scale - offsetResidual);
        end
    end
    
    if overviewPlots && strcmp(downField_MM_Met, 'DF')
        %         text(xLimitsText,0.05,'Data + Fit', 'FontSize', FontSize, 'FontWeight', 'bold');
        text(xLimitsText,0.07,'Data + Fit', 'FontSize', FontSize, 'FontWeight', 'bold');
    else
        text(xLimitsText,0.07,'Data + Fit', 'FontSize', FontSize, 'FontWeight', 'bold');
    end
    text(xLimitsText,-offsetResidual,'Residual', 'FontSize', FontSize, 'FontWeight', 'bold');
    %     text(xLimitsText,-offsetBaseline,'Baseline', 'FontSize', FontSize, 'FontWeight', 'bold');
    text(xLimitsText,-offsetBaseline-0.01,'Baseline', 'FontSize', FontSize, 'FontWeight', 'bold');
    for plots = 1:length(p)
        set(p(plots),'LineWidth',LineWidth);
    end
    
    for indexMetabolite = 1:length(metaboliteNames)
        %evaluate each metabolite
        if strcmp(metaboliteNames{indexMetabolite},'NAD+')
            individualMet = {'NAD+'};
        else
            individualMet = strsplit(metaboliteNames{indexMetabolite},'+');
        end
        metaboliteSpectrum = zeros(nOfPoints,1);
        for indexIndividualMet=1:length(individualMet)
            %sum up the individual components
            indexOfMet   = find(strcmp(c,individualMet{indexIndividualMet}),1,'last')+3;
            endOfMet     = nOfPoints+indexOfMet-1;
            if (indexOfMet > indexOfPpm) %make sure that we indeed have a metabolite quantification spectrum
                metaboliteSpectrum = metaboliteSpectrum + str2double(c(indexOfMet:endOfMet,1)) - bData{index};
            end
        end

        if MMB_available
            pMetabolite = plot(ppm{index}, metaboliteSpectrum ./ scale - indexMetabolite * offsetMetabolite);
            text(xLimitsText, indexMetabolite * -offsetMetabolite,metaboliteLabels{indexMetabolite}, 'FontSize', FontSize, 'FontWeight', 'bold');
        else
            pMetabolite = plot(ppm{index}, metaboliteSpectrum ./ scale - (indexMetabolite-1) * offsetMetabolite);
            text(xLimitsText, (indexMetabolite-1) * -offsetMetabolite,metaboliteLabels{indexMetabolite}, 'FontSize', FontSize, 'FontWeight', 'bold');
        end

        set(pMetabolite,'LineWidth',LineWidth);
    end
    
    xlim(xLimits);
    xlabel('\delta (ppm)');
    %     ylim([-offsetBaseline-offsetMetabolite*2 0.07]);
    ylim([-offsetBaseline-offsetMetabolite*2 1.1]);% for ProFit
    if strcmp(downField_MM_Met, 'Met moiety')
    else
        %     ylim([-0.08 0.1]);
        %     ylim([-0.19 0.07]);
        %     ylim([-0.006 0.055]);
        ylim([-0.21 0.115]);
        ylim([-0.21 0.1]);
%         ylim([-0.16 0.115]);
        ylim([-2 1.1]);

    end

    set(gca,'xDir','reverse')
    set(gca,'ytick',[]);
    
    %     title(['Summed Metabolite Spectrum with Fitted Metabolites TE = 24 ms'])
    %     title(FileName{index}, 'Interpreter', 'none')

    set(gca,'fontsize',FontSize);
    set(gca,'FontWeight','bold');

    clear   nOfPoints indexOfMet i
    
    if saveFigures
        set(figId, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
        set(gca, 'Position', [0.13,0.11,0.75,0.815])
        set(gca, 'Position', [0.13,0.11,0.75,0.88])
%         saveas(figId, [PathName,FileName{index}(1:end-6), '.tif']);
        savefig(figId, [PathName,FileName{index}(1:end-6), '.fig'],'compact');
    end
end
