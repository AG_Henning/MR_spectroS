function Conc_LCModelFit_31P_T2(FileName, PathName, saveFigures, displayBaseline)
clear; close all;
%%%% select all 9 TEs

if ~exist('FileName', 'var')
    [FileName,PathName] = uigetfile('*.coord','Please select .coord files from LCModel','Multiselect','on');
end
%
if ~iscell(FileName)
    FileName= {FileName};
end

if ~exist('saveFigures','var')
    saveFigures = true;
end

if ~exist('displayBaseline','var')
    displayBaseline = false;
end

metaboliteNames = {'PCr'   'ATP-g'     'ATP-a'     'GPC' 'GPE' 'Pi_in'    'Pi_ext'   'PC' 'PE' 'NADH' 'NAD'};
metaboliteLabels = {'PCr' 'ATP-gamma' 'ATP-alpha' 'GPC' 'GPE' 'Pi intra' 'Pi extra' 'PC' 'PE' 'NADH' 'NAD+'};
xLimits = [-10 8];

xLimitsText = xLimits(1) -0.03;

numberOfMet = length(metaboliteNames);

%plotting offsets

offsetMetabolite = 0.15;
offsetResidual = offsetMetabolite * (numberOfMet+1);
offsetBaseline = offsetResidual + offsetMetabolite;

FontSize = 18;
LineWidth = 2;

%
ppm = {}; pData = {}; fData = {}; bData = {}; mmData = {}; rData = {}; % close all;

for index =1:length(FileName)
    figId = figure;%(index+10); %I am not sure if we need this indexing anymore. It is useful to have it like this for ProFit comparison
    %[c1 c2 c3 c4] = textread(strcat(PathName,FileName{i}),'%s %s %s %s');
    c          = textread(strcat(PathName,FileName{index}),'%s');
    nOfPoints  = find(strcmp(c ,'points'),1);
    nOfPoints  = str2num (c{nOfPoints-1});
    
    %ppm axis
    indexOfPpm   = find(strcmp(c,'ppm-axis'))+3;
    endOfMet     = nOfPoints+indexOfPpm-1;
    ppm{index}= str2double( c(indexOfPpm:endOfMet,1));
    
    %phaseData
    indexOfMet   = find(strcmp(c,'phased'))+4;
    endOfMet     = nOfPoints+indexOfMet-1;
    pData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %fit data
    indexOfMet   = find(strcmp(c,'fit'))+5;
    endOfMet     = nOfPoints+indexOfMet-1;
    fData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %background
    indexOfMet   = find(strcmp(c,'background'))+3;
    endOfMet     = nOfPoints+indexOfMet-1;
    bData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %residual
    rData{index} = pData{index} - fData{index};
    
    %scaling calculation
    scale = max(pData{index});
    
    %plotting
    hold on
    if displayBaseline
        p = plot(ppm{index}, pData{index} ./ scale, ...
            ppm{index},fData{index} ./ scale, ...
            ppm{index},bData{index} ./ scale - offsetBaseline, ...
            ppm{index},rData{index} ./ scale - offsetResidual);
    else
        p = plot(ppm{index}, (pData{index}- bData{index}) ./ scale, ...
            ppm{index},(fData{index}- bData{index}) ./ scale, ...
            ppm{index},bData{index} ./ scale - offsetBaseline, ...
            ppm{index},rData{index} ./ scale - offsetResidual);
    end
    
    text(xLimitsText,0.07,'Data + Fit', 'FontSize', FontSize, 'FontWeight', 'bold');
    
    text(xLimitsText,-offsetResidual,'Residual', 'FontSize', FontSize, 'FontWeight', 'bold');
    %     text(xLimitsText,-offsetBaseline,'Baseline', 'FontSize', FontSize, 'FontWeight', 'bold');
    text(xLimitsText,-offsetBaseline-0.01,'Baseline', 'FontSize', FontSize, 'FontWeight', 'bold');
    for plots = 1:length(p)
        set(p(plots),'LineWidth',LineWidth);
    end
    
%     FWHM of LCModel Fit
%     metaboliteNames = {'PCr', 'gATP', 'aATP' 'GPC', 'GPE', 'Pi_int', 'Pi_ext', 'PC', 'PE', 'NADH', 'NAD'};
    centerPpm =       [ 0,     -2.48,  -7.62  2.96,  3.51,  4.83,     5.20,     6.23, 6.77, -8.1    -8.15];
    bandwidth = 10000; scanFreq_MHz = 161.794000; nTimepoints = length(fData{1});
    ppmStart = ppm{1}(1); ppmEnd = ppm{1}(end);
    searchArea = 0.31; %ppm
    ppmVector = ppm{1};
%     fwhm = zeros(length(metaboliteNames),length(FileName));
%     freq = zeros(length(metaboliteNames),length(FileName));
    
    for indexMetabolite = 1:length(metaboliteNames)
        %evaluate each metabolite
        if strcmp(metaboliteNames{indexMetabolite},'NAD+')
            individualMet = {'NAD+'};
        else
            individualMet = strsplit(metaboliteNames{indexMetabolite},'+');
        end
        metaboliteSpectrum = zeros(nOfPoints,1);
        for indexIndividualMet=1:length(individualMet)
            %sum up the individual components
            indexOfMet   = find(strcmp(c,individualMet{indexIndividualMet}),1,'last')+3;
            endOfMet     = nOfPoints+indexOfMet-1;
            if (indexOfMet > indexOfPpm) %make sure that we indeed have a metabolite quantification spectrum
                metaboliteSpectrum = metaboliteSpectrum + str2double(c(indexOfMet:endOfMet,1)) - bData{index};
            end
        end
        %     FWHM of LCModel Fit
        ppmStartMet = centerPpm(indexMetabolite) + searchArea;
        ppmEndMet = centerPpm(indexMetabolite) - searchArea;
        spectrumPeak = metaboliteSpectrum;
        spectrumPeak(ppmVector>ppmStartMet | ppmVector < ppmEndMet) = 0;
        [fwhm(indexMetabolite,index), ~] = fwhm_Hz(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
        freq(indexMetabolite,index) = freq_ppm(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
        
        % peak integral
        peakIntReal(indexMetabolite,index) = sum(metaboliteSpectrum(2:end)-metaboliteSpectrum(2));       %omit first point for reference peak
        peakIntAbs(indexMetabolite,index)  = sum(abs(metaboliteSpectrum(2:end))-metaboliteSpectrum(2));  %omit first point for reference peak
        
        pMetabolite = plot(ppm{index}-(ppm{index}(2)-ppm{index}(1)), metaboliteSpectrum ./ scale - (indexMetabolite) * offsetMetabolite);
        text(xLimitsText, (indexMetabolite) * -offsetMetabolite,metaboliteLabels{indexMetabolite}, 'FontSize', FontSize, 'FontWeight', 'bold');
        
        set(pMetabolite,'LineWidth',LineWidth);
    end
    
    xlim(xLimits);
    xlabel('\delta (ppm)');
    ylim([-2.2 1.1]);

set(gca,'xDir','reverse')
set(gca,'ytick',[]);

%     title(['Summed Metabolite Spectrum with Fitted Metabolites TE = 24 ms'])
%     title(FileName{index}, 'Interpreter', 'none')

set(gca,'fontsize',FontSize);
set(gca,'FontWeight','bold');

clear   nOfPoints indexOfMet i

if saveFigures
    outputFigurePath = [PathName,'LCModelFigs\'];
    mkdir(outputFigurePath)
    savefig(figId, [outputFigurePath,FileName{index}(1:end-6), '.fig'],'compact');
end
    
end

close all


%% FWHM of LCModel Fit
%be careful with Pi intra/extra, NADH and bATP!
% metaboliteNames = {'PE', 'PC', 'Pi_ext' 'Pi_int', 'GPE', 'GPC', 'PCr', 'gATP', 'aATP', 'NADH', 'NAD'};
% centerPpm =       [ 6.77, 6.23, 5.20     4.83,     3.51,  2.96,  0,     -2.48, -7.62,  -8.1     -8.15];
% bandwidth = 10000; scanFreq_MHz = 161.794000; nTimepoints = length(fData{1});
% ppmStart = ppm{1}(1); ppmEnd = ppm{1}(end);
% searchArea = 0.2; %ppm
% ppmVector = ppm{1};
% fwhm = zeros(length(metaboliteNames),length(FileName));
% freq = zeros(length(metaboliteNames),length(FileName));
% for index =1:length(FileName)
%     for iMetabolite = 1:length(metaboliteNames)
%         ppmStartMet = centerPpm(iMetabolite) + searchArea;
%         ppmEndMet = centerPpm(iMetabolite) - searchArea;
%         spectrumPeak = fData{index};
%         spectrumPeak(ppmVector>ppmStartMet | ppmVector < ppmEndMet) = 0;
%         [fwhm(iMetabolite,index), ~] = fwhm_Hz(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
%         freq(iMetabolite,index) = freq_ppm(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
%     end
% end

%PCr should be at 0ppm
for index =1:length(FileName)
    [ d, ix ] = min( abs( freq(:,index)-0 ) );
    freq_toShift = freq(ix,index);
    for iMetabolite = 1:length(metaboliteNames)
        if freq_toShift > 0
            freq(iMetabolite,index) = freq(iMetabolite,index)-d;
        elseif freq_toShift < 0
            freq(iMetabolite,index) = freq(iMetabolite,index)+d;
        end
    end
end

pH = zeros(length(metaboliteNames),length(FileName));
pK = 6.73; delta_a = 3.275; delta_b = 5.685;
Pis = {'Pi_ext','Pi_in'};
for index =1:length(FileName)
    for indexPi = 1:length(Pis)
        idx_Pi = find(strcmp(metaboliteNames,Pis{indexPi})==1);
        pH(idx_Pi,index) = 6.73 + log10((freq(idx_Pi,index) - delta_a)/(delta_b - freq(idx_Pi,index)));
    end
end

%save FWHM & pH
FWHMValues = [];
for indexTE = 1:length(FileName)
    FWHMValues = [FWHMValues;
        fwhm(1,indexTE) fwhm(2,indexTE) fwhm(3,indexTE) fwhm(4,indexTE) fwhm(5,indexTE) fwhm(6,indexTE) fwhm(7,indexTE) fwhm(8,indexTE) fwhm(9,indexTE) fwhm(10,indexTE) fwhm(11,indexTE);...
        freq(1,indexTE) freq(2,indexTE) freq(3,indexTE) freq(4,indexTE) freq(5,indexTE) freq(6,indexTE) freq(7,indexTE) freq(8,indexTE) freq(9,indexTE) freq(10,indexTE) freq(11,indexTE);...
        pH(1,indexTE) pH(2,indexTE) pH(3,indexTE) pH(4,indexTE) pH(5,indexTE) pH(6,indexTE) pH(7,indexTE) pH(8,indexTE) pH(9,indexTE) pH(10,indexTE) pH(11,indexTE)];
end
rowNames = {'FWHM_TE6ms' 'Freq_TE6ms' 'pH_TE6ms' 'FWHM_TE8ms' 'Freq_TE8ms' 'pH_TE8ms'...
            'FWHM_TE11ms' 'Freq_TE11ms' 'pH_TE11ms' 'FWHM_TE15ms' 'Freq_TE15ms' 'pH_TE15ms'...
            'FWHM_TE20ms' 'Freq_TE20ms' 'pH_TE20ms' 'FWHM_TE30ms' 'Freq_TE30ms' 'pH_TE30ms'...
            'FWHM_TE50ms' 'Freq_TE50ms' 'pH_TE50ms' 'FWHM_TE80ms' 'Freq_TE80ms' 'pH_TE80ms'...
            'FWHM_TE150ms' 'Freq_TE150ms' 'pH_TE150ms'};
colNames = {'PCr', 'gATP', 'aATP' 'GPC', 'GPE', 'Pi_int', 'Pi_ext', 'PC', 'PE', 'NADH', 'NAD'};
FWHM = array2table(FWHMValues,'RowNames',rowNames,'VariableNames',colNames)

fileName = fullfile(PathName, strcat(FileName{1}([1:17,end-13:end-6]), '_FWHM_Freq_Fit'));

save(fileName, 'FWHM');

%% calculate and save concentrations 
%need to calculate T2 values of summed spectra first!

% load T1 data from Pohmann, Proc. Intl. Soc. Mag. Reson. Med. 26 (2018) 3994
load('D:\PAPER\31P_T2_RelaxationTimes\Data\T1_Pohmann.mat')     %saved as T1_ms
T1 = table2array(T1_ms);

if ~isempty(strfind(PathName,'Summed'))
    load(strcat(PathName,'\T2_Results_Summed_STEAM_Real__LW_noTrunc.mat')) %saved as tableT2toSave
else
    load(strcat(PathName(1:end-25),'Summed\T2_Results_Summed_STEAM_Real__LW_noTrunc.mat')) %saved as tableT2toSave
end
T2 = table2array(tableT2toSave(3,:));
metaboliteNamesT2 = tableT2toSave.Properties.VariableNames;

TR = 5000;  %ms
TEs = {'6'; '8'; '11'; '15'; '20'; '30'; '50'; '80'; '150'};  %ms
TM = 5;     %ms
numberOfTEs   = size(peakIntReal,2);
numberMetabolite = size(peakIntReal,1);

%calculate NADH_NAD
peakIntReal(12,:) = peakIntReal(10,:) + peakIntReal(11,:);
peakIntAbs(12,:)  = peakIntAbs(10,:)  + peakIntAbs(11,:);

for idxTE = 1:numberOfTEs
TD = TR - TM - cellfun(@str2num,TEs(idxTE))/2;
    for idxMetabolite = 1:numberMetabolite
        %keep sorting of metaboliteNames
        if idxMetabolite > numberMetabolite
            idxT = idxMetabolite;
        else
            exact_match_mask = strcmp(metaboliteNames{idxMetabolite},metaboliteNamesT2);
            idxT = find(exact_match_mask);
            if isempty(idxT)
                exact_match_mask = strcmp(metaboliteNames{idxMetabolite},strrep(metaboliteNamesT2,'_','-'));
                idxT = find(exact_match_mask);
            end
        end
        if ~isempty(find(isnan(T1(idxT))))  %no T1 corr for Pi extracellular
            %no TM
            Conc_RelaxationCorrected_Real(idxTE,idxMetabolite) = ...
                peakIntReal(idxMetabolite,idxTE)./...
                (exp(-cellfun(@str2num,TEs(idxTE))/T2(idxT)));
            Conc_RelaxationCorrected_Abs(idxTE,idxMetabolite) = ...
                peakIntAbs(idxMetabolite,idxTE)./...
                (exp(-cellfun(@str2num,TEs(idxTE))/T2(idxT)));
            %take TM into account
            Conc_RelaxationCorrected_Real_STEAM(idxTE,idxMetabolite) = ...
                peakIntReal(idxMetabolite,idxTE)./...
                (exp(-cellfun(@str2num,TEs(idxTE))/T2(idxT))*exp(-TM/T2(idxT)));
            Conc_RelaxationCorrected_Abs_STEAM(idxTE,idxMetabolite) = ...
                peakIntAbs(idxMetabolite,idxTE)./...
                (exp(-cellfun(@str2num,TEs(idxTE))/T2(idxT))*exp(-TM/T2(idxT)));
        else
            %no TM
            Conc_RelaxationCorrected_Real(idxTE,idxMetabolite) = ...
                peakIntReal(idxMetabolite,idxTE)./...
                ((1-exp(-TR/T1(idxT)))*exp(-cellfun(@str2num,TEs(idxTE))/T2(idxT)));
            Conc_RelaxationCorrected_Abs(idxTE,idxMetabolite) = ...
                peakIntAbs(idxMetabolite,idxTE)./...
                ((1-exp(-TR/T1(idxT)))*exp(-cellfun(@str2num,TEs(idxTE))/T2(idxT)));
            %take TM into account
            Conc_RelaxationCorrected_Real_STEAM(idxTE,idxMetabolite) = ...
                peakIntReal(idxMetabolite,idxTE)./...
                ((1-exp(-TD/T1(idxT)))*exp(-cellfun(@str2num,TEs(idxTE))/T2(idxT))*exp(-TM/T2(idxT)));
            Conc_RelaxationCorrected_Abs_STEAM(idxTE,idxMetabolite) = ...
                peakIntAbs(idxMetabolite,idxTE)./...
                ((1-exp(-TD/T1(idxT)))*exp(-cellfun(@str2num,TEs(idxTE))/T2(idxT))*exp(-TM/T2(idxT)));
        end
    end
end

%normalize to g-ATP 3mM
for idxTE = 1:numberOfTEs
    Conc_RelaxationCorrected_Real_Norm(idxTE,:) = Conc_RelaxationCorrected_Real(idxTE,:)*3/Conc_RelaxationCorrected_Real(idxTE,2);
    Conc_RelaxationCorrected_Abs_Norm(idxTE,:)  = Conc_RelaxationCorrected_Abs(idxTE,:)*3/Conc_RelaxationCorrected_Abs(idxTE,2);
    Conc_RelaxationCorrected_Real_Norm_STEAM(idxTE,:) = Conc_RelaxationCorrected_Real_STEAM(idxTE,:)*3/Conc_RelaxationCorrected_Real_STEAM(idxTE,2);
    Conc_RelaxationCorrected_Abs_Norm_STEAM(idxTE,:)  = Conc_RelaxationCorrected_Abs_STEAM(idxTE,:)*3/Conc_RelaxationCorrected_Abs_STEAM(idxTE,2);
end

rowNames = {'Conc_Real_TE6ms' 'Conc_Abs_TE6ms' 'Conc_Real_TE8ms' 'Conc_Abs_TE8ms'...
            'Conc_Real_TE11ms' 'Conc_Abs_TE11ms' 'Conc_Real_TE15ms' 'Conc_Abs_TE15ms'...
            'Conc_Real_TE20ms' 'Conc_Abs_TE20ms' 'Conc_Real_TE30ms' 'Conc_Abs_TE30ms'...
            'Conc_Real_TE50ms' 'Conc_Abs_TE50ms' 'Conc_Real_TE80ms' 'Conc_Abs_TE80ms'...
            'Conc_Real_TE150ms' 'Conc_Abs_TE150ms'};
        
Conc_LCModelFit_ = interleave2(Conc_RelaxationCorrected_Real_Norm, Conc_RelaxationCorrected_Abs_Norm, 'row');
Conc_LCModelFit = array2table(Conc_LCModelFit_,'RowNames',rowNames,'VariableNames',colNames)

fileName = fullfile(PathName, strcat(FileName{1}([1:17,end-13:end-6]), '_Conc_LCModelFit'));
save(fileName, 'Conc_LCModelFit');

%take TM into account
Conc_LCModelFit_STEAM_ = interleave2(Conc_RelaxationCorrected_Real_Norm_STEAM, Conc_RelaxationCorrected_Abs_Norm_STEAM, 'row');
Conc_LCModelFit_STEAM = array2table(Conc_LCModelFit_STEAM_,'RowNames',rowNames,'VariableNames',colNames)

fileName = fullfile(PathName, strcat(FileName{1}([1:17,end-13:end-6]), '_Conc_LCModelFit_STEAM'));
save(fileName, 'Conc_LCModelFit_STEAM');


end
