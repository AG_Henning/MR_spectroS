function RemoveExtraPoints(this)

if  size(this.Data{1},this.kx_dim) ~= this.Parameter.Headers.VectorSize_OS
    
    NColumns= size(this.Data{1},this.kx_dim);
    
    % the factor 2 is introduced for the oversampling!
    SamplesBeforeEcho= 2*this.Parameter.Headers.SamplesBeforeEcho;
    
    True_NColumns= this.Parameter.Headers.VectorSize_OS;
    
    SamplesFilterSettle= NColumns - True_NColumns- SamplesBeforeEcho; 
    
    
    new_Data= this.Data{1}(1+SamplesBeforeEcho:end-SamplesFilterSettle,...
        :,:,:,:,:,:,:,:,:,:,:);
    
    this.Data{1}=new_Data;
    
    
    %Update the flag
    this.Parameter.ReconFlags.isextrapoints= false;
    
    
else
    warning('No extra points to be removed')
    
end


end

