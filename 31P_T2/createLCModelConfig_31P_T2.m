function outputFile = createLCModelConfig_31P_T2(controlFilesPath, outputControlFilesPath, defaultControlFile, ...
    subjectsPath, defaultSubjectsPath, subjectID, defaultSubject, currentTE, defaultTE,currentOutputFile,...
    defaultOutputFile,TruncToFit,defaultTrunc)

% Intro comments need to be rewritten
% Creates the control files for the TE series, given a default control file.
% the newly generated files will be placed in the same path as indicated by
% the controlFilesPath.
% downField_MM_Met can be: 'DF' 'MM', 'Met' or 'pH'
% A sample input:
% controlFilesPath = 'Fitsettings/MMocc_noExtraMM_06knots/';
% defaultControlFile = 'fitsettingsDefault.control'

fitSettingsDefaultFileID = fopen([controlFilesPath defaultControlFile], 'r');

mkdir(outputControlFilesPath);
outputFile = strcat(outputControlFilesPath, strrep(strrep(strrep(defaultControlFile,defaultTE,currentTE),defaultSubject,subjectID),defaultTrunc,TruncToFit));

fitSettingsFileID = fopen(outputFile, 'w');
display(outputFile);

while(~feof(fitSettingsDefaultFileID))
    s = fgetl(fitSettingsDefaultFileID);
    k = strfind(s,'FILRAW');
    if ~isempty(k) && ~strcmp('Summed',subjectsPath)
        if ~isempty(strfind(currentTE,'JD'))
            s = strrep(s, defaultTE, strcat(subjectsPath(1:end-9),'_',currentTE(4:end)));
        else
            s = strrep(s, defaultTE, strcat(subjectsPath(1:end-9),'_',currentTE));
        end
        s = strrep(s, defaultSubjectsPath, subjectsPath(1:end-9));
        s = strrep(s, defaultSubject, subjectID);
        s = strrep(s, defaultTrunc, TruncToFit);
    elseif ~isempty(k) && strcmp('Summed',subjectsPath)
        if ~isempty(strfind(currentTE,'JD'))
            s = strrep(s, defaultTE, strcat(subjectsPath,'_',currentTE(4:end)));
        else
            s = strrep(s, defaultTE, strcat(subjectsPath,'_',currentTE));
        end
        s = strrep(s, defaultSubjectsPath, subjectsPath);
        s = strrep(s, defaultSubject, subjectID);
        s = strrep(s, defaultTrunc, TruncToFit);
    else
        s = strrep(s, defaultTE, currentTE);
        s = strrep(s, defaultSubjectsPath, subjectsPath);
        s = strrep(s, defaultSubject, subjectID);
        s = strrep(s, defaultOutputFile, currentOutputFile);
        s = strrep(s, defaultTrunc, TruncToFit);
    end
    
    fprintf(fitSettingsFileID,'%s\n', s);
end
fclose(fitSettingsFileID);
fclose(fitSettingsDefaultFileID);
end