function this = DeleteCoilChannels (this,coilChannels4Delete)
% version: 26 Feb 2014

if ~this.Parameter.ReconFlags.isSorted
    error('The data has to be sorted first')
end


if this.Parameter.ReconFlags.isCoilChannelsDeleted
    warning('These coil elements have been already deleted:\n %s', num2str(this.Parameter.ReconFlags.isCoilChannelsDeleted))
end


oldData                  = this.Data{1};
nCoils                   = size(this.Data{1}, this.coil_dim);


nCoilChannels4Delete        = length(coilChannels4Delete);
newNCoils                   = nCoils-nCoilChannels4Delete;

newDataSize                 = size(oldData);
newDataSize(this.coil_dim)  = newNCoils;

newData                     =zeros(newDataSize);

coilChannelsSum=0;
for indexOfCoil=1:nCoils
    if isempty( find(coilChannels4Delete==indexOfCoil,1) )
        coilChannelsSum=coilChannelsSum+1;
        newData( :, 1, 1, coilChannelsSum, 1, 1, 1, 1, :, :, 1, : )=...
            oldData( :, 1, 1, indexOfCoil, 1, 1, 1, 1, :, :, 1, : ) ;
    end
end

this.Data{1}                                              =newData ;
this.Parameter.ReconFlags.isCoilChannelsDeleted           =[this.Parameter.ReconFlags.isCoilChannelsDeleted coilChannels4Delete];
end

