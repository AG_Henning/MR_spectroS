function [figureIds, sortedIndeces] = evaluateT2FitsNew(downField_MM_Met, offsetPlot, figureIds, sortedIndeces)

if ~exist('downField_MM_Met','var')
    downField_MM_Met = 'DF';
end

if ~exist('offsetPlot','var')
    offsetPlot = 0;
end
if ~exist('figureIds','var')
    figureIds = {};
end

useTotalNAA = true;
doMetaboliteFWHMs = false;

if offsetPlot <= 0
    plotColors = {[0 0 1], [1 0 0]};
    plotColors2 = {[0.0 0.4 0.0], [1 0.5 0.3]};
    plotColors2 = {[0.0 0.4 0.0], [0 0 0]};
%     plotColors2 = {[0.3 0.3 0.1], [0.2 0.5 0.05]};
else
    plotColors = {[0.0 0.7 0.9], [1 0 1]};
    plotColors2 = {[0.1 0.9 0.1], [0.9 0.6 0]};
    plotColors2 = {[0.1 0.9 0.1], [0.5 0.5 0.5]};
end

sortT2sDescending = false;
usePpm = false;

if isempty(figureIds)
    emptyFigureIds = true;
else
    emptyFigureIds = false;
end

switch downField_MM_Met
    case 'DF'
        pathNameMM = 'DF data path';
        outputFolder = 'Output\FWHMBA Results 7.5e-3\';
    case 'Met'
        pathNameMM = 'DF data path';
        outputFolder = 'Output_UF\';
    case 'MM'
        pathNameMM = 'MM data path';
        outputFolder = 'Output\';
    case 'MM WM'
        pathNameMM = 'MM WM data path';
        outputFolder = 'Output\';
end
sampleCriteriaMM = {outputFolder};
data_path = pathToDataFolder(pathNameMM, sampleCriteriaMM);

load([data_path outputFolder 'tableT2sSubjects.mat'],'tableT2sSubjects')
load([data_path outputFolder 'tableConcentrationsSubjects.mat'],'tableConcentrationsSubjects')
if exist([data_path outputFolder 'tableFWHMSubjects.mat'],'file')
    doFWHMevalutation = true;
else
    doFWHMevalutation = false;
end
if doFWHMevalutation
    load([data_path outputFolder 'tableFWHMSubjects.mat'],'tableFWHMSubjects')
    load([data_path outputFolder 'tableFWHMSubjects.mat'],'orderMetFWHMSubjects')
end

switch downField_MM_Met
    case 'DF'
        if useTotalNAA
            metaboliteNameOrder = {'Cr-CH3';'DF58';'DF60';'DF61';'DF68';'DF70';'DF73';'DF75';'DF82';'DF83';'DF835';'DF85';'hCs';'NAA Broad';'NAA';'total NAA'};'ATP';'hist';'NAD';
            metaboliteNameOrderConc = {'Cr';'DF58';'DF60';'DF61';'DF68';'DF70';'DF73';'DF75';'DF82';'DF83';'DF835';'DF85';'hCs';'NAAB';'NAA_DF';'NAAB+NAA_DF'};'NAD';
            metaboliteNamesDisplay = {'tCr-CH3';'DF5.75';'DF5.97';'DF6.12';'DF6.83';'DF7.04';'DF7.30';'DF7.48';'DF8.18';'DF8.24';'DF8.37';'DF8.49';'hCs';'NAA Broad';'NAA';'total NAA'};'ATP';'hist';'NAD+';
            metaboliteNamesDisplay2 = {'tCr-CH3';'DF_{5.75}';'DF_{5.97}';'DF_{6.12}';'DF_{6.83}';'DF_{7.04}';'DF_{7.30}';'DF_{7.48}';'DF_{8.18}';'DF_{8.24}';'DF_{8.37}';'DF_{8.49}';'hCs';'NAA Broad';'NAA';'total NAA'};'ATP';'hist';'NAD^+';
        else
            metaboliteNameOrder = {'Cr-CH3';'DF58';'DF60';'DF61';'DF68';'DF70';'DF73';'DF75';'DF82';'DF83';'DF835';'DF85';'hCs';'NAA Broad';'NAA'};'ATP';'hist';'NAD';
            metaboliteNameOrderConc = {'Cr';'DF58';'DF60';'DF61';'DF68';'DF70';'DF73';'DF75';'DF82';'DF83';'DF835';'DF85';'hCs';'NAAB';'NAA_DF'};'ATP';'hist';'NAD';
            metaboliteNamesDisplay = {'tCr-CH3';'DF5.75';'DF5.97';'DF6.12';'DF6.83';'DF7.04';'DF7.30';'DF7.48';'DF8.18';'DF8.24';'DF8.37';'DF8.49';'hCs';'NAA Broad';'NAA'};'ATP';'hist';'NAD+';
            metaboliteNamesDisplay2 = {'tCr-CH3';'DF_{5.75}';'DF_{5.97}';'DF_{6.12}';'DF_{6.83}';'DF_{7.04}';'DF_{7.30}';'DF_{7.48}';'DF_{8.18}';'DF_{8.24}';'DF_{8.37}';'DF_{8.49}';'hCs';'NAA Broad';'NAA';};'ATP';'hist'; 'NAD^+';           
        end
        metaboliteNameOrderFWHM = metaboliteNameOrder;
        metaboliteNameOrderFWHM{1} = 'Cr';
        subjects = {'1658';'1717';'2017';'3373';'3490';'5771';'6249';'6971';'7338';'7782';'9810'};
        
        yAxisLim = 65;
        yAxisLim = 90;
    case 'Met'
        metaboliteNameOrder = {'Leu';'Asp';'Cr-CH3';'Cr-CH2';'GABA';'Glu';'Gln';'GSH_no';'Glyc';'GPC';'mI';'NAA-asp';'NAA-ace';'NAAG';'PCh';'PE';'Scyllo';'Tau';'tCho';'tNAA';'mI+Glyc';'Glx'};
        metaboliteNameOrder = {'Leu';'Asp';'Cr-CH3';'Cr-CH2';'GABA';'Glu';'Gln';'GSH_no';'Glyc';'mI';'NAA-asp';'NAA-ace';'NAAG';'Scyllo';'Tau';'tCho_PE';'tNAA';'mI+Glyc';'Glx'; 'Lac'; 'NAA+NAAG'};
        %         metaboliteNameOrder = {'Leu';'Asp';'Cr-CH3';'PCr';'GABA';'Glu';'Gln';'GSH_no';'Glyc';'mI';'NAA-asp';'NAA-ace';'NAAG';'Scyllo';'Tau';'tCho';'PE';'tNAA';'mI+Glyc';'Glx'; 'Lac'; 'Cr+PCr'};
        
        %actual values to plot in final version
        metaboliteNameOrder = {'Asp';'Cr-CH3';'Cr-CH2';'Glu';'Gln';'GSH';'Glyc';'mI';'NAA-asp';'NAA-ace';'NAAG';'tCho_PE';'tNAA+';'mI+Glyc';'Glx';};%'Leu';'GABA';'Scyllo';'Tau'};'tNAA';
        metaboliteNameOrderFWHM = metaboliteNameOrder;
        metaboliteNamesDisplay = {'Asp';'tCr(CH3)';'tCr(CH2)';'Glu';'Gln';'GSH';'Glyc';'mI';'NAA(CH2)';'NAA(CH3)';'NAAG';'tCho+';'tNAA+';'mI+Glyc';'Glx';};%'Leu';'GABA';'Scyllo';'Tau'};'tNAA';
        
        
        if doMetaboliteFWHMs
            metaboliteNameOrder = {'Cr-CH3' 'Cr-CH2' 'NAA-ace' 'tCho_PE'};
            metaboliteNameOrderFWHM = metaboliteNameOrder;
            metaboliteNamesDisplay = {'tCr(CH3)';'tCr(CH2)';'NAA(CH3)';'tCho+';};
        else
            doFWHMevalutation = false;
        end
        
        subjects = {'1658';'1706';'1717';'2017';'3373';'3490';'5771';'6249';'6971';'7338';'7782';'9810'};
        yAxisLim = 11;
    case 'MM'
        metaboliteNameOrder = {'M0.92';'M1.21';'M1.39';'M1.67';'M2.04';'M2.26';'M2.56';'M2.70';'M2.99';'M3.21';'M3.62';'M3.75';'M3.86';'M4.03'; 'tCr(CH2)'};
        metaboliteNameOrderFWHM = {'M_{0.92}';'M_{1.21}';'M_{1.39}';'M_{1.67}';'M_{2.04}';'M_{2.26}';'M_{2.56}';'M_{2.70}';'M_{2.99}';'M_{3.21}';'M_{3.62}';'M_{3.75}';'M_{3.86}';'M_{4.03}'; 'tCr(CH_2)'};
        subjects = {'1658';'1706';'1717';'2017';'3373';'3490';'5771';'6249';'6971';'7338';'7782';'9810'};
        metaboliteNamesDisplay = metaboliteNameOrder;
        yAxisLim = 85;
    case 'MM WM'
        metaboliteNameOrder = {'M0.92';'M1.21';'M1.39';'M1.67';'M2.04';'M2.26';'M2.56';'M2.70';'M2.99';'M3.21';'M3.62';'M3.75';'M3.86';'M4.03'; 'tCr(CH2)'};
        metaboliteNameOrderFWHM = {'M_{0.92}';'M_{1.21}';'M_{1.39}';'M_{1.67}';'M_{2.04}';'M_{2.26}';'M_{2.56}';'M_{2.70}';'M_{2.99}';'M_{3.21}';'M_{3.62}';'M_{3.75}';'M_{3.86}';'M_{4.03}'; 'tCr(CH_2)'};
        subjects = {'1658';'1717';'2016';'2020';'3490'};
        metaboliteNamesDisplay = metaboliteNameOrder;
        metaboliteNamesDisplay{end} = 'tCr(CH2)';
        yAxisLim = 85;
end

numberOfMet = length(metaboliteNameOrder);
TEs = [24 32 40 52 60];
numberOfTEs = 5;
scanFrequency = 399.719;
numOfSubjects = length(tableT2sSubjects);
T2s = cell(numOfSubjects,numberOfMet);
R2s = cell(numOfSubjects,numberOfMet);
concentrations = cell(numOfSubjects, numberOfMet, numberOfTEs);
CRLBs = cell(numOfSubjects, numberOfMet, numberOfTEs);
for indexSubject = 1:numOfSubjects        
    T2times = tableT2sSubjects{indexSubject}(2,2:end);
    R2times = tableT2sSubjects{indexSubject}(3,2:end);
    concentrationsSubject = tableConcentrationsSubjects{indexSubject};
    for indexMetabolite = 1:numberOfMet
        index = find(strcmp(metaboliteNameOrder(indexMetabolite), tableT2sSubjects{indexSubject}(1,2:end)));
        T2s(indexSubject,indexMetabolite) = T2times(index);
        R2s(indexSubject,indexMetabolite) = R2times(index);
        indexConc = find(strcmp(metaboliteNameOrderConc(indexMetabolite),concentrationsSubject(1,:))); 
        concentrations(indexSubject, indexMetabolite,:) = concentrationsSubject(2:6,indexConc);
        CRLBs(indexSubject, indexMetabolite,:) = concentrationsSubject(2:6,indexConc+1);
    end
end

saveCRLBs(CRLBs, TEs, metaboliteNamesDisplay, data_path, outputFolder)

concentrations = cell2mat(concentrations);
T2sNumeric = str2double(T2s);
R2sNumeric = str2double(R2s);
nonNegT2s = T2sNumeric;
nonNegT2s(nonNegT2s<0) = NaN;
reshape(nonNegT2s,numberOfMet, numOfSubjects)
meanT2s = mean(nonNegT2s,1,'omitNaN')
stdT2s = std(nonNegT2s,1,'omitNaN')

acceptableT2s = nonNegT2s;
acceptableR2s = R2sNumeric;
switch downField_MM_Met
    case 'DF'
        acceptableT2s(R2sNumeric<=0.53) = NaN;
        acceptableR2s(R2sNumeric<=0.53) = NaN;
        for indexSubject = 1:numOfSubjects
            for indexMetabolite = 1:numberOfMet
                if R2sNumeric(indexSubject, indexMetabolite) <= 0.5
%                     concentrations(indexSubject, indexMetabolite,:) = NaN;
                end
            end
        end
    case 'MM'
        acceptableT2s(R2sNumeric<=0.5) = NaN;
        acceptableR2s(R2sNumeric<=0.5) = NaN;
    case 'MM WM'
        %TODO manually set the R2sNumeric and R2SummedNumeric of M4.03 to 0
        acceptableT2s(R2sNumeric<=0.5) = NaN;
        acceptableR2s(R2sNumeric<=0.5) = NaN;
    case 'Met'
        acceptableT2s(R2sNumeric<=0.5) = NaN;
        acceptableR2s(R2sNumeric<=0.5) = NaN;
end
%write the metabolites/subjects with issues
metaboliteNameOrder(sum(isnan(acceptableT2s))>0)
sum(isnan(acceptableT2s))
subjects(sum(isnan(acceptableT2s),2)>0)
sum(isnan(acceptableT2s),2)
sum(sum(isnan(acceptableT2s),2))

reshape(acceptableT2s,numberOfMet, numOfSubjects);
acceptableMeanT2s = mean(acceptableT2s,1,'omitNaN');
acceptableStdT2s = std(acceptableT2s,1,'omitNaN');
acceptableMeanR2s = mean(acceptableR2s,1,'omitNaN');
acceptableStdR2s = std(acceptableR2s,1,'omitNaN');
numberOfNaNs = sum(sum(isnan(acceptableT2s)))

TEsRep = repmat(TEs,[numOfSubjects,1]);
TE_LS = linspace(0,70,71);
colors = {'r','g','b','k'};%'y','m','c',
colors = {[0.49,0.18,0.56],[0,0,0],[0,0.45,0.74],[0.07,0.61,0.07]};%'y','m','c',[0.85, 0.33, 0.10]
markers= {'x','d','p','s','.','o','+'};%'^','v','>','<',h','*'
plotOffset = 0.;
plotGroups = 4;
LineWidth = 1.5;

for index = 2:numberOfMet
    if index == 11
        indexMetabolite = 14;
    else
        if index == 14
            indexMetabolite = 11;
        else
            indexMetabolite = index;
        end
    end
    
    concentrationsMet = squeeze(concentrations(:,indexMetabolite,:));
    nonNanIndexes = ~isnan(concentrationsMet(:));
    f1 = fit(TEsRep(nonNanIndexes), concentrationsMet(nonNanIndexes), 'exp1');
%     currentLabel = string([metaboliteNamesDisplay2{indexMetabolite}, ' : T_2=' num2str(acceptableMeanT2s(indexMetabolite),2), ' ms']);
    currentLabel = string([metaboliteNamesDisplay2{indexMetabolite}]);
    if mod(index,plotGroups) == 2
        myVal = mod(ceil(index/plotGroups)+1,2)+1;
        if myVal == 1
            figure;
            sgtitle('Estimated T_2^{app} Decay by Fitting: Signal \propto exp( - TE / T_2^{app} )','FontWeight', 'bold')
        end
        subplot(1,2, myVal);
        if myVal == 1
            text(-0.15,0.98,'A','Units', 'Normalized', 'VerticalAlignment', 'Top', 'FontSize', 12,'FontWeight', 'bold');
        else
            text(-0.15,0.98,'B','Units', 'Normalized', 'VerticalAlignment', 'Top', 'FontSize', 12,'FontWeight', 'bold');
        end
        hold on
        offsetRec = plotOffset * (plotGroups/2+1.5);
        for indexTE = 1: numberOfTEs
            rectangle('position',[TEs(indexTE)-offsetRec -0.92 offsetRec*2 100], 'FaceColor',[0.85 .85 .85], 'LineStyle', 'none')
        end
        ylim([-1,0]);
%         title('T_2 decay curves and measured T_2 times')
        xlim([20 65])
        xlabel('TE (ms)')
        ylabel('Signal (arb. u.)')
        xticks([24,32,40,52,60])
        set(gca,'FontWeight', 'bold');
        indexLabel = 1;
    else
        hold on
        indexLabel = indexLabel + 1;
    end
    currentColor = colors{indexLabel};
    currentMarker = markers{indexLabel};
    offsetTEs = plotOffset * (indexLabel - plotGroups/2);
    errorbar(TEs+offsetTEs,mean(concentrationsMet, 'omitnan'), std(concentrationsMet,'omitnan'),currentMarker,'Color', currentColor, 'MarkerSize',8,...
        'MarkerEdgeColor',currentColor,'MarkerFaceColor',currentColor,  'LineWidth', LineWidth, 'HandleVisibility','off');
    scatter(TEs+offsetTEs, mean(concentrationsMet, 'omitnan'),currentMarker, 'MarkerEdgeColor',currentColor,'MarkerFaceColor',currentColor, 'DisplayName', currentLabel);
%     scatter(TEsRep(:), concentrationsMet(:),currentMarker, 'MarkerEdgeColor',currentColor,'MarkerFaceColor',currentColor);
    hold on
    meanVal = mean(concentrationsMet(:,1),'omitnan');
    meanValStart = meanVal ./ exp(-24/acceptableMeanT2s(indexMetabolite));
    plot(TE_LS+offsetTEs, meanValStart * exp(-TE_LS./acceptableMeanT2s(indexMetabolite)), 'color', currentColor, 'HandleVisibility','off', 'LineWidth', LineWidth, 'LineStyle','-.')
%     plot(f1)
    yLimits = get(gca,'yLim');
    maxValue = max(mean(concentrationsMet, 'omitnan')+std(concentrationsMet,'omitnan'));
    if (maxValue+1) > yLimits(2)
        ylim([-1 maxValue+1]);
    else
        ylim([-1 yLimits(2)]);
    end
    legend;
end

plusMinusSign = [' ' char(177) ' '];
tableT2Summarized = cell(numberOfMet+1,4);
tableT2Summarized(2:end,1) = metaboliteNamesDisplay;
tableT2Summarized(1,3) = {'Std'};
tableT2Summarized(1,4) = {['T2' plusMinusSign 'Std']};
tableT2Summarized(1,5) = {['R2' plusMinusSign 'Std']};
tableT2Summarized(2:end,2) = cellstr(num2str(acceptableMeanT2s','%.2f'));
tableT2Summarized(2:end,3) = cellstr(num2str(acceptableStdT2s','%.2f'));
tableT2Summarized(1,2) = {'T2 Summed'};
tableT2Summarized(1,3) = {['T2' plusMinusSign 'Std']};
tableT2Summarized(1,4) = {['R2' plusMinusSign 'Std']};
for index=1:numberOfMet
    tableT2Summarized{index+1,4} = [num2str(acceptableMeanT2s(index), '%.2f') plusMinusSign  num2str(acceptableStdT2s(index), '%.2f')];
    tableT2Summarized{index+1,5} = [num2str(acceptableMeanR2s(index), '%.2f') plusMinusSign  num2str(acceptableStdR2s(index), '%.2f')];
    tableT2Summarized{index+1,3} = [num2str(acceptableMeanT2s(index), '%.1f') plusMinusSign  num2str(acceptableStdT2s(index), '%.1f')];
    tableT2Summarized{index+1,4} = [num2str(acceptableMeanR2s(index), '%.2f') plusMinusSign  num2str(acceptableStdR2s(index), '%.2f')];
end

save([data_path , outputFolder, 'meanT2s.mat'],'acceptableMeanT2s', 'acceptableStdT2s', 'metaboliteNamesDisplay');
exportFileXlsx = [data_path , outputFolder, 'T2_Results_Mean_std.xlsx'];
xlswrite(exportFileXlsx, tableT2Summarized, 1, 'A1');

if sortT2sDescending
    if ~exist('sortedIndeces','var')
        if useTotalNAA
            % cheating, swapping the two metabolites. NAAB has high outliers,
            % and the mean value is higher
            indexNAAB = find(strcmp(metaboliteNameOrder,'NAA Broad'));
            indexNAA = find(strcmp(metaboliteNameOrder,'total NAA'));
            NAA_T2 = acceptableMeanT2s(indexNAA);
            acceptableMeanT2s(indexNAA) = acceptableMeanT2s(indexNAAB);
            acceptableMeanT2s(indexNAAB) = NAA_T2;
        end
        [acceptableMeanT2Sorted, sortedIndeces] = sort(acceptableMeanT2s,  'descend');
    else
        acceptableMeanT2Sorted = acceptableMeanT2s(sortedIndeces);
    end
    metaboliteNameOrder = metaboliteNameOrder(sortedIndeces);
    metaboliteNameOrderFWHM = metaboliteNameOrderFWHM(sortedIndeces);
    acceptableStdT2s = acceptableStdT2s(sortedIndeces);
    acceptableT2s = acceptableT2s(:, sortedIndeces);
    acceptableR2s = acceptableR2s(:, sortedIndeces);
    R2sNumeric = R2sNumeric(:, sortedIndeces);
    nonNegT2s = nonNegT2s(:, sortedIndeces);
    metaboliteNamesDisplay = metaboliteNamesDisplay(sortedIndeces);
end

%% plot the T2 Box plots
if emptyFigureIds
    figureIds{1} = figure;
else
    figure(figureIds{1});
end
    

positionsTicks = [2.2:1.5:1.5*numberOfMet+1.5];
positions1 = positionsTicks + offsetPlot;
boxPlotT2 = boxplot(acceptableT2s, metaboliteNamesDisplay,'colors',plotColors{1},'symbol','+','positions',positions1,'width',0.18);
ylabel('T_2^{app}   (ms)')
axT2 = get(figureIds{1},'CurrentAxes');
axT2.XAxis.TickLength = [0,0];
axT2.YColor = 'k';

set(boxPlotT2(:,:),'linewidth',1);
switch downField_MM_Met
    case 'DF'
        set(gca, 'YLim', [-5,70]);
        set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8+1.5 positionsTicks(end)+abs(offsetPlot)+0.8]);
    case 'MM'
        set(gca, 'YLim', [-5,95]);
        set(gca, 'XLim', [positionsTicks(2)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
    case 'MM WM'
        set(gca, 'YLim', [-5,95]);
        set(gca, 'XLim', [positionsTicks(2)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
    case 'Met'
        set(gca, 'YLim', [-5,200]);
        set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
end

set(gca, 'YGrid', 'on');
set(gca, 'XTickMode', 'manual');
set(gca, 'XTick', positionsTicks);
set(gca, 'XTickLabelRotation', 90.0);
set(gca, 'FontSize', 16);
title('T_2^{app} Relaxation Times');

%% FWHM
if doFWHMevalutation
    fwhmReshaped = zeros(size(tableFWHMSubjects,1)*size(tableFWHMSubjects,2), numberOfMet);
    for indexMetabolite = 1 :numberOfMet
        
        indexTable = find(strcmp(orderMetFWHMSubjects(:), metaboliteNameOrderFWHM(indexMetabolite)));
        
        fwhms = tableFWHMSubjects(:,:,indexTable);
        fwhms(fwhms>100)= NaN;
        fwhmReshaped(:,indexMetabolite) = fwhms(:);
    end
    
    
    if sortT2sDescending
        % Attention! "fwhmReshaped" is sorted already in the code above!
        % fwhmReshaped = fwhmReshaped(:,sortedIndeces);
%         FWHMSummedNumeric = FWHMSummedNumeric(sortedIndeces);
    end
    
    %% T2 corrected FWHM
    for i = 1:size(fwhmReshaped,1)
        %     fwhmReshapedT2corrected(i,:) = fwhmReshaped(i,:) - 1./(pi*acceptableT2s(floor(i./5)+1)*1e-3);
        if sortT2sDescending
            fwhmReshaped3(i,:) = fwhmReshaped(i,:) - 1./(pi*acceptableMeanT2Sorted*1e-3);
        else
            fwhmReshaped3(i,:) = fwhmReshaped(i,:) - 1./(pi*acceptableMeanT2s*1e-3);
        end
    end
    if sortT2sDescending
        fwhmReshaped3 = fwhmReshaped3 - fwhmReshaped3(:,1);
    else
        fwhmReshaped3 = fwhmReshaped3 - fwhmReshaped3(:,end);
    end
    
    T2RelaxationLinewidth = 1./(pi*acceptableT2s*1e-3);
    
    %% plotting of FWHM
    if emptyFigureIds
        figureIds{2} = plotFWHMs(fwhmReshaped, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors, '\Delta\nu_{1/2}', usePpm, yAxisLim, offsetPlot);
        figureIds{3} = plotFWHMs(fwhmReshaped3, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, '\Delta\nu_{residual}', usePpm, yAxisLim, offsetPlot);
    else
        figureIds{2} = plotFWHMs(fwhmReshaped, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors, '\Delta\nu_{1/2}', usePpm, yAxisLim, offsetPlot, figureIds{2});
        figureIds{3} = plotFWHMs(fwhmReshaped3, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, '\Delta\nu_{residual}', usePpm, yAxisLim, offsetPlot, figureIds{3});
    end
    
    %%
    if emptyFigureIds
        figureIds{4} = plotFWHMs(T2RelaxationLinewidth, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, '\Delta\nu_{1/2} vs. (\pi T_2^{app})^{-1}', usePpm, yAxisLim, offsetPlot);
    else
        figureIds{4} = plotFWHMs(T2RelaxationLinewidth, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, '\Delta\nu_{1/2} vs. (\pi T_2^{app})^{-1}', usePpm, yAxisLim, offsetPlot, figureIds{4});
    end
    figureIds{4} = plotFWHMs(fwhmReshaped, [], metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors, '\Delta\nu_{1/2} vs. (\pi T_2^{app})^{-1}', usePpm, yAxisLim, offsetPlot, figureIds{4});
    if usePpm
        text(0.06,1.0,'\Delta\nu_{1/2}','Units', 'Normalized', 'VerticalAlignment', 'Top', ...
            'FontSize', 13,'FontWeight', 'bold', 'Color',[0 0 1]);
        text(0.06,0.90,'(\pi T_2^{app})^{-1}','Units', 'Normalized', 'VerticalAlignment', 'Top', ...
            'FontSize', 13,'FontWeight', 'bold', 'Color',[0 0.5 0]);
        scatter(2.2,0.217,'filled','s','MarkerEdgeColor',[0,0,1], 'MarkerFaceColor',[0,0,1], 'LineWidth',2)
        scatter(2.2,0.187,'filled','s','MarkerEdgeColor',[0,0.5,0], 'MarkerFaceColor',[0,0.5,0], 'LineWidth',2)
    end
    tableFWHMSummarized = cell(numberOfMet+1,4);
    tableFWHMSummarized(2:end,1) = metaboliteNamesDisplay;
    tableFWHMSummarized(1,2) = {['FWHM' plusMinusSign 'Std']};
    if usePpm
        meanFwhmReshaped = mean(fwhmReshaped./scanFrequency, 'omitnan');
        stdFwhmReshaped = std(fwhmReshaped./scanFrequency, 'omitnan');
        writePrecision = '%.3f';
    else
        meanFwhmReshaped = mean(fwhmReshaped, 'omitnan');
        stdFwhmReshaped = std(fwhmReshaped, 'omitnan');
        writePrecision = '%.2f';
        writePrecision = '%.1f';
    end
    for index=1:numberOfMet
        tableFWHMSummarized{index+1,2} = [num2str(meanFwhmReshaped(index), writePrecision) plusMinusSign  num2str(stdFwhmReshaped(index), writePrecision)];
    end
    exportFileXlsx = [data_path , outputFolder, 'FWHM_Results_Mean_std.xlsx'];
    xlswrite(exportFileXlsx, tableFWHMSummarized, 1, 'A1');
end

end

function [figId] = plotFWHMs(fwhmReshaped, FWHMSummedNumeric, metaboliteNames, numberOfMet, scanFrequency, ...
    plotColors, titleName, usePpm, yAxisLim, offsetPlot, figId)
%% plot the FWHM Box plots
if ~exist('figId', 'var')
    figId = figure;
else
    figure(figId);
    hold on
end
positionsTicks = 2.2:1.5:1.5*numberOfMet+1.5;
positions = positionsTicks + offsetPlot;
fwhm_plot_limit = 150;

if (usePpm)
    fwhmReshapedPpm = fwhmReshaped ./ scanFrequency;
    fwhm_plot_limit_ppm = fwhm_plot_limit ./ scanFrequency;
    %set yyaxis left active, hence both
    yyaxis left
end

boxPlotFWHM = boxplot(fwhmReshaped, metaboliteNames,'colors',plotColors{1},'symbol','+','positions',positions);

if (usePpm)
    yyaxis right
    boxPlotFWHMPpm = boxplot(fwhmReshapedPpm, metaboliteNames,'colors',plotColors{1},'symbol','+','positions',positions);
    set(gca,'YColor','k');
    yyaxis left
end

% if isDownField
%     yyaxis left
%     set(gca, 'YLim', [0,fwhm_plot_limit]);
%     yyaxis right
%     set(gca, 'YLim', [0,fwhm_plot_limit_ppm]);
% end

ylabel('\Delta\nu (Hz)')
if usePpm
    yyaxis right
    ylabel('\Delta\nu (ppm)', 'Color', 'k')
    yyaxis left
end

ax = get(figId,'CurrentAxes');
ax.XAxis.TickLength = [0,0];
ax.YColor = 'k';

set(boxPlotFWHM(:,:),'linewidth',1);
set(gca, 'YGrid', 'on');
set(gca, 'XTickMode', 'manual');
set(gca, 'XTick', positionsTicks);
set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
set(gca, 'XTickLabelRotation', 90.0);
set(gca, 'FontSize', 16);
title(titleName);

if usePpm
%     yLimLower = 0;
    yLimLower = -3; %Hz, negative to add the 0 line :)
    yLimLower = -15; %Hz, negative to add the 0 line :)
    set(gca, 'YLim', [yLimLower,yAxisLim]);
    yyaxis right
    set(gca, 'YLim', [yLimLower/scanFrequency,yAxisLim./scanFrequency]);
else
    set(gca, 'YLim', [0,yAxisLim]);
end
end

function saveCRLBs(CRLBs, TEs, metaboliteNamesDisplay, data_path, outputFolder)
CRLBs = cell2mat(CRLBs(:,2:end,:));
metaboliteNamesDisplay = metaboliteNamesDisplay(2:end);
CRLBs_mean = squeeze(mean(CRLBs,1, 'omitnan'));
CRLBs_std  = squeeze(std(CRLBs,1, 'omitnan'));
numberOfMet = size(CRLBs_mean,1);
plusMinusSign = [' ' char(177) ' '];
tableCRLBSummarized = cell(numberOfMet+1,6);
tableCRLBSummarized(2:end,1) = metaboliteNamesDisplay;
for indexTE = 1:length(TEs)
    tableCRLBSummarized{1,indexTE+1} = ['TE = ', num2str(TEs(indexTE)), ' ms'];
    for indexMet=1:numberOfMet
        tableCRLBSummarized{indexMet+1,indexTE+1} = [sprintf('%.1f', CRLBs_mean(indexMet, indexTE))...
            plusMinusSign  sprintf('%.1f', CRLBs_std(indexMet, indexTE))];
    end
end

exportFileXlsx = [data_path , outputFolder, 'CRLB_Results_Mean_std.xlsx'];
xlswrite(exportFileXlsx, tableCRLBSummarized, 1, 'A1');

colors = {[0 0 1]; [0 0 0]; [0 0.5 0]; [0 0 0]; [0.7 0.2 0.2]};
offsetPlot = 0.25;
positionsTicks = 2.2:1.5:1.5*numberOfMet+1.5;
figId = figure;
hold on
for indexTE = 1:2:length(TEs)
    
    positions = positionsTicks + offsetPlot * (indexTE-3);
    boxplot(CRLBs(:,:,indexTE), metaboliteNamesDisplay,'colors',colors{indexTE},'symbol','+',...
        'positions',positions, 'Widths',0.3);
    text(0.3,0.95 - (indexTE-1) * 0.03,['TE = ' num2str(TEs(indexTE)), ' ms'],'Units', 'Normalized', ...
        'VerticalAlignment', 'Top', 'FontSize', 12,'FontWeight', 'bold', 'Color',colors{indexTE});
    scatter(7.5,580 - (indexTE-1) * 20,'filled','s','MarkerEdgeColor',colors{indexTE}, 'MarkerFaceColor',colors{indexTE}, 'LineWidth',1.5);
end

ylabel('CRLBs (%)')
ylim([0 620])

ax = get(figId,'CurrentAxes');
ax.XAxis.TickLength = [0,0];
ax.YColor = 'k';

set(gca, 'YGrid', 'on');
set(gca, 'XTickMode', 'manual');
set(gca, 'XTick', positionsTicks);
set(gca, 'XLim', [positionsTicks(1)-0.8 positionsTicks(end)+0.8]);
set(gca, 'XTickLabelRotation', 90.0);
end