function plotAminoAcidBasis


% aminoAcids = {'ALA';'ARG';'ASN';'ASP';'CYS';'GLU';'GLN';'GLY';'HIS';'ILE';'LEU';'LYS';'MET';'PHE';'PRO';'SER';'THR';'TRP';'TYR';'VAL'};
aminoAcids = {'GLU';'LEU';'LYS';'PRO';'TYR'};
dna = {'DA';'DC';'DG';'DT'};
rna = {'A';'C';'G';'U'};

pathBase = 'D:/Software/Spectro Data/AminoAcids/';
currentAcids = 'AminoAcids';


switch currentAcids
    case 'AminoAcids'
        currentMetabolites = aminoAcids;
        pathAminoAcids = [pathBase 'AminoAcids/'];
    case 'DNA'
        currentMetabolites = dna;
        pathAminoAcids = [pathBase 'DNA/'];
    case 'RNA'
        currentMetabolites = rna;
        pathAminoAcids = [pathBase 'RNA/'];
end

figID = figure;
numberOfMetabolites = length(currentMetabolites);
offsetStep = 1.2;

for indexAminoAcid = 1:numberOfMetabolites
    currentAminoAcid = currentMetabolites{indexAminoAcid};
    load([pathAminoAcids currentAminoAcid '.mat'], 'yBinsTotal', 'xBins', 'yBinsFitTotal', 'yBinsFitTotalVisible');
    
    scaleValue =  max(yBinsFitTotal);
    visibleScaledOffset = yBinsFitTotalVisible ./ scaleValue + offsetStep * (numberOfMetabolites-indexAminoAcid+1);
    unvisibleScaledOffset = (yBinsFitTotal-yBinsFitTotalVisible) ./ scaleValue;
    
    set(0, 'currentfigure', figID);
    hold on
    h = area(xBins, [visibleScaledOffset unvisibleScaledOffset]);
    
    h(1).FaceColor = 'none';
    h(2).FaceColor = [0 0.7 0.8];
    h(2).FaceAlpha = 1.0;
    h(1).EdgeColor = 'none';
    h(2).EdgeColor = [0 0 0];
end

set(0, 'currentfigure', figID);
set(gca,'YLim',[0.7, (indexAminoAcid+1) * offsetStep + 0.1]);
set(gca,'XLim',[0, 10]);
set(gca,'ytick',[]);
xlabel('ppm');
set(gca,'xDir','reverse');
set(gca,'LineWidth',1);

% phase0Deg= -50;
% phase0         = phase0Deg * pi/180; %in rads 
% fid = ifft(ifftshift(yBinsFitTotalVisible));
% fidPhased = fid .* exp(1i*phase0);
% figure
% plot(xBins,real(fftshift(fft(fidPhased))))
% set(gca,'XLim',[0, 10]);
% xlabel('ppm');
% set(gca,'xDir','reverse');