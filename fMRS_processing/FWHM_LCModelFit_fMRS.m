function FWHM_LCModelFit_fMRS(FileName, PathName, saveFigures, displayBaseline)
clear; close all;
%%%% select all 10 blocks
%D:\PAPER\1H_fMRS\Data_TimeCourse\Output_v2_MMBSummed\Summed_fMRSData

%check file2save
file2save = 'BOLD_XXXX_Stim_woBOLDCorr_v2_Excluded_SummedData';

if ~exist('FileName', 'var')
    [FileName,PathName] = uigetfile('*.coord','Please select .coord files from LCModel','Multiselect','on');    %select coord files from all 10 blocks (2:40min)
end
%
if ~iscell(FileName)
    FileName= {FileName};
end

xLimits = [-10 8];

xLimitsText = xLimits(1) -0.03;


%
ppm = {}; pData = {}; fData = {}; bData = {}; mmData = {}; rData = {}; % close all;

for index =1:length(FileName)
%     figId = figure;%(index+10); %I am not sure if we need this indexing anymore. It is useful to have it like this for ProFit comparison
    %[c1 c2 c3 c4] = textread(strcat(PathName,FileName{i}),'%s %s %s %s');
    c          = textread(strcat(PathName,FileName{index}),'%s');
    nOfPoints  = find(strcmp(c ,'points'),1);
    nOfPoints  = str2num (c{nOfPoints-1});
    
    %ppm axis
    indexOfPpm   = find(strcmp(c,'ppm-axis'))+3;
    endOfMet     = nOfPoints+indexOfPpm-1;
    ppm{index}= str2double( c(indexOfPpm:endOfMet,1));
    
    %phaseData
    indexOfMet   = find(strcmp(c,'phased'))+4;
    endOfMet     = nOfPoints+indexOfMet-1;
    pData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %fit data
    indexOfMet   = find(strcmp(c,'fit'))+5;
    endOfMet     = nOfPoints+indexOfMet-1;
    fData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %background
    indexOfMet   = find(strcmp(c,'background'))+3;
    endOfMet     = nOfPoints+indexOfMet-1;
    bData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %residual
    rData{index} = pData{index} - fData{index};
    
    %scaling calculation
    scale = max(pData{index});
    
    
%     FWHM of LCModel Fit
    metaboliteNames = {'NAA', 'Cr', 'PCr'};
    centerPpm =       [ 2.008, 3.027,3.029];
    bandwidth = 8000; scanFreq_MHz = 399.717736; nTimepoints = length(fData{1});
    ppmStart = ppm{1}(1); ppmEnd = ppm{1}(end);
    searchArea = 0.3; %ppm
    ppmVector = ppm{1};
%     fwhm = zeros(length(metaboliteNames),length(FileName));
%     freq = zeros(length(metaboliteNames),length(FileName));
    metaboliteSpectrum = zeros(nOfPoints,1);
    for indexMetabolite = 1:length(metaboliteNames)
        %evaluate each metabolite
            individualMet = strsplit(metaboliteNames{indexMetabolite},'+');
        for indexIndividualMet=1:length(individualMet)
            %sum up the individual components
            indexOfMet   = find(strcmp(c,individualMet{indexIndividualMet}),1,'last')+3;
            endOfMet     = nOfPoints+indexOfMet-1;
            if (indexOfMet > indexOfPpm) %make sure that we indeed have a metabolite quantification spectrum
                metaboliteSpectrum = metaboliteSpectrum + str2double(c(indexOfMet:endOfMet,1)) - bData{index};
            end
        end
    end
    for indexMetabolite = 1:length(metaboliteNames)
        %     FWHM of LCModel Fit
        ppmStartMet = centerPpm(indexMetabolite) + searchArea;
        ppmEndMet = centerPpm(indexMetabolite) - searchArea;
        spectrumPeak = metaboliteSpectrum;
        spectrumPeak(ppmVector>ppmStartMet | ppmVector < ppmEndMet) = 0;
        [fwhm(indexMetabolite,index), ~] = fwhm_Hz(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
        freq(indexMetabolite,index) = freq_ppm(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
        amp(indexMetabolite,index) = max(spectrumPeak);
    end
end
close all

metaboliteLabels = {'NAA-CH3', 'tCr-CH3'};
    file2save_NAA = (strcat('D:\PAPER\1H_fMRS\Data_TimeCourse\Output_v2_MMBSummed\BOLD\',strrep(file2save,'XXXX','NAA-CH3'),'.mat'));
    BOLD_NAA_Stim_woBOLDCorr_v2_Excluded_SummedData.fwhm = fwhm(1,:);
    BOLD_NAA_Stim_woBOLDCorr_v2_Excluded_SummedData.amplitude = amp(1,:);
    save(file2save_NAA,'BOLD_NAA_Stim_woBOLDCorr_v2_Excluded_SummedData')
    file2save_Cr = fullfile(strcat('D:\PAPER\1H_fMRS\Data_TimeCourse\Output_v2_MMBSummed\BOLD\',strrep(file2save,'XXXX','tCr-CH3'),'.mat'));
    BOLD_Cr_Stim_woBOLDCorr_v2_Excluded_SummedData.fwhm = fwhm(2,:);
    BOLD_Cr_Stim_woBOLDCorr_v2_Excluded_SummedData.amplitude = amp(2,:);
%     save(file2save_Cr,'BOLD_Cr_Stim_woBOLDCorr_v2_Excluded_SummedData')

x = linspace(320/60/4,(320*5/60-320/60/4),10);
for indexMet = 1:length(metaboliteLabels)
    %     figure
    %     plot(x,fwhm(indexMet,:),'o-')
    %
    %     title (['BOLD_fromLCModelFit_',num2str(metaboliteLabels{indexMet}),' fwhm'], 'Interpreter', 'none');
    %     xlabel 't / min';
    %     yLimits = get(gca,'YLim');
    %     nBlocks = 10; boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
    %     for i = 1:5
    %         boxoff  = [boxoff 320/60*i 320/60*i];
    %         if i < nBlocks/2
    %             boxon   = [boxon 320/60*i 320/60*i];
    %         end
    %     end
    %     for ii = 1:nBlocks/4
    %         boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    %         boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    %     end
    %     patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
    %     patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
    %     ylim([yLimits(1) yLimits(2)]);
    %     xlim([0 320*5/60]);set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
    %     set(gca, 'FontWeight','bold');
    %     fig2save = fullfile(strcat('D:\PAPER\1H_fMRS\Data_TimeCourse\Output_v2_MMBSummed\BOLD\',strrep(file2save,'XXXX',metaboliteLabels{indexMet}),'_fwhm.fig'));
    %     savefig(fig2save)
    
    %%amplitude in a.u.
%     figure
%     plot(x,amp(indexMet,:)./10,'o-')
%     
%     title (['BOLD_fromLCModelFit_',num2str(metaboliteLabels{indexMet}),' amplitude'], 'Interpreter', 'none');
%     xlabel 't / min';
%     yLimits = get(gca,'YLim');
%     nBlocks = 10; boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
%     for i = 1:nBlocks/2
%         boxoff  = [boxoff 320/60*i 320/60*i];
%         if i < nBlocks/2
%             boxon   = [boxon 320/60*i 320/60*i];
%         end
%     end
%     for ii = 1:nBlocks/4
%         boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%     end
%     patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
%     patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
%     ylim([yLimits(1) yLimits(2)]);
%     xlim([0 320*5/60]);set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
%     set(gca, 'FontWeight','bold');
%     fig2save = fullfile(strcat('D:\PAPER\1H_fMRS\Data_TimeCourse\Output_v2_MMBSummed\BOLD\',strrep(file2save,'XXXX',metaboliteLabels{indexMet}),'_amplitude.fig'));
%     savefig(fig2save)
    
    %amplitude in percent change
    figure
    amp_Perc = (amp(indexMet,:)-amp(indexMet,2))./amp(indexMet,2).*100;
    plot(x,amp_Perc,'o-')
    
    title (['BOLD_fromLCModelFit_',num2str(metaboliteLabels{indexMet}),' amplitude'], 'Interpreter', 'none');
    xlabel 't / min'; ylabel 'Peak height change / %'
    yLimits = get(gca,'YLim');
    nBlocks = 10; boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
    for i = 1:nBlocks/2
        boxoff  = [boxoff 320/60*i 320/60*i];
        if i < nBlocks/2
            boxon   = [boxon 320/60*i 320/60*i];
        end
    end
    for ii = 1:nBlocks/4
        boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    end
    patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
    patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
    ylim([yLimits(1) yLimits(2)]);
    xlim([0 320*5/60]);set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
    set(gca, 'FontWeight','bold');
    fig2save = fullfile(strcat('D:\PAPER\1H_fMRS\Data_TimeCourse\Output_v2_MMBSummed\BOLD\',strrep(file2save,'XXXX',metaboliteLabels{indexMet}),'_amplitudePerc.fig'));
    savefig(fig2save)
end

% % correlation FWHM, Amplitude
metabType = {'NAA-CH3', 'tCr-CH3'};
metabType = {'Water', 'NAA-CH3'};
metabType = {'Water', 'tCr-CH3'};
OutputFilesPathLocal = 'D:\PAPER\1H_fMRS\Data_TimeCourse\Output_v2_MMBSummed\BOLD\';
CorrBOLDSpearman(metabType,'Stim','_woBOLDCorr_v2',OutputFilesPathLocal,'','true')
end




function CorrBOLDSpearman(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end

for idxmetab = 1:length(metabType)
    if strcmp(metabType{idxmetab}(1:2),'Wa')
        if exist('excludeSubjects')
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType(1:end-3),'_Excluded_SummedData');
        else
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType(1:end-3),'_SummedData');
        end
    else
        if exist('excludeSubjects')
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,woBaseline,'_Excluded_SummedData');
        else
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,woBaseline,'_SummedData');
        end
    end
    impFileMat = strcat(OutputFilesPathLocal,impName,'.mat');
    BOLDData = load(impFileMat);
    fields = fieldnames(BOLDData);
    BOLDDataXY(idxmetab) = BOLDData.(fields{1});
end

if exist('excludeSubjects')
    if length(metabType) == 2
        file2save = strcat('Corr_',metabType{1},'_',metabType{2},'_',visualType,BOLDType,woBaseline,'_Excluded_SummedData');
    elseif length(metabType) == 1
        file2save = strcat('Corr_',metabType{1},'_FWHMAmp_',visualType,BOLDType,woBaseline,'_Excluded_SummedData');
    end
else
    if length(metabType) == 2
        file2save = strcat('Corr_',metabType{1},'_',metabType{2},'_',visualType,BOLDType,woBaseline,'_SummedData');
    elseif length(metabType) == 1
        file2save = strcat('Corr_',metabType{1},'_FWHMAmp_',visualType,BOLDType,woBaseline,'_SummedData');
    end
end

% subjLength = length(BOLDDataXY(1).fwhm);
subjLength = 1;
subjects = ones(subjLength*10,1);
for i = 1:subjLength
    subjects((1+(i-1)*10):(10+(i-1)*10)) = subjects((1+(i-1)*10):(10+(i-1)*10)).*i;
end

if length(metabType) == 2
    %for FWHM (Hz)
    fwhmTab = table(BOLDDataXY(1).fwhm(:),BOLDDataXY(2).fwhm(:),subjects);
%     fwhmTab = table(BOLDDataXY(1).amplitude(:),BOLDDataXY(2).fwhm(:),subjects);
    BOLDData_FWHM = table2array(fwhmTab);
    %Spearman correlation
    [RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Spearman');
    % [RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Pearson');
    %linear regression
    fit = fitlm(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2));
    figure
    hold on
    % plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
    plot(fit); box on;
    xlabel(['FWHM ',num2str(metabType{1}), ' / Hz'])
    ylabel(['FWHM ',num2str(metabType{2}), ' / Hz'])
    title(['Correlation FWHM ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
    txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
    text(min(BOLDData_FWHM(:,1)),max(BOLDData_FWHM(:,2)),txt)
    set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
        lines(i).LineWidth = 1.5;
    end
    fig2saveFWHM = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhm.fig'));
    savefig(fig2saveFWHM)
    
%     %for FWHM (Delta Hz)
%     subjectsDelta = subjects(1:9);
%     fwhmTabDelta = table(diff(BOLDDataXY(1).fwhm(:)),diff(BOLDDataXY(2).fwhm(:)),subjectsDelta);
%     BOLDData_FWHM_Delta = table2array(fwhmTabDelta);
%     %Spearman correlation
%     [RHODelta,PVALDelta] = corr(BOLDData_FWHM_Delta(:,1),BOLDData_FWHM_Delta(:,2),'Type','Spearman');
%     %linear regression
%     fitDelta = fitlm(BOLDData_FWHM_Delta(:,1),BOLDData_FWHM_Delta(:,2));
%     figure
%     hold on
%     % plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
%     plot(fitDelta); box on;
%     xlabel([char(916) 'FWHM ',num2str(metabType{1}), ' / Hz'])
%     ylabel([char(916) 'FWHM ',num2str(metabType{2}), ' / Hz'])
%     title(['Correlation ' char(916) 'FWHM ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
%     txt = {['R = ' num2str(RHODelta)],['p < ' num2str(PVALDelta)]};
%     text(min(BOLDData_FWHM_Delta(:,1)),max(BOLDData_FWHM_Delta(:,2)),txt)
%     set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
%     lines = findobj(gcf,'Type','Line');
%     for i = 1:numel(lines)
%         lines(i).LineWidth = 1.5;
%     end
%     fig2saveFWHMDelta = fullfile(OutputFilesPathLocal, strcat(file2save,'_Deltafwhm.fig'));
% %     savefig(fig2saveFWHMDelta)
    
    %for amplitude (a.u.)
    ampTab = table(BOLDDataXY(1).amplitude(:),BOLDDataXY(2).amplitude(:)./10,subjects);
    BOLDData_amp = table2array(ampTab);
    %Spearman correlation
    [RHO,PVAL] = corr(BOLDData_amp(:,1),BOLDData_amp(:,2),'Type','Spearman');
    %linear regression
    fit = fitlm(BOLDData_amp(:,1),BOLDData_amp(:,2));
    figure
    hold on
    % plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
    plot(fit); box on;
    xlabel(['Amplitude ',num2str(metabType{1}), ' / a.u.'])
    ylabel(['Amplitude ',num2str(metabType{2}), ' / a.u.'])
    title(['Correlation Amplitude ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
    txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
    text(min(BOLDData_amp(:,1)),max(BOLDData_amp(:,2)),txt)
    set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
        lines(i).LineWidth = 1.5;
    end
    fig2saveAmplitude = fullfile(OutputFilesPathLocal, strcat(file2save,'_amp.fig'));
    savefig(fig2saveAmplitude)
    
%     %for amplitude (Delta a.u.)
%     ampTabDelta = table(diff(BOLDDataXY(1).amplitude(:)),diff(BOLDDataXY(2).amplitude(:)),subjectsDelta);
%     BOLDData_amp_Delta = table2array(ampTabDelta);
%     %Spearman correlation
%     [RHODelta,PVALDelta] = corr(BOLDData_amp_Delta(:,1),BOLDData_amp_Delta(:,2),'Type','Spearman');
%     %linear regression
%     fitDelta = fitlm(BOLDData_amp_Delta(:,1),BOLDData_amp_Delta(:,2));
%     figure
%     hold on
%     % plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
%     plot(fitDelta); box on;
%     xlabel([char(916) 'Amplitude ',num2str(metabType{1}), ' / a.u.'])
%     ylabel([char(916) 'Amplitude ',num2str(metabType{2}), ' / a.u.'])
%     title(['Correlation ' char(916) 'Amplitude ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
%     txt = {['R = ' num2str(RHODelta)],['p < ' num2str(PVALDelta)]};
%     text(min(BOLDData_amp_Delta(:,1)),max(BOLDData_amp_Delta(:,2)),txt)
%     set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
%     lines = findobj(gcf,'Type','Line');
%     for i = 1:numel(lines)
%         lines(i).LineWidth = 1.5;
%     end
%     fig2saveAmplitudeDelta = fullfile(OutputFilesPathLocal, strcat(file2save,'_Deltaamp.fig'));
% %     savefig(fig2saveAmplitudeDelta)
    
% elseif length(metabType) == 1
%     %for FWHM (Hz) / Amp (a.u.)
%     fwhmAmpTab = table(BOLDDataXY(1).fwhm(:),BOLDDataXY(1).amplitude(:),subjects);
%     BOLDData = table2array(fwhmAmpTab);
%     %Spearman correlation
%     [RHO,PVAL] = corr(BOLDData(:,1),BOLDData(:,2),'Type','Spearman');
%     % [RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Pearson');
%     %linear regression
%     fit = fitlm(BOLDData(:,2),BOLDData(:,1));
%     figure
%     hold on
%     plot(fit); box on;
%     xlabel(['Amplitude ',num2str(metabType{1}), ' / a.u.'])
%     ylabel(['FWHM ',num2str(metabType{1}), ' / Hz'])
%     title(['Correlation FWHM & Amplitude ', num2str(metabType{1}), woBaseline], 'Interpreter', 'none')
%     txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
%     text(min(BOLDData(:,2))+min(BOLDData(:,2))*0.001,min(BOLDData(:,1))+min(BOLDData(:,1))*0.001,txt)
%     set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
%     lines = findobj(gcf,'Type','Line');
%     for i = 1:numel(lines)
%         lines(i).LineWidth = 1.5;
%     end
%     fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'.fig'));
%     savefig(fig2save)
%     
%     %for FWHM (Delta Hz) / Amp (Delta a.u.)
%     subjectsDelta = subjects(1:9);
%     fwhmAmpTabDelta = table(diff(BOLDDataXY(1).fwhm(:)),diff(BOLDDataXY(1).amplitude(:)),subjectsDelta);
%     BOLDData_Delta = table2array(fwhmAmpTabDelta);
%     %Spearman correlation
%     [RHODelta,PVALDelta] = corr(BOLDData_Delta(:,1),BOLDData_Delta(:,2),'Type','Spearman');
%     %linear regression
%     fitDelta = fitlm(BOLDData_Delta(:,2),BOLDData_Delta(:,1));
%     figure
%     hold on
%     % plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
%     plot(fitDelta); box on;
%     xlabel([char(916) 'Amplitude ',num2str(metabType{1}), ' / a.u.'])
%     ylabel([char(916) 'FWHM ',num2str(metabType{1}), ' / Hz'])
%     title(['Correlation ' char(916) 'FWHM & Amplitude ', num2str(metabType{1}), woBaseline], 'Interpreter', 'none')
%     txt = {['R = ' num2str(RHODelta)],['p < ' num2str(PVALDelta)]};
%     text(min(BOLDData_Delta(:,2)),min(BOLDData_Delta(:,1))+0.05,txt)
%     set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
%     lines = findobj(gcf,'Type','Line');
%     for i = 1:numel(lines)
%         lines(i).LineWidth = 1.5;
%     end
%     fig2saveDelta = fullfile(OutputFilesPathLocal, strcat(file2save,'_Delta.fig'));
% %     savefig(fig2saveDelta)
end
end