% load LCModel fit results
pathName = 'MM data path';
sampleCriteria = {'OutputAminoAcids'};
[MMPathBase] = pathToDataFolder(pathName, sampleCriteria);
LCModelOutputPath = [MMPathBase 'OutputAminoAcids\'];
LCModelTableFile = 'Summed_Averaged_TE24.table';
currentTE = {'24'};
[tableConcentrations, wconc_LCModel, atth2o_LCModel] = importConcentrationLCModelTable({LCModelTableFile},LCModelOutputPath, currentTE, 'AA');
[relativeConcentrations, subjects, metaboliteNames]= extractConcentrations({tableConcentrations}, 1);

%load Amino acids basis set
pathNameAA = 'AA basis path';
sampleCriteriaAA = {''};
[filePathAA] = pathToDataFolder(pathNameAA, sampleCriteriaAA);
load([filePathAA, 'basis_set.mat'], 'basis');

% match the creatines even if naming might be slightly different
indexCrFit = find(contains(metaboliteNames, 'Cr'));
indexCrBasis = find(contains(basis.met, 'Cr'));
metaboliteNames = strrep(metaboliteNames,metaboliteNames{indexCrFit},basis.met{indexCrBasis});

nTimepoints = length(basis.fid{1});

% get the original spectrum
load([MMPathBase, 'Summed_Averaged_TE24.mat'], 'summedSpectraTE')
data = summedSpectraTE.Data{1};
measuredSpectrum = fftshift(fft(data(:,1,1,1,1,1,1,1,1,1,1,1,1), nTimepoints));

%get some starting values
numOfAminoAcids_Met = length(basis.met);
fittedSpectrum = zeros(size(basis.fid{1}));
scanFreuency_Hz = summedSpectraTE.Parameter.Headers.ScanFrequency;
bandwidth_Hz = summedSpectraTE.Parameter.Headers.Bandwidth_Hz;
ppmVector = ppmVectorFunction(scanFreuency_Hz, bandwidth_Hz, nTimepoints, '1H');

figure;
%reconstruct a pseudo fit spectrum
for indexAA = 1:numOfAminoAcids_Met
    currentAA = basis.met{indexAA};
    spectrumAA = fftshift(fft(basis.fid{indexAA}));
    indexConc = find(strcmp(metaboliteNames, currentAA));
    if ~isempty(indexConc)
        if ~isnan(relativeConcentrations(indexConc))
            fittedSpectrumAA = spectrumAA * relativeConcentrations(indexConc);
            hold on
            plot(ppmVector, real(fittedSpectrumAA))
            fittedSpectrum = fittedSpectrum + fittedSpectrumAA;
        end
    end
end

% plotting result
figure;
plot(ppmVector, real(fittedSpectrum))
hold on 
plot(ppmVector, real(measuredSpectrum))
