function aminoAcidsCalculations

currentAcids = 'DNA'; % 'DNA' 'RNA' 'AminoAcids'
useSimpleBins = false;

aminoAcids = {'ALA'; 'ARG';'ASN';'ASP';'CYS';'GLU';'GLN';'GLY';'HIS';'ILE';'LEU';'LYS';'MET';'PHE';'PRO';'SER';'THR';'TRP';'TYR';'VAL'};

AA_resonances = {...
    {'H','HA','HB1','HB2','HB3'}; ... ALA
    {'H','HA','HB2','HB3','HD2','HD3','HE','HG2','HG3','HH11','HH12','HH21','HH22'}; ... ARG
    {'H','HA','HB2','HB3','HD21','HD22'}; ... ASN
    {'H','HA','HB2','HB3','HD2'}; ... ASP
    {'H','HA','HB2','HB3','HG'}; ... CYS
    {'H','HA','HB2','HB3','HE2','HG2','HG3'}; ... GLU
    {'H','HA','HB2','HB3','HE21','HE22','HG2','HG3'}; ... GLN
    {'H','HA2','HA3'}; ... GLY
    {'H','HA','HB2','HB3','HD1','HD2','HE1','HE2'}; ... HIS
    {'H','HA','HB','HD11','HD12','HD13','HG12','HG13','HG21','HG22','HG23'}; ... ILE
    {'H','HA','HB2','HB3','HD11','HD12','HD13','HD21','HD22','HD23', 'HG'}; ... LEU
    {'H','HA','HB2','HB3','HD2','HD3','HE2','HE3','HG2','HG3','HZ1','HZ2','HZ3'}; ... LYS
    {'H','HA','HB2','HB3','HE1','HE2','HE3','HG2','HG3'}; ... MET
    {'H','HA','HB2','HB3','HD1','HD2','HE1','HE2','HZ'}; ... PHE
    {'H2','HA','HB2','HB3','HD2','HD3','HG2','HG3'}; ... PRO
    {'H','HA','HB2','HB3','HG'}; ... SER
    {'H','HA','HB','HG1','HG21','HG22','HG23'}; ... THR
    {'H','HA','HB2','HB3','HD1','HE1','HE3','HH2','HZ2','HZ3'}; ... TRP
    {'H','HA','HB2','HB3','HD1','HD2','HE1','HE2','HH'}; ... TYR
    {'H','HA','HB','HG11','HG12','HG13','HG21','HG22','HG23'}; ... VAL
    };

AA_visible_resonances = {...
    {'HA','HB1','HB2','HB3'}; ... ALA
    {'HA','HB2','HB3','HD2','HD3','HG2','HG3'}; ... ARG
    {'HA','HB2','HB3'}; ... ASN
    {'HA','HB2','HB3'}; ... ASP
    {'HA','HB2','HB3','HG'}; ... CYS
    {'HA','HB2','HB3','HG2','HG3'}; ... GLU
    {'HA','HB2','HB3','HG2','HG3'}; ... GLN
    {'HA2','HA3'}; ... GLY
    {'HA','HB2','HB3','HD2','HE1'}; ... HIS  ?? ,'HD1','HE2' (ring maybe HD2 and HE1 also)
    {'HA','HB','HD11','HD12','HD13','HG12','HG13','HG21','HG22','HG23'}; ... ILE
    {'HA','HB2','HB3','HD11','HD12','HD13','HD21','HD22','HD23', 'HG'}; ... LEU
    {'HA','HB2','HB3','HD2','HD3','HE2','HE3','HG2','HG3'}; ... LYS
    {'HA','HB2','HB3','HE1','HE2','HE3','HG2','HG3'}; ... MET
    {'HA','HB2','HB3','HD1','HD2','HE1','HE2','HZ'}; ... PHE
    {'HA','HB2','HB3','HD2','HD3','HG2','HG3'}; ... PRO
    {'HA','HB2','HB3'}; ... SER
    {'HA','HB','HG21','HG22','HG23'}; ... THR
    {'HA','HB2','HB3','HD1','HE3','HH2','HZ2','HZ3'}; ... TRP
    {'HA','HB2','HB3','HD1','HD2','HE1','HE2'}; ... TYR
    {'HA','HB','HG11','HG12','HG13','HG21','HG22','HG23'}; ... VAL
    };

%######################################################################################
dna = {'DA';'DC';'DG';'DT'};
dna_resonances = {...
    {'H1''','H2','H2''','H2''''','H3''','H4''','H5''','H5''''','H61','H62','H8'}; ... DA
    {'H1''','H2''','H2''''','H3''','H4''','H41','H42','H5','H5''','H5''''','H6'}; ... DC
    {'H1','H1''','H2''','H2''''','H21','H22','H3''','H4''','H5''','H5''''','H8'}; ... DG
    {'H1''','H2''','H2''''','H3','H3''','H4''','H5''','H5''''','H6','H71','H72','H73'}; ... DT
    };

dna_visible_resonances = {...
    {'H1''','H2','H2''','H2''''','H3''','H4''','H5''','H5''''','H61','H8'}; ... DA % not ,'H62' ! ,'H61' -> connected?
    {'H1''','H2''','H2''''','H3''','H4''','H41','H5','H5''','H5''''','H6'}; ... DC % not ,'H42' ! ,'H41' -> connected?
    {'H1','H1''','H2''','H2''''','H21','H3''','H4''','H5''','H5''''','H8'}; ... DG % not ,'H22' ! ,'H21' -> connected?
    {'H1''','H2''','H2''''','H3','H3''','H4''','H5''','H5''''','H6','H71','H72','H73'}; ... DT
    };

%######################################################################################

rna = {'A';'C';'G';'U'};
rna_resonances = {...
    {'H1''','H2','H2''','H3''','H4''','H5''','H5''''','H5''''+','H61','H62','H8','HO2'''}; ... A
    {'H1''','H2''','H3''','H4''','H41','H42','H5','H5''','H5''''','H5''''+','H6','HO2'''}; ... C
    {'H1','H1''','H2''','H21','H22','H3''','H4''','H5''','H5''''','H5''''+','H8','HO2'''}; ... G
    {'H1''','H2''','H3','H3''','H4''','H5','H5''','H5''''','H5''''+','H6','HO2'''}; ... U
    };

pathBase = 'D:/Software/Spectro Data/AminoAcids/';

switch currentAcids
    case 'AminoAcids' 
        currentMetabolites = aminoAcids;
        metabolite_resonances = AA_resonances;
        metabolite_visible_resonances = AA_visible_resonances;
        pathAminoAcids = [pathBase 'AminoAcids/'];
    case 'DNA'
        currentMetabolites = dna;
        metabolite_resonances = dna_resonances;
        metabolite_visible_resonances = dna_visible_resonances;
        pathAminoAcids = [pathBase 'DNA/'];
    case 'RNA'
        currentMetabolites = rna;
        metabolite_resonances = rna_resonances;
        metabolite_visible_resonances = rna_resonances; %TODO CORRECT
        pathAminoAcids = [pathBase 'RNA/'];
end

interpFactor = 4;
scanFrequencyMock = 1; %we use this to get the fwhm in Hz

%% preparing LCModel export
scanFrequency = 399.718195 * 1e6;
bandwidth_Hz = 8000;
dwellTimeMs = 1/bandwidth_Hz * 1e3;

if useSimpleBins    
    ppmStart = 10.0;
    ppmEnd = 0.0;
    xBins = [ppmEnd:0.01:ppmStart]';
else
    dataSize = 8192;
    [ppmVector] = getPpmVector(scanFrequency, bandwidth_Hz, dataSize);
    xBins = ppmVector';
    ppmStart = max(ppmVector);
    ppmEnd = min(ppmVector);
end


highNumberVector = zeros(size(xBins));
highNumberVector = highNumberVector + 5000;

colors = {[0.5 0.5 0.8];  [0 0.7 0.5]; [0 0 0];  [0 1 1]; [1 0.5 0]; [1 0 0]; [1 1 0]; [0.7 0.3 0];};
alphas = {     0.9;          0.9;        0.9;      0.7;      1.0;        0.8;     0.7;      0.7};
        
%% end of LCModel export preparations
numOfAminoAcids = length(currentMetabolites);
tableFWHM_infos = {};

for indexAminoAcid = 1:numOfAminoAcids
    currentAminoAcid = currentMetabolites{indexAminoAcid};
    numberOfResonances = length(metabolite_resonances{indexAminoAcid});
   
    yBinsTotal = zeros(size(xBins));
    yBinsFitTotal = zeros(size(xBins));
    yBinsTotalVisible = zeros(size(xBins));
    yBinsFitTotalVisible = zeros(size(xBins));
    
    for indexProtonResonance = 1:numberOfResonances
        currentResonance = metabolite_resonances{indexAminoAcid}{indexProtonResonance};
        currentFile = [currentAminoAcid, '/', currentAminoAcid, '_', currentResonance];
        currentCSVFile =  [currentFile '.csv'];
        
        resonanceTable = readtable([pathAminoAcids currentCSVFile]);
        resonanceValues = resonanceTable{1:end,8};
        
        yBins = zeros(size(xBins));
        for indexResonance = 1:length(resonanceValues)
            xBinsDiff = abs(xBins - resonanceValues(indexResonance));
            [~, indexMin] = min(xBinsDiff);
            yBins(indexMin) = yBins(indexMin) + 1;
        end
        % null the ends
        yBins(end) = 0;
        yBins(1) = 0;
        [myFit, gof, out] = fit(xBins, yBins, 'smoothingspline', 'SmoothingParam', 0.99999);
        yBinsFit = yBins - out.residuals;
        
        
        
        [fwhm, warningFlag] = fwhm_Hz(yBinsFit, ppmStart, ppmEnd, length(yBinsFit), scanFrequencyMock, interpFactor)
        [~, ppmCenterIndex] = max(yBinsFit);
        ppmCenter = xBins(ppmCenterIndex);
        
        save([pathAminoAcids currentFile '.mat'], 'yBins', 'yBinsFit', 'xBins', 'fwhm', 'ppmCenter');
        tableFWHM_infos{indexAminoAcid}{indexProtonResonance} = {currentAminoAcid, currentResonance, fwhm, ppmCenter};
        
        yBinsTotal = yBinsTotal + yBins;
        yBinsFitTotal = yBinsFitTotal +  yBinsFit;
        
        indexVisible = find(strcmp(currentResonance, metabolite_visible_resonances{indexAminoAcid}), 1);
        if ~isempty(indexVisible)
            yBinsTotalVisible = yBinsTotalVisible + yBins;
            yBinsFitTotalVisible = yBinsFitTotalVisible + yBinsFit;
        end
        
        
        if useSimpleBins
            %
            figure(101)
            %                 hold on
            %                 bar(xBins, yBins);
            %                 hold on
            %                 bar(xBins, yBinsFit);
            %                 hold on
            %                 plot(myFit)
            hold on
            h = area(xBins, [zeros(size(yBinsFit)) yBinsFit]);
            h(1).FaceColor = 'none';
            h(2).FaceColor = colors{indexProtonResonance};
            h(2).FaceAlpha = alphas{indexProtonResonance};
            h(1).EdgeColor = 'none';
            h(2).EdgeColor = [0 0 0];%[colors{indexProtonResonance};
            set(gca,'YLim',[0,max(yBinsFit) * 1.1])
            set(gca,'XLim',[0, 10])
            xlabel('ppm')
            ylabel('Bin Count')
            set(gca,'xDir','reverse')
            set(gca,'LineWidth',1);
        end
    end
        
    fidFits = ifft(ifftshift(yBinsFitTotal));
    fidFitsVisible = ifft(ifftshift(yBinsFitTotalVisible));
    
    figure
    plot(xBins, imag(fftshift(fft(fidFitsVisible))))
    figure
    plot(xBins, imag(fftshift(fft(fidFits))))
    
    if ~useSimpleBins
        ExportLCModelBasis(fidFits, dwellTimeMs, scanFrequency, pathAminoAcids, currentAminoAcid, currentAminoAcid, 'Yes');
        ExportLCModelBasis(fidFitsVisible, dwellTimeMs, scanFrequency, pathAminoAcids, [currentAminoAcid '_visible'], currentAminoAcid, 'Yes');
    end
    save([pathAminoAcids currentAminoAcid '.mat'], 'yBinsTotal', 'xBins', 'yBinsFitTotal', 'yBinsFitTotalVisible');
    figAA = figure;
    bar(xBins, yBinsTotal);
    xlabel('ppm')
    title(currentAminoAcid);
    savefig(figAA,[pathAminoAcids currentAminoAcid '.fig']);
    export_fig(figAA,[pathAminoAcids currentAminoAcid '.png'], '-png');
    
    %%
    save([pathAminoAcids currentAminoAcid '_visible.mat'], 'yBinsTotalVisible', 'xBins')
    figAA_visible = figure;
    bar(xBins, yBinsTotalVisible);
    xlabel('ppm')
    title(currentAminoAcid);
    savefig(figAA_visible,[pathAminoAcids currentAminoAcid '_visible.fig']);
    export_fig(figAA_visible,[pathAminoAcids currentAminoAcid '_visible.png'], '-png');
end

save([pathAminoAcids 'tableFWHM_infos.mat'], 'tableFWHM_infos');

end

function [ppm] = getPpmVector(scanFrequency, bandwidth_Hz, dataSize)
   bw     = linspace(-bandwidth_Hz/2,bandwidth_Hz/2,dataSize);
   ppm    = bw/(scanFrequency*(1e-6))+4.7;
end
