function [concentrations, subjects, metaboliteNames]= extractConcentrations(tableConcentrationsSubjects, TE_step)

if ~exist('TE_step', 'var')
    TE_step = 1;
end

numOfSubjects = length(tableConcentrationsSubjects);
subjects = cell(1,numOfSubjects);
metaboliteNames = tableConcentrationsSubjects{1}(1,2:3:end);
if isempty(metaboliteNames{end})
    metaboliteNames = metaboliteNames(1:end-1);
end
numOfMetabolites = length(metaboliteNames);
concentrations = zeros(numOfSubjects,numOfMetabolites);
for indexSubject = 1:numOfSubjects
    subjects{indexSubject} = strrep(tableConcentrationsSubjects{indexSubject}{2,1},'_TE24', '');
    concentrations(indexSubject,:) = cell2mat(tableConcentrationsSubjects{indexSubject}(1+TE_step,2:3:1+3*numOfMetabolites));
end

end