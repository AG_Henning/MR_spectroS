function fittingLCModel(LCModelCallerInstance, controlFiles, outputFiles, ...
    controlFilesPathRemote, controlFilesPathLocal, preprocessedFilesPathLocal, copyFilesLocally)
% Starts an LCModel call for fitting on the remote computer:
% 1. copies the "controlFiles" to the "controlFilesPathRemote" path from
% "controlFilesPathLocal"
% 2. if "preprocessedFilesPathLocal" is given, and not empty, copies from the local to the
% remote computer:
%    i.  The .RAW spectrum file
%    ii. The water reference file (if "dows=T" in the control file) 
% 3. Calls the LCModel fitting on the remote computer
% 4. Copies resulting output files "outputFiles": .coord, .table, .ps and
%    .csv back to the local computer
% copies the 
if nargin < 3
    error('Too few arguments were passed. The arguments are: LCModelCallerInstance, controlFiles, outputFiles, controlFilesPathRemote, controlFilesPathLocal');
end

if length(controlFiles) ~= length(outputFiles)
    error('The number of the LCModel control Files and output files supplied as parameters have to have the same length');
end

if exist('preprocessedFilesPathLocal', 'var')
    if ~isempty(preprocessedFilesPathLocal)
        copyRawFilesToRemote = true;
    else
        copyRawFilesToRemote = false;
    end
else
    copyRawFilesToRemote = false;
end

%% configuration of connection
if ~exist('copyFilesLocally', 'var')
    copyFilesLocally = true;
else
    
end

%% file paths setup
if ~exist('controlFilesPathRemote', 'var')
    controlFilesPathRemote = '/Desktop/MM/LCModelConfig/';
end
if ~exist('controlFilesPathLocal', 'var')
    controlFilesPathLocal =  'D:/Software/Spectro Data/MM/LCModelConfig/';
end

%% do the fitting for the individual files
for index = 1:length(controlFiles)
    currentControlFile = controlFiles{index};
    currentOutputFile = outputFiles{index};
    % copy the control file to the LCModel computer
    LCModelCallerInstance.CopyDataToRemote(controlFilesPathLocal, controlFilesPathRemote, currentControlFile);
    
    if copyRawFilesToRemote
        preprocessedFilesPathRemote = '';
        currentPreprocessedFile = '';
        doWaterScaling = false;
        controlFileId = fopen([controlFilesPathLocal currentControlFile]);
        while(~feof(controlFileId))
            s = fgetl(controlFileId);
            %look for spectrum file in control file
            if strfind(s, 'FILRAW')
                indices = strfind(s, '''');
                fullPathRawFile = s(indices(1)+1:indices(2)-1);
                [pathRawFile, rawFile, rawFileExt] = fileparts(fullPathRawFile);
                pathRawFile = strrep(pathRawFile, ['/kyb/agks/' LCModelCallerInstance.LCModelUser], '');
                preprocessedFilesPathRemote = [pathRawFile '/'];
                currentPreprocessedFile = [rawFile, rawFileExt];
            end
            %look for water reference in control file
            if strfind(s, 'FILH2O')
                indices = strfind(s, '''');
                fullPathH2OFile = s(indices(1)+1:indices(2)-1);
                [pathH2OFile, waterRefFile, waterRefFileExt] = fileparts(fullPathH2OFile);
                pathH2OFile = strrep(pathH2OFile, ['/kyb/agks/' LCModelCallerInstance.LCModelUser], '');
                preprocessedWaterRefPathRemote = [pathH2OFile '/'];
                currentWaterRefFile = [waterRefFile, waterRefFileExt];
            end
            %check if we actually should do water scaling / FILH2O is needed
            if strfind(s, 'dows')
                if strfind(s,'T')
                    doWaterScaling = true;
                end
            end
        end
        fclose(controlFileId);
        if strcmp(preprocessedFilesPathRemote,'') || strcmp(currentPreprocessedFile,'')
            error('The control file %s does not contain a correct FILRAW path.', [controlFilesPathLocal currentControlFile])
        end
        % copy the .RAW LCModel file to the LCModel computer
        LCModelCallerInstance.CopyDataToRemote(preprocessedFilesPathLocal, preprocessedFilesPathRemote, currentPreprocessedFile);
        
        if doWaterScaling
            if strcmp(preprocessedWaterRefPathRemote,'') || strcmp(currentWaterRefFile,'')
                error('The control file %s does not contain a correct FILH2O path.', [controlFilesPathLocal currentControlFile])
            end
            % copy the .RAW LCModel water reference file to the LCModel computer
            LCModelCallerInstance.CopyDataToRemote(preprocessedFilesPathLocal, preprocessedWaterRefPathRemote, currentWaterRefFile);
        end

    end
    % do the fitting
    LCModelCallerInstance = LCModelCallerInstance.FitSpectrum(currentControlFile, currentOutputFile, copyFilesLocally);
end

end