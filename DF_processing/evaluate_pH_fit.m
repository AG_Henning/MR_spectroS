function evaluate_pH_fit(tableConcentrations)
use_hist = false;
if use_hist
    outputFolder = 'Output\pH_sweep_hCs_hist_shift_fixed_all_TE\';
else
    outputFolder = 'Output\pH_sweep_final_settings\';
%     outputFolder = 'Output\pH_sweep_hCs_shift_fixed\';
end
pathName = 'DF data path';
sampleCriteria = {outputFolder};
data_path = pathToDataFolder(pathName, sampleCriteria);
pH_values = [6.9:0.01:7.15]';
doSubjects = true;

if doSubjects
    subjects = {'1658';'1717';'2017';'3373';'3490';'5771';'6249';'6971'; '7338'; '7782';'9810'};
%     subjects = {'3373'};
else
    subjects = {'Summed'};
end

echoTimes = {'24', '32', '40', '52', '60'};
% echoTimes = {'40'};

if use_hist
    metabolites = {'ATP','hCs', 'NAA_DF', 'NAD', 'DF58', 'DF60', 'DF68', 'DF73', 'DF75', 'DF82', 'NAAB', 'hist'}; %'DF83', 'DF835', 
else
    metabolites = {'ATP','hCs', 'NAA_DF', 'NAD', 'DF58', 'DF60', 'DF68', 'DF73', 'DF75', 'DF82', 'NAAB'}; %'DF83', 'DF835', 
end
numberOfTEs = length(echoTimes);
numberOfSubjects = length(subjects);
close all

if use_hist
    labels = {'hCs conc', 'hCs crlb', 'hist conc', 'hist crlb', 'hCs+hist conc', 'voting'};
else
    labels = {'hCs conc', 'hCs crlb', 'voting'};
end
categories = length(labels);
pH_subjects_table = cell(numberOfSubjects+2, categories * numberOfTEs);
pH_subjects_table(1,1) = {'Calculated pH value/Subject:'};
       
for indexTE = 1:numberOfTEs
    offset = (indexTE-1) * categories+2;
    pH_subjects_table(1,offset) = echoTimes(indexTE);
    pH_subjects_table(2,offset:offset+categories-1) = labels;
end

chosen_pH_valuesIndex = (find(strcmp(echoTimes, '40'))-1)*categories + 2;
pH_subjects_table(3:numberOfSubjects+2,1) = subjects;
chosenT2s =  zeros(numberOfSubjects, length(metabolites));
chosenR2s =  zeros(numberOfSubjects, length(metabolites));
for indexSubject = 1:numberOfSubjects
    if doSubjects
        load([data_path outputFolder subjects{indexSubject} '/tableConcentrations_' subjects{indexSubject} '.mat']);
    else
        load([data_path outputFolder 'tableConcentrationsSummed.mat']);
        tableConcentrations = tableConcentrationsSummed;
    end
    
    
    numberOfpH = length(tableConcentrations);
    sampleTable = tableConcentrations{1};
    metaboliteNames = sampleTable(1,2:3:end-2);
    sampleCrlbs = cell2mat(sampleTable(2:1+numberOfTEs,3:3:end-2));
    crlbs = zeros([numberOfpH, size(sampleCrlbs)]);
    concentrations = zeros([numberOfpH, size(sampleCrlbs)]);
    T2s = zeros([numberOfpH, size(sampleCrlbs,2)]);
    R2s = zeros([numberOfpH, size(sampleCrlbs,2)]);
    
    for index_pH = 1 : numberOfpH
        currentTable = tableConcentrations{indexSubject, index_pH};
        
        crlbs(index_pH,:,:) = cell2mat(currentTable(2:1+numberOfTEs,3:3:end-2));
        concentrations(index_pH,:,:) = cell2mat(currentTable(2:1+numberOfTEs,2:3:end-2));
        if numberOfTEs > 1
            T2s(index_pH,:) = cell2mat(currentTable(3+numberOfTEs,2:3:end-2));
            R2s(index_pH,:) = cell2mat(currentTable(4+numberOfTEs,2:3:end-2));
        end
    end
    
    titleSuffix = subjects{indexSubject};
    
    labels = {};
    labels2 = {};
    labels3 = {};
    index_hCs = find(strcmp(metaboliteNames, 'hCs'));
    labels{end+1} = 'hCs CRLB';
    [figureIDs, crlbs_hCs] = calculate_plot_crlb_met(crlbs, index_hCs, numberOfTEs, pH_values, echoTimes, ...
        labels, titleSuffix);
    
    labels{end+1} = 'hCs Conc';
    [figureIDs, scalingFactors, conc_hCs] = calculate_plot_conc_met(concentrations,...
        index_hCs, numberOfTEs, pH_values, echoTimes, labels, titleSuffix, [], figureIDs);
    
    if use_hist
        index_hist = find(strcmp(metaboliteNames, 'hist'));
        labels{end+1} = 'hist CRLB';
        [figureIDs, crlbs_hist] = calculate_plot_crlb_met(crlbs, index_hist, numberOfTEs, pH_values, echoTimes,...
            labels, titleSuffix, figureIDs);
        
        labels{end+1} = 'hist Conc';
        [figureIDs, scalingFactors, conc_hist] = calculate_plot_conc_met(concentrations,...
            index_hist, numberOfTEs, pH_values, echoTimes, labels, titleSuffix, scalingFactors, figureIDs);
    end
    
    for indexTE = 1:numberOfTEs
        figure(figureIDs{indexTE});
        ylim([0 110]);
    end
    
    for indexTE = 1:numberOfTEs
        
        minIndexCrlb_hCs  = floor(median(find(crlbs_hCs{indexTE} == min(crlbs_hCs{indexTE}))));
        maxIndexConc_hCs  = floor(median(find(conc_hCs{indexTE} == max(conc_hCs{indexTE})))); 
        % for each TE
        offset = (indexTE-1) * categories;
        pH_subjects_table{indexSubject+2,offset+2} = pH_values(maxIndexConc_hCs);
        pH_subjects_table{indexSubject+2,offset+3} = pH_values(minIndexCrlb_hCs);
        if use_hist
            maxIndexConc_hist = floor(median(find(conc_hist{indexTE} == max(conc_hist{indexTE}))));
            minIndexCrlb_hist = floor(median(find(crlbs_hist{indexTE} == min(crlbs_hist{indexTE}))));
            maxIndexConc_Im = floor(median(find((conc_hCs{indexTE} + conc_hist{indexTE}) == max(conc_hCs{indexTE} + conc_hist{indexTE}))));
            pH_subjects_table{indexSubject+2,offset+4} = pH_values(maxIndexConc_hist);
            pH_subjects_table{indexSubject+2,offset+5} = pH_values(minIndexCrlb_hist);
            pH_subjects_table{indexSubject+2,offset+6} = pH_values(maxIndexConc_Im);
        end
    end
    labels2{end+1} = 'hCs T2';
    figureID2 = calculate_plot_T2_met(T2s, index_hCs, pH_values, labels2);
    if use_hist
        labels2{end+1} = 'hist T2';
        figureID2 = calculate_plot_T2_met(T2s, index_hist, pH_values, labels2, figureID2);
    end
    title(['T_2 ' subjects{indexSubject}])
    
    labels3{end+1} = 'hCs R2';
    figureID3 = calculate_plot_T2_met(R2s, index_hCs, pH_values, labels3);
    if use_hist
        labels3{end+1} = 'hist R2';
        figureID3 = calculate_plot_T2_met(R2s, index_hist, pH_values, labels3, figureID3);
    end
    figure(figureID3);
    ylim([0 1.1]);
    title(['R^2 ' subjects{indexSubject}])

    chosen_pH_value = cell2mat(pH_subjects_table(indexSubject+2,chosen_pH_valuesIndex));
    indexCurrent_pH = find(pH_values == chosen_pH_value);
%     for indexMet = 1:length(metabolites)
%         indexMetCurrent = find(strcmp(metaboliteNames, metabolites{indexMet}));
%         chosenT2s(indexSubject,indexMet) = T2s(indexCurrent_pH, indexMetCurrent);
%         chosenR2s(indexSubject,indexMet) = R2s(indexCurrent_pH, indexMetCurrent);
% %         concentrations(indexCurrent_pH,:,indexMetCurrent)
%     end
end
% close all
exportFileXlsx = [data_path , outputFolder, 'pH_Results_Subjects.xlsx'];
xlswrite(exportFileXlsx, pH_subjects_table, 1, 'A1');

chosenT2s 
chosenR2s
end

%%
function [figureIDs, crlbsMet] = calculate_plot_crlb_met(crlbs, index_met, numberOfTEs, pHValues, ...
    echoTimes, labels, titleSuffix, figureIDs)
if exist('figureIDs', 'var')
    createNewFigures = true;
else
    createNewFigures = false;
end

crlbsMet = cell(numberOfTEs,1);
for indexTE = 1:numberOfTEs
    crlbsMet{indexTE} = crlbs(:,indexTE,index_met);
    if createNewFigures
        figureIDs{indexTE} = figure(figureIDs{indexTE});
        hold on
    else
        figureIDs{indexTE} = figure;
    end
    plot(pHValues,crlbsMet{indexTE})
    legend(labels)
    title(['TE ' num2str(echoTimes{indexTE}) ' ms ' titleSuffix])
end
end

%%
function [figureIDs, scalingFactors, concMet] = calculate_plot_conc_met(concentrations,...
    index_met, numberOfTEs, pHValues, echoTimes, labels, titleSuffix, scalingFactors, figureIDs)
if exist('figureIDs', 'var')
    createNewFigures = true;
else
    createNewFigures = false;
end
calculateScalingFactors = true;
if exist('scalingFactors', 'var')
    if ~isempty(scalingFactors)
        calculateScalingFactors = false;
    end
end

concMet = cell(numberOfTEs,1);
for indexTE = 1:numberOfTEs
    concMet{indexTE} = concentrations(:,indexTE,index_met);
    if calculateScalingFactors
        scalingFactors{indexTE} = max(concMet{indexTE})/100;
    end
    concMet{indexTE} = concMet{indexTE} / scalingFactors{indexTE};
    
    if createNewFigures
        figureIDs{indexTE} = figure(figureIDs{indexTE});
        hold on
    else
        figureIDs{indexTE} = figure;
    end
    plot(pHValues,concMet{indexTE})
    legend(labels)
    title(['TE ' num2str(echoTimes{indexTE}) ' ms ' titleSuffix])
end
end

%%

function figureID = calculate_plot_T2_met(T2_R2s, index_met, pHValues, labels, figureID)
if exist('figureID', 'var')
    figureID = figure(figureID);
    hold on
else
    figureID = figure;
end

T2_R2Met = T2_R2s(:,index_met);
plot(pHValues,T2_R2Met)
legend(labels)
end