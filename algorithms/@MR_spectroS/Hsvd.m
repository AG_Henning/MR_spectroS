function [thisFittedData, this] = Hsvd (this)


 p              = this.Parameter.HsvdSettings.p;
 n              = this.Parameter.HsvdSettings.n;
 bound          = this.Parameter.HsvdSettings.bound;
 bandwidth      = this.Parameter.Headers.Bandwidth_Hz;
 
 
 %normalized frequencies
 bound = bound/bandwidth;
 
 if this.Parameter.ReconFlags.isHsvdFiltered
     warning(' HSVD has already be applied on the data!!! ' )
 end
 
 thisFittedData     = this;
 oldData            = this.Data{1};
 dataFitted         = zeros(size(this.Data{1}));  
 dataSuppressed     = oldData;%zeros(size(this.Data{1}));
 
 
              %NCol=  size( data, this.kx_dim);   % # timepoints
              NMeas        = size( oldData, this.meas_dim); % # Averages
              NCha         = size( oldData, this.coil_dim); % # Channels
              NMix         = size( oldData, this.mix_dim);  % # Mixes
             
              NMix        = 1;
               for ncha=1:NCha
                   %fprintf('Coil #%d   \n',ncha);
                    for nmeas=1:NMeas
                        for nmix=1:NMix
                            tmpData                             = oldData(:,1,1,ncha,1,1,1,1,nmix,1,1,nmeas) ;
                            [~,~,~,~,~,~,tmpDataFitted, ~ ,~,~] = hsvd(tmpData',p,n,bound,0,0);
                                                              
                              dataFitted(:,1,1,ncha,1,1,1,1,nmix,1,1,nmeas)     = tmpDataFitted;
                              dataSuppressed(:,1,1,ncha,1,1,1,1,nmix,1,1,nmeas) = tmpData - tmpDataFitted';
                            
                        end
                    end
               end
               
               
    thisFittedData.Data{1}     = dataFitted;          
    this.Data{1}               = dataSuppressed;
    
    thisFittedData.Parameter.ReconFlags.isHsvdFiltered  = true;
    this.Parameter.ReconFlags.isHsvdFiltered            = true;
end
