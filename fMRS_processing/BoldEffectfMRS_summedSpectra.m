function BoldEffectfMRS_summedSpectra()
clear; clc; close all;

pathName = 'fMRS TimeCourse data path';
[localFilePathBase] = pathToDataFolder(pathName);
visualType = 'Stim';      %'Stim', 'Rest'
BOLDType = '_woBOLDCorr';
nBlocks = 10;
BlockNo = cell(1,nBlocks);
for k = 1:nBlocks
    BlockNo{k} = strcat('_Block', num2str(k));
end
woBaseline = false;  %false, false

subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
    '4085';'3333';'4012';'4085';'6524'};
% prefixes = {'2020-02-18_';};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
    '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
    '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
suffixFolder = '_fMRS';
excludeSubjects = {'5166';'9810';'5269'};       %doesn't work for not excludeSubjects

if exist('excludeSubjects')
    n = length(subjects);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(subjects(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    subjects([excludeIdx],:) = [];
    prefixes([excludeIdx],:) = [];
end

numberOfSubjects = length(subjects);
defaultSubject = 'XXXX';
subjectsPath = strcat(prefixes, subjects, suffixFolder);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath, '/');
OutputFilesPathLocal = strcat(localFilePathBase, 'Output_v2_MMBSummed/BOLD/');
outputFileNameBase = strcat(defaultSubject, '_', num2str(visualType), BOLDType);
extensionMat   = '.mat';

%% sum fMRS spectra of all volunteers, weighted with water fit (need to run doFitting_fMRS_Water.m first)
WaterReferenceTable = (readtable(strcat(OutputFilesPathLocal,'WaterReference_Fit.xlsx')));
WaterReferenceID = table2cell(WaterReferenceTable(2:end,1));
WaterReference = str2double(table2array(WaterReferenceTable(2:end,2:end)));
fMRSDataScaled = zeros(4096,numberOfSubjects,2,length(BlockNo));    %samples, subjects, data/water
for indexCurrentSubject = 1:numberOfSubjects
    for indexBlock = 1:length(BlockNo)
        idx = find(ismember(WaterReferenceID, strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject})));
        currentScalingFactor = WaterReference(idx,1);
        currentfMRSDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject});
        load(strrep(strcat(currentfMRSDataPath, '\',outputFileNameBase,BlockNo{indexBlock},extensionMat),defaultSubject,subjects{indexCurrentSubject}));
        currentfMRSData = squeeze(this.Data{1});
        currentfMRSDataScaled = currentfMRSData./currentScalingFactor;
        fMRSDataScaled(:,indexCurrentSubject,:,indexBlock) = currentfMRSDataScaled;
    end
end
cd ('D:\PAPER\1H_fMRS\Data_TimeCourse\SummedData')
fMRSData_Summed_Path = strcat(localFilePathBase,'SummedData\');
this.Parameter.Filepath = fMRSData_Summed_Path;
fMRSData_Summed = squeeze(sum(fMRSDataScaled,2));
this.Parameter.Filename = 'fMRSData_Summed_Block1_v2_MMBSummed_Excluded'; 
fMRSData_Summed_Block1 = fMRSData_Summed(:,:,1); fMRSData_Summed_Block1 = permute(fMRSData_Summed_Block1,[1 3 4 5 6 7 8 9 2]);
% this.Data{1} = fMRSData_Summed_Block1; save('fMRSData_Summed_Block1','this'); this.ExportLcmRaw(fMRSData_Summed_Path,'fMRSData_Summed_Block1','Yes', 'Yes');
this.Data{1} = fMRSData_Summed_Block1; %save(strcat('fMRSData_Summed_',visualType,BOLDType,'_Block1'),'this'); this.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLDType,'_Block1'),'Yes', 'Yes');
this.Parameter.Filename = 'fMRSData_Summed_Block2_v2_MMBSummed_Excluded'; 
fMRSData_Summed_Block2 = fMRSData_Summed(:,:,2); fMRSData_Summed_Block2 = permute(fMRSData_Summed_Block2,[1 3 4 5 6 7 8 9 2]);
this.Data{1} = fMRSData_Summed_Block2; %save(strcat('fMRSData_Summed_',visualType,BOLDType,'_Block2'),'this'); this.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLDType,'_Block2'),'Yes', 'Yes');
this.Parameter.Filename = 'fMRSData_Summed_Block3_v2_MMBSummed_Excluded'; 
fMRSData_Summed_Block3 = fMRSData_Summed(:,:,3); fMRSData_Summed_Block3 = permute(fMRSData_Summed_Block3,[1 3 4 5 6 7 8 9 2]);
this.Data{1} = fMRSData_Summed_Block3; %save(strcat('fMRSData_Summed_',visualType,BOLDType,'_Block3'),'this'); this.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLDType,'_Block3'),'Yes', 'Yes');
this.Parameter.Filename = 'fMRSData_Summed_Block4_v2_MMBSummed_Excluded'; 
fMRSData_Summed_Block4 = fMRSData_Summed(:,:,4); fMRSData_Summed_Block4 = permute(fMRSData_Summed_Block4,[1 3 4 5 6 7 8 9 2]);
this.Data{1} = fMRSData_Summed_Block4; %save(strcat('fMRSData_Summed_',visualType,BOLDType,'_Block4'),'this'); this.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLDType,'_Block4'),'Yes', 'Yes');
this.Parameter.Filename = 'fMRSData_Summed_Block5_v2_MMBSummed_Excluded'; 
fMRSData_Summed_Block5 = fMRSData_Summed(:,:,5); fMRSData_Summed_Block5 = permute(fMRSData_Summed_Block5,[1 3 4 5 6 7 8 9 2]);
this.Data{1} = fMRSData_Summed_Block5; %save(strcat('fMRSData_Summed_',visualType,BOLDType,'_Block5'),'this'); this.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLDType,'_Block5'),'Yes', 'Yes');
this.Parameter.Filename = 'fMRSData_Summed_Block6_v2_MMBSummed_Excluded'; 
fMRSData_Summed_Block6 = fMRSData_Summed(:,:,6); fMRSData_Summed_Block6 = permute(fMRSData_Summed_Block6,[1 3 4 5 6 7 8 9 2]);
this.Data{1} = fMRSData_Summed_Block6; %save(strcat('fMRSData_Summed_',visualType,BOLDType,'_Block6'),'this'); this.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLDType,'_Block6'),'Yes', 'Yes');
this.Parameter.Filename = 'fMRSData_Summed_Block7_v2_MMBSummed_Excluded'; 
fMRSData_Summed_Block7 = fMRSData_Summed(:,:,7); fMRSData_Summed_Block7 = permute(fMRSData_Summed_Block7,[1 3 4 5 6 7 8 9 2]);
this.Data{1} = fMRSData_Summed_Block7; %save(strcat('fMRSData_Summed_',visualType,BOLDType,'_Block7'),'this'); this.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLDType,'_Block7'),'Yes', 'Yes');
this.Parameter.Filename = 'fMRSData_Summed_Block8_v2_MMBSummed_Excluded'; 
fMRSData_Summed_Block8 = fMRSData_Summed(:,:,8); fMRSData_Summed_Block8 = permute(fMRSData_Summed_Block8,[1 3 4 5 6 7 8 9 2]);
this.Data{1} = fMRSData_Summed_Block8; %save(strcat('fMRSData_Summed_',visualType,BOLDType,'_Block8'),'this'); this.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLDType,'_Block8'),'Yes', 'Yes');
this.Parameter.Filename = 'fMRSData_Summed_Block9_v2_MMBSummed_Excluded'; 
fMRSData_Summed_Block9 = fMRSData_Summed(:,:,9); fMRSData_Summed_Block9 = permute(fMRSData_Summed_Block9,[1 3 4 5 6 7 8 9 2]);
this.Data{1} = fMRSData_Summed_Block9; %save(strcat('fMRSData_Summed_',visualType,BOLDType,'_Block9'),'this'); this.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLDType,'_Block9'),'Yes', 'Yes');
this.Parameter.Filename = 'fMRSData_Summed_Block10_v2_MMBSummed_Excluded'; 
fMRSData_Summed_Block10 = fMRSData_Summed(:,:,10); fMRSData_Summed_Block10 = permute(fMRSData_Summed_Block10,[1 3 4 5 6 7 8 9 2]);
this.Data{1} = fMRSData_Summed_Block10; %save(strcat('fMRSData_Summed_',visualType,BOLDType,'_Block10'),'this'); this.ExportLcmRaw(fMRSData_Summed_Path,strcat('fMRSData_Summed_',visualType,BOLDType,'_Block10'),'Yes', 'Yes');

%% BOLD FWHM / Amplitude
% % for Water (not useful w/o baseline, since it doesn't have a baseline)
% PeakRange = [4 5];  %ppm where to find the peak
% metabType = 'Water';
% if exist('excludeSubjects')
    CalcBOLDEffect(BlockNo,visualType,BOLDType,localFilePathBase,...
        OutputFilesPathLocal,nBlocks,PeakRange,metabType,'')
% else
%     CalcBOLDEffect(BlockNo,visualType,BOLDType,localFilePathBase,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType)
% end
% 
% %for NAA (2.008 ppm)
% PeakRange = [1.8 2.3];  %ppm where to find the peak
% metabType = 'NAA';
% if exist('excludeSubjects')
%     CalcBOLDEffect(BlockNo,visualType,BOLDType,localFilePathBase,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
% else
%     CalcBOLDEffect(BlockNo,visualType,BOLDType,localFilePathBase,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
% end
% 
% %for Cr (methyl signal of tCr: 3.027ppm)
% PeakRange = [2.9 3.15];  %ppm where to find the peak
% metabType = 'Cr';
% if exist('excludeSubjects')
%     CalcBOLDEffect(BlockNo,visualType,BOLDType,localFilePathBase,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
% else
%     CalcBOLDEffect(BlockNo,visualType,BOLDType,localFilePathBase,...
%         OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
% end
% 
% % correlation FWHM, Amplitude
metabType = {'NAA', 'Cr'};
metabType = {'Water', 'NAA'};
metabType = {'Water', 'Cr'};
% metabType = {'Water'};
if exist('excludeSubjects')
    CorrBOLDSpearman(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects)
else
    CorrBOLDSpearman(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline)
end
% % % % didn't check this for summed data
% % % % CorrBOLDCategorical(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline)

%% correlation Water, Metabolite Concentration
% metabType = {'Glu'};
if exist('excludeSubjects')
    CorrBOLDSpearman_MetConc({'Cr'},visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects); close all
    CorrBOLDSpearman_MetConc({'PCr'},visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects); close all
    CorrBOLDSpearman_MetConc({'Lac'},visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects); close all
    CorrBOLDSpearman_MetConc({'Glu'},visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects); close all
else
    CorrBOLDSpearman_MetConc({'Cr'},visualType,BOLDType,OutputFilesPathLocal,woBaseline)
    CorrBOLDSpearman_MetConc({'PCr'},visualType,BOLDType,OutputFilesPathLocal,woBaseline)
%     CorrBOLDSpearman_MetConc({'Lac'},visualType,BOLDType,OutputFilesPathLocal,woBaseline)
%     CorrBOLDSpearman_MetConc({'Glu'},visualType,BOLDType,OutputFilesPathLocal,woBaseline)
end

end

function CorrBOLDSpearman_MetConc(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end

if exist('excludeSubjects')
    impName = strcat('BOLD_Water_',visualType,BOLDType,'_Excluded_SummedData');
else
    impName = strcat('BOLD_Water_',visualType,BOLDType,'_SummedData');
end
impFileMat = strcat(OutputFilesPathLocal,impName,'.mat');
BOLDData = load(impFileMat);
fields = fieldnames(BOLDData);
BOLDDataXY(1) = BOLDData.(fields{1});

%import Met Conc
impNameMetConc = strcat('BlockConc_Summed_Stim_woBOLDCorr_MMBSummed_v2',woBaseline);
impFileMatMetConc = strcat(OutputFilesPathLocal(1:end-5),impNameMetConc,'.mat');
BOLDData = load(impFileMatMetConc);
fields = fieldnames(BOLDData);
BOLDData_temp = BOLDData.(fields{1});
indexMet = strcmp(BOLDData_temp(1,:),metabType);
idxMet = find(indexMet);
for i = 1:10
    BOLDData_Met(i) = BOLDData_temp{i+1,idxMet};    %Met conc per block
end

if exist('excludeSubjects')
    file2save = strcat('Corr_Water_',metabType{1},'_Conc_Summed_',visualType,BOLDType,woBaseline,'_Excluded');
else
    file2save = strcat('Corr_Water_',metabType{1},'_Conc_Summed_',visualType,BOLDType,woBaseline);
end

% subjLength = length(BOLDDataXY(1).fwhm);
subjLength = 1;
subjects = ones(subjLength*10,1);
for i = 1:subjLength
    subjects((1+(i-1)*10):(10+(i-1)*10)) = subjects((1+(i-1)*10):(10+(i-1)*10)).*i;
end

%for FWHM (Water Hz vs Met a.u.)
fwhmTab = table(BOLDDataXY(1).fwhm(:),BOLDData_Met',subjects);
BOLDData_FWHM = table2array(fwhmTab);
%Spearman correlation
[RHO,PVAL] = corr(BOLDData_FWHM(:,2),BOLDData_FWHM(:,1),'Type','Spearman');
% [RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Pearson');
%linear regression
fit = fitlm(BOLDData_FWHM(:,2),BOLDData_FWHM(:,1));
figure
hold on
% plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
plot(fit); box on
xlabel(['Concentration ' , char(metabType), ' / a.u.'])
ylabel(['FWHM Water / Hz'])
title(['Correlation FWHM Water & concentration ', char(metabType), woBaseline], 'Interpreter', 'none')
txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
text(max(BOLDData_FWHM(:,2))-0.2,max(BOLDData_FWHM(:,1)),txt);
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');

fig2saveFWHM = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhmConc.fig'));
% savefig(fig2saveFWHM)

%for FWHM (Delta Water Hz vs Delta Met a.u.)
subjectsDelta = subjects(1:9);
fwhmTabDelta = table(diff(BOLDDataXY(1).fwhm(:)),diff(BOLDData_Met'),subjectsDelta);
BOLDData_FWHM_Delta = table2array(fwhmTabDelta);
%Spearman correlation
[RHODelta,PVALDelta] = corr(BOLDData_FWHM_Delta(:,2),BOLDData_FWHM_Delta(:,1),'Type','Spearman');
% [RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Pearson');
%linear regression
fitDelta = fitlm(BOLDData_FWHM_Delta(:,2),BOLDData_FWHM_Delta(:,1));
figure
hold on
% plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
plot(fitDelta); box on
xlabel([char(916), ' Concentration ' , char(metabType), ' / a.u.'])
ylabel([char(916), ' FWHM Water / Hz'])
title(['Correlation ', char(916), 'FWHM Water & concentration ', char(metabType), woBaseline], 'Interpreter', 'none')
txt = {['R = ' num2str(RHODelta)],['p < ' num2str(PVALDelta)]};
text(max(BOLDData_FWHM_Delta(:,2))-0.2,max(BOLDData_FWHM_Delta(:,1)),txt);
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');

fig2saveFWHMDelta = fullfile(OutputFilesPathLocal, strcat(file2save,'_DeltafwhmConc.fig'));
% savefig(fig2saveFWHMDelta)

%for amplitude (Water a.u. vs Met a.u.)
ampTab = table(BOLDDataXY(1).amplitude(:),BOLDData_Met',subjects);
BOLDData_amp = table2array(ampTab);
%Spearman correlation
[RHO,PVAL] = corr(BOLDData_amp(:,2),BOLDData_amp(:,1),'Type','Spearman');
%linear regression
fit = fitlm(BOLDData_amp(:,2),BOLDData_amp(:,1));
figure
hold on
% plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
plot(fit)
ylim([1430 1500]); box on
xlabel(['Concentration ' , char(metabType), ' / a.u.'])
ylabel(['Amplitude Water / a.u.'])
title(['Correlation Amplitude Water & concentration ', char(metabType), woBaseline], 'Interpreter', 'none')
txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
text((max(BOLDData_amp(:,2))-max(BOLDData_amp(:,2))*0.06),(min(BOLDData_amp(:,1))+min(BOLDData_amp(:,1))*0.01),txt);
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
lines = findobj(gcf,'Type','Line');
for i = 1:numel(lines)
lines(i).LineWidth = 1.5;
end
fig2saveAmplitude = fullfile(OutputFilesPathLocal, strcat(file2save,'_ampConc.fig'));
savefig(fig2saveAmplitude)

%for amplitude (Delta Water a.u. vs Delta Met a.u.)
ampTabDelta = table(diff(BOLDDataXY(1).amplitude(:)),diff(BOLDData_Met'),subjectsDelta);
BOLDData_amp_Delta = table2array(ampTabDelta);
%Spearman correlation
[RHODelta,PVALDelta] = corr(BOLDData_amp_Delta(:,2),BOLDData_amp_Delta(:,1),'Type','Spearman');
%linear regression
fitDelta = fitlm(BOLDData_amp_Delta(:,2),BOLDData_amp_Delta(:,1));
figure
hold on
% plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
plot(fitDelta); box on
xlabel([char(916), ' Concentration ' , char(metabType), ' / a.u.'])
ylabel([char(916), ' Amplitude Water / a.u.'])
title(['Correlation ', char(916), 'Amplitude Water & concentration ', char(metabType), woBaseline], 'Interpreter', 'none')
txt = {['R = ' num2str(RHODelta)],['p < ' num2str(PVALDelta)]};
text((max(BOLDData_amp_Delta(:,2))-max(BOLDData_amp_Delta(:,2))*0.2),(min(BOLDData_amp_Delta(:,1))+min(BOLDData_amp_Delta(:,1))*0.01),txt);
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
fig2saveAmplitudeDelta = fullfile(OutputFilesPathLocal, strcat(file2save,'_DeltaampConc.fig'));
% savefig(fig2saveAmplitudeDelta)
end

function CorrBOLDSpearman(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline,excludeSubjects)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end

for idxmetab = 1:length(metabType)
    if strcmp(metabType{idxmetab}(1:2),'Wa')
        if exist('excludeSubjects')
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,'_Excluded_SummedData');
        else
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,'_SummedData');
        end
    else
        if exist('excludeSubjects')
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,woBaseline,'_Excluded_SummedData');
        else
            impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,woBaseline,'_SummedData');
        end
    end
    impFileMat = strcat(OutputFilesPathLocal,impName,'.mat');
    BOLDData = load(impFileMat);
    fields = fieldnames(BOLDData);
    BOLDDataXY(idxmetab) = BOLDData.(fields{1});
end

if exist('excludeSubjects')
    if length(metabType) == 2
        file2save = strcat('Corr_',metabType{1},'_',metabType{2},'_',visualType,BOLDType,woBaseline,'_Excluded_SummedData');
    elseif length(metabType) == 1
        file2save = strcat('Corr_',metabType{1},'_FWHMAmp_',visualType,BOLDType,woBaseline,'_Excluded_SummedData');
    end
else
    if length(metabType) == 2
        file2save = strcat('Corr_',metabType{1},'_',metabType{2},'_',visualType,BOLDType,woBaseline,'_SummedData');
    elseif length(metabType) == 1
        file2save = strcat('Corr_',metabType{1},'_FWHMAmp_',visualType,BOLDType,woBaseline,'_SummedData');
    end
end

% subjLength = length(BOLDDataXY(1).fwhm);
subjLength = 1;
subjects = ones(subjLength*10,1);
for i = 1:subjLength
    subjects((1+(i-1)*10):(10+(i-1)*10)) = subjects((1+(i-1)*10):(10+(i-1)*10)).*i;
end

if length(metabType) == 2
    %for FWHM (Hz)
    fwhmTab = table(BOLDDataXY(1).fwhm(:),BOLDDataXY(2).fwhm(:),subjects);
    BOLDData_FWHM = table2array(fwhmTab);
    %Spearman correlation
    [RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Spearman');
    % [RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Pearson');
    %linear regression
    fit = fitlm(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2));
    figure
    hold on
    % plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
    plot(fit); box on;
    xlabel(['FWHM ',num2str(metabType{1}), ' / Hz'])
    ylabel(['FWHM ',num2str(metabType{2}), ' / Hz'])
    title(['Correlation FWHM ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
    txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
    text(min(BOLDData_FWHM(:,1)),max(BOLDData_FWHM(:,2)),txt)
    set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
        lines(i).LineWidth = 1.5;
    end
    fig2saveFWHM = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhm.fig'));
%     savefig(fig2saveFWHM)
    
    %for FWHM (Delta Hz)
    subjectsDelta = subjects(1:9);
    fwhmTabDelta = table(diff(BOLDDataXY(1).fwhm(:)),diff(BOLDDataXY(2).fwhm(:)),subjectsDelta);
    BOLDData_FWHM_Delta = table2array(fwhmTabDelta);
    %Spearman correlation
    [RHODelta,PVALDelta] = corr(BOLDData_FWHM_Delta(:,1),BOLDData_FWHM_Delta(:,2),'Type','Spearman');
    %linear regression
    fitDelta = fitlm(BOLDData_FWHM_Delta(:,1),BOLDData_FWHM_Delta(:,2));
    figure
    hold on
    % plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
    plot(fitDelta); box on;
    xlabel([char(916) 'FWHM ',num2str(metabType{1}), ' / Hz'])
    ylabel([char(916) 'FWHM ',num2str(metabType{2}), ' / Hz'])
    title(['Correlation ' char(916) 'FWHM ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
    txt = {['R = ' num2str(RHODelta)],['p < ' num2str(PVALDelta)]};
    text(min(BOLDData_FWHM_Delta(:,1)),max(BOLDData_FWHM_Delta(:,2)),txt)
    set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
        lines(i).LineWidth = 1.5;
    end
    fig2saveFWHMDelta = fullfile(OutputFilesPathLocal, strcat(file2save,'_Deltafwhm.fig'));
%     savefig(fig2saveFWHMDelta)
    
    %for amplitude (a.u.)
    ampTab = table(BOLDDataXY(1).amplitude(:),BOLDDataXY(2).amplitude(:),subjects);
    BOLDData_amp = table2array(ampTab);
    %Spearman correlation
    [RHO,PVAL] = corr(BOLDData_amp(:,1),BOLDData_amp(:,2),'Type','Spearman');
    %linear regression
    fit = fitlm(BOLDData_amp(:,1),BOLDData_amp(:,2));
    figure
    hold on
    % plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
    plot(fit); box on;
    xlabel(['Amplitude ',num2str(metabType{1}), ' / a.u.'])
    ylabel(['Amplitude ',num2str(metabType{2}), ' / a.u.'])
    title(['Correlation Amplitude ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
    txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
    text(min(BOLDData_amp(:,1)),max(BOLDData_amp(:,2)),txt)
    set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
        lines(i).LineWidth = 1.5;
    end
    fig2saveAmplitude = fullfile(OutputFilesPathLocal, strcat(file2save,'_amp.fig'));
%     savefig(fig2saveAmplitude)
    
    %for amplitude (Delta a.u.)
    ampTabDelta = table(diff(BOLDDataXY(1).amplitude(:)),diff(BOLDDataXY(2).amplitude(:)),subjectsDelta);
    BOLDData_amp_Delta = table2array(ampTabDelta);
    %Spearman correlation
    [RHODelta,PVALDelta] = corr(BOLDData_amp_Delta(:,1),BOLDData_amp_Delta(:,2),'Type','Spearman');
    %linear regression
    fitDelta = fitlm(BOLDData_amp_Delta(:,1),BOLDData_amp_Delta(:,2));
    figure
    hold on
    % plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
    plot(fitDelta); box on;
    xlabel([char(916) 'Amplitude ',num2str(metabType{1}), ' / a.u.'])
    ylabel([char(916) 'Amplitude ',num2str(metabType{2}), ' / a.u.'])
    title(['Correlation ' char(916) 'Amplitude ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
    txt = {['R = ' num2str(RHODelta)],['p < ' num2str(PVALDelta)]};
    text(min(BOLDData_amp_Delta(:,1)),max(BOLDData_amp_Delta(:,2)),txt)
    set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
        lines(i).LineWidth = 1.5;
    end
    fig2saveAmplitudeDelta = fullfile(OutputFilesPathLocal, strcat(file2save,'_Deltaamp.fig'));
%     savefig(fig2saveAmplitudeDelta)
    
elseif length(metabType) == 1
    %for FWHM (Hz) / Amp (a.u.)
    fwhmAmpTab = table(BOLDDataXY(1).fwhm(:),BOLDDataXY(1).amplitude(:),subjects);
    BOLDData = table2array(fwhmAmpTab);
    %Spearman correlation
    [RHO,PVAL] = corr(BOLDData(:,1),BOLDData(:,2),'Type','Spearman');
    % [RHO,PVAL] = corr(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'Type','Pearson');
    %linear regression
    fit = fitlm(BOLDData(:,2),BOLDData(:,1));
    figure
    hold on
    plot(fit); box on;
    xlabel(['Amplitude ',num2str(metabType{1}), ' / a.u.'])
    ylabel(['FWHM ',num2str(metabType{1}), ' / Hz'])
    title(['Correlation FWHM & Amplitude ', num2str(metabType{1}), woBaseline], 'Interpreter', 'none')
    txt = {['R = ' num2str(RHO)],['p < ' num2str(PVAL)]};
    text(min(BOLDData(:,2))+min(BOLDData(:,2))*0.001,min(BOLDData(:,1))+min(BOLDData(:,1))*0.001,txt)
    set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
        lines(i).LineWidth = 1.5;
    end
    fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'.fig'));
    savefig(fig2save)
    
    %for FWHM (Delta Hz) / Amp (Delta a.u.)
    subjectsDelta = subjects(1:9);
    fwhmAmpTabDelta = table(diff(BOLDDataXY(1).fwhm(:)),diff(BOLDDataXY(1).amplitude(:)),subjectsDelta);
    BOLDData_Delta = table2array(fwhmAmpTabDelta);
    %Spearman correlation
    [RHODelta,PVALDelta] = corr(BOLDData_Delta(:,1),BOLDData_Delta(:,2),'Type','Spearman');
    %linear regression
    fitDelta = fitlm(BOLDData_Delta(:,2),BOLDData_Delta(:,1));
    figure
    hold on
    % plot(BOLDData_FWHM(:,1),BOLDData_FWHM(:,2),'o')
    plot(fitDelta); box on;
    xlabel([char(916) 'Amplitude ',num2str(metabType{1}), ' / a.u.'])
    ylabel([char(916) 'FWHM ',num2str(metabType{1}), ' / Hz'])
    title(['Correlation ' char(916) 'FWHM & Amplitude ', num2str(metabType{1}), woBaseline], 'Interpreter', 'none')
    txt = {['R = ' num2str(RHODelta)],['p < ' num2str(PVALDelta)]};
    text(min(BOLDData_Delta(:,2)),min(BOLDData_Delta(:,1))+0.05,txt)
    set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]); set(gca, 'FontWeight','bold');
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
        lines(i).LineWidth = 1.5;
    end
    fig2saveDelta = fullfile(OutputFilesPathLocal, strcat(file2save,'_Delta.fig'));
%     savefig(fig2saveDelta)
end
end




function CalcBOLDEffect(BlockNo,visualType,BOLDType,preprocessedFilesPathLocal,...
    OutputFilesPathLocal,nBlocks,PeakRange,metabType,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
    fitVersion = '_v2';
else
    woBaseline = '';
    fitVersion = '';
end
SelProcValues = SelectedProcessingValues();
fwhmArray = zeros(1,length(BlockNo));
amplitudeArray = zeros(1,length(BlockNo));
for nBl = 1:length(BlockNo)
    impName = strcat('fMRSData_Summed_',visualType,BOLDType,fitVersion, BlockNo{nBl},woBaseline);
    impFileMat = strcat(preprocessedFilesPathLocal,'SummedData\',impName,'.mat');
    dataPerBlock = load(impFileMat);
    fields = fieldnames(dataPerBlock);
    dataPerBlock = dataPerBlock.(fields{1});
    SelProcValues.PeakRange = PeakRange;  %ppm
    currentValue = SelProcValues.PeakRange;
    currentValue = convertPPMtoIndex(dataPerBlock, currentValue,1);
    currentValue =[find(currentValue==1,1,'first') find(currentValue==1,1,'last')];
    SelProcValues.PeakRange = [currentValue(1) currentValue(2)];
    bandwidth = dataPerBlock.Parameter.Headers.Bandwidth_Hz;
    datafid = squeeze(dataPerBlock.Data{1});
    peakRange = get(SelProcValues, 'PeakRange');
    
    if strcmp(metabType(1:2),'Wa')
        datafid = datafid(:,2);
        dataSpecReal = real(fftshift(fft(datafid)));
    else
        datafid = datafid(:,1);
        dataSpecReal = real(fftshift(fft(datafid)));
    end
    
    fwhmArray(1,nBl) = linewidth(datafid, SelProcValues, bandwidth);
    amplitudeArray(1,nBl) = max(abs(dataSpecReal(peakRange(1):peakRange(2))));
end

file2save = strcat('BOLD_',metabType,'_',visualType,BOLDType,woBaseline,'_Excluded_SummedData');

BOLD.fwhm = fwhmArray; BOLD.amplitude = amplitudeArray;
eval([file2save '= BOLD']);
exportFileMat = [OutputFilesPathLocal, strcat(file2save,'.mat')];
% save(exportFileMat,file2save)

x = linspace(320/60/4,(320*5/60-320/60/4),10);
% figure
% plot(x,fwhmArray,'o-')
% 
% title ([num2str(file2save),' fwhm'], 'Interpreter', 'none');
% xlabel 't / min';
% yLimits = get(gca,'YLim');
% boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
% for i = 1:nBlocks/2
%     boxoff  = [boxoff 320/60*i 320/60*i];
%     if i < nBlocks/2
%         boxon   = [boxon 320/60*i 320/60*i];
%     end
% end
% for ii = 1:nBlocks/4
%     boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%     boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
% end
% patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
% patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
% ylim([yLimits(1) yLimits(2)]);
% xlim([0 320*5/60]);set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
% set(gca, 'FontWeight','bold');
% fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhm.fig'));
% % savefig(fig2save)

% figure
% plot(x,amplitudeArray./10,'o-')
% 
% title ([num2str(file2save),' amplitude'], 'Interpreter', 'none');
% xlabel 't / min';
% yLimits = get(gca,'YLim');
% boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
% for i = 1:nBlocks/2
%     boxoff  = [boxoff 320/60*i 320/60*i];
%     if i < nBlocks/2
%         boxon   = [boxon 320/60*i 320/60*i];
%     end
% end
% for ii = 1:nBlocks/4
%     boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%     boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
% end
% patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
% patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
% ylim([yLimits(1) yLimits(2)]);
% xlim([0 320*5/60]);set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
% set(gca, 'FontWeight','bold');
% fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_amplitude.fig'));
% % savefig(fig2save)

figure
amplitudeArrayPerc = (amplitudeArray(:)-amplitudeArray(2))./amplitudeArray(2).*100;
plot(x,amplitudeArrayPerc,'o-')

title ([num2str(file2save),' amplitude'], 'Interpreter', 'none');
xlabel 't / min'; ylabel 'Peak height change / %';
yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]);set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
set(gca, 'FontWeight','bold');
fig2save = fullfile(OutputFilesPathLocal, strcat(file2save,'_amplitudePerc.fig'));
% savefig(fig2save)

close all
end

function CorrBOLDCategorical(metabType,visualType,BOLDType,OutputFilesPathLocal,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end

for idxmetab = 1:length(metabType)
    if strcmp(metabType{idxmetab}(1:2),'Wa')
        impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType);
    else
        impName = strcat('BOLD_',metabType{idxmetab},'_',visualType,BOLDType,woBaseline);
    end
    impFileMat = strcat(OutputFilesPathLocal,impName,'.mat');
    BOLDData = load(impFileMat);
    fields = fieldnames(BOLDData);
    BOLDDataXY(idxmetab) = BOLDData.(fields{1});
end
file2save = strcat('Corr_',metabType{1},'_',metabType{2},'_',visualType,BOLDType,woBaseline);
figure
subjects = ones(130,1);
for i = 1:13
    subjects((1+(i-1)*10):(10+(i-1)*10)) = subjects((1+(i-1)*10):(10+(i-1)*10)).*i;
end
fwhmTab = table(BOLDDataXY(1).fwhm(:),BOLDDataXY(2).fwhm(:),subjects);
fwhmTab.subjects = categorical(fwhmTab.subjects);
fit = fitlm(fwhmTab,'Var2~Var1*subjects');
w = linspace(min(BOLDDataXY(1).fwhm(:)),max(BOLDDataXY(1).fwhm(:)));

plot(BOLDDataXY(1).fwhm,BOLDDataXY(2).fwhm,'o')
xlabel(['FWHM ',num2str(metabType{1}), ' / Hz'])
ylabel(['FWHM ',num2str(metabType{2}), ' / Hz'])
title(['Correlation FWHM ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
for i = 1:size(BOLDDataXY(1).fwhm,1)
    line(w,feval(fit,w,num2str(i)))
end
fig2saveFWHM = fullfile(OutputFilesPathLocal, strcat(file2save,'_fwhm.fig'));
savefig(fig2saveFWHM)

figure
fwhmAmp = table(BOLDDataXY(1).amplitude(:),BOLDDataXY(2).amplitude(:),subjects);
fwhmAmp.subjects = categorical(fwhmAmp.subjects);
fit = fitlm(fwhmAmp,'Var2~Var1*subjects');
w = linspace(min(BOLDDataXY(1).amplitude(:)),max(BOLDDataXY(1).amplitude(:)));

plot(BOLDDataXY(1).amplitude,BOLDDataXY(2).amplitude,'o')
xlabel(['Amplitude ',num2str(metabType{1}), ' / a.u.'])
ylabel(['Amplitude ',num2str(metabType{2}), ' / a.u.'])
title(['Correlation Amplitude ', num2str(metabType{1}), ' & ', num2str(metabType{2}), woBaseline], 'Interpreter', 'none')
for i = 1:size(BOLDDataXY(1).fwhm,1)
    line(w,feval(fit,w,num2str(i)))
end
fig2saveAmplitude = fullfile(OutputFilesPathLocal, strcat(file2save,'_amp.fig'));
savefig(fig2saveAmplitude)

% close all
end