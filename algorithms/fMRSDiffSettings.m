classdef fMRSDiffSettings
  
    properties
        indecesRegion
        lorenzianFactor
        frequencyShift
        PhaseCorrection
        OffsetCorrection
        Automatic
    end
       
    methods
        function this = fMRSDiffSettings()
            this.indecesRegion    =  [];
            this.lorenzianFactor  =  [];
            this.frequencyShift   =  [];
            this.PhaseCorrection  =  [];
            this.Automatic        =  false;
            this.OffsetCorrection =  [];
        end

    end
    
end

