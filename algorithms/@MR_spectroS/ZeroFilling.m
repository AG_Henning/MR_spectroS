function this = ZeroFilling(this,zerroFillFactor)
%Update 09-Nov-2014
%
%
%
    if this.Parameter.ReconFlags.isContainsExtraPoints
        warning('Remove the extra points first!');

    else    

        if nargin == 1
 
           zerroFillFactor  = this.Parameter.ZeroFillSettings.ZerroFillFactor;
           

        else

           this.Parameter.ZeroFillSettings.ZerroFillFactor = zerroFillFactor;

        end



        if this.Parameter.ReconFlags.isZeroFilled && zerroFillFactor~=0  
            warning(strcat('The data are already zero filled... !'));
            this.Parameter.ZeroFillSettings.ZerroFillFactor = this.Parameter.ZeroFillSettings.ZerroFillFactor*2;
        end



         
           oldData                    = this.Data{1} ;
           initialVectorSize          = size(oldData,this.kx_dim);
           newDataSize                = size(oldData);

           if   zerroFillFactor ~=0

                newVectorSize                                      =  zerroFillFactor* initialVectorSize;
                newDataSize(this.kx_dim)                           =  newVectorSize;
                newData                                            =  zeros(newDataSize);
                newData(1:initialVectorSize,:,:,:,:,:,:,:,:,:,:,:) =  oldData(:,:,:,:,:,:,:,:,:,:,:,:);
                this.Parameter.ReconFlags.isZeroFilled             =  true;
                display                                            ('ZeroFilling Done...')

           else
                %if ZerroFillFactor=0 then remove zerrofilling
                newVectorSize                                      = initialVectorSize;
                newDataSize(this.kx_dim)                           = newVectorSize;
                newData                                            = zeros(newDataSize);
                newData(:,:,:,:,:,:,:,:,:,:,:,:)                   = oldData(1:initialVectorSize,:,:,:,:,:,:,:,:,:,:,:);
                this.Parameter.ReconFlags.isZeroFilled             = false;
                this.Parameter.Headers.AcquisitionTime_ms          = this.Parameter.Headers.AcquisitionTime_ms * zerroFillFactor ; 
                display                                            ('ZeroFilling was removed...')
           end

           this.Data{1}                               = newData;

    end
end
    






