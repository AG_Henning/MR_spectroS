%% ISIS ordering scheme 
function [this] = ISIS_Scheme(this)

if ~this.Parameter.ReconFlags.isISISAveraged
    warning('The data should be generally ISIS averaged first')
end

if this.Parameter.ReconFlags.isISISOrdered
    warning('The data are already ordered according to the ISIS scheme')
end

scheme = this.Parameter.ISISSettings.scheme;
nSamples      = size(this.Data{1},this.kx_dim);
nCoils        = size(this.Data{1},this.coil_dim);
nAverages     = size(this.Data{1},this.meas_dim);   

Data       = this.Data{1};     
tmp        = squeeze(Data);
if nCoils == 1
    tmp = permute(tmp,[1 3 2]);
end

if strcmp(scheme,'ISIS-orig')
    nISIS  = 8;
    nSpec  = nAverages/nISIS;
    tmp2       = zeros(nSamples,nCoils,nSpec);
    for idxOfSpec = 0:nSpec-1
        offsetISIS = idxOfSpec*nISIS;
        for idxOfCh = 1:nCoils
            tmp2(:,idxOfCh, idxOfSpec+1)    = tmp(:,idxOfCh,1+offsetISIS) + tmp(:,idxOfCh,2+offsetISIS) + tmp(:,idxOfCh,3+offsetISIS) + ...
                tmp(:,idxOfCh,4+offsetISIS) + tmp(:,idxOfCh,5+offsetISIS) + tmp(:,idxOfCh,6+offsetISIS) + ...
                tmp(:,idxOfCh,7+offsetISIS) + tmp(:,idxOfCh,8+offsetISIS);
        end
    end

elseif strcmp(scheme,'EISIS-full')
    nISIS  = 36;
    nSpec  = nAverages/nISIS;
    tmp2       = zeros(nSamples,nCoils,nSpec);
    for idxOfSpec = 0:nSpec-1
        offsetISIS = idxOfSpec*nISIS;
        for idxOfCh = 1:nCoils
            tmp2(:,idxOfCh, idxOfSpec+1)    = ...
                + tmp(:,idxOfCh,1+offsetISIS) + tmp(:,idxOfCh,2+offsetISIS) + tmp(:,idxOfCh,3+offsetISIS) ...
                + tmp(:,idxOfCh,4+offsetISIS) + tmp(:,idxOfCh,5+offsetISIS) + tmp(:,idxOfCh,6+offsetISIS) ...
                + tmp(:,idxOfCh,7+offsetISIS) + tmp(:,idxOfCh,8+offsetISIS) + tmp(:,idxOfCh,11+offsetISIS) ...
                + tmp(:,idxOfCh,12+offsetISIS) + tmp(:,idxOfCh,13+offsetISIS) + tmp(:,idxOfCh,14+offsetISIS) ...
                + tmp(:,idxOfCh,15+offsetISIS) + tmp(:,idxOfCh,16+offsetISIS) + tmp(:,idxOfCh,17+offsetISIS) ...
                + tmp(:,idxOfCh,18+offsetISIS) + tmp(:,idxOfCh,19+offsetISIS) + tmp(:,idxOfCh,20+offsetISIS) ...
                + tmp(:,idxOfCh,21+offsetISIS) + tmp(:,idxOfCh,22+offsetISIS) + tmp(:,idxOfCh,23+offsetISIS) ...
                + tmp(:,idxOfCh,24+offsetISIS) + tmp(:,idxOfCh,25+offsetISIS) + tmp(:,idxOfCh,26+offsetISIS) ...
                + tmp(:,idxOfCh,29+offsetISIS) + tmp(:,idxOfCh,30+offsetISIS) + tmp(:,idxOfCh,31+offsetISIS) ...
                + tmp(:,idxOfCh,32+offsetISIS) + tmp(:,idxOfCh,33+offsetISIS) + tmp(:,idxOfCh,34+offsetISIS) ...
                + tmp(:,idxOfCh,35+offsetISIS) + tmp(:,idxOfCh,36+offsetISIS);
        end
    end
else
    error('ISIS scheme should be ISIS-orig or EISIS-full')
end

this.Data{1}                                = double(permute(tmp2,[1,4,5,2,6,7,8,9,10,11,12,3]));
this.Parameter.ReconFlags.isISISOrdered     = true;
