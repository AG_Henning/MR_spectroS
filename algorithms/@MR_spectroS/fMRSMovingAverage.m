 function this = fMRSMovingAverage( this )

shiftedAverages = this.Parameter.fMRSAveSettings.shiftedAverages;
nOfAverages     = this.Parameter.fMRSAveSettings.numberOfAverages;

oldData         = this.Data{1};
nAverages       = size(oldData,this.meas_dim);

if (this.Parameter.ReconFlags.isfMRSMovingAveraged)
    error('The data are already moving-averaged!!!')
end

% (numberOfNewPoints -1 )*shiftedAverages + nOfAverages = nAverages
 
numberOfNewPoints = (nAverages - nOfAverages)/shiftedAverages + 1;

if (numberOfNewPoints - floor(numberOfNewPoints)) %check if is not integer  
    warning('numberOfNewPoints is not an integer! Please select an appropriate parameters!. We skip this step for this time.')    
    
else
    newSize                = size(oldData);
    newSize(this.meas_dim) = numberOfNewPoints;
    newData                = zeros(newSize);
    for n=1:numberOfNewPoints 
      startPoint =  1 + (n-1)* shiftedAverages;
      endPoint   = (n-1)*shiftedAverages + nOfAverages;
      newData(:,:,:,:,:,:,:,:,:,:,:,n) = sum(oldData(:,:,:,:,:,:,:,:,:,:,:,startPoint:endPoint),this.meas_dim);    
    end
    
    this.Data{1}                                     = newData;
    this.Parameter.ReconFlags.isfMRSMovingAveraged   = true;
    this.Parameter.fMRSAveSettings.numberOfNewPoints = numberOfNewPoints;
    
end




