classdef FilterSettings <hgsetget
    
    properties
        Gaussian
        Exponential
    end
    
    methods
        function this=FilterSettings
            this.Gaussian=0;
            this.Exponential=0;
        end
    end
    
end

