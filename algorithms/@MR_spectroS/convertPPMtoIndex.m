function [y] = convertPPMtoIndex(this, x, zeroFillFactor)
%CONVERTPPMTOINDEX converts the ppm pair x to indexes 
%   Returns the vector of the size of the data points, 
%   with the selected region set to 1
   txFreq = this.Parameter.Headers.ScanFrequency;
   bw     = this.Parameter.Headers.Bandwidth_Hz;
   bw     = linspace(-bw/2,bw/2,size(this.Data{1},this.kx_dim)*zeroFillFactor);
   if strcmp(this.Parameter.Headers.Nucleus,'31P')
       ppm    = bw/(txFreq*1e-6);
   else
       ppm    = bw/(txFreq*1e-6) + 4.7;
   end
   index1 = ppm >= x(1);
   index2 = ppm <= x(2);
   y      = index1 & index2; 
end
