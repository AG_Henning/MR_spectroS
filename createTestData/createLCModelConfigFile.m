function [controlFile] = createLCModelConfigFile(controlFilesPath, defaultControlFile, ...
    defaultTE, currentTE, defaultLCModelUser, currentLCModelUser,  ...
    defaultSpectraName, filenameData, defaultWaterRefName, filenameWater)
% Creates the control files based on given a default control file.
% the newly generated files will be placed in the same path as indicated by
% the controlFilesPath.
% The defaultControlFile has to include the keyword "defaultSpectraName" in the file name.
% A sample input:
% controlFilesPath = 'Fitsettings/';
% defaultControlFile = 'fitsettings_XXXX_Metabolite.control'

if ~contains(defaultControlFile, defaultSpectraName)
    error('The default control file should have the keyword %s in its file name.', defaultSpectraName);
end

fitSettingsDefaultFileID = fopen([controlFilesPath defaultControlFile], 'r');

% change the control file name
controlFile = strrep(defaultControlFile, defaultSpectraName, filenameData);

fitSettingsFileID = fopen([controlFilesPath controlFile], 'w');

while(~feof(fitSettingsDefaultFileID))
    s = fgetl(fitSettingsDefaultFileID);
    s = strrep(s, defaultLCModelUser, currentLCModelUser);
    s = strrep(s, defaultTE, currentTE);
    s = strrep(s, defaultSpectraName, filenameData);
    if ~isempty(filenameWater)
        s = strrep(s, defaultWaterRefName, filenameWater);
    else
        s = strrep(s, 'dows= T', 'dows= F');
    end
    
    fprintf(fitSettingsFileID,'%s\n', s);
end
frewind(fitSettingsDefaultFileID);
fclose(fitSettingsFileID);

fclose(fitSettingsDefaultFileID);
end