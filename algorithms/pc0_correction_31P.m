function phase = pc0_correction_31P(data, sing, maxPhasePc0Deg, bw, verbose, detect)

%            pc0: zeroth order phase
%           spec: raw spectra
% maxPhasePc0Deg: maximal possible 0th order phase 
%             bw: bandwidths (divided by fo)[ppm]
%       singlets: position of singlets      [ppm]
%         detect: 1=phantom
%                 2=in vivo
%        verbose: specifies if function should show output

spec = data;
si = size(spec);
% sing = [0]; 
% si = size(spec);
% maxPhasePc0Deg = 180;
% bw = 4000/162;
% verbose = true;
% wi_roi = 0.4;

switch detect
    case 1,
        wi_roi = 0.4;			% width [ppm] around peak
    case 2,
        wi_roi = 0.2;			% width [ppm] around peak
end

ax1 = [-si(1)/2:(si(1)-1)/2]/si(1);
% ax2 = [-si(2)/2:(si(2)-1)/2]/si(2);

numberOfPhaseSteps = 50;
maxPhasePc0Rad = maxPhasePc0Deg / 180 * pi;
phaseStep = maxPhasePc0Rad/numberOfPhaseSteps * 2;
if phaseStep < 0.005 %smaller resolution does not make sense from experience
    phaseStep = 0.005;
end

%zero order phases to check for
phis = [-maxPhasePc0Rad:phaseStep:maxPhasePc0Rad];
l_phis = length(phis);		% length of index (phis) list
ls = length(sing);
phase = [];

for m = 1:si(2) %run over all coils
    specCoil = spec(:,m);
    spec_freq = fftshift(fft(specCoil(:,1)));
    
        for l=1:ls %run for all singlets
            %select centrum of spectral indices in dim 1
            ind1 = find(((ax1*bw)>sing(l)-wi_roi/2) & ...
                ((ax1*bw)<sing(l)+wi_roi/2));
            i1h = (ind1(end)-ind1(1))/2; %middle index in dim1
        %     ind2 = find(((ax2*bw(2)+zp2)>sing(l)-wi_roi/2) & ...
        %         ((ax2*bw(2)+zp2)<sing(l)+wi_roi/2));

        %     spec_roi = spec(:,1);
        %     spec_roi = spec(ind1,:);
        %     spec_roi = spec_roi(:,1);   %fuer erste Spule momentan
%             spec = spec(:,1);
%             spec_freq = fftshift(fft(spec(:,1)));
            spec_roi = spec_freq(ind1,:);
            maxRealAmpl = [];
            integralRealAmpl = [];

            abs_h_ampl = 0.0;
            %set min_sum_im to a ridiculously high value
            min_sum_im = max(specCoil(:)) * si(1);
            %difference between max and min peak in the dispersion spectra: search
            %for minimum, start with a ridiculously high value
            min_diff_ampl=max(specCoil(:)) * si(1);

            for phi=phis % phi = pc0/180*pi
                    % iterating is unelegant methods for some:
                    %  could be calculated through combintation of real and
                    %  imaginary part
                    spec_roi_phased = spec_roi*exp(-1i*phi);
                    spec_roi_phased_real = real(spec_roi_phased);
                    spec_roi_phased_imag = imag(spec_roi_phased);
                    % 1: determine maximum
                    maxRealAmpl = [maxRealAmpl max(spec_roi_phased_real(:))];
                    % 2: determine integral of whole peak
                    integralRealAmpl = [integralRealAmpl sum(spec_roi_phased_real(:))];

                    % Alex's methods
        %             abs_h_ampl = max(abs(spec_roi_phased_real));
        %             h_ampl_imag = max(spec_roi_phased_imag);
        %             l_ampl_imag = min(spec_roi_phased_imag);
        % 
        %             %find phase representing the minimal imaginary integral
        %             abs_sum_im = abs(sum(spec_roi_phased_imag));
        %             if(abs_sum_im < min_sum_im) && (maxRealAmpl(index) == abs_h_ampl)
        %                 min_sum_im = abs_sum_im;
        %                 phase_sum_im = phi;
        %             end
        % 
        %             %find phase representing the minimal amplitude
        %             %difference of the imaginary part
        %             diff_ampl = abs(abs(h_ampl_imag)-abs(l_ampl_imag));
        %             if(diff_ampl < min_diff_ampl) && (maxRealAmpl(index) == abs_h_ampl)
        %                 min_diff_ampl = diff_ampl;
        %                 phase_diff_ampl = phi;
        %             end

        %             index = index + 1;
            end

        % centre of gravity of maxima (much more reliable)
            [yy_mami,ii_mami] = min(maxRealAmpl);
            maxRealAmpl = maxRealAmpl-yy_mami;
            if ii_mami<l_phis/2
                cg_phis = phis + ([1:l_phis]<ii_mami)*2*pi;
            else
                cg_phis = phis - ([1:l_phis]>=ii_mami)*2*pi;
            end

            pc_max = sum(maxRealAmpl.*cg_phis)/sum(maxRealAmpl);

            [yy,ii_int] = max(integralRealAmpl);

            % determine the closer aread to search
            % ia: index list around

            iawi = l_phis/10;		% widths of indices around
            if ii_int<=iawi,
                ia = round([1:(ii_int+iawi) (l_phis-iawi+ii_int):l_phis]);
            else
                if ii_int>l_phis-iawi
                    ia = round([1:(ii_int-l_phis+iawi) (ii_int-iawi:l_phis)]);
                else
                    ia = round([ii_int-iawi:ii_int+iawi]);
                end
            end

            % different phases in [rad]
            pc_all = [pc_max phis(ii_int)];
            pcs(l) = mean(unwrap(sort(pc_all)));

            if verbose
                fprintf('Phase correction 0 order phases:\n Peak:%g\t%0.3g\t\t%0.3g\t\t%0.3g\n',...
                    sing(l),pc_all(1)*180/pi,pc_all(2)*180/pi,pcs(l)*180/pi);
            end


        end
        pcs_tmp = pcs;
        pcs = unwrap(pcs)*180/pi;

        if verbose
            if any(pcs_tmp~=unwrap(pcs_tmp)),
                fprintf('Unwrapping\n');
                display(pcs);
            end
        end

        % switch detect
        switch 2
            case 1,		% phantom
                p = polyfit((zp2-sing)/bw(2),pcs,1);
                pc0 = p(2);
                pc1 = -p(1);
            case 2,		% in vivo
                pc0 = mean(pcs);
                pc1 = 0;
            otherwise, error('nothing detected');
        end
        if pc0>180,
            pc0 = pc0-360;
        end
        if pc0<-180,
            pc0 = pc0+360;
        end
    
    phase = [phase, pc0];    
end