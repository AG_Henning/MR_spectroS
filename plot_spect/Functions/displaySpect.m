function displaySpect(handles)


currentPosition           = handles.MRSpectroMatrix.currentPosition;
this                      = handles.MRSpectroMatrix.Matrix(currentPosition);
selectedDisplayValues     = handles.SelectedDisplayValues; 
selectedProcessingValues  = handles.SelectedProcessingValues;
displayProperties         = handles.DisplayProperties;

% edit real-imaginary-absolute 
data=this.Data{1};

nTimepoints         = size(this.Data{1},this.kx_dim);
selectedCoil        = get(selectedDisplayValues,'selectedCoil');
selectedDyn         = get(selectedDisplayValues,'selectedDyn');
bandwidth           = get(displayProperties,'bandwidth');
readout             = get(displayProperties,'acquisitionTime');
zeroPPM             = get(selectedDisplayValues,'selectedZeroPoint');
%spectralResolution  = 1000/readout;%Hz per timepoint
time                = linspace(0,readout,nTimepoints);
waterFreq           = this.Parameter.Headers.ScanFrequency * 1E-6;
dwellTime           = this.Parameter.Headers.DwellTimeSig_ns * 1e-9; %in sec
plotOnOff           = displayProperties.plotOnOff;

%########################################################################



frequencyRange    = linspace(-bandwidth/2,bandwidth/2,nTimepoints);
%zeroPoint         = length(frequencyRange)/2;


selectedType      = get(selectedDisplayValues,'selectedType');
selectedMode      = get(selectedDisplayValues,'selectedMode');
selectedMix       = get(selectedDisplayValues,'selectedMix');


selectedAverage   = int16 (get(selectedDisplayValues,'selectedAverage') );


if get(selectedDisplayValues,'selectedAveraging')
    
    range =get(selectedDisplayValues,'selectedAveRange');
    range = str2num(range);
    %[a, b]=find_range(range);
    % no squeeze!   
    
    data=sum(data(:,1,1,selectedCoil,1,1,1,1,selectedMix,selectedDyn,1,range(1):range(2)),this.meas_dim);
    
else
    data=squeeze(data(:,1,1,selectedCoil,1,1,1,1,selectedMix,selectedDyn,1,selectedAverage));
end 

if plotOnOff
    data2=this.Data{1};
    secondAverage = handles.DisplayProperties.secondAverage;
    data2 = squeeze(data2(:,1,1,selectedCoil,1,1,1,1,selectedMix,1,1,secondAverage));
    fft2 = fftshift(fft(data2));
end

   
%##########################################################################    
%                            PROCESSING STEPS   
%##########################################################################




  

%--------------------------Phase 0-----------------------------------------  
   phase0= get(selectedProcessingValues,'Phase0');
   %phase_0= str2double(phase_0);
   phase0= (phase0*pi)/180; %in rads
   
    
   
   
  
%-----------------------Phase 1--------------------------------------------
   phase1= get(selectedProcessingValues,'Phase1');
   %phase_1= str2double(phase_1);
   phase1= (phase1*pi)/180; %in rads
   
   
%=-------------------Ref frequency for phase correction--------------------

 refPhaseFreq = get(selectedProcessingValues,'RefPhaseFrequency');
 
   
%------------------------------Voigt Filter -------------------------------
%                exp(-t*exp_filter)*exp(-t^2/(2*gauss_filter^2))
   expFilter= get(selectedProcessingValues,'Exponential');
   expFilter= expFilter*pi; %In order to be in Hz exp(-At)--> linewidth= A/pi
   
   gaussFilter= get(selectedProcessingValues,'Gaussian');
   gaussFilter= gaussFilter/readout; %normalised % of aquisition time
   
   
   
   if gaussFilter==0
       gaussPart=1;
   else
       gaussPart=(exp(-linspace(0,1,length(data)).^2/(2*gaussFilter^2))');
   end
   
   
       expPart=(exp(((0:length(data)-1)*dwellTime)*expFilter)');
       voigt=expPart.*gaussPart;
   
   
   
   
%------------------Data after Voigt, Phase_0 and Phase_1---------------------
   data=data.*voigt.*exp(1i*phase0);
 
   %%% missing point prediction
   imiss= get(selectedProcessingValues,'MissingPointPred_imiss');
   T_factor= get(selectedProcessingValues,'MissingPointPred_T_factor');
   polyOrder = get(selectedProcessingValues,'MissingPointPred_polyOrder');

   data = MR_spectroS.missingPointPredictionArray(data, imiss, T_factor, polyOrder);

   %%%
   
%    %does not work properly?!

     if phase1~=0
     ffts=fftshift(fft(data));
     ffts=ffts.*(exp(1i*(frequencyRange-refPhaseFreq)*phase1)');
     data=ifft(fftshift(ffts));
     
%    data(end-2:end)=0;
     %ffts=fftshift(fft(data));
     end
   
   
%--------------------------HSVD--------------------------------------------
     if get(handles.SelectedProcessingValues,'hsvd') 
         
        range             = get(handles.SelectedProcessingValues,'boundaries');
        lB                = range(1);  %in Hz
        uB                = range(2);  %in Hz 
        
        bound             = [lB, uB]/bandwidth; %Normalised units 
        nSinusoids       = get(handles.SelectedProcessingValues,'nSinusoids');
        matrixSize        = get(handles.SelectedProcessingValues,'matrixSize');
        
        [~,~,~,~,~,~,fidFit, ~ ,~,~] = ... 
        hsvd_plot(data',nSinusoids,matrixSize,bound,0,0);
    
        originalData    = data;
        data            = data-fidFit'; % %
     end
     ffts=fftshift(fft(data));
   
     
     
% ffts: data in frequency domain (fftshift) 
% data: data in the time domain
   
   

 
   
%----------------------------FWHM Calculation------------------------------

    fwhm = linewidth(data, selectedProcessingValues, bandwidth);
    fwhmString = sprintf('%3.2f', fwhm);
    set(handles.text_fwhm,'String',fwhmString);
    
%------------------------SNR calculation-----------------------------------

     snrValue = snr(data,selectedProcessingValues);
     %snrValue = snrValue * fwhm;
     %snrValue = 20*log10(snrValue) ;
     set(handles.text_snr,'String',num2str(snrValue));



%##########################################################################
%                        PLOTTING STEPS
%##########################################################################



% in case that a HSVD filter is applied rearrange the data format in order
% to show also the original data and the fitted data as well. In this way
% the observer can evaluate the efficiency of the water peak removal

% hOriginalV
% hFittedV
% hSuppressedV
if  get(selectedProcessingValues,'hsvd') 
    if ~get(selectedProcessingValues,'hFittedV'), fidFit=[]; end
    if ~get(selectedProcessingValues,'hOriginalV'), originalData=[]; end
    if ~get(selectedProcessingValues,'hSuppressedV'), ffts=[]; end
    ffts=[fftshift( fft(originalData)) ffts  fftshift( fft(fidFit'))];
    
end    
 

if plotOnOff
    ffts=[ffts fft2];
end



switch selectedMode
    case 'Real'
        
        ffts=real(ffts);
        data=real(data);
        
    case 'Imaginary'
        
        ffts=imag(ffts);
        data=imag(data);
        
    case 'Phase'
        
        ffts=(180/pi)*angle(ffts);
        data=(180/pi)*angle(data);
         
    otherwise 
        
        ffts=abs(ffts);
        data=abs(data);
end




axes(handles.axes_plot)


switch selectedType
    
    case 'ppm'
          
        plot((frequencyRange)/waterFreq+zeroPPM,ffts)
        xlabel(sprintf( 'ppm, water peak= %1.1f ppm',zeroPPM))
            set(gca,'XDir','reverse')
            
    case 'frequencies'
          
          plot(frequencyRange,ffts)
          xlabel('Frequencies')
          set (gca,'XLim',[0 0.5]);
          set(gca,'XDir','reverse')
          
        
          
    case 'indices'
        
        plot( 1:length(ffts), ffts); 
            xlabel('Indeces')
            %set(gca,'XDir','reverse')
           
        
    otherwise
        
        plot(time,data);
        xlabel('time (ms)');
      
end  

% Scale properly the axes 
axis tight

if get(displayProperties,'dimRangeLock');
    
    XLim = get(displayProperties,'xRange');
    YLim = get(displayProperties,'yRange');
    
    set(gca,'XLim',XLim);
    set(gca,'YLim',YLim);
else
    
    v = axis; 
    set(gca,'XLim',[v(1)*1.05 v(2)*1.05]);
    set(gca,'YLim',[v(3)*1.05 v(4)*1.05]);
    
end






% if size(ffts,2)~=1
%     legend('fid','residual','fitted')
% else
%     legend('fid')
% end
legend off

 
if plotOnOff
    switch secondAverage
        case 1  
             legend('OFF ','ON');
        case 2
             legend('ON','OFF');
    end 
end

  




        

