function this = CorrectMCSubtractionArtifacts(this)
% This function is probably useles!!
warning('This function is probably useles!!')

%retrieve general information from the dataset
oldData     = this.Data{1};
newData     = zeros(size(oldData));
nAverages   = size(oldData,this.meas_dim);
samples     = size(oldData, this.kx_dim);
nCoils      = size(oldData, this.coil_dim);
nMix        = size(this.Data{1}, this.mix_dim);
scanFreq    = this.Parameter.Headers.ScanFrequency;
dwellTime   = this.Parameter.Headers.DwellTimeSig_ns*1e-9;

zeroFillFactor = 1;
ppmRangeToCorrect = [0.6 1.8];
splineFitConfidenceIntervalOffset = 2;
ppmRangeToFitSpline = ppmRangeToCorrect + [-splineFitConfidenceIntervalOffset splineFitConfidenceIntervalOffset];

[ppmRangeToCorrectIndeces] = convertPPMtoIndex(this, ppmRangeToCorrect, zeroFillFactor);
[ppmRangeToFitSplineIndeces] = convertPPMtoIndex(this, ppmRangeToFitSpline, zeroFillFactor);
%TODO hard coded for only one coil and average
selectedCoil = 1;
iAverage = 1;

spectraMix = 1;
waterMix = 2;
spectraFID = oldData(:,1,1,selectedCoil,1,1,1,1,spectraMix,:,1,iAverage);
spectraFFTs      = fftshift(fft(spectraFID));
waterFID = oldData(:,1,1,selectedCoil,1,1,1,1,waterMix,:,1,iAverage);
waterFFTs      = fftshift(fft(waterFID));

waterRegionToFit = waterFFTs(ppmRangeToFitSplineIndeces);
y = linspace(ppmRangeToFitSpline(1), ppmRangeToFitSpline(2),length(waterRegionToFit))';
[fitWaterReal, gofReal, outputReal] = fit(y, real(waterRegionToFit), 'poly3','Normalize','on','Robust','Bisquare');
[fitWaterImag, gofImag, outputImag] = fit(y, imag(waterRegionToFit), 'poly3','Normalize','on','Robust','Bisquare');
%% display
figure
hold on
plot(fitWaterReal,y, real(waterRegionToFit))
plot(y, real(waterRegionToFit))
plot(y, outputReal.residuals)

figure
hold on
plot(fitWaterImag,y, imag(waterRegionToFit))
plot(y, imag(waterRegionToFit))
%%
y2 = linspace(ppmRangeToCorrect(1), ppmRangeToCorrect(2),length(waterFFTs(ppmRangeToCorrectIndeces)))';
figure
plot(fitWaterReal, y2, real(waterFFTs(ppmRangeToCorrectIndeces)))

waterToSubtract = complex(zeros(1,samples));
waterToSubtract(ppmRangeToFitSplineIndeces) = complex(outputReal.residuals, outputImag.residuals);
waterToSubtract = waterToSubtract .* ppmRangeToCorrectIndeces;
figure
plot(real(waterToSubtract))
newSpectra = spectraFFTs + waterToSubtract';
newWater = waterFFTs - waterToSubtract';

figure
hold on
plot(real(newSpectra))
plot(real(spectraFFTs))
plot(real(waterToSubtract'))
set(gca,'xDir','reverse')

newData(:,1,1,selectedCoil,1,1,1,1,spectraMix,:,1,iAverage) = ifft(ifftshift(newSpectra));
newData(:,1,1,selectedCoil,1,1,1,1,waterMix,:,1,iAverage) = ifft(ifftshift(newWater));

this.Data{1}                                    = newData;
% this.Parameter.ReconFlags.isFrequencyAligned    = true;

fprintf('Frequency alignment...FINISHED')     

end