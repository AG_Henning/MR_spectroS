function doFittingAndT2Calculations_MM(fitAllSubjects)

doWM_GM = 'WM';

if strcmp(doWM_GM,'GM')
    downField_MM_Met = 'MM';
    pathName = 'MM data path';
    subjects = {'1658';'1706';'1717';'2017';'3373';'3490';'5771';'6249';'6971';'7338';'7782';'9810'};
    remotePathBase = '/Desktop/MM';
else
    downField_MM_Met = 'MM WM';
    pathName = 'MM WM data path';
    subjects = {'1658';'1717';'2016';'2020';'3490'};
    remotePathBase = '/Desktop/MM_T2_WM';
end
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
fitAllSubjects = false;
fitAminoAcids = false;

fitToSubtractCr = false;
if fitToSubtractCr
    fitAllSubjects = false;
end

if fitAminoAcids
    orderTEs = {'24'};
else
    orderTEs = {'24' '32' '40' '52' '60'};
end

extensionTable = '.table';
extensionCoord = '.coord';
defaultTE = 'TE24';
defaultSubject = 'XXXX';
defaultLCModelUser = 'tborbath';

if fitAllSubjects
    controlFilesBase = 'fitsettings_Subject_XXXX_';
    controlFilesBaseTE = strcat(controlFilesBase, 'TE');
    controlFilesBaseSuffix = '_Cre.control';
    controlFilesBaseSuffix = '_Lopez.control';
    outputFileNameBase = strcat(defaultSubject, '_TE');
else
    controlFilesBase = 'fitsettings_Summed_Averaged_';
    controlFilesBaseTE = strcat(controlFilesBase, 'TE');
    %     controlFilesBaseSuffix = '_Cre.control';
    %     controlFilesBaseSuffix = '_Cre_PpmPfeuffer.control';
    controlFilesBaseSuffix = '_Cre_Pfeuffer_FWHM_T2.control';
    controlFilesBaseSuffix = '_Lopez_all_linewidths.control';
%     controlFilesBaseSuffix = '_paper_J_2.7.control';
    if fitToSubtractCr
        controlFilesBaseSuffix = '_subtract_Cr.control';
    end
    if fitAminoAcids
        controlFilesBaseSuffix = '_AminoAcids.control';
    end
    outputFileNameBase = 'Summed_Averaged_TE';
end

%% file paths setup
controlFilesPathRemote = [remotePathBase '/LCModelConfig/'];
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
if fitAminoAcids
    LCModelOutputFilesPathRemote = [remotePathBase '/OutputAminoAcids/'];
    LCModelOutputPath = [localFilePathBase, 'OutputAminoAcids/'];
else
    LCModelOutputFilesPathRemote = [remotePathBase '/Output/'];
    LCModelOutputPath = [localFilePathBase, 'Output/'];
end
defaultControlFile = strcat(controlFilesBase, 'Default', controlFilesBaseSuffix);


if fitAllSubjects
    subjectsPath = strcat(subjects, '/');
else
    subjects = {'Summed'};
    subjectsPath = {''};
end

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath);
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath);
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath);
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath);

%% basic configurations
LCModelTableFits = strcat(outputFileNameBase, orderTEs, extensionTable);
LCModelTableFitsCoord = strcat(outputFileNameBase, orderTEs, extensionCoord);
LCModelOutputFiles = strcat(outputFileNameBase, orderTEs);
LCModelControlFiles = strcat(controlFilesBaseTE, orderTEs, controlFilesBaseSuffix);

numberOfTes = length(orderTEs);
numberOfSubjects = length(subjects);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

%% create the control file series
if fitAllSubjects
    createLCModelConfigsTEseries('MM', controlFilesPathLocal, defaultControlFile, orderTEs, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser, subjects, defaultSubject);
else
    % call the Configuration file creator without subject configuration
    createLCModelConfigsTEseries('MM', controlFilesPathLocal, defaultControlFile, orderTEs, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser);
end

%% load the table with the deleted averages
load([localFilePathBase, 'tableDeletedAverages.mat'], 'tableDeletedAverages');

%% do the actual LCModel fitting and T2 calculation
for indexCurrentSubject = 1:numberOfSubjects
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    if fitAllSubjects
        currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
        currentLCModelTableFits = strrep(LCModelTableFits, defaultSubject, subjects{indexCurrentSubject});
        currentLCModelTableFitsCoord = strrep(LCModelTableFitsCoord, defaultSubject, subjects{indexCurrentSubject});
    else
        currentControlFiles = LCModelControlFiles;
        currentOutputFiles = LCModelOutputFiles;
        currentLCModelTableFits = LCModelTableFits;
        currentLCModelTableFitsCoord = LCModelTableFitsCoord;
    end
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
    
    %%extract the TEs to eliminate
    eliminateTEs = {};
    indexTableSubject = find(strcmp(subjects{indexCurrentSubject}, tableDeletedAverages(:,1)), 1);
    %if the index is empty, then we don't eliminate any TE
    if (~isempty(indexTableSubject))
        % we assume that
        for indexTableDeleteTE = 1:numberOfTes
            indexCurrentTE = tableDeletedAverages{indexTableSubject+indexTableDeleteTE-1,2};
            if strcmp(indexCurrentTE,['TE' orderTEs{indexTableDeleteTE}]) == 1
                deletedAverages = tableDeletedAverages(indexTableSubject+indexTableDeleteTE-1,3:end);
                deletedAveragesNum = cell2mat(deletedAverages);
                if (length(deletedAveragesNum) > 2) %TODO check what is best 2 or 0
                    eliminateTEs{end+1} = orderTEs{indexTableDeleteTE};
                end
            else
                warning('Subject %s does not have the matching TEs in the table of deleted averages.',subjects{indexCurrentSubject});
            end
        end
    else
        warning('Subject %s not found in the table of deleted averages.',subjects{indexCurrentSubject});
    end

    %% calculate the available TEs
    availableTEs = {};
    availableLCModelTableFits = {};
    availableLCModelTableFitsCoord = {};
    indexTE = 0;
    for indexCurrentTE = 1:numberOfTes
        foundTE  = find(strcmp(orderTEs{indexCurrentTE}, eliminateTEs), 1);
        if isempty(foundTE)
            indexTE = indexTE +1;
            availableTEs{indexTE} = orderTEs{indexCurrentTE};
            availableLCModelTableFits{indexTE} = currentLCModelTableFits{indexCurrentTE};
            availableLCModelTableFitsCoord{indexTE} = currentLCModelTableFitsCoord{indexCurrentTE};
        end
    end
    numOfAvailableTEs = length(availableTEs);
    %% do the T2 exponential fits
    [tableConcentrations{indexCurrentSubject}, tableT2s{indexCurrentSubject}] = calculate_T2(availableLCModelTableFits, ...
        subjectsLCModelOutputPath{indexCurrentSubject}, false, availableTEs, downField_MM_Met);
    
    %% mean Rsquared
    Rsquares = str2double(tableT2s{indexCurrentSubject}(3,2:end));
    meanRsquared =  mean(Rsquares);
    
    %% mean CRLB
    CRLBs = cell2mat((tableConcentrations{indexCurrentSubject}(2:numberOfTes + 1,3:3:end)));
    meanCrlbTEs = mean(CRLBs,2, 'omitnan');
    meanCrlb = mean(meanCrlbTEs);
    
    %% T2 of Cr
    metNames = tableT2s{indexCurrentSubject}(1,2:end);
    indexes = strfind(metNames,'Cr');
    indexCr = find(not(cellfun('isempty', indexes))) + 1;
    T2Cr = tableT2s{indexCurrentSubject}(2,indexCr);
    
    %% display means
    table(1,1) = {'TE'};
    table(1,2:numOfAvailableTEs+1) = availableTEs(1:end);
    table(2,1) = {'CRLB'};
    table(2,2:numOfAvailableTEs+1) = num2cell(meanCrlbTEs(1:end));
    table(3,1) = {'meanCRLB'};
    table(3,2) = num2cell(meanCrlb);
    table(4,1) = {'meanR2'};
    table(4,2) = num2cell(meanRsquared);
    table(5,1) = {'T2_Cr'};
%     table(5,2) = T2Cr;
    display(table)
    
    %% save table to file
    exportFileXlsx = [LCModelOutputPath ,'Quantification_Summaries.xlsx'];
    startIndex = 1+ 7 *(indexCurrentSubject-1);
    cellPositionTitle = ['A' num2str(startIndex)];
    xlswrite(exportFileXlsx, {['Subject: ' subjects{indexCurrentSubject}]}, 1, cellPositionTitle);
    cellPosition = ['A' num2str(startIndex + 1)];
    xlswrite(exportFileXlsx, table, 1, cellPosition);
    
    %% calculate FWHMs
    if indexCurrentSubject == 1
        [fwhms, orderMetFWHM] =calculate_FWHM(currentLCModelTableFitsCoord, subjectsLCModelOutputPath{indexCurrentSubject}, false, downField_MM_Met);
        tableFWHM = zeros(numberOfSubjects, size(fwhms,1), size(fwhms,2));
        tableFWHM(indexCurrentSubject,:,:) = fwhms;
    else
        tableFWHM(indexCurrentSubject,:,:) = calculate_FWHM(currentLCModelTableFitsCoord, subjectsLCModelOutputPath{indexCurrentSubject}, false, downField_MM_Met);
    end
end

% [tableT2sAll] = calculate_T2s_all(tableConcentrations, availableTEs, LCModelOutputPath, 'MM', false);

if fitAllSubjects
    fileNameSuffix = 'Subjects';
else
    fileNameSuffix = 'Summed';
end
tableFWHM_name = 'tableFWHM';
tableT2s_name = 'tableT2s';
orderMet_name = 'orderMetFWHM';
tableConcentrations_name = 'tableConcentrations';
tableFWHM_nameSuffixed = [tableFWHM_name fileNameSuffix];
tableT2s_nameSuffixed = [tableT2s_name fileNameSuffix];
orderMet_nameSuffixed = [orderMet_name fileNameSuffix];
tableConcentrations_nameSuffixed = [tableConcentrations_name fileNameSuffix];
eval( [tableFWHM_nameSuffixed, '=',  tableFWHM_name ';']);
eval( [tableT2s_nameSuffixed, '=',  tableT2s_name ';']);
eval( [orderMet_nameSuffixed, '=',  orderMet_name ';']);
eval( [tableConcentrations_nameSuffixed, '=',  tableConcentrations_name ';']);
save([LCModelOutputPath, tableFWHM_nameSuffixed, '.mat'], tableFWHM_nameSuffixed, orderMet_nameSuffixed);
save([LCModelOutputPath, tableT2s_nameSuffixed, '.mat'], tableT2s_nameSuffixed)
save([LCModelOutputPath, tableConcentrations_nameSuffixed, '.mat'], tableConcentrations_nameSuffixed)
end