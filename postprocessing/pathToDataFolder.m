function [searchedPath] = pathToDataFolder(pathName, sampleCriteria)
changedTable = false;
if nargin < 1
    error('The function pathToDataFolder needs to be called with at least 1 input argument. \n The function call is [searchedPath] = pathToDataFolder(pathName, sampleCriteria)');
end
pathFunction = mfilename('fullpath');
pathStorageFile = strrep(pathFunction, '\postprocessing\pathToDataFolder', '\PathToDataFolders.xlsx');

%% open files with paths or create basic table
tablePaths = {};
if exist(pathStorageFile, 'file') == 2
    [~, ~, tablePaths] = xlsread(pathStorageFile);
else
    tablePaths{1,1} = 'Folder name';
    tablePaths{1,2} = 'Path';
    changedTable = true;
end

%% get the desired path
index = find(strcmp(tablePaths(1:end,1), pathName));
if isempty(index)
    searchedPath = uigetdir(pwd, ['Please select the: ' pathName]);
    if (searchedPath == 0) 
        error('Can not proceed. You need to select a folder for the data: "%s"', pathName)
    end
    searchedPath = [searchedPath '\'];
    % append the table of paths
    tablePaths{end+1, 1} = pathName;
    tablePaths{end, 2} = searchedPath;
    changedTable = true;
else
    searchedPath = tablePaths{index,2};
end

%% check validity of path
if exist(searchedPath,'dir') ~= 7
    error('According to the PathToDataFolders.xlsx the "%s" should be: \n%s\nHowever this path does not exist. \nQuitting', pathName, searchedPath);
end
if exist('sampleCriteria', 'var')
    if ~iscell(sampleCriteria)
        error('SampleCriteria parameter of the pathToDataFolder function should be a cellarray')
    end
    for criteria = sampleCriteria
        if exist([searchedPath criteria{1}],'dir') ~= 7
            error('The folder "%s" can not be found in the selected Path:\n %s\nQuitting', criteria{1}, searchedPath)
        end
    end
end

%% save the paths if we didn't encounter a validity error
if changedTable
    xlswrite(pathStorageFile, tablePaths);
end
end