classdef DeleteMovedAveragesSettings
    
    properties
        
        confidenceNumOfStd
        displayPlots
        deletedAverages
        
    end
    
    methods
        
        function this= DeleteMovedAveragesSettings()
            
            this.displayPlots      =true  ;
            this.confidenceNumOfStd = 4;
            this.deletedAverages = [];
        end
        
        
        
    end
    
end

