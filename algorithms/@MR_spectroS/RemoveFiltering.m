function RemoveFiltering(this)

gauss_filter= this.Parameter.FilterSettings.Gaussian;
exp_filter=this.Parameter.FilterSettings.Exponential;
readout=this.Parameter.Headers.AcquisitionTime_ms;
        
if ~this.Parameter.ReconFlags.isfiltered
    warning('The data is not filtered. No filter to be removed');
else
        
    % NCol, 1, 1, NCha, 1, 1, 1, 1, n_mix, NRep, 1, NMeas  

    data=this.Data{1};

    %NCol=  size( data, this.kx_dim);   % # timepoints
    NMeas= size( data, this.meas_dim); % # Averages
    NCha=  size( data, this.coil_dim); % # Channels
    NRep=  size( data, this.dyn_dim);  % # Repetitions
    NMix=  size( data, this.mix_dim);  % # Mixes


    %------------------------------Voigt Filter -------------------------------
    %       exp(-t*exp_filter)*exp(-t^2/(2*gauss_filter^2))

    exp_filter= exp_filter*pi; %In order to be in Hz exp(-At)--> linewidth= A/pi
    gauss_filter= gauss_filter/readout; %normalised % of aquisition time


    if gauss_filter==0
        gauss_part=1;
    else
        gauss_part=(exp(linspace(0,1,length(data)).^2/(2*gauss_filter^2))'); %comparing to Filterning.m opposite sign
    end

    exp_part=(exp(-linspace(0,1,length(data))*exp_filter)'); %comparing to Filterning.m opposite sign
    voigt=exp_part.*gauss_part;

    for ncha=1:NCha
    for nrep=1:NRep
    for nmeas=1:NMeas
    for nmix=1:NMix
        data(:,1,1,ncha,1,1,1,1,nmix,nrep,1,nmeas)=data(:,1,1,ncha,1,1,1,1,nmix,nrep,1,nmeas).*voigt;
    end
    end
    end
    end

    this.Data{1}=data;
    this.Parameter.ReconFlags.isfiltered= false;
    this.Parameter.FilterSettings.Gaussian=0;
    this.Parameter.FilterSettings.Exponential=0;
    
end





end

