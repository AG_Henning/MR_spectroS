function this = PhaseCorrection( this )
%version 9 Oct 2014
%Update  26 Sep 2015

%method         = this.Parameter.PhaseCorrSettings.method;

phase0         = this.Parameter.PhaseCorrSettings.phase0;
phase0         = phase0 * pi/180; %in rads 

phase1         = this.Parameter.PhaseCorrSettings.phase1;
phase1         = phase1 * pi/180  ; %in rads/Hz 

nTimepoints     = size(this.Data{1},this.kx_dim);
bandwidth       = this.Parameter.Headers.Bandwidth_Hz;
frequencyRange  = linspace(-bandwidth/2,bandwidth/2,nTimepoints);
frequencyCentre = this.Parameter.PhaseCorrSettings.freqCentre;
 if this.Parameter.ReconFlags.isDataPhased
    warning('The data are already phased!!!')
 end

 % NCol, 1, 1, NCha, 1, 1, 1, 1, n_mix, NRep, 1, NMeas  
              
              data=this.Data{1};
              
              %NCol=  size( data, this.kx_dim);   % # timepoints
              NMeas        = size( data, this.meas_dim); % # Averages
              NCha         = size( data, this.coil_dim); % # Channels
              NMix         = size( data, this.mix_dim);  % # Mixes
              phase0Vector = exp(1i*phase0);
              phase1Vector = exp(1i*(frequencyRange-frequencyCentre)*phase1)';

%               if phase1~=0
%               ffts=fftshift(fft(data));
%               ffts=ffts.*(exp(1i*frequencyRange*phase1)');
%               data=ifft(fftshift(ffts));
%               end
              
              
               for ncha=1:NCha
                    for nmeas=1:NMeas
                        for nmix=1:NMix
                            data(:,1,1,ncha,1,1,1,1,nmix,1,1,nmeas)=data(:,1,1,ncha,1,1,1,1,nmix,1,1,nmeas).*phase0Vector;
                            if phase1~=0
                              ffts    = fftshift(fft(data(:,1,1,ncha,1,1,1,1,nmix,1,1,nmeas)));
                              ffts    = ffts.*phase1Vector;
                              newData = ifft(fftshift(ffts));
                              data(:,1,1,ncha,1,1,1,1,nmix,1,1,nmeas) = newData;
                            end
                        end
                    end
              end

              this.Data{1}=data;
              this.Parameter.ReconFlags.isDataPhased= true;
      



end

