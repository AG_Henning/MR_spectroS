function [pc1] = phaseCorrect1Order(spec, maxPhasePc1, pc1RefPpm, bw,zp2, singlets,detect, verbose)
% phaseCorrect1Order Automatic detection of 1st order phase for JPRESS 
%   [pc1] = phaseCorrect1Order(spec,maxPhasePc1,pc1RefPpm,bw,zp2,singlets,detect, verbose)
%
%         pc1: linear phase
%        spec: 2D JPRESS
% maxPhasePc1: maximal possible phase
%   pc1RefPpm: reference pivot point for the 1st order phase correction
%          bw: bandwidths (divided by fo)[ppm]
%         zp2: zero point in f2          [ppm]
%    singlets: position of singlets      [ppm]
%      detect: 1=phantom
%              2=in vivo
%     verbose: specifies if function should show output
%
% See also JPRESS, JZPS.

if nargin<1, help(mfilename); return; end
narginchk(7,8);
if ~exist('verbose','var')
    verbose = false;
end

if verbose
    plt = true;
    fprintf('Automatic 1st order phase correction\n');
else
    plt = false;
end

useSinglets = 1; %don't use the singlets

wi_roi2 = 0.1;			% width [ppm] around peak in df2 dimension
wi_roi1 = 0.1;			% width [ppm] around peak in df1 dimension

%calculate eligible phis for phase correction
eligibility_wi_roi1 = 0.02;
eligibility_min_ppm2 = 1.8;
eligibility_max_ppm2 = 4.1;

numberOfPhaseSteps = 50;
phaseStep = maxPhasePc1/numberOfPhaseSteps * 2;
if phaseStep < 5 %smaller resolution does not make sense from experience
    phaseStep = 5;
end
%first order phases to check for
pc1s = [-maxPhasePc1:phaseStep:maxPhasePc1];

%eligibility integral percent, defines how far from the maximal integral in
%the eligibility area we search for the best possible 1st order phase
eligibility_integral_percent = 0.5;

sing = singlets{detect};
si = size(spec);

pc1RefPts = (pc1RefPpm - zp2) / bw(2) * si(2) + si(2) / 2 ;
pc1RefDt = (pc1RefPts - si(2) / 2 ) / si(2);

ax1 = [-si(1)/2:(si(1)/2-1)]/si(1);
ax2 = [-si(2)/2:(si(2)/2-1)]/si(2);

phis = pc1s*pi/180;
ls = length(sing);

ax2_dt = ax2 - pc1RefDt;
% ppm
ax2_ppm = ax2 * bw(2)+zp2;

%select centrum of spectral indices in dim 1
ind1 = find(((ax1*bw(1))> (-eligibility_wi_roi1/2)) & ...
    ((ax1*bw(1))< (eligibility_wi_roi1/2)));
ind1half = (ind1(end)-ind1(1))/2; %middle index in dim1

ind2 = find(((ax2*bw(2)+zp2)>eligibility_min_ppm2) & ...
    ((ax2*bw(2)+zp2)<eligibility_max_ppm2));

%define the calculation parameters based on the newly defined indexes
eligibility_spec_roi = spec(ind1,ind2);
siroi = size(eligibility_spec_roi);
eligibility_ax2_ppm = ax2_ppm(ind2);
eligibility_ax2_dt_roi = ax2_dt(ind2);
eligibility_ax1 = ax1(ind1);

%calculate the sign for the imaginary part to "define a phased and dephased" signal
elgibility_spec_roi_im = imag(eligibility_spec_roi);
spec_up_im = elgibility_spec_roi_im(1:ind1half, :);
spec_down_im = elgibility_spec_roi_im(ind1half+1:end, :);
sumUpSpecIm = sum(spec_up_im(:));
sumLowSpecIm = sum(spec_down_im (:));
if (sumUpSpecIm < sumLowSpecIm )
    sign = -1;
else %they can't be equal unless sth very weird happens
    sign = 1;
end

if plt
    %plot the region where the 1st order phase correction is performed
    figure(40);
    plot2d_log(eligibility_ax2_ppm, eligibility_ax1, real(eligibility_spec_roi));
    hold on;
    eligibility_mid_point1= eligibility_ax1(round(siroi(1)/2));
    eligibility_mid_point2= eligibility_ax2_ppm(round(siroi(2)/2));
    %plot the reference lines
    plot([eligibility_ax2_ppm(1) eligibility_ax2_ppm(end)],[eligibility_mid_point1 eligibility_mid_point1],'g:');
    plot([eligibility_mid_point2 eligibility_mid_point2], [eligibility_ax1(1) eligibility_ax1(end)],'g:');
    hold off;
    cmax = log(max(real(eligibility_spec_roi(:)))+1)/log(2);
    if (cmax < 0)
        cmax = -cmax;
    end
    caxis([-cmax cmax]); colorbar; colormap(cmap_dark);
    set(gca,'XDir','reverse');
end

%% calculate 1st order phase correction in the eligibility area
elibility_integral_re = zeros(size(phis));
elibilityPhasedSpectrumIm = zeros(size(phis));
elibilityDePhasedSpectrumIm = zeros(size(phis));
index = 0;
for phi = phis
    index = index +1;
    %calculate the spectrum for each 1st order phase
    spec_roi_phased = eligibility_spec_roi.*(ones(size(eligibility_spec_roi,1),1)* ...
        exp(-1i*phi*eligibility_ax2_dt_roi));
    spec_roi_phased_real = real(spec_roi_phased);
    spec_roi_phased_imag = imag(spec_roi_phased);
    %check integral should be positive
    elibility_integral_re(index) = sum(spec_roi_phased_real(:));
    
    %imaginary parts above and under midline should be oposing signs
    spec_up_phased_im = spec_roi_phased_imag(1:ind1half, :);
    spec_down_phased_im = spec_roi_phased_imag(ind1half+1:end, :);
    %calculate the negative and positive parts of the spectrum in each imaginary half
    sumUpSpecImPositive = sum((spec_up_phased_im(:)>0).*spec_up_phased_im(:));
    sumLowSpecImPositive = sum((spec_down_phased_im(:)>0).*spec_down_phased_im(:));
    sumUpSpecImNegative = sum((spec_up_phased_im(:)<0).*spec_up_phased_im(:));
    sumLowSpecImNegative = sum((spec_down_phased_im(:)<0).*spec_down_phased_im(:));
    %discriminate the phased and dephased parts of the signal in the imaginary spectrum
    phasedSpectrumIm = sumUpSpecImPositive - sumLowSpecImNegative;
    dePhasedSpectrumIm = sumLowSpecImPositive - sumUpSpecImNegative;
    elibilityPhasedSpectrumIm(index) = phasedSpectrumIm ;
    elibilityDePhasedSpectrumIm(index) = dePhasedSpectrumIm ;
end

%calculate the index and the max value for the phased data integrals
[maxValue_eligibility_integral_re, ii_eligibility_integral_re] = max(elibility_integral_re);

%define the eligible (possible) phases for the follow up calculations.
eligible_indexes = elibility_integral_re > maxValue_eligibility_integral_re * eligibility_integral_percent;
eligible_phis = phis(eligible_indexes);

%find the phase for which the different imaginary parts are minimal
if (sign == 1)
    [bestPhasedSpectrumIm, ii_PhasedSpectrumIm] = min(elibilityPhasedSpectrumIm(eligible_indexes));
    [bestDePhasedSpectrumIm, ii_DePhasedSpectrumIm] = min(elibilityDePhasedSpectrumIm(eligible_indexes));
else
    [bestPhasedSpectrumIm, ii_PhasedSpectrumIm] = min(elibilityPhasedSpectrumIm(eligible_indexes));
    [bestDePhasedSpectrumIm, ii_DePhasedSpectrumIm] = min(elibilityDePhasedSpectrumIm(eligible_indexes));
end

%calculate the phases coresponding to all the calculated indexes
phi_integral_re = phis(ii_eligibility_integral_re)*180/pi;
phi_phased_im = phis(ii_PhasedSpectrumIm)*180/pi;
phi_dephased_im = phis(ii_DePhasedSpectrumIm)*180/pi;
phaseWholeSpectrum = (phi_integral_re + phi_phased_im + phi_dephased_im) / 3;

if verbose
    fprintf('Phases eligibility (whole spectrum):\n')
    fprintf('real integral\tPhased imaginary\tDephased imaginary\t Mean\n');
    fprintf('%4.3g\t\t%4.3g\t\t%4.3g\t\t%4.3g\n\n',...
        phi_integral_re,phi_phased_im,...
        phi_dephased_im,phaseWholeSpectrum);
end
if plt
    figure(41);
    plot(phis*180/pi, elibility_integral_re/abs(max(elibility_integral_re)));
    hold on
    plot(phis*180/pi, elibilityPhasedSpectrumIm/abs(max(elibilityPhasedSpectrumIm)));
    plot(phis*180/pi, elibilityDePhasedSpectrumIm/abs(max(elibilityDePhasedSpectrumIm)));
    plot(eligible_phis*180/pi, eligible_phis ./ eligible_phis * maxValue_eligibility_integral_re/abs(max(elibility_integral_re))/2);
    legend('Real integral', 'Phased Im', 'DePhased Im');
    hold off
end

%% calculate 1st order phase correction using the singlets
if (useSinglets)
    if verbose
        fprintf('Phases singlets:\n')
        fprintf('ppm\tmax\tint\tint neg\tmin neg\tmean\n');
    end
        
    %select centrum of spectral indices in dim 1
    ind1 = find(((ax1*bw(1))>-wi_roi1/2) & ((ax1*bw(1))<wi_roi1/2));
    
    spec_singlets = spec(ind1,:);
    %update the spectrum to include the phasing from the eligibility criteria
    specPhasedGlobal = spec_singlets .*(ones(size(spec_singlets,1),1)* exp(-1i*phaseWholeSpectrum/180*pi*ax2_dt));
    %update the eligible phis based on the changed spectrum
    eligible_phis = eligible_phis - phaseWholeSpectrum/180*pi;
    
    for l=1:ls %run for all singlets
        %select the indices around the singlets
        ind2 = find(((ax2*bw(2)+zp2)>sing(l)-wi_roi2/2) & ...
            ((ax2*bw(2)+zp2)<sing(l)+wi_roi2/2));
        
        spec_roi = specPhasedGlobal(:,ind2);
        ax2_dt_roi = ax2_dt(ind2);
        ma = [];
        in = [];
        ineg = [];
        mineg = [];
        integral_im = [];
        for phi=eligible_phis
            % iterating is unelegant methods for some:
            %  could be calculated through combintation of real and
            %  imaginary part
            spec_roi_phased = spec_roi.*(ones(size(spec_roi,1),1)* exp(-1i*phi*ax2_dt_roi));
            spec_roi_phased_real = real(spec_roi_phased);
            spec_roi_phased_imag = imag(spec_roi_phased);
            
            % 1: determine maximum
            ma = [ma max(spec_roi_phased_real(:))];
            % 2: determine integral of whole peak
            in = [in sum(spec_roi_phased_real(:))];
            
            % 5: determine minimal imaginary part
            integral_im = [integral_im abs(sum(spec_roi_phased_imag(:)))];
            % 5: integral of whole peak = 0 => phase+-pi/2
            % 6: minimum=maximum => phase+-pi/2
            
        end
        [yy_max,ii_max] = max(ma);
        
        [yy,ii_int] = max(in);
                
        [yy,ii_integral_imag] = min(integral_im);
        
        % different phases in [deg] scaling also back from [rad]
        pc_all = [eligible_phis(ii_max) eligible_phis(ii_int) ...
            eligible_phis(ii_integral_imag)] *180/pi ;
        pcs(l) = mean(pc_all);
        
        if verbose
            fprintf('%g\t%0.3g\t%0.3g\t%0.3g\t%0.3g\n',...
                sing(l),pc_all(1),pc_all(2),...
                pc_all(3),pcs(l));
        end
        
        % plotting
        if plt
            figure(31);
            cmax = log(max(ma)+1)/log(2);
            siroi = size(spec_roi);
            
            subplot(length(sing)+1,2,2*l-1);
            plot2d_log(real(spec_roi.*(ones(size(spec_roi,1),1)*...
                exp(-1i*pcs(l)/180*pi*ax2_dt_roi))));
            hold on;
            plot([1 siroi(2)],[siroi(1)/2 siroi(1)/2],'g:',...
                [siroi(2)/2 siroi(2)/2],[1 siroi(1)],'g:');
            hold off;
            if (cmax < 0)
                cmax = -cmax;
            end
            caxis([-cmax cmax]); 
            colorbar; 
            colormap(cmap_dark);
            title([num2str(sing(l)) ' ppm: pc1 ' num2str(pcs(l))]);
            
            subplot(length(sing)+1,2,2*l);
            plot(eligible_phis*180/pi,ma/abs(max(ma)),'b',...
                eligible_phis*180/pi,in/abs(max(in)),'r',...
                eligible_phis*180/pi,integral_im/abs(max(integral_im)),'y');
            xlabel('b: max; r: integral re; m: integral im');
            axis tight;
        end
    end
    
    pc1 = mean(pcs) + phaseWholeSpectrum;
else
    pc1 = phaseWholeSpectrum;
end
end