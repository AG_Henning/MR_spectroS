  function varargout = plot_spect(varargin)
% PLOT_SPECT MATLAB code for plot_spect.fig
%      PLOT_SPECT, by itself, creates a new PLOT_SPECT or raises the existing
%      singleton*.
%
%      H = PLOT_SPECT returns the handle to a new PLOT_SPECT or the handle to
%      the existing singleton*.
%
%      PLOT_SPECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOT_SPECT.M with the given input arguments.
%
%      PLOT_SPECT('Property','Value',...) creates a new PLOT_SPECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before plot_spect_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to plot_spect_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help plot_spect

% Last Modified by GUIDE v2.5 21-Aug-2018 10:25:38

% Begin initialization code - DO NOT EDIT


%values_obj= values();
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @plot_spect_OpeningFcn, ...
                   'gui_OutputFcn',  @plot_spect_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
              
               
              
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT




%//////////////////////////////////////////////////////////////////////////
%             Executes just before plot_spect is made visible.
%    function plot_spect_OpeningFcn(hObject, eventdata, handles, varargin)
%//////////////////////////////////////////////////////////////////////////

% --- Executes just before plot_spect is made visible.
function plot_spect_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to plot_spect (see VARARGIN)

% Choose default command line output for plot_spect
handles.output = hObject;


%//////////////////////////////////////////////////////////////////////////
%                  ADD CLASSES AND FUNCTIONS FOLDERS 
%//////////////////////////////////////////////////////////////////////////
str1             = mfilename;
str2             = mfilename('fullpath');
directory        = str2(1:length(str2)-length(str1));
pathClasses      = strcat(directory,'Classes');
pathFunctions    = strcat(directory,'Functions');
addpath(pathClasses,pathFunctions);

%//////////////////////////////////////////////////////////////////////////
%                        INITIALIZATION OF HANDLES 
%//////////////////////////////////////////////////////////////////////////

n                                     = 8                        ;
handles.MRSpectroMatrix               = MRSpectroMatrix(n)       ;
handles.DisplayProperties             = DisplayProperties        ;
handles.SelectedProcessingValues      = SelectedProcessingValues ;
handles.SelectedDisplayValues         = SelectedDisplayValues    ;


%//////////////////////////////////////////////////////////////////////////
%                              IMAGE GROUP
%//////////////////////////////////////////////////////////////////////////
axes(handles.axes_image);
image=imread('image.png');
imagesc(image);
axis(gca,'off');

updateDisplayProperties(handles);   


%//////////////////////////////////////////////////////////////////////////
%                IF A MR_SPECTRO IS GIVEN AS AN INPUT
%//////////////////////////////////////////////////////////////////////////
cla(handles.axes_plot,'reset')
if nargin==4
    inputParameter=varargin{1};
    if ~isa(inputParameter,'MR_spectroS')
        msgbox('The input is not a MR_spectroS object', 'Error','error');
    else
        
        handles.MRSpectroMatrix.Save(inputParameter);
        handles.DisplayProperties.setDisplayProperties(handles);
        displaySpect(handles);
    end
end



%Update handles structure
guidata(hObject, handles);

% UIWAIT makes plot_spect wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = plot_spect_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;





%##########################################################################
%                          SLIDERS
%##########################################################################

% --- Executes on slider movement.
function slider_coils_Callback(hObject, eventdata, handles)
% hObject    handle to slider_coils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_coil=get(hObject,'Value');
set(handles.edit_coils,'String',num2str(current_coil));
set(handles.SelectedDisplayValues,'selectedCoil',current_coil);
displaySpect(handles);




% --- Executes on slider movement.
function slider_dyns_Callback(hObject, eventdata, handles)
% hObject    handle to slider_dyns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_dyn= int16( get(hObject,'Value'));
set(handles.edit_dyns,'String',num2str(current_dyn));
set(handles.SelectedDisplayValues,'selectedDyn',current_dyn);
displaySpect(handles);


% --- Executes on slider movement.
function slider_means_Callback(hObject, eventdata, handles)
% hObject    handle to slider_means (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_mean=get(hObject,'Value');
set(handles.edit_means,'String',num2str(current_mean));
set(handles.SelectedDisplayValues,'selectedAverage',current_mean);

toggleToolPlotOnOff_ClickedCallback(handles.toggleToolPlotOnOff,[],handles)

displaySpect(handles);




%##########################################################################
%                                EDITS
%##########################################################################

function edit_coils_Callback(hObject, eventdata, handles)
% hObject    handle to edit_coils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_coil=str2double(get(hObject,'String'));
set(handles.slider_coils,'Value',current_coil);
set(handles.SelectedDisplayValues,'selectedCoil',current_coil);
displaySpect(handles);


function edit_means_Callback(hObject, eventdata, handles)
% hObject    handle to edit_means (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_mean=str2double(get(hObject,'String'));
set(handles.slider_means,'Value',current_mean);
set(handles.SelectedDisplayValues,'selectedAverage',current_mean);
displaySpect(handles);

function edit_dyns_Callback(hObject, eventdata, handles)
% hObject    handle to edit_dyns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_dyn=str2double(get(hObject,'String'));
set(handles.slider_dyns,'Value',current_dyn);
set(handles.SelectedDisplayValues,'selectedDyn',current_dyn);
displaySpect(handles);


function edit_readout_Callback(hObject, eventdata, handles)
% hObject    handle to edit_readout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_readout as text
%        str2double(get(hObject,'String')) returns contents of edit_readout as a double



function edit_bandwidth_Callback(hObject, eventdata, handles)
% hObject    handle to edit_bandwidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_bandwidth as text
%        str2double(get(hObject,'String')) returns contents of edit_bandwidth as a double



function edit_averaging_Callback(hObject, eventdata, handles)
% hObject    handle to edit_averaging (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.SelectedDisplayValues,'selectedAveRange',get(hObject,'String'));

   


function edit_zero_Callback(hObject, eventdata, handles)
% hObject    handle to edit_zero (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_zero as text
%        str2double(get(hObject,'String')) returns contents of edit_zero as a double
set(handles.SelectedDisplayValues,'selectedZeroPoint',str2double(get(hObject,'String')));
displaySpect(handles);





%##########################################################################
%                            BUTTONS
%##########################################################################


% --- Executes on button press in button_show.
function button_show_Callback(hObject, eventdata, handles)
% hObject    handle to button_show (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


displaySpect(handles)




  % --- Executes on button press in button_plotsnr.
function button_plotsnr_Callback(hObject, eventdata, handles)
% hObject    handle to button_plotsnr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%plot_snr(handles);
%handles.SelectedDisplayValues



% --- Executes on button press in pushbutton_noise_corr.
function pushbutton_noise_corr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_noise_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

noiseData   =  handles.MRSpectroMatrix;
noiseData   =  noiseData.getCurrent;
noiseData   =  noiseData.Data{1};
noiseData   =  squeeze(noiseData(4096-1000:4096, :, :, :, :, :, :, :, 2, :, :, :));
noise_correlation( noiseData );  


% --- Executes on button press in pushbutton_hsvd.
function pushbutton_hsvd_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_hsvd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



%//////////////////////////////////////////////////////////////////////////
%                             CHECKBOX 
%//////////////////////////////////////////////////////////////////////////

% --- Executes on button press in checkbox_averaging.
function checkbox_averaging_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_averaging (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_averaging

if get(hObject,'Value')
    set(handles.SelectedDisplayValues,'selectedAveraging', true)
    set(handles.edit_averaging,'Enable','on')
else
    set(handles.edit_averaging,'Enable','inactive')
    set(handles.SelectedDisplayValues,'selectedAveraging', false)
end



%//////////////////////////////////////////////////////////////////////////
%                             POPUPMENUS
%//////////////////////////////////////////////////////////////////////////

% --- Executes on selection change in popupmenu_mode.
function popupmenu_mode_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_mode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

selectedModeString = get(hObject,'String');
selectedMode = get(hObject,'Value');
set(handles.SelectedDisplayValues,'selectedMode',selectedModeString{selectedMode});
displaySpect(handles)



   % --- Executes on selection change in popupmenu_mix.
function popupmenu_mix_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_mix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
currentValue = get(hObject,'Value');
set(handles.SelectedDisplayValues,'selectedMix',currentValue);
displaySpect(handles)


% --- Executes on selection change in popupmenu_type.
function popupmenu_type_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_type

selectedTypeString = get(hObject,'String');
selectedType = get(hObject,'Value');
set(handles.SelectedDisplayValues,'selectedType',selectedTypeString{selectedType});
displaySpect(handles)


%//////////////////////////////////////////////////////////////////////////
%                   TOOLBAR BUTTONS
%//////////////////////////////////////////////////////////////////////////

function uipushtool2_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


[filename, pathname, FilterIndex] = uiputfile( ...
{'*.fig','MATLAB figure (*.fig)';...
 '*.jpg', 'JPEG image (*.jpg)';...
 '*.bmp','Bitmap file (*.bmp)';...
 '*.png','Portable Network Graphics file (*.png)'},...
 'Save figure as');

handles.figure1;
filepath=strcat(pathname,filename);
h_axes=handles.axes_plot;
new_handles=figure;
set(new_handles,'Visible','off');
copyobj(h_axes,new_handles);
new_axes=get(new_handles,'CurrentAxes');
set(new_axes,'Units', 'normalized', 'OuterPosition', [0,0,1,1]);

switch FilterIndex
    case 1
        set(new_handles,'Visible','on');
        saveas(new_handles,filepath);
        close(new_handles)
    case 2
        print(new_handles,'-djpeg ',filepath);
    case 3
        print(new_handles,'-dbmp ',filepath);
    case 4
        print(new_handles,'-dpng',filepath);
    otherwise
        %print(new_handles,filepath);
end
        



% --------------------------------------------------------------------
function uipushtoolBack_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtoolBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.MRSpectroMatrix.Back
handles.DisplayProperties.setDisplayProperties(handles);
displaySpect(handles)

% --------------------------------------------------------------------
function uipushtoolForward_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtoolForward (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.MRSpectroMatrix.Forward
handles.DisplayProperties.setDisplayProperties(handles);
displaySpect(handles)



% --------------------------------------------------------------------
function toggleToolLockDim_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toggleToolLockDim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if strcmp ( get(hObject, 'State'), 'on')
 
    XLim          = get(gca, 'XLim');
    YLim          = get(gca, 'YLim');

    set(handles.DisplayProperties,'xRange',XLim);
    set(handles.DisplayProperties,'yRange',YLim);
    set(handles.DisplayProperties,'dimRangeLock',true);
else 
    set(handles.DisplayProperties,'dimRangeLock',false);   
end


% --------------------------------------------------------------------
function toggleToolPlotOnOff_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toggleToolPlotOnOff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MR_spectroSObj     = handles. MRSpectroMatrix.getCurrent;
if strcmp( get(hObject,'State'),'on')
    
        
        if MR_spectroSObj.Parameter.ReconFlags.isfMRSAveraged
            
            set(handles.DisplayProperties,'plotOnOff',true)
            selAve = get(handles.SelectedDisplayValues,'selectedAverage');
            switch selAve
                case 1
                    set(handles.DisplayProperties,'secondAverage',2)
                otherwise
                    set(handles.DisplayProperties,'secondAverage',1)
            end
            
          
        else
            
          warning(' ')
          warning('MR_spectroSObj.Parameter.ReconFlags.isfMRSAveraged == false')
          warning('Nothing to do...')
          
          
        end
       
    
else
       set(handles.DisplayProperties,'plotOnOff',false)
       set(handles.DisplayProperties,'secondAverage',[])
end
displaySpect(handles);


%##########################################################################
%                             MENUS
%##########################################################################


%//////////////////////////////////////////////////////////////////////////
%                          FILE MENU
%//////////////////////////////////////////////////////////////////////////

% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Open_Callback(hObject, eventdata, handles)
% hObject    handle to Open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --------------------------------------------------------------------
function openRaw_Callback(hObject, eventdata, handles)
% hObject    handle to openRaw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MRSpectroObj= MR_spectroS;
handles.MRSpectroMatrix.Save(MRSpectroObj);
handles.DisplayProperties.setDisplayProperties(handles);
displaySpect(handles);

% --------------------------------------------------------------------
function openMFile_Callback(hObject, eventdata, handles)
% hObject    handle to openMFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[FileName,PathName] = uigetfile({'*.mat; *.MAT','MAT files (*.mat)'},'Select the M-file');
path                = strcat(PathName,FileName);
inputMFile          = load(path);
field               = fieldnames(inputMFile);
MRSpectroObj        = getfield(inputMFile,field{1});

if ~isa(MRSpectroObj,'MR_spectroS')
    error('The input is not a MR_spectroS object!!!')
end

handles.MRSpectroMatrix.Save(MRSpectroObj);
handles.DisplayProperties.setDisplayProperties(handles);
displaySpect(handles);
% --------------------------------------------------------------------
function openMruiText_Callback(hObject, eventdata, handles)
% hObject    handle to openMFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[FileName,PathName] = uigetfile({'*.txt; *.mrui'},'Select the MRUI-file');

MRSpectroObj= MR_spectroS(0,0); % create an empty object
MRSpectroObj = MRSpectroObj.ImportMruiText(PathName, FileName);

handles.MRSpectroMatrix.Save(MRSpectroObj);
handles.DisplayProperties.setDisplayProperties(handles);
displaySpect(handles);


% --------------------------------------------------------------------
function Export_Callback(hObject, eventdata, handles)
% hObject    handle to Export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function LCModel_Callback(hObject, eventdata, handles)
% hObject    handle to LCModel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles. MRSpectroMatrix.getCurrent;
this.ExportLcmRaw


% --------------------------------------------------------------------
function exportMFile_Callback(hObject, eventdata, handles)
% hObject    handle to exportMFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MR_spectroSObj     = handles. MRSpectroMatrix.getCurrent;
filename           = MR_spectroSObj.Parameter.Filename;
filename           = filename(1:end-4);
try
    
    prompt    = {'Name of the MAT-file:'};
    dlg_title = 'Export to MAT-file';
    num_lines = [1 50];
    def       = {filename};
    answer    = inputdlg(prompt,dlg_title,num_lines,def);
    answer    = answer{1};

  v = genvarname(answer);
  eval([v '=MR_spectroSObj;']);
  save(answer,v);
catch err
    
    display(err)
end


% --------------------------------------------------------------------
function sDatFile_Callback(hObject, eventdata, handles)
% hObject    handle to sDatFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

MR_spectroSObj     = handles. MRSpectroMatrix.getCurrent;
filename           = MR_spectroSObj.Parameter.Filename;
filename           = filename(1:end-4);
sparfile           = 'sparfile.spar';
par                = read_spar(sparfile);
this               = MR_spectroSObj;
data               = this.Data{1};
if this.Parameter.ReconFlags.isJresolved == false
    par.samples                = size(data,this.kx_dim);
    par.averages               = size(data,this.meas_dim);
    par.dim1_pnts              = size(data,this.kx_dim);
    par.rows                   = 1;
    par.dim1_low_val           = 1;
    data                       = data(:,1);
    data                       = squeeze(data);
else % J resolved
    par.samples                = size(data,this.kx_dim);
    par.averages               = 1;
    par.dim1_pnts              = size(data,this.kx_dim);
    par.rows                   = size(data,this.meas_dim);
    par.dim1_low_val           = 1;
    data                       = conj(squeeze(data)');
end
par.echo_time              = this.Parameter.Headers.TE_us/1000;
    
par.spectrum_echo_time     = this.Parameter.Headers.TE_us/1000;
par.synthesizer_frequency  = this.Parameter.Headers.ScanFrequency;
par.sample_frequency       = this.Parameter.Headers.Bandwidth_Hz;



write_sdat(strcat(filename,'.sdat'),data,par);





% --------------------------------------------------------------------
function exportRawData_Callback(hObject, eventdata, handles)
% hObject    handle to exportRawData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this     = handles. MRSpectroMatrix.getCurrent;

prompt    = {'Name of the COORD-file:', 'PPM range'};
dlg_title = 'Export to COORD-file';
num_lines = 1;
defaultans = {'_COORD','0.6 4.2'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

freq    = this.Parameter.Headers.ScanFrequency;
BW      = this.Parameter.Headers.Bandwidth_Hz;
nPoints = size(this.Data{1},1);
ppmRange = linspace(-BW*1e+6/2/freq+4.7,BW*1e+6/2/freq+4.7,nPoints);
ppmRes   = BW*1e+6/freq/nPoints;


ppmIndex = str2num(answer{2});

indexUp = find(ppmRange<ppmIndex(2)+ppmRes & ppmRange>ppmIndex(2)-ppmRes);
indexUp = indexUp(2);
indexDown = find(ppmRange<ppmIndex(1)+ppmRes & ppmRange>ppmIndex(1)-ppmRes);
indexDown = indexDown(1);


nAve = handles.SelectedDisplayValues.selectedAverage;
nMix = handles.SelectedDisplayValues.selectedMix;

data = this.Data{1};
data = squeeze(data(:,1,1,1,1,1,1,1,nMix,1,1,nAve));
data = fftshift(fft(data));

ppmAxis = ppmRange(indexDown:indexUp)';
fReal   = real(data(indexDown:indexUp));
%fReal   = fReal/max(fReal); % to avoid division with water residuals 

filename = strcat(answer{1},'_',num2str(ppmIndex(1)),'to',num2str(ppmIndex(2)),'.mat');
save(filename,'ppmAxis','fReal');

% --------------------------------------------------------------------
function exportMruiText_Callback(hObject, eventdata, handles)
% hObject    handle to exportMFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this     = handles. MRSpectroMatrix.getCurrent;
filename           = this.Parameter.Filename;
filename           = filename(1:end-4);
try
    prompt    = {'Name of the MRUI-file:'};
    dlg_title = 'Export to MRUI-file';
    num_lines = [1 50];
    def       = {filename};
    answer    = inputdlg(prompt,dlg_title,num_lines,def);
    answer    = answer{1};

    this.ExportMruiText([pwd, '\'], answer);
catch err
    display(err)
end

% --------------------------------------------------------------------
function Exit_Callback(hObject, eventdata, handles)
% hObject    handle to Exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


delete(handles.MRSpectroMatrix)
close

%//////////////////////////////////////////////////////////////////////////
%                       FILTERING MENU      
%//////////////////////////////////////////////////////////////////////////

% --------------------------------------------------------------------
function Filtering_Callback(hObject, eventdata, handles)
% hObject    handle to Filtering (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --------------------------------------------------------------------
function VoigtAnalysis_Callback(hObject, eventdata, handles)
% hObject    handle to VoigtAnalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
voigtAnalysis(handles);


% --------------------------------------------------------------------
function Voigt_Callback(hObject, eventdata, handles)
% hObject    handle to Voigt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filtering(handles);



% --------------------------------------------------------------------
function Truncate_Callback(hObject, eventdata, handles)
% hObject    handle to Truncate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try 
    this       = handles.MRSpectroMatrix.getCurrent;
    truncPoint = inputdlg('Enter the truncation time (ms):',...
                 'Truncation', [1 30]);
    truncPoint = floor( str2num(truncPoint{:})/(this.Parameter.Headers.DwellTimeSig_ns*1e-6)); 
    this       = this.Truncate(truncPoint);
    handles.MRSpectroMatrix.Save(this);
    handles.DisplayProperties.setDisplayProperties(handles);
    displaySpect(handles);

catch err
    
end

% --------------------------------------------------------------------
function hsvd_Callback(hObject, eventdata, handles)
% hObject    handle to hsvd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hsvd_water(handles);


% --------------------------------------------------------------------
function smoothing_Callback(hObject, eventdata, handles)
% hObject    handle to smoothing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    smoothing(handles)



%//////////////////////////////////////////////////////////////////////////
%                           PHASE CORRECTION MENU
%//////////////////////////////////////////////////////////////////////////


% --------------------------------------------------------------------
function Phase_Callback(hObject, eventdata, handles)
% hObject    handle to Phase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function ph0RemovalMenu_Callback(hObject, eventdata, handles)
% hObject    handle to ph0RemovalMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
this = this.phase0Removal;

handles.MRSpectroMatrix.Save(this);

handles.DisplayProperties.setDisplayProperties(handles);
displaySpect(handles);



% --------------------------------------------------------------------
function Phase2_Callback(hObject, eventdata, handles)
% hObject    handle to Phase2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
phase_correction(handles);







%//////////////////////////////////////////////////////////////////////////
%                         SETTINGS MENU                   
%//////////////////////////////////////////////////////////////////////////


% --------------------------------------------------------------------
function Settings_Callback(hObject, eventdata, handles)
% hObject    handle to Settings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function DisplaySetting_Callback(hObject, eventdata, handles)
% hObject    handle to DisplaySetting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --------------------------------------------------------------------
function NoiseSettings_Callback(hObject, eventdata, handles)
% hObject    handle to NoiseSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
noiseSettings(handles);


    



%//////////////////////////////////////////////////////////////////////////
%                       ADVANCED SETTINGS MENU
%//////////////////////////////////////////////////////////////////////////

% --------------------------------------------------------------------

% --------------------------------------------------------------------
function zeroFillingMenu_Callback(hObject, eventdata, handles)
% hObject    handle to zeroFillingMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
this = this.ZeroFilling;
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);


% --------------------------------------------------------------------
function freqAlignementMenu_Callback(hObject, eventdata, handles)
% hObject    handle to freqAlignementMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
frequencyAlignment(handles)


% --------------------------------------------------------------------
function rescaleMenu_Callback(hObject, eventdata, handles)
% hObject    handle to rescaleMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
this = this.Rescale;
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);



% --------------------------------------------------------------------
function reconMenu_Callback(hObject, eventdata, handles)
% hObject    handle to reconMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
this = this.ReconData;
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);



% --------------------------------------------------------------------
function eddyCurrCorrMenu_Callback(hObject, eventdata, handles)
% hObject    handle to eddyCurrCorrMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
nMixes = size(this.Data{1},this.mix_dim);
if (nMixes ==1)
    [FileName,PathName] = uigetfile({'*.dat'},'Select the Water Suppressed data, cancel if Eddy Current Correction with own phase');
    if (FileName ~= 0)
        path = strcat(PathName,FileName);
        tmpData      = mapVBVD(path);
        %############################################################
        % Order of raw data:
        %  1) Columns
        %  2) Channels/Coils
        %  3) Lines
        %  4) Partitions
        %  5) Slices
        %  6) Averages
        %  7) (Cardiac-) Phases
        %  8) Contrasts/Echoes
        %  9) Repetitions
        % 10) Sets
        %#############################################################       
        tmpWaterReferenceData = tmpData.image(''); %Read the data
        waterReferenceData = double(permute(tmpWaterReferenceData,[1 3 4 2 5 7 8 11 12 9 10 6]));
        clear tmpData tmpWaterReferenceData;    
        this = this.EddyCurrentCorrection(waterReferenceData);
    else %do Eddy current corection without water reference data      
        this = this.EddyCurrentCorrection();
    end
else
    this = this.EddyCurrentCorrection();
end
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);



% --------------------------------------------------------------------
function averagingMenu_Callback(hObject, eventdata, handles)
% hObject    handle to averagingMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
averaging(handles)


% --------------------------------------------------------------------
function coilCombinationMenu_Callback(hObject, eventdata, handles)
% hObject    handle to coilCombinationMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
this = this.CombineCoils;
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);



% --------------------------------------------------------------------
function scaleDataMenu_Callback(hObject, eventdata, handles)
% hObject    handle to scaleDataMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
this = this.ScaleData();
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);


% --------------------------------------------------------------------
function moreMenu2_Callback(hObject, eventdata, handles)
% hObject    handle to moreMenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function denoisingMenu_Callback(hObject, eventdata, handles)
% hObject    handle to denoisingMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
this = this.Denoise;
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);

% --------------------------------------------------------------------
function deleteCoilsMenu_Callback(hObject, eventdata, handles)
% hObject    handle to deleteCoilsMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
deleteCoilChannels(handles)





%//////////////////////////////////////////////////////////////////////////
%                       fMRS ANALYSIS MENU
%//////////////////////////////////////////////////////////////////////////


% --------------------------------------------------------------------
function setParadigmMenu_Callback(hObject, eventdata, handles)
% hObject    handle to setParadigmMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

this = handles.MRSpectroMatrix.getCurrent;

if isempty(this.Parameter.fMRSAveSettings.paradigm)
    prompt = {'Paradigm matrix:'};
    dlg_title = 'Set paradigm matrix';
    num_lines = 1;
    def = {''};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    if ~isempty(answer)
        answer = str2num(answer{:});
        this.Parameter.fMRSAveSettings.paradigm = answer;
        handles.MRSpectroMatrix.Save(this);
        choice = 'See it'; 
    else
        choice = 'Cancel';
    end
else
    % Construct a questdlg with three options
     choice = questdlg('A paradigm has been already set, what do you want?', ...
	'Set paradigm', ...
	'See it','Reset it','Cancel','See it');
    answer = this.Parameter.fMRSAveSettings.paradigm;
end

switch choice
    case 'See it'
        %hOldFig = gcf; 
        hFig = figure;
        hAxes =  get(hFig,'CurrentAxes');
        ave = 0 ;
        for i=1:2:length(answer)
            if answer(i)==1
                bar(1+ave:answer(i+1)+ave,ones(1,answer(i+1)),'r')
            elseif answer(i)==0
                bar(1+ave:answer(i+1)+ave,ones(1,answer(i+1)),'b')
            else
                 bar(1+ave:answer(i+1)+ave,ones(1,answer(i+1)),'k')
            end
            ave = ave + answer(i+1);
            hold on
        end
        title('Red = ON  Blue = OFF, Black= Discarded');
      
    case 'Reset it'
        this.Parameter.fMRSAveSettings.paradigm = [];
        handles.MRSpectroMatrix.Save(this);
        setParadigmMenu_Callback(hObject, eventdata, handles)
      
    case 'Cancel'
        
end
        
        
 
% --------------------------------------------------------------------
function boldEffectMenu_Callback(hObject, eventdata, handles)
% hObject    handle to boldEffectMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
boldEffectGui( handles )

% --------------------------------------------------------------------
function fMRSAveragingMenu_Callback(hObject, eventdata, handles)
% hObject    handle to fMRSAveragingMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

this = handles.MRSpectroMatrix.getCurrent;
this = this.fMRSAverage;
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);


% --------------------------------------------------------------------
function movingAverage_Callback(hObject, eventdata, handles)
% hObject    handle to movingAverage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

prompt = {'Number of averages:','Number of shifted averages:'};
dlg_title = 'Input';
num_lines = 1;
defaultans = {'',''};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);


if ~isempty(answer)  
    this = handles.MRSpectroMatrix.getCurrent;
    this.Parameter.fMRSAveSettings.numberOfAverages = str2num(answer{1});
    this.Parameter.fMRSAveSettings.shiftedAverages  = str2num(answer{2});
    this = fMRSMovingAverage( this );
    handles.MRSpectroMatrix.Save(this);
    handles.DisplayProperties.setDisplayProperties(handles);
end



% --------------------------------------------------------------------
function boldCorr_Callback(hObject, eventdata, handles)
% hObject    handle to boldCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


this = handles.MRSpectroMatrix.getCurrent;

if ~this.Parameter.ReconFlags.isfMRSAveraged
  warning('The data have to be fMRSAveraged first!')
else
     boldCorrection(handles);
end

% --------------------------------------------------------------------
function diffSpectrumMenu_Callback(hObject, eventdata, handles)
% hObject    handle to diffSpectrumMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt     = {'Indeces region:','Linewidth broading(Hz)'};
dlg_title  = 'fMRS Difference settings';
num_lines  = 1;
def        = {'',''};
answer     = inputdlg(prompt,dlg_title,num_lines,def);
answer1    = str2num(answer{1,:});
answer2    = str2num(answer{2,:});
this           = handles.MRSpectroMatrix.getCurrent;
this.Parameter.fMRSDiffSettings.indecesRegion   = answer1;
this.Parameter.fMRSDiffSettings.lorenzianFactor = answer2;
this       = this.fMRSDifferenceGUI;
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);



%//////////////////////////////////////////////////////////////////////////
%                       CSI MENU
%//////////////////////////////////////////////////////////////////////////

% --------------------------------------------------------------------
function CSIMenu_Callback(hObject, eventdata, handles)
% hObject    handle to CSIMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function plotCSIMenu_Callback(hObject, eventdata, handles)
% hObject    handle to plotCSIMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



%//////////////////////////////////////////////////////////////////////////
%                       MORE MENU
%//////////////////////////////////////////////////////////////////////////

% --------------------------------------------------------------------
function headersDispaly_Callback(hObject, eventdata, handles)
% hObject    handle to headersDispaly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

this    = handles.MRSpectroMatrix.getCurrent;
headers = this.Parameter.Headers;

fields         = fieldnames(headers);


maxLength = 0;
for i= 1:length(fields)
    if maxLength < length(fields{i})
        maxLength = length(fields{i});
    end
end
      
    

for index   = 1: length(fields)
    padding  = maxLength +4;
    argument = ['%-',num2str(padding),'s: %s'];
    tmpStr     = sprintf(argument,fields{index},num2str(getfield(headers,fields{index})));
    str{index} = tmpStr;
end

[selection ok] = listdlg('ListSize',[220 500],'PromptString','Headers','SelectionMode','single','ListString',str);


% --------------------------------------------------------------------
function ReconFlags_Callback(hObject, eventdata, handles)
% hObject    handle to ReconFlags (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
this.Parameter.ReconFlags



%//////////////////////////////////////////////////////////////////////////
%                       HELP MENU
%//////////////////////////////////////////////////////////////////////////


% --------------------------------------------------------------------
function separateWaterMenu_Callback(hObject, eventdata, handles)
% hObject    handle to separateWaterMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
this = this.SeparateWaterSpectra(16);
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);



% --------------------------------------------------------------------
function About_Callback(hObject, eventdata, handles)
% hObject    handle to About (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

about;



















%##########################################################################
%##########################################################################



% --- Executes during object creation, after setting all properties.
function slider_coils_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_coils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function slider_dyns_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_dyns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function slider_means_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_means (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function edit_coils_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_coils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit_dyns_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dyns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function popupmenu_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function edit_readout_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_readout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit_bandwidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_bandwidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function edit_zero_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_zero (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit_averaging_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_averaging (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_means_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_means (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function popupmenu_mode_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_mode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes during object creation, after setting all properties.
function popupmenu_mix_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_mix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Help1_Callback(hObject, eventdata, handles)
% hObject    handle to Help1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Help2_Callback(hObject, eventdata, handles)
% hObject    handle to Help2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)





% --------------------------------------------------------------------
function advancedProcessingMenu_Callback(hObject, eventdata, handles)
% hObject    handle to advancedProcessingMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --------------------------------------------------------------------
function dcOffserCorrMenu_Callback(hObject, eventdata, handles)
% hObject    handle to dcOffserCorrMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function alignWaterMenu_Callback(hObject,eventdata,handles)
% hObject    handle to dcOffserCorrMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
this = handles.MRSpectroMatrix.getCurrent;
this = this.AlignWaterToPpm;
handles.MRSpectroMatrix.Save(this);
handles.DisplayProperties.setDisplayProperties(handles);

function missingPointPredictionMenu_Callback(hObject,eventdata,handles)
% hObject    handle to dcOffserCorrMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
missingPointPred(handles)

% --------------------------------------------------------------------
function moreMenu_Callback(hObject, eventdata, handles)
% hObject    handle to moreMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function fMRSAnalysisMenu_Callback(hObject, eventdata, handles)
% hObject    handle to fMRSAnalysisMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

%if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
%    set(hObject,'BackgroundColor','white');
%end
