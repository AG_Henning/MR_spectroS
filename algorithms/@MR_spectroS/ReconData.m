function this = ReconData(this)
%##########################################################################
%
%function  ReconData(this)
% INPUTs: this:  
%
%##########################################################################

if ~this.Parameter.ReconFlags.isSorted
    error('The data has to  sorted first ')
end

if this.Parameter.ReconFlags.isDataRecon
    error('The data is already reconstructed')
end

%this.Parameter.Chunk.Def
%={'kx','ky','kz','coil','5','6','7','8','mix','dyn',11,'meas')
nAverages     = size(this.Data{1},this.meas_dim);
oldData       = this.Data{1};

%Metabolite Cycling
newSize= size(this.Data{1});
newSize(this.mix_dim)   = newSize(this.mix_dim)+1;
newSize(this.meas_dim)  = nAverages/2;
newData = zeros(newSize);
%      signM =  sign(real(oldData(1,1,1,1,1,1,1,1,1,1,1,1) * oldData(1,1,1,1,1,1,1,1,1,1,1,2)));

for indexAverage=1:2:nAverages
    newData(:,1,1,:,1,1,1,1,1,1,1,(indexAverage+1)/2)=...
        oldData(:,1,1,:,1,1,1,1,1,1,1,indexAverage)- oldData(:,1,1,:,1,1,1,1,1,1,1,indexAverage+1);

    newData(:,1,1,:,1,1,1,1,2,1,1,(indexAverage+1)/2)=...
        oldData(:,1,1,:,1,1,1,1,1,1,1,indexAverage)+ oldData(:,1,1,:,1,1,1,1,1,1,1,indexAverage+1);    
end

this.Data{1}                                           = newData;
this.Parameter.ReconFlags.isDataRecon                  = true;
this.Parameter.EddyCurrSettings.numberOfWaterSignals   = nAverages/2; %TODO check if this is needed!

end


                     
                     
            