function DoMARSSMetabolitesLineBroadening()

% filepath = 'D:\PAPER\1H_fMRS\Data\Basis_sets\Basis_sets_15x18x20_TE24ms_URef120V_Ref24ppm_LineBroadened';
filepath = 'D:\PAPER\1H_fMRS\Data\Basis_sets\MARSS_Output\sLASER_15x18x20_120V_Tx2.4ppm\RawBasis_for_MARSSinput';

metaboliteFileNames = {'PCr'};

plotBasis = true;

for indexMetabolite = 1 : length(metaboliteFileNames)
    filtered_metabolite_name = metaboliteFileNames{indexMetabolite};
    
    %read in the metabolites
    [oldData, bandwidth, scanFrequency, metaboliteName, singletPresentMix] = ...
        ImportLCModelBasisMARSS(filepath, metaboliteFileNames{indexMetabolite}, plotBasis, '1H');
    
    dwellTime = 1/bandwidth;
    t = (0:4095)*dwellTime;
    
    %Gaussian apodization after simulation
    %sensitivity and resolution enhancement (Keeler, ch.5)
    %weighting function:
    Fgau = 5;   %parameter that sets the decay rate
    L = -1;     %linewidth in Hz, L positive: line broadening, L negative: resolution enhancement
    W = exp(-t'*pi*L).*exp(- t'.^2*(Fgau)^2);
    newData = oldData.*W;
% newData = oldData;

    %add singlet and export
    addSinglet = true;
    dwellTimeMs = dwellTime * 1e3;
    
    filePathExport = [filepath '\Basis_sets_LineBroadened\'];
    mkdir(filePathExport)
    
    ExportLCModelBasis((newData/100)', dwellTimeMs, scanFrequency, filePathExport, ...
        filtered_metabolite_name, metaboliteFileNames{indexMetabolite}, addSinglet, ...
        false, 4.7, 1.5,'Yes',24,'jd_semiLASER');
    
    
    bw     = linspace(-bandwidth/2,bandwidth/2,length(newData));
    ppmVector    = bw/(scanFrequency) + 4.7;
         
    %plot filtered basis set on top of MARSS raw and save
    hold on
    plot(ppmVector,real(fftshift(fft(newData))))
%     xlabel('ppm');
%     xlim([min(ppmVector) max(ppmVector)])
%     set(gca,'xDir','reverse');
%     title(metaboliteName);
%     xlim([0 9])
    
    filename = [filePathExport metaboliteFileNames{indexMetabolite} '.fig'];
    savefig(filename);
end

end