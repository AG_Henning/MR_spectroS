function varargout = boldCorrection(varargin)
% BOLDCORRECTION MATLAB code for boldCorrection.fig
%      BOLDCORRECTION, by itself, creates a new BOLDCORRECTION or raises the existing
%      singleton*.
%
%      H = BOLDCORRECTION returns the handle to a new BOLDCORRECTION or the handle to
%      the existing singleton*.
%
%      BOLDCORRECTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BOLDCORRECTION.M with the given input arguments.
%
%      BOLDCORRECTION('Property','Value',...) creates a new BOLDCORRECTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before boldCorrection_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to boldCorrection_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help boldCorrection

% Last Modified by GUIDE v2.5 28-Mar-2017 13:33:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @boldCorrection_OpeningFcn, ...
                   'gui_OutputFcn',  @boldCorrection_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before boldCorrection is made visible.
function boldCorrection_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to boldCorrection (see VARARGIN)

% Choose default command line output for boldCorrection
handles.output = hObject;




%---------------Initialise the shown values, sliders etc-------------------
if nargin == 4
    handles.hInput= varargin{1};
end

thisOriginal  = handles.hInput.MRSpectroMatrix.getCurrent;
handles.hInput.MRSpectroMatrix.setTemporary(thisOriginal);

offset     = get(handles.hInput.SelectedProcessingValues,'boldOffset');
linewidth  = get(handles.hInput.SelectedProcessingValues,'boldLinewidth');
phase      = get(handles.hInput.SelectedProcessingValues,'boldPhase');
frequency  = get(handles.hInput.SelectedProcessingValues,'boldFrequency');


set(handles.sliderOffset,'Value',offset);
set(handles.editOffset,'String',num2str(offset));

set(handles.sliderPhase,'Value',phase);
set(handles.editPhase,'String',num2str(phase));

set(handles.sliderFrequency,'Value',frequency);
set(handles.editFrequency,'String',num2str(frequency));

set(handles.sliderLinewidth,'Value',linewidth);
set(handles.editLinewidth,'String',num2str(linewidth));




% Update handles structure
guidata(hObject, handles);

% UIWAIT makes boldCorrection wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = boldCorrection_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% Linewidth

% --- Executes on slider movement.
function sliderLinewidth_Callback(hObject, eventdata, handles)
% hObject    handle to sliderLinewidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider



linewidth  = get(handles.sliderLinewidth ,'Value');
phase      = get(handles.sliderPhase,'Value');
frequency  = get(handles.sliderFrequency,'Value');
offset     = get(handles.sliderOffset,'Value');

set(handles.hInput.SelectedProcessingValues,'boldLinewidth', linewidth);
set(handles.hInput.SelectedProcessingValues,'boldPhase', phase);
set(handles.hInput.SelectedProcessingValues,'boldFrequency', frequency);
set(handles.hInput.SelectedProcessingValues,'boldOffset',offset);


thisTemp                                            = handles.hInput.MRSpectroMatrix.getTemporary;

thisTemp.Parameter.fMRSDiffSettings.lorenzianFactor  = linewidth;
thisTemp.Parameter.fMRSDiffSettings.PhaseCorrection  = phase;
thisTemp.Parameter.fMRSDiffSettings.frequencyShift   = frequency;
thisTemp.Parameter.fMRSDiffSettings.OffsetCorrection = offset;

set(handles.editLinewidth,'String',linewidth);

thisTemp                                            = thisTemp.fMRSDifferenceGUI;

handles.hInput.MRSpectroMatrix.Save(thisTemp);
handles.hInput.DisplayProperties.setDisplayProperties(handles.hInput);
displaySpect(handles.hInput)



function editLinewidth_Callback(hObject, eventdata, handles)
% hObject    handle to editLinewidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editLinewidth as text
%        str2double(get(hObject,'String')) returns contents of editLinewidth as a double

linewidth = str2double(get(hObject,'String'));
set(handles.sliderLinewidth,'Value',linewidth);

linewidth  = get(handles.sliderLinewidth ,'Value');
phase      = get(handles.sliderPhase,'Value');
frequency  = get(handles.sliderFrequency,'Value');
offset     = get(handles.sliderOffset,'Value');

set(handles.hInput.SelectedProcessingValues,'boldLinewidth', linewidth);
set(handles.hInput.SelectedProcessingValues,'boldPhase', phase);
set(handles.hInput.SelectedProcessingValues,'boldFrequency', frequency);
set(handles.hInput.SelectedProcessingValues,'boldOffset',offset);


thisTemp                                            = handles.hInput.MRSpectroMatrix.getTemporary;

thisTemp.Parameter.fMRSDiffSettings.lorenzianFactor  = linewidth;
thisTemp.Parameter.fMRSDiffSettings.PhaseCorrection  = phase;
thisTemp.Parameter.fMRSDiffSettings.frequencyShift   = frequency;
thisTemp.Parameter.fMRSDiffSettings.OffsetCorrection = offset;


thisTemp                                            = thisTemp.fMRSDifferenceGUI;

handles.hInput.MRSpectroMatrix.Save(thisTemp);

handles.hInput.DisplayProperties.setDisplayProperties(handles.hInput);
displaySpect(handles.hInput)



%Phase 


% --- Executes on slider movement.
function sliderPhase_Callback(hObject, eventdata, handles)
% hObject    handle to sliderPhase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider



linewidth  = get(handles.sliderLinewidth ,'Value');
phase      = get(handles.sliderPhase,'Value');
frequency  = get(handles.sliderFrequency,'Value');
offset     = get(handles.sliderOffset,'Value');

set(handles.hInput.SelectedProcessingValues,'boldLinewidth', linewidth);
set(handles.hInput.SelectedProcessingValues,'boldPhase', phase);
set(handles.hInput.SelectedProcessingValues,'boldFrequency', frequency);
set(handles.hInput.SelectedProcessingValues,'boldOffset',offset);


thisTemp                                            = handles.hInput.MRSpectroMatrix.getTemporary;

thisTemp.Parameter.fMRSDiffSettings.lorenzianFactor  = linewidth;
thisTemp.Parameter.fMRSDiffSettings.PhaseCorrection  = phase;
thisTemp.Parameter.fMRSDiffSettings.frequencyShift   = frequency;
thisTemp.Parameter.fMRSDiffSettings.OffsetCorrection = offset;

set(handles.editPhase,'String',phase);

thisTemp                                            = thisTemp.fMRSDifferenceGUI;

handles.hInput.MRSpectroMatrix.Save(thisTemp);

handles.hInput.DisplayProperties.setDisplayProperties(handles.hInput);
displaySpect(handles.hInput)




function editPhase_Callback(hObject, eventdata, handles)
% hObject    handle to editPhase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editPhase as text
%        str2double(get(hObject,'String')) returns contents of editPhase as a double

phase = str2double(get(hObject,'String'));
set(handles.sliderPhase,'Value',phase);


linewidth  = get(handles.sliderLinewidth ,'Value');
phase      = get(handles.sliderPhase,'Value');
frequency  = get(handles.sliderFrequency,'Value');
offset     = get(handles.sliderOffset,'Value');

set(handles.hInput.SelectedProcessingValues,'boldLinewidth', linewidth);
set(handles.hInput.SelectedProcessingValues,'boldPhase', phase);
set(handles.hInput.SelectedProcessingValues,'boldFrequency', frequency);
set(handles.hInput.SelectedProcessingValues,'boldOffset',offset);


thisTemp                                            = handles.hInput.MRSpectroMatrix.getTemporary;

thisTemp.Parameter.fMRSDiffSettings.lorenzianFactor  = linewidth;
thisTemp.Parameter.fMRSDiffSettings.PhaseCorrection  = phase;
thisTemp.Parameter.fMRSDiffSettings.frequencyShift   = frequency;
thisTemp.Parameter.fMRSDiffSettings.OffsetCorrection = offset;


thisTemp                                            = thisTemp.fMRSDifferenceGUI;

handles.hInput.MRSpectroMatrix.Save(thisTemp);


handles.hInput.DisplayProperties.setDisplayProperties(handles.hInput);
displaySpect(handles.hInput)


%Frequency


% --- Executes on slider movement.
function sliderFrequency_Callback(hObject, eventdata, handles)
% hObject    handle to sliderFrequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

linewidth  = get(handles.sliderLinewidth ,'Value');
phase      = get(handles.sliderPhase,'Value');
frequency  = get(handles.sliderFrequency,'Value');
offset     = get(handles.sliderOffset,'Value');

set(handles.hInput.SelectedProcessingValues,'boldLinewidth', linewidth);
set(handles.hInput.SelectedProcessingValues,'boldPhase', phase);
set(handles.hInput.SelectedProcessingValues,'boldFrequency', frequency);
set(handles.hInput.SelectedProcessingValues,'boldOffset',offset);


thisTemp                                            = handles.hInput.MRSpectroMatrix.getTemporary;

thisTemp.Parameter.fMRSDiffSettings.lorenzianFactor  = linewidth;
thisTemp.Parameter.fMRSDiffSettings.PhaseCorrection  = phase;
thisTemp.Parameter.fMRSDiffSettings.frequencyShift   = frequency;
thisTemp.Parameter.fMRSDiffSettings.OffsetCorrection = offset;

set(handles.editFrequency,'String',frequency);

thisTemp                                            = thisTemp.fMRSDifferenceGUI;

handles.hInput.MRSpectroMatrix.Save(thisTemp);
handles.hInput.DisplayProperties.setDisplayProperties(handles.hInput);
displaySpect(handles.hInput)



function editFrequency_Callback(hObject, eventdata, handles)
% hObject    handle to editFrequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFrequency as text
%        str2double(get(hObject,'String')) returns contents of editFrequency as a double

frequency = str2double(get(hObject,'String'));
set(handles.sliderFrequency,'Value',frequency);

linewidth  = get(handles.sliderLinewidth ,'Value');
phase      = get(handles.sliderPhase,'Value');
frequency  = get(handles.sliderFrequency,'Value');
offset     = get(handles.sliderOffset,'Value');

set(handles.hInput.SelectedProcessingValues,'boldLinewidth', linewidth);
set(handles.hInput.SelectedProcessingValues,'boldPhase', phase);
set(handles.hInput.SelectedProcessingValues,'boldFrequency', frequency);
set(handles.hInput.SelectedProcessingValues,'boldOffset',offset);


thisTemp                                            = handles.hInput.MRSpectroMatrix.getTemporary;

thisTemp.Parameter.fMRSDiffSettings.lorenzianFactor  = linewidth;
thisTemp.Parameter.fMRSDiffSettings.PhaseCorrection  = phase;
thisTemp.Parameter.fMRSDiffSettings.frequencyShift   = frequency;
thisTemp.Parameter.fMRSDiffSettings.OffsetCorrection = offset;

thisTemp                                            = thisTemp.fMRSDifferenceGUI;

handles.hInput.MRSpectroMatrix.Save(thisTemp);

handles.hInput.DisplayProperties.setDisplayProperties(handles.hInput);
displaySpect(handles.hInput)


%Offset 




% --- Executes on slider movement.
function sliderOffset_Callback(hObject, eventdata, handles)
% hObject    handle to sliderOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
linewidth  = get(handles.sliderLinewidth ,'Value');
phase      = get(handles.sliderPhase,'Value');
frequency  = get(handles.sliderFrequency,'Value');
offset     = get(handles.sliderOffset,'Value');

set(handles.hInput.SelectedProcessingValues,'boldLinewidth', linewidth);
set(handles.hInput.SelectedProcessingValues,'boldPhase', phase);
set(handles.hInput.SelectedProcessingValues,'boldFrequency', frequency);
set(handles.hInput.SelectedProcessingValues,'boldOffset',offset);


thisTemp                                            = handles.hInput.MRSpectroMatrix.getTemporary;

thisTemp.Parameter.fMRSDiffSettings.lorenzianFactor  = linewidth;
thisTemp.Parameter.fMRSDiffSettings.PhaseCorrection  = phase;
thisTemp.Parameter.fMRSDiffSettings.frequencyShift   = frequency;
thisTemp.Parameter.fMRSDiffSettings.OffsetCorrection = offset;

set(handles.editOffset,'String',offset);

thisTemp                                            = thisTemp.fMRSDifferenceGUI;

handles.hInput.MRSpectroMatrix.Save(thisTemp);
handles.hInput.DisplayProperties.setDisplayProperties(handles.hInput);
displaySpect(handles.hInput)



function editOffset_Callback(hObject, eventdata, handles)
% hObject    handle to editOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editOffset as text
%        str2double(get(hObject,'String')) returns contents of editOffset as a double

offset = str2double(get(hObject,'String'));
set(handles.sliderOffset,'Value',offset);

linewidth  = get(handles.sliderLinewidth ,'Value');
phase      = get(handles.sliderPhase,'Value');
frequency  = get(handles.sliderFrequency,'Value');
offset     = get(handles.sliderOffset,'Value');

set(handles.hInput.SelectedProcessingValues,'boldLinewidth', linewidth);
set(handles.hInput.SelectedProcessingValues,'boldPhase', phase);
set(handles.hInput.SelectedProcessingValues,'boldFrequency', frequency);
set(handles.hInput.SelectedProcessingValues,'boldOffset',offset);


thisTemp                                            = handles.hInput.MRSpectroMatrix.getTemporary;

thisTemp.Parameter.fMRSDiffSettings.lorenzianFactor  = linewidth;
thisTemp.Parameter.fMRSDiffSettings.PhaseCorrection  = phase;
thisTemp.Parameter.fMRSDiffSettings.frequencyShift   = frequency;
thisTemp.Parameter.fMRSDiffSettings.OffsetCorrection = offset;

thisTemp                                            = thisTemp.fMRSDifferenceGUI;

handles.hInput.MRSpectroMatrix.Save(thisTemp);

handles.hInput.DisplayProperties.setDisplayProperties(handles.hInput);
displaySpect(handles.hInput)




%//////////////////////////////////////////////////////////////////////////
%                             BUTTONS
%//////////////////////////////////////////////////////////////////////////

% --- Executes on button press in autoButton.
function autoButton_Callback(hObject, eventdata, handles)
% hObject    handle to autoButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

linewidth  = get(handles.sliderLinewidth ,'Value');
phase      = get(handles.sliderPhase,'Value');
frequency  = get(handles.sliderFrequency,'Value');
indeces    = str2num( get(handles.editOffset,'String'));

thisTemp                                            = handles.hInput.MRSpectroMatrix.getTemporary;

thisTemp.Parameter.fMRSDiffSettings.lorenzianFactor = linewidth;
thisTemp.Parameter.fMRSDiffSettings.PhaseCorrection = phase;
thisTemp.Parameter.fMRSDiffSettings.frequencyShift  = frequency;
thisTemp.Parameter.fMRSDiffSettings.indecesRegion   = indeces;
thisTemp.Parameter.fMRSDiffSettings.Automatic       = true;

thisTemp                                            = thisTemp.fMRSDifferenceGUI;


linewidth = thisTemp.Parameter.fMRSDiffSettings.lorenzianFactor;
phase     = thisTemp.Parameter.fMRSDiffSettings.PhaseCorrection;
frequency = thisTemp.Parameter.fMRSDiffSettings.frequencyShift;

set(handles.sliderLinewidth,'Value',linewidth);
set(handles.sliderPhase,'Value',phase);
set(handles.sliderFrequency,'Value',frequency);


set(handles.editLinewidth,'String', num2str(linewidth));
set(handles.editPhase,'String', num2str(phase));
set(handles.editFrequency,'String',num2str(frequency));

handles.hInput.MRSpectroMatrix.Save(thisTemp);

handles.hInput.DisplayProperties.setDisplayProperties(handles.hInput);
displaySpect(handles.hInput)


% --- Executes on button press in applyButton.
function applyButton_Callback(hObject, eventdata, handles)
% hObject    handle to applyButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in resetButton.
function resetButton_Callback(hObject, eventdata, handles)
% hObject    handle to resetButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

thisTemp                                            = handles.hInput.MRSpectroMatrix.getTemporary;

handles.hInput.MRSpectroMatrix.Save(thisTemp);



set(handles.sliderLinewidth,'Value',0);
set(handles.sliderPhase,'Value',0);
set(handles.sliderFrequency,'Value',0);


set(handles.editLinewidth,'String', num2str(0));
set(handles.editPhase,'String', num2str(0));
set(handles.editFrequency,'String',num2str(0));
displaySpect(handles.hInput);



%##########################################################################
%##########################################################################

% --- Executes during object creation, after setting all properties.
function editLinewidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editLinewidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function sliderLinewidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderLinewidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function sliderPhase_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderPhase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes during object creation, after setting all properties.
function editPhase_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editPhase (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function sliderFrequency_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderFrequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function editFrequency_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFrequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function sliderOffset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function editOffset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


