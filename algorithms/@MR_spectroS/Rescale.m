function this = Rescale( this )

if ~this.Parameter.ReconFlags.isSorted
    error('The data has to be sorted first');
end

if  this.Parameter.ReconFlags.isRescaled
    error('The data have been already rescaled');
end

if ~this.Parameter.ReconFlags.isFrequencyAligned
    warning('The data have not been frequency aligned');
end    
    
oldData                = this.Data{1};
nCoils                 = size(this.Data{1}, this.coil_dim);
nAverages              = size(this.Data{1}, this. meas_dim);
newData                = oldData;
reFactorsArray         = zeros([nCoils nAverages/2]);
rescaleMethod          = 1;

        

      
     

if mod(nAverages,2)
    error('The number has to be multiply of 2!!!')
end



%Enter a input dialox. If user presses Cancel then aautomatic procedure is
%applied for the determination of rescaling factor of MC data. Otherwise
%user manually defines the appropiate rescaling factor
% x = inputdlg('Enter rescaling factor for MC:',...
%              'ScaleData', [1 20]);
 x = [];
if isempty(x)   
            
    if rescaleMethod ==1 

        x1 = 0.8;
        x2 = 1.2;
        options.TolX = 1e-11;
        %function error = rescaleCostFun2(reFactor,data)
        x  =  fminbnd(@(x) rescaleCostFun2(x,oldData),x1,x2,options);


    else
        for indexOfCoil =1:nCoils
            for indexOfAverage = 2:2:nAverages
                fid    = oldData(:,:,:,indexOfCoil,:,:,:,:,:,:,:,indexOfAverage);
                fidRef = oldData(:,:,:,indexOfCoil,:,:,:,:,:,:,:,indexOfAverage-1);

                %x = fminbnd(fun,x1,x2)

                x1 = 0.5;
                x2 = 1.5;
                options.TolX = 1e-9;
                %function [error, fidRescaled] =
                %rescaleCostFun(reFactor,fid,fidRef)
                x     =  fminbnd(@(x) rescaleCostFun(x,fid,fidRef),x1,x2,options);

                reFactorsArray(indexOfCoil,indexOfAverage/2) = x;

            end
        end

    end
    
else
    
    
    
    x = str2double(x{:});  
    
end



%//////////////////////////////////////////////////////////////////////////
%                                Reconstruct data
%//////////////////////////////////////////////////////////////////////////

if rescaleMethod ==1  

     newData(:,:,:,:,:,:,:,:,:,:,:,2:2:end)= x* oldData(:,:,:,:,:,:,:,:,:,:,:,2:2:end);
    
else
    
    for indexOfCoil =1:nCoils
        for indexOfAverage = 2:2:nAverages
            fidRef           = [];
            fid              = oldData(:,:,:,indexOfCoil,:,:,:,:,:,:,:,indexOfAverage);
            reFactor         = reFactorsArray(indexOfCoil,indexOfAverage/2);
            [~, fidRescaled] = rescaleCostFun(reFactor,fid,fidRef);
            newData(:,:,:,indexOfCoil,:,:,:,:,:,:,:,indexOfAverage)=...
            fidRescaled;

        end
    end
    
end



%//////////////////////////////////////////////////////////////////////////
%                            Plotting results
%//////////////////////////////////////////////////////////////////////////

figure

oData1   = squeeze(oldData(:,:,:,1,:,:,:,:,:,:,:,:));
oData2   = oData1(:,1:2:end) - oData1(:,2:2:end);
fftOData = fftshift( fft( oData2 ) );


nData1   = squeeze(newData(:,:,:,1,:,:,:,:,:,:,:,:));
nData2   = nData1(:,1:2:end) - nData1(:,2:2:end);
fftNData = fftshift( fft( nData2 ) );

if rescaleMethod ==1  
    
    subplot(2,1,1)
    plot(real(sum(fftOData,2)))
    title('w/o rescaling')
    
    subplot(2,1,2)
    plot(real(sum(fftNData,2)))
    title('with rescaling')
    
else
    
    subplot(3,1,1)
    plot(reFactorsArray(1,:),'r--*')

    subplot(3,1,2)
    oData1   = squeeze(oldData(:,:,:,1,:,:,:,:,:,:,:,:));
    oData2   = oData1(:,1:2:end) - oData1(:,2:2:end);
    fftOData = fftshift( fft( oData2 ) );
    plot(real(sum(fftOData,2)))

    subplot(3,1,3)
    nData1   = squeeze(newData(:,:,:,1,:,:,:,:,:,:,:,:));
    nData2   = nData1(:,1:2:end) - nData1(:,2:2:end);
    fftNData = fftshift( fft( nData2 ) );
    plot(real(sum(fftNData,2)))

end

%Update the MR_spectroS object
 this.Data{1}                           = newData;
 this.Parameter.ReconFlags.isRescaled   = x; %save the  rescale factor
 clc
 display(['Rescale factor: ',num2str(x)]);
 %this.Parameter.ReconFlags.isRescaled   = true;


 


%//////////////////////////////////////////////////////////////////////////
%                          COST FUNCTIONS
%//////////////////////////////////////////////////////////////////////////
    function [error, fidRescaled] = rescaleCostFun(reFactor,fid,fidRef)
     fidRescaled = reFactor*fid;
     
%      fftsFidRef     = fftshift (fft ( fidRef ));
%      fftsFidScaled  = fftshift (fft ( fidRescaled ));
%      error          = fftsFidRef- fftsFidScaled;
%      error          = error(length(round(error/2)-50:round(error/2)+50));

    if ~isempty(fidRef) 
     error       = fidRef - fidRescaled;
     error       = sum(abs(error).^2);
     
    else
        error=0;
    end
    
    end


function error = rescaleCostFun2(reFactor,data)
    data1 = squeeze(data(:,:,:,:,:,:,:,:,:,:,:,1:2:end));
    data2 = squeeze(data(:,:,:,:,:,:,:,:,:,:,:,2:2:end));
    error = data1 -reFactor*data2;
    error = sum(sum(sum(abs(error).^2)));      
end





end


