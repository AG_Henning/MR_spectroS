%% configuration of connection
LCModelUser = 'tborbath@mpi.localnet';

[LCModelPassword, LCModelUser] = passwordEntryDialog('enterUserName', true, 'DefaultUserName', LCModelUser, 'CheckPasswordLength', false );
LCModelIP = '10.41.60.215';

%% file paths setup 
controlFilesPathRemote = '/Desktop/MM/LCModelConfig/';
outputFilesPathRemote = '/Desktop/MM/Output/';
outputFilesPathLocal = 'D:/Software/Spectro Data/MM/Output/';
%%
currentControlFile = 'fitsettings_Summed_Averaged_TE24_Cre_PpmPfeuffer.control';
currentOutputFiles = 'Summed_Averaged_TE24';
copyFilesLocally = true;


%% ---------------------------------------------------------------
myLCModelCaller = LCModel_Caller(LCModelUser, LCModelPassword, LCModelIP, controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal)
myLCModelCaller = myLCModelCaller.FitSpectrum(currentControlFile, currentOutputFiles, copyFilesLocally)

%%
fileName = 'fitsettings_Summed_Averaged_TE24_Cre.control';
localPath =  'D:/Software/Spectro Data/MM/LCModelConfig/';
remoteDirectory = '/Desktop/MM/LCModelConfig/';

myLCModelCaller.CopyDataToRemote(localPath, remoteDirectory, fileName)
myLCModelCaller.CopyDataFromRemote(remoteDirectory, localPath, fileName)