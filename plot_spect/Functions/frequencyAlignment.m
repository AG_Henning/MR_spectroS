function varargout = frequencyAlignment(varargin)
% FREQUENCYALIGNMENT MATLAB code for frequencyAlignment.fig
%      FREQUENCYALIGNMENT, by itself, creates a new FREQUENCYALIGNMENT or raises the existing
%      singleton*.
%
%      H = FREQUENCYALIGNMENT returns the handle to a new FREQUENCYALIGNMENT or the handle to
%      the existing singleton*.
%
%      FREQUENCYALIGNMENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FREQUENCYALIGNMENT.M with the given input arguments.
%
%      FREQUENCYALIGNMENT('Property','Value',...) creates a new FREQUENCYALIGNMENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before frequencyAlignment_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to frequencyAlignment_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help frequencyAlignment

% Last Modified by GUIDE v2.5 28-Feb-2014 16:54:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @frequencyAlignment_OpeningFcn, ...
                   'gui_OutputFcn',  @frequencyAlignment_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before frequencyAlignment is made visible.
function frequencyAlignment_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to frequencyAlignment (see VARARGIN)

% Choose default command line output for frequencyAlignment
handles.output = hObject;


if nargin == 4
    handles.hInput = varargin{1};
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes frequencyAlignment wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = frequencyAlignment_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenuDomain.
function popupmenuDomain_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuDomain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuDomain contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuDomain





function editIndeces_Callback(hObject, eventdata, handles)
% hObject    handle to editIndeces (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editIndeces as text
%        str2double(get(hObject,'String')) returns contents of editIndeces as a double




% --- Executes on button press in aplpyButton.
function aplpyButton_Callback(hObject, eventdata, handles)
hFig  =gcf;
% hObject    handle to aplpyButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

domainString               = get(handles.popupmenuDomain,'String');
domainValue                = get(handles.popupmenuDomain,'Value');
domain                     = domainString {domainValue};
indecesOfInterest          = get(handles.editIndeces,'String');
displayPlots               = get(handles.checkbox1,'Value');

this   = handles.hInput.MRSpectroMatrix.getCurrent();

set(this.Parameter.FreqAlignSettings,'domain',domain);
set(this.Parameter.FreqAlignSettings,'indecesOfInterest',indecesOfInterest);
set(this.Parameter.FreqAlignSettings,'displayPlots',displayPlots);

this   = this.FrequencyAlign;
handles.hInput.MRSpectroMatrix.Save(this);
displaySpect(handles.hInput);
close(hFig);







% --- Executes during object creation, after setting all properties.
function editIndeces_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editIndeces (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1




% --- Executes during object creation, after setting all properties.
function popupmenuDomain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuDomain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
