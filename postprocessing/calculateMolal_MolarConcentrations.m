function [absoluteConcentrations] = calculateMolal_MolarConcentrations(relativConcentrations, tissueFractions, doMolal_Molar, TR_met, TE_met, TR_water, TE_water, ...
    NEX_H2O, NEX_signal, rescaleFactorMC, wconc_LCModel, atth2o_LCModel, T2_met, T1_met)
%% calculateMolal_MolarConcentrations calculates absolute concentrations
% does the absolute quantifications based on Gasparovic formulas from MRS
% Workshop Konstanz
% - relativeConcentrations: relativeConcentrations scaled to water
% - tissueFactions: tissue volume fractions struct
% - calculateMolal: flag to calculate Molal or Molar
% - TR_met: Repetition time value for metabolite spectrum [ms]
% - TE_met: echo time for metabolite spectrum [ms]
% - TR_water: Repetition time value for water reference [ms]
% - TE_water: echo time for water reference [ms]
% - NEX_H2O: number of averages measured for the water-unsuppressed scans
% - NEX_signal: number of averages measured for the water suppressed scan
% - rescaleFactorMC: rescaling factor from the metabolite cycling
% - wconc_LCModel: wconc parameter from the LCModel fit
% - atth2o_LCModel: atth2o parameter from the LCModel fit

%do molal calculations if not defined
if ~exist('doMolal_Molar','var')
    doMolal_Molar = 'Molal';
end
%Repetition time value [ms]
if ~exist('TR_met','var')
    TR_met = 6000;
end
% echo time [ms]
if ~exist('TE_met','var')
    TE_met = 24;
end
%Repetition time value [ms]
if ~exist('TR_water','var')
    TR_water = 6000;
end
% echo time [ms]
if ~exist('TE_water','var')
    TE_water = 24;
end
%number of averages measured for the water-unsuppressed and the water suppressed scan
if ~exist('NEX_H2O','var')
    NEX_H2O = 16;
end
if ~exist('NEX_signal','var')
    NEX_signal = 96; %divide by two to account for the metabolite cycling
end
%rescaling factor from the metabolite cycling
if ~exist('rescaleFactorMC', 'var')
    rescaleFactorMC = 1;
end
% wconc parameter from the LCModel fit
if ~exist('wconc_LCModel', 'var')
    wconc_LCModel = 40873;
end
% atth2o parameter from the LCModel fit
if ~exist('atth2o_LCModel', 'var')
    atth2o_LCModel = 1;
end

%% factors taken from literature
%relative densities of water in the given tissue types
%(Kreis 1993 - Absolute Quantitation of Water and Metabolites in the Human Brain.
% I. Compartments and Water)
a_WM = 0.65;
a_GM = 0.78;
a_CSF = 0.97;

% tissue densities (g/mL) 
% (Brooks R. - 1980 - Explanation of cerebral white - gray contrast in computer tomography
% additional reference in Kreis R. & Ernst T. - 1993 - Absolute Quantitation of Water and Metabolites in the Human Brain. I. Compartments and Water)
d_WM = 1.04;
d_GM = 1.04;
d_CSF = 1;

%T1 relaxation times of water in the given tissue type [ms] at 9.4T : From
%Gisela Hagberg 2017 Neuroimage
T1_GM = 2120;
T1_WM = 1400;
T1_CSF = 4800; %Recalculated estimation from Gisela

%Add T2 relaxation times of water in the given tissue type [ms] at 9.4T :
%Gisela Hagberg 2017 Neuroimage
T2_GM = 37;
T2_WM = 30;
T2_CSF = 181; %Recalculated estimation from Gisela

%Relaxation time correction factors  //Add T2 relaxation times of H20 at
%7T
R_GM = exp(-TE_water/T2_GM) .* (1-exp(-TR_water./T1_GM));
R_WM = exp(-TE_water/T2_WM) .* (1-exp(-TR_water./T1_WM));
R_CSF = exp(-TE_water/T2_CSF) .* (1-exp(-TR_water./T1_CSF));

%Relaxation Time correction factors Averaged T2* and T1 values from
%Deelchand 2010 : we need T2-rho
T1_ave = 1516;
T2_ave = 77.2;
if exist('T1_met','var')
    T1_exp = (1- exp(-TR_met./T1_met));
else
    T1_exp = (1- exp(-TR_met./T1_ave));
end
if exist('T2_met','var')
    if ~isnan(T2_met) 
        T2_exp = exp(-TE_met./T2_met);
    else
        T2_exp = 1; % catch the situation when we make the T2_met on purpose NaN (not using metabolite T2 correction)
    end
else
    T2_exp = exp(-TE_met./T2_ave);
end
R_M = T2_exp .* T1_exp; %exp(-TE./T2_ave).* (1- exp(-TR./T1_ave));

%% percentages of the tissue GM, WM, CSF in the selected voxels for the
%given scan
fv_GM  = tissueFractions.fv_GM;
fv_WM  = tissueFractions.fv_WM;
fv_CSF = tissueFractions.fv_CSF;

if strcmpi(doMolal_Molar, 'Molal') %MOLAL calculations
    
    %pure water concentration at body temperature  [mMolal/kg]
    %molality is temperature-independent
    conc_pure_water = 55510;
    
    %Calculate water fraction from volume fractions obtained from segmentation
    %and relative water fraction in each segmentation
    f_sum = fv_GM .* a_GM + fv_WM .* a_WM + fv_CSF .* a_CSF;
    %corrected version considering also the water densities
    f_sum = fv_GM .* a_GM .* d_GM + fv_WM .* a_WM .* d_WM + fv_CSF .* a_CSF .* d_CSF;
    
    f_GM = fv_GM .* a_GM .* d_GM ./ f_sum;
    f_WM = fv_WM .* a_WM .* d_WM ./ f_sum;
    f_CSF = fv_CSF .* a_CSF .* d_CSF ./f_sum;
    
    %% calculate true water concentration for each subject : density terms do not come in molality
    water_conc_relaxation_corrected = conc_pure_water * ...
        ( f_GM * R_GM + ... correction for gray matter concentration
        f_WM * R_WM + ... correction for white matter concentration
        f_CSF  * R_CSF ) / ... correction for cerebrospinal fluid concentration
        (1 - f_CSF);
    
else %MOLAR calculations
    
    %pure water concentration at body temperature [mM]
    conc_pure_water = 55126;
    
    %% calculate true water concentration for each subject
    water_conc_relaxation_corrected = conc_pure_water * ...
        ( fv_GM * a_GM * d_GM * R_GM + ... correction for gray matter concentration
        fv_WM * a_WM * d_WM * R_WM + ... correction for white matter concentration
        fv_CSF * a_CSF * d_CSF * R_CSF ) / ... correction for cerebrospinal fluid concentration
        (1 - fv_CSF);
end

%correct for the LCModel assumptions
water_conc_relaxation_corrected_LCModel = water_conc_relaxation_corrected / wconc_LCModel * (1/atth2o_LCModel);

%calculate absolutely quantified metabolite
absoluteConcentrations = relativConcentrations .* water_conc_relaxation_corrected_LCModel  ./ R_M .* ...
    (NEX_H2O/NEX_signal) .* 2./(1+rescaleFactorMC);
end