function plot_mean_std_of_spectra_MM

downField_MM_Met = 'MM';
switch downField_MM_Met
    case 'DF'
        pathName = 'DF data path';
        TEs = [24; 24; 32; 40; 52; 60; ];
        colors = {[0 0.7 0]; [0 0.7 0]; [0 0 0];  [0 0 1]; [1 0 0]; [0 0.5 0.5];};
        offsetStep = 2.2e-5;
    case 'MM'
        pathName = 'MM T1 data path';
        TEs = [1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; ];
        colors = {[76 187 23]/255; [76 187 23]/255; [76 187 23]/255; [76 187 23]/255; [76 187 23]/255; [1 121 111]/255; [75 0 130]/255; [75 0 130]/255; [75 0 130]/255; [75 0 130]/255; [75 0 130]/255;};
        offsetStep = 7.0e-5;
    case 'MM WM'
        pathName = 'MM WM T1 data path';
        TEs = [1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; ];
        colors = {[76 187 23]/255; [76 187 23]/255; [76 187 23]/255; [76 187 23]/255; [76 187 23]/255; [1 121 111]/255; [75 0 130]/255; [75 0 130]/255; [75 0 130]/255; [75 0 130]/255; [75 0 130]/255;};
        offsetStep = 7.0e-5;
    case 'Met'
        pathName = 'DF data path';
        TEs = [24; 32; 40; 52; 60; ];
        colors = {[0 0.7 0]; [0 0 0]; [0 0 1]; [1 0 0]; [0 0.5 0.5];};
        offsetStep = 2.3e-4;
end

[path] = pathToDataFolder(pathName);

fileNameBase = [path, 'Summed_Averaged_TI1_'];


numberOfTEs = length(TEs);

offSetBase = offsetStep * (numberOfTEs + 7);  %+7 for WM

for indexTE = 1:numberOfTEs
    fileName = [fileNameBase num2str(TEs(indexTE))];
    offset = offSetBase - indexTE * offsetStep;
    if (indexTE == 1) && (strcmp(downField_MM_Met,'DF'))
        plotOneTE(fileName, TEs(indexTE), colors{indexTE}, offset+offsetStep/2, 'DF_up');
    else
        plotOneTE(fileName, TEs(indexTE), colors{indexTE}, offset, downField_MM_Met);
    end
end

end

function plotOneTE(fileName, TE, colorSpectrum, offset, downField_MM_Met)
load([fileName '.mat'], 'summedSpectraTI')

zeroPPM = 4.67;
nTimepoints = size(summedSpectraTI.Data{1},summedSpectraTI.kx_dim);
nAverages = size(summedSpectraTI.Data{1},summedSpectraTI.meas_dim);
waterFreq  = summedSpectraTI.Parameter.Headers.ScanFrequency * 1E-6;
bandwidth  = summedSpectraTI.Parameter.Headers.Bandwidth_Hz;
frequencyRange    = linspace(-bandwidth/2,bandwidth/2,nTimepoints);

if strcmp(downField_MM_Met,'DF_up')
    fidSpectra = summedSpectraTI.Data{1}(:,1,1,1,1,1,1,1,1,1,1,:) * 5;
else
    fidSpectra = summedSpectraTI.Data{1}(:,1,1,1,1,1,1,1,1,1,1,:) * 1;
end

fidWater = summedSpectraTI.Data{1}(:,1,1,1,1,1,1,1,2,1,1,:);
spectra = squeeze(fftshift(fft(fidSpectra)));
water = squeeze(fftshift(fft(fidWater)));
meanSpectrum = mean(spectra,2);
stdSpectrum = std(spectra,[],2);
relativeStd = stdSpectrum ./ max(meanSpectrum) * 100;

ppmVector = (frequencyRange)/waterFreq+zeroPPM;

% figure(101)
% plot(ppmVector,relativeStd)
% xlim([0.5 4.2]);
% set(gca,'xDir','reverse')


%%
scalingRange = (ppmVector < 1.8) & (ppmVector > 0);
for indexAverage = 1:nAverages
    [maxWater,indexMaxWater] = max(abs(real(water(:, indexAverage))));
    
    [~, indexMaxWaterInRange] = max(abs(real((water(scalingRange, indexAverage)-mean(water(scalingRange, indexAverage))))));
    
    [maxWaterInRange, ~] = max(real(water(scalingRange, indexAverage)));
    minWaterInRange = min(real(water(scalingRange, indexAverage)));
    
    maxSpectraInRange = max(real(spectra(scalingRange, indexAverage)));
    minSpectraInRange = min(real(spectra(scalingRange, indexAverage)));
    
    indexStart = find(scalingRange);
    indexMaxWaterInRange = indexMaxWaterInRange + indexStart(1);
    referenceLines = zeros(nTimepoints,1);
    referenceLines(:) = -maxWaterInRange*2;
    referenceLines(indexMaxWaterInRange:indexMaxWater + (indexMaxWater - indexMaxWaterInRange)) = maxWaterInRange*2;
    
    plotOffset = maxSpectraInRange;
%     figWaterSpectra = figure(2);
%     hold on
%     plot(ppmScale, real(water(:,indexAverage)) - minWaterInRange + plotOffset,'b')
%     plotScale = (maxWaterInRange-minWaterInRange);
%     ylim([minSpectraInRange-(plotOffset*0.3) plotScale*1.2+plotOffset]);
%     hold on
%     pSpectra = plot(ppmScale, real(spectra(:,indexAverage)),'r');
%     hold on
% %     figure
%     plot(ppmScale, referenceLines, 'k--')
%     xlim([0 8.5]);
%     set(gca,'xDir','reverse')
%     set(gca,'LineWidth',1);
%     % set(gca,'ytick',[])
%     xlabel('[ppm]');
%     makedatatip(pSpectra,[indexMaxWaterInRange])
%     
%     print(['WaterSpectraComparisonPlots\' num2str(indexAverage) '_' fileName],'-dpng')
%     close(figWaterSpectra)
end
%%
% figure
% plot(ppmScale, real(spectra))

% xlim([0.5 4.2]);
% set(gca,'xDir','reverse')
% set(gca,'LineWidth',1);
% % set(gca,'ytick',[])
% xlabel('[ppm]');
% title(['Individual Spectra TE ' num2str(TE) ' ms'])

%%
figure(1)
stdAlpha = 0.4;
hold on
if strcmp(downField_MM_Met,'DF_up')
    indexDFCut  = find(ppmVector > 8.6, 1, 'first');
    ppmVector = ppmVector(indexDFCut:end);
    meanSpectrum = meanSpectrum(indexDFCut:end);
    stdSpectrum = stdSpectrum(indexDFCut:end);
end

h = area(ppmVector,[(meanSpectrum - stdSpectrum) + offset (stdSpectrum * 2)]);

h(1).FaceColor = 'none';
h(2).FaceColor = colorSpectrum;
h(2).FaceAlpha = stdAlpha;
h(1).EdgeColor = 'none';
h(2).EdgeColor = 'none';
hold on
pSpectrum = plot(ppmVector,meanSpectrum + offset, 'LineWidth',2, 'Color', colorSpectrum);
    
switch downField_MM_Met
    case 'DF'
        xlim([5.5 9.4]);
    case 'DF_up'
        xlim([5.5 9.4]);
    case 'MM'
        xlim([0.5 4.2]);
%         ylim([4.5e-6 4.7e-5]);
    case 'MM WM'
        xlim([0.5 4.2]);
        %ylim([2e-6 1.3e-4]); %TODO: check why scaling is different, then MM
    case 'Met'
        xlim([0.5 4.2]);
        ylim([7e-5 1.65e-3]);
end
set(gca,'xDir','reverse')
set(gca,'LineWidth',1);
set(gca,'ytick',[])
xlabel('[ppm]');
title('Spectra mean and standard deviation')
% title(['Spectra TE ' num2str(TE) ' ms'])
end