function this    = CreateBasisSet( this, makeBasisFilePathLocal, makeBasisFilePathRemote, makeBasisFile, copyFileLocally, basisFileName)

this.CopyDataToRemote(makeBasisFilePathLocal, makeBasisFilePathRemote, makeBasisFile);

makeBasisFullFilePath = [this.homePath makeBasisFilePathRemote makeBasisFile];

[this.connection, res] = sshfrommatlabissue(this.connection, [this.homePath this.MakeBasisPath ' < ' makeBasisFullFilePath]);

if (copyFileLocally)
    fileExtensions = {'.basis', '.ps'};
    for extension = fileExtensions
        scptomatlab(this.LCModelUser, this.LCModelIP, this.LCModelPassword, this.outputFilesPathLocal, [this.homePath this.outputFilesPathRemote basisFileName extension{1}])
    end
end

end
