function this = Anonimize_MR_spectroS(this, subjectNumber)
% subjectNumber should be random, not pseudo-random.

% if the subjectNumber is not given, use the timestamp of the execution as
% a subjectID. Using the timestamp makes the data anominized, but also
% unique. No other data set will have the same execution timestamp.
if ~exist('subjectNumber','var')
    subjectNumber = round(now*1e8);
end
% make subjectNumber a string and give at least 4 digit precision
subjectNumberStr = ['Subj_', num2str(subjectNumber,'%.4d')];

%% Header info
patientID = this.Parameter.Headers.PatientID;
this.Parameter.Headers.PatientID = subjectNumber;
this.Parameter.Headers.PatientName = subjectNumber;

% keep birthday only as year of birth, set rest to 1st of January
birthday = this.Parameter.Headers.BirthDay;
this.Parameter.Headers.BirthDay = round(birthday / 1e4)*1e4+0101;

%% Filename
% remove MID and FID numbers from filename and add the subjectNumberStr
[~, fileName, extension] = fileparts(this.Parameter.Filename);
fileNameChunks = strsplit(fileName,'_');
idx = contains(fileNameChunks,'MID');
fileNameChunks(idx) = [];
idx = contains(fileNameChunks,'FID');
fileNameChunks(idx) = [];
fileNameAnonimized = [strjoin(fileNameChunks,'_'), '_', subjectNumberStr, extension];
this.Parameter.Filename = fileNameAnonimized;

%% Filepath
%filepath may contain patientID. If true, this will replace it
filepath = this.Parameter.Filepath;
this.Parameter.Filepath = strrep(filepath,num2str(patientID),subjectNumberStr);
end