function [a] = reconstructWater_sbe(fid_id, isInVivo, saveResults)

nsamplesBeforeEcho = 4;

if ~exist('isInVivo', 'var')
    isInVivo = true;
end

if ~exist('saveResults', 'var')
    saveResults = true;
end

a = MR_spectroS(fid_id);

%%
if nsamplesBeforeEcho ~= 0
    startpoint = nsamplesBeforeEcho + 1;
    endpoint   = size(a.Data{1},a.kx_dim)-(32-nsamplesBeforeEcho);
    data = squeeze(a.Data{1});
    data = data(startpoint:endpoint,:,:);
    dataNew(:,1,1,:,1,1,1,1,1,1,1,:) = data;
    a.Data{1} = dataNew;
    clear data dataNew
end

%%
if isInVivo
    a = a.FrequencyAlign;
end
a = a.AverageData;
a = a.EddyCurrentCorrection;
a = a.CombineCoils;
%% setup the settings for the FrequencyAlignFreqDomain
a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;

%indices of Interest: given in ppm
% water peak
a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = [4.67];

%set zeroFillingParameter to get smooth approximations
a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;

%search area (+-) in ppm
a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.3;
%spline filtering is not needed if the spectra are not too noisy.
%flag to do spline filtering
a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
%spline filtering coefficient
a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;
% do the actual Frequency Alignment
a = a.FrequencyAlignFreqDomain;
%%
if isInVivo
    a = a.Truncate(2000);
end

if saveResults
    %saving the file
    filename = sprintf('%d',a.Parameter.Headers.PatientName);
    save([filename '_WRef.mat'], 'a')
    %export to LCModel
    addSinglet0ppm = 'Yes';
    a.ExportLcmRaw('',strcat(filename,'_WRef'), addSinglet0ppm);
end

figure; hold on;
data = squeeze(a.Data{1});
ppmVector = a.getPpmVector;
plot(ppmVector,real(fftshift(fft(data(:,1)))))
set(gca, 'XDir','reverse')
xlabel 'f / ppm'
xlim([0.5 8.9]); 

end