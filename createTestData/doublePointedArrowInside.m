function doublePointedArrowInside(xa,yUpper,yLower, arrowsColor)
     set(gcf,'Units','normalized');
     yaDown = [yUpper+1e-8 yUpper];
     yaLine = [yLower yUpper];
     yaUp = [yLower-1e-8 yLower];
     [xaf,yafLine] = axescoord2figurecoord(xa,yaLine);
     line = annotation('line',xaf,yafLine);
     [xaf,yafDown] = axescoord2figurecoord(xa,yaDown);
     arDown = annotation('arrow',xaf,yafDown);
     [xaf,yafUp] = axescoord2figurecoord(xa,yaUp);
     arUp = annotation('arrow',xaf,yafUp);
     %styling
     line.Color = arrowsColor;
     arDown.HeadStyle = 'vback2';
     arDown.HeadLength = 5;
     arDown.HeadWidth = 7;
     arDown.Color = arrowsColor;
     arUp.HeadStyle = 'vback2';
     arUp.HeadLength = 5;
     arUp.HeadWidth = 7;
     arUp.Color = arrowsColor;
end