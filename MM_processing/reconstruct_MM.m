function [a] = reconstruct_MM(fid_id, phase0, phase1Hz, doReverse)
if ~exist('doReverse', 'var')
    doReverse = true;
end
%%
a = MR_spectroS(fid_id,1);
%%
% trunc_ms = 250;
% truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
% a = a.Truncate(truncPoint);

a = a.FrequencyAlign;
a = a.DeleteMovedAverages;
a = a.Rescale;
a = a.ReconData;
a = a.AverageData;
a = a.EddyCurrentCorrection;
a = a.CombineCoils;
%% setup the settings for the FrequencyAlignFreqDomain
a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;

%indices of Interest: given in ppm
% water peak
% a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = [3.925];

%set zeroFillingParameter to get smooth approximations
% a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;

%search area (+-) in ppm
a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.1;
%spline filtering is not needed if the spectra are not too noisy.
%flag to do spline filtering
a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
%spline filtering coefficient
a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;
% do the actual Frequency Alignment
a = a.FrequencyAlignFreqDomain;
%%
% a.Parameter.HsvdSettings.bound = [-160 160];
% a.Parameter.HsvdSettings.n = 4096; %size(a.Data{1},1);
% a.Parameter.HsvdSettings.p = 25;
% [~, a] = a.Hsvd;
% %%
% if(doReverse)
%     a = a.ReverseMC;
%     a.Parameter.HsvdSettings.bound = [-160 160];
%     [~, a] = a.Hsvd;
% end
%%
% trunc_ms = 150;
% truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
% a = a.Truncate(truncPoint);
%% Phase correction
if ~exist('phase0','var')
    phase0 = 0;
end
if ~exist('phase1Hz','var')
    phase1Hz = 0;
end
a.Parameter.PhaseCorrSettings.phase0     =  phase0;
a.Parameter.PhaseCorrSettings.phase1     =  phase1Hz;
a.Parameter.PhaseCorrSettings.freqCentre =  0;
a = a.PhaseCorrection;

end