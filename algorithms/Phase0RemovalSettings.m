classdef Phase0RemovalSettings
% version: 17 June 2019
% phase0 in rad

    properties
        phase0        
    end
    
    methods
        
        function this=Phase0RemovalSettings
            this.phase0=0;            
        end
    end
    
end

