function outputFile = createLCModelConfigfMRSTimeCourse(controlFilesPath, outputControlFilesPath, defaultControlFile, ...
    currentfMRS, defaultfMRS, spectrumFileName, waterRefFileName, defaultWaterRef, subjectsPath,...
    defaultSubjectsPath, subjectID, defaultSubject, MMBType, defaultMMB, BlockNo)
% Intro comments need to be rewritten
% Creates the control files for the TE series, given a default control file.
% the newly generated files will be placed in the same path as indicated by
% the controlFilesPath.
% downField_MM_Met can be: 'DF' 'MM', 'Met' or 'pH'
% A sample input:
% controlFilesPath = 'Fitsettings/MMocc_noExtraMM_06knots/';
% defaultControlFile = 'fitsettingsDefault.control'

controlFileSuffix = '.control';

fitSettingsDefaultFileID = fopen([controlFilesPath defaultControlFile], 'r');

mkdir(outputControlFilesPath);
outputFile = strcat(outputControlFilesPath, 'fitsettings_', spectrumFileName, controlFileSuffix);

fitSettingsFileID = fopen(outputFile, 'w');
display(outputFile);

while(~feof(fitSettingsDefaultFileID))
    s = fgetl(fitSettingsDefaultFileID);
    k = strfind(s,'FILBAS');
    l = strfind(s,'FILRAW');
    currentfMRSextended = strcat(currentfMRS, MMBType, BlockNo);
    if ~isempty(k)
        s = strrep(s, defaultSubjectsPath, subjectsPath);
        s = strrep(s, defaultSubject, subjectID);
        s = strrep(s, defaultMMB, MMBType);
    elseif ~isempty(l)
        if ~isempty(strfind(MMBType, 'v2')) & ~isempty(strfind(BlockNo, '_woBaseline'))
            s = strrep(s, defaultfMRS, strcat(currentfMRS, '_v2', BlockNo));
        else
            s = strrep(s, defaultfMRS, strcat(currentfMRS, BlockNo));
        end
        s = strrep(s, defaultSubjectsPath, subjectsPath);
        s = strrep(s, defaultSubject, subjectID);
    else
        s = strrep(s, defaultfMRS, currentfMRSextended);
        s = strrep(s, defaultSubjectsPath, subjectsPath);
        s = strrep(s, defaultWaterRef, waterRefFileName);
        s = strrep(s, defaultSubject, subjectID);
        s = strrep(s, defaultMMB, MMBType);
    end
    
    fprintf(fitSettingsFileID,'%s\n', s);
end
fclose(fitSettingsFileID);
fclose(fitSettingsDefaultFileID);
end