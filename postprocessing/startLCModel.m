function [LCModelCallerInstance] = startLCModel(controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal)

%% configuration of connection
LCModelUser = 'jdorst';

% I recommend you to change this password and the user name above, but please don't commit it
LCModelPassword = 'XXXX';

pathFunction = mfilename('fullpath');
pathLicenseFile = strrep(pathFunction, '\postprocessing\startLCModel', '/algorithms/@LCModel_Caller/LCModelLogin.xlsx');
if exist(pathLicenseFile, 'file') == 2
    [~, ~, raw] = xlsread(pathLicenseFile);
    LCModelUser = raw{1,2};
    LCModelPassword = raw{2,2};
end

if strcmp(LCModelPassword,'XXXX')
    [LCModelPassword, LCModelUser] = passwordEntryDialog('enterUserName', true, 'DefaultUserName', LCModelUser, 'CheckPasswordLength', false );
    answer = questdlg('Would you like to save the password in LCModelLogin.xlsx ?', 'Save Password' , 'Yes', 'No', 'No');
    if strcmp(answer, 'Yes')
        tableLCModelLogin = {'LCModelUser', LCModelUser ; 'LCModelPassword', LCModelPassword};
        xlswrite(pathLicenseFile, tableLCModelLogin)
    end
end
LCModelIP = '10.41.60.215';

%% file paths setup
if ~exist('controlFilesPathRemote', 'var')
    controlFilesPathRemote = '/Desktop/MM/LCModelConfig/';
end
if ~exist('outputFilesPathRemote', 'var')
    outputFilesPathRemote = '/Desktop/MM/Output/';
end
if ~exist('outputFilesPathLocal', 'var')
    outputFilesPathLocal = 'D:/Software/Spectro Data/MM/Output/';
end

if nargin < 3
    warning('Too few arguments were passed. Taking default values. \n The chosen arguments are:\n controlFilesPathRemote = %s\n outputFilesPathRemote = %s\n outputFilesPathLocal = %s', controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal);
end
%% initialize the LCModel remote connection
LCModelCallerInstance = LCModel_Caller(LCModelUser, LCModelPassword, LCModelIP, controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal);
