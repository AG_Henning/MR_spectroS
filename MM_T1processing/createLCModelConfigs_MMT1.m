function createLCModelConfigs_MMT1(controlFilesPath, defaultControlFile, orderTI1s, orderTI2s, defaultTI, defaultLCModelUser, currentLCModelUser, subjects, defaultSubject)
% Creates the control files for the TE series, given a default control file.
% the newly generated files will be placed in the same path as indicated by
% the controlFilesPath.
% The defaultControlFile has to include the keyword "Default" in the file name.
% A sample input:
% controlFilesPath = 'Fitsettings/MMocc_noExtraMM_06knots/';
% defaultControlFile = 'fitsettingsDefault.control'

if(nargin  <  6)
    error('Error: createLCModelConfigsTEseries requires minimum 6 input arguments: controlFilesPath, defaultControlFile, orderTI1s, orderTI2s, defaultTI, defaultLCModelUser, currentLCModelUser, subjects, defaultSubject .');
end
orderTIsLong = strcat('TI1_', orderTI1s, '_TI2_', orderTI2s);
defaultKeyword = 'Default';
indexFind  = strfind(defaultControlFile, defaultKeyword);
if isempty(indexFind)
    error('The default control file should have the keyword "Default" in its file name.');
end

fitAllSubjects = false;
numOfSubjects = 1;
if  exist('subjects','var') && exist('defaultSubject','var')
    indexFindSubject  = strfind(defaultControlFile, defaultSubject);
    if isempty(indexFindSubject)
        error('The default control file should have the keyword %s in its file name when using subject fitting.', defaultSubject);
    end
    subjectsControlFilesPath = strcat(controlFilesPath, subjects, '/');

    fitAllSubjects = true;
    numOfSubjects = length(subjects);
    % create the folders for the individual subjects
    for indexSubject = 1:numOfSubjects
        mkdir(subjectsControlFilesPath{indexSubject})
    end
end

controlFileBase = defaultControlFile(1:indexFind-1);
controlFileSuffix = defaultControlFile(indexFind+length(defaultKeyword):end);

fitSettingsDefaultFileID = fopen([controlFilesPath defaultControlFile], 'r');

%iterate over all TEs
for index = 1:length(orderTIsLong)
    %iterate over all subjects, if we have it set. Otherwise the loop is executed only once
    for indexSubject = 1:numOfSubjects
        if fitAllSubjects
            % add the subject name to the output path, and the control file name
            subjectControlFileBase = strrep(controlFileBase, defaultSubject, subjects{indexSubject});
            outputFile = strcat(subjectsControlFilesPath{indexSubject}, subjectControlFileBase, orderTIsLong{index}, controlFileSuffix);
        else
            outputFile = strcat(controlFilesPath, controlFileBase, orderTIsLong{index}, controlFileSuffix);
        end
        fitSettingsFileID = fopen(outputFile, 'w');
        display(outputFile);

        while(~feof(fitSettingsDefaultFileID))
            s = fgetl(fitSettingsDefaultFileID);
            s = strrep(s, defaultLCModelUser, currentLCModelUser);
            s = strrep(s, defaultTI, orderTIsLong{index});
            if fitAllSubjects
                s = strrep(s, defaultSubject, subjects{indexSubject});
            end
%             if (index > 3)
%                 if (~isempty(strfind(s,'MM27')))
%                     s = strrep(s, 'AMP= 1', 'AMP= -1');
%                 end
%             end
            fprintf(fitSettingsFileID,'%s\n', s);
        end
        frewind(fitSettingsDefaultFileID);
        fclose(fitSettingsFileID);
    end
end
fclose(fitSettingsDefaultFileID);
end