function this = Denoise(this)
%First Version: 1st-March-2016 
%
%
%
%Check all the flags fisrt. It would be good if not any process step has
%been applied before the denoise procedure 
 
 
 isAveraged      = this.Parameter.ReconFlags.isAveraged;
 isCoilsCombined = this.Parameter.ReconFlags.isCoilsCombined;
 isZeroFilled    = this.Parameter.ReconFlags.isZeroFilled;
 isDenoised      = this.Parameter.ReconFlags.isDenoised;
 
 
 if (~isAveraged && ~isCoilsCombined && ~isZeroFilled && ~isDenoised )
 

           oldData         = this.Data{1} ;
           denoisedData    = zeros(size(oldData));
          
           
           nCoils          = size(oldData,this.coil_dim);
           nNoisePoints    = 150;
           
           noiseData    = oldData(end-nNoisePoints+1:end,:,:,:,:,:,:,:,:,:,:,:);
           noiseData    = reshape(noiseData,[],nCoils);
           
 
           covMatrix    = cov(noiseData);
           figure
           set(gcf,'Name','Before','NumberTitle','off')
           for i = 1:nCoils
              subplot(4,ceil(nCoils/4),i)
              hist(real(noiseData(:,i)),100)
           end
           
           
          
           
           figure
           psi = covMatrix.';
           scaling2 = sqrt(diag(psi));
           psi2 = diag(1./scaling2)*psi*diag(1./scaling2);
           corr_mtx = abs(psi2)-diag(diag(psi2));   % Noise correlation matrix
           corr_mtx_dB = (20*log10(corr_mtx));
           
           subplot(1,2,1)
           imagesc(corr_mtx_dB)
           title('Before')
           
           %% That is the main part of the function!
           
           [X,D]        = eig(covMatrix); 
           W            = X*sqrt(2*D);
           
           dims = size(oldData);
           dims(this.coil_dim)=1;
           for i = 1:nCoils
               for j= 1:nCoils
                   
                   wij          = W(i,j)*ones(dims);
                   denoisedData(:,:,:,i,:,:,:,:,:,:,:,:) = wij.*oldData(:,:,:,j,:,:,:,:,:,:,:,:)+...
                       denoisedData(:,:,:,i,:,:,:,:,:,:,:,:);
               end
           end
           
           this.Data{1}                               = denoisedData;
           this.Parameter.ReconFlags.isDenoised       = true;
           
           
           
           %%
           
             oldData         = this.Data{1} ;     
             noiseData    = oldData(end-nNoisePoints+1:end,:,:,:,:,:,:,:,:,:,:,:);
             noiseData    = reshape(noiseData,[],nCoils);
             
             
         
             
             covMatrix    = cov(noiseData);
             
             
             
             psi = covMatrix.';
             scaling2 = sqrt(diag(psi));
             psi2 = diag(1./scaling2)*psi*diag(1./scaling2);
             corr_mtx = abs(psi2)-diag(diag(psi2));   % Noise correlation matrix
             corr_mtx_dB = (20*log10(corr_mtx));

             subplot(1,2,2)
             imagesc(corr_mtx_dB)
             title('After')
             
             
             
           figure
            set(gcf,'Name','After','NumberTitle','off')
           for i = 1:nCoils
              subplot(4,ceil(nCoils/4),i)
              hist(real(noiseData(:,i)),100)
           end
           

           
                
          
           
           
 else
     warning('Data are already processed. So denoising is skipped...')
           
           
 end
               
      
   
end
    






