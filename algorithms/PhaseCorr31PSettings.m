classdef PhaseCorr31PSettings 
% version: 21 Aug 2018
% phases in degrees

    properties
        sing
        phases    
    end
    
    methods
        function this=PhaseCorr31PSettings
            this.sing   = [0];
            this.phases = [0];            
        end
    end
    
end

