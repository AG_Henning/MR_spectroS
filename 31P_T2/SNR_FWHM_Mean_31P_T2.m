function SNR_FWHM_Mean_31P_T2()
clear; clc; close all;

%%%%%%%%
%SNR and FWHM of raw data, not fitted data


pathName = 'T2 31P Paper Data';
% sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName);

subjects = {'9873';'1090';'3490';'9810';'1658';'2017';'6524';'3009';...
    '6260';'2823';'2455';'4085'};
prefixes = {'2019-09-30_';'2019-10-10_';'2019-10-14_';'2019-10-14_';...
    '2019-10-22_';'2019-11-07_';'2019-11-07_';'2019-11-20_';...
    '2019-11-20_';'2019-12-05_';'2020-07-22_';'2020-07-23_';};
% excludeSubjects = {'5166';'9810';'5269'};
suffixFolder = '_SurfCoil';

trunc_ms = 90;
TruncToFit = 'noTrunc';        %'noTrunc';   %'Trunc90ms';

%be careful with Pi intra/extra, NADH and bATP!
metaboliteNames = {'PE', 'PC', 'Pi_int', 'GPE', 'GPC', 'PCr', 'gATP', 'aATP', 'NADH'};
centerPpm =       [ 6.77, 6.23, 4.83,     3.51,   2.96,  0,     -2.48, -7.62,  -8.16];

TEs = {'6'; '8'; '11'; '15'; '20'; '30'; '50'; '80'; '150'};  %ms
numberOfSubjects = length(subjects);
paths = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    paths{indexSubject} = [localFilePathBase prefixes{indexSubject} subjects{indexSubject} suffixFolder '\'];
end

numberOfTEs = length(TEs);

SNRs = zeros(numberOfSubjects,numberOfTEs,length(metaboliteNames));
FWHMs = zeros(numberOfSubjects,numberOfTEs,length(metaboliteNames));
for indexTE = 1:numberOfTEs
    for indexSubject = 1:numberOfSubjects
        load([paths{indexSubject},strcat('TE',num2str((TEs{indexTE})),'ms_SNR.mat')])   %saved as SNR
        if strcmp(TruncToFit,'noTrunc')
            SNRs(indexSubject,indexTE,:) = table2array(SNR(1,:));    %subjects x TEs x SNR
            FWHMs(indexSubject,indexTE,:) = table2array(SNR(2,:));    %subjects x TEs x FWHM
        else
            SNRs(indexSubject,indexTE,:) = table2array(SNR(3,:));    %subjects x TEs x SNR
            FWHMs(indexSubject,indexTE,:) = table2array(SNR(4,:));    %subjects x TEs x FWHM
        end
    end
end

for indexTE = 1:numberOfTEs
    SNR_Mean(indexTE,:) = nanmean(squeeze(SNRs(:,indexTE,:)),1);
    SNR_Std(indexTE,:) = nanstd(squeeze(SNRs(:,indexTE,:)),1);
    FWHM_Mean(indexTE,:) = nanmean(squeeze(FWHMs(:,indexTE,:)),1);
    FWHM_Std(indexTE,:) = nanstd(squeeze(FWHMs(:,indexTE,:)),1);
end

SummaryTable = zeros(4*numberOfTEs,length(metaboliteNames));
s = 1;
for indexTE = 1:numberOfTEs
    SummaryTable(s,:) = SNR_Mean(indexTE,:);
    SummaryTable(s+1,:) = SNR_Std(indexTE,:);
    SummaryTable(s+2,:) = FWHM_Mean(indexTE,:);
    SummaryTable(s+3,:) = FWHM_Std(indexTE,:);
    s = s+4;
end

s = 1;
for indexTE = 1:numberOfTEs
    snrValues = [SummaryTable(s,1) SummaryTable(s,2) SummaryTable(s,3) SummaryTable(s,4) SummaryTable(s,5) SummaryTable(s,6) SummaryTable(s,7) SummaryTable(s,8) SummaryTable(s,9);...
        SummaryTable(s+1,1) SummaryTable(s+1,2) SummaryTable(s+1,3) SummaryTable(s+1,4) SummaryTable(s+1,5) SummaryTable(s+1,6) SummaryTable(s+1,7) SummaryTable(s+1,8) SummaryTable(s+1,9);...
        SummaryTable(s+2,1) SummaryTable(s+2,2) SummaryTable(s+2,3) SummaryTable(s+2,4) SummaryTable(s+2,5) SummaryTable(s+2,6) SummaryTable(s+2,7) SummaryTable(s+2,8) SummaryTable(s+2,9);...
        SummaryTable(s+3,1) SummaryTable(s+3,2) SummaryTable(s+3,3) SummaryTable(s+3,4) SummaryTable(s+3,5) SummaryTable(s+3,6) SummaryTable(s+3,7) SummaryTable(s+3,8) SummaryTable(s+3,9) ];
    rowNames = {'SNR_Mean','SNR_Std','FWHM_Mean','FWHM_Std'};
    colNames = {'PE','PC','Piint','GPE','GPC','PCr','g_ATP','a_ATP','NADH'};
    SNR = array2table(snrValues,'RowNames',rowNames,'VariableNames',colNames)
    
    fileName = fullfile(strcat(localFilePathBase,'Output\'), strcat('TE', num2str(TEs{indexTE}), 'ms_SNR_MeanStd_',TruncToFit));
    save(fileName, 'SNR');
    s = s+4;
end
end