classdef Recon 
% version: 26 Feb 2014     
    
    properties
        
        EddyCurrentCorrection
        DcOffsetCorrection
        CoilCombination
        Averaging
        PhaseCorrection
        ZeroFilling
        Filtering
        
    end
    
    
    
    methods
        
        
        function this=Recon()
            
            this.EddyCurrentCorrection= 'No';
            this.DcOffsetCorrection= 'No';
            this.CoilCombination= 'No';
            this.Averaging= 'No';
            this.PhaseCorrection= 'No';
            this.ZeroFilling= 'No';
            this.Filtering= 'No';
            
        end
        
    end
    
end
            
        