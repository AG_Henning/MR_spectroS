function compareTableConcentrations_MM(tableConcentrations, tableConcentrationsRef)
numOfSubjects = length(tableConcentrations);
for indexSubject = 1:numOfSubjects
    CRLBsSubject = tableConcentrations{indexSubject}(2:6,3:3:end);
    CRLBsSubjectRef = tableConcentrationsRef{indexSubject}(2:6,3:3:end);
    diffCRLBs = cell2mat(CRLBsSubject) - cell2mat(CRLBsSubjectRef)

    ConcSubject = tableConcentrations{indexSubject}(2:6,2:3:end);
    ConcSubjectRef = tableConcentrationsRef{indexSubject}(2:6,2:3:end);
    diffConc = cell2mat(ConcSubject) - cell2mat(ConcSubjectRef)
end