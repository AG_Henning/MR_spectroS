function plotSimulationResults2()


%% path to the export of the spectra
exportFolder = {'11 Simulations'};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolder);
dataExportPathBase = strcat(pathBaseExportFiles, exportFolder{1}, '\');

paramKeyword = 'conc';
truncSuffix = '_truncOn';
iterationProFit = 4;
if contains(paramKeyword,'conc')
    scalingOn = true;
else
    scalingOn = false;
end
% name of metabolites in LCModel
metabolitesLCModel     = {'Asp', 'Cr_CH2', 'Cr', 'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA_as', 'NAA_ac', 'NAAG', 'tCho_P',  'Scyllo',    'Tau', 'Leu'};

% name of metabolites in ProFit
metabolitesProFit     = {'Asp', 'Cr_CH2', 'Cr', 'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA_as', 'NAA_ac', 'NAAG', 'tCho_P',  'Scyllo',    'Tau', 'Leu'};

%% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');

%% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;

%% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;

plotCorrelations = true;
plotColors = {[0 0 1], [1 0 0]};
% plotColors2 = {[0.0 0.4 0.0], [1 0.5 0.3]};
plotColors2 = {[0.0 0.4 0.0], [0 0 0], [1 0.5 0.3], [0.49 0.18 0.56]};
offsetPlot = 0.2;

plotCorrelations = false;
plotPrecision = true;

%%
paramKeyword = 'baseline';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, '_dkntmn0.15', truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = -0.4;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, figIds, figIdsCorr, meanError_015] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors2{3}, offsetPlot, [], []);
%%
paramKeyword = 'baseline';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, '_dkntmn0.25', truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = 0.;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, figIds, figIdsCorr, meanError_025] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors2{4}, offsetPlot, figIds, []);
%%
paramKeyword = 'baseline';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, '_dkntmn0.5', truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = 0.4;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, figIds, figIdsCorr, meanError_05] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors2{2}, offsetPlot, figIds, []);

txt = {'LCModel Setup/error', ['orange: dkntmn=0.15 / ', num2str(meanError_015,3)], ...
    ['purple: dkntmn=0.25 / ', num2str(meanError_025,3)], [' black: dkntmn=0.5 / ', num2str(meanError_05,3)]};
title(txt, 'Interpreter','latex','FontSize', 14)
end

function [allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, figIds, figIdsCorr, meanError] = ...
    getConcentrations(metabolites, metabolitesLCModel, metabolitesProFit, ...
        concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
        scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
        plotPrecision, plotColors, offsetPlot, figIds, figIdsCorr)
    
    iterationProFit = 4;
    if isempty(figIdsCorr)
        emptyFigureIdsCorr = true;
        figIdsCorr = cell(1,length(metabolites));
    else
        emptyFigureIdsCorr = false;
    end
    allSimulMetConc = concentrationsRm;
    allLCModelMetConc = zeros(size(allSimulMetConc));
    allProFitMetConc = zeros(size(allSimulMetConc));
    allLCModelMetConcDev = zeros(size(allSimulMetConc));
    allProFitMetConcDev = zeros(size(allSimulMetConc));
    for indexMetabolite = 1:length(metabolites)
        simulatedMetConc = concentrationsRm(:, indexMetabolite);
        indexMetSim = find(strcmp(metabolites,'tCr(CH3)')); %'MMB'
        fittedMetConcLCModel = zeros(size(simulatedMetConc));
        for indexFit = 1:length(concentrationsLCModel)
            currentConcentrationsLCModel = concentrationsLCModel{indexFit};
            indexMetLCModel = find(strcmp(currentConcentrationsLCModel(1,:),'Cr')); %'Leu'
            MMBConc = currentConcentrationsLCModel{2,indexMetLCModel};
            MMBConcSim = concentrationsRm(indexFit,indexMetSim);
            if scalingOn
                scaling = MMBConcSim/MMBConc;
            else
                scaling = 1;
            end
            indexMetLCModel = find(strcmp(currentConcentrationsLCModel(1,:),metabolitesLCModel(indexMetabolite)));
            fittedMetConcLCModel(indexFit) = currentConcentrationsLCModel{2,indexMetLCModel}*scaling;
        end
        fittedMetConcProFit = zeros(size(simulatedMetConc));
%         for indexFit = 1:length(concentrationsProFit)
%             currentConcentrationsProFit = concentrationsProFit{indexFit}{iterationProFit};
%             indexMetProFit = find(strcmp(activeMetabolites{iterationProFit},'Cr')); %'Leu'
%             MMBConc = currentConcentrationsProFit(indexMetProFit);
%             MMBConcSim = concentrationsRm(indexFit,indexMetSim);
% %             if scalingOn
% %                 scaling = MMBConcSim/MMBConc;
% %             else
%                 scaling = 1;
% %             end
%             indexMetProFit = find(strcmp(activeMetabolites{iterationProFit},metabolitesProFit(indexMetabolite)));
%             fittedMetConcProFit(indexFit) = currentConcentrationsProFit(indexMetProFit)*scaling;
%         end
%         if strcmp(metabolitesProFit(indexMetabolite),'Leu')
%             fittedMetConcProFit = fittedMetConcProFit *1e8;
%         end
        allLCModelMetConc(:, indexMetabolite) = fittedMetConcLCModel;
        allProFitMetConc(:, indexMetabolite)  = fittedMetConcProFit;
        allLCModelMetConcDev(:, indexMetabolite) = (simulatedMetConc-fittedMetConcLCModel)./simulatedMetConc.*100;
        allProFitMetConcDev(:, indexMetabolite) = (simulatedMetConc-fittedMetConcProFit)./simulatedMetConc.*100;
        if plotCorrelations
            if emptyFigureIdsCorr
                figIdsCorr{indexMetabolite} = figure;
            end
            figure(figIdsCorr{indexMetabolite});
            hold on
            scatter(simulatedMetConc, fittedMetConcLCModel);
            scatter(simulatedMetConc, fittedMetConcProFit, 'x'); % ###

%             if ~emptyFigureIdsCorr % ### delete this if later
                plot(simulatedMetConc,simulatedMetConc);           
%             end
            title(metabolites(indexMetabolite))
            xlabel('Simulated concentrations [mmol/kg]');
            ylabel('Fitted concentrations [mmol/kg]');
            legend('LCModel concentrations', 'ProFit concentrations', 'Identity Line', 'Location', 'Northwest') %##
%             legend('LCModel concentrations', 'Identity Line', 'Location', 'Northwest')
%             legend('\nu_e = all identical T_2', '\nu_e = metabolite specific T_2',  'Identity Line', 'Location', 'Northwest')
            xl = xlim;
            if xl(2) > max(simulatedMetConc)
                xl(2) = max(simulatedMetConc);
            end
            xlim(xl);
                
            pathName = [dataExportPathBase, paramKeyword, '/correlations/'];
            if ~exist(pathName, 'dir')
                mkdir(pathName);
            end
            fileName = [metabolites{indexMetabolite}, '_', paramKeyword, truncSuffix];
            saveas(figIdsCorr{indexMetabolite}, [pathName, fileName, '.tif']);
            savefig(figIdsCorr{indexMetabolite}, [pathName,fileName, '.fig'],'compact');
%                 BlandAltman(simulatedMetConc, fittedMetConcLCModel);
%                 title(metabolites(indexMetabolite));
%                 BlandAltman(simulatedMetConc, fittedMetConcProFit);
%                 title(metabolites(indexMetabolite));
        end
    end
    
    allLCModelMetConcDev = allLCModelMetConcDev - median(allLCModelMetConcDev,1, 'omitnan');
    allProFitMetConcDev = allProFitMetConcDev - median(allProFitMetConcDev,1, 'omitnan');
    meanError =0;
    if plotPrecision
        yLimValue = 50;
        numberOfMet = length(metabolites);
        if isempty(figIds)
            figIds{1} = figure;
            figIds{2} = figure;
        end
        positionsTicks = [3:1.5:1.5*numberOfMet+1.5];
        positions = positionsTicks + offsetPlot;
        figure(figIds{1})
        hold on
        boxPlotLCModel = boxplot(allLCModelMetConcDev, metabolites,'colors',plotColors,'symbol','+', 'positions',positions,'width',0.3);
        xtickangle(45)
        title('LCModel')
        ylim([-yLimValue,yLimValue]);
        xlim([positionsTicks(1)-1, positionsTicks(end)+1])
        ylabel('Concentration change [%]')
        set(boxPlotLCModel(:,:),'linewidth',1);
        meanLCModelMetConcDev = mean(abs(allLCModelMetConcDev),1, 'omitnan');
        meanError = mean(meanLCModelMetConcDev)
    end
end
