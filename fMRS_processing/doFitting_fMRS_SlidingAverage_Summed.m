function doFitting_fMRS_SlidingAverage_Summed()
clear; clc; close all;

fitversion = 'v2';
pathName = 'fMRS SlidingAverage data path';
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);

visualType = 'Stim';      %'Stim', 'Rest'
BOLDType = '_woBOLDCorr';  %_woBOLDCorr128, _withBOLDCorr128, _woBOLDCorr64, _withBOLDCorr64
if strcmp(fitversion,'v2')
    MMBType = '_MMBSummed_v2';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated
else
    MMBType = '_MMBSummed';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated
end
nOfSpectra = 32;   %how many spectra should be averaged? Need full PC+MC
nslidingAv = 8;    %how many spectra should be skipped before next averaging
nBlocks = (320 - nOfSpectra)/nslidingAv + 1;    %here 320 averages per .dat file acquired. nBlocks = #spectra after sliding average

BlockNo = cell(1,nBlocks);
for k = 1:nBlocks
    BlockNo{k} = strcat('_Av', num2str(k));
end

% subjects = {'2823';};
subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
    '4085';'3333';'4012';'4085';'6524'};
% prefixes = {'2020-02-18_';};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
    '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
    '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
excludeSubjects = {'5166';'9810';'5269'};
suffixFolder = '_fMRS';

if exist('excludeSubjects','var')
    n = length(subjects);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(subjects(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    subjects([excludeIdx],:) = [];
    prefixes([excludeIdx],:) = [];
end
numberOfSubjects = length(subjects);

extensionTable = '.table';
extensionCoord = '.coord';
defaultVisual = 'Stim';
defaultSubject = 'XXXX';
defaultSubjectsPath = 'YYYY';
if fitversion == 'v2'
    defaultMMB = '_MMBSummed_v2';
else
    defaultMMB = '_MMBSummed';
end
defaultLCModelUser = 'jdorst';
defaultWaterRef = 'XXXX_WRef';

if fitversion == 'v2'
    controlFilesBase = 'fitsettings_v2_XXXX_';
else
    controlFilesBase = 'fitsettings_XXXX_';
end
controlFilesBaseSuffix = '.control';
outputFileNameBase = strcat(defaultSubject, '_');
outputFileNameBase_WRef = strcat(defaultSubject, '_WRef');

pathNameBaseline = 'fMRS TimeCourse data path';
sampleCriteriaBaseline = {'Output_v2_MMBSummed', 'Output', 'LCModelConfig'};
[localFilePathBaseBaseline] = pathToDataFolder(pathNameBaseline, sampleCriteriaBaseline);
if fitversion == 'v2'
    localBaselinePath = [localFilePathBaseBaseline, 'Output_v2_MMBSummed/'];
    BaselineFiles = strcat('BaselineConc_',num2str(visualType), BOLDType, MMBType);
else
    localBaselinePath = [localFilePathBaseBaseline, 'Output/'];
    BaselineFiles = strcat('BaselineConc_',num2str(visualType), BOLDType, MMBType);
end

%% file paths setup
controlFilesPathRemote = '/Desktop/1H_fMRS/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/1H_fMRS/Output/';
LCModelOutputPath = [localFilePathBase, 'Output/'];
defaultControlFile = strcat(controlFilesBase, defaultVisual, controlFilesBaseSuffix);

subjectsPath = strcat(prefixes, subjects, suffixFolder);
summedPath = 'Summed_fMRSData';

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, summedPath, '/');
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, summedPath, '/');
preprocessedFilesPathLocal = strcat(localFilePathBase, summedPath, '/');
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, summedPath, '/');
subjectsLCModelOutputPath = strcat(LCModelOutputPath, summedPath, '/');


%% sum fMRS spectra of all volunteers, weighted with water fit (need to run 
%doFitting_fMRS_Water.m & doFitting_fMRS_SlidingAverage.m first)
% WaterReferenceTable = (readtable(strcat(LCModelOutputPath,'WaterReference_Fit.xlsx')));
% WaterReferenceID = table2cell(WaterReferenceTable(2:end,1));
% WaterReference = str2double(table2array(WaterReferenceTable(2:end,2:end)));
% for indexCurrentSubject = 1:numberOfSubjects
%     for nBl = 1:nBlocks
%         idx = find(ismember(WaterReferenceID, strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject})));
%         currentScalingFactor = WaterReference(idx,1);
%         currentfMRSDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject},'\');
%         currentDataFile = strcat(subjects{indexCurrentSubject},'_',visualType,'_woBOLDCorr','_Av',num2str(nBl),'.mat');
%         load([currentfMRSDataPath,currentDataFile]);
%         currentfMRSData = squeeze(this.Data{1});
%         currentfMRSDataScaled = currentfMRSData./currentScalingFactor;
%         fMRSDataScaled(:,:,indexCurrentSubject,nBl) = currentfMRSDataScaled;
%     end
% end
% %sum over all subjects
% fMRSData_Summed = squeeze(sum(fMRSDataScaled,3));
% %save summed data
% for nBl = 1:nBlocks
%     current_fMRSData_Summed = fMRSData_Summed(:,:,nBl);
%     current_fMRSData_Summed = permute(current_fMRSData_Summed,[1 3 4 5 6 7 8 9 2]);
%     this.Data{1} = current_fMRSData_Summed;
%     fMRSData_Summed_Path = strcat(localFilePathBase,summedPath);
%     this.Parameter.Filepath = fMRSData_Summed_Path;
%     this.Parameter.Filename = strcat('fMRSData_Summed_',visualType,BOLDType,'_SlidingAverages');
%     if exist('excludeSubjects','var')
%         this.Parameter.Filename = strcat('fMRSData_Summed_',visualType,BOLDType,'_SlidingAverages_Excluded_Av',num2str(nBl));
%         exportFile = strcat(fMRSData_Summed_Path,'\Summed_',visualType,BOLDType,'_Excluded_Av',num2str(nBl));
%     else
%         this.Parameter.Filename = strcat('fMRSData_Summed_',visualType,BOLDType,'_SlidingAverages_Av',num2str(nBl));
%         exportFile = strcat(fMRSData_Summed_Path,'\Summed_',visualType,BOLDType,'_Av',num2str(nBl));
%     end
%     save(strcat(exportFile,'.mat'),'this')
%     this.ExportLcmRaw('',exportFile, 'Yes', 'Yes');
% end

%% sum Water reference spectra of all volunteers, weighted with water fit (need to run doFitting_fMRS_Water.m first)
% WaterReferenceTable = (readtable(strcat(LCModelOutputPath,'WaterReference_Fit.xlsx')));
% WaterReferenceID = table2cell(WaterReferenceTable(2:end,1));
% WaterReference = str2double(table2array(WaterReferenceTable(2:end,2:end)));
% WRefDataScaled = zeros(4096,numberOfSubjects);    %samples, subjects, data/water
% for indexCurrentSubject = 1:numberOfSubjects
%     idx = find(ismember(WaterReferenceID, strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject})));
%     currentScalingFactor = WaterReference(idx,1);
%     currentWRefDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject});
%     load(strrep(strcat(currentWRefDataPath, '\',outputFileNameBase_WRef,'.mat'),defaultSubject,subjects{indexCurrentSubject}));
%     currentfMRSData_WRef = squeeze(a.Data{1});
%     currentfMRSDataScaled_WRef = currentfMRSData_WRef./currentScalingFactor;
%     WRefDataScaled(:,indexCurrentSubject) = currentfMRSDataScaled_WRef;
% end
% %sum over all subjects
% fMRSData_Summed_WRef = squeeze(sum(WRefDataScaled,2));
% %save summed data
% this = a;
% this.Data{1} = fMRSData_Summed_WRef;
% this.Parameter.Filepath = fMRSData_Summed_Path;
% this.Parameter.Filename = 'fMRSData_Summed_WRef';
% if exist('excludeSubjects','var')
%     exportFileWRef = strcat(fMRSData_Summed_Path,'\WRef_Summed_Excluded');
% else
%     exportFileWRef = strcat(fMRSData_Summed_Path,'\WRef_Summed');
% end
% save(strcat(exportFileWRef,'.mat'),'this')
% this.ExportLcmRaw('',exportFileWRef, 'Yes', 'Yes');

%% do the LCModel fitting
if exist('excludeSubjects','var')
    LCModelOutputFiles = strcat('Summed_', visualType, MMBType, '_Excluded', BlockNo);
    LCModelOutputFileswoBaseline = strcat('Summed_', visualType, MMBType, '_Excluded', BlockNo, '_woBaseline');
    LCModelControlFiles = strcat(controlFilesBase, 'Summed_', visualType, MMBType, '_Excluded', BlockNo, controlFilesBaseSuffix);
    LCModelControlFileswoBaseline = strcat(controlFilesBase, 'Summed_', visualType, MMBType, '_Excluded', BlockNo, '_woBaseline', controlFilesBaseSuffix);
    waterRefFileName = 'WRef_Summed_Excluded';
else
    LCModelOutputFiles = strcat('Summed_', visualType, MMBType, BlockNo);
    LCModelOutputFileswoBaseline = strcat('Summed_', visualType, MMBType, BlockNo, '_woBaseline');
    LCModelControlFiles = strcat(controlFilesBase, 'Summed_', visualType, MMBType, BlockNo, controlFilesBaseSuffix);
    LCModelControlFileswoBaseline = strcat(controlFilesBase, 'Summed_', visualType, MMBType, BlockNo, '_woBaseline', controlFilesBaseSuffix);
    waterRefFileName = 'WRef_Summed';
end


%% do the LCModel fitting
% LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
% 
% LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote, subjectsLCModelOutputFilesRemote, ...
%     subjectsLCModelOutputPath);
% 
% if strcmp(fitversion,'v2')
%     currentControlFiles = strrep(LCModelControlFiles, strcat('_v2_',defaultSubject), '');
% else
%     currentControlFiles = strrep(LCModelControlFiles, strcat('_',defaultSubject), '');
% end
% currentOutputFiles = strrep(LCModelOutputFiles,'Corr','Corr_v2');
% 
% 
% % create the control file series
% for nBl = 1:nBlocks
%     currentOutputFile = currentOutputFiles{nBl};
%     if exist('excludeSubjects','var')
%         currentFMRS = strcat(visualType,BOLDType,'_Excluded');
%     else
%         currentFMRS = strcat(visualType,BOLDType);
%     end
%     createLCModelConfigfMRSTimeCourse(controlFilesPathLocal, subjectsControlFilesPathLocal, defaultControlFile, ...
%         currentFMRS, defaultVisual, currentOutputFile,...
%         waterRefFileName, defaultWaterRef, 'Summed_fMRSData', defaultSubjectsPath, 'Summed',...
%         defaultSubject, MMBType, defaultMMB, BlockNo{nBl});
% end
% if exist('excludeSubjects','var')
%     currentOutputFilesLCModel = strcat('Summed_',visualType,BOLDType,'_Excluded',MMBType,BlockNo);
% else
%     currentOutputFilesLCModel = strcat('Summed_',visualType,BOLDType,MMBType,BlockNo);
% end
% fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFilesLCModel, subjectsControlFilesPathRemote, ...
%     subjectsControlFilesPathLocal, preprocessedFilesPathLocal);

%%
%%% doesn't exist in Data_TimeCourse, but same as nBl = 5 (33-64 average)
% % Create and save table with baseline concentrations fitted with baseline, taken from second block of first REST
% createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
%     LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,0)

% % Create and save table from fitted concentrations with baseline, taken from
% % all blocks    
% if exist('excludeSubjects','var')
%     createConcTableBlock(BlockNo,currentOutputFilesLCModel,defaultSubject,'Summed',...
%         LCModelOutputPath,'Summed_fMRSData',visualType,strcat(BOLDType,'_Excl'),MMBType,0)
% else
%     createConcTableBlock(BlockNo,currentOutputFilesLCModel,defaultSubject,'Summed',...
%         LCModelOutputPath,'Summed_fMRSData',visualType,BOLDType,MMBType,0)
% end

%% subtract baseline from difference spectrum and fit again
%only useful for MMBSummed
if strcmp(MMBType(1:10),'_MMBSummed')
    %% do the LCModel fitting
%     LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
%     
%     LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote, subjectsLCModelOutputFilesRemote, ...
%         subjectsLCModelOutputPath);
%     %
%     if fitversion == 'v2'
%         currentControlFileswoBaseline = strrep(LCModelControlFileswoBaseline, strcat('_v2_',defaultSubject), '');
%     else
%         currentControlFileswoBaseline = strrep(LCModelControlFileswoBaseline, strcat('_',defaultSubject), '');
%     end
%     if exist('excludeSubjects','var')
%         currentDataFiles = strrep(strcat(outputFileNameBase, visualType, BOLDType, '_Excluded', BlockNo), defaultSubject, 'Summed');
%     else
%         currentDataFiles = strrep(strcat(outputFileNameBase, visualType, BOLDType, BlockNo), defaultSubject, 'Summed');
%     end
%     
%     if exist('excludeSubjects','var')
%         currentOutputFileswoBaseline = strcat('Summed_',visualType,BOLDType,'_Excluded',MMBType,BlockNo);
%     else
%         currentOutputFileswoBaseline = strcat('Summed_',visualType,BOLDType,MMBType,BlockNo);
%     end
%     currentDataPath = strcat(localFilePathBase,'Summed_fMRSData\');
%     for nBl = 1:nBlocks
%         %check save option for .mat file in subtractBaseline_fMRS
%         subtractBaseline_fMRS(currentDataPath,subjectsLCModelOutputPath,currentDataFiles{nBl},currentOutputFileswoBaseline{nBl},'NAA')
%         currentFitsettingsOutputFileswoBaseline = LCModelOutputFileswoBaseline{nBl};
%         if exist('excludeSubjects','var')
%             currentFMRS = strcat(visualType,BOLDType,'_Excluded');
%         else
%             currentFMRS = strcat(visualType,BOLDType);
%         end
%         createLCModelConfigfMRSTimeCourse(controlFilesPathLocal, subjectsControlFilesPathLocal, defaultControlFile, ...
%             currentFMRS, defaultVisual, currentFitsettingsOutputFileswoBaseline,...
%             waterRefFileName, defaultWaterRef, 'Summed_fMRSData', defaultSubjectsPath, 'Summed',...
%             defaultSubject, MMBType, defaultMMB, strcat(BlockNo{nBl}, '_woBaseline'));
%     end
%     if exist('excludeSubjects','var')
%         currentOutputFilesLCModelwoBaseline = strcat('Summed_',visualType,BOLDType,'_Excluded',MMBType,BlockNo,'_woBaseline');
%     else
%         currentOutputFilesLCModelwoBaseline = strcat('Summed_',visualType,BOLDType,MMBType,BlockNo,'_woBaseline');
%     end
%     fittingLCModel(LCModelCallerInstance, currentControlFileswoBaseline, currentOutputFilesLCModelwoBaseline, subjectsControlFilesPathRemote, ...
%         subjectsControlFilesPathLocal, preprocessedFilesPathLocal);
    
    %% not useful here, take it from Data_TimeCourse
    % Create and save table with baseline concentrations fitted w/o baseline, taken from second block of first REST
    % createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
    %     LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,1)
    
    %     Create and save table from fitted concentrations w/o baseline, taken from all blocks
%     if exist('excludeSubjects','var')
%         createConcTableBlock(BlockNo,currentOutputFilesLCModelwoBaseline,defaultSubject,'Summed',...
%             LCModelOutputPath,'Summed_fMRSData',visualType,strcat(BOLDType,'_Excl'),MMBType,1)
%     else
%         createConcTableBlock(BlockNo,currentOutputFilesLCModelwoBaseline,defaultSubject,'Summed',...
%             LCModelOutputPath,'Summed_fMRSData',visualType,BOLDType,MMBType,1)
%     end
end


%% create concentration time course from fit with and w/o Baseline
if exist('excludeSubjects','var')
    ExpName = strcat('BlockConc_Summed_', num2str(visualType), strcat(BOLDType,'_Excl'), MMBType);
else
    ExpName = strcat('BlockConc_Summed_', num2str(visualType), BOLDType, MMBType);
end

% createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,'PCr',0)
% createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,'Cr',0)

createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,'Cr',0)
createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,'PCr',0)
if strcmp(MMBType(1:10),'_MMBSummed')
    createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,'PCr',1)
    createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,'Cr',1)
    
    createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,'Cr',1)
    createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,'PCr',1)
end

close all

end

function createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,metabToFig,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
ExpName = strcat(ExpName, woBaseline);
exportFileMat = strcat(subjectsLCModelOutputPath,ExpName,'.mat');

tmp = load(exportFileMat);
dataTimeCourse(:,:) = tmp.(ExpName);

metaboliteNames = dataTimeCourse(1,:);
indexMet = strcmp(metaboliteNames,metabToFig);
idxMet = find(indexMet == true);

%Baseline concentration, equals 5th sliding average
baselineConcIdxMet = dataTimeCourse{6,idxMet};

[n,~] = size(dataTimeCourse);
for k = 2:n
    MetConc(k-1) = dataTimeCourse(k,idxMet(1));
end

%calculate concentration change of data points with reference to baseline
[~,b] = size(MetConc);
for av = 1:b
    PercChange(av) = (MetConc{1,av}-baselineConcIdxMet)/baselineConcIdxMet*100;
end

figure
x = linspace(16*5,(320*5-16*5),37)/60;
hold on
plot(x,PercChange)
hold on
plot([0,320*5/60],[0 0])
xlabel 't / min'; ylabel 'Concentration change / %.'
if ~isempty(strfind(ExpName,'Rest'))
    visualType = 'Rest';
else
    visualType = 'Stim';
end
title ([metabToFig,', ',ExpName], 'Interpreter', 'none')
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
xlim([0 320*5/60]);

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:5
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < 5
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:2.5
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
PathName = strcat(LCModelOutputPath,'SlAvTimeCourse/');

if ~isempty(strfind(ExpName,'Rest'))
    metabToFig = strcat(metabToFig,'_Rest_Summed');
else
    metabToFig = strcat(metabToFig,'_Stim_Summed');
end
if ~isempty(strfind(ExpName,'_v2'))
    if ~isempty(strfind(ExpName,'Excl'))
        fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '_PercChange_Excluded.fig'));
    else
        fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '_PercChange.fig'));
    end
else
    if ~isempty(strfind(ExpName,'Excl'))
        fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '_PercChange_Excluded.fig'));
    else
        fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '_PercChange.fig'));
    end
end
savefig(fig2save)
end

function createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,metabToFig,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
ExpName = strcat(ExpName, woBaseline);
exportFileMat = strcat(subjectsLCModelOutputPath,ExpName,'.mat');

tmp = load(exportFileMat);
dataTimeCourse(:,:) = tmp.(ExpName);

metaboliteNames = dataTimeCourse(1,:,1);
indexMet = strcmp(metaboliteNames,metabToFig);
idxMet = find(indexMet == true);

[n,~] = size(dataTimeCourse);
for k = 2:n
    MetConc(k-1) = dataTimeCourse(k,idxMet(1));
end

figure
x = linspace(16*5,(320*5-16*5),37)/60;
hold on
% errorbar(x,MetConc_Mean,MetConc_Std)
plot(x,cell2mat(MetConc))
xlabel 't / min'; ylabel 'Concentration / a.u.'
title ([metabToFig,', ',ExpName], 'Interpreter', 'none')
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
xlim([0 320*5/60]);

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:5
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < 5
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:2.5
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
PathName = strcat(LCModelOutputPath,'SlAvTimeCourse/');
if ~isempty(strfind(ExpName,'Rest'))
    metabToFig = strcat(metabToFig,'_Rest_Summed');
else
    metabToFig = strcat(metabToFig,'_Stim_Summed');
end
if ~isempty(strfind(ExpName,'_v2'))
    if ~isempty(strfind(ExpName,'Excl'))
        fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '_Excluded.fig'));
    else
        fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '.fig'));
    end
else
    if ~isempty(strfind(ExpName,'Excl'))
        fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '_Excluded.fig'));
    else
        fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '.fig'));
    end
end
savefig(fig2save)
end

% function createConcTimeCourseFig(ExpName,LCModelOutputPath,subjectsLCModelOutputPath,subjects,metabToFig,woBaseline)
% if exist('woBaseline', 'var') & woBaseline == true
%     woBaseline = '_woBaseline';
% else
%     woBaseline = '';
% end
% ExpName = strcat(ExpName, woBaseline);
% 
% exportFileMat = strcat(subjectsLCModelOutputPath,ExpName,'.mat');
% 
% for k = 1:numel(exportFileMat)
%     tmp = load(exportFileMat{k});
%     dataTimeCourse(:,:,k) = tmp.(ExpName{k});
% end
% 
% metaboliteNames = dataTimeCourse(1,:,1);
% indexMet = strcmp(metaboliteNames,metabToFig);
% % idxMet = find(not(cellfun('isempty',indexMet)));
% idxMet = find(indexMet == true);
% 
% [n,~,m] = size(dataTimeCourse);
% for k = 2:n
%     for l = 1:m
%         MetConc(k-1,l) = dataTimeCourse(k,idxMet(1),l);
%     end
% end
% 
% for k = 1:size(MetConc,1)
%     MetConc_Mean(k) = mean(cell2mat(MetConc(k,:)));
%     MetConc_Std(k)  = std(cell2mat(MetConc(k,:)));
% end
% 
% figure
% x = linspace(16*5,(320*5-16*5),37)/60;
% hold on;
% errorbar(x,MetConc_Mean,MetConc_Std)
% xlabel 't / min'; ylabel 'Concentration / a.u.'
% title ([metabToFig,' time course'])
% set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
% xlim([0 320*5/60]);
% 
% yLimits = get(gca,'YLim');
% boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
% for i = 1:5
%         boxoff  = [boxoff 320/60*i 320/60*i];
%         if i < 5
%         boxon   = [boxon 320/60*i 320/60*i];
%         end
% end
% for ii = 1:2.5
%     boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%     boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
% end
% patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
% patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
% ylim([yLimits(1) yLimits(2)]);
% PathName = strcat(LCModelOutputPath,'SlAvTimeCourse/');
% if ~isempty(find(~cellfun(@isempty,strfind(ExpName,'v2'))))
%     fig2save = fullfile(PathName, strcat(metabToFig, '_v2', woBaseline, '.fig'));
% else
%     fig2save = fullfile(PathName, strcat(metabToFig, woBaseline, '.fig'));
% end
% savefig(fig2save)
% end

function createConcTableBlock(BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
    LCModelOutputPath,subjectsPath,visualType,BOLDType,MMBType,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
tableConcentrations = [];
isFirstIter = 1;
s = 1;
    for nBl = 1:length(BlockNo)     
        %         retrieve parameters
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects);
        currentPath = strcat(LCModelOutputPath,subjectsPath,'/',currentOutputFiles{nBl},'.table');
        [c1 c2 c3 c4] = textread(currentPath,'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
            tableConcentrations{1,s} = 'water concentration';
        end
        
        %         add ID and Water conc to table
        tableConcentrations{nBl+1,1} = strcat(subjects, '_', visualType, BlockNo{nBl});
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        tableConcentrations{nBl+1,end} = wconc_LCModel;
        %         add metabolites to table
        s = 2;
        for j=start:finish
            %             add quantified metabolite
            tableConcentrations{nBl+1,s} = str2num( c1{j});
            %             add CRLB
            tableConcentrations{nBl+1,s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end

% save table to file
ExpName = strcat('BlockConc_', subjects, '_', num2str(visualType), BOLDType, MMBType, woBaseline);
exportFileXlsx = [LCModelOutputPath,subjectsPath,'/',strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath,subjectsPath,'/',strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)
end

function createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
    LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
tableConcentrations = [];
isFirstIter = 1;
s = 1;
for indexCurrentSubject = 1:numberOfSubjects
    for nBl = 2     %take baseline from second block of first REST
        % retrieve parameters
        currentOutputFiles = strrep(strcat(LCModelOutputFiles,woBaseline), defaultSubject, subjects{indexCurrentSubject});
        currentPath = strcat(LCModelOutputPath,subjectsPath{indexCurrentSubject},'/',currentOutputFiles{nBl},'.table');
        [c1 c2 c3 c4] = textread(currentPath,'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
            tableConcentrations{1,s} = 'water concentration';
        end
        
        % add ID and Water conc to table
        tableConcentrations{indexCurrentSubject+1,1} = strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject}, BlockNo{nBl});
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        tableConcentrations{indexCurrentSubject+1,end} = wconc_LCModel;
        % add metabolites to table
        s = 2;
        for j=start:finish
            %add quantified metabolite
            tableConcentrations{indexCurrentSubject+1,s} = str2num( c1{j});
            %add CRLB
            tableConcentrations{indexCurrentSubject+1,s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end
end

%calculate mean, std
[n,m] = size(tableConcentrations);
subArray = cell2mat(tableConcentrations(2:end,2:end));
meanValue = mean(subArray);
stdValue = std(subArray);
tableConcentrations{n+1,1} = 'mean';
tableConcentrations{n+2,1} = 'std';
for indexOfMetab = 1:m-1
    tableConcentrations{n+1,indexOfMetab+1} = meanValue(1,indexOfMetab);
    tableConcentrations{n+2,indexOfMetab+1} = stdValue(1,indexOfMetab);
end

% save table to file
ExpName = strcat('BaselineConc_', num2str(visualType), BOLDType, MMBType, woBaseline);
exportFileXlsx = [LCModelOutputPath ,strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)
end