function this = DeleteMovedAverages(this)

if ~this.Parameter.ReconFlags.isSorted
    error('The data has to be sorted first')
end


oldData                = this.Data{1};
nCoils                 = size(this.Data{1}, this.coil_dim);
nAverages              = size(this.Data{1}, this.meas_dim);
nMix                   = size(this.Data{1}, this.mix_dim);

displayPlots           = this.Parameter.DeleteMovedAveragesSettings.displayPlots;
confidenceNumOfStd     = this.Parameter.DeleteMovedAveragesSettings.confidenceNumOfStd; % default is 4

fprintf('Delete moved averages starts... Please wait\n');

if nMix  == 2
    selectedMix =  2;
else
    selectedMix =  1;
end

tmpData                            =  squeeze(oldData(:,1,1,:,1,1,1,1,selectedMix,1,1,:));
tmpData                            =  permute(tmpData,[1 3 2]); %permute data in order to order them average-wise
tmpData                            =  reshape(tmpData,size(tmpData,1),nAverages*nCoils); %reshape and find the max
[~, maxPosition]                   =  max(max(tmpData(1:30,:,:)));

selectedCoil                       =  floor(maxPosition/nAverages)+1;
selectedAveraged                   =  mod(maxPosition,nAverages);
if selectedAveraged == 0
    selectedAveraged = 1;
end

clc
fprintf('Selected #Average: %d and #Coil: %d\n',selectedAveraged, selectedCoil)

%//////////////////////////////////////////////////////////////////////////

tmpData                            =  squeeze(oldData(:,1,1,selectedCoil,1,1,1,1,selectedMix,1,1,:));

meanTmpData = mean(tmpData,2);
stdTmpData = std(tmpData,0,2);
upperConfidenceBound = meanTmpData + confidenceNumOfStd * stdTmpData;
lowerConfidenceBound = meanTmpData - confidenceNumOfStd * stdTmpData;

sumOffDataPoints = zeros(1,nAverages);
for indexOfAverage=1:nAverages
    %time domain
    offDataPoints = (tmpData(:,indexOfAverage) > upperConfidenceBound) | (tmpData(:,indexOfAverage) < lowerConfidenceBound);
    sumOffDataPoints(indexOfAverage) = sum(offDataPoints);  
end

averages4Delete = 1:nAverages;
averages4Delete = averages4Delete((sumOffDataPoints ~= 0));

if this.Parameter.ReconFlags.isMetaboliteCycling 
    %find the base
    mcAverages4Delete = floor((averages4Delete+1)/2);
    %remove the duplicate entry pairs
    mcAverages4Delete = unique(mcAverages4Delete);
    numMcAverages4Delete = length(mcAverages4Delete);
    %calculate the MC pairs
    newAverages4Delete(1:2:numMcAverages4Delete*2) = 2*mcAverages4Delete - 1;
    newAverages4Delete(2:2:numMcAverages4Delete*2) = 2*mcAverages4Delete;
    averages4Delete  = newAverages4Delete;
end

%/////////////////////////////////////////////////////////////////////////



%//////////////////////////////////////////////////////////////////////////
%                   Deletion of data averages
%//////////////////////////////////////////////////////////////////////////

nAverages4Delete            = length(averages4Delete);
newNAverages                = nAverages-nAverages4Delete;
newDataSize                 = size(oldData);
newDataSize(this.meas_dim)  = newNAverages;

newData                     =zeros(newDataSize);

averagesSum=0;
for indexOfAverage=1:nAverages
    if isempty( find(averages4Delete==indexOfAverage,1) )
        averagesSum=averagesSum+1;
        newData( :, 1, 1, :, 1, 1, 1, 1, :, :, 1, averagesSum )=...
            oldData( :, 1, 1, :, 1, 1, 1, 1, :, :, 1, indexOfAverage ) ;
    end
end

%//////////////////////////////////////////////////////////////////////////
%                             Display results
%//////////////////////////////////////////////////////////////////////////
if displayPlots
    
    figure
    
    preData = squeeze(oldData(:,1,1,selectedCoil,1,1,1,1,selectedMix,1,1,:));
    preFFTS = fftshift(fft(preData));
    preFFTS = preFFTS(length(preFFTS)/2-100:length(preFFTS)/2+100,:);
    
    postData = squeeze(newData(:,1,1,selectedCoil,1,1,1,1,selectedMix,1,1,:));
    postFFTS = fftshift(fft(postData));
    postFFTS = postFFTS(length(postFFTS)/2-100:length(postFFTS)/2+100,:);

    subplot(2,2,1)
    plot(real(preData))
    title('Before, TD')
    
    subplot(2,2,2)
    plot(real(preFFTS));
    title('Before, FD')
    
    subplot(2,2,3)
    plot(real(postData))
    title('After, TD')
    
    subplot(2,2,4)
    plot(real(postFFTS));
    title(['Deleted coils: ' num2str(averages4Delete) ])
end

%//////////////////////////////////////////////////////////////////////////
%                            Replace new data
%//////////////////////////////////////////////////////////////////////////

this.Data{1}                                    = newData;
this.Parameter.DeleteMovedAveragesSettings.deletedAverages = averages4Delete;
this.Parameter.ReconFlags.isDeletedMoved    = true;

clc
fprintf('Deleted moved...FINISHED')

end


