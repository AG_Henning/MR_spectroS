function calculate_conc_31P_T2()
clear; clc; close all;

pathName = 'T2 31P Paper Data';
[localFilePathBase] = pathToDataFolder(pathName);

outputPath = 'D:\PAPER\31P_T2_RelaxationTimes\Data\Output\';
% outputPath = 'D:\PAPER\31P_T2_RelaxationTimes\Data\Output_Real_Vespa_Original\';    
% outputPath = 'D:\PAPER\31P_T2_RelaxationTimes\Data\Output_Real_Vespa_Phased\';
% outputPath = 'D:\PAPER\31P_T2_RelaxationTimes\Data\Output_Real_Vespa_Phased_NormAbs400\';
% outputPath = 'D:\PAPER\31P_T2_RelaxationTimes\Data\Output_Real_Vespa_Phased_NormReal400\';
    
subjects = {'9873';'1090';'3490';'9810';'1658';'2017';'6524';'3009';...
    '6260';'2823';'2455';'4085'};
prefixes = {'2019-09-30_';'2019-10-10_';'2019-10-14_';'2019-10-14_';...
    '2019-10-22_';'2019-11-07_';'2019-11-07_';'2019-11-20_';...
    '2019-11-20_';'2019-12-05_';'2020-07-22_';'2020-07-23_';};
suffixFolder = '_SurfCoil';

% %be careful with Pi intra/extra, NADH and bATP!
% metaboliteNames = {'PE', 'PC', 'Pi_int', 'GPE', 'GPC', 'PCr', 'gATP', 'aATP', 'NADH'};
% centerPpm =       [ 6.77, 6.23, 4.83,     3.51,   2.96,  0,     -2.48, -7.62,  -8.16];

numberOfSubjects = length(subjects);
TR = 5000;  %ms
TM = 5;     %ms
TEs = {'6'; '8'; '11'; '15'; '20'; '30'; '50'; '80'; '150'};  %ms
numberOfTEs = length(TEs);

paths = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    paths{indexSubject} = [localFilePathBase prefixes{indexSubject} subjects{indexSubject} suffixFolder '\'];
end

% load T1 data from Pohmann, Proc. Intl. Soc. Mag. Reson. Med. 26 (2018) 3994
load('D:\PAPER\31P_T2_RelaxationTimes\Data\T1_Pohmann.mat')     %saved as T1_ms
T1 = table2array(T1_ms);

% load T2 data from this paper, calculated from summed spectra
% load('D:\PAPER\31P_T2_RelaxationTimes\Data\Output\Summed\T2_Results_Summed_STEAM_Ideal_LW_noTrunc.mat') %saved as tableT2toSave
load(strcat(outputPath,'Summed\T2_Results_Summed_STEAM_Real__LW_noTrunc.mat')) %saved as tableT2toSave
T2 = table2array(tableT2toSave(3,:));
metaboliteNames = tableT2toSave.Properties.VariableNames;

%load LCModel fit concentrations, saved as
%LCModelFit_STEAM_Ideal_TM5ms_LW_noTrunc. Columns 2-13 are results for 12
%volunteers for TE 6ms
% load('D:\PAPER\31P_T2_RelaxationTimes\Data\Output\LCModelFit_STEAM_Ideal_TM5ms_LW_noTrunc.mat')
load(strcat(outputPath,'LCModelFit_STEAM_Real_TM5ms_LW_noTrunc.mat'))

%load LCModel fit concentrations of summed spectra, saved as
%LCModelFit_Summed_STEAM_Ideal_TM5ms_LW_noTrunc.
% load('D:\PAPER\31P_T2_RelaxationTimes\Data\Output\LCModelFit_Summed_STEAM_Ideal_TM5ms_LW_noTrunc.mat')
load(strcat(outputPath,'LCModelFit_Summed_STEAM_Real_TM5ms_LW_noTrunc.mat'))

%calculate concentrations
for idxTE = 1:numberOfTEs
    TD = TR - TM - cellfun(@str2num,TEs(idxTE))/2;
    for idxSubject = 1:numberOfSubjects
        for idxMetabolite = 1:length(metaboliteNames)
            if LCModelFit_STEAM_Real_TM5ms_LW_noTrunc{(idxSubject+(idxTE-1)*12)+1,idxMetabolite*2+1} < 50
                if ~isempty(find(isnan(T1(idxMetabolite))))  %no T1 corr for Pi extracellular
                    Conc_RelaxationCorrected((idxSubject+(idxTE-1)*12),idxMetabolite) = ...
                        LCModelFit_STEAM_Real_TM5ms_LW_noTrunc{(idxSubject+(idxTE-1)*12)+1,idxMetabolite*2}./...
                        (exp(-cellfun(@str2num,TEs(idxTE))/T2(idxMetabolite)));
                    Conc_RelaxationCorrected_STEAM((idxSubject+(idxTE-1)*12),idxMetabolite) = ...
                        LCModelFit_STEAM_Real_TM5ms_LW_noTrunc{(idxSubject+(idxTE-1)*12)+1,idxMetabolite*2}./...
                        (exp(-cellfun(@str2num,TEs(idxTE))/T2(idxMetabolite)));%*exp(-TM/T1(idxMetabolite)));
                else
                    Conc_RelaxationCorrected((idxSubject+(idxTE-1)*12),idxMetabolite) = ...
                        LCModelFit_STEAM_Real_TM5ms_LW_noTrunc{(idxSubject+(idxTE-1)*12)+1,idxMetabolite*2}./...
                        ((1-exp(-TR/T1(idxMetabolite)))*exp(-cellfun(@str2num,TEs(idxTE))/T2(idxMetabolite)));
                    Conc_RelaxationCorrected_STEAM((idxSubject+(idxTE-1)*12),idxMetabolite) = ...
                        LCModelFit_STEAM_Real_TM5ms_LW_noTrunc{(idxSubject+(idxTE-1)*12)+1,idxMetabolite*2}./...
                        ((1-exp(-TD/T1(idxMetabolite)))*exp(-cellfun(@str2num,TEs(idxTE))/T2(idxMetabolite))*exp(-TM/T1(idxMetabolite)));
                end
            else
                Conc_RelaxationCorrected((idxSubject+(idxTE-1)*12),idxMetabolite) = NaN;
                Conc_RelaxationCorrected_STEAM((idxSubject+(idxTE-1)*12),idxMetabolite) = NaN;
            end
        end
    end
end

%normalize to g-ATP 3mM
for idxTE = 1:numberOfTEs
    for idxSubject = 1:numberOfSubjects
        Conc_Norm(idxSubject+(idxTE-1)*12,:) = Conc_RelaxationCorrected((idxSubject+(idxTE-1)*12),:)*3/Conc_RelaxationCorrected((idxSubject+(idxTE-1)*12),2);
        Conc_Norm_STEAM(idxSubject+(idxTE-1)*12,:) = Conc_RelaxationCorrected_STEAM((idxSubject+(idxTE-1)*12),:)*3/Conc_RelaxationCorrected_STEAM((idxSubject+(idxTE-1)*12),2);
    end
end

rowNames = cell(1,length(subjects)*length(TEs));
for indexSubject = 1:numberOfSubjects
    for indexTE = 1:numberOfTEs
        rowNames{1,(indexSubject+(indexTE-1)*12)} = [prefixes{indexSubject} subjects{indexSubject} '_TE' TEs{indexTE} 'ms']
    end
end

Conc2save_Norm = array2table(Conc_Norm,'RowNames',rowNames,'VariableNames',metaboliteNames);
Conc2save_Norm_STEAM = array2table(Conc_Norm_STEAM,'RowNames',rowNames,'VariableNames',metaboliteNames);

fileName = strcat(outputPath,'Concentrations_Real_allVolunteers');
save(fileName, 'Conc2save_Norm');
fileName_STEAM = strcat(outputPath,'Concentrations_Real_STEAM_allVolunteers');
save(fileName_STEAM, 'Conc2save_Norm_STEAM');

%calculate mean and std concentration
for idxTE = 1:numberOfTEs
    for idxMetabolite = 1:length(metaboliteNames)
        Conc_Norm_Mean(idxTE,idxMetabolite) = nanmean(Conc_Norm((idxTE*12-11):idxTE*12,idxMetabolite));
        Conc_Norm_Std(idxTE,idxMetabolite)  = nanstd(Conc_Norm((idxTE*12-11):idxTE*12,idxMetabolite));
        Conc_Norm_Mean_STEAM(idxTE,idxMetabolite) = nanmean(Conc_Norm_STEAM((idxTE*12-11):idxTE*12,idxMetabolite));
        Conc_Norm_Std_STEAM(idxTE,idxMetabolite)  = nanstd(Conc_Norm_STEAM((idxTE*12-11):idxTE*12,idxMetabolite));
    end
end

%% calculate concentrations for summed spectra
for idxTE = 1:numberOfTEs
    for idxMetabolite = 1:length(metaboliteNames)
        if LCModelFit_Summed_STEAM_Real_TM5ms_LW_noTrunc{idxTE+1,idxMetabolite*2+1} < 50
            if ~isempty(find(isnan(T1(idxMetabolite))))  %no T1 corr for Pi extracellular
                Conc_RelaxationCorrected_Summed(idxTE,idxMetabolite) = ...
                    LCModelFit_Summed_STEAM_Real_TM5ms_LW_noTrunc{idxTE+1,idxMetabolite*2}./...
                    (exp(-cellfun(@str2num,TEs(idxTE))/T2(idxMetabolite)));
                Conc_RelaxationCorrected_Summed_STEAM(idxTE,idxMetabolite) = ...
                    LCModelFit_Summed_STEAM_Real_TM5ms_LW_noTrunc{idxTE+1,idxMetabolite*2}./...
                    (exp(-cellfun(@str2num,TEs(idxTE))/T2(idxMetabolite)));%*exp(-TM/T1(idxMetabolite)));
            else
                Conc_RelaxationCorrected_Summed(idxTE,idxMetabolite) = ...
                    LCModelFit_Summed_STEAM_Real_TM5ms_LW_noTrunc{idxTE+1,idxMetabolite*2}./...
                    ((1-exp(-TR/T1(idxMetabolite)))*exp(-cellfun(@str2num,TEs(idxTE))/T2(idxMetabolite)));
                Conc_RelaxationCorrected_Summed_STEAM(idxTE,idxMetabolite) = ...
                    LCModelFit_Summed_STEAM_Real_TM5ms_LW_noTrunc{idxTE+1,idxMetabolite*2}./...
                    ((1-exp(-TD/T1(idxMetabolite)))*exp(-cellfun(@str2num,TEs(idxTE))/T2(idxMetabolite))*exp(-TM/T1(idxMetabolite)));
            end
        else
            Conc_RelaxationCorrected_Summed(idxTE,idxMetabolite) = NaN;
            Conc_RelaxationCorrected_Summed_STEAM(idxTE,idxMetabolite) = NaN;
        end
    end
end

%normalize to g-ATP 3mM
for idxTE = 1:numberOfTEs
    Conc_Norm_Summed(idxTE,:) = Conc_RelaxationCorrected_Summed(idxTE,:)*3/Conc_RelaxationCorrected_Summed(idxTE,2);
    Conc_Norm_Summed_STEAM(idxTE,:) = Conc_RelaxationCorrected_Summed_STEAM(idxTE,:)*3/Conc_RelaxationCorrected_Summed_STEAM(idxTE,2);
end

%%
rowNames = {'TE6ms_Mean' 'TE6ms_Std' 'TE6ms_Summed' 'TE8ms_Mean' 'TE8ms_Std' 'TE8ms_Summed' ...
    'TE11ms_Mean' 'TE11ms_Std' 'TE11ms_Summed' 'TE15ms_Mean' 'TE15ms_Std' 'TE15ms_Summed' ...
    'TE20ms_Mean' 'TE20ms_Std' 'TE20ms_Summed' 'TE30ms_Mean' 'TE30ms_Std' 'TE30ms_Summed' ...
    'TE50ms_Mean' 'TE50ms_Std' 'TE50ms_Summed' 'TE80ms_Mean' 'TE80ms_Std' 'TE80ms_Summed' ...
    'TE150ms_Mean' 'TE150ms_Std' 'TE150ms_Summed' };
n = 1;
for idxTE = 1:numberOfTEs
    Conc(n,:) = Conc_Norm_Mean(idxTE,:);
    Conc(n+1,:) = Conc_Norm_Std(idxTE,:);
    Conc(n+2,:) = Conc_Norm_Summed(idxTE,:);
    Conc_STEAM(n,:) = Conc_Norm_Mean_STEAM(idxTE,:);
    Conc_STEAM(n+1,:) = Conc_Norm_Std_STEAM(idxTE,:);
    Conc_STEAM(n+2,:) = Conc_Norm_Summed_STEAM(idxTE,:);
    n = n + 3;
end
% Concentrations_Ideal = array2table(Conc,'RowNames',rowNames,'VariableNames',metaboliteNames)
if ~isempty(strfind(outputPath,'Original'))
    Concentrations_Real_Original = array2table(Conc,'RowNames',rowNames,'VariableNames',metaboliteNames)
    Concentrations_Real_Original_STEAM = array2table(Conc_STEAM,'RowNames',rowNames,'VariableNames',metaboliteNames)
elseif ~isempty(strfind(outputPath,'NormReal400'))
    Concentrations_Real_Phased_NormReal400 = array2table(Conc,'RowNames',rowNames,'VariableNames',metaboliteNames)
    Concentrations_Real_Phased_NormReal400_STEAM = array2table(Conc_STEAM,'RowNames',rowNames,'VariableNames',metaboliteNames)
elseif ~isempty(strfind(outputPath,'NormAbs400'))
    Concentrations_Real_Phased_NormAbs400 = array2table(Conc,'RowNames',rowNames,'VariableNames',metaboliteNames)
    Concentrations_Real_Phased_NormAbs400_STEAM = array2table(Conc_STEAM,'RowNames',rowNames,'VariableNames',metaboliteNames)
elseif ~isempty(strcmp(outputPath(end-6:end),'Output\'))
    Concentrations_Real = array2table(Conc,'RowNames',rowNames,'VariableNames',metaboliteNames)
    Concentrations_Real_STEAM = array2table(Conc_STEAM,'RowNames',rowNames,'VariableNames',metaboliteNames)
else
    Concentrations_Real_Phased = array2table(Conc,'RowNames',rowNames,'VariableNames',metaboliteNames)
    Concentrations_Real_Phased_STEAM = array2table(Conc_STEAM,'RowNames',rowNames,'VariableNames',metaboliteNames)
end
    
if ~isempty(strfind(outputPath,'Original'))
    fileName = strcat(outputPath,'Concentrations_Real_Original');
    save(fileName, 'Concentrations_Real_Original');
    fileName_STEAM = strcat(outputPath,'Concentrations_Real_Original_STEAM');
    save(fileName_STEAM, 'Concentrations_Real_Original_STEAM');
elseif ~isempty(strfind(outputPath,'NormReal400'))
    fileName = strcat(outputPath,'Concentrations_Real_Phased_NormReal400');
    save(fileName, 'Concentrations_Real_Phased_NormReal400') 
    fileName_STEAM = strcat(outputPath,'Concentrations_Real_Phased_NormReal400_STEAM');
    save(fileName_STEAM, 'Concentrations_Real_Phased_NormReal400_STEAM') 
elseif ~isempty(strfind(outputPath,'NormAbs400'))
    fileName = strcat(outputPath,'Concentrations_Real_Phased_NormAbs400');
    save(fileName, 'Concentrations_Real_Phased_NormAbs400' )
    fileName_STEAM = strcat(outputPath,'Concentrations_Real_Phased_NormAbs400_STEAM');
    save(fileName_STEAM, 'Concentrations_Real_Phased_NormAbs400_STEAM' )
elseif ~isempty(strcmp(outputPath(end-6:end),'Output\'))
    fileName = strcat(outputPath,'Concentrations_Real');
    save(fileName, 'Concentrations_Real' )
    fileName_STEAM = strcat(outputPath,'Concentrations_Real_STEAM');
    save(fileName_STEAM, 'Concentrations_Real_STEAM' )    
else
    fileName = strcat(outputPath,'Concentrations_Real_Phased');
    save(fileName, 'Concentrations_Real_Phased') 
    fileName_STEAM = strcat(outputPath,'Concentrations_Real_Phased_STEAM');
    save(fileName_STEAM, 'Concentrations_Real_Phased_STEAM') 
end

end