function     subtractMetabolitesFromMMB_fMRS(currentDataPath,subjectsLCModelOutputPath,currentLCModelMatFit,currentLCModelCoordFit,currentLCModelFitWithoutMetabolite);

metabolitesToSubtract = {'Cre'};
doNullingOfDownField = true;
reconstructedSpectra = strcat(currentDataPath,currentLCModelMatFit);
reconstructedSpectraWithoutMetabolite = strcat(currentDataPath, currentLCModelFitWithoutMetabolite);

phase90 = -90 * pi/180;
zeroFillFactor = 2;

%plot configs
FontSize = 14;
LineWidth = 1.5;

load(reconstructedSpectra, 'a');
[nOfPoints, ppmVector, phasedData, fitData, baselineData, residualData, ...
    metaboliteNames, metaboliteData, tableConcentrations, ppmStart, ppmEnd, ...
    scanFrequency,  frequencyShift, ppmGap] = ...
    extractFits(subjectsLCModelOutputPath,currentLCModelCoordFit);

% align the two spectra
fid = a.Data{1};
spectrum = fftshift(fft(fid(:,1,1,1,1,1,1,1,1,1,1,1),size(fid,1)*zeroFillFactor));
ppmSpectrum = a.getPpmVector(zeroFillFactor);

[~, maxPpmValueSpectrum] = getMaxPeak(ppmSpectrum', real(spectrum), 3.925, 0.1);
[~, maxPpmValueLCModel] = getMaxPeak(ppmVector, phasedData, 3.925, 0.1);

frequencyShiftDiff = maxPpmValueSpectrum - maxPpmValueLCModel;
summedSpectraTE = a.ApplyFrequencyShift(frequencyShiftDiff);
%get the aligned reconstructed spectra
fid = summedSpectraTE.Data{1};
spectrum = fftshift(fft(fid(:,1,1,1,1,1,1,1,1,1,1,1),size(fid,1)*zeroFillFactor));
% max of the reconstructed spectrum
if isempty(ppmGap)
    ppmSpectrumMask  = (ppmSpectrum < ppmStart) & (ppmSpectrum > ppmEnd);
    ppmMetaboliteMask  = (ppmVector < ppmStart) & (ppmVector > ppmEnd);
else
    ppmSpectrumMask  = (ppmSpectrum < ppmGap) & (ppmSpectrum > ppmEnd);
    ppmMetaboliteMask  = (ppmVector < ppmGap) & (ppmVector > ppmEnd);
end
maxSpectrum = max(real(spectrum(ppmSpectrumMask)));
% max of the LCModel fit
maxPhasedData = max(phasedData);
% calculate scaling
scalingFactor = maxSpectrum/maxPhasedData;
oldSpectrum = spectrum;
for indexMetabolite = 1:length(metabolitesToSubtract)
    metaboliteToSubtract = metabolitesToSubtract{indexMetabolite};
    indexMetaboliteToSubtract = find(strcmp(metaboliteNames,metaboliteToSubtract));
    if isempty(indexMetaboliteToSubtract)
        error('Metabolite %s not found in fit',metaboliteToSubtract);
    end
    
    spectrumToSubtract = metaboliteData{indexMetaboliteToSubtract}.metaboliteSpectrum(ppmMetaboliteMask);
    spectrumToSubtract(1) = 0;
    %eliminate first point, which is anyway an error; Also scale it
    spectrumToSubtractFlipped = flipud(spectrumToSubtract).*scalingFactor;
    
    spectrumToSubtractReal = zeros(size(oldSpectrum));
    spectrumToSubtractReal(ppmSpectrumMask) = spectrumToSubtractReal(ppmSpectrumMask) + spectrumToSubtractFlipped;
    fidToSubtract = ifft(ifftshift(complex(spectrumToSubtractReal)));
    fidToSubtract(length(fidToSubtract)/2:end)=0;
    spectrumToSubtractPhased90 = fftshift(fft(fidToSubtract .* exp(1i*phase90))).*2; %multiply by 2 to account for nulling the half of the FID
    spectrumToSubtractImag = real(spectrumToSubtractPhased90);
    newSpectrumReal = real(oldSpectrum) - spectrumToSubtractReal;
    newSpectrumImag = imag(oldSpectrum) - spectrumToSubtractImag;
    newSpectrum = complex(newSpectrumReal, newSpectrumImag);
    
    if (doNullingOfDownField)
        newSpectrum(length(newSpectrum)/2:end) = 0;
    end
    
    oldSpectrum = newSpectrum;
end

figure
% plot(ppmVector, phasedData.*scalingFactor)
% hold on
plot(ppmSpectrum, real(spectrum),'r')
hold on
plot(ppmSpectrum, real(newSpectrum),'k')
set(gca,'xDir','reverse')
xlim([0 4.2])
title(sprintf('MMBaseline with and w/o the Cr peak'))
legend('Before subtraction','After subtration')

fig2save_Conc = fullfile(currentDataPath, 'MMBaseline.fig');
savefig(fig2save_Conc)

close all

% figure(101)
% hold on
% p1 = plot(ppmSpectrum, real(spectrum), 'r');
% hold on
% p2 = plot(ppmSpectrum, real(newSpectrum),'k');
% xlim([0 4.2])
% xlabel('[ppm]');
% %     ylim([(indexTE+1) * offsetPlot-offsetPlot*0.5 5e-5]);
% set(gca,'xDir','reverse')
% set(gca,'ytick',[]);
% set(gca,'fontsize',FontSize);
% set(p1,'LineWidth',LineWidth);
% set(p2,'LineWidth',LineWidth);
% title('Macromolecular baseline with and w/o the Cr peak')

MMBaseline_wo_Cr = summedSpectraTE;
newFid = ifft(ifftshift(newSpectrum));
MMBaseline_wo_Cr.Data{1} = newFid;
trunc_ms = 250;
truncPoint = floor( trunc_ms/(MMBaseline_wo_Cr.Parameter.Headers.DwellTimeSig_ns*1e-6));
MMBaseline_wo_Cr = MMBaseline_wo_Cr.Truncate(truncPoint);


save(reconstructedSpectraWithoutMetabolite,'MMBaseline_wo_Cr');
MMBaseline_wo_Cr.ExportLcmRaw(currentDataPath, currentLCModelFitWithoutMetabolite, 'Yes', 'Yes');
end

function [maxValue, maxPpmValue] = getMaxPeak(ppmVector, spectrum, peakPpm, searchAreaPpm)
minPpm = peakPpm - searchAreaPpm;
maxPpm = peakPpm + searchAreaPpm;
ppmMask = (ppmVector > minPpm) & (ppmVector < maxPpm);
tempSpectrum = spectrum .* ppmMask;
[maxValue, maxIndex] = max(real(tempSpectrum));
maxPpmValue = ppmVector(maxIndex);
end
