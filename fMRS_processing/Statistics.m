function Statistics()
clear; clc; close all;

%check StimType, BOLDType, MMBType


pathName = 'fMRS data path';
sampleCriteria = {'Output'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
StimType = {'StimON', 'StimOFF'};      %'StimON', 'StimOFF', 'RestON', 'RestOFF'
if ~isempty(strfind(StimType{1},'Stim'))
    visualType = 'Stim';
elseif ~isempty(strfind(StimType{1},'Rest'))
    visualType = 'Rest';
else
    error('StimType needs to be Stim or Rest');
end
BOLDType = '_woBOLDCorr128';  %_woBOLDCorr128, _withBOLDCorr128, _woBOLDCorr64, _woBOLDCorr64_1, _withBOLDCorr64, _withBOLDCorr64_1
MMBType = '_MMBSummed_v2';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated, _MMBSummed_v2
% subjects = {'2823';};
subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
    '4085';'3333';'4012';'4085';'6524'};
% prefixes = {'2020-02-18_';};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
    '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
    '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
excludeSubjects = {'5166';'9810';'5269'};
if strcmp(MMBType,'_MMBSummed')
    excludeMet = {'NAA';'GABA';'Gln';'Glyc';'GSH';'mI';'NAAG';'PCh';'GPC';'PE';'Scyllo';'Tau';'Glc';'NAA_NAAG';'Glu_Gln';'Glyc';'mI_Glyc';'PCh'};  %for v1
elseif strcmp(MMBType,'_MMBSummed_v2')
    excludeMet = {'NAA';'Asc';'GABA';'Glc';'Gln';'GSH';'mI';'NAAG';'tCh';'PE';'Scyllo';'Tau';'NAA_NAAG';'Glu_Gln'};  %for v2
end
%% file paths setup
if ~isempty(strfind(MMBType,'_v2'))
    LCModelOutputPath = [localFilePathBase, 'Output_v2_MMBSummed/'];
else
    LCModelOutputPath = [localFilePathBase, 'Output/'];
end
ExpName = strcat('LCModelFit_', num2str(visualType), BOLDType, MMBType);
exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];

%% calculate percent changes Stim OFF vs Stim ON, save result tables
load(exportFileMat)
[~, rawFile, ~] = fileparts(exportFileMat);
LCModelFitTable = eval(rawFile);
metaboliteNames = LCModelFitTable(1,4:2:end-2);
metaboliteNames = strrep(metaboliteNames,'+','_');

if exist('excludeSubjects')
    [n,~] = size(LCModelFitTable);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 2:n
            xx = find(~cellfun(@isempty,strfind(LCModelFitTable(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    LCModelFitTable([excludeIdx],:) = [];
end

ON = cell2mat(LCModelFitTable(2:2:end,4:2:end-2));
OFF = cell2mat(LCModelFitTable(3:2:end,4:2:end-2));
PercDiff = 100./OFF.*ON-100;
PercDiffMean = mean(PercDiff);
PercDiffStd = std(PercDiff);

%% two-sided Wilcoxon signed rank test
for indexOfMetabolite = 1:length(metaboliteNames)
    if isempty(find(~isnan(PercDiff(:,indexOfMetabolite))))
        p_Wilc(indexOfMetabolite) = NaN;
        h_Wilc(indexOfMetabolite) = 0;
    else
        [p_Wilc(indexOfMetabolite),h_Wilc(indexOfMetabolite)] = signrank(PercDiff(:,indexOfMetabolite));
    end
end

%check histogram of p-values
edges = linspace(0,1,length(p_Wilc));
histogram(p_Wilc,edges)

%Benjamini-Hochberg correction for multiple testing
[h_BH_Wilc, crit_p_Wilc, adj_ci_cvrg_Wilc, adj_p_Wilc]=fdr_bh(p_Wilc,0.05,'pdep','yes');
%h=1: reject H0, h=0: H0 not rejected
%crit_p: max p-value that is significant
%adj_ci_cvrg: BH adjusted confidence interval coverage
%adj_p: N pj / j, all adjusted p-values less than or equal to q are significant


if exist('excludeMet')
    [~,n] = size(metaboliteNames);
    m = length(excludeMet);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = strcmp(metaboliteNames(1,i),excludeMet{j});
%             xx = find(~cellfun(@isempty,strcmp(metaboliteNames(1,i),excludeMet{j})));
%             if ~isempty(xx)
            if xx
                excludeMetIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    pExclMet_Wilc = p_Wilc;
    pExclMet_Wilc(excludeMetIdx) = [];
    [h_BH_ExclMet_Wilc, crit_p_ExclMet_Wilc, adj_ci_cvrg_ExclMet_Wilc, adj_p_ExclMet_Wilc]=fdr_bh(pExclMet_Wilc,0.05,'pdep','yes');
    temph = ones(1,length(h_Wilc))*NaN; temp_p = ones(1,length(p_Wilc))*NaN; tempidx = [0 sort(excludeMetIdx)]; nstep = 1;
    for j = 1:length(tempidx)-1
        temp_p((tempidx(j)+1):(tempidx(j+1)-1)) = adj_p_ExclMet_Wilc((tempidx(j)+2-nstep):(tempidx(j+1)-nstep));
        temph((tempidx(j)+1):(tempidx(j+1)-1)) = h_BH_ExclMet_Wilc((tempidx(j)+2-nstep):(tempidx(j+1)-nstep));
        nstep = nstep + 1;
    end
    h_BH_ExclMet_Wilc = temph;
    adj_p_ExclMet_Wilc = temp_p;
end

%% paired two-tailed t-test (data need to be normally distributed, which they aren't here! -> use nonparametric Wilcoxon)
for indexOfMetabolite = 1:length(metaboliteNames)
    if isempty(find(~isnan(PercDiff(:,indexOfMetabolite))))
        h_ttest(indexOfMetabolite) = NaN;
        p_ttest(indexOfMetabolite) = 0;
    else
        [h_ttest(indexOfMetabolite),p_ttest(indexOfMetabolite)] = ttest(PercDiff(:,indexOfMetabolite));
    end
end

%check histogram of p-values
edges = linspace(0,1,length(p_ttest));
histogram(p_ttest,edges)

%Benjamini-Hochberg correction for multiple testing
[h_BH_ttest, crit_p_ttest, adj_ci_cvrg_ttest, adj_p_ttest]=fdr_bh(p_ttest,0.05,'pdep','yes');
%h=1: reject H0, h=0: H0 not rejected
%crit_p: max p-value that is significant
%adj_ci_cvrg: BH adjusted confidence interval coverage
%adj_p: N pj / j, all adjusted p-values less than or equal to q are significant


if exist('excludeMet')
    [~,n] = size(metaboliteNames);
    m = length(excludeMet);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = strcmp(metaboliteNames(1,i),excludeMet{j});
%             xx = find(~cellfun(@isempty,strcmp(metaboliteNames(1,i),excludeMet{j})));
%             if ~isempty(xx)
            if xx
                excludeMetIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    pExclMet_ttest = p_ttest;
    pExclMet_ttest(excludeMetIdx) = [];
    [h_BH_ExclMet_ttest, crit_p_ExclMet_ttest, adj_ci_cvrg_ExclMet_ttest, adj_p_ExclMet_ttest]=fdr_bh(pExclMet_ttest,0.05,'pdep','yes');
    temph = ones(1,length(h_ttest))*NaN; temp_p = ones(1,length(p_ttest))*NaN; tempidx = [0 sort(excludeMetIdx)]; nstep = 1;
    for j = 1:length(tempidx)-1
        temp_p((tempidx(j)+1):(tempidx(j+1)-1)) = adj_p_ExclMet_ttest((tempidx(j)+2-nstep):(tempidx(j+1)-nstep));
        temph((tempidx(j)+1):(tempidx(j+1)-1)) = h_BH_ExclMet_ttest((tempidx(j)+2-nstep):(tempidx(j+1)-nstep));
        nstep = nstep + 1;
    end
    h_BH_ExclMet_ttest = temph;
    adj_p_ExclMet_ttest = temp_p;
end


%% save data in table
if exist('excludeSubjects')
    m = length(excludeSubjects);
    for k = 1:m
        [~, idx(k)] = ismember(excludeSubjects(k), subjects);
    end
    prefixes([idx],:) = [];
    subjects([idx],:) = [];
end
rowNames = strcat(prefixes, subjects);
% rowNames = [rowNames; 'Mean'; 'Std'; 'p-Value'; 'h-Value'];
if exist ('excludeMet')
    rowNames = [rowNames; 'Mean'; 'Std'; 'p-Value_Wilc'; 'h-Value_Wilc'; 'p_BH_Wilc'; 'h_BH_Wilc'; 'p_BH_Excl_Wilc'; 'h_BH_Excl_Wilc'; 'p-Value_ttest'; 'h-Value_ttest'; 'p_BH_ttest'; 'h_BH_ttest'; 'p_BH_Excl_ttest'; 'h_BH_Excl_ttest'];
else
    rowNames = [rowNames; 'Mean'; 'Std'; 'p-Value_Wilc'; 'h-Value_Wilc'; 'p_BH_Wilc'; 'h_BH_Wilc'; 'p-Value_ttest'; 'h-Value_ttest'; 'p_BH_ttest'; 'h_BH_ttest'];
end
if exist('excludeSubjects')
    ExpNameDiff = strcat('ConcDiff_', num2str(visualType), BOLDType, MMBType, '_Excluded');
else
    ExpNameDiff = strcat('ConcDiff_', num2str(visualType), BOLDType, MMBType);
end
if exist ('excludeMet')
    ConcentrationsDiff_xx = array2table([PercDiff; PercDiffMean; PercDiffStd; p_Wilc; h_Wilc; adj_p_Wilc; h_BH_Wilc; adj_p_ExclMet_Wilc; h_BH_ExclMet_Wilc; p_ttest; h_ttest; adj_p_ttest; h_BH_ttest; adj_p_ExclMet_ttest; h_BH_ExclMet_ttest],'rowNames',rowNames,'VariableNames',metaboliteNames);
else
    ConcentrationsDiff_xx = array2table([PercDiff; PercDiffMean; PercDiffStd; p_Wilc; h_Wilc; adj_p_Wilc; h_BH_Wilc; p_ttest; h_ttest; adj_p_ttest; h_BH_ttest],'rowNames',rowNames,'VariableNames',metaboliteNames);
end
eval([ExpNameDiff '=ConcentrationsDiff_xx']);
exportFileMatDiff = ['D:\PAPER\1H_fMRS\Statistics\' ,strcat(ExpNameDiff,'.mat')];
save(exportFileMatDiff,ExpNameDiff)

close all
%% plot concentrations mean and std
% y = table2array(ConcDiff_Stim_withBOLDCorr64_MMBSummed(14,[1:4,6:7,9:12,14:17]));
% x = 1:14;
% err = table2array(ConcDiff_Stim_withBOLDCorr64_MMBSummed(15,[1:4,6:7,9:12,14:17]));
% errorbar(x,y,err)
% hold on
% y2 = table2array(ConcDiff_Stim_withBOLDCorr64_MMBSummed_Excluded(11,[1:4,6:7,9:12,14:17]));
% err2 = table2array(ConcDiff_Stim_withBOLDCorr64_MMBSummed_Excluded(12,[1:4,6:7,9:12,14:17]));
% errorbar(x,y2,err2)
% legend('All subjects','w/o 5166, 9810, 5269')
% title 'ConcDiff_Stim_withBOLDCorr64_MMBSummed'
end