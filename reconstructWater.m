function [a, stats] = reconstructWater(fid_id, isInVivo, saveResults)
if ~exist('isInVivo', 'var')
    isInVivo = true;
end

if ~exist('saveResults', 'var')
    saveResults = true;
end

a = MR_spectroS(fid_id);
if isInVivo
    a = a.FrequencyAlign;
end
a = a.AverageData;
a = a.EddyCurrentCorrection;
a = a.CombineCoils;
%% setup the settings for the FrequencyAlignFreqDomain
a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;

%indices of Interest: given in ppm
% water peak
a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = [4.67];

%set zeroFillingParameter to get smooth approximations
a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;

%search area (+-) in ppm
a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.3;
%spline filtering is not needed if the spectra are not too noisy.
%flag to do spline filtering
a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
%spline filtering coefficient
a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;
% do the actual Frequency Alignment
a = a.FrequencyAlignFreqDomain;
%%
if isInVivo
    a = a.Truncate(2000);
end

if saveResults
    %saving the file
    save([a.Parameter.Filename(1:end-4) '.mat'], 'a')
    filename = a.Parameter.Filename(1:end-4);
    %export to LCModel
    addSinglet0ppm = 'Yes';
    a.ExportLcmRaw('',filename, addSinglet0ppm);
end

stats = a.Stats({});
end