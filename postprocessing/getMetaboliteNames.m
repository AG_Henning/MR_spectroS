function [mapMet_Names, mapMet_NamesInverse, mapMetNamesDisplay, mapMetNamesDisplayInverse] = getMetaboliteNames(downField_MM_Met)
switch downField_MM_Met
    case 'DF'
        key_ppmNaming = {'DF58' 'DF60' 'DF61' 'DF68' 'DF70' 'DF73' 'DF75' 'DF82' 'DF83' 'DF835' 'DF85' 'NAAB' 'ATP' 'hCs' 'hist' 'NAD' 'NAA_DF' 'NAAB+NAA_DF' 'Cr' 'Leu' 'Asp' 'Cr'       'Cr_CH2'   'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'GPC' 'Lac' 'mI' 'NAA_as'   'NAA_ac'   'NAA'   'NAAG' 'PCh' 'PCr' 'PE' 'Scyllo' 'Tau' 'GPC+PCh' 'NAA+NAAG' 'NAA_ac+NAA_as+NAAG' 'NAA_ac+NAA_as' 'mI+Glyc' 'Cr+PCr' 'Glu+Gln' 'Asc' 'Glc_al' 'Glc_be' 'Glc' 'Gln_de' 'GSH_no' 'hCs' 'Serine' 'GABA_d' 'histid' 'ATP' 'tCho' 'tCho_P' 'Scy_Ta'  'Cho'  'sI'     'Gly'  'mI+Gly'  'Cho+GPC+PCh' 'Lip13a' 'Lip13b' 'Lip13c' 'Lip13d' 'Lip20' 'Lip13a+Lip13b'};
        value =         {'DF58';'DF60';'DF61';'DF68';'DF70';'DF73';'DF75';'DF82';'DF83';'DF835';'DF85';'NAA Broad';'ATP';'hCs';'hist';'NAD';'NAA';'total NAA';'Cr';'Leu';'Asp';'Cr-CH3'  ;'Cr-CH2'  ;'GABA';'Glu';'Gln';'GSH';'Glyc';'GPC';'Lac';'mI';'NAA-asp' ;'NAA-ace' ;'NAA';  'NAAG';'PCh';'PCr';'PE';'Scyllo';'Tau';'tCho';   'tNAA';     'tNAA+';            'tNAA';         'mI+Glyc';'Cr+PCr';'Glx';    'Asc';'Glc_al';'Glc_be';'Glc';'Gln_de';'GSH';'hCs';'Serine';'GABA_d';'histid';'ATP';'tCho';'tCho_PE' ; 'Scy_Tau';'tCho';'Scyllo';'Glyc';'mI+Glyc';'tCho';       'Lip13a';'Lip13b';'Lip13c';'Lip13d';'Lip20';'Lip13a+Lip13b'};
        %     keyMM_ppmNaming = {'DF58' 'DF60' 'DF68' 'DF71' 'DF73' 'DF75' 'DF82' 'DF83' 'DF835' 'ATP' 'NAAB' 'hist' 'hCs' 'NAADF' 'Cr'};
        %     valueMM = {'DF58';'DF60';'DF68';'DF71';'DF73';'DF75';'DF82';'DF83';'DF835';'ATP';'NAA Broad';'hist';'hCs';'NAA';'Cr'};
        metaboliteNamesDisplay = value;
    case 'MM'
        key_ppmNaming = {'MM09'  'MM12'  'MM14'  'MM17'  'MM20'  'MM22'  'MM26'  'MM27'  'MM30'  'MM32'  'MM36'  'MM37'  'MM38'  'MM40'  'Cre'  'Cr'   'NAA'};
        value = {        'M0.92';'M1.21';'M1.39';'M1.67';'M2.04';'M2.26';'M2.56';'M2.70';'M2.99';'M3.21';'M3.62';'M3.75';'M3.86';'M4.03';'tCr(CH2)';  'NAA'; 'NAA'};
        metaboliteNamesDisplay = value;
    case 'MM WM'
        key_ppmNaming = {'MM09'  'MM12'  'MM14'  'MM17'  'MM20'  'MM22'  'MM26'  'MM27'  'MM30'  'MM32'  'MM36'  'MM37'  'MM38'  'MM40'  'Cre'  'Cr' };
        value = {        'M0.92';'M1.21';'M1.39';'M1.67';'M2.04';'M2.26';'M2.56';'M2.70';'M2.99';'M3.21';'M3.62';'M3.75';'M3.86';'M4.03';'tCr(CH2)';  'NAA'};
        metaboliteNamesDisplay = value;
    case 'UF'
%             keyMM_ppmNaming = {'Leu' 'Asp' 'Cho' 'Cr' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'GPC' 'Lac' 'mI' 'NAA' 'NAAG' 'PCr' 'PCh' 'PE' 'Scyllo' 'Tau' 'Cr+PCr' 'Cho+GPC+PCh' 'NAA+NAAG' 'mI+Glyc' 'Glu+Gln' 'CrRes'};
%             valueMM = {'Leu';'Asp';'Cho';'Cr';'GABA';'Glu';'Gln';'GSH';'Glyc';'GPC';'Lac';'mI';'NAA';'NAAG';'PCr';'PCh';'PE';'Scyllo';'Tau';'tCr';'tCho';'tNAA';'mI+Glyc';'Glx';'CrRes'};

%             keyMM_ppmNaming = {'Leu' 'Asp' 'Cr' 'Cr_CH2' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'GPC' 'Lac' 'mI' 'NAA_as' 'NAA' 'NAAG' 'PCh' 'PE' 'Scyllo' 'Tau' 'GPC+PCh' 'NAA+NAAG' 'mI+Glyc' 'Glu+Gln'};
%             valueMM = {'Leu';'Asp';'Cr-CH3';'Cr-CH2';'GABA';'Glu';'Gln';'GSH';'Glyc';'GPC';'Lac';'mI';'NAA-asp';'NAA-ace';'NAAG';'PCh';'PE';'Scyllo';'Tau';'tCho';'tNAA';'mI+Glyc';'Glx'};
            
            key_ppmNaming = {         'Leu' 'Asp' 'Cr'       'Cr_CH2'   'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'GPC' 'Lac' 'mI' 'NAA_as'   'NAA_ac'   'NAA'   'NAAG' 'PCh' 'PCr' 'PE' 'Scyllo' 'Tau' 'GPC+PCh' 'NAA+NAAG' 'NAA_ac+NAA_as+NAAG' 'NAA_ac+NAA_as' 'mI+Glyc' 'Cr+PCr' 'Glu+Gln' 'Asc' 'Glc_al' 'Glc_be' 'Glc' 'Gln_de' 'GSH_no' 'hCs' 'Serine' 'GABA_d' 'histid' 'ATP' 'tCho' 'tCho_P' 'Scy_Ta'  'Cho'  'sI'     'Gly'  'mI+Gly'  'Cho+GPC+PCh' 'Lip13a' 'Lip13b' 'Lip13c' 'Lip13d' 'Lip20' 'Lip13a+Lip13b'};
            value = {                 'Leu';'Asp';'Cr-CH3'  ;'Cr-CH2'  ;'GABA';'Glu';'Gln';'GSH';'Glyc';'GPC';'Lac';'mI';'NAA-asp' ;'NAA-ace' ;'NAA';  'NAAG';'PCh';'PCr';'PE';'Scyllo';'Tau';'tCho';   'tNAA';     'tNAA+';            'tNAA';         'mI+Glyc';'Cr+PCr';'Glx';    'Asc';'Glc_al';'Glc_be';'Glc';'Gln_de';'GSH';'hCs';'Serine';'GABA_d';'histid';'ATP';'tCho';'tCho_PE' ; 'Scy_Tau';'tCho';'Scyllo';'Glyc';'mI+Glyc';'tCho';       'Lip13a';'Lip13b';'Lip13c';'Lip13d';'Lip20';'Lip13a+Lip13b'};
            metaboliteNamesDisplay = {'MMB';'Asp';'tCr(CH3)';'tCr(CH2)';'GABA';'Glu';'Gln';'GSH';'Glyc';'GPC';'Lac';'mI';'NAA(CH2)';'NAA(CH3)';'NAA';  'NAAG';'PCh';'PCr';'PE';'Scyllo';'Tau';'tCho';   'tNAA';     'tNAA+';            'tNAA';         'mI+Glyc';'tCr';   'Glx';    'Asc';'Glc_al';'Glc_be';'Glc';'Gln_de';'GSH';   'hCs';'Serine';'GABA_d';'histid';'ATP';'tCho';'tCho+'; 'Scy_Tau';'tCho';'Scyllo';'Glyc';'mI+Glyc';'tCho';       'Lip13a';'Lip13b';'Lip13c';'Lip13d';'Lip20';'Lip13a+Lip13b'};
    otherwise
            error('Not defined downField_MM_Met case.');
end
mapMet_Names = containers.Map(key_ppmNaming,value);
mapMet_NamesInverse = containers.Map(value, key_ppmNaming);
mapMetNamesDisplay = containers.Map(key_ppmNaming, metaboliteNamesDisplay);
mapMetNamesDisplayInverse = containers.Map(metaboliteNamesDisplay, key_ppmNaming);
end