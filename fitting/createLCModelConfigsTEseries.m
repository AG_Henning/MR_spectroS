function createLCModelConfigsTEseries(downField_MM_Met, controlFilesPath, defaultControlFile, ...
    orderTEs, defaultTE, defaultLCModelUser, currentLCModelUser, subjects, defaultSubject,...
    pH_Values, default_pH)
% Creates the control files for the TE series, given a default control file.
% the newly generated files will be placed in the same path as indicated by
% the controlFilesPath.
% The defaultControlFile has to include the keyword "Default" in the file name.
% downField_MM_Met can be: 'DF' 'MM', 'Met' or 'pH'
% A sample input:
% controlFilesPath = 'Fitsettings/MMocc_noExtraMM_06knots/';
% defaultControlFile = 'fitsettingsDefault.control'

if(nargin  <  6)
    error('Error: createLCModelConfigsTEseries requires minimum 6 input arguments: controlFilesPath, defaultControlFile, orderTEs, defaultTE, defaultLCModelUser, currentLCModelUser, subjects, defaultSubject .');
end
orderTEsLong = strcat('TE', orderTEs);
defaultKeyword = 'Default';
indexFind  = strfind(defaultControlFile, defaultKeyword);
if isempty(indexFind)
    error('The default control file should have the keyword "Default" in its file name.');
end

fitAllSubjects = false;
numOfSubjects = 1;
numOf_pH_values = 1;
if  exist('subjects','var') && exist('defaultSubject','var')
    if ~isempty(subjects)
        if ~contains(defaultControlFile, defaultSubject)
            error('The default control file should have the keyword %s in its file name when using subject fitting.', defaultSubject);
        end
        subjectsControlFilesPath = strcat(controlFilesPath, subjects, '/');
        
        fitAllSubjects = true;
        numOfSubjects = length(subjects);
        % create the folders for the individual subjects
        for indexSubject = 1:numOfSubjects
            mkdir(subjectsControlFilesPath{indexSubject})
        end
    end
end

if  exist('pH_Values','var') && exist('default_pH','var')
    if ~contains(defaultControlFile, default_pH)
        error('The default control file should have the keyword %s in its file name when using pH fitting.', default_pH);
    end
    numOf_pH_values = length(pH_Values);
end

controlFileBase = defaultControlFile(1:indexFind-1);
controlFileSuffix = defaultControlFile(indexFind+length(defaultKeyword):end);

fitSettingsDefaultFileID = fopen([controlFilesPath defaultControlFile], 'r');

for index_pH = 1:numOf_pH_values
    %iterate over all TEs
    for index = 1:length(orderTEsLong)
        %iterate over all subjects, if we have it set. Otherwise the loop is executed only once
        for indexSubject = 1:numOfSubjects
            if fitAllSubjects
                % add the subject name to the output path, and the control file name
                subjectControlFileBase = strrep(controlFileBase, defaultSubject, subjects{indexSubject});
                outputFile = strcat(subjectsControlFilesPath{indexSubject}, subjectControlFileBase, orderTEsLong{index}, controlFileSuffix);
            else
                outputFile = strcat(controlFilesPath, controlFileBase, orderTEsLong{index}, controlFileSuffix);
            end
            if strcmp(downField_MM_Met, 'pH')
                outputFile = strrep(outputFile, default_pH, pH_Values{index_pH});
            end
            fitSettingsFileID = fopen(outputFile, 'w');
            display(outputFile);
            
            while(~feof(fitSettingsDefaultFileID))
                s = fgetl(fitSettingsDefaultFileID);
                s = strrep(s, defaultLCModelUser, currentLCModelUser);
                s = strrep(s, defaultTE, orderTEsLong{index});
                if fitAllSubjects
                    s = strrep(s, defaultSubject, subjects{indexSubject});
                end
                switch downField_MM_Met
                    case 'MM'
                        if (index > 3)
                            if (contains(s,'MM27'))
                                s = strrep(s, 'AMP= 1', 'AMP= -1');
                            end
                        end
%                         if (~isempty(strfind(s,'MM27')))
%                             currentTE = str2double(orderTEs{index}) * 1e-3;
%                             FWHM = '.01';
%                             FWHM_std = '.05';
%                             amplitudeHB2 = 0.5 * (cos(pi*-17.5*currentTE) + cos(pi*5.6*currentTE)); %J couplings: HB2-HB3 = -17.5Hz HA-HB2 = 5.5Hz
%                             amplitudeHB3 = 0.5 * (cos(pi*-17.5*currentTE) + cos(pi*9.5*currentTE)); %J couplings: HB2-HB3 = -17.5Hz HA-HB3 = 9Hz
%                             newAmplitudes = ['MM27 @ 2.695 +- 0.02 FWHM= ' FWHM ' < ' FWHM_std ' +- .005 AMP= ' num2str(amplitudeHB2) ' @2.663 FWHM=' FWHM ' AMP=' num2str(amplitudeHB3)];
%                             s = strrep(s, 'MM27 @ 2.7 +- 0.02 FWHM= .03 < .10 +- .005 AMP= .58 @2.66 FWHM=.03 AMP=.51', newAmplitudes);
%                         end
                        
                    case 'pH'
                        s = strrep(s, default_pH, pH_Values{index_pH});
                    otherwise
                        % DO nothing
                end
                fprintf(fitSettingsFileID,'%s\n', s);
            end
            frewind(fitSettingsDefaultFileID);
            fclose(fitSettingsFileID);
        end
    end
end
fclose(fitSettingsDefaultFileID);
end