function calculate_correlations_MM_DF()
r_value_threshold = 0.7;
p_value_threshold = 0.05;

createCorrelationFigures = false;

valueSuffix = '_24';

compare1 = 'DF'; % can be 'DF', 'MM', 'Met'
compare2 = 'Met'; % can be 'DF', 'MM', 'Met'

doMolal_Molar = 'Molar';

suffix = ['_' compare1 '_' compare2 valueSuffix];

% - TE_step: tells which of the TE steps we take from the file
TE_step1 = 1;
TE_step2 = 1;

[absoluteConcentrations1, metaboliteNames1, subjects1, data_path1] = doAbsoluteConcentrationProcessing(compare1, doMolal_Molar, TE_step1);
[absoluteConcentrations2, metaboliteNames2, subjects2, data_path2] = doAbsoluteConcentrationProcessing(compare2, doMolal_Molar, TE_step2);

numOfSubjects2 = length(subjects2);

exponentMM_scaling = floor(log10(median(absoluteConcentrations2(:), 'omitnan')));
absoluteConcentrations2 = absoluteConcentrations2 * (10^(-exponentMM_scaling));

metaboliteNamesTotal = [metaboliteNames2 metaboliteNames1];
numOfMetTotal = length(metaboliteNamesTotal);


% prealocate the concentrations assuming maximal subjects.
% We will cut the ones which are extra later.
concentrationsTotal = zeros(numOfSubjects2, numOfMetTotal);
subjectsTotal = cell(1,numOfSubjects2);

indexTotal = 0;
for indexSubject2 = 1:numOfSubjects2
    currentSubject = subjects2{indexSubject2};
    indexDF = find(strcmp(subjects1, currentSubject));
    if ~isempty(indexDF)
        indexTotal = indexTotal + 1;
        subjectsTotal{indexTotal} = currentSubject;
        concentrationsTotal(indexTotal,:) = [absoluteConcentrations2(indexSubject2,:) absoluteConcentrations1(indexDF, :)];
    end
end
% cut the surplus subjects
subjectsTotal = subjectsTotal(1:indexTotal);
concentrationsTotal = concentrationsTotal(1:indexTotal, :);

% [r,p,rlo,rup] = corrcoef(concentrationsTotal,'rows','pairwise'); %Pearson correlation
[r,p] = corr(concentrationsTotal, 'Type','Spearman', 'Rows', 'pairwise');

confidentCor = r;
confidentCor(p>=p_value_threshold) = NaN;

correlationsFile1 = ['Correlations_Rvalues' suffix '.xlsx'];
xlswrite([data_path2 correlationsFile1], {'R-values'}, 1, 'A1')
xlswrite([data_path2 correlationsFile1], r, 1, 'B2')
xlswrite([data_path2 correlationsFile1], metaboliteNamesTotal', 1, 'A2')
xlswrite([data_path2 correlationsFile1], metaboliteNamesTotal, 1, 'B1')

correlationsFile1 = ['Correlations_Pvalues' suffix '.xlsx'];
xlswrite([data_path2 correlationsFile1], {'P-values'}, 1, 'A1')
xlswrite([data_path2 correlationsFile1], p, 1, 'B2')
xlswrite([data_path2 correlationsFile1], metaboliteNamesTotal', 1, 'A2')
xlswrite([data_path2 correlationsFile1], metaboliteNamesTotal, 1, 'B1')

correlationsFile1 = ['Correlations_ConfidentRvalues' suffix '.xlsx'];
xlswrite([data_path2 correlationsFile1], {'Confident R-values (p<0.05)'}, 1, 'A1')
xlswrite([data_path2 correlationsFile1], confidentCor, 1, 'B2')
xlswrite([data_path2 correlationsFile1], metaboliteNamesTotal', 1, 'A2')
xlswrite([data_path2 correlationsFile1], metaboliteNamesTotal, 1, 'B1')

realConfidentCor = confidentCor;
realConfidentCor(realConfidentCor < r_value_threshold) = NaN;

correlationsFile1 = ['Correlations_RealConfidentRvalues' suffix '.xlsx'];
xlswrite([data_path2 correlationsFile1], {'Confident R-values (p<0.05, R>0.8)'}, 1, 'A1')
xlswrite([data_path2 correlationsFile1], realConfidentCor, 1, 'B2')
xlswrite([data_path2 correlationsFile1], metaboliteNamesTotal', 1, 'A2')
xlswrite([data_path2 correlationsFile1], metaboliteNamesTotal, 1, 'B1')

if createCorrelationFigures
    correlationsFolder = [data_path2 'Correlations\'];
    mkdir(correlationsFolder)
    for indexMetabolite = 1: numOfMetTotal
        for indexMetabolite2 = indexMetabolite+1 : numOfMetTotal
            if (p(indexMetabolite,indexMetabolite2) < p_value_threshold) && ...
                    (r(indexMetabolite,indexMetabolite2) >= r_value_threshold)
                fig = figure;
                figureTitle = {['Correlation of ' metaboliteNamesTotal{indexMetabolite} ' and ' metaboliteNamesTotal{indexMetabolite2} ],...
                    ['P-value: ' num2str(p(indexMetabolite,indexMetabolite2),2) '  R-value: ' num2str(r(indexMetabolite,indexMetabolite2),2)]};
                figureName = ['Correlation ' metaboliteNamesTotal{indexMetabolite} ' and ' metaboliteNamesTotal{indexMetabolite2}];
                scatter(concentrationsTotal(:,indexMetabolite),concentrationsTotal(:,indexMetabolite2))
                refline
                title(figureTitle)
                xlabel(['Conc. ' metaboliteNamesTotal{indexMetabolite} ' [arb. u.]'])
                ylabel(['Conc. ' metaboliteNamesTotal{indexMetabolite2} ' [arb. u.]'])
                savefig([correlationsFolder figureName '.fig'])
                print(fig, [correlationsFolder figureName '.png'], '-dpng')
            end
        end
    end
    close all
end

end

