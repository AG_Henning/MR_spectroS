function [this, this2, weights] = SPECIAL_main_processing(this,verbose)
%% SPECIAL Averaging according to applied Inversion

this = this.SPECIAL_Averaging;

%% Overall averaging

this = this.SPECIAL_Scheme;

%% truncate data

% trunc_ms = 100;
% truncPoint = floor( trunc_ms/(this.Parameter.Headers.DwellTimeSig_ns*1e-6));
% this = this.Truncate(truncPoint);
%% coil combination with svd

% this = this.CombineCoils;     %aus MRSpectro, funktioniert nur, wenn
                                %Phasen davor korrigiert wurden, irgendwas
                                %komisch mit Normierung

%Filtering
oldData = this.Data{1};
oldData = squeeze(oldData);

dwellTime   = this.Parameter.Headers.DwellTimeSig_ns * 1e-9; %in sec
t = (0:length(oldData)-1)*dwellTime;
nCoils        = size(this.Data{1},this.coil_dim);
Fexp = 50;     %in Hz
Fgau = 100;

%None
% oldDataFiltered = oldData;

% %Exponential
% ExpFilter = exp(-t'*Fexp);
% for idxOfCh = 1:nCoils
%     oldDataFiltered(:,idxOfCh) = oldData(:,idxOfCh).*ExpFilter;
% end

%Gaussian
GauFilter = exp ( - t'.^2*(Fgau)^2 );
for idxOfCh = 1:nCoils
    oldDataFiltered(:,idxOfCh) = oldData(:,idxOfCh).*GauFilter;
end

[~,~,V] = svd(oldDataFiltered(:,:)'*oldDataFiltered(:,:));
weights = V(:,1);
comb = oldData(:,:)*weights;

this.Data{1}                                = comb;


%% pc0 correction, cc vor phase
this = this.Phase0_correction_31P;

%% phase first order correction + MPP
if verbose
    plot_spect(this)
    %% ATTENTION: always add a break point here, do phase correction and missing point prediction
    % in the gui (truncate and filter before if necessary) and save the data
    fileNameAfterPhaseCorrectionBase = this.Parameter.Filename(1:end-4);
    fileNameAfterPhaseCorrection = [fileNameAfterPhaseCorrectionBase '.mat'];
    load(fileNameAfterPhaseCorrection, fileNameAfterPhaseCorrectionBase);
    this2 = eval('this = %s', fileNameAfterPhaseCorrectionBase);
    delete(fileNameAfterPhaseCorrection);
else
    this2 = this;
end

end