classdef ZeroFillSettings 
% version: 26 Feb 2014    
    
    properties
        ZerroFillFactor
    end
    
    methods
        
        function this=ZeroFillSettings()
        
            this.ZerroFillFactor=2;
            
        end
        
    end
    
end

