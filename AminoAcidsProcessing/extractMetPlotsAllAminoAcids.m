function extractMetPlotsAllAminoAcids()


pathNameMM = 'MM data path';
sampleCriteriaMM = {'OutputAminoAcids'};
[localFilePathBaseMM] = pathToDataFolder(pathNameMM, sampleCriteriaMM);

FileName= 'Summed_Averaged_TE24.coord';

PathName = [localFilePathBaseMM 'OutputAminoAcids\New_AminoAcids_visible\'];

% [FileName,PathName] = uigetfile('*.coord','Please select .coord files from LCModel','Multiselect','on');

if ~iscell(FileName)
    FileName= {FileName};
end

aminoAcids = {'ALA'; 'ARG';'ASN';'ASP';'CYS';'GLU';'GLN';'GLY';'HIS';'ILE';'LEU';'LYS';'MET';'PHE';'PRO';'SER';'THR';'TRP';'TYR';'VAL'; 'Cr'; 'NAA'};
aminoAcids = {'Cr'; 'NAA'; 'ALA'; 'ARG';'ASN';'ASP';'GLU';'GLN';'LEU';'LYS';'MET';'PHE';'PRO';'THR';'TYR'; };


FontSize = 14;
LineWidth = 1.5;
%
ppm = {};
pData = {};
fData = {};
bData = {};
mmData = {};
rData = {};
close all;

for index =1:length(FileName)
    figure(index);
    %[c1 c2 c3 c4] = textread(strcat(PathName,FileName{i}),'%s %s %s %s');
    c          = textread(strcat(PathName,FileName{index}),'%s');
    nOfPoints  = find(strcmp(c ,'points'),1);
    nOfPoints  = str2num (c{nOfPoints-1});
    
    %ppm axis
    indexOfPpm   = find(strcmp(c,'ppm-axis'))+3;
    endOfMet     = nOfPoints+indexOfPpm-1;
    ppm{index}= str2double( c(indexOfPpm:endOfMet,1));
    
    %phaseData
    indexOfMet   = find(strcmp(c,'phased'))+4;
    endOfMet     = nOfPoints+indexOfMet-1;
    pData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %fit data
    indexOfMet   = find(strcmp(c,'fit'))+5;
    endOfMet     = nOfPoints+indexOfMet-1;
    fData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %background
    indexOfMet   = find(strcmp(c,'background'))+3;
    endOfMet     = nOfPoints+indexOfMet-1;
    bData{index} = str2double( c(indexOfMet:endOfMet,1));
    
    %residual
    rData{index} = pData{index} - fData{index};
    
    %scaling calculation 
    scale = max(pData{1});
    
    %plotting
    hold on
    p = plot(ppm{index}, (pData{index} - bData{index}) ./ scale, ...
        ppm{index},(fData{index} - bData{index}) ./ scale, ...
        ppm{index},bData{index} ./ scale - 1.7, ...
        ppm{index},rData{index} ./ scale - 1.7);
    text(0.47,0.05,'Data + Fit', 'FontSize', FontSize);
%     text(0.47,-0.03,'MM', 'FontSize', FontSize);
    text(0.47,-1.7,'Residual', 'FontSize', FontSize);
%     text(0.47,-1.8,'Baseline', 'FontSize', FontSize);
    for plots = 1:length(p)
        set(p(plots),'LineWidth',LineWidth);
    end
        
    for indexMetabolite = 1:length(aminoAcids)
        %evaluate each metabolite
        individualMet = strsplit(aminoAcids{indexMetabolite},'+');
        metaboliteSpectrum = zeros(nOfPoints,1);
        for indexIndividualMet=1:length(individualMet)
            %sum up the individual components
            indexOfMet   = find(strcmp(c,individualMet{indexIndividualMet}),1,'last')+3;
            endOfMet     = nOfPoints+indexOfMet-1;
            if (indexOfMet > indexOfPpm) %make sure that we indeed have a metabolite quantification spectrum
                metaboliteSpectrum = metaboliteSpectrum + str2double(c(indexOfMet:endOfMet,1)) - bData{index};
            end
        end
        pMetabolite = plot(ppm{index}, metaboliteSpectrum ./ scale - indexMetabolite * 0.1);
        text(0.47, indexMetabolite * -0.1,aminoAcids{indexMetabolite}, 'FontSize', FontSize);
        set(pMetabolite,'LineWidth',LineWidth);
    end
    
%     plot(ppm{index}, bData{index} ./ scale - 1.7);
    xlim([0.5 8.8]);  
    xlabel('[ppm]');
    ylim([-1.9 1.3]);
    set(gca,'xDir','reverse')
    set(gca,'ytick',[]);
        
    title('Spectrum with fitted amino acids')
    set(gca,'fontsize',FontSize);
    clear   nOfPoints indexOfMet i
end


