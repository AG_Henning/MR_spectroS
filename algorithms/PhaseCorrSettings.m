classdef PhaseCorrSettings 
% version: 09 Oct 2014
% Update : 26 Sep 2015
% method : future releases
% phase0 in degrees
% phase1 in degress per Hz
% freqCentre in Hz, for linear phase correction


    properties
        method
        phase0
        phase1
        freqCentre
        
    end
    
    methods
        
        function this=PhaseCorrSettings
            this.method='manual';
            this.phase0=0;
            this.phase1=0;
            this.freqCentre =0; %water
            
        end
    end
    
end

