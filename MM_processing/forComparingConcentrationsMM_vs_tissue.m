function forComparingConcentrationsMM_vs_tissue
%after running the script no clear differences btw GM and WM can be found
% scaling was tested for Cr(CH2) and M0.9

%tableConcentrationsSubjects has to be loaded for GM and WM respectively
for index = 1:length(tableConcentrationsSubjects)
    concentrations = tableConcentrationsSubjects{index};
    lengthTable = size(concentrations,2);
    if index == 1
        metabolites = concentrations(1,2:3:lengthTable-2);
        concentrationsScaled = zeros(length(tableConcentrationsSubjects),length(metabolites));
    end
    concentrationScaler = concentrations{2,lengthTable-4}; %Cr(CH2)
    concentrationScaler = concentrations{2,5}; % M0.9
    concentrationsScaled(index,:) = cell2mat(concentrations(2,2:3:lengthTable-2))./concentrationScaler;
end

for indexMet = 1:length(metabolites)
    figure(indexMet)
    hold on
    plot(concentrationsScaled(:,indexMet))
    title(metabolites{indexMet})
end

    