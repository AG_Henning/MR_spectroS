function lineBroadeningExchangingPeaks()

%sample data and the equations from "Spin Dynamics - Malcolm Levitt
% page 520 chapter 19.5.2 and Lorentzian lineshape: pag 97 Chapter 5.8.2
gamma = [-3000:0.1:3000]*2*pi;
gammaA = -1000*2*pi;
gammaB = 1000*2*pi;
lambda=0;

plotPpm = false;
[S, figId] = calculateLineShape(gamma, gammaA, gammaB, 0.5*1e3, lambda, plotPpm, []);
[S, figId] = calculateLineShape(gamma, gammaA, gammaB, 1.0*1e3, lambda, plotPpm, [], figId);
[S, figId] = calculateLineShape(gamma, gammaA, gammaB, 2.5*1e3, lambda, plotPpm, [], figId);
[S, figId] = calculateLineShape(gamma, gammaA, gammaB, 3.5*1e3, lambda, plotPpm, [], figId);
[S, figId] = calculateLineShape(gamma, gammaA, gammaB, 6.3*1e3, lambda, plotPpm, [], figId);
legend('k=0.5x10^3s^{-1}','k=1.0x10^3s^{-1}','k=2.5x10^3s^{-1}','k=3.5x10^3s^{-1}','k=6.3x10^3s^{-1}')

% -------------------------------------------------------------
water = 0.0; %ppm
ppmRange = [-10:0.0001:10];

omega = 42.577 * 9.41 * (5.75-4.7);
scanfrequency = 399*2*pi; 
% scanfrequency = 42.57747 * 2.8*2*pi;%3T 
gamma = ppmRange * scanfrequency;
gammaWater = water * scanfrequency;
lambda = 0;

%values from Haris 2012, NMR Biomed
PCr_1 = 2.5; % ppm
PCr_2 = 1.8; % ppm
Cr = 1.8; % ppm
k_PCr_1 = 140;%s-1
k_PCr_2 = 120;%s-1
k_Cr = 950;%s-1

gammaPCr_1 = PCr_1 * scanfrequency;
gammaPCr_2 = PCr_2 * scanfrequency;
gammaCr = Cr * scanfrequency;
plotPpm = true;
[S, figId] = calculateLineShape(gamma, gammaWater, gammaPCr_1, k_PCr_1, lambda, plotPpm, ppmRange);
[S, figId] = calculateLineShape(gamma, gammaWater, gammaPCr_2, k_PCr_2, lambda, plotPpm, ppmRange, figId);
[S, figId] = calculateLineShape(gamma, gammaWater, gammaCr, k_Cr, lambda, plotPpm, ppmRange, figId);
legend('PCr 2.5ppm k=140s^{-1}', 'PCr 1.8ppm k=120s^{-1}', 'Cr 1.8ppm k=950s^{-1}');

Urea = 5.75-4.7; % ppm
k_Urea = 7;% k= 6.76 Hz Occ lobe GM rich; k = 8.00 Hz Parietal WM [Fichtner 9.4T, MRM 2017]

gammaUrea = Urea * scanfrequency;
plotPpm = true;
[S, figId] = calculateLineShape(gamma, gammaWater, gammaUrea, 0.1, lambda, plotPpm, ppmRange+4.7);
[S, figId] = calculateLineShape(gamma, gammaWater, gammaUrea, k_Urea, lambda, plotPpm, ppmRange+4.7,figId);
[S, figId] = calculateLineShape(gamma, gammaWater, gammaUrea, 13, lambda, plotPpm, ppmRange+4.7,figId);
legend('Urea 5.75ppm k=0.1s^{-1}','Urea 5.75ppm k=7s^{-1}','Urea 5.75ppm k=13s^{-1}');
if plotPpm
    xlim([5.7 5.8])
else
    xlim([410 430])
end
ylim([-0.001 0.1])

%values from [Fichtner 9.4T, MRM 2017]
DF58 = 5.8-4.7; % ppm
DF68 = 6.8-4.7; % ppm
DF82 = 8.2-4.7; % ppm
DF83 = 8.3-4.7; % ppm
DF85 = 8.5-4.7; % ppm
NAA  = 7.82-4.7; % ppm
k_DF58 = 6.76;% k= 6.76 Hz Occ lobe GM rich; k = 8.00 Hz Parietal WM 
k_DF68 = 2.34;% k= 2.34 Hz Occ lobe GM rich; k = 2.42 Hz Parietal WM 
k_DF82 = 9.32;% k= 9.32 Hz Occ lobe GM rich; k = 10.6 Hz Parietal WM 
k_DF83 = 13.8;% k= 13.8 Hz Occ lobe GM rich; k = 13.4 Hz Parietal WM 
k_DF85 = 3.31;% k= 3.31 Hz Occ lobe GM rich; k = 4.05 Hz Parietal WM 
k_NAA  = 0.74;% k= 0.74 Hz Occ lobe GM rich; k = 0.92 Hz Parietal WM 

gammaDF58 = DF58 * scanfrequency;
gammaDF68 = DF68 * scanfrequency;
gammaDF82 = DF82 * scanfrequency;
gammaDF83 = DF83 * scanfrequency;
gammaDF85 = DF85 * scanfrequency;
gammaNAA  = NAA  * scanfrequency;
plotPpm = true;
[S, figId] = calculateLineShape(gamma, gammaWater, gammaDF58, k_DF58, lambda, plotPpm, ppmRange+4.7);
[S, figId] = calculateLineShape(gamma, gammaWater, gammaDF68, k_DF68, lambda, plotPpm, ppmRange+4.7,figId);
[S, figId] = calculateLineShape(gamma, gammaWater, gammaDF82, k_DF82, lambda, plotPpm, ppmRange+4.7,figId);
[S, figId] = calculateLineShape(gamma, gammaWater, gammaDF83, k_DF83, lambda, plotPpm, ppmRange+4.7,figId);
[S, figId] = calculateLineShape(gamma, gammaWater, gammaDF85, k_DF85, lambda, plotPpm, ppmRange+4.7,figId);
[S, figId] = calculateLineShape(gamma, gammaWater, gammaNAA,  k_NAA,  lambda, plotPpm, ppmRange+4.7,figId);
legend('DF_{5.75} k=6.76s^{-1}','DF_{6.80} k=2.346s^{-1}','DF_{8.20} k=9.32s^{-1}','DF_{8.30} k=13.8s^{-1}','DF_{8.50} k=3.31s^{-1}', 'NAA k=0.74s^{-1}');
if plotPpm
    xlim([5.7 8.6])
    xlim([4.6 8.6])
else
    xlim([400 1600])
end
ylim([-0.001 0.25])
set(gca,'xdir','reverse')
xlabel('\delta (ppm)')
ylabel('Signal (arb. u.)')

% results of FWHM
%DF58 = 2.17 Hz
%DF68 = 0.7581 Hz
%DF82 = 2.95 Hz
%DF83 = 4.389 Hz
%DF85 = 1.0374 Hz
%NAA  = 0.2394 Hz
end

function [S, figId] = calculateLineShape(gamma, gammaA, gammaB, k, lambda, plotPpm, ppmRange, figId)
gammaDif = gammaB-gammaA;
R = sqrt(abs(k^2-(gammaDif/2)^2));
gammaM = 0.5*(gammaA+gammaB);

% signal_L = 1 ./ ((lambda+k) + 1i*(gamma-(gammaM+R)));
% signal_R = 1 ./ ((lambda+k) + 1i*(gamma-(gammaM-R)));
%     
% S = 0.5 .*(1-1i*k/R) .* signal_L+...
%     0.5 .*(1+1i*k/R) .* signal_R;
S = 0.5 .*(1-1i*k/R) .* lorentz(gamma, gammaM+R, lambda+k)+...
    0.5 .*(1+1i*k/R) .* lorentz(gamma, gammaM-R, lambda+k);

if exist('figId', 'var')
    figure(figId)
else
    figId = figure;
    hold on
end
if plotPpm 
    plot(ppmRange, real(S))
else
    plot(gamma/(2*pi), real(S))
end
end

function signal = lorentz(gamma,gammaL, lambda)
    signal = 1 ./ (lambda + 1i*(gamma-gammaL));
end