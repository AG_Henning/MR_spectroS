function summarize_Deleted_Averages(subjects, pathBase, TEs, fileSuffix)

if ~exist('fileSuffix', 'var')
    fileSuffix = '';
end

numberOfSubjects = length(subjects);
numberOfTEs = length(TEs);
paths = cell(1,numberOfSubjects);

for indexSubj = 1:numberOfSubjects
    paths{indexSubj} = [pathBase subjects{indexSubj} '\\'];
end
tableDeletedAverages = {};
tableRescaleFactors = {};
for indexSubj = 1:numberOfSubjects
    subjectIndexBase = (indexSubj-1)* numberOfTEs;
    tableDeletedAverages{subjectIndexBase+1,1} = subjects{indexSubj};
    tableRescaleFactors{subjectIndexBase+1,1} = subjects{indexSubj};
    %% reconstruct Macromolecule data
    for indexTE = 1:numberOfTEs
        tableDeletedAverages{subjectIndexBase+indexTE,2} = ['TE' num2str(TEs(indexTE))];
        tableRescaleFactors{subjectIndexBase+indexTE,2} = ['TE' num2str(TEs(indexTE))];
        filename = [paths{indexSubj} subjects{indexSubj} '_TE' num2str(TEs(indexTE)) '.mat'];
        load(filename, 'a');
        deletedAverages = a.Parameter.DeleteMovedAveragesSettings.deletedAverages;
        tableRescaleFactors{subjectIndexBase+indexTE,3} = a.Parameter.ReconFlags.isRescaled;
        if ~isempty(deletedAverages)
            numDeletedAverages = length(deletedAverages);
            for iDeletedAverage = 1:numDeletedAverages
                tableDeletedAverages{subjectIndexBase+indexTE,2+iDeletedAverage} = deletedAverages(iDeletedAverage);
            end
        end
    end
end

save([pathBase, 'tableDeletedAverages' fileSuffix '.mat'], 'tableDeletedAverages');
save([pathBase, 'tableRescaleFactors' fileSuffix '.mat'], 'tableRescaleFactors');
%% save table to file
exportFileXlsx = [pathBase, 'DeletedAverages' fileSuffix '_Summaries.xlsx'];
cellPosition = 'A1';
xlswrite(exportFileXlsx, tableDeletedAverages, 1, cellPosition);
