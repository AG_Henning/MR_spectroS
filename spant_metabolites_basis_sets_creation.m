
function spant_metabolites_basis_sets_creation()

plotBasis = false;

metabolites     = {'Asp', 'Cr_singlet_tot_3.925', 'Cr_singlet_tot_3.028', 'GABA', 'Gln', 'Glu', 'Glyc', ...
    'GSH', 'Lac', 'mI', 'NAA_2', 'NAA_1', 'NAAG', 'tCho_PE',  'Scyllo',    'Tau'};

numberOfMet = length(metabolites);
abbreviations = metabolites;
metaboliteNames = metabolites;
scalingFactors = zeros(numberOfMet,1)+1;

abbreviationsUnique = unique(abbreviations);

pathName = 'DF data path';
sampleCriteria = {};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);

filePathBase = strcat(localFilePathBase,  'Basis_sets/final_T2_met_MM_paper/sLASER_new_UF_7ppm/');
filePathBaseOut = strcat(filePathBase, 'Out/');
orderTEs = {'24' '32' '40' '52' '60'};
filePaths = strcat(filePathBase, 'TE', orderTEs , '\');
filePathsOut = strcat(filePathBaseOut, 'TE', orderTEs , '\');

for indexTE = 1 : length(orderTEs)
    filePath = filePaths{indexTE};
    filePathOut = filePathsOut{indexTE};
    for indexMetabolite = 1 : length(abbreviationsUnique)
        combined_metabolite_name = abbreviationsUnique{indexMetabolite};
        indeces = strcmp(abbreviations,combined_metabolite_name);
        numOfMetToCombine = sum(indeces);
        metabolitesToMix = metaboliteNames(indeces);
        factorsToMix = scalingFactors(indeces);
        %
        dataMix           = cell(1,numOfMetToCombine);
        metaboliteNameMix = cell(1,numOfMetToCombine);
        singletPresentMix = cell(1,numOfMetToCombine);

        for indexMetabolitesToMix =  1:numOfMetToCombine
            [dataMix{indexMetabolitesToMix}, bandwidth, scanFrequency, metaboliteNameMix{indexMetabolitesToMix}, ...
                singletPresentMix{indexMetabolitesToMix}] = ...
                ImportLCModelBasis(filePath, metabolitesToMix{indexMetabolitesToMix}, ...
                plotBasis);
        end
        %combine
        combinedData = dataMix{1} * factorsToMix(1);
        for indexMetabolitesToMix =  2:numOfMetToCombine
            % average the contributions. Don't worry about the
            % ref. singlet, it does not influence the fitting
            combinedData = combinedData + factorsToMix{indexMetabolitesToMix} * ...
                dataMix{indexMetabolitesToMix};
        end
        
        dwellTimeMs = 1/bandwidth * 1e3;
        addSinglet = false;

        ExportLCModelBasis(combinedData', dwellTimeMs, scanFrequency, filePathOut, ...
                combined_metabolite_name, combined_metabolite_name, addSinglet);
    end
end
