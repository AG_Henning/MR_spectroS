function doFitting_DF_MM(fitAllSubjects)

pathName = 'DF data path';
sampleCriteria = {'OutputMM', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
fitAllSubjects = false;
fitAminoAcids = true;
orderTEs = {'24'};
subjects = {'1717';'2017';'3373';'3490';'5771';'6249';'6971'; '7338'; '7782';'9810'};

extensionTable = '.table';
extensionCoord = '.coord';
defaultTE = 'TE24';
defaultSubject = 'XXXX';
defaultLCModelUser = 'tborbath';

if fitAllSubjects
    controlFilesBase = 'fitsettings_Subject_XXXX_';
    controlFilesBaseTE = strcat(controlFilesBase, 'TE');
    controlFilesBaseSuffix = '_MMBaseline_Cre.control';
    outputFileNameBase = strcat(defaultSubject, '_MMBaseline_TE');
else
    controlFilesBase = 'fitsettings_Summed_Averaged_';
    controlFilesBaseTE = strcat(controlFilesBase, 'TE');
    controlFilesBaseSuffix = '_MMBaseline.control';    
    if fitAminoAcids
        controlFilesBaseSuffix = '_AminoAcids.control';
    end
    outputFileNameBase = 'Summed_Averaged_MMBaseline_TE';
end

%% file paths setup
controlFilesPathRemote = '/Desktop/DATA_df/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
if fitAminoAcids
    LCModelOutputFilesPathRemote = '/Desktop/DATA_df/OutputAminoAcids/';
    LCModelOutputPath = [localFilePathBase, 'OutputAminoAcids/'];
else
    LCModelOutputFilesPathRemote = '/Desktop/DATA_df/OutputMM/';
    LCModelOutputPath = [localFilePathBase, 'OutputMM/'];
end
defaultControlFile = strcat(controlFilesBase, 'Default', controlFilesBaseSuffix);


if fitAllSubjects
    subjectsPath = strcat(subjects, '/');
else
    subjects = {'Summed'};
    subjectsPath = {''};
end

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath);
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath);
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath);
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath);

%% basic configurations
LCModelTableFits = strcat(outputFileNameBase, orderTEs, extensionTable);
LCModelTableFitsCoord = strcat(outputFileNameBase, orderTEs, extensionCoord);
LCModelOutputFiles = strcat(outputFileNameBase, orderTEs);
LCModelControlFiles = strcat(controlFilesBaseTE, orderTEs, controlFilesBaseSuffix);

numberOfTes = length(orderTEs);
numberOfSubjects = length(subjects);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

%% create the control file series
if fitAllSubjects
    createLCModelConfigsTEseries('MM', controlFilesPathLocal, defaultControlFile, orderTEs, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser, subjects, defaultSubject);
else
    % call the Configuration file creator without subject configuration
    createLCModelConfigsTEseries('MM', controlFilesPathLocal, defaultControlFile, orderTEs, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser);
end

%% do the actual LCModel fitting and T2 calculation
for indexCurrentSubject = 1:numberOfSubjects
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    if fitAllSubjects
        currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
        currentLCModelTableFits = strrep(LCModelTableFits, defaultSubject, subjects{indexCurrentSubject});
        currentLCModelTableFitsCoord = strrep(LCModelTableFitsCoord, defaultSubject, subjects{indexCurrentSubject});
    else
        currentControlFiles = LCModelControlFiles;
        currentOutputFiles = LCModelOutputFiles;
        currentLCModelTableFits = LCModelTableFits;
        currentLCModelTableFitsCoord = LCModelTableFitsCoord;
    end
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
    
    numOfAvailableTEs = length(orderTEs);
    %% get fitted concentrations
    tableConcentrations{indexCurrentSubject} = importConcentrationLCModelTable(currentLCModelTableFits, ...
        subjectsLCModelOutputPath{indexCurrentSubject}, orderTEs, 'MM');
        
    %% mean CRLB
    CRLBs = cell2mat((tableConcentrations{indexCurrentSubject}(2:numberOfTes + 1,3:3:end)));
    meanCrlbTEs = mean(CRLBs,2, 'omitnan');
    meanCrlb = mean(meanCrlbTEs);
        
    %% display means
    table(1,1) = {'TE'};
    table(1,2:numOfAvailableTEs+1) = orderTEs(1:end);
    table(2,1) = {'CRLB'};
    table(2,2:numOfAvailableTEs+1) = num2cell(meanCrlbTEs(1:end));
    table(3,1) = {'meanCRLB'};
    table(3,2) = num2cell(meanCrlb);
    display(table)
    
    %% save table to file
    exportFileXlsx = [LCModelOutputPath ,'Quantification_Summaries_MMBaseline.xlsx'];
    startIndex = 1+ 5 *(indexCurrentSubject-1);
    cellPositionTitle = ['A' num2str(startIndex)];
    xlswrite(exportFileXlsx, {['Subject: ' subjects{indexCurrentSubject}]}, 1, cellPositionTitle);
    cellPosition = ['A' num2str(startIndex + 1)];
    xlswrite(exportFileXlsx, table, 1, cellPosition);
end

if fitAllSubjects
    fileNameSuffix = 'Subjects';
else
    fileNameSuffix = 'Summed';
end
tableConcentrations_name = 'tableConcentrations';
tableConcentrations_nameSuffixed = [tableConcentrations_name fileNameSuffix];
eval( [tableConcentrations_nameSuffixed, '=',  tableConcentrations_name]);
save([LCModelOutputPath, tableConcentrations_nameSuffixed, '.mat'], tableConcentrations_nameSuffixed)
end