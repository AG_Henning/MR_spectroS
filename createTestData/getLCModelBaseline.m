function [baselineFid] = getLCModelBaseline()

exportFolders = {'1 Normal recon', '5 Water Supp Off', '8 Rescale Water Supp Off'};

subjects    = {{1658; 2017; 5771; 6971;};...
    {3373; 7338;}; ...
    {3490;      6249;       1717;   3373;    7338;       9810;      7782};};
Labels      = {{'Flat_drop_H20'; 'Flat'; 'Flat'; 'Flat';};...
    {'Wildish'; 'Wildish'}; ...
    {'Very_wild';'Very_wild';'Wild'; 'Wild';'Wildish'; 'Wildish'; 'Normal'};};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolders);
pathBaselineExport = strcat(pathBaseExportFiles, '11 Simulations\Baselines\');
pathBaseExportFiles(end:end+1) = '_\';
dataPathMatFiles = strcat(pathBaseExportFiles, exportFolders{1}, '\');

extensionCoord = '.coord';
extensionMat = '.mat';
phase90 = -90 * pi/180;
zeroFillFactor = 2;

runningIndex = 0;
for indexFolder = 1:3
    LCModelOutputPathBase = strcat(pathBaseExportFiles, exportFolders{indexFolder}, '\');
    for indexSubj = 1 : length(subjects{indexFolder})
        runningIndex = runningIndex + 1;
        currentSubject = num2str(subjects{indexFolder}{indexSubj});
        LCModelOutputPath = strcat(LCModelOutputPathBase, currentSubject, '\TE24\');

        fileName = [currentSubject '_TE24'];
        
        LCModelTableFitsCoord = strcat(fileName,  extensionCoord);
        reconstructedSpectra = strcat(dataPathMatFiles, currentSubject, '\TE24\', fileName,  extensionMat);
        
        
        load(reconstructedSpectra, 'a');
        currentLCModelCoordFit      = LCModelTableFitsCoord;
        [nOfPoints, ppmVector_lcm, phasedData_lcm, fitData, baselineData_lcm, residualData, ...
            metaboliteNames, metaboliteData, tableConcentrations, ppmStart_lcm, ppmEnd_lcm, ...
            scanFrequency,  frequencyShift, ppmGap, phase0_deg_lcm, phase1_deg_ppm_lcm] = ...
            extractFits(LCModelOutputPath,currentLCModelCoordFit);
        
        water_ppm = 4.7;
        
        % align the two spectra
        fid = a.Data{1}(:,1,1,1,1,1,1,1,1,1,1,1)';

        samples = length(fid);
        scanFreq = a.Parameter.Headers.ScanFrequency*1e-6;
        bandWidth     = a.Parameter.Headers.Bandwidth_Hz;
        frequencyRange_zf  = linspace(-bandWidth/2,bandWidth/2, samples * zeroFillFactor);
        ppm_zf    = frequencyRange_zf/scanFreq + water_ppm;
        
        % max of the LCModel fit
        maxSpectrum_lcm = max(phasedData_lcm+baselineData_lcm);
        baselineData_lcm = baselineData_lcm ./ maxSpectrum_lcm;
                
        c = polyfit([ppmVector_lcm(end-300:end); ppm_zf(ppm_zf<0)'], [baselineData_lcm(end-300:end); 0*(ppm_zf(ppm_zf<0))'],30);
        baselineData_Low = polyval(c,ppm_zf(ppm_zf<ppmEnd_lcm));
        
        c = polyfit([ppmVector_lcm(1:300); ppm_zf(ppm_zf>4.7)'], [baselineData_lcm(1:300); 0*(ppm_zf(ppm_zf>4.7))'],30);
        baselineData_High = polyval(c,ppm_zf(ppm_zf>ppmStart_lcm));
        
        figure; plot(ppmVector_lcm,baselineData_lcm); 
        hold on; plot(ppm_zf(ppm_zf<ppmEnd_lcm),baselineData_Low)
        hold on; plot(ppm_zf(ppm_zf>ppmStart_lcm),baselineData_High)
        
        baselineFullReal = [baselineData_Low(2:end)'; flipud(baselineData_lcm); baselineData_High(1:end-1)'];
        
        figure; plot(ppm_zf,baselineFullReal); 
        
        baselineFid = ifft(ifftshift(complex(baselineFullReal)));
        baselineFid(length(baselineFid)/2:end)=0;
        baselineSpectrumPhased90 = fftshift(fft(baselineFid .* exp(1i*phase90))).*2; %multiply by 2 to account for nulling the half of the FID
        baselineFullImag = real(baselineSpectrumPhased90);
        baselineFid = ifft(ifftshift(complex(baselineFullReal, baselineFullImag)));
        
        aBaseline = a;
        aBaseline.Data{1} = baselineFid;
        aBaseline.ExportLcmRaw(pathBaselineExport,['Baseline_', num2str(runningIndex), Labels{indexFolder}{indexSubj}],false);
    end
end

%% extract a sample dataset with the new data naming.
% currentSubject = 'Subj_1';
% fileName = [currentSubject '_TE24'];
% reconstructedSpectra = strcat(dataPathMatFiles, currentSubject, '\TE24\', fileName,  extensionMat);
% load(reconstructedSpectra, 'a');
%% single lipid peak 1.3ppm 
runningIndex = runningIndex + 1; %would be 14

fid = a.Data{1}(:,1,1,1,1,1,1,1,1,1);
ppmVector = a.getPpmVector();
time = (0:length(fid) - 1)' * (1/a.Parameter.Headers.Bandwidth_Hz);
%create peak to fit gradient modulation downfield
scanFrequency = a.Parameter.Headers.ScanFrequency;
f         =  (1.3-4.66) *scanFrequency*1E-6; %Hz
T2 = 0.2;   %s
singlet   =  exp(1i*2*pi*f*time).*exp(-time/T2);
Fgau = 400;   %parameter that sets the decay rate
W = exp(- time.^2*(Fgau)^2);
singletW = singlet.*W;

maxValue = max(real(fftshift(fft(singletW))));
singletW = singletW ./ maxValue .* 0.3; %scale to 0.3 of NAA peak later

figure; 
plot(ppmVector, real(fftshift(fft(singletW)))); 
xlim([0.6 4.2])

aBaseline = a;
aBaseline.Data{1} = singletW;
aBaseline.ExportLcmRaw(pathBaselineExport,['Baseline_', num2str(runningIndex), 'Lipid_1.3'],false);

%% phased single lipid peak 1.3ppm
runningIndex = runningIndex + 1;
phasedSingletW = singletW * exp(-pi*1i*90/180);

figure; 
plot(ppmVector, real(fftshift(fft(phasedSingletW)))); 
xlim([0.6 4.2])

aBaseline = a;
aBaseline.Data{1} = phasedSingletW;
aBaseline.ExportLcmRaw(pathBaselineExport,['Baseline_', num2str(runningIndex), 'Lipid_1.3_phased'],false);

%% zero baseline
runningIndex = runningIndex + 1;
zeroBaseline = zeros(size(singletW));

aBaseline = a;
aBaseline.Data{1} = zeroBaseline;
aBaseline.ExportLcmRaw(pathBaselineExport,['Baseline_', num2str(runningIndex), 'Zero'],false);

end