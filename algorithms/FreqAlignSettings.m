classdef FreqAlignSettings < hgsetget
    
    
    properties
        domain;
        indecesOfInterest
        displayPlots
        
    end
    
    methods
        
        function this= FreqAlignSettings()
            
            this.domain            ='time';
            this.indecesOfInterest =[]    ;
            this.displayPlots      =true  ;
        end
        
    end
    
end
