function outputFile = createLCModelConfigfMRS(downField_MM_Met, controlFilesPath, outputControlFilesPath, defaultControlFile, ...
    currentfMRS, defaultfMRS, defaultLCModelUser, currentLCModelUser, spectrumFileName, defaultSpectrum,...
    waterRefFileName, defaultWaterRef, subjectsPath, defaultSubjectsPath, subjectID, defaultSubject, MMBType,...
    defaultMMB, fMRSDataSuff)
% Intro comments need to be rewritten
% Creates the control files for the TE series, given a default control file.
% the newly generated files will be placed in the same path as indicated by
% the controlFilesPath.
% downField_MM_Met can be: 'DF' 'MM', 'Met' or 'pH'
% A sample input:
% controlFilesPath = 'Fitsettings/MMocc_noExtraMM_06knots/';
% defaultControlFile = 'fitsettingsDefault.control'

controlFileSuffix = '.control';

fitSettingsDefaultFileID = fopen([controlFilesPath defaultControlFile], 'r');

mkdir(outputControlFilesPath);
outputFile = strcat(outputControlFilesPath, 'fitsettings_fMRS_', spectrumFileName, controlFileSuffix);

if exist('fMRSDataSuff', 'var') & ~isempty(fMRSDataSuff)
    spectrumFileName = strcat(fMRSDataSuff, spectrumFileName);
end

fitSettingsFileID = fopen(outputFile, 'w');
display(outputFile);

while(~feof(fitSettingsDefaultFileID))
    s = fgetl(fitSettingsDefaultFileID);
    k = strfind(s,'FILRAW');
        currentfMRSextended = strcat(currentfMRS, MMBType);
    if strfind(spectrumFileName,'_woBaseline')
        if strfind(MMBType,'v2')
            currentfMRSNew = strcat(currentfMRS,'_v2_woBaseline');
        else
            currentfMRSNew = strcat(currentfMRS,'_woBaseline');
        end
    else
%         if strfind(MMBType,'v2')
%             currentfMRSNew = strcat(currentfMRS,'_v2');
%         else
            currentfMRSNew = currentfMRS;
%         end
    end
    if ~isempty(k)
        s = strrep(s, defaultLCModelUser, currentLCModelUser);
        s = strrep(s, defaultfMRS, currentfMRSNew);
        s = strrep(s, defaultSubjectsPath, subjectsPath);
        s = strrep(s, defaultSpectrum, spectrumFileName);
        s = strrep(s, defaultWaterRef, waterRefFileName);
        s = strrep(s, defaultSubject, subjectID);
    else
        s = strrep(s, defaultLCModelUser, currentLCModelUser);
        s = strrep(s, defaultfMRS, currentfMRSextended);
        s = strrep(s, defaultSubjectsPath, subjectsPath);
        s = strrep(s, defaultSpectrum, spectrumFileName);
        s = strrep(s, defaultWaterRef, waterRefFileName);
        s = strrep(s, defaultSubject, subjectID);
        s = strrep(s, defaultMMB, MMBType);
    end
    
    fprintf(fitSettingsFileID,'%s\n', s);
end
fclose(fitSettingsFileID);
fclose(fitSettingsDefaultFileID);
end