function ppmVector = ppmVectorFunction(scanFrequency_Hz, bandwidth_Hz, nTimepoints, nucleus)
if ~exist('nucleus', 'var')
    nucleus = '1H';
end

bw    = linspace(-bandwidth_Hz/2,bandwidth_Hz/2,nTimepoints);

if strcmp(nucleus,'31P')
    ppmVector    = bw/(scanFrequency_Hz*1e-6);
else
    ppmVector    = bw/(scanFrequency_Hz*1e-6) + 4.7;
end
