function [a, stats] = reconstruct_DF(fid_id, phase0, phase1Hz)
%%
a = MR_spectroS(fid_id,1);

%% there should be actually this phase correction, but we don't use it.
% phase0 = 0;
% phase1_ms = 0.020;
% phase1Hz = phase1_ms*(2e-3) * 180;
% a.Parameter.PhaseCorrSettings.phase0     =  phase0;
% a.Parameter.PhaseCorrSettings.phase1     =  phase1Hz;
% a.Parameter.PhaseCorrSettings.freqCentre =  920; % ref at 7 ppm 
% a = a.PhaseCorrection;
%%
trunc_ms = 250;
truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
a = a.Truncate(truncPoint);

a = a.FrequencyAlign;
% a = a.DeleteMovedAverages;
a = a.Rescale;
a = a.ReconData;
a = a.AverageData;
a = a.EddyCurrentCorrection;
a = a.CombineCoils;
%% setup the settings for the FrequencyAlignFreqDomain
a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;

%indices of Interest: given in ppm
% water peak
a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = [3.028];

%set zeroFillingParameter to get smooth approximations
a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;

%search area (+-) in ppm
a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.1;
%spline filtering is not needed if the spectra are not too noisy.
%flag to do spline filtering
a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
%spline filtering coefficient
a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;
% do the actual Frequency Alignment
a = a.FrequencyAlignFreqDomain;
%% do a very strong water suppression by applying it 3 times
a.Parameter.HsvdSettings.bound = [-160 160];
a.Parameter.HsvdSettings.bound = [-200 200];
a.Parameter.HsvdSettings.n = size(a.Data{1},1);
a.Parameter.HsvdSettings.p = 25;
[~, a] = a.Hsvd;
[~, a] = a.Hsvd;
[~, a] = a.Hsvd;
%% Reverse MC
a = a.ReverseMC; 
% The fitting results of fitting both up-field and down-field at the same
% time showed, that the best results are achieved if HSVD water suppression
% is applied both before and after the ReverseMC. 
% Secondly, the fitting showed, that using a more flexible baseline
% dkntmn=0.25 is the best for having comparable results and to accomodate
% the residual water signals.
% This was concluded after intensive evaluations! Don't redo it. ;)

%%
trunc_ms = 250; %adapted it to 250 ms, since we use also the upfield for quantification. Maybe change it back to 150ms, if results are worse
trunc_ms = 200;
truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
a = a.Truncate(truncPoint);

%% due to some weird things happening with the ReverseMC it is questionable, whether one needs a broader HSVD....
a.Parameter.HsvdSettings.bound = [-200 200];
[~, a] = a.Hsvd;
%% Phase correction
if ~exist('phase0','var')
    phase0 = 0;
end
if ~exist('phase1Hz','var')
    phase1Hz = 0;
end
a.Parameter.PhaseCorrSettings.phase0     =  phase0;
a.Parameter.PhaseCorrSettings.phase1     =  phase1Hz;
a.Parameter.PhaseCorrSettings.freqCentre =  0;
a = a.PhaseCorrection;

% the noise is calculated from the noise window of the first 500 points (-5 to -3 ppm)
% subtracting a 1st order polynomial from the points
% The peak amplitude is calculated from the real spectra
list = {[7.7 7.95] 'NAA_DF';[1.9 2.1] 'NAA'; [2.9 3.1] 'Cr';[2.25 2.45] 'Glu'; [3.1 3.3] 'tCho'};
stats = a.Stats(list);
end