function calculate_Mean_SNR_FWHM_31P_T2()
clear; clc; close all;

pathName = 'T2 31P Paper Data';
% sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName);

subjects = {'9873';'1090';'3490';'9810';'1658';'2017';'6524';'3009';...
    '6260';'2823';'2455';'4085'};
prefixes = {'2019-09-30_';'2019-10-10_';'2019-10-14_';'2019-10-14_';...
    '2019-10-22_';'2019-11-07_';'2019-11-07_';'2019-11-20_';...
    '2019-11-20_';'2019-12-05_';'2020-07-22_';'2020-07-23_';};
suffixFolder = '_SurfCoil';

%be careful with Pi intra/extra, NADH and bATP!
metaboliteNames = {'PE', 'PC', 'Pi_int', 'GPE', 'GPC', 'PCr', 'gATP', 'aATP', 'NADH'};
centerPpm =       [ 6.77, 6.23, 4.83,     3.51,   2.96,  0,     -2.48, -7.62,  -8.16];
rowNames = {'SNR_Mean', 'SNR_Std', 'FWHM_Mean', 'FWHM_Std', 'SNR_Mean_Trunc', 'SNR_Std_Trunc', 'FWHM_Mean_Trunc', 'FWHM_Std_Trunc'};

numberOfSubjects = length(subjects);
TEs = {'6'; '8'; '11'; '15'; '20'; '30'; '50'; '80'; '150'};  %ms
numberOfTEs = length(TEs);

paths = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    paths{indexSubject} = [localFilePathBase prefixes{indexSubject} subjects{indexSubject} suffixFolder '\'];
end

SNR_temp = zeros(numberOfSubjects,numberOfTEs);FWHM_temp = zeros(numberOfSubjects,numberOfTEs);
SNR_temp_trunc = zeros(numberOfSubjects,numberOfTEs); FWHM_temp_trunc = zeros(numberOfSubjects,numberOfTEs);
for indexTE = 1:numberOfTEs
    for indexSubject = 1:numberOfSubjects
        fileToLoad = [paths{indexSubject} strcat('TE',num2str(TEs{indexTE}),'ms_SNR.mat')];
        load(fileToLoad)    %saved as SNR
        SNR_temp(indexSubject,:) = table2array(SNR(1,:));
        FWHM_temp(indexSubject,:) = table2array(SNR(2,:));
        SNR_temp_trunc(indexSubject,:) = table2array(SNR(3,:));
        FWHM_temp_trunc(indexSubject,:) = table2array(SNR(4,:));
    end
    SNR_Mean = nanmean(SNR_temp,1); SNR_Std = nanstd(SNR_temp,1);
    FWHM_Mean = nanmean(FWHM_temp,1); FWHM_Std = nanstd(FWHM_temp,1);
    SNR_Mean_trunc = nanmean(SNR_temp_trunc,1); SNR_Std_trunc = nanstd(SNR_temp_trunc,1);
    FWHM_Mean_trunc = nanmean(FWHM_temp_trunc,1); FWHM_Std_trunc = nanstd(FWHM_temp_trunc,1);
    Mean_Table = [SNR_Mean;SNR_Std;FWHM_Mean;FWHM_Std;SNR_Mean_trunc;SNR_Std_trunc;FWHM_Mean_trunc;FWHM_Std_trunc];
    tableT2toSave = array2table(Mean_Table,'RowNames',rowNames,'VariableNames',metaboliteNames)
    fileName = [localFilePathBase 'SNR_FWHM_Mean_Std_TE' TEs{indexTE} 'ms.mat'];
    save(fileName, 'tableT2toSave');
end


end