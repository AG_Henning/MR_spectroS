function this = ImportMruiText(this, filePath, fileName)

this.Parameter           = Parameter(); 
this.Parameter.Headers   = Headers();

this.Parameter.Filepath  = filePath;
this.Parameter.Filename  = fileName;

[data, fileNameNew, samplingInterval, transmitterFrequency, typeOfNucleus, nameOfPatient, ...
    signalNames] = importMrui(filePath, fileName);

pointsInDataset = size(data,1);
dataSetsInFile = size(data, 2);
newData = zeros(pointsInDataset,1,1,1,1,1,1,1,1,1,1,dataSetsInFile);
for dataSetIndex = 1:dataSetsInFile
    newData(:,1,1,1,1,1,1,1,1,1,1,dataSetIndex) = data(:, dataSetIndex);
end
this.Data{1} = newData;

this.Parameter.Headers.AcquisitionTime_ms = pointsInDataset * samplingInterval;
this.Parameter.Headers.Bandwidth_Hz = 1/samplingInterval * 1e3;
this.Parameter.Headers.DwellTimeSig_ns = samplingInterval * 1e6;
this.Parameter.Headers.NAveMeas = dataSetsInFile;
this.Parameter.Headers.PatientID = str2double(fileNameNew(1:4));
if isempty(nameOfPatient)
    this.Parameter.Headers.PatientName = this.Parameter.Headers.PatientID;
else
    this.Parameter.Headers.PatientName = nameOfPatient;
end
this.Parameter.Headers.ScanFrequency = transmitterFrequency;
this.Parameter.Headers.VectorSize = pointsInDataset;
this.Parameter.Headers.VectorSize_OS = pointsInDataset;
if (typeOfNucleus == 0)
    this.Parameter.Headers.Nucleus = '1H';
else
    this.Parameter.Headers.Nucleus = typeOfNucleus;
end
this.Parameter.Headers.SequenceDescription = signalNames;

this.Parameter.Headers.AfterHowManyAverages = 0;
this.Parameter.Headers.BirthDay = 0;
this.Parameter.Headers.FlipAngle_deg = [];
this.Parameter.Headers.InstitutionName = 'MPI';
this.Parameter.Headers.NRepMeas = 1;
this.Parameter.Headers.SamplesBeforeEcho = 1;
%randomly set values, which should not reflect the truth
this.Parameter.Headers.TE_us = 11111;
this.Parameter.Headers.TM_us = [];
this.Parameter.Headers.TR_us = 5555555;
this.Parameter.ReconFlags.isCoilsCombined = true;
%%
display('Import...FINISH.');

end