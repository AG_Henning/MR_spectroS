function [summedFid, summedFidWithNoise, current_em, currentConcentrationRm, stats] = createSummedSpectrum( ...
    fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, shift_t2_mx, phase_1_f2_mx, ppmMask, ...
    current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
    truncPoint, saveData, paramKeyWord, indexParam, dwellTimeMs, scanFrequency, dataExportPath, referencePeakPpm, scalingFactorRefPeak, baseline)

if ~exist('baseline','var')
    baseline = zeros(size(fids{1}));
end

summedFid = zeros(size(fids{1}));
current_em = size(1,length(metabolites));
currentConcentrationRm = size(1,length(metabolites));
for indexMetabolite = 1:length(metabolites)
    current_T2 = T2s(indexMetabolite) + current_em_std_factor(indexMetabolite) * T2_std(indexMetabolite);%in ms
    if strcmp(metabolites(indexMetabolite),'MMB')
        current_em(indexMetabolite) = 0;
        %the Gaussian for the acquired MMB should be smaller than the one for
        %metabolites, since it was already measured with the inherent
        %micro- and macro-susceptibility. It should however not be negative!
        if current_gm - 8 > 0
            gm = current_gm - 8;
        else
            gm = 0;
        end        
    else
        current_em(indexMetabolite) = 1 / (pi * current_T2 * 1e-3);%from ms to s and then Hz
        gm = current_gm; 
    end    
    %----- precalculate coefficients for the corrections and filters -----%
    % zero order phase correction
    % calculate zero order phase correction coefficient
    zero_order_phase_coeff = 1i*current_pc0*pi/180;
    
    % metabolite specific exp filter
    % calculate the metabolite specific exponential filter coefficient
    metabolite_filter_coeff = -current_em(indexMetabolite)*pi;

    % frequency shift in the F2
    % calculate the frequency shift in the F2 coefficient
    frequency_shift_F2_coeff = 1i*current_df2(indexMetabolite)*2*pi;
    
    % global exp filter
    % calculate the global exp filter coeff
    % global_exp_filter_coeff = -em_g(em_g_idx)*pi;    %ignored for the moment
    
    % gauss filter
    % calculate the gauss filter coefficients
    gauss_filter_coeff1 = gm*pi;
    gauss_filter_coeff2 = -(4*log(2));
    gauss_filter_coeff = gauss_filter_coeff1 ^2 /gauss_filter_coeff2;
    %--------------- end of coefficient precalculations ------------------%

    %------- apply the corrections and filters on the basis_fid ----------%
    % calculate the exponential cofficient matrix to apply on basis_fid
    % and apply the corrections and filters on the basis_fid
    filteredFid = fids{indexMetabolite} .* exp(...
        ... zero order phase correction ...
        zero_order_phase_coeff + ...
        ... metabolite specific exp filter ...
        metabolite_filter_coeff * abs(TE + shift_t2_mx) + ...
        ... frequency shift in the F2 ...
        frequency_shift_F2_coeff * shift_t2_mx + ...
        ... global exp filter ...
        ... global_exp_filter_coeff * abs(data.shifted_lb_mx) + ... %ignored for the moment
        ... gauss filter ...
        gauss_filter_coeff * (shift_t2_mx.^2));
    
        % save tilted version of basis spectrum     
	filteredSpec = fftshift(fft(filteredFid));

    % precompute coefficients needed to define the parameter unit as
    % degree /ppm
    coeff_F2_degree_ppm = 1i*current_pc1*pi/180;
    
    % try to define the paramter unit as degree / ppm
    filteredSpec = filteredSpec .* exp( ... 
        ... applying the F2 corrections ...
        coeff_F2_degree_ppm * phase_1_f2_mx);
    filteredFid = ifft(ifftshift(filteredSpec));
    
    if strcmp(metabolites(indexMetabolite),'MMB')
        relaxationCoeff = 1;
    else
        % Calculate the relaxation of the metabolite. Always the calculated T2
        % value is taken, such that concentration should not change for the simple fitting
        relaxationCoeff = exp(-TE/(T2s(indexMetabolite)*1e-3));
    end
    % calculate the concentrations with a desired standard deviation
    currentConcentration = concentrations(indexMetabolite) + conc_std_factor(indexMetabolite) * conc_std(indexMetabolite);
    if currentConcentration < 0
        currentConcentration = 0;
    end
    % adjust the concentration to the relaxation of the metabolite
    currentConcentrationRm(indexMetabolite) = currentConcentration .* relaxationCoeff;
    %% finally add the fid multiplied by the desired concentration to the final summed fid (spectrum)
    summedFid = summedFid + filteredFid .* currentConcentrationRm(indexMetabolite);
    
end

% the baselines were scaled down to the maximal peak being 1, hence we need
% to rescale the baseline; Use the NAA peak (cheating hard coded points!)
spectrum = fftshift(fft(summedFid));
% We use abs because of already added phase
maxValueSpectrum = max(abs(spectrum(1400:1600)));
baselineScaled = baseline * maxValueSpectrum;
summedFidWithNoise = summedFid + noiseVector + baselineScaled;
% figure; plot(real(fftshift(fft(summedFid)))); hold on; plot(real(fftshift(fft(baseline*maxValueSpectrum))))

bandwidth_Hz = 1 / dwellTimeMs * 1e3;
stats = calculateSNR(scanFrequency, bandwidth_Hz, summedFidWithNoise);

if saveData
    % export the spectra but also the used concentrations and all parameters
    % export not truncated data
    fileName = ['Simulated_', paramKeyWord, '_', num2str(indexParam) '_truncOff'];
    ExportLCModelBasis(summedFidWithNoise, dwellTimeMs, scanFrequency, dataExportPath, fileName, fileName, 'No', ...
        true, referencePeakPpm, scalingFactorRefPeak, 'No', TE*1e3, 'sLASER');
    save([dataExportPath, fileName, '.mat'], 'summedFidWithNoise', 'current_em', 'currentConcentrationRm', ...
        'current_pc0', 'current_pc1', 'current_df2', 'current_gm', 'current_em_std_factor', 'conc_std_factor', 'baselineScaled');
    % export truncated data
    summedFidWithNoise(:, truncPoint:end)=0;
    fileNameTrunc = ['Simulated_', paramKeyWord, '_', num2str(indexParam) '_truncOn'];
    ExportLCModelBasis(summedFidWithNoise, dwellTimeMs, scanFrequency, dataExportPath, fileNameTrunc, fileNameTrunc, 'No', ...
        true, referencePeakPpm, scalingFactorRefPeak, 'No', TE*1e3, 'sLASER');
    save([dataExportPath, fileName, '.mat'], 'summedFidWithNoise', 'current_em', 'currentConcentrationRm', ...
        'current_pc0', 'current_pc1', 'current_df2', 'current_gm', 'current_em_std_factor', 'conc_std_factor', 'baselineScaled');
end
end

function stats = calculateSNR(scanFrequency, bandwidth_Hz, fid)

noiseRangePpm = [-4, -1];
number_Pts = size(fid,2);

bw     = linspace(-bandwidth_Hz/2,bandwidth_Hz/2,number_Pts);
ppmVector    = bw/scanFrequency + 4.66;
   
indexNoiseStart = find(ppmVector>noiseRangePpm(1),1,'first');
indexNoiseEnd = find(ppmVector>noiseRangePpm(2),1,'first');
noiseRange = [indexNoiseStart indexNoiseEnd];
specReal = real(fftshift(fft(fid)));
%calculate noise
[noise_std] = noiseCalc(specReal, noiseRange);
    
list = {[1.9 2.1] 'NAA'; [2.9 3.1] 'Cr';[2.25 2.5] 'Glu'; [3.1 3.3] 'tCho'};
stats = cell(size(list,1)+1,1);
stats{1,1} = 'metaboliteName';
stats{1,2} = 'snr';
stats{1,3} = 'linewidth (Hz)';
stats{1,4} = 'amplitude';
for i=1:size(list,1)
    metRange = list{i,1};
    metRange = convertPPMtoIndex(ppmVector, metRange);
    metRange =[find(metRange==1,1,'first') find(metRange==1,1,'last')];
    
    [snrV, amplitude] = snr(specReal,metRange,noise_std);
    lineWidthValue = linewidth(specReal,metRange,bandwidth_Hz);
    %area          = sum(data(metRange(1):metRange(2)));
    stats{i+1,1} = list{i,2};
    stats{i+1,2} = snrV;
    stats{i+1,3} = lineWidthValue;
    stats{i+1,4} = amplitude;
    %stats{i,4} = area;
end
end

function [y] = convertPPMtoIndex(ppmVect, x)
%CONVERTPPMTOINDEX converts the ppm pair x to indexes 
%   Returns the vector of the size of the data points, 
%   with the selected region set to 1   
   index1 = ppmVect >= x(1);
   index2 = ppmVect <= x(2);
   y      = index1 & index2; 
end

function [noise_std] = noiseCalc(fftsReal, noiseRange)
% noise calculation
noise     = (fftsReal(noiseRange(1):noiseRange(2)))';
fitobject = fit((0:length(noise)-1)',noise,'poly1'); %fit a poly2 to noise
y         = feval(fitobject,(0:length(noise)-1)');   % substract poly1 from noise
noise     = noise - y;% now the noise does not have linear component
noise_std     = std(noise);
end

function [snrValue, amplitude] = snr(specReal,peakRange,noise_std)
amplitude = max(abs(specReal(peakRange(1):peakRange(2))));
snrValue  = amplitude/noise_std;
%snrValue  = 20*log(amplitude/noise); %in dB
end

function lineWidthValue = linewidth(specReal, metRange, bandwidth_Hz)
peakRange     = metRange;
peak          = specReal(peakRange(1):peakRange(2));

interpFactor  = 100; %
indeces       = [];
i = 1;
while (length(indeces)<2 && i <4)
    peakTmp         = interp(peak,interpFactor*i);
    [~,  maxPos] = max(abs(peakTmp));
    normValues   = peakTmp/(peakTmp(maxPos)/2);
    indeces      = find(normValues> 0.995 & normValues< 1.005);
    i            = i+1;
end

interpFactor    = interpFactor*(i-1);
freqResolution  = bandwidth_Hz/(length(specReal)*interpFactor);
try
    lineWidthValue  = (indeces(end) - indeces(1))*freqResolution;
catch err
    lineWidthValue = [];
end
end