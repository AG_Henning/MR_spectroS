classdef EddyCurrSettings 
 
    properties
        
       howManyAveragedWaterSignals 
       everyHowManyAcquisitions 
       correctedWater
       numberOfWaterSignals
       
    end
    
    methods
        
        function this= EddyCurrSettings()
            
            this.howManyAveragedWaterSignals   = 1;
            this.everyHowManyAcquisitions      = 1;
            this.correctedWater                = true;
            this.numberOfWaterSignals          = 0;
        end
        
        
        
    end
    
end

