function [ppm] = getPpmVector(this, zeroFillFactor)

if ~exist('zeroFillFactor','var')
    zeroFillFactor = 1;
end

txFreq = this.Parameter.Headers.ScanFrequency;
bw     = this.Parameter.Headers.Bandwidth_Hz;
bw     = linspace(-bw/2,bw/2,size(this.Data{1},this.kx_dim) * zeroFillFactor);
if strcmp(this.Parameter.Headers.Nucleus,'31P')
    ppm    = bw/(txFreq*1e-6);
else
    ppm    = bw/(txFreq*1e-6) + 4.66;
end

end