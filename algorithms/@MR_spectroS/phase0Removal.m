function this = phase0Removal(this)
%version 9 Oct 2014

if ~this.Parameter.ReconFlags.isSorted
    error('The data has to be sorted first')
end

if  this.Parameter.ReconFlags.isPhase0Removed
    warning('The data have been already phase0Removed')
end

oldData                = this.Data{1};
nCoils                 = size(this.Data{1}, this.coil_dim);
nAverages              = size(this.Data{1}, this. meas_dim);
nMixes                 = size(this.Data{1}, this.mix_dim);
newData                = zeros (size(oldData));

phase0 = zeros(nCoils, nAverages, nMixes);

for indexOfCoil =1:nCoils
    for indexOfAverage = 1:nAverages
        for indexOfMixes = 1:nMixes
         
        phase0(indexOfCoil, indexOfAverage, indexOfMixes)   = angle(oldData(1,:,:,indexOfCoil,:,:,:,:,max(nMixes),:,:,indexOfAverage));
        
        newData(:,:,:,indexOfCoil,:,:,:,:,indexOfMixes,:,:,indexOfAverage) =...
        oldData(:,:,:,indexOfCoil,:,:,:,:,indexOfMixes,:,:,indexOfAverage)* exp(-1i*phase0(indexOfCoil, indexOfAverage, indexOfMixes));
        
        end
    end
end


this.Parameter.Phase0RemovalSettings.phase0 = phase0;
this.Data{1}                                = newData;
this.Parameter.ReconFlags.isPhase0Removed   = true;

        
          
