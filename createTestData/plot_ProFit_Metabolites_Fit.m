function [concentrations, activeMetabolites, FQNs, FQNs2] = plot_ProFit_Metabolites_Fit(FileName, PathName, downField_MM_Met, saveFigures, displayBaseline)

if ~exist('FileName', 'var')
    [FileName,PathName] = uigetfile('*.mat','Please select .mat files from ProFit output','Multiselect','on');
end
%
if ~iscell(FileName)
    FileName= {FileName};
end

if ~exist('downField_MM_Met','var')
    downField_MM_Met = 'Met';
end
if ~exist('saveFigures','var')
    saveFigures = false;
end
if ~exist('displayBaseline','var')
    displayBaseline = false;
end

iterations = 4;

switch downField_MM_Met
    case 'Met moiety'
        metaboliteNames = {'Asp' 'Cr' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'Lac' 'mI' 'NAA_as' 'NAAG' 'Scyllo' 'Tau' 'tCho_P'};
        metaboliteLabels = {'Asp';'tCr';'GABA';'Glu';'Gln';'GSH';'Glyc';'Lac';'mI';'NAA'; 'NAAG';'Scyllo';'Tau';'tCho+';};
        xLimits = [0.5 4.097];
    case 'Met'
        metaboliteNames = {'NAA' 'NAAG' 'Asp' 'Glu' 'Gln' 'GABA' 'Lac' ...
            'Cr+PCr' 'Cho+GPC+PCh' 'mI' 'sI' 'Gly' 'Glc' 'GSH' 'PE' 'Tau'};
        
        metaboliteLabels = {'NAA' 'NAAG' 'Asp' 'Glu' 'Gln' 'GABA' 'Lac'  ...
            'tCr' 'tCho'  'mI' 'sI' 'Gly' 'Glc' 'GSH' 'PE' 'Tau'};
        
        metaboliteNames = {'Asp' 'Cr' 'PCr' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'Lac' 'mI' 'NAA_as' 'NAA_ac' 'NAAG' 'Scyllo' 'Tau' 'tCho' 'PE'};
        metaboliteLabels = {'Asp';'Cr';'PCr';'GABA';'Glu';'Gln';'GSH';'Glyc';'Lac';'mI';'NAA(CH_2)';'NAA(CH_3)';'NAAG';'Scyllo';'Tau';'tCho';'PE'};
        metaboliteNames = {'Asp' 'Cr' 'PCr' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'Lac' 'mI' 'NAA' 'NAAG' 'Scyllo' 'Tau' 'PCh' 'GPC' 'PE'};
        metaboliteLabels = {'Asp';'Cr';'PCr';'GABA';'Glu';'Gln';'GSH';'Glyc';'Lac';'mI';'NAA';'NAAG';'Scyllo';'Tau';'PCh';'GPC';'PE'};

        metaboliteNames = {'Asp'   'Cr'     'Cr_CH2'  'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'Lac' 'mI' 'NAA_as'   'NAA_ac'    'NAAG' 'Scyllo' 'Tau' 'tCho_P'};
        metaboliteLabels = {'Asp';'tCr(CH_3)';'tCr(CH_2)';'GABA';'Glu';'Gln';'GSH';'Gly';'Lac';'mI';'NAA(CH_2)';'NAA(CH_3)'; 'NAAG';'sI';'Tau';'tCho+';};

        %         %Braino
        %         metaboliteNames = {'Cr_CH3' 'Cr_CH2' 'Glu' 'Lac' 'mI' 'NAA_as' 'NAA_ac' 'Cho'};
        %         metaboliteLabels = {'Cr_CH3';'Cr_CH2';'Glu';'Lac';'mI';'NAA(CH_2)';'NAA(CH_3)';'Cho'};
        
        xLimits = [0.5 4.097];
        
    case 'DF'
        metaboliteNames = {'Asp';'Cr';'Cr_CH2';           'GABA';'Glu';'Gln';'GSH';'Glyc';'Lac';'mI';'NAA_ac';   'NAA_as';   'NAAG';'Scyllo';'Tau';'tCho_P';...
            'hCs';'hist';'ATP';'NAA_DF';'NAD';'DF58';'DF60';'DF68';'DF73';'DF75';'DF82';'DF83';'DF835';'NAAB'};
        metaboliteLabels = {'Asp';'tCr(CH_3)';'tCr(CH_2)';'GABA';'Glu';'Gln';'GSH';'Glyc';'Lac';'mI';'NAA(CH_3)';'NAA(CH_2)';'NAAG';'Scy';   'Tau';'tCho+';...
            'hCs';'hist';'ATP';'NAA_DF';'NAD';'DF58';'DF60';'DF68';'DF73';'DF75';'DF82';'DF83';'DF835';'NAAB'};
        xLimits = [0.5 9];
        metaboliteNames = {'hCs';};
        metaboliteLabels = {'hCs';};
        xLimits = [5.5 9];
        offsetMetabolite = 0.01;
    case '31P'
        
        metaboliteNames = {'PCr'  'ATP-g'     'ATP-a'     'ATP-b'     'GPC' 'GPE' 'Pi_in'    'Pi_ext'   'PC' 'PE' 'NADH' 'NAD+'  'UDPG'};
        metaboliteLabels = {'PCr' 'ATP-gamma' 'ATP-alpha' 'ATP-beta' 'GPC' 'GPE' 'Pi intra' 'Pi extra' 'PC' 'PE' 'NADH'  'NAD+' 'UDPG'};
        xLimits = [-20 10];
    otherwise
        error('Not known downField_MM_Met');
end

xLimitsText = xLimits(1) -0.03;
numberOfMet = length(metaboliteNames);

%plotting offsets
offsetMetabolite = 0.1;
offsetResidual = offsetMetabolite * (numberOfMet+1);
offsetBaseline = offsetResidual + offsetMetabolite;

FontSize = 14;
LineWidth = 1.5;
%
ppm = {};
pData = {};
fData = {};
bData = {};
mmData = {};
rData = {};
% close all;
concentrations = cell(length(FileName),length(iterations));
activeMetabolites = cell(length(FileName),length(iterations));
FQNs = cell(length(FileName),length(iterations));
FQNs2 = cell(length(FileName),length(iterations));

for iteration = iterations
    for index =1:length(FileName)
        figId = figure;
        load(strcat(PathName,FileName{index}),'fitresult', 'data');
        
        currentFitresult = fitresult{1,iteration};
        % get mean values for pc0, pc1 and df1, df2
        % values which should be applied on spectra and not the basis set
        pc0 = zeros(1, currentFitresult.nr_act_mets);
        pc12 = zeros(1, currentFitresult.nr_act_mets);
        df2 = zeros(1, currentFitresult.nr_act_mets);
        for met_cnt=1:currentFitresult.nr_act_mets
           pc0(met_cnt) = currentFitresult.fitted_values.pc0(currentFitresult.sub_mask.pc0.full(:,met_cnt));
           pc12(met_cnt) = currentFitresult.fitted_values.pc12(currentFitresult.sub_mask.pc12.full(:,met_cnt));
           df2(met_cnt) = currentFitresult.fitted_values.df2(currentFitresult.sub_mask.df2.full(:,met_cnt));
        end
        %ppm axis
        ppm{index} = currentFitresult.common_data.data.fitroi.ppm2;
        
        %phaseData
        pData{index} = currentFitresult.processed_spec;
        %fit data
        fData{index} = currentFitresult.fitted_spec;
        %residual
        rData{index} = currentFitresult.residual_spec;
        %spline baseline
        if ~isempty(currentFitresult.splines_baseline)
            spBB = currentFitresult.splines_baseline.basis_matrix;
            concSpline = currentFitresult.splines_baseline.coeff;
            bData{index} = (spBB * concSpline(1:size(spBB,2)))';
        else
            bData{index} = zeros(size(currentFitresult.residual_spec));
        end
        %Macromolecular baseline
        if strcmp(downField_MM_Met,'Met') || strcmp(downField_MM_Met,'Met moiety')
            indexMMB   = find(strcmp(currentFitresult.met(currentFitresult.active_mets), 'Leu'));
            if ~isempty(indexMMB)
                MMB_available = true;
                fit_spec_metabolite = ...
                    reshape(currentFitresult.basis_matrix(:,currentFitresult.sub_mask.conc.full(:,indexMMB)),...
                    [currentFitresult.common_data.data.fitroi.pts(1) ...
                    currentFitresult.common_data.data.fitroi.pts(2)]);
                fit_conc_metabolite = currentFitresult.fitted_values.conc(currentFitresult.sub_mask.conc.full(:,indexMMB)');
                mmData{index}= fit_spec_metabolite * fit_conc_metabolite;
            else
                MMB_available = false;
                mmData{index}= zeros(size(bData));
            end
        else
            MMB_available = false;
        end
        
        %scaling calculation
        scale = real(max(pData{index}));
        
        %plotting
        hold on
        if displayBaseline
            if MMB_available
                p = plot(ppm{index}, pData{index} ./ scale, ...
                    ppm{index},fData{index} ./ scale, ...
                    ppm{index}, mmData{index} ./ scale - 0.03, ...
                    ppm{index},bData{index} ./ scale - offsetBaseline, ...
                    ppm{index},rData{index} ./ scale - offsetResidual);
                text(xLimitsText,-0.03,'MM spectrum', 'FontSize', FontSize);
            else
                p = plot(ppm{index}, pData{index} ./ scale, ...
                    ppm{index},fData{index} ./ scale, ...
                    ppm{index},bData{index} ./ scale - offsetBaseline, ...
                    ppm{index},rData{index} ./ scale - offsetResidual);
            end
        else
            if MMB_available
                p = plot(ppm{index}, (pData{index} - bData{index}) ./ scale, ...
                    ppm{index},(fData{index} - bData{index}) ./ scale, ...
                    ppm{index}, mmData{index} ./ scale - 0.03, ...
                    ppm{index},bData{index} ./ scale - offsetBaseline, ...
                    ppm{index},rData{index} ./ scale - offsetResidual);
                text(xLimitsText,-0.03,'MM spectrum', 'FontSize', FontSize);
            else
                p = plot(ppm{index}, (pData{index} - bData{index}) ./ scale, ...
                    ppm{index},(fData{index} - bData{index}) ./ scale, ...
                    ppm{index},bData{index} ./ scale - offsetBaseline, ...
                    ppm{index},rData{index} ./ scale - offsetResidual);
            end
        end
        
        text(xLimitsText,0.05,'Data + Fit', 'FontSize', FontSize);
        text(xLimitsText,-offsetResidual,'Residual', 'FontSize', FontSize);
        text(xLimitsText,-offsetBaseline,'Baseline', 'FontSize', FontSize);
        for plots = 1:length(p)
            set(p(plots),'LineWidth',LineWidth);
        end
        
        for indexMetabolite = 1:length(metaboliteNames)
            %evaluate each metabolite
            indexMet   = find(strcmp(currentFitresult.met(currentFitresult.active_mets), metaboliteNames{indexMetabolite}));
            
            fit_spec_metabolite = ...
                reshape(currentFitresult.basis_matrix(:,currentFitresult.sub_mask.conc.full(:,indexMet)),...
                [currentFitresult.common_data.data.fitroi.pts(1) ...
                currentFitresult.common_data.data.fitroi.pts(2)]);
            fit_conc_metabolite = currentFitresult.fitted_values.conc(currentFitresult.sub_mask.conc.full(:,indexMet)');
            metaboliteSpectrum = fit_spec_metabolite * fit_conc_metabolite;
            
            pMetabolite = plot(ppm{index}, metaboliteSpectrum ./ scale - indexMetabolite * offsetMetabolite);
            text(xLimitsText, indexMetabolite * -offsetMetabolite,metaboliteLabels{indexMetabolite}, 'FontSize', FontSize);
            set(pMetabolite,'LineWidth',LineWidth);
        end
        
        deltaChar = char(948);
        
        xlim(xLimits);
        xlabel([deltaChar ' [ppm]']);
        ylim([-offsetBaseline-offsetMetabolite*2 1.1]);
        %     ylim([-0.08 0.1]);
        set(gca,'xDir','reverse')
        set(gca,'ytick',[]);
        
        title(FileName{index})
        set(gca,'fontsize',FontSize);
        
        if saveFigures
            set(figId, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);  
            set(gca, 'Position', [0.13,0.11,0.75,0.815])
%             set(gca, 'Position', [0.13,0.11,0.75,0.88])
            saveas(figId, [PathName,FileName{index}(1:end-4), '_iter', num2str(iteration), '.tif']);
            savefig(figId, [PathName,FileName{index}(1:end-4), '_iter', num2str(iteration), '.fig'],'compact');
        end
        [concentrations{index,iteration}, activeMetabolites{index,iteration}] = saveFitResultsTable(currentFitresult, saveFigures, PathName, FileName{index}, iteration);
        
        FQN2 = calculate_fit_quality_number(currentFitresult.residual_spec, currentFitresult.common_data.data.fitroi.ppm2, ...
            currentFitresult.common_data.spec, currentFitresult.common_data.data.ppm2, [1.95 4.0]);
        FQNs{index,iteration} = currentFitresult.fit_quality_number;
        FQNs2{index,iteration} = FQN2;
    end
end
end

function [concentrations, active_met_names] = saveFitResultsTable(currentFitresult, saveFigures, PathName, FileName, iteration)
figId2=figure;
set(figId2,...
    'Position',[350 400 890 600],'Color',[1 1 1]);
crlb_print =currentFitresult.crlb;
crlb_print(abs(crlb_print)>=1000) = 999.9999;
active_met_names = currentFitresult.met(currentFitresult.active_mets);
dataOfTable={};
columnname =  {'met','conc', '/Cr', '<html>crlb<br />[%]</html>', ...
    '<html>T2<br />[ms]</html>','<html>em<br />[Hz]</html>',...
    '<html>gm<br />[Hz]</html>', ...
    '<html>pc0<br />[deg]</html>', '<html>df2<br />[Hz]</html>', ...
    '<html>pc12<br />[deg/ppm]</html>'};
columnformat={'char',[],[],[],[],[],[],[],[],[],[]};
concentrations = zeros(1,currentFitresult.nr_act_mets);
creatineConc = currentFitresult.GetFittedValues({'Cr'},{'conc'});

boundsTable = zeros(currentFitresult.nr_act_mets,10);
for met_cnt=1:currentFitresult.nr_act_mets
    dataOfTable=[dataOfTable;{...
        active_met_names{met_cnt},...
        num2str(currentFitresult.fitted_values.conc(...
        currentFitresult.sub_mask.conc.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.conc(...
        currentFitresult.sub_mask.conc.full(:,met_cnt)')/...
        creatineConc,'%6.4f'),...
        num2str(crlb_print(...
        currentFitresult.sub_mask.conc.full(:,met_cnt)'),'%6.4f'),...
        num2str(1/(pi*currentFitresult.fitted_values.em(...
        currentFitresult.sub_mask.em.full(:,met_cnt)'))*1e3,'%6.4f'),...
        num2str(currentFitresult.fitted_values.em(...
        currentFitresult.sub_mask.em.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.gm(...
        currentFitresult.sub_mask.gm.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.pc0(...
        currentFitresult.sub_mask.pc0.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.df2(...
        currentFitresult.sub_mask.df2.full(:,met_cnt)'),'%6.4f'),...
        num2str(currentFitresult.fitted_values.pc12(...
        currentFitresult.sub_mask.pc12.full(:,met_cnt)'),'%6.4f')...
        }];
    concentrations(met_cnt) = currentFitresult.fitted_values.conc(currentFitresult.sub_mask.conc.full(:,met_cnt)');
    boundsTable(met_cnt,:) = [...
        0,...
        checkBounds(currentFitresult,'conc', met_cnt),...
        0,... %conc/Cr
        0,... %crlb
        0,... % T2
        checkBounds(currentFitresult,'em', met_cnt),...
        checkBounds(currentFitresult,'gm', met_cnt),...
        checkBounds(currentFitresult,'pc0', met_cnt),...
        checkBounds(currentFitresult,'df2', met_cnt),...
        checkBounds(currentFitresult,'pc12', met_cnt),...
        ];
end
%reached upper bounds- blue
idx = boundsTable == 1;
dataOfTable(idx) = strcat(...
    '<html><span style="color: #0000FF; font-weight: bold;">', ...
    dataOfTable(idx), ...
    '</span></html>');
%reached lower bounds - red
idx = boundsTable == -1;
dataOfTable(idx) = strcat(...
    '<html><span style="color: #FF0000; font-weight: bold;">', ...
    dataOfTable(idx), ...
    '</span></html>');
%reached both bounds - green
idx = isnan(boundsTable);
dataOfTable(idx) = strcat(...
    '<html><span style="color: #00FF00; font-weight: bold;">', ...
    dataOfTable(idx), ...
    '</span></html>');

tableProfit = uitable(...%'Units','normalized',...%
    'Position',[20 20 850 580],'Data', dataOfTable,...
    'ColumnName', columnname,'ColumnFormat',columnformat,...
    'RowName',[],'ColumnWidth',{65});

if saveFigures
    saveas(figId2, [PathName,FileName(1:end-4), '_iter', num2str(iteration), '_table.tif']);
    savefig(figId2, [PathName,FileName(1:end-4), '_iter', num2str(iteration), '_table.fig'],'compact');
end
end

function bounds = checkBounds(currentFitresult,param, met_cnt)
    % bounds = 0   : no bounds reached
    % bounds = 1   : upper bounds reached
    % bounds = -1  : lower bounds reached
    % bounds = NaN : both bounds reached
    bounds = 0;
    factorBounds = 1.1; %within 10% from bound
    sub_mask = eval(['currentFitresult.sub_mask.', param, '.full(:,met_cnt)'])';
    preprocessed_value = 0;
    try %should work for pc0, pc12, df2 (df2_global is not a parameter of interest)
        preprocessed_value = eval(['currentFitresult.preprocessedValues.' param]);
    catch
        %nothing
    end
    fitted_value = eval(['currentFitresult.fitted_values.', param])-preprocessed_value;
    if (eval(['currentFitresult.full_upper_bounds.', param,'(sub_mask)']) < ...
            (fitted_value(sub_mask) * factorBounds))
        bounds = 1;
    end
    if (eval(['currentFitresult.full_lower_bounds.', param,'(sub_mask)']) > ...
            (fitted_value(sub_mask) / factorBounds))
        if bounds == 0
            bounds = -1; %reached only lower bounds
        else
            bounds = NaN; %reached both bounds
        end
    end
end
