
function [figureID] = plotConcentrations(concentrations, metaboliteNames, eliminateMetabolites, doMolal_Molar, plotColor, offsetXAxis, figureID)
if ~exist('offsetXAxis','var')
    offsetXAxis = 0;
end
microSign = [char(181) ];
numberOfMet = length(metaboliteNames);
updatedMetaboliteNames = cell(1,numberOfMet);
updatedConcentrations = zeros(size(concentrations));
updatedIndex = 0;
for indexMetabolite = 1:numberOfMet
    indexTable = find(strcmp(eliminateMetabolites(:), metaboliteNames(indexMetabolite)));
    if isempty(indexTable)
        updatedIndex = updatedIndex + 1;
        updatedMetaboliteNames{updatedIndex} = metaboliteNames{indexMetabolite};
        updatedConcentrations(:, updatedIndex) = concentrations(:, indexMetabolite);
    end
end
updatedMetaboliteNames = updatedMetaboliteNames(1:updatedIndex);
updatedConcentrations = updatedConcentrations(:, 1:updatedIndex);
updatedNumberOfMet = length(updatedMetaboliteNames);

if exist('figureID','var')
    hold on
    figure(figureID)
else
    figureID = figure;
end
positions1 = 1.5+offsetXAxis:updatedNumberOfMet+0.5+offsetXAxis;
boxPlotT2 = boxplot(updatedConcentrations, updatedMetaboliteNames,'colors',plotColor,'symbol','+','positions',positions1,'width',0.25);

if strcmp(doMolal_Molar,'Molar')
    ylabel('Concentrations (mmol/L)')
else
%     ylabel(['Concentrations (' microSign 'mol/g')])
    ylabel('Concentrations (mmol/kg)')
end
axT2 = get(figureID,'CurrentAxes');
axT2.XAxis.TickLength = [0,0];
axT2.YColor = 'k';

set(boxPlotT2(:,:),'linewidth',1);
%     set(gca, 'YLim', [-5,140]);

set(gca, 'YGrid', 'on');
% set(gca, 'YTickMode', 'manual');
% set(gca, 'YTickLabel', [0.1, 1, 10, 100]);
set(gca, 'XTickMode', 'manual');
set(gca, 'XTick', [1.5:1:updatedNumberOfMet+0.5]);
set(gca, 'XTickLabelRotation', 90.0);
set(gca, 'FontSize', 16);
title('Absolute Concentrations');
end
