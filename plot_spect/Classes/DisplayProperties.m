classdef DisplayProperties < hgsetget
    
    properties
        
        nCoils
        nTimepoints
        nDyns
        nAverages
        nAverages2
        nMixes
        acquisitionTime
        bandwidth
        zeroPoint
        isDataRead
        filename
        xRange
        yRange
        dimRangeLock
        plotOnOff
        secondAverage
    end
   
    
    methods
        
        function this= DisplayProperties()
        
        this.nCoils          =   0;
        this.nTimepoints     =   0;
        this.nDyns           =   0;
        this.nAverages       =   0;
        this.nAverages2      =   0;
        this.nMixes          =   0;
        this.acquisitionTime =   0;
        this.bandwidth       =   0;
        this.zeroPoint       =   4.7;
        this.isDataRead      =  false;
        this.filename        =  '';
        this.xRange          =  [];
        this.yRange          =  [];
        this.dimRangeLock    =  false;
        this.plotOnOff       =  false;
        this.secondAverage   =  [];
        
        end
        
        function setDisplayProperties(this,handles)
           
           MR_SpectroObj         = handles.MRSpectroMatrix.getCurrent; 
           this.nTimepoints      = size(MR_SpectroObj.Data{1},MR_SpectroObj.kx_dim);
           this.nCoils           = size(MR_SpectroObj.Data{1},MR_SpectroObj.coil_dim);
           this.nDyns            = size(MR_SpectroObj.Data{1},MR_SpectroObj.dyn_dim);
           this.nAverages        = size(MR_SpectroObj.Data{1},MR_SpectroObj.meas_dim);
           this.nMixes           = size(MR_SpectroObj.Data{1},MR_SpectroObj.mix_dim);
           this.isDataRead       = true;
           this.filename         =  MR_SpectroObj.Parameter.Filename;
           if this.nMixes>1
                this.nAverages2=size(MR_SpectroObj.Data{1},MR_SpectroObj.meas_dim);
           end
           
           
           
           if MR_SpectroObj.Parameter.ReconFlags.isZeroFilled
               
               this.acquisitionTime= MR_SpectroObj.Parameter.Headers.AcquisitionTime_ms*...
                             MR_SpectroObj.Parameter.ZeroFillSettings.ZerroFillFactor;
           else
        
               this.acquisitionTime= MR_SpectroObj.Parameter.Headers.AcquisitionTime_ms;
        
           end
           
          this.bandwidth= MR_SpectroObj.Parameter.Headers.Bandwidth_Hz;
         
        updateDisplayProperties(handles);  
        end% function
        
        
        
        
        function  this= setProperties(this,figureHandles)
            
            %//////////////////////////////////////////////////////////////
            %                 TEXT PROPERTIES
            %//////////////////////////////////////////////////////////////
            
            
            set(figureHandles.text_points,'String', sprintf('#Timepoints: %d',this.nTimepoints));
            set(figureHandles.text_coils,'String',  sprintf('#Coils: %d',this.nCoils)); 
            set(figureHandles.text_dyns,'String',   sprintf('#Dynamics: %d',this.nDyns));
            set(figureHandles.text_means,'String',  sprintf('#Averages: %d',this.nAverages));
            set(figureHandles.text_mixes,'String',  sprintf('#Mixes: %d',this.nMixes));
            
            if this.nMixes>1
                
               set(figureHandles.text_means2,'String', sprintf('#Averages2: %d', this.nAverages2));
               set(figureHandles.text_means2,'Visible','on');
       
               for i=1:this.nMixes
                   s{i}=num2str(i);
               end
               set(figureHandles.popupmenu_mix,'String',s)
               
           else
               set(figureHandles.popupmenu_mix,'Enable','off');

            end
           
            %//////////////////////////////////////////////////////////////
            %                 SLIDERS PROPERTIES
            %//////////////////////////////////////////////////////////////
            
            set(figureHandles.slider_coils,'Max',this.nCoils);
            
            if this.nCoils~=1
                
               set(figureHandles.slider_coils,'SliderStep',[1 1]/(this.nCoils-1));
            else
               
               set(figureHandles.slider_coils,'Enable','off');
               set(figureHandles.slider_coils,'Min',0);
           end

           set(figureHandles.slider_dyns,'Max',this.nDyns);
           if this.nDyns~=1
               
               set(figureHandles.slider_dyns,'SliderStep',[1 1]/(this.nDyns-1));
           else
                   
               set(figureHandles.slider_dyns,'Enable','off');
               set(figureHandles.slider_dyns,'Min',0);
           end

           set(figureHandles.slider_means,'Max',this.nAverages);
           if this.nAverages~=1
               
               set(figureHandles.slider_means,'SliderStep',[1 1]/(this.nAverages-1));
               else
               set(figureHandles.slider_means,'Enable','off');
               set(figureHandles.slider_means,'Min',0);
           end
            
            
            
            
        end
            
            
        
    end
    
end

