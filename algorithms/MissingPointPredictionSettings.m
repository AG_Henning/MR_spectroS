classdef MissingPointPredictionSettings 
% version: 21 Aug 2018


    properties
        imiss
        T_factor
        polyOrder
        
    end
    
    methods
        
        function this=MissingPointPredictionSettings
            this.imiss=4;
            this.T_factor = 2;
            this.polyOrder = 60;
            
        end
    end
    
end

