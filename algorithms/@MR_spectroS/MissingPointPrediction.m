%% Missing point prediction (integer phase 1)
function this = MissingPointPrediction( this )

newData = this.Data{1};

imiss = this.Parameter.MissingPointPredictionSettings.imiss;
T_factor = this.Parameter.MissingPointPredictionSettings.T_factor;
polyOrder = this.Parameter.MissingPointPredictionSettings.polyOrder;


missingPointPredArrray    = MR_spectroS.missingPointPredictionArray(newData, imiss, T_factor, polyOrder);

this.Data{1}                                = missingPointPredArrray;

end