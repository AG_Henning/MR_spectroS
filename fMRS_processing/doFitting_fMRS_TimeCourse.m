function doFitting_fMRS_TimeCourse()
clear; clc; close all;

allVisTypes = {'Stim', 'Rest'};      %'Stim', 'Rest'

for i = 1:length(allVisTypes)
pathName = 'fMRS TimeCourse data path';
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
visualType = allVisTypes{i};      %'Stim', 'Rest'
BOLDType = '_woBOLDCorr';  %only _woBOLDCorr reasonable since block wise fitting for metabolite time courses
MMBType = '_MMBSummed_v2';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated
%     nBlocks = 5;
nBlocks = 10;

BlockNo = cell(1,nBlocks);
for k = 1:nBlocks
    BlockNo{k} = strcat('_Block', num2str(k));
end

% subjects = {'2823';};
subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
    '4085';'3333';'4012';'4085';'6524'};
% prefixes = {'2020-02-18_';};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
    '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
    '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
suffixFolder = '_fMRS';
excludeSubjects = {'5166';'9810';'5269'};

extensionTable = '.table';
extensionCoord = '.coord';
defaultVisual = 'Stim';
defaultSubject = 'XXXX';
defaultSubjectsPath = 'YYYY';
if ~isempty(strfind(MMBType,'_v2'))
    defaultMMB = '_MMBSummed_v2';
else
    defaultMMB = '_MMBSummed';
end
defaultLCModelUser = 'jdorst';
outputFileNameBaseWithoutMetabolite_OFF = strcat(defaultSubject, '_StimOFF_woBOLDCorr128');
outputFileNameBaseWithoutMetabolite_ON  = strcat(defaultSubject, '_StimON_woBOLDCorr128');

if ~isempty(strfind(MMBType,'_v2'))
    controlFilesBase = 'fitsettings_v2_XXXX_';
else
    controlFilesBase = 'fitsettings_XXXX_';
end
controlFilesBaseSuffix = '.control';
outputFileNameBase = strcat(defaultSubject, '_');

%% file paths setup
controlFilesPathRemote = '/Desktop/1H_fMRS/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/1H_fMRS/Output/';
if ~isempty(strfind(MMBType,'_v2'))
    LCModelOutputPath = [localFilePathBase, 'Output_v2_MMBSummed/'];
else
    LCModelOutputPath = [localFilePathBase, 'Output/'];
end
defaultControlFile = strcat(controlFilesBase, defaultVisual, controlFilesBaseSuffix);

subjectsPath = strcat(prefixes, subjects, suffixFolder);

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath, '/');
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath, '/');
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath, '/');
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath, '/');
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath, '/');


%% process fMRS data and save .RAW file
dataPath = strcat(localFilePathBase,subjectsPath, '/');
% for i = 1:length(dataPath)
%     currentFolder = num2str(cell2mat(dataPath(i)));
%     cd (currentFolder);
%     subFolderName = dir(dataPath{i});
%     if strcmp(visualType,'Stim')
%         dataFile = dir('*Stim*.dat');
%     elseif strcmp(visualType,'Rest')
%         dataFile = dir('*Rest*.dat');
%     else
%         error('visualType needs to be Stim or Rest');
%     end
%     PCSteps = 16;
%     reconstruct_1H_fMRS_TimeCourse(currentFolder,dataFile.name,1,1,nBlocks,false,PCSteps,[],1,visualType); %currentFolder, fid_id, isMC, isInVivo, nBlocks, nOnAverages, PCSteps, weights, saveResults
%     close all; cd ..;
% end

%% basic configurations
LCModelOutputFiles = strcat(outputFileNameBase, visualType, BOLDType, MMBType, BlockNo);
LCModelOutputFileswoBaseline = strcat(outputFileNameBase, visualType, BOLDType, MMBType, BlockNo, '_woBaseline');
LCModelControlFiles = strcat(controlFilesBase, visualType, BOLDType, MMBType, BlockNo, controlFilesBaseSuffix);
LCModelControlFileswoBaseline = strcat(controlFilesBase, visualType, BOLDType, MMBType, BlockNo, '_woBaseline', controlFilesBaseSuffix);
defaultWaterRef = strcat(outputFileNameBase, 'WRef');

numberOfSubjects = length(subjects);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

do the actual LCModel fitting
parfor indexCurrentSubject = 1:numberOfSubjects
    %% do the LCModel fitting
    LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
    
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    
    currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
    currentControlFiles = strrep(currentControlFiles,'fitsettings_v2_','fitsettings_');
    currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
    waterRefFileName = strrep(defaultWaterRef, defaultSubject, subjects{indexCurrentSubject});
    
    %% create the control file series
    for nBl = 1:nBlocks
        currentOutputFile = currentOutputFiles{nBl};
        currentFMRS = strcat(visualType,BOLDType);
%         currentLCModelOutputFile = LCModelOutputFiles{nBl};
        createLCModelConfigfMRSTimeCourse(controlFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, defaultControlFile, ...
            currentFMRS, defaultVisual, currentOutputFile,...
            waterRefFileName, defaultWaterRef, subjectsPath{indexCurrentSubject}, defaultSubjectsPath, subjects{indexCurrentSubject},...
            defaultSubject, MMBType, defaultMMB, BlockNo{nBl});
    end
    
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
end
% 
% % Create and save table with baseline concentrations fitted with baseline, taken from second block of first REST
% if exist('excludeSubjects')
%     createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
%         LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,0,excludeSubjects)
% else
%     createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
%         LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,0)
% end
% 
% % Create and save table from fitted concentrations with baseline, take from blocks 3-10
% createConcTableBlock(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
%     LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,0)

%% subtract baseline from difference spectrum and fit again
% only useful for MMBSummed
% if strcmp(MMBType(1:10),'_MMBSummed')
% %     LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
%     parfor indexCurrentSubject = 1:numberOfSubjects
%         %% do the LCModel fitting
%             LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
%         
%         LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
%             subjectsLCModelOutputPath{indexCurrentSubject});
%         
%         currentControlFileswoBaseline = strrep(LCModelControlFileswoBaseline, defaultSubject, subjects{indexCurrentSubject});
%         currentControlFileswoBaseline = strrep(currentControlFileswoBaseline,'fitsettings_v2_','fitsettings_');
%         if ~isempty(strfind(MMBType,'_v2'))
%             currentDataFiles = strrep(strcat(outputFileNameBase, visualType, BOLDType, '_v2', BlockNo), defaultSubject, subjects{indexCurrentSubject});
%         else
%             currentDataFiles = strrep(strcat(outputFileNameBase, visualType, BOLDType, BlockNo), defaultSubject, subjects{indexCurrentSubject});
%         end
%         currentOutputFileswoBaseline = strrep(LCModelOutputFileswoBaseline, defaultSubject, subjects{indexCurrentSubject});
%         currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
%         waterRefFileName = strrep(defaultWaterRef, defaultSubject, subjects{indexCurrentSubject});
%         currentDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject}, '/');
%         for nBl = 1:nBlocks
%             %check save option for .mat file in subtractBaseline_fMRS
%             subtractBaseline_fMRS(currentDataPath,subjectsLCModelOutputPath{indexCurrentSubject},currentDataFiles{nBl},currentOutputFileswoBaseline{nBl},'NAA')
%             currentFMRS = strcat(visualType,BOLDType);
%             createLCModelConfigfMRSTimeCourse(controlFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, defaultControlFile, ...
%                 currentFMRS, defaultVisual, currentOutputFileswoBaseline{nBl},...
%                 waterRefFileName, defaultWaterRef, subjectsPath{indexCurrentSubject}, defaultSubjectsPath, subjects{indexCurrentSubject},...
%                 defaultSubject, MMBType, defaultMMB, strcat(BlockNo{nBl}, '_woBaseline'));
%         end
%         fittingLCModel(LCModelCallerInstance, currentControlFileswoBaseline, currentOutputFileswoBaseline, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
%             subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
%         
%     end
%     
% % Create and save table with baseline concentrations fitted w/o baseline, taken from second block of first REST
% if exist('excludeSubjects')
%     createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
%     LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,1,excludeSubjects)
% else
%     createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
%     LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,1)
% end
% 
% % Create and save table from fitted concentrations w/o baseline, taken from blocks 3-10
% createConcTableBlock(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
%     LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,1)
% end


%% create concentration time course from fit with and w/o Baseline
ExpName = strcat('BlockConc_', num2str(visualType), BOLDType, MMBType);
if exist('excludeSubjects')
    createConcTimeCourseFig(ExpName,LCModelOutputPath,subjects,nBlocks,'Lac',0,excludeSubjects)
    createConcTimeCourseFig(ExpName,LCModelOutputPath,subjects,nBlocks,'Glu',0,excludeSubjects)
    createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjects,nBlocks,'Lac',0,excludeSubjects)
    createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjects,nBlocks,'Glu',0,excludeSubjects)
    if strcmp(MMBType(1:10),'_MMBSummed')
        createConcTimeCourseFig(ExpName,LCModelOutputPath,subjects,nBlocks,'Lac',1,excludeSubjects)
        createConcTimeCourseFig(ExpName,LCModelOutputPath,subjects,nBlocks,'Glu',1,excludeSubjects)
        createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjects,nBlocks,'Lac',1,excludeSubjects)
        createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjects,nBlocks,'Glu',1,excludeSubjects)
    end
else
    createConcTimeCourseFig(ExpName,LCModelOutputPath,subjects,nBlocks,'Lac',0)
    createConcTimeCourseFig(ExpName,LCModelOutputPath,subjects,nBlocks,'Glu',0)
    createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjects,nBlocks,'Lac',0)
    createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjects,nBlocks,'Glu',0)
    if strcmp(MMBType(1:10),'_MMBSummed')
        createConcTimeCourseFig(ExpName,LCModelOutputPath,subjects,nBlocks,'Lac',1)
        createConcTimeCourseFig(ExpName,LCModelOutputPath,subjects,nBlocks,'Glu',1)
        createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjects,nBlocks,'Lac',1)
        createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjects,nBlocks,'Glu',1)
    end
end
close all

end
end

function createConcTimeCourseFigPercChange(ExpName,LCModelOutputPath,subjects,nBlocks,metabToFig,woBaseline,excludeSubjects)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
ExpName = strcat(ExpName, woBaseline);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
dataTimeCourse = load(exportFileMat);
fields = fieldnames(dataTimeCourse);
dataTimeCourse = dataTimeCourse.(fields{1});
metaboliteNames = (dataTimeCourse(1,:));
indexMet = strfind(metaboliteNames,metabToFig);
idxMet = find(not(cellfun('isempty',indexMet)));

if exist('excludeSubjects')
    n = length(dataTimeCourse);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(dataTimeCourse(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    dataTimeCourse([excludeIdx],:) = [];
end

%Baseline concentration, equals 2nd block (second 64 averages of first
%REST)
nsteps = 1;
for i = 3:floor(length(dataTimeCourse)/10):length(dataTimeCourse)
    baselineConcIdxMet(nsteps,1) = dataTimeCourse{i,idxMet};
    nsteps = nsteps + 1;
end

[n,~] = size(dataTimeCourse);
for k = 2:n
    MetConc(k-1) = dataTimeCourse(k,idxMet(1));
end

%calculate concentration change of data points with reference to baseline
[~,b] = size(MetConc);
for av = 1:b-4
    PercChange(av) = (MetConc{1,av}-baselineConcIdxMet(ceil(av/10)))/baselineConcIdxMet(ceil(av/10))*100;
end

% figure
% x = linspace(320/60/4,(320*5/60-320/60/4),10);
% hold on
% [ss,~] = size(dataTimeCourse);
% for idxSubject = 1:floor(ss/nBlocks)
%     if metabToFig == 'Lac'
%         plot(x,cell2mat(dataTimeCourse(((idxSubject-1)*10+2):((idxSubject-1)*10+11),idxMet(1)))+(idxSubject-1)*1.5,'d-')
%     elseif metabToFig == 'Glu'
%         plot(x,cell2mat(dataTimeCourse(((idxSubject-1)*10+2):((idxSubject-1)*10+11),idxMet(1)))+(idxSubject-1)*5,'d-')
%     else
%         error('can only plot Lac and Glu so far');
%     end
% end
% xlabel 't / min'; ylabel 'Concentration Change / %'
% if exist('excludeSubjects')
%     if ~isempty(strfind(ExpName,'Rest'))
%         title ([metabToFig,' time course, subjects excluded, Rest'])
%     else
%         title ([metabToFig,' time course, subjects excluded, Stim'])
%     end
% else
%     if ~isempty(strfind(ExpName,'Rest'))
%         title ([metabToFig,' time course, Rest'])
%     else
%         title ([metabToFig,' time course, Stim'])
%     end
% end
% 
% yLimits = get(gca,'YLim');
% boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
% for i = 1:nBlocks/2
%     boxoff  = [boxoff 320/60*i 320/60*i];
%     if i < nBlocks/2
%         boxon   = [boxon 320/60*i 320/60*i];
%     end
% end
% for ii = 1:nBlocks/4
%     boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%     boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
% end
% patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
% patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
% ylim([yLimits(1) yLimits(2)]);
% xlim([0 320*5/60]); set(gca, 'FontWeight','bold');
% set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 12, 20]);
% PathName = strcat(LCModelOutputPath,'BlockConcTimeCourse/');
% if exist('excludeSubjects')
%     fig2save = fullfile(PathName, strcat(ExpName, '_', metabToFig, '_Excluded_PercChange.fig'));
% else
%     fig2save = fullfile(PathName, strcat(ExpName, '_', metabToFig, '_PercChange.fig'));
% end
% savefig(fig2save)


% PercChange_temp = cell2mat(dataTimeCourse(2:end-4,2:end));
for indexOfBlock = 1:nBlocks
    PercChange_mean{indexOfBlock,1} = mean(PercChange(1,indexOfBlock:nBlocks:end));
    PercChange_std{indexOfBlock,1} = std(PercChange(1,indexOfBlock:nBlocks:end));
end

figure
x = linspace(320/60/4,(320*5/60-320/60/4),10);
hold on
errorbar(x,cell2mat(PercChange_mean),cell2mat(PercChange_std),'-d')
xlabel 't / min'; ylabel 'Mean Concentration Change / %'
if exist('excludeSubjects')
    if ~isempty(strfind(ExpName,'Rest'))
        title ([metabToFig,' time course, subjects excluded, Rest'])
    else
        title ([metabToFig,' time course, subjects excluded, Stim'])
    end
else
    if ~isempty(strfind(ExpName,'Rest'))
        title ([metabToFig,' time course, Rest'])
    else
        title ([metabToFig,' time course, Stim'])
    end
end

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]); set(gca, 'FontWeight','bold')
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
PathName = strcat(LCModelOutputPath,'BlockConcTimeCourse/');
if exist('excludeSubjects')
    fig2save = fullfile(PathName, strcat(ExpName, '_', metabToFig, '_Mean_Excluded_PercChange.fig'));
else
    fig2save = fullfile(PathName, strcat(ExpName, '_', metabToFig, '_Mean_PercChange.fig'));
end
savefig(fig2save)

end


function createConcTimeCourseFig(ExpName,LCModelOutputPath,subjects,nBlocks,metabToFig,woBaseline,excludeSubjects)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
ExpName = strcat(ExpName, woBaseline);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
dataTimeCourse = load(exportFileMat);
fields = fieldnames(dataTimeCourse);
dataTimeCourse = dataTimeCourse.(fields{1});
metaboliteNames = (dataTimeCourse(1,:));
indexMet = strfind(metaboliteNames,metabToFig);
idxMet = find(not(cellfun('isempty',indexMet)));

if exist('excludeSubjects')
    n = length(dataTimeCourse);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(dataTimeCourse(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    dataTimeCourse([excludeIdx],:) = [];
end

figure
x = linspace(320/60/4,(320*5/60-320/60/4),10);
hold on
[ss,~] = size(dataTimeCourse);
for idxSubject = 1:floor(ss/nBlocks)
    if metabToFig == 'Lac'
        plot(x,cell2mat(dataTimeCourse(((idxSubject-1)*10+2):((idxSubject-1)*10+11),idxMet(1)))+(idxSubject-1)*1.5,'d-')
    elseif metabToFig == 'Glu'
        plot(x,cell2mat(dataTimeCourse(((idxSubject-1)*10+2):((idxSubject-1)*10+11),idxMet(1)))+(idxSubject-1)*5,'d-')
    else
        error('can only plot Lac and Glu so far');
    end
end
xlabel 't / min'; ylabel 'Concentration / a.u.'
if exist('excludeSubjects')
    if ~isempty(strfind(ExpName,'Rest'))
        title ([metabToFig,' time course, subjects excluded, Rest'])
    else
        title ([metabToFig,' time course, subjects excluded, Stim'])
    end
else
    if ~isempty(strfind(ExpName,'Rest'))
        title ([metabToFig,' time course, Rest'])
    else
        title ([metabToFig,' time course, Stim'])
    end
end

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]); set(gca, 'FontWeight','bold');
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 12, 20]);
PathName = strcat(LCModelOutputPath,'BlockConcTimeCourse/');
if exist('excludeSubjects')
    fig2save = fullfile(PathName, strcat(ExpName, '_', metabToFig, '_Excluded.fig'));
else
    fig2save = fullfile(PathName, strcat(ExpName, '_', metabToFig, '.fig'));
end
savefig(fig2save)


dataTimeCourse_temp = cell2mat(dataTimeCourse(2:end-4,2:end));
n = length(metaboliteNames);
for indexOfMet = 1:n-1
    for indexOfBlock = 1:nBlocks
        dataTimeCourse_mean{indexOfBlock,indexOfMet+1} = mean(dataTimeCourse_temp(indexOfBlock:nBlocks:end,indexOfMet));
        dataTimeCourse_std{indexOfBlock,indexOfMet+1} = std(dataTimeCourse_temp(indexOfBlock:nBlocks:end,indexOfMet));
    end
end

figure
x = linspace(320/60/4,(320*5/60-320/60/4),10);
hold on
errorbar(x,cell2mat(dataTimeCourse_mean(:,idxMet(1))),cell2mat(dataTimeCourse_std(:,idxMet(1))))
xlabel 't / min'; ylabel 'Mean Concentration / a.u.'
if exist('excludeSubjects')
    if ~isempty(strfind(ExpName,'Rest'))
        title ([metabToFig,' time course, subjects excluded, Rest'])
    else
        title ([metabToFig,' time course, subjects excluded, Stim'])
    end
else
    if ~isempty(strfind(ExpName,'Rest'))
        title ([metabToFig,' time course, Rest'])
    else
        title ([metabToFig,' time course, Stim'])
    end
end

yLimits = get(gca,'YLim');
boxoff = [0 0]; boxon = []; boxyoff = [yLimits(1) yLimits(2) yLimits(2) yLimits(1)];  boxyon = []; n = 1;
for i = 1:nBlocks/2
    boxoff  = [boxoff 320/60*i 320/60*i];
    if i < nBlocks/2
        boxon   = [boxon 320/60*i 320/60*i];
    end
end
for ii = 1:nBlocks/4
    boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
    boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
xlim([0 320*5/60]); set(gca, 'FontWeight','bold');
set(gcf, 'Units', ' centimeters', 'OuterPosition', [0, 0, 18, 12.6]);
PathName = strcat(LCModelOutputPath,'BlockConcTimeCourse/');
if exist('excludeSubjects')
    fig2save = fullfile(PathName, strcat(ExpName, '_', metabToFig, '_Mean_Excluded.fig'));
else
    fig2save = fullfile(PathName, strcat(ExpName, '_', metabToFig, '_Mean.fig'));
end
savefig(fig2save)

end

function createConcTableBlock(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
    LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,woBaseline)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
tableConcentrations = [];
isFirstIter = 1;
s = 1;
for indexCurrentSubject = 1:numberOfSubjects
    for nBl = 1:length(BlockNo)     
        %         retrieve parameters
        currentOutputFiles = strrep(strcat(LCModelOutputFiles,woBaseline), defaultSubject, subjects{indexCurrentSubject});
        currentPath = strcat(LCModelOutputPath,subjectsPath{indexCurrentSubject},'/',currentOutputFiles{nBl},'.table');
        [c1 c2 c3 c4] = textread(currentPath,'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
            tableConcentrations{1,s} = 'water concentration';
        end
        
        %         add ID and Water conc to table
%         tableConcentrations{(length(BlockNo)-2)*(indexCurrentSubject-1)+nBl-1,1} = strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject}, BlockNo{nBl});
        tableConcentrations{(length(BlockNo))*(indexCurrentSubject-1)+nBl+1,1} = strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject}, BlockNo{nBl});
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        tableConcentrations{(length(BlockNo))*(indexCurrentSubject-1)+nBl+1,end} = wconc_LCModel;
        %         add metabolites to table
        s = 2;
        for j=start:finish
            %             add quantified metabolite
            tableConcentrations{(length(BlockNo))*(indexCurrentSubject-1)+nBl+1,s} = str2num( c1{j});
            %             add CRLB
            tableConcentrations{(length(BlockNo))*(indexCurrentSubject-1)+nBl+1,s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end
end

% calculate mean, std
[n,m] = size(tableConcentrations);
subArray = cell2mat(tableConcentrations(2:end,2:end));
subArrayON1 = subArray(3:10:end,:);
subArrayON2 = subArray(4:10:end,:);
subArrayON3 = subArray(7:10:end,:);
subArrayON4 = subArray(8:10:end,:);
subArrayON = [subArrayON1; subArrayON2; subArrayON3; subArrayON4];
meanValueON = mean(subArrayON);
stdValueON = std(subArrayON);
tableConcentrations{n+1,1} = 'meanON';
tableConcentrations{n+2,1} = 'stdON';
for indexOfMetab = 1:m-1
    tableConcentrations{n+1,indexOfMetab+1} = meanValueON(1,indexOfMetab);
    tableConcentrations{n+2,indexOfMetab+1} = stdValueON(1,indexOfMetab);
end
subArrayOFF1 = subArray(5:10:end,:);
subArrayOFF2 = subArray(6:10:end,:);
subArrayOFF3 = subArray(9:10:end,:);
subArrayOFF4 = subArray(10:10:end,:);
subArrayOFF = [subArrayOFF1; subArrayOFF2; subArrayOFF3; subArrayOFF4];
meanValueOFF = mean(subArrayOFF);
stdValueOFF = std(subArrayOFF);
tableConcentrations{n+3,1} = 'meanOFF';
tableConcentrations{n+4,1} = 'stdOFF';
for indexOfMetab = 1:m-1
    tableConcentrations{n+3,indexOfMetab+1} = meanValueOFF(1,indexOfMetab);
    tableConcentrations{n+4,indexOfMetab+1} = stdValueOFF(1,indexOfMetab);
end

% save table to file
ExpName = strcat('BlockConc_', num2str(visualType), BOLDType, MMBType, woBaseline);
exportFileXlsx = [LCModelOutputPath ,strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)
end

function createConcTableBaseline(numberOfSubjects,BlockNo,LCModelOutputFiles,defaultSubject,subjects,...
    LCModelOutputPath,subjectsPath,prefixes,visualType,BOLDType,MMBType,woBaseline,excludeSubjects)
if exist('woBaseline', 'var') & woBaseline == true
    woBaseline = '_woBaseline';
else
    woBaseline = '';
end
if exist('excludeSubjects')
    n = length(subjects);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(subjects(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    subjects([excludeIdx],:) = [];
    prefixes([excludeIdx],:) = [];
    subjectsPath([excludeIdx],:) = [];
    numberOfSubjects = length(subjects);
end
tableConcentrations = [];
isFirstIter = 1;
s = 1;
for indexCurrentSubject = 1:numberOfSubjects
    for nBl = 2     %take baseline from second block of first REST
        % retrieve parameters
        currentOutputFiles = strrep(strcat(LCModelOutputFiles,woBaseline), defaultSubject, subjects{indexCurrentSubject});
        currentPath = strcat(LCModelOutputPath,subjectsPath{indexCurrentSubject},'/',currentOutputFiles{nBl},'.table');
        [c1 c2 c3 c4] = textread(currentPath,'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
            tableConcentrations{1,s} = 'water concentration';
        end
        
        % add ID and Water conc to table
        tableConcentrations{indexCurrentSubject+1,1} = strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject}, BlockNo{nBl});
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        tableConcentrations{indexCurrentSubject+1,end} = wconc_LCModel;
        % add metabolites to table
        s = 2;
        for j=start:finish
            %add quantified metabolite
            tableConcentrations{indexCurrentSubject+1,s} = str2num( c1{j});
            %add CRLB
            tableConcentrations{indexCurrentSubject+1,s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end
end

%calculate mean, std
[n,m] = size(tableConcentrations);
subArray = cell2mat(tableConcentrations(2:end,2:end));
meanValue = mean(subArray);
stdValue = std(subArray);
tableConcentrations{n+1,1} = 'mean';
tableConcentrations{n+2,1} = 'std';
for indexOfMetab = 1:m-1
    tableConcentrations{n+1,indexOfMetab+1} = meanValue(1,indexOfMetab);
    tableConcentrations{n+2,indexOfMetab+1} = stdValue(1,indexOfMetab);
end

% save table to file
if exist('excludeSubjects')
    ExpName = strcat('BaselineConc_', num2str(visualType), BOLDType, MMBType, woBaseline, '_Excluded');
else
    ExpName = strcat('BaselineConc_', num2str(visualType), BOLDType, MMBType, woBaseline);
end
exportFileXlsx = [LCModelOutputPath ,strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)
end