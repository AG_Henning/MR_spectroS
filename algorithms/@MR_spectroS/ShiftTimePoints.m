function this = ShiftTimePoints(this, pointStop)

%
if this.Parameter.ReconFlags.isPointsShifted
    warning('Timepoints have been already shifted');
end

oldData     = this.Data{1};
oldSize     = size(oldData);
nTimepoints = oldSize(1);
newData    = zeros(oldSize);


newData(1:nTimepoints-pointStop+1, :, :, :, :, :, :, :, :, :, :, :)  =...
oldData(pointStop:end, :, :, :, :, :, :, :, :, :, :, :);


this.Parameter.ReconFlags.isPointsShifted = true;
this.Data{1}                              = newData;


end

