k_0 = 0;
pKw = 13.617;
k_a = 9.95*1e6; %l/mol*s
k_b = 6.21*1e6; %l/mol*s
R_1s = 1.9; %s-1
E_Ab= 43.52; %kJ/mol
HR = 55.84; %kJ/mol
R = 8.314; %J/(mol K)
pHs = [6.0:0.01:8.0];
k_sw = zeros(length(pHs),1);
for index=1:length(pHs)
    pH =pHs(index);
    k_sw(index) = k_b * 10^(pH-pKw)+k_a *10^(-pH) + k_0;
end
figure 
plot(pHs, k_sw)

T = 310.15;
temp1 = HR / (R*log(10)) * (1/298.15 - 1/T);
temp2 = E_Ab / (R*log(10)) * (1/310.15-1/T);
k_sw = k_b * 10^(pH-14+temp1 + temp2)

1/R_1s

pi^2*2.5*1e-3*63.5^2