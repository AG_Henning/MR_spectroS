function [tableT2s] = calculate_T2s_all(tableConcentrations, availableTEs, PathName, downField_MM_Met, displaySilent)

if ~exist('downField_MM_Met', 'var')
    downField_MM_Met = 'MM';
end

if ~exist('displaySilent', 'var')
    displaySilent = false;
end

if strcmp(downField_MM_Met,'DF')
    keyMM_ppmNaming = {'DF58' 'DF60' 'DF68' 'DF71' 'DF73' 'DF75' 'DF82' 'DF83' 'DF835' 'NAAB' 'ATP' 'hCs' 'hist' 'NAD' 'NAA_DF' 'Cr'};
    valueMM = {'DF58';'DF60';'DF68';'DF71';'DF73';'DF75';'DF82';'DF83';'DF835';'NAA Broad';'ATP';'hCs';'hist';'NAD';'NAA';'Cr'};
%     keyMM_ppmNaming = {'DF58' 'DF60' 'DF68' 'DF71' 'DF73' 'DF75' 'DF82' 'DF83' 'DF835' 'ATP' 'NAAB' 'hist' 'hCs' 'NAADF' 'Cr'};
%     valueMM = {'DF58';'DF60';'DF68';'DF71';'DF73';'DF75';'DF82';'DF83';'DF835';'ATP';'NAA Broad';'hist';'hCs';'NAA';'Cr'};
else
    if strcmp(downField_MM_Met,'MM')
        keyMM_ppmNaming = {'MM09' 'MM12' 'MM14' 'MM17' 'MM20' 'MM22' 'MM26' 'MM27' 'MM30' 'MM32' 'MM36' 'MM37' 'MM38' 'MM39' 'MM42' 'Cre' 'MM18' 'Lip' 'MM40'};
        valueMM = {'M1';'M2';'M3';'M4';'M5';'M6';'M7';'M8';'M9';'M10';'M11';'M12';'M13';'M14';'M15'; 'Cr'; 'M18'; 'Lip'; 'MM40'};
    else
        if strcmp(downField_MM_Met,'UF')
%             keyMM_ppmNaming = {'Leu' 'Asp' 'Cho' 'Cr' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'GPC' 'Lac' 'mI' 'NAA' 'NAAG' 'PCr' 'PCh' 'PE' 'Scyllo' 'Tau' 'Cr+PCr' 'Cho+GPC+PCh' 'NAA+NAAG' 'mI+Glyc' 'Glu+Gln' 'CrRes'};
%             valueMM = {'Leu';'Asp';'Cho';'Cr';'GABA';'Glu';'Gln';'GSH';'Glyc';'GPC';'Lac';'mI';'NAA';'NAAG';'PCr';'PCh';'PE';'Scyllo';'Tau';'tCr';'tCho';'tNAA';'mI+Glyc';'Glx';'CrRes'};

%             keyMM_ppmNaming = {'Leu' 'Asp' 'Cr' 'Cr_CH2' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'GPC' 'Lac' 'mI' 'NAA_as' 'NAA' 'NAAG' 'PCh' 'PE' 'Scyllo' 'Tau' 'GPC+PCh' 'NAA+NAAG' 'mI+Glyc' 'Glu+Gln'};
%             valueMM = {'Leu';'Asp';'Cr-CH3';'Cr-CH2';'GABA';'Glu';'Gln';'GSH';'Glyc';'GPC';'Lac';'mI';'NAA-asp';'NAA-ace';'NAAG';'PCh';'PE';'Scyllo';'Tau';'tCho';'tNAA';'mI+Glyc';'Glx'};
            
            keyMM_ppmNaming = {'Leu' 'Asp' 'Cr' 'Cr_CH2' 'GABA' 'Glu' 'Gln' 'GSH' 'Glyc' 'GPC' 'Lac' 'mI' 'NAA_as' 'NAA' 'NAAG' 'PCh' 'PE' 'Scyllo' 'Tau' 'GPC+PCh' 'NAA+NAAG' 'mI+Glyc' 'Glu+Gln' 'Asc' 'Glc_al' 'Glc_be' 'Glc' 'Gln_de' 'GSH_no' 'hCs' 'Serine' 'GABA_d' 'histid' 'ATP'};
            valueMM = {'Leu';'Asp';'Cr-CH3';'Cr-CH2';'GABA';'Glu';'Gln';'GSH';'Glyc';'GPC';'Lac';'mI';'NAA-asp';'NAA-ace';'NAAG';'PCh';'PE';'Scyllo';'Tau';'tCho';'tNAA';'mI+Glyc';'Glx';'Asc';'Glc_al';'Glc_be';'Glc';'Gln_de';'GSH_no';'hCs';'Serine';'GABA_d';'histid';'ATP'};
        else
            error('Not defined downField_MM_Met case.');
        end
    end
end
mapMM_Names = containers.Map(keyMM_ppmNaming,valueMM);

tableT2s = [];
TEs = str2double(availableTEs(:));
tableT2s{2,1} = 'T2';
tableT2s{3,1} = 'STD';
tableT2s{4,1} = 'RSquare';
numberOfSubjects = length(tableConcentrations);
numberOfTEs = length(availableTEs);
numOfMetabolite = (size(tableConcentrations{1},2) - 3 ) / 3; % -3 for name and water concentration; /3 for conc, CRLB, rel. CRLB
for iMetabolite = 1:numOfMetabolite
    indexMetabolite = iMetabolite * 3 - 1;
    x = zeros(numberOfSubjects*numberOfTEs,1);
    y = zeros(numberOfSubjects*numberOfTEs,1);
    for iSubject = 1:numberOfSubjects
        tableConcentration = tableConcentrations{iSubject};
        metaboliteName = tableConcentration(1,indexMetabolite);
        startIndex = (iSubject-1)*numberOfTEs + 1;
        x(startIndex:startIndex+numberOfTEs-1) = TEs;
        y(startIndex:startIndex+numberOfTEs-1) = cell2mat(tableConcentration(2:numberOfTEs+1,indexMetabolite));
        y(startIndex:startIndex+numberOfTEs-1) = y(startIndex:startIndex+numberOfTEs-1) ./ y(startIndex);
    end
    tempX = x(~(isnan(y)));
    tempY = y(~(isnan(y)));
    if length(tempX) >= 2
        
        [expofit, gof] = fit(tempX,tempY,'exp1');
        calculatedT2 = 1.0/(-1.0*expofit.b);
        confInt_95 = confint(expofit);
        confT2 = 1.0./(-1.0 .* confInt_95(:,2));
        calculatedT2_std = confT2(2) - calculatedT2;
        if length(tempX) == 2
            if isnan(y(1)) || isnan(y(2))
                thisValue = [metaboliteName ' ' num2str(calculatedT2) ' ' num2str(gof.rsquare)]
                calculatedT2 = NaN;
                gof.rsquare = 0;
            end
        end
    else
        calculatedT2 = NaN;
        gof.rsquare = 0;
    end
    %saving results
    tableT2s{1,iMetabolite+1} = mapMM_Names(metaboliteName{1});
    tableT2s{2,iMetabolite+1} = num2str(calculatedT2,'%.2f');
    tableT2s{3,iMetabolite+1} = num2str(calculatedT2_std,'%.2f');
    tableT2s{4,iMetabolite+1} = num2str(gof.rsquare,'%.2f');
    if displaySilent == false
        % plotting
        figure(iMetabolite);
        plot(expofit,x,y);
        title(metaboliteName);
    end
end


%% save table to file
exportFileXlsx = [PathName ,'T2_Results_all.xlsx'];
xlswrite(exportFileXlsx, {PathName}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableT2s, 1, cellPosition);
end