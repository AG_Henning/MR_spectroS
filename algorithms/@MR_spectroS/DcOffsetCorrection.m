function this= DcOffsetCorrection(this)

mean_factor=0.20; % *100%

if this.Parameter.ReconFlags.isdcoffsetcorr
    
    warning('The data is already DC_offset corrected');
    
else
    
    if this.Parameter.ReconFlags.iszerofill
        
        warning(' The data is zero filled...') %I will change in the future
        
    else
        
         % NCol, 1, 1, NCha, 1, 1, 1, 1, n_mix, NRep, 1, NSet  
       
          data=this.Data{1};
          
          NCol=  size( data, this.kx_dim);   % # timepoints
          NMeas= size( data, this.meas_dim); % # Averages
          NCha=  size( data, this.coil_dim); % # Channels
          NRep=  size( data, this.dyn_dim);  % # Repetitions
          
          number_points=ceil(mean_factor* NCol);
          
          if (size(data,this.mix_dim))==1
            selected_mix=1;
             
            % Find the averaged DC_offset in every dynamic and coil
            tmp_data=sum(data(end-number_points:end, 1, 1, :, 1, 1, 1, 1, selected_mix, :, 1, :),this.meas_dim)/NMeas;
            means= mean(tmp_data,1);
            
            for coil=1:NCha
                for dyn=1:NRep
                    
                    data(:, 1, 1, coil, 1, 1, 1, 1, selected_mix, dyn, 1, :)=...
                    data(:, 1, 1, coil, 1, 1, 1, 1, selected_mix, dyn, 1, :)-...
                    means(:, 1, 1, coil, 1, 1, 1, 1, selected_mix, dyn, 1, :);
                
                end
            end
            
            
          
          else %if the water peak is available
    
              
          end
          this.Data{1}=data;
          this.Parameter.ReconFlags.isdcoffsetcorr=true;
       
    end
    
    
    
    
    
    plot_checked=true;
    if plot_checked
   figure
   subplot(2,2,1)
    hist([real(squeeze(means(1,1,1,1,1,1,1,1,1,:,1,1))) imag(squeeze(means(1,1,1,1,1,1,1,1,1,:,1,1)))]);
   subplot(2,2,2)
    hist([real(squeeze(means(1,1,1,4,1,1,1,1,1,:,1,1))) imag(squeeze(means(1,1,1,4,1,1,1,1,1,:,1,1)))]);
   subplot(2,2,3)
    hist([real(squeeze(means(1,1,1,8,1,1,1,1,1,:,1,1))) imag(squeeze(means(1,1,1,8,1,1,1,1,1,:,1,1)))]);
   subplot(2,2,4)
    hist([real(squeeze(means(1,1,1,10,1,1,1,1,1,:,1,1))) imag(squeeze(means(1,1,1,10,1,1,1,1,1,:,1,1)))]);
    end
    
    
end

