function ISIS_MRSpectroS_fc(measurementID)
%% ISIS processing with MR_spectroS

if ~exist('measurementID','var')
    measurementID = 91;
end

% read data
this = MR_spectroS(measurementID);
% data = squeeze(this.Data{1});
% data = data(:,:,33:end);
% % data = data(:,:,65:end);
% data = permute(data,[1 4 5 2 6 7 8 9 10 11 12 3]);
% this.Data{1} = data;
% clear data

this.Parameter.ISISSettings.scheme = 'ISIS-orig';
% this.Parameter.ISISSettings.scheme = 'EISIS-full';

this.Parameter.PhaseCorr31PSettings.sing = [0];


%be careful with Pi intra/extra, NADH and bATP!
metaboliteNames = {'PE', 'PC', 'Pi_ext', 'Pi_int', 'GPE', 'GPC', 'PCr', 'gATP', 'aATP', 'NADH', 'bATP'};
centerPpm =       [ 6.76, 6.24, 5.15,    4.8,       3.5,   2.95,  0,     -2.52,  -7.53,  -8.15,  -16.24];
%% somehow only 11-D complex double, make it 12-D that it is consistent with MR_spectroS

if length(size(this.Data{1})) == 11
    oldData             = this.Data{1};
    permData            = double(permute(oldData,[1 2 3 4 5 6 7 8 9 10 12 11]));
    this.Data{1}        = permData;
clear oldData permData
end
%% remove 1H channels (1-10 1H, 11-20 31P, 11-12: vertical loops on top of head)
this = this.DeleteCoilChannels([1:10]); 
%% pre-calculate parameters for Freq Alignment
[~, this2, weights] = ISIS_main_processing(this,1);   %Add breakpoint in ISIS_main_processing!
                                                      %ISIS_main_processing(this,verbose) verbose=1: open plot_spect
[thisFreqAlign] = ISIS_prep_freqAlign(this, this2, weights);


%% setup the settings for the FrequencyAlignFreqDomain
a = this; % just added an MRSpectro_S object
a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1;
a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;
%indices of Interest: given in ppm
% PCr peak
a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = [0];
%set zeroFillingParameter to get smooth approximations
a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;

%search area (+-) in ppm
a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.5;
%spline filtering is not needed if the spectra are not too noisy.
%flag to do spline filtering
a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
%spline filtering coefficient
a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;


%% Frequency Alignment 1
thisFreqAlignIsisBlock = thisFreqAlign.ISIS_Scheme;
% do the actual Frequency Alignment
a.Data{1} = thisFreqAlignIsisBlock.Data{1};
a2 = a.FrequencyAlignFreqDomain;
freqShiftsISISBlock = a2.Parameter.FreqAlignFreqDomainSettings.freqShifts;

%% Frequency Alignment 2
thisFreqAlignIsisSteps = thisFreqAlign.ISIS_Averaging;
% do the actual Frequency Alignment
a.Data{1} = thisFreqAlignIsisSteps.Data{1};
a2 = a.FrequencyAlignFreqDomain;
freqShiftsISISSteps = a2.Parameter.FreqAlignFreqDomainSettings.freqShifts;

%% do ISIS Frequency Alignment
% this = this.FrequencyAlignISIS(freqShiftsISISBlock,freqShiftsISISSteps);

% do actual processing after frequency alignment
[this, ~, ~] = ISIS_main_processing(this,0);  %ISIS_main_processing(this,verbose) verbose=0: no plot_spect

%% apply the previously calculated values
this.Parameter.MissingPointPredictionSettings = this2.Parameter.MissingPointPredictionSettings;
this.Parameter.PhaseCorrSettings = this2.Parameter.PhaseCorrSettings;
this.Parameter.Phase0RemovalSettings.phase0 = this2.Parameter.Phase0RemovalSettings.phase0; 
if this.Parameter.Phase0RemovalSettings.phase0 ~= 0
    phase0 = this.Parameter.Phase0RemovalSettings.phase0;
    this.Data{1} = this.Data{1}* exp(-1i*phase0);
end
this = this.PhaseCorrection;
if this.Parameter.MissingPointPredictionSettings.polyOrder ~= 60
    this = this.MissingPointPrediction;
end
%% FrequencyAlign PCr to 0
this = this.FrequencyAlignFreqDomain;

%% save the data
fileName = [this.Parameter.Filename(1:end-4) '_no_gauss_filter.mat'];
save(fileName, 'this');
%% Gaussian Filtering 50Hz
Fgau = 20;
oldData = this.Data{1};
oldData = squeeze(oldData);

dwellTime   = this.Parameter.Headers.DwellTimeSig_ns * 1e-9; %in sec
t = (0:length(oldData)-1)*dwellTime;
GauFilter = exp ( - t'.^2*(Fgau)^2 );
oldDataFiltered = oldData.*GauFilter;

this.Data{1} = oldDataFiltered;
%% exponential filtering
% oldData = this.Data{1};
% oldData = squeeze(oldData);
% 
% dwellTime   = this.Parameter.Headers.DwellTimeSig_ns * 1e-9; %in sec
% t = (0:length(oldData)-1)*dwellTime;
% nCoils        = size(this.Data{1},this.coil_dim);
% Fexp = 30;     %in Hz
% 
% ExpFilter = exp(-t'*Fexp);
% 
% oldDataFiltered = oldData.*ExpFilter;
% 
% this.Data{1} = oldDataFiltered;
% 
% clearvars -except fileNameAfterPhaseCorrection this trunc_ms truncPoint

%% low rank approximation (Qu et al, Angewandte Chemie, (54) 2015)
% 1. Build Hankel matrix H
% 2. Given H, construct its SVD, H = USV^T
% 3. Derive from S matrix Sk formed by replacing by 0 the nCols-k smallest
% singular values on diagonal of S
% 4. compute and output Hk = USkV^T as the rank-k approximation to H

% k = 8;  %rank of low-rank matrix
% Data = this.Data{1};
% H = hankel(Data);
% [n,m] = size(H);
% % H = H(1:n-round(0.1*m)+1,1:round(0.1*m));
% 
% [U,S,V] = svd(H);
% Sk = S;
% 
% % delete nCols-k eigenvalues on diag 
% [nRows,nCols] = size(Sk);
% subsetIdx = [(k+1):1:nCols];
% diagonalIdx = (subsetIdx-1) * (nRows + 1) + 1;
% Sk(diagonalIdx) = 0;
% 
% Hk = U*Sk*V';
% 
% figure
% plot(freq/162,real(fftshift(fft(Data))))
% hold on
% x = linspace(1,n,nCols)';
% plot(freq/162,real(fftshift(fft(Hk(1,:)))))
% axis([-20 20 -inf inf])
% set(gca,'xdir','reverse')
% 
% bla                  = Hk(1,:);
% bla = fftshift(fft(bla'));   
% snrValue = max( abs(bla(1:end)))/std(bla(((size(bla))-round(size(bla)*1/4)):end))

% this.Data{1} = Hk(1,:);

%% save the data
fileName = [this.Parameter.Filename(1:end-4) '.mat'];
save(fileName, 'this');

%% MRUI Export
this.Parameter.ReconFlags.isCoilsCombined = true;
filename = [this.Parameter.Filename(1:end-4)];
this.ExportMruiText(filename,'.txt','31P')

%% quick look at data
nTimepoints     = size(this.Data{1},this.kx_dim);
bandwidth       = this.Parameter.Headers.Bandwidth_Hz;
% deltaT          = this.Parameter.Headers.DwellTimeSig_ns;

freq    = bandwidth/nTimepoints*(-(nTimepoints/2-1):1:nTimepoints/2)';
time    = 1/bandwidth*(0:nTimepoints-1)'*1000;      %ms
scanFreq_MHz = this.Parameter.Headers.ScanFrequency/1E6;

%Spectrum
figure()
subplot(2,1,1)
plot(time,(real(this.Data{1})))
xlim([0 200])
xlabel 't / ms'
subplot(2,1,2)
% plot(freq/scanFreq_MHz,(real(fftshift(fft(this.Data{1})))))
plot(freq/scanFreq_MHz,(real(fftshift(fft(this.Data{1}))))/max(real(fftshift(fft(this.Data{1})))))
axis([-30 30 -inf inf])
set(gca,'xdir','reverse')
xlabel 'f / ppm'

figure
plot(freq/scanFreq_MHz,(real(fftshift(fft(this.Data{1}))))/max(real(fftshift(fft(this.Data{1})))))
axis([-20 8 -inf inf])
set(gca,'xdir','reverse')
xlabel 'f / ppm'
%% SNR in frequency domain after Filtering

Data                  = this.Data{1};
Data = fftshift(fft(Data));   

searchArea = 0.2; %ppm
snrValue_Filtered = zeros(length(metaboliteNames),1);
for iMetabolite = 1:length(metaboliteNames)
    ppmStartMet = round(nTimepoints/2 + (centerPpm(iMetabolite) + searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
    ppmEndMet = round(nTimepoints/2 + (centerPpm(iMetabolite) - searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
    snrValue_Filtered(iMetabolite) = max( abs(Data(ppmEndMet:ppmStartMet)))/std(Data(((size(Data))-size(Data)*1/4):end));
end

%% FWHM of filtered data
ppmStart = bandwidth/scanFreq_MHz/2; ppmEnd = -bandwidth/scanFreq_MHz/2;
searchArea = 0.2; %ppm
ppmVector = freq/scanFreq_MHz;
fwhm = zeros(length(metaboliteNames),1);
for iMetabolite = 1:length(metaboliteNames)
    ppmStartMet = centerPpm(iMetabolite) + searchArea;
    ppmEndMet = centerPpm(iMetabolite) - searchArea;
    spectrumPeak = Data;
    spectrumPeak(ppmVector>ppmStartMet | ppmVector < ppmEndMet) = 0;
    [fwhm_Filtered(iMetabolite), ~] = fwhm_Hz(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
end


%% SNR in frequency domain, no filtering but truncation 
fileNameAfterBeforeFiltering = [this.Parameter.Filename(1:end-4) '_no_gauss_filter.mat'];
load(fileNameAfterBeforeFiltering);
trunc_ms = 100;
truncPoint = floor( trunc_ms/(this.Parameter.Headers.DwellTimeSig_ns*1e-6));
this = this.Truncate(truncPoint);
Data                  = this.Data{1};
Data = fftshift(fft(Data));   

searchArea = 0.2; %ppm
snrValue_Trunc = zeros(length(metaboliteNames),1);
for iMetabolite = 1:length(metaboliteNames)
    ppmStartMet = round(nTimepoints/2 + (centerPpm(iMetabolite) + searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
    ppmEndMet = round(nTimepoints/2 + (centerPpm(iMetabolite) - searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
    snrValue_Trunc(iMetabolite) = max( abs(Data(ppmEndMet:ppmStartMet)))/std(Data(((size(Data))-size(Data)*1/4):end));
end

%% FWHM of non filtered, but truncated data
ppmStart = bandwidth/scanFreq_MHz/2; ppmEnd = -bandwidth/scanFreq_MHz/2;
searchArea = 0.2; %ppm
ppmVector = freq/scanFreq_MHz;
fwhm = zeros(length(metaboliteNames),1);
for iMetabolite = 1:length(metaboliteNames)
    ppmStartMet = centerPpm(iMetabolite) + searchArea;
    ppmEndMet = centerPpm(iMetabolite) - searchArea;
    spectrumPeak = Data;
    spectrumPeak(ppmVector>ppmStartMet | ppmVector < ppmEndMet) = 0;
    [fwhm_Trunc(iMetabolite), ~] = fwhm_Hz(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
end

%% save SNR table
snrValues = [snrValue_Filtered(1) snrValue_Filtered(2) snrValue_Filtered(3) snrValue_Filtered(4) snrValue_Filtered(5) snrValue_Filtered(6) snrValue_Filtered(7) snrValue_Filtered(8) snrValue_Filtered(9) snrValue_Filtered(10) snrValue_Filtered(11);...
    fwhm_Filtered(1) fwhm_Filtered(2) fwhm_Filtered(3) fwhm_Filtered(4) fwhm_Filtered(5) fwhm_Filtered(6) fwhm_Filtered(7) fwhm_Filtered(8) fwhm_Filtered(9) fwhm_Filtered(10) fwhm_Filtered(11);... 
    snrValue_Trunc(1) snrValue_Trunc(2) snrValue_Trunc(3) snrValue_Trunc(4) snrValue_Trunc(5) snrValue_Trunc(6) snrValue_Trunc(7) snrValue_Trunc(8) snrValue_Trunc(9) snrValue_Trunc(10) snrValue_Trunc(11);...
    fwhm_Trunc(1) fwhm_Trunc(2) fwhm_Trunc(3) fwhm_Trunc(4) fwhm_Trunc(5) fwhm_Trunc(6) fwhm_Trunc(7) fwhm_Trunc(8) fwhm_Trunc(9) fwhm_Trunc(10) fwhm_Trunc(11)];
rowNames = {'SNR Filtered','FWHM Filtered','SNR Truncated','FWHM Truncated'};
colNames = {'PE','PC','Piext','Piint','GPE','GPC','PCr','g_ATP','a_ATP','NADH','b_ATP'};
SNR = array2table(snrValues,'RowNames',rowNames,'VariableNames',colNames)

fileName = [this.Parameter.Filename(1:end-4) '_SNR'];
save(fileName, 'SNR');

%% plot truncated spectrum, not normalized
figure
plot(freq/scanFreq_MHz,(real(fftshift(fft(this.Data{1})))))
axis([-20 8 -inf inf])
set(gca,'xdir','reverse')
xlabel 'f / ppm'