function plot_31P_T2_meanFitCurves()
%%%%%%%%
% read quantification results created in calculate_31P_T2.m
% normalize to PCr TE 8ms, fit mean, plot errorbars

pathName = 'T2 31P Paper Data';
[localFilePathBase] = pathToDataFolder(pathName);

outputPath = 'Output\'; 
% outputPath = 'Output_Real_Vespa_Original\';   
% outputPath = 'Output_Real_Vespa_Phased\';
% outputPath = 'Output_Real_Vespa_Phased_NormAbs400\';
% outputPath = 'Output_Real_Vespa_Phased_NormReal400\';
    
subjects = {'9873';'1090';'3490';'9810';'1658';'2017';'6524';'3009';...
    '6260';'2823';'2455';'4085';'Summed'};
prefixes = {'2019-09-30_';'2019-10-10_';'2019-10-14_';'2019-10-14_';...
    '2019-10-22_';'2019-11-07_';'2019-11-07_';'2019-11-20_';...
    '2019-11-20_';'2019-12-05_';'2020-07-22_';'2020-07-23_';''};
suffixFolder = '_SurfCoil';

% %be careful with Pi intra/extra, NADH and bATP!
% metaboliteNames = {'PE', 'PC', 'Pi_int', 'GPE', 'GPC', 'PCr', 'gATP', 'aATP', 'NADH'};
% centerPpm =       [ 6.77, 6.23, 4.83,     3.51,   2.96,  0,     -2.48, -7.62,  -8.16];

numberOfSubjects = length(subjects);
TR = 5000;  %ms
TM = 5;     %ms
TEs = {'6'; '8'; '11'; '15'; '20'; '30'; '50'; '80'; '150'};  %ms
numberOfTEs = length(TEs);

paths = cell(1,numberOfSubjects);
files = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    if ~isempty(strfind(subjects{indexSubject},'Summed'))
        paths{indexSubject} = [localFilePathBase outputPath prefixes{indexSubject} subjects{indexSubject} '\'];
        files{indexSubject} = ['Quantification_Results_' subjects{indexSubject} '_STEAM_Real__LW_noTrunc.xlsx'];
    else
        paths{indexSubject} = [localFilePathBase outputPath prefixes{indexSubject} subjects{indexSubject} suffixFolder '\'];
        files{indexSubject} = ['Quantification_Results_' subjects{indexSubject} '_STEAM_Real__LW_noTrunc.xlsx'];
    end
end

%load LCModel quantification results
Conc = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    Conc{indexSubject} = readtable([paths{indexSubject} files{indexSubject}]);
end

%normalize to PCr TE 8ms
yPerSubj = cell(numberOfSubjects,12);
for indexSubject = 1:numberOfSubjects
    for iMetabolite = 1:12
        indexMetabolite = iMetabolite * 3 - 1;
        metaboliteName = table2array(Conc{1,indexSubject}(1,indexMetabolite));
        metaboliteNames(iMetabolite) = metaboliteName;
        y = table2array(Conc{1,indexSubject}(2:numberOfTEs+1,indexMetabolite));
        y(cellfun(@isempty, y)) = {nan};
        y = cellfun(@(x)str2double(x), y);
        %normalize to PCr TE 8ms
        PCrConcTE8ms = cellfun(@str2num,(table2array(Conc{1,indexSubject}(3,29))));
        y = y./PCrConcTE8ms.*200;
        yPerSubj{indexSubject,iMetabolite} = y;
        
%         [calculatedT2, gof] = calculateT2Fit(x,y, iMetabolite, metaboliteName, displaySilent, PathName, volunteer);
%         tableConcentrations{11,indexMetabolite} = calculatedT2;
%         tableConcentrations{12,indexMetabolite} = gof.rsquare;
%         [calculatedT2ManualTE, gofManualTE] = calculateT2FitManualTE(x,y, iMetabolite, metaboliteName, displaySilent, PathName, volunteer);
%         tableConcentrations{13,indexMetabolite} = calculatedT2ManualTE;
%         tableConcentrations{14,indexMetabolite} = gofManualTE.rsquare;
%         %saving results
%         metaboliteName{1};
%         tableT2s{1,iMetabolite+1} = metaboliteName{1};
%         tableT2s{2,iMetabolite+1} = num2str(calculatedT2,'%.2f');
%         tableT2s{3,iMetabolite+1} = num2str(gof.rsquare,'%.2f');
%         tableT2s{4,iMetabolite+1} = num2str(calculatedT2ManualTE,'%.2f');
%         tableT2s{5,iMetabolite+1} = num2str(gofManualTE.rsquare,'%.2f');
    end
end

%calculate mean and std per TE and metabolite over volunteers
for iMetabolite = 1:12
    for indexSubject = 1:numberOfSubjects-1     %last one is summed data
        temp(:,indexSubject) = yPerSubj{indexSubject,iMetabolite};
    end
    for indexTE = 1:numberOfTEs
        temp_Mean(indexTE) = nanmean(temp(indexTE,:));
        temp_Std(indexTE)  = nanstd(temp(indexTE,:));
    end
    metabolite_Mean(iMetabolite,:) = temp_Mean;     %metabolite x TE, equals summed data
    metabolite_Std(iMetabolite,:)  = temp_Std;      %metabolite x TE, equals summed data
end


%fit T2 curve to mean of each metabolite
x = str2double(TEs(:));
displaySilent = false;
PathName = [localFilePathBase outputPath];
for iMetabolite = 1:12
    metaboliteName = metaboliteNames(1,iMetabolite);
    y = metabolite_Mean(iMetabolite,:);
    y_error = metabolite_Std(iMetabolite,:);
%     [calculatedT2, gof] = calculateT2Fit(x,y,y_error, iMetabolite, metaboliteName, displaySilent, PathName);

    [calculatedT2ManualTE, gofManualTE] = calculateT2FitManualTE(x,y,y_error, iMetabolite, metaboliteName, displaySilent, PathName);
    %saving results
    metaboliteName{1};
    tableT2s{1,iMetabolite+1} = metaboliteName{1};
    tableT2s{2,iMetabolite+1} = num2str(calculatedT2,'%.2f');
    tableT2s{3,iMetabolite+1} = num2str(gof.rsquare,'%.2f');
    tableT2s{4,iMetabolite+1} = num2str(calculatedT2ManualTE,'%.2f');
    tableT2s{5,iMetabolite+1} = num2str(gofManualTE.rsquare,'%.2f');

end

%save T2s
for i = 2:length(tableT2s)
    tableT2(1,i-1) = str2num(tableT2s{2,i});
    tableT2(2,i-1) = str2num(tableT2s{3,i});
    tableT2(3,i-1) = str2num(tableT2s{4,i});
    tableT2(4,i-1) = str2num(tableT2s{5,i});
end
rowNames = {'T2','Rsquare','T2_ManualTE','Rsquare_ManualTE'};
for i = 2:length(tableT2s)
    colNames{1,i-1} = tableT2s{1,i};
    colNames{1,i-1} = strrep(colNames{1,i-1},'-','_');
    colNames{1,i-1} = strrep(colNames{1,i-1},'+','_');
end
tableT2toSave = array2table(tableT2,'RowNames',rowNames,'VariableNames',colNames)

fileName = [PathName ,'T2_plots_MeanStd\T2_Results_MeanOver12Volunteers'];
save(fileName, 'tableT2toSave');


end

function [calculatedT2, gof] = calculateT2Fit(x, y, y_error, iMetabolite, metaboliteName, displaySilent, PathName)
tempX = x(~(isnan(y)));
tempY = y(~(isnan(y)))'; %best fit precision when y values between 10 and 100
tempYError = y_error(~(isnan(y_error)))';
if length(tempX) >= 3
    options = fitoptions('exp1', 'Upper', [Inf 0]);
    [expofit, gof] = fit(tempX,tempY, 'exp1', options);
    calculatedT2 = 1.0/(-1.0*expofit.b);
    if length(tempX) == 2
        if isnan(y(1)) || isnan(y(2))
            thisValue = [metaboliteName ' ' num2str(calculatedT2) ' ' num2str(gof.rsquare)]
            calculatedT2 = NaN;
            gof.rsquare = 0;
        end
    end
    if displaySilent == false
        % plotting
        figure(iMetabolite);
%         xx = linspace(0,max(tempX),100);
        %             plot(expofit,tempX,tempY);
        plot(linspace(0,(max(tempX)+10),100),expofit.a*exp(expofit.b*linspace(0,(max(tempX)+10),100)),'b--');
%         plot(xx,expofit.a*exp(expofit.b*xx),'b--'); 
        hold on;
        errorbar(tempX,tempY,tempYError,'bx')
        title({metaboliteName{1}},'Interpreter','none');
        xlabel 'TE / ms'; ylabel 'Signal Amp. / a.u.';
        annotation('textbox', [0.7, 0.8, 0.1, 0.1], 'String', {strcat('R2: ',num2str(gof.rsquare)),strcat('T2: ',num2str(calculatedT2),' ms')})
        mkdir([PathName 'T2_plots_MeanStd\'])
        savefig([PathName 'T2_plots_MeanStd\' metaboliteName{1} '_MeanOver12Volunteers.fig'])
    end
else
    calculatedT2 = NaN;
    gof.rsquare = 0;
end
close all
end



function [calculatedT2ManualTE, gofManualTE] = calculateT2FitManualTE(x, y, y_error, iMetabolite, metaboliteName, displaySilent, PathName)
%     if strfind(PathName,'Summed')
        if (~isempty(strfind(metaboliteName{1},'PCr'))||~isempty(strfind(metaboliteName{1},'PE'))||~isempty(strfind(metaboliteName{1},'PC'))||...
                ~isempty(strfind(metaboliteName{1},'GPE'))||~isempty(strfind(metaboliteName{1},'GPC')))%TE 6-150ms
            tempX = x(1:end);
            tempY = y(1:end);
            tempYError = y_error(1:end);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))); 
            tempYError = tempYError(~(isnan(tempYError)))';
        elseif (~isempty(strfind(metaboliteName{1},'NAD')))    %TE 6-20ms
            tempX = x(1:5);
            tempY = y(1:5);
            tempYError = y_error(1:5);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))); 
            tempYError = tempYError(~(isnan(tempYError)))';
        elseif (~isempty(strfind(metaboliteName{1},'ext')))    %TE 6-15ms
            tempX = x(1:4);
            tempY = y(1:4);
            tempYError = y_error(1:4);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))); 
            tempYError = tempYError(~(isnan(tempYError)))';
       elseif ~isempty(strfind(metaboliteName{1},'ATP'))    %TE 6-50ms
            tempX = x(1:7);
            tempY = y(1:7);
            tempYError = y_error(1:7);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))); 
            tempYError = tempYError(~(isnan(tempYError)))';
        elseif ~isempty(strfind(metaboliteName{1},'in'))    %TE 6-80ms
            tempX = x(1:8);
            tempY = y(1:8);
            tempYError = y_error(1:8);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))); 
            tempYError = tempYError(~(isnan(tempYError)))';
        end
        tempY = tempY';
        tempYError = tempYError';
        
    if length(tempX) >= 3
        options = fitoptions('exp1', 'Upper', [Inf 0]);
        [expofit, gofManualTE] = fit(tempX,tempY, 'exp1', options);
        %identify outliers as points at a distance greater than 1.0 std
        %from the baseline model and refit the data with the outliers excluded
        fdata = feval(expofit,tempX);
        I = abs(fdata - tempY) > 2.0*std(tempY);
        outliers = excludedata(tempX,tempY,'indices',I);
        options = fitoptions('exp1', 'Upper', [Inf 0],'Exclude',outliers);
        if (length(tempX)-sum(outliers(:))) >= 3
            [expofit, gofManualTE] = fit(tempX,tempY, 'exp1', options);
            calculatedT2ManualTE = 1.0/(-1.0*expofit.b);
            if length(tempX) == 2
                if isnan(y(1)) || isnan(y(2))
                    thisValue = [metaboliteName ' ' num2str(calculatedT2ManualTE) ' ' num2str(gofManualTE.rsquare)]
                    calculatedT2ManualTE = NaN;
                    gofManualTE.rsquare = 0;
                end
            end
            if displaySilent == false
                % plotting
                figure(iMetabolite);
%                 plot(tempX(~outliers),tempY(~outliers),'.');
                hold on;
                plot(linspace(0,(max(tempX)+10),100),expofit.a*exp(expofit.b*linspace(0,(max(tempX)+10),100)),'b--');
                plot(tempX(outliers),tempY(outliers),'k*');
                errorbar(tempX,tempY,tempYError,'bx')
                title({metaboliteName{1}},'Interpreter','none');
                plots=get(gca, 'Children');
%                 if sum(outliers(:))~=0
%                     legend(plots([3,1,2]), {'data','excluded data','fit'});
%                 else
%                     legend('data','fit');
%                 end
                xlabel 'TE / ms'; ylabel 'Signal Amp. / a.u.';
                annotation('textbox', [0.7, 0.8, 0.1, 0.1], 'String', {strcat('R2: ',num2str(gofManualTE.rsquare)),strcat('T2: ',num2str(calculatedT2ManualTE),' ms')})
                mkdir([PathName 'T2_plots_MeanStd\'])
                savefig([PathName 'T2_plots_MeanStd\' metaboliteName{1} '_MeanOver12Volunteers_ManualTE.fig'])
            end
        else
            calculatedT2ManualTE = NaN;
            gofManualTE.rsquare = 0;
        end
    else
        calculatedT2ManualTE = NaN;
        gofManualTE.rsquare = 0;
    end
    close all
end