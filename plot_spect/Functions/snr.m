function snrValue = snr( data,SelectedProcessingValues )
%version 26 Sep 2015


noiseRangeTD     = get(SelectedProcessingValues, 'NoiseRangeTD') ;
noiseTimeDomain  = get(SelectedProcessingValues, 'NoiseTimeDomain');
noiseRangeFD     = get(SelectedProcessingValues, 'NoiseRangeFD');
peakRange        = get(SelectedProcessingValues, 'PeakRange');

if noiseTimeDomain
   snrValue = max( abs(data(1:30)))/std(data(noiseRangeTD(1):noiseRangeTD(2)));
else
    fftsReal =  real(fftshift(fft(data)));
    %amplitude = sum(fftsReal(peakRange(1):peakRange(2)));
    amplitude = max(abs(fftsReal(peakRange(1):peakRange(2))));
    
    % noise calculation
    noise     = (fftsReal(noiseRangeFD(1):noiseRangeFD(2)));
    fitobject = fit((0:length(noise)-1)',noise,'poly1'); %fit a poly1 to noise
    y         = feval(fitobject,(0:length(noise)-1)');   % substract poly1 from noise
    noise     = noise - y;% now the noise does not have linear component
    noise     = std(noise);
    
    snrValue  = amplitude/noise;
    %snrValue  = 20*log(amplitude/noise); %in dB
   
   
end

