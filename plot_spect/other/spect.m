function varargout = spect(varargin)
% SPECT MATLAB code for spect.fig
%      SPECT, by itself, creates a new SPECT or raises the existing
%      singleton*.
%
%      H = SPECT returns the handle to a new SPECT or the handle to
%      the existing singleton*.
%
%      SPECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPECT.M with the given input arguments.
%
%      SPECT('Property','Value',...) creates a new SPECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before spect_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to spect_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help spect

% Last Modified by GUIDE v2.5 08-Feb-2013 20:38:20




%##########################################################################
%Some global parameters used for the plot
 global gamma B0 water_freq bandwidth readout_time water_peak 
 gamma=42.576; %MHz/T;
 B0= 7; %T
 water_freq= gamma * B0; %MHz
 bandwidth=4000; %Hz;
 readout_time=1024 ;%ms
 water_peak=4.8; %ppm
 
%##########################################################################

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @spect_OpeningFcn, ...
                   'gui_OutputFcn',  @spect_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before spect is made visible.
function spect_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to spect (see VARARGIN)

% Choose default command line output for spect

img=imread('Image_Group.png');
figure(get(handles.axes1,'parent'))
axes(handles.axes1);
imagesc(img);
set(handles.axes1,'Visible','off');

handles.output = hObject;


% Update handles structure
guidata(hObject, handles);


% UIWAIT makes spect wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = spect_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;








%##########################################################################
% --- Executes on button press in button_data.
function button_data_Callback(hObject, eventdata, handles)
% hObject    handle to button_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global MR_objects 
%--------------------------------------------------------------------------
% Select the data
[FileName,PathName,FilterIndex] = uigetfile(...
{ '*.raw', 'RAW files (*.raw)'},...
'Please select the RAW data files',...
'MultiSelect','on');

set(handles.listbox_data,'Enable','inactive');    

%Update the listbox_data
set(handles.listbox_data,'String',FileName);

% If only one raw file is given make FileName to be a cell
if ~iscell(FileName)
  FileName=cellstr(FileName);
end
   


% Create MRecon objects

set(handles.text_status,'String','Loading data... Please wait');
pause(1);

n_raw_files=length(FileName);   

MR_objects= cell(n_raw_files,1) ;

for i=1:n_raw_files;
    
    raw_path=strcat(PathName,FileName{i});
    
    tmp_object=MRecon_spectro(raw_path);
    tmp_object.ReadData;
    tmp_object.SortData;
    
    MR_objects{i}=tmp_object;
    
end
    
set(handles.listbox_data,'Enable','on');    
    
set(handles.text_status,'String','Loading data... Finished');
pause(1)
 set(handles.text_status,'String','');
 
 
 
 
%##########################################################################
% --- Executes on selection change in listbox_data.
function listbox_data_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global MR_objects selected_data selected_value;
contents = cellstr(get(hObject,'String')); 
selected_value=get(hObject,'Value');
selected_file=contents{selected_value}; 

set(handles.text_status,'String',selected_file);
selected_data =MR_objects{selected_value};


%------------------------Static Texts-------------------------------------
global n_coils n_dynamics n_averages n_timepoints 
update_texts(handles);


%----------------------Slide bars & Edit texts-----------------------------
set(handles.edit_coils,'Visible','on');
set(handles.edit_dynamics,'Visible','on');
set(handles.edit_averages,'Visible','on');

set(handles.slider_coils,'Max',n_coils);
set(handles.slider_dynamics,'Max',n_dynamics);
set(handles.slider_averages,'Max',n_averages);

if n_coils~=1
    set(handles.slider_coils,'sliderstep',[1 1]/(n_coils-1));
end

if n_dynamics~=1
    set(handles.slider_dynamics,'sliderstep',[1 1]/(n_dynamics-1));
end

if n_averages~=1
    set(handles.slider_averages,'sliderstep',[1 1]/(n_averages-1));
end






%##########################################################################
function update_texts(handles);
global n_coils n_dynamics n_averages n_timepoints selected_data 
n_coils= size(selected_data.Data{1},selected_data.coil_dim);
n_dynamics= size(selected_data.Data{1},selected_data.dyn_dim);
n_averages= size(selected_data.Data{1},selected_data.meas_dim);
n_timepoints= size(selected_data.Data{1},selected_data.kx_dim);


set(handles.text_time,'String',sprintf('# timepoints= %i',n_timepoints));
set(handles.text_coils,'String',sprintf('# coils= %i',n_coils));
set(handles.text_dynamics,'String',sprintf('# dynamics= %i',n_dynamics));
set(handles.text_averages,'String',sprintf('# averages= %i',n_averages));









%##########################################################################
% --- Executes during object creation, after setting all properties.
function listbox_data_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end









%###########SLIDERS########################################################
% --- Executes on slider movement.
function slider_dynamics_Callback(hObject, eventdata, handles)
% hObject    handle to slider_dynamics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

value_dynamic=get(hObject,'Value');
set(handles.edit_dynamics,'String',num2str(value_dynamic));
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
plot_fid(handles)




%##########################################################################
% --- Executes on slider movement.
function slider_coils_Callback(hObject, eventdata, handles)
% hObject    handle to slider_coils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

value_coil=get(hObject,'Value');
set(handles.edit_coils,'String',num2str(value_coil));
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
plot_fid(handles)






%#########################################################################
% --- Executes on slider movement.
function slider_averages_Callback(hObject, eventdata, handles)
% hObject    handle to slider_averages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

value_average=get(hObject,'Value');
set(handles.edit_averages,'String',num2str(value_average));
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


plot_fid(handles)



% --- Executes during object creation, after setting all properties.
function slider_averages_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_averages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end




function edit_coils_Callback(hObject, eventdata, handles)
% hObject    handle to edit_coils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global n_coils;
value_coil= str2double(get(hObject,'String'));

if value_coil> n_coils 
    set(handles.text_status,'String','Please type a valid # of coil')
end

set(handles.slider_coils,'Value',value_coil);
plot_fid(handles)



%##########################################################################
function edit_averages_Callback(hObject, eventdata, handles)
% hObject    handle to edit_averages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global n_averages;
value_average= str2double(get(hObject,'String'));

if value_average > n_averages 
    set(handles.text_status,'String','Please type a valid # of average');
end

set(handles.slider_averages,'Value',value_average);
plot_fid(handles)




%##########################################################################
function edit_dynamics_Callback(hObject, eventdata, handles)
% hObject    handle to edit_dynamics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global n_dynamics;
value_dynamic= str2double(get(hObject,'String'));

if value_dynamic> n_dynamics
    set(handles.text_status,'String','Please type a valid # of dynamic');
end

set(handles.slider_dynamics,'Value',value_dynamic);
plot_fid(handles)




%#########################BUTTON_PLOT######################################
% --- Executes on button press in button_plot.
function button_plot_Callback(hObject, eventdata, handles)
% hObject    handle to button_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global is_pressed
is_pressed= true;
plot_fid(handles)





%##########################################################################
function plot_fid(handles)

global is_pressed
if is_pressed
    
    global n_timepoints water_peak water_freq selected_data
    coil=get(handles.slider_coils,'Value');
    dynamic=get(handles.slider_dynamics,'Value');
    average=get(handles.slider_averages,'Value');
    
    style=get(handles.popupmenu_plot,'String');
    style=style{get(handles.popupmenu_plot,'Value')};
    bandwidth=str2double( get(handles.edit_bandwidth,'String'));
    readout=str2double( get(handles.edit_readout,'String'));
    frequency_range=linspace(-n_timepoints*1000/(2*readout),n_timepoints*1000/(2*readout),n_timepoints);
     zero_point=length(frequency_range)/2;
    fids=squeeze(selected_data.Data{1});
    
 
    if coil~=1
    fid=fids(:,coil,1,dynamic,average);
    else
    fid=fids(:,1,dynamic,average);
    end
    
    ffts=fft(fid);
    
    if strcmp(style,'ppm')
        
        figure(get(handles.axes2,'parent'))
        axes(handles.axes2);
        
          
        plot(...
            frequency_range(zero_point-bandwidth/2:zero_point+bandwidth/2-1)/water_freq+water_peak,...
            fftshift([ffts(1:bandwidth/2) ;ffts(end-bandwidth/2:end-1)])...
            )
         xlabel('ppm, water peak=4.8 ppm')
    else
        
         
          plot(...
            frequency_range(zero_point-bandwidth/2:zero_point+bandwidth/2-1),...
            fftshift([ffts(1:bandwidth/2) ;ffts(end-bandwidth/2:end-1)])...
             )
         xlabel('Frequencies')
        
    end
    
end
    
 
    

%##########################################################################

% --- Executes on button press in pushbutton_process.
function pushbutton_process_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_process (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


global MR_objects selected_value selected_data
set(handles.text_status,'String','Data Processing... Please wait');

MR_objects{selected_value}=process(handles,selected_data);

set(handles.text_status,'String','Data Processing... Finished');
pause(1);
update_texts(handles);



























%##########################################################################
%--------------------------------------------------------------------------
%##########################################################################




% --- Executes during object creation, after setting all properties.
function edit_coils_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_coils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_plot.

function popupmenu_plot_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_plot contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_plot


% --- Executes during object creation, after setting all properties.
function edit_averages_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_averages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function edit_dynamics_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dynamics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes during object creation, after setting all properties.
function popupmenu_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function slider_coils_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_coils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function edit_bandwidth_Callback(hObject, eventdata, handles)
% hObject    handle to edit_bandwidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_bandwidth as text
%        str2double(get(hObject,'String')) returns contents of edit_bandwidth as a double


% --- Executes during object creation, after setting all properties.
function edit_bandwidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_bandwidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_readout_Callback(hObject, eventdata, handles)
% hObject    handle to edit_readout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_readout as text
%        str2double(get(hObject,'String')) returns contents of edit_readout as a double


% --- Executes during object creation, after setting all properties.
function edit_readout_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_readout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function slider_dynamics_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_dynamics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in checkbox_DcOffset.
function checkbox_DcOffset_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_DcOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_DcOffset


% --- Executes on button press in checkbox_CombCoils.
function checkbox_CombCoils_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_CombCoils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_CombCoils


% --- Executes on selection change in popupmenu_CombCoils.
function popupmenu_CombCoils_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_CombCoils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_CombCoils contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_CombCoils


% --- Executes during object creation, after setting all properties.
function popupmenu_CombCoils_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_CombCoils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_Downsampling.
function checkbox_Downsampling_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_Downsampling (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_Downsampling


% --- Executes on selection change in popupmenu_Downsampling_times.
function popupmenu_Downsampling_times_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_Downsampling_times (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_Downsampling_times contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_Downsampling_times


% --- Executes during object creation, after setting all properties.
function popupmenu_Downsampling_times_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_Downsampling_times (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_EddyCurrents.
function checkbox_EddyCurrents_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_EddyCurrents (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_EddyCurrents





% --- Executes on selection change in popupmenu_Downsampling_alg.
function popupmenu_Downsampling_alg_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_Downsampling_alg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_Downsampling_alg contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_Downsampling_alg


% --- Executes during object creation, after setting all properties.
function popupmenu_Downsampling_alg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_Downsampling_alg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
