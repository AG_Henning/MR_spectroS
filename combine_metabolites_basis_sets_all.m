
function combine_metabolites_basis_sets_all()

plotBasis = false;

% namings = { ...
% {'Ascorbic_acid_de_Graaf';'Asc';1}; ...
% {'Cr_singlet_tot_3.028';'Cr_singlet_tot_3.028';3}; ...
% {'Cr_singlet_tot_3.925';'Cr_singlet_tot_3.925';2}; ...
% {'GPC-gp';'GPC';1}; ...
% {'GPC-pcholine1';'GPC';1}; ...
% {'GPC-trimethyl_to_use';'GPC';9}; ...
% {'GSH_no_NH_cysteine_de_Graaf';'GSH_no_DF';1}; ...
% {'GSH_no_NH_glutamate_de_Graaf';'GSH_no_DF';1}; ...
% {'GSH_no_NH_glycine_de_Graaf_corrected';'GSH_no_DF';1}; ... %
% ...{'GSH_no_NH_glycine_de_Graaf';'GSH_no_DF';1}; ... %
% {'Glc_alpha_de_Graaf';'Glc';0.36}; ...
% {'Glc_beta_de_Graaf';'Glc';0.64}; ...
% {'NAAG-acetyl_to_use';'NAAG';3}; ...
% {'NAAG-aspartyl_to_use';'NAAG';1}; ...
% {'NAAG-glutamate_moiety_to_use';'NAAG';1}; ...
% {'NAA_1';'NAA';1}; ...
% {'NAA_2';'NAA';3}; ...
% {'NAA_2';'NAA_ace';3}; ...
% {'alanine_to_use';'Ala';1}; ...
% {'aspartate';'Asp';1}; ...
% {'atp-adenosine_de_Graaf_no_NH';'ATP';1}; ...
% {'atp-ribose_deGraaf';'ATP';1}; ...
% {'choline-methylene_to_use';'Cho';1}; ...
% {'choline-trimethyl_to_use';'Cho';9}; ...
% {'creatine_1';'Cr';3}; ...
% {'creatine_2';'Cr';1}; ...
% {'gaba_to_use';'GABA';1}; ...
% {'glutamate';'Glu';1}; ...
% {'glutamine';'Gln';1}; ...
% {'glutathione-1_to_use';'GSH';1}; ...
% {'glutathione-2_to_use';'GSH';1}; ...
% {'glutathione-3_to_use';'GSH';1}; ...
% {'glycine';'Glyc';1}; ...
% {'lactate';'Lac';1}; ...
% {'myo-inositol';'mI';1}; ...
% {'phosphocreatine_1';'PCr';3}; ...
% {'phosphocreatine_2';'PCr';1}; ...
% {'phosphorylcholine-notrimethyl_to_use';'PCh';1}; ...
% {'phosphorylcholine-trimethyl_to_use';'PCh';9}; ...
% {'phosphorylethanolamine';'PE';1}; ...
% {'scyllo-to_use';'Scyllo';6}; ...
% {'taurine';'Tau';1}; ...
% ... tCho:  GPC 1mM	PCh	0.6mM (values from de Graaf)
% {'phosphorylcholine-notrimethyl_to_use';'tCho';0.375}; ...
% {'phosphorylcholine-trimethyl_to_use';'tCho';3.375}; ...
% {'GPC-gp';'tCho';0.625}; ...
% {'GPC-pcholine1';'tCho';0.625}; ...
% {'GPC-trimethyl_to_use';'tCho';5.625}; ...
% ... tCho+PE:  GPC 1mM	PCh	0.6mM PE 1.5mM (values from de Graaf)
% {'phosphorylcholine-notrimethyl_to_use';'tCho_PE';0.193548387}; ...
% {'phosphorylcholine-trimethyl_to_use';'tCho_PE';1.741935484}; ...
% {'GPC-gp';'tCho_PE';0.322580645}; ...
% {'GPC-pcholine1';'tCho_PE';0.322580645}; ...
% {'GPC-trimethyl_to_use';'tCho_PE';2.903225806}; ...
% {'phosphorylethanolamine';'tCho_PE';0.483870968}; ...
% };
namings = { ...
{'Ascorbic_acid_de_Graaf';'Asc';1}; ...
{'Cr_singlet_tot_3.028';'Cr_singlet_tot_3.028';3}; ...
{'Cr_singlet_tot_3.925';'Cr_singlet_tot_3.925';2}; ...
{'GPC-gp';'GPC';1}; ...
{'GPC-pcholine1_de_Graaf';'GPC';1}; ...
{'GPC-trimethyl_to_use';'GPC';9}; ...
% {'GSH_no_NH_cysteine_de_Graaf';'GSH_no_DF';1}; ...
% {'GSH_no_NH_glutamate_de_Graaf';'GSH_no_DF';1}; ...
% {'GSH_no_NH_glycine_de_Graaf_corrected';'GSH_no_DF';1}; ... %
% ...{'GSH_no_NH_glycine_de_Graaf';'GSH_no_DF';1}; ... %
{'Glc_alpha_de_Graaf';'Glc';0.36}; ...
{'Glc_beta_de_Graaf';'Glc';0.64}; ...
{'NAAG-acetyl_to_use';'NAAG';3}; ...
{'NAAG-aspartyl_to_use';'NAAG';1}; ...
{'NAAG-glutamate_moiety_de_Graaf';'NAAG';1}; ...
{'NAAG-acetyl_to_use';'NAAG_noDF';3}; ...
{'NAAG-aspartyl_to_use';'NAAG_noDF';1}; ...
{'NAAG-glutamate_moiety_de_Graaf_noDF';'NAAG_noDF';1}; ...
{'NAA_asp_de_Graaf';'NAA';1}; ...
{'NAA_2';'NAA';3}; ...
{'NAA_2';'NAA_ace';3}; ...
% {'alanine_to_use';'Ala';1}; ...
{'aspartate_de_Graaf';'Asp';1}; ...
{'atp-adenosine_de_Graaf_no_NH';'ATP';1}; ...
{'atp-ribose_deGraaf';'ATP';1}; ...
% {'choline-methylene_to_use';'Cho';1}; ...
% {'choline-trimethyl_to_use';'Cho';9}; ...
{'creatine_1';'Cr';3}; ...
{'creatine_2';'Cr';1}; ...
{'GABA_de_Graaf';'GABA';1}; ...
{'glutamate_de_Graaf';'Glu';1}; ...
{'glutamine_de_Graaf';'Gln';1}; ...
{'glutathione-1_to_use';'GSH';1}; ...
{'glutathione-2_to_use';'GSH';1}; ...
{'glutathione-3_de_Graaf';'GSH';1}; ...
{'glycine_de_Graaf';'Glyc';1}; ...
{'lactate_de_Graaf';'Lac';1}; ...
{'myo-inositol';'mI';1}; ...
{'phosphocreatine_1';'PCr';3}; ...
{'phosphocreatine_2';'PCr';1}; ...
{'phosphorylcholine-notrimethyl_de_Graaf';'PCh';1}; ...
{'phosphorylcholine-trimethyl_de_Graaf';'PCh';9}; ...
{'phosphorylethanolamine_de_Graaf';'PE';1}; ...
{'scyllo-to_use';'Scyllo';6}; ...
{'taurine';'Tau';1}; ...
... tCho:  GPC 1mM	PCh	0.6mM (values from de Graaf)
{'phosphorylcholine-notrimethyl_de_Graaf';'tCho';0.375}; ...
{'phosphorylcholine-trimethyl_de_Graaf';'tCho';3.375}; ...
{'GPC-gp';'tCho';0.625}; ...
{'GPC-pcholine1_de_Graaf';'tCho';0.625}; ...
{'GPC-trimethyl_to_use';'tCho';5.625}; ...
... tCho+PE:  GPC 1mM	PCh	0.6mM PE 1.5mM (values from de Graaf)
{'phosphorylcholine-notrimethyl_de_Graaf';'tCho_PE';0.193548387}; ...
{'phosphorylcholine-trimethyl_de_Graaf';'tCho_PE';1.741935484}; ...
{'GPC-gp';'tCho_PE';0.322580645}; ...
{'GPC-pcholine1_de_Graaf';'tCho_PE';0.322580645}; ...
{'GPC-trimethyl_to_use';'tCho_PE';2.903225806}; ...
{'phosphorylethanolamine_de_Graaf';'tCho_PE';0.483870968}; ...
};

numberOfMet = length(namings);
abbreviations = cell(numberOfMet,1);
metaboliteNames = cell(numberOfMet,1);
scalingFactors = cell(numberOfMet,1);
for iMet = 1:numberOfMet
    abbreviations{iMet}=namings{iMet}{2};
    metaboliteNames{iMet}=namings{iMet}{1};
    scalingFactors{iMet}=namings{iMet}{3};
end

abbreviationsUnique = unique(abbreviations);

pathName = 'DF data path';
sampleCriteria = {};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
filePathBase = [localFilePathBase 'Basis_sets\sLASER_de_Graaf_7ppm\TE'];
orderTEs = {'24' '32' '40' '52' '60'};
filePaths = strcat(filePathBase, orderTEs , '\');

for indexTE = 1 : length(orderTEs)
    filePath = filePaths{indexTE};
    for indexMetabolite = 1 : length(abbreviationsUnique)
        combined_metabolite_name = abbreviationsUnique{indexMetabolite};
        indeces = strcmp(abbreviations,combined_metabolite_name);
        numOfMetToCombine = sum(indeces);
        metabolitesToMix = metaboliteNames(indeces);
        factorsToMix = scalingFactors(indeces);
        %
        dataMix           = cell(1,numOfMetToCombine);
        metaboliteNameMix = cell(1,numOfMetToCombine);
        singletPresentMix = cell(1,numOfMetToCombine);

        for indexMetabolitesToMix =  1:numOfMetToCombine
            [dataMix{indexMetabolitesToMix}, bandwidth, scanFrequency, metaboliteNameMix{indexMetabolitesToMix}, ...
                singletPresentMix{indexMetabolitesToMix}] = ...
                ImportLCModelBasis(filePath, metabolitesToMix{indexMetabolitesToMix}, ...
                plotBasis);
        end
        %combine
        combinedData = dataMix{1} * factorsToMix{1};
        for indexMetabolitesToMix =  2:numOfMetToCombine
            % average the contributions. Don't worry about the
            % ref. singlet, it does not influence the fitting
            combinedData = combinedData + factorsToMix{indexMetabolitesToMix} * ...
                dataMix{indexMetabolitesToMix};
        end
        
        dwellTimeMs = 1/bandwidth * 1e3;
        addSinglet = false;

        ExportLCModelBasis(combinedData', dwellTimeMs, scanFrequency, filePath, ...
                combined_metabolite_name, combined_metabolite_name, addSinglet);
    end
end
