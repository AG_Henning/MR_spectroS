function [this] = Phase0_correction_31P(this)
oldData                 = this.Data{1};
newDataSize             = size(oldData);
newData                 = zeros(newDataSize);
nChannels = size(this.Data{1},this.coil_dim);

sing = this.Parameter.PhaseCorr31PSettings.sing;
% sing = [0]; 
% sing = [2.3845]; 
maxPhasePc0Deg = 180;
bw = this.Parameter.Headers.Bandwidth_Hz/162;
verbose = true;
detect = 1;

phases = zeros(1,nChannels);
for indexOfChannel=1:nChannels
    currentDataToPhase =oldData(:,1,1,indexOfChannel);
    phases(indexOfChannel) = pc0_correction_31P(currentDataToPhase, sing, maxPhasePc0Deg, bw, verbose, detect);

    newData(:,1,1,indexOfChannel) = currentDataToPhase .*exp(1i*-phases(indexOfChannel) *pi/180);
end

this.Parameter.PhaseCorr31PSettings.phases = phases;
this.Data{1}                                = newData;

clear oldData sing maxPhasePc0Deg bw verbose detect newData