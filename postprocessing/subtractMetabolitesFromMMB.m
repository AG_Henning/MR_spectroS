function subtractMetabolitesFromMMB()

pathName = 'MM T1 data path';
sampleCriteria = {'Output_GM_FinalCorrected'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
LCModelOutputPath = [localFilePathBase, 'Output_GM_FinalCorrected/'];

outputFileNameBase = 'Summed_Averaged_TI1_';
fileNameBaseWithoutMetabolite = 'MMB_without_metabolite_TI1_';
orderTEs = {'1900'}; %'2360'  '2000' '1900' '1800'
extensionCoord = '.coord';
extensionMat = '.mat';
metabolitesToSubtract = {'Cr39', 'Cr30', 'NAA', 'Glx'};
doNullingOfDownField = true;
LCModelTableFitsCoord = strcat(outputFileNameBase, orderTEs, extensionCoord);
reconstructedSpectra = strcat(localFilePathBase, outputFileNameBase, orderTEs, extensionMat);
fileNameWithoutMetabolite = strcat(localFilePathBase, fileNameBaseWithoutMetabolite, orderTEs, extensionMat);
fileNameLCModelExport = strcat(fileNameBaseWithoutMetabolite, orderTEs);

phase90 = -90 * pi/180;
zeroFillFactor = 2;

numberOfTEs = length(orderTEs);
%plot configs
offsetPlot = -7e-5;
FontSize = 14;
LineWidth = 1.5;

for indexTE = 1:numberOfTEs
    currentReconstructedSpectra = reconstructedSpectra{indexTE};
    load(currentReconstructedSpectra, 'summedSpectraTI');
    currentLCModelCoordFit      = LCModelTableFitsCoord{indexTE};
    [nOfPoints, ppmVector, phasedData, fitData, baselineData, residualData, ...
        metaboliteNames, metaboliteData, tableConcentrations, ppmStart, ppmEnd, ...
        scanFrequency,  frequencyShift, ppmGap] = ...
        extractFits(LCModelOutputPath,currentLCModelCoordFit);
  ppmGap = 4.1  
    % align the two spectra
    fid = summedSpectraTI.Data{1};
    spectrum = fftshift(fft(fid(:,1,1,1,1,1,1,1,1,1,1,1),size(fid,1)*zeroFillFactor));
    ppmSpectrum = summedSpectraTI.getPpmVector(zeroFillFactor);
    
    [~, maxPpmValueSpectrum] = getMaxPeak(ppmSpectrum', real(spectrum), 3.925, 0.1);
    [~, maxPpmValueLCModel] = getMaxPeak(ppmVector, phasedData, 3.925, 0.1);
    
    frequencyShiftDiff = maxPpmValueSpectrum - maxPpmValueLCModel;
    summedSpectraTI = summedSpectraTI.ApplyFrequencyShift(frequencyShiftDiff);
    %get the aligned reconstructed spectra
    fid = summedSpectraTI.Data{1};
    spectrum = fftshift(fft(fid(:,1,1,1,1,1,1,1,1,1,1,1),size(fid,1)*zeroFillFactor));
    % max of the reconstructed spectrum
    if isempty(ppmGap)
        ppmSpectrumMask  = (ppmSpectrum < ppmStart) & (ppmSpectrum > ppmEnd);
        ppmMetaboliteMask  = (ppmVector < ppmStart) & (ppmVector > ppmEnd);
    else
        ppmSpectrumMask  = (ppmSpectrum < ppmGap) & (ppmSpectrum > ppmEnd);
        ppmMetaboliteMask  = (ppmVector < ppmGap) & (ppmVector > ppmEnd);
    end
    maxSpectrum = max(real(spectrum(ppmSpectrumMask)));
    % max of the LCModel fit
    maxPhasedData = max(phasedData);
    % calculate scaling
    scalingFactor = maxSpectrum/maxPhasedData;
    oldSpectrum = spectrum;
    for indexMetabolite = 1:length(metabolitesToSubtract)
        metaboliteToSubtract = metabolitesToSubtract{indexMetabolite};
        indexMetaboliteToSubtract = find(strcmp(metaboliteNames,metaboliteToSubtract));
        if isempty(indexMetaboliteToSubtract)
            error('Metabolite %s not found in fit',metaboliteToSubtract);
        end
    
        spectrumToSubtract = metaboliteData{indexMetaboliteToSubtract}.metaboliteSpectrum(ppmMetaboliteMask);
        spectrumToSubtract(1) = 0;
        %eliminate first point, which is anyway an error; Also scale it
        spectrumToSubtractFlipped = flipud(spectrumToSubtract).*scalingFactor;
        
        spectrumToSubtractReal = zeros(size(oldSpectrum));
        spectrumToSubtractReal(ppmSpectrumMask) = spectrumToSubtractReal(ppmSpectrumMask) + spectrumToSubtractFlipped;
        fidToSubtract = ifft(ifftshift(complex(spectrumToSubtractReal)));
        fidToSubtract(length(fidToSubtract)/2:end)=0;
        spectrumToSubtractPhased90 = fftshift(fft(fidToSubtract .* exp(1i*phase90))).*2; %multiply by 2 to account for nulling the half of the FID
        spectrumToSubtractImag = real(spectrumToSubtractPhased90);
        newSpectrumReal = real(oldSpectrum) - spectrumToSubtractReal;
        newSpectrumImag = imag(oldSpectrum) - spectrumToSubtractImag;
        newSpectrum = complex(newSpectrumReal, newSpectrumImag);
        
        if (doNullingOfDownField)
            newSpectrum(length(newSpectrum)/2:end) = 0;
        end
        
        oldSpectrum = newSpectrum;
    end
    
    figure
    plot(ppmVector, phasedData.*scalingFactor)
    hold on
    plot(ppmSpectrum, real(spectrum))
    hold on
    plot(ppmSpectrum, real(newSpectrum),'k')
    xlim([0 4.2])
    title(sprintf('Real MMB TE %s', orderTEs{indexTE}))
    
    figure(101)
    hold on
    p1 = plot(ppmSpectrum, real(spectrum)+ indexTE * offsetPlot, 'r');
    hold on
    p2 = plot(ppmSpectrum, real(newSpectrum)+ indexTE * offsetPlot,'k');
    xlim([0 4.2])
    xlabel('[ppm]');
    ylim([(indexTE+1) * offsetPlot-offsetPlot*0.5 5e-5]);
    set(gca,'xDir','reverse')
    set(gca,'ytick',[]);
    set(gca,'fontsize',FontSize);
    set(p1,'LineWidth',LineWidth);
    set(p2,'LineWidth',LineWidth);
    title('Macromolecular baseline with and w/o the Cr peak')    

%     figure
%     plot(ppmSpectrum, imag(spectrum))
%     hold on
%     plot(ppmSpectrum, imag(newSpectrum),'k')
%     xlim([0 4.2])
%     title(sprintf('Imag MMB TE %s', orderTEs{indexTE}))
    
    MMB_without_metabolite = summedSpectraTI;
    newFid = ifft(ifftshift(newSpectrum));
    MMB_without_metabolite.Data{1} = newFid;
    
    save(fileNameWithoutMetabolite{indexTE},'MMB_without_metabolite');
    %TODO check which of the two you need
%     dwellTimeMs = MMB_without_metabolite.Parameter.Headers.DwellTimeSig_ns * 1e-6;
%     ExportLCModelBasis(MMB_without_metabolite.Data{1}', dwellTimeMs, scanFrequency * 1e6, ...
%         localFilePathBase, fileNameLCModelExport{indexTE}, 'Leu', 'Yes');
    MMB_without_metabolite.ExportLcmRaw(localFilePathBase, fileNameLCModelExport{indexTE}, 'Yes', 'Yes');
end
end

function [maxValue, maxPpmValue] = getMaxPeak(ppmVector, spectrum, peakPpm, searchAreaPpm)
    minPpm = peakPpm - searchAreaPpm;
    maxPpm = peakPpm + searchAreaPpm;
    ppmMask = (ppmVector > minPpm) & (ppmVector < maxPpm);
    tempSpectrum = spectrum .* ppmMask;
    [maxValue, maxIndex] = max(real(tempSpectrum));
    maxPpmValue = ppmVector(maxIndex);
end