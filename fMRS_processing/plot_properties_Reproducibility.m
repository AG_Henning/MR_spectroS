function plot_properties_Reproducibility()
clear; clc; close all;

pathName = 'fMRS TimeCourse data path';
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
visualType = 'Stim';      %'Stim', 'Rest'
BOLDType = '_woBOLDCorr';  %only _woBOLDCorr reasonable since block wise fitting for metabolite time courses
MMBType = '_MMBSummed_v2';    %_MMBIndFiltered, _MMBSummed, _MMBInd, _MMBSimulated
%     nBlocks = 5;
nBlocks = 10;

BlockNo = cell(1,nBlocks);
for k = 1:nBlocks
    BlockNo{k} = strcat('_Block', num2str(k));
end

% subjects = {'2823';};
subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
    '4085';'3333';'4012';'4085';'6524'};
% prefixes = {'2020-02-18_';};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
    '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
    '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
suffixFolder = '_fMRS';
excludeSubjects = {'5166';'9810';'5269'};

if exist('excludeSubjects')
    n = length(subjects);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(subjects(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    subjects([excludeIdx],:) = [];
    prefixes([excludeIdx],:) = [];
end

subjectsPath = strcat(prefixes, subjects, suffixFolder);

%load Rest data per block, then mean, std, plot
for indexCurrentSubject = 1:length(subjects)
    currentDataFilePath = [localFilePathBase,subjectsPath{indexCurrentSubject}];
    for nBl = 1:nBlocks
        %         currentData = load(strcat(currentDataFilePath,'\',subjects{indexCurrentSubject},'_',visualType,BOLDType,BlockNo{nBl}));
        %         fields = fieldnames(currentData);
        %         currentData = currentData.(fields{1});
        %hopefully saved as 'this'
        load(strcat(currentDataFilePath,'\',subjects{indexCurrentSubject},'_',visualType,BOLDType,BlockNo{nBl}));
        Data_temp = squeeze(real(fftshift(fft(this.Data{1}))));
        Data(indexCurrentSubject,nBl,:,:) = squeeze(real(fftshift(fft(this.Data{1}))));  %Subjects x Blocks x Data x Water/Met
    end
    DataBlockMean_Met(indexCurrentSubject,:) = squeeze(mean(Data(indexCurrentSubject,:,:,2),2));
    DataBlockStd_Met(indexCurrentSubject,:) = squeeze(std(Data(indexCurrentSubject,:,:,2)));
    DataBlockMean_H2O(indexCurrentSubject,:) = squeeze(mean(Data(indexCurrentSubject,:,:,1),2));
    DataBlockStd_H2O(indexCurrentSubject,:) = squeeze(std(Data(indexCurrentSubject,:,:,1)));
end
ppmvector = this.getPpmVector();
colors = {[0 0.7 0]; [0 0.4 0.8]; [0.8 0 0.8]; [0.8 0.4 0];[0 0.5 0.5]; [0.4 0 0.8]; [1 0 0]; [0 0.6 0.3]; [1 0 0.5]; [0 0 1];};

for indexCurrentSubject = 1:length(subjects)
    lo = (DataBlockMean_Met(indexCurrentSubject,:) - DataBlockStd_Met(indexCurrentSubject,:)) + 0.01 * (indexCurrentSubject-1); hi = DataBlockMean_Met(indexCurrentSubject,:) + DataBlockStd_Met(indexCurrentSubject,:) + 0.01 * (indexCurrentSubject-1);
%     figure
    hp = patch([ppmvector ppmvector(end:-1:1) ppmvector(1)], [lo hi(end:-1:1) lo(1)], 'r');
    hold on;
    hl = line(ppmvector,DataBlockMean_Met(indexCurrentSubject,:) + 0.01 * (indexCurrentSubject-1));
    set(hp, 'facecolor', colors{indexCurrentSubject},'FaceAlpha',.3, 'edgecolor', colors{indexCurrentSubject},'EdgeAlpha',.2);
    set(hl, 'color', colors{indexCurrentSubject});
    set(gca,'xdir','reverse')
    xlim([0.6 8.5]); xlabel 'f / ppm'
    title(['Reproducibility ',visualType])
end
figPath2save = fullfile(localFilePathBase, 'Output_v2_MMBSummed\Reproducibility');
fig2save = [figPath2save, '\Reproducibility_all_',visualType,'.fig'];
% fig2save = [figPath2save, '\Reproducibility_first3_',visualType,'.fig'];
% fig2save = [figPath2save, '\Reproducibility_second3_',visualType,'.fig'];
% fig2save = [figPath2save, '\Reproducibility_last4_',visualType,'.fig'];
savefig(fig2save)
end