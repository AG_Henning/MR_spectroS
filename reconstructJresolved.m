function [a] = reconstructJresolved(fid_id)
a = MR_spectroS(fid_id,4);
trunc_ms = 250;
truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
a = a.Truncate(truncPoint);

TEsteps = 25;
a.Parameter.FreqAlignSettings.indecesOfInterest = TEsteps;
a = a.FrequencyAlign;
a.Parameter.AverageSettings.nBlocks = TEsteps;
a = a.AverageData;
%% Eddy current correction
[FileName,PathName] = uigetfile({'*.dat'},'Select the Water Suppressed data, cancel if Eddy Current Correction with own phase');
if (FileName ~= 0)
    path = strcat(PathName,FileName);
    tmpData      = mapVBVD(path);
    %############################################################
    % Order of raw data:
    %  1) Columns
    %  2) Channels/Coils
    %  3) Lines
    %  4) Partitions
    %  5) Slices
    %  6) Averages
    %  7) (Cardiac-) Phases
    %  8) Contrasts/Echoes
    %  9) Repetitions
    % 10) Sets
    %#############################################################
    tmpWaterReferenceData = tmpData.image(''); %Read the data
    waterReferenceData = double(permute(tmpWaterReferenceData,[1 3 4 2 5 7 8 11 12 9 10 6]));
    clear tmpData tmpWaterReferenceData;
    a = a.EddyCurrentCorrection(waterReferenceData);
else %do Eddy current corection without water reference data
    a = a.EddyCurrentCorrection();
end
%%
fprintf('Coil Combine starts... Please wait\n');
a = a.CombineCoils;
clc
fprintf('Coil Combine...FINISHED\n')
clc   
fprintf('HSVD starts... Please wait\n');
a.Parameter.HsvdSettings.bound = [-100 100];
a.Parameter.HsvdSettings.n = 1024;
[~, a] = a.Hsvd;
clc
fprintf('HSVD...FINISHED')    

%saving the file
%addSinglet0ppm = true;
%TE = '24';
%filename = ['sLASER_TE_' TE];
% a.ExportLcmRaw('',filename, addSinglet0ppm);
%save the .mat file
save([FileName '.mat'], 'a');

%save the .spar, .sdat file
%sparfile           = 'sparfile.spar';
%par                = read_spar(sparfile);
%MR_spectroSObj     = handles. MRSpectroMatrix.getCurrent;
%filename           = MR_spectroSObj.Parameter.Filename;
%filename           = filename(1:end-4);
sparfile           = 'sparfile.spar';
par                = read_spar(sparfile);
%this               = MR_spectroSObj;
%a               = this.Data{1};
data               = a.Data{1};
if a.Parameter.ReconFlags.isJresolved == false
    par.samples                = size(data,a.kx_dim);
    par.averages               = size(data,a.meas_dim);
    par.dim1_pnts              = size(data,a.kx_dim);
    par.rows                   = 1;
    par.dim1_low_val           = 1;
    data                       = a(:,1);
    data                       = squeeze(data);
else % J resolved
    par.samples                = size(data,a.kx_dim);
    par.averages               = 1;
    par.dim1_pnts              = size(data,a.kx_dim);
    par.rows                   = size(data,a.meas_dim);
    par.dim1_low_val           = 1;
    data                       = conj(squeeze(data)');
end
par.echo_time              = a.Parameter.Headers.TE_us/1000;
    
par.spectrum_echo_time     = a.Parameter.Headers.TE_us/1000;
par.synthesizer_frequency  = a.Parameter.Headers.ScanFrequency;
par.sample_frequency       = a.Parameter.Headers.Bandwidth_Hz;



write_sdat(strcat(FileName,'.sdat'),a.Data{1},par);


% trunc_ms = 250;
% truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
% a = a.Truncate(truncPoint);
end