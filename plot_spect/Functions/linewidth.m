function lineWidthValue = linewidth( data, selectedProcessingValues,bandwidth)

peakRange     = get(selectedProcessingValues, 'PeakRange');
fftsReal      = real(fftshift(fft(data))); 
peak          = fftsReal(peakRange(1):peakRange(2));

interpFactor  = 100; %
indeces       = [];
i = 1;

while (length(indeces)<2 && i <4)
   
   peakTmp         = interp(peak,interpFactor*i);
   [~,  maxPos] = max(abs(peakTmp));
   normValues   = peakTmp/(peakTmp(maxPos)/2);
   indeces      = find(normValues> 0.995 & normValues< 1.005);
   i            = i+1;
end
   
   
   interpFactor    = interpFactor*(i-1);
   freqResolution  = bandwidth/(length(data)*interpFactor);
   try 
    lineWidthValue  = (indeces(end) - indeces(1))*freqResolution;
   catch err
       lineWidthValue = [];
   end
