function this = SplineSmoothing(this, smoothingFactor, doPlotting)

oldData = this.Data{1};

if ~exist('doPlotting', 'var')
    doPlotting = false;
end

newData     = zeros(size(oldData));
nAverages   = size(oldData,this.meas_dim);
nCoils      = size(oldData, this.coil_dim);
nMix        = size(this.Data{1}, this.mix_dim);

 fprintf('Smoothing of the data. Please wait...')
for indexOfAverage=1:nAverages
  for indexOfChannel=1:nCoils
      for selectedMix = 1:nMix
          signal         = oldData(:, 1, 1, indexOfChannel, 1, 1, 1, 1, selectedMix, 1, 1, indexOfAverage);
          %apply the frequency shift on the time domain vector
          smoothedArray  = MR_spectroS.smoothArray(signal, smoothingFactor, doPlotting);
          %1:NCol, 1, 1, 1:NCha, 1, 1, 1, 1, 1, 1:NRep, 1, 1:NSet
          newData(:, 1, 1, indexOfChannel, 1, 1, 1, 1, selectedMix, 1, 1, indexOfAverage)=smoothedArray;
      end
  end
end

this.Data{1} = newData;
fprintf('Smoothing...FINISHED')  
end