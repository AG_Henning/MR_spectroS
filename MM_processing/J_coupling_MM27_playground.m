jCoupling MM2.7
frequency:
x = [
% TE left right middle [peaks]
24 2.703 2.659  2.679
32 2.713 2.664  2.679
52 2.703 2.664  2.674
60 2.718 2.684  2.708
]
meanX = mean(x)
42.0000    2.7092    2.6677    2.6850
center = (meanX(2) + meanX(3)) / 2
2.6885
meanCenter = (center + meanX(4)) /2
2.6868

scanFreq = 399.72
diff = meanX(2) - meanX(3)
diffHz = diff * scanFreq
16.5884 [Hz]
