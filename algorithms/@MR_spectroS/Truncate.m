function this = Truncate( this, truncPoint )

if this.Parameter.ReconFlags.isTruncated
    warning('The data is already Truncated')
end


data = this.Data{1};
data(truncPoint:end, :, :, :, :, :, :, :, :, :, :, :)=0;
this.Data{1} = data;


this.Parameter.ReconFlags.isTruncated = truncPoint;

end

