function createSimulatedSpectra()

%% Concentrations and T2 values taken from the Murali Manohar MRM 2019 paper
% Concentration values were down or upscaled a bit to match better the literature comparison, if they were needed (Table 3)
% Values were rounded, standard deviations increased for values where possible mismatch with literature could be assumed. Both for concentrations and T2s
% Default T2 value taken for the metabolites, where these were not determined in the paper: GABA, Lac, Scyllo, Tau were taken as 75+-35ms
metabolites     = {'Asp', 'tCr(CH2)', 'tCr(CH3)', 'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA(CH3)', 'NAA(CH2)', 'NAAG', 'tCho+',  'Scy',    'Tau', 'MMB'};
% filenames       = {'Asp', 'Cr_CH2',   'Cr',       'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA_ac',   'NAA_as',   'NAAG', 'tCho_P', 'Scyllo', 'Tau', 'Leu'};
concentrations  = [  3,     8.5,        8.5,       1.8,     3,     9,      1.2,    1.4,   0.7,   5.5,   12,          12,        1.4,    1.5,      0.4,     1.5,   1.5e7]; %mmol/kg
conc_std        = [0.8,     0.7,        0.7,       0.5,   0.8,     1,     0.3,   0.2,   0.2,   0.5,    1,           1,        0.3,    0.4,      0.1,     0.3,   0.2e7]; %mmol/kg
T2s             = [ 54,     82,         100,        75,    50,    85,     60,     75,    75,   90,    110,         100,        45,     90,       75,      75,   25]; %ms
T2_std          = [ 12,     10,          16,        35,    20,    25,     10,     35,    35,   20,     30,          25,        20,     25,       35,      35,   10]; %ms

TE = 24 * 1e-3;
numMetabolites = length(metabolites);

saveData = false;
saveFigures = false;
saveRandNumbers = false; %should generally be kept false to have the same random numbers consistently

%% path to the export of the spectra
exportFolder = {'11 Simulations'};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolder);
dataExportPathBase = strcat(pathBaseExportFiles, exportFolder{1}, '\');

% think whether to change tCho+ from original (1.0+-0.2) to max(2.5+-0.5) current(1.5+-0.4) to accomodate PE

gaussianValues = [4:2:32]; %Hz
pc0 = [-35:5:35]; %grad
% The first order phase does not fully mimick in vivo conditions. While we
%could mimick a truncation of the first points, a maximum echo sampling is
%not possible with the normal echo sampling basis set
pc1 = [-17.5:2.5:17.5]; %grad/ppm
% pc1 = [-30:5:30]; %grad/ppm
df2_global = [-17.5:2.5:17.5]; % Hz : Global and local shifts
df2_local_scale = [0.25 0.25 0.5 0.5 0.75 0.75 1 1 1.5 1.5 2 2 3 3 5];
df2_local_std = 3;% 3 Hz coresponds to ~0.0075 ppm.
% This is the maximal shift, which can occur by temperature between
% moieties (Wermter, MRMP 2017), assuming up to 10�C temperature change.
% pH changes should have a similar order of magnitude.

em_local_scale = [0.2 0.2 0.4 0.4 0.6 0.6 0.8 0.8 1 1 1.2 1.2 1.5 1.5 2];
%check the resulting SNR levels from noiseLevel in snr_NAA_noise in the noise simulation
noiseLevel = [1:25];

numberDfPermutations = length(df2_global);
if saveRandNumbers
    %generally you should not enter this if. It creates random numbers which can be used for calculating the concentrations
    df2_local_RandNumbers = rand(numberDfPermutations,numMetabolites);
    save([dataExportPathBase, 'df2_local_RandNumbers.mat'], 'df2_local_RandNumbers');
end
load([dataExportPathBase, 'df2_local_RandNumbers.mat'], 'df2_local_RandNumbers');
em_local_RandNumbers = df2_local_RandNumbers; %use the same random numbers for simplicity

%%
metaboliteFileNames = metabolites;
% replace the weirdos
metaboliteFileNames = strrep(metaboliteFileNames, 'tCr(CH2)', 'Cr_singlet_tot_3.925');
metaboliteFileNames = strrep(metaboliteFileNames, 'tCr(CH3)', 'Cr_singlet_tot_3.028');
metaboliteFileNames = strrep(metaboliteFileNames, 'NAA(CH3)', 'NAA_2');
metaboliteFileNames = strrep(metaboliteFileNames, 'NAA(CH2)', 'NAA_1');
metaboliteFileNames = strrep(metaboliteFileNames, 'tCho+',    'tCho_PE');
metaboliteFileNames = strrep(metaboliteFileNames, 'Scy',      'Scyllo');
metaboliteFileNames = strrep(metaboliteFileNames, 'MMB',      'MMB_without_metabolite_TE24');

%% paths to the basis set files
pathName = 'DF data path';
sampleCriteria = {'Basis_sets'};
[filePath] = pathToDataFolder(pathName, sampleCriteria);


filePathBasis = strcat(filePath,  'Basis_sets/final_T2_met_MM_paper/');
pathMM = 'sLASER_MM/';
pathMetabolites = 'sLASER_new_UF_7ppm/TE24/';

fids = cell(1, numMetabolites);
plotBasis = false;

for indexMetabolite = 1:numMetabolites
    fileName = metaboliteFileNames{indexMetabolite};
    if strcmp(metabolites{indexMetabolite},'MMB')
        filePathMetabolite = strcat(filePathBasis, pathMM);
    else
        filePathMetabolite = strcat(filePathBasis, pathMetabolites);
    end
    [fid, bandwidth, scanFrequency, metaboliteName, singletPresent] = ...
        ImportLCModelBasis(filePathMetabolite, fileName, plotBasis, '1H');
    if strcmp(metabolites{indexMetabolite},'NAA_ac')
        %         fid = fid * 3;
    end
    if strcmp(metabolites{indexMetabolite},'MMB')
        fids{indexMetabolite}(1,:)= fid(1:4096); % stupid change to make all metabolites 4096 long!
    else
        fids{indexMetabolite}(1,:)= fid;
    end
end

water_ppm = 4.66; % ppm
referencePeakPpm = -water_ppm;
nTimepoints = length(fids{1});
dwellTime = 1/bandwidth;
timePointsSampling =(0:nTimepoints - 1) * dwellTime;
scalingFactorRefPeak = 0.05;

refFreqPpm = -0; % the reference Frequency for the excitation pulse, given in ppm compared to the water (minus is upfield, + downfield)

phasePivot_ppm = water_ppm + refFreqPpm;%in ppm
ppmVector = ppmVectorFunction(scanFrequency*1e6, bandwidth, nTimepoints, '1H');
phase_1_f2_mx = ppmVector-phasePivot_ppm;

ppmMask = (ppmVector>0.6) & (ppmVector<4.1);

legendLabels = cell(3,1);

%% set default values for the parameters
default_pc0 = 0;
default_pc1 = 0;
default_df2 = zeros(1,numMetabolites);
default_gm  = 12;
default_em_std_factor = zeros(1,numMetabolites);
default_noiseLevel = 3;
noiseVector = wgn(1,nTimepoints, default_noiseLevel);
trunc_ms = 200;
truncPoint = floor( trunc_ms/(dwellTime*1e+3));

%% create the actual spectra
current_pc0 = default_pc0;
current_pc1 = default_pc1;
current_df2 = default_df2;
current_gm = default_gm;
current_em_std_factor = default_em_std_factor;
conc_std_factor = zeros(1,numMetabolites);

% generate the default spectrum
[defaultSummedFid, defaultSummedFidWithNoise, defaultCurrent_em, defaultCurrentConcentrationRm, defaultStats] = ...
    createSummedSpectrum(...
    fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
    current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
    truncPoint, saveData, 'meanConc', [], dwellTime * 1e3, scanFrequency, dataExportPathBase, water_ppm, scalingFactorRefPeak);

plot(ppmVector,real(fftshift(fft(defaultSummedFid))));
% figure(1);
% subplot(5,4,8+7:8+8)
figure(1);
subplot(3,4,9:10)%Revision 1
% figure(2);
% subplot(3,4,7:8)

%% generate the series of different concentrations
stdLimConc = 3.5;
numberConcPermutations = 100;
concentrationsRm = zeros(numberConcPermutations, numMetabolites);
paramKeyword = 'conc';
titleStr = '{\boldmath$c$}';
if saveRandNumbers
    %generally you should not enter this if. It creates random numbers which can be used for calculating the concentrations
    concentrationRandNumbers = rand(numberConcPermutations,numMetabolites);
    save([dataExportPathBase, 'concentrationRandNumbers.mat'], 'concentrationRandNumbers');
end
load([dataExportPathBase, 'concentrationRandNumbers.mat'], 'concentrationRandNumbers');
dataExportPath = [dataExportPathBase, paramKeyword, '\'];
snr_NAA_conc = zeros(numberConcPermutations,1);
for indexParam = 1:numberConcPermutations
    for indexMetabolite = 1:numMetabolites
        % generate a random number between [-stdLim; stdLim]
        stdRand = 2 * stdLimConc * concentrationRandNumbers(indexParam,indexMetabolite) - stdLimConc;
        switch metabolites{indexMetabolite}
            case 'tCr(CH2)'
                creatineStdRand = stdRand;
            case 'tCr(CH3)'
                stdRand = creatineStdRand;
            case 'NAA(CH3)'
                NaaStdRand = stdRand;
            case 'NAA(CH2)'
                stdRand = NaaStdRand;
            otherwise
                    % nothing to do
        end
        conc_std_factor(indexMetabolite) = stdRand;
    end
    [summedFid, summedFidWithNoise, current_em, currentConcentrationRm, stats] = createSummedSpectrum(...
        fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
        current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
        truncPoint, saveData, paramKeyword, indexParam, dwellTime * 1e3, scanFrequency, dataExportPath, referencePeakPpm, scalingFactorRefPeak);

    snr_NAA_conc(indexParam) = stats{2,2};
    concentrationsRm(indexParam,:) = currentConcentrationRm;
    hold on;
    plot(ppmVector,real(fftshift(fft(summedFidWithNoise))));
%     ImportLCModelBasis(dataExportPath, fileName, true, '1H');
end

plotSimulationSetup(titleStr, paramKeyword, saveFigures, dataExportPathBase);
if saveData
    save([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteFileNames');
end

% for indexMetabolite = 1:numMetabolites
%     figure;
%     scatter(concentrationsRm(:,indexMetabolite), concentrationsRm(:,indexMetabolite));
%     title(metabolites(indexMetabolite))
% end

%% #############################################################################################
% generate the series of different parameters, same concentrations
%  #############################################################################################
conc_std_factor = zeros(1,numMetabolites); % reset value

%% gauss
paramKeyword = 'gauss';
titleStr = '$$\nu_{g}$$';
units = '(Hz)';
dataExportPath = [dataExportPathBase, paramKeyword, '\'];
concentrationsRm = zeros(length(gaussianValues), numMetabolites);

% figure(1);
% subplot(5,4,5:6)
figure(2);
subplot(2,4,1:2)%Revision 1
% subplot(2,4,5:6)
snr_NAA_Gauss = zeros(length(gaussianValues),1);
for indexParam = 1:length(gaussianValues)
    
    current_gm = gaussianValues(indexParam);
    [summedFid, summedFidWithNoise, current_em, currentConcentrationRm, stats] = createSummedSpectrum(...
        fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
        current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
        truncPoint, saveData, paramKeyword, indexParam, dwellTime * 1e3, scanFrequency, dataExportPath, referencePeakPpm, scalingFactorRefPeak);
    
    snr_NAA_Gauss(indexParam) = stats{2,2};
    concentrationsRm(indexParam,:) = currentConcentrationRm;
    
    legendLabels = plotSpectra(summedFidWithNoise, ppmVector, false, legendLabels, indexParam, gaussianValues, '$\rm\nu_{g}$', units);
end
% reset parameter
current_gm = default_gm;

plotSimulationSetup(titleStr, paramKeyword, saveFigures, dataExportPathBase);
legend(legendLabels, 'Interpreter', 'latex');
plotArrowsPlotOffsets()
%%
if saveData
    save([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteFileNames');
end

%% pc0 - zero order phase
paramKeyword = 'pc0';
titleStr = '$$\varphi_{0}$$';
units = '(degrees)';
dataExportPath = [dataExportPathBase, paramKeyword, '\'];
concentrationsRm = zeros(length(pc0), numMetabolites);

figure(1);
% subplot(5,4,1:2)
subplot(3,4,1:2) %Revision 1
% subplot(2,4,1:2)
snr_NAA_pc0 = zeros(length(pc0),1);
for indexParam = 1:length(pc0)
    
    current_pc0 = pc0(indexParam);
    [summedFid, summedFidWithNoise, current_em, currentConcentrationRm, stats] = createSummedSpectrum(...
        fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
        current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
        truncPoint, saveData, paramKeyword, indexParam, dwellTime * 1e3, scanFrequency, dataExportPath, referencePeakPpm, scalingFactorRefPeak);
    
    snr_NAA_pc0(indexParam) = stats{2,2};
    concentrationsRm(indexParam,:) = currentConcentrationRm;
    
    legendLabels = plotSpectra(summedFidWithNoise, ppmVector, true, legendLabels, indexParam, pc0, titleStr, units);
end
% reset parameter
current_pc0 = default_pc0;

plotSimulationSetup(titleStr, paramKeyword, saveFigures, dataExportPathBase);
if saveData
    save([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteFileNames');
end

%% pc1 - first order phase
paramKeyword = 'pc1';
titleStr = '$$\varphi_{1}$$';
units = '(degrees/ppm)';
dataExportPath = [dataExportPathBase, paramKeyword, '\'];
concentrationsRm = zeros(length(pc1), numMetabolites);

figure(1);
% subplot(5,4,3:4)
subplot(3,4,3:4)%Revision 1
% subplot(2,4,3:4)
snr_NAA_pc1 = zeros(length(pc1),1);
for indexParam = 1:length(pc1)
    
    current_pc1 = pc1(indexParam);
    [summedFid, summedFidWithNoise, current_em, currentConcentrationRm, stats] = createSummedSpectrum(...
        fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
        current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
        truncPoint, saveData, paramKeyword, indexParam, dwellTime * 1e3, scanFrequency, dataExportPath, referencePeakPpm, scalingFactorRefPeak);
    
    snr_NAA_pc1(indexParam) = stats{2,2};
    concentrationsRm(indexParam,:) = currentConcentrationRm;
    
    legendLabels = plotSpectra(summedFidWithNoise, ppmVector, true, legendLabels, indexParam, pc1, titleStr, units);
end
% reset parameter
current_pc1 = default_pc1;

plotSimulationSetup(titleStr, paramKeyword, saveFigures, dataExportPathBase);
if saveData
    save([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteFileNames');
end

%% df2 local - frequency shifts
paramKeyword = 'df2_local';
titleStr = '{\boldmath$\omega_{local}$}';
units = '(Hz)';
dataExportPath = [dataExportPathBase, paramKeyword, '\'];
concentrationsRm = zeros(length(df2_local_scale), numMetabolites);

% figure(1);
% subplot(5,4,8+1:8+2)
figure(1);
subplot(3,4,7:8)%Revision 1
% figure(2);
% subplot(3,4,1:2)
snr_NAA_df2_local = zeros(length(df2_local_scale),1);
for indexParam = 1:length(df2_local_scale)
    
    for indexMetabolite = 1:numMetabolites
        % generate a random number between [-stdLim; stdLim]
        stdRand = 2 * df2_local_std * df2_local_RandNumbers(indexParam,indexMetabolite) - df2_local_std;
        switch metabolites{indexMetabolite}
            case 'tCr(CH2)'
                creatineStdRand = stdRand;
            case 'tCr(CH3)'
                stdRand = creatineStdRand;
            case 'NAA(CH3)'
                NaaStdRand = stdRand;
            case 'NAA(CH2)'
                stdRand = NaaStdRand;
            otherwise
                % nothing to do
        end
        current_df2(indexMetabolite) = df2_local_scale(indexParam) *  stdRand;
    end
    [summedFid, summedFidWithNoise, current_em, currentConcentrationRm, stats] = createSummedSpectrum(...
        fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
        current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
        truncPoint, saveData, paramKeyword, indexParam, dwellTime * 1e3, scanFrequency, dataExportPath, water_ppm, scalingFactorRefPeak);
    
    snr_NAA_df2_local(indexParam) = stats{2,2};
    concentrationsRm(indexParam,:) = currentConcentrationRm;
    
    legendLabels = plotSpectra(summedFidWithNoise, ppmVector, true, legendLabels, indexParam, df2_local_scale, titleStr, units);
end
% reset parameter
current_df2 = default_df2;

plotSimulationSetup(titleStr, paramKeyword, saveFigures, dataExportPathBase);
if saveData
    save([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteFileNames');
end

%% df2 global - frequency shifts
paramKeyword = 'df2_global';
titleStr = '$$\omega_{global}$$';
units = ('Hz');
dataExportPath = [dataExportPathBase, paramKeyword, '\'];
concentrationsRm = zeros(length(df2_global), numMetabolites);

% figure(1);
% subplot(5,4,7:8)
figure(1);
subplot(3,4,5:6)%Revision 1
% subplot(2,4,7:8)
snr_NAA_df2_global = zeros(length(df2_global),1);
for indexParam = 1:length(df2_global)
    for indexMetabolite = 1:numMetabolites
        current_df2(indexMetabolite) = df2_global(indexParam);
    end
    [summedFid, summedFidWithNoise, current_em, currentConcentrationRm, stats] = createSummedSpectrum(...
        fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
        current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
        truncPoint, saveData, paramKeyword, indexParam, dwellTime * 1e3, scanFrequency, dataExportPath, water_ppm, scalingFactorRefPeak);
    
    snr_NAA_df2_global(indexParam) = stats{2,2};
    concentrationsRm(indexParam,:) = currentConcentrationRm;
    
    legendLabels = plotSpectra(summedFidWithNoise, ppmVector, true, legendLabels, indexParam, df2_global, titleStr, units);
end
% reset parameter
current_df2 = default_df2;

plotSimulationSetup(titleStr, paramKeyword, saveFigures, dataExportPathBase);
if saveData
    save([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteFileNames');
end

%% em - lorentzian components of T2
paramKeyword = 'em';
titleStr = '{\boldmath$\nu_{e}$}';
units = '(Hz)';
dataExportPath = [dataExportPathBase, paramKeyword, '\'];
concentrationsRm = zeros(length(df2_global), numMetabolites);

% figure(1);
% subplot(5,4,8+3:8+4)
figure(2);
subplot(2,4,3:4)%Revision 1
% figure(2);
% subplot(3,4,3:4)
snr_NAA_em = zeros(length(df2_global),1);
for indexParam = 1:length(df2_global)
    for indexMetabolite = 1:numMetabolites
        % generate a random number between [-stdLim; stdLim]
        stdRand = 2 * em_local_RandNumbers(indexParam,indexMetabolite) - 1;
        switch metabolites{indexMetabolite}
            case 'tCr(CH2)'
                creatineStdRand = stdRand;
            case 'tCr(CH3)'
                stdRand = creatineStdRand;
            case 'NAA(CH3)'
                NaaStdRand = stdRand;
            case 'NAA(CH2)'
                stdRand = NaaStdRand;
            otherwise
                % nothing to do
        end
        current_em_std_factor(indexMetabolite) = em_local_scale(indexParam) * stdRand;
    end
    [summedFid, summedFidWithNoise, current_em, currentConcentrationRm, stats] = createSummedSpectrum(...
        fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
        current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
        truncPoint, saveData, paramKeyword, indexParam, dwellTime * 1e3, scanFrequency, dataExportPath, referencePeakPpm, scalingFactorRefPeak);
    
    snr_NAA_em(indexParam) = stats{2,2};
    concentrationsRm(indexParam,:) = currentConcentrationRm;
    
    legendLabels = plotSpectra(summedFidWithNoise, ppmVector, false, legendLabels, indexParam, em_local_scale, '$\rm T_{2,k} = T_{2,k} + X_{-1,1}^{k} \cdot $', '$\rm\cdot std(T_{2,k})$');
end
% reset parameter
current_em_std_factor = default_em_std_factor;

plotSimulationSetup(titleStr, paramKeyword, saveFigures, dataExportPathBase);
legend(legendLabels, 'Interpreter', 'latex');
plotArrowsPlotOffsets()
if saveData
    save([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteFileNames');
end


%% noise - add different noise levels
paramKeyword = 'noise';
titleStr = '{\boldmath$noise$}';
units = '';
dataExportPath = [dataExportPathBase, paramKeyword, '\'];
concentrationsRm = zeros(length(noiseLevel), numMetabolites);

% figure(1);
% subplot(5,4,8+5:8+6)
figure(2);
subplot(2,4,6:7)%Revision 1
% figure(2);
% subplot(3,4,5:6)
snr_NAA_noise = zeros(length(noiseLevel),1);
for indexParam = 1:length(noiseLevel)
    noiseVector = wgn(1,nTimepoints, noiseLevel(indexParam));
    [summedFid, summedFidWithNoise, current_em, currentConcentrationRm, stats] = createSummedSpectrum(...
        fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
        current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
        truncPoint, saveData, paramKeyword, indexParam, dwellTime * 1e3, scanFrequency, dataExportPath, referencePeakPpm, scalingFactorRefPeak);
    
    snr_NAA_noise(indexParam) = stats{2,2};
    concentrationsRm(indexParam,:) = currentConcentrationRm;

    legendLabels = plotSpectra(summedFidWithNoise, ppmVector, false, legendLabels, indexParam, snr_NAA_noise, '$\rm SNR_{NAA}$', units);
end
% reset parameter
noiseVector = wgn(1,nTimepoints, default_noiseLevel);

plotSimulationSetup(titleStr, paramKeyword, saveFigures, dataExportPathBase);
legend(legendLabels, 'Interpreter', 'latex');
plotArrowsPlotOffsets()
if saveData
    save([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteFileNames');
end

%% noise - add different noise levels
paramKeyword = 'baseline';
titleStr = '{\boldmath$baseline$}';
units = '';
dataExportPath = [dataExportPathBase, paramKeyword, '\'];
concentrationsRm = zeros(length(noiseLevel), numMetabolites);

baselinesPath = [dataExportPathBase 'Baselines\'];
prefixBaseline = 'Baseline_';
baselines = {'1Flat_drop_H20'; '2Flat'; '3Flat'; '4Flat'; '5Wildish'; '6Wildish'; '7Very_wild';...
    '8Very_wild'; '9Wild'; '10Wild'; '11Wildish'; '12Wildish'; '13Normal'; '14Lipid_1.3'; '15Lipid_1.3_phased'; '16Zero'};

% figure(1);
% subplot(5,4,8+10:8+11)
figure(1);
subplot(3,4,11:12)
% figure(2);
% subplot(3,4,10:11)
snr_NAA_baselines = zeros(length(baselines),1);
for indexParam = 1:length(baselines)
    baselineFileName = strcat(prefixBaseline, baselines{indexParam});
    [currentBaseline, ~, ~, ~, ~] = ImportLCModelBasis(baselinesPath, baselineFileName, plotBasis, '1H');
    currentBaseline = currentBaseline(1:length(fids{1}));% will fail if the fids are longer than the baseline
    [summedFid, summedFidWithNoise, current_em, currentConcentrationRm, stats] = createSummedSpectrum(...
        fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
        current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
        truncPoint, saveData, paramKeyword, indexParam, dwellTime * 1e3, scanFrequency, dataExportPath, referencePeakPpm, scalingFactorRefPeak, currentBaseline);
    
    snr_NAA_baselines(indexParam) = stats{2,2};
    concentrationsRm(indexParam,:) = currentConcentrationRm;

    legendLabels = plotSpectra(summedFidWithNoise, ppmVector, true, legendLabels, indexParam, baselines, titleStr, units);
end
plotSimulationSetup(titleStr, paramKeyword, saveFigures, dataExportPathBase);
if saveData
    save([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteFileNames');
end
end

function legendLabels = plotSpectra(summedFidWithNoise, ppmVector, plotAll, legendLabels, indexParam, ...
    paramValues, titleStr, units)

if (plotAll == true)
    %plot each fid
    hold on;
    plot(ppmVector,real(fftshift(fft(summedFidWithNoise))));
else
    if ~contains(titleStr,'=')
        titleStr = [titleStr, ' = '];
    end
    % plot just the max, min and median value
    offsetPlot = -1;
    if (indexParam == 1)
        offsetPlot = 0;
    end
    if (indexParam == round(length(paramValues)/2))
        offsetPlot = 1;
    end
    if (indexParam == length(paramValues))
        offsetPlot = 2;
    end
    if (offsetPlot ~= -1)
        hold on;
        plot(ppmVector,real(fftshift(fft(summedFidWithNoise)))-offsetPlot*2e3);
        legendLabels{offsetPlot+1} = [titleStr, num2str(paramValues(indexParam)), ' ', units];
    end
end
end

function plotSimulationSetup(titleStr, paramKeyword, saveFigures, dataExportPathBase)
FontSize = 12;
LineWidth = 1.5;
xlim([0.6, 4.1])
xlabel('\delta (ppm)')
ylabel('Signal (arb. u.)')
set(gca,'xDir','reverse')
set(gca,'ytick',[]);
title(['Spectra with ', titleStr, ' variations'], 'Interpreter','latex');
set(gca,'fontsize',FontSize);
set(gca,'FontWeight','bold');
h = findobj(gca,'Type','line');
for plots = h
    set(plots,'LineWidth',LineWidth)
end
if saveFigures
    fileNameFull = [dataExportPathBase, 'Plots\Simulations_', paramKeyword];
    print([fileNameFull, '.tif'],'-dtiff','-r600');
    savefig(gcf, [fileNameFull, '.fig'],'compact');
end
end

function plotArrowsPlotOffsets()

arrowsColor = [0.4 0.4 0.4];
offSetStep = 2*1e3;
%arrows first - second
yUpper = -0.2*offSetStep;
yLower = -1.2*offSetStep;
xa = [0.6 0.6]-0.05;
doublePointedArrowInside(xa,yUpper,yLower, arrowsColor);
%arrows second - third
yUpper = -1.2*offSetStep;
yLower = -2.2*offSetStep;
xa = [0.6 0.6]-0.1;
doublePointedArrowInside(xa,yUpper,yLower, arrowsColor);

% text box with label "Plot offsets"
xa = [0.6 0]+0.1;
ya = [1e3 0];
[xaf,yaf] = axescoord2figurecoord(xa,ya);
str = {'Plot','offsets'};
annotation('textbox',[xaf(1) yaf(1)-0.02 0.1 0.1],'String',str,'FitBoxToText','On', ...
    'EdgeColor','none', 'FontWeight','bold','Color',arrowsColor);

set(gcf,'Color','w')
end
