 function this = fMRSAverage( this )


if this.Parameter.ReconFlags.isfMRSAveraged
    error('The data are already fMRSAveraged')
end


paradigm     = this.Parameter.fMRSAveSettings.paradigm;

%plotParadigm = this.Parameter.fMRSAveSettings.plotParadigm;

lParadigm = length(paradigm); 
if mod(lParadigm,2)
    error('wrong paradigm was set')
end

 
oldData                  = this.Data{1};

 
onAverages      = [];
offAverages     = [];
discardAverages = [];
totalAverages   = 0;

% find which acquitisions belong to each block
for i=1:2:lParadigm
    if paradigm(i) == 0 %is OFF
        offAverages = [offAverages totalAverages+1:totalAverages+paradigm(i+1)];
    elseif paradigm(i) == 1 %is ON
        onAverages  = [onAverages totalAverages+1:totalAverages+paradigm(i+1)];  
    else %descarded 
        discardAverages = [discardAverages totalAverages+1:totalAverages+paradigm(i+1)];  
    end
     totalAverages = totalAverages + paradigm(i+1);
end


if totalAverages ~= size(oldData,this.meas_dim)
    error('totalAverages ~= size(oldData,this.meas_dim)');
end

    % arrangement of kx_dim : 1=ON, 2=OFF, 3= Discarded 
    newData = cat(this.meas_dim, sum(oldData(:,:,:,:,:,:,:,:,:,:,:,onAverages)/length(onAverages),this.meas_dim),...
                                 sum(oldData(:,:,:,:,:,:,:,:,:,:,:,offAverages)/length(offAverages),this.meas_dim),...
                                 sum(oldData(:,:,:,:,:,:,:,:,:,:,:,discardAverages)/length(discardAverages),this.meas_dim)); %normalize them!
                             
    newData = cat(this.meas_dim, sum(oldData(:,:,:,:,:,:,:,:,:,:,:,onAverages),this.meas_dim),...
                                 sum(oldData(:,:,:,:,:,:,:,:,:,:,:,offAverages),this.meas_dim)); %normalize them!
                                
                             
    if length(onAverages) ~= length(offAverages)
        warning('#onAverages ~= #offAverages');
    end
                           
                           
    this.Data{1} = newData;
    this.Parameter.ReconFlags.isfMRSAveraged = true;
    %this.Parameter.EddyCurrSettings.numberOfWaterSignals = 2;
end

