function fitting_simulated_test_data_profit(fitLCModel)
if ~exist('fitLCModel','var')
    fitLCModel = false; %false fits profit
end

%TODO change
TR = 6000;%ms

truncOn = true;

exportFolder = {'11 Simulations'};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolder);
dataExportPathBase = strcat(pathBaseExportFiles, exportFolder{1}, '\');

TE = 24;

if fitLCModel
    %% create LCModel instance
    controlFilesPathRemote = '/Desktop/TestDataFitting/LCModelConfig/';
    outputFilesPathRemote = '/Desktop/TestDataFitting/Output/';
    outputFilesPathLocal = [pathBaseExportFiles, 'Output/'];
    LCModelCallerInstance = startLCModel(controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal);
    copyFiles = true;
else
    % these variables are actually not needed for ProFit fitting
    outputFilesPathRemote = [];
    outputFilesPathLocal = [];
    LCModelCallerInstance = [];
    copyFiles = false;
end

%% do the actual fitting
if truncOn
    numberConcPermutations = 100;
    paramKeyword = 'conc';
    truncSuffix = '_truncOn';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
else
    numberConcPermutations = 100;
    paramKeyword = 'conc';
    truncSuffix = '_truncOff';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
end

if truncOn
    numberConcPermutations = 15;
    paramKeyword = 'df2_local';
    truncSuffix = '_truncOn';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
else 
    numberConcPermutations = 15;
    paramKeyword = 'df2_local';
    truncSuffix = '_truncOff';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
end

if truncOn
    numberConcPermutations = 15;
    paramKeyword = 'df2_global';
    truncSuffix = '_truncOn';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
else 
    numberConcPermutations = 15;
    paramKeyword = 'df2_global';
    truncSuffix = '_truncOff';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
end

if truncOn
    numberConcPermutations = 15;
    paramKeyword = 'pc1';
    truncSuffix = '_truncOn';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
else
    numberConcPermutations = 15;
    paramKeyword = 'pc1';
    truncSuffix = '_truncOff';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
end

if truncOn
    numberConcPermutations = 15;
    paramKeyword = 'pc0';
    truncSuffix = '_truncOn';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
else
    numberConcPermutations = 15;
    paramKeyword = 'pc0';
    truncSuffix = '_truncOff';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
end

if truncOn
    numberConcPermutations = 15;
    paramKeyword = 'gauss';
    truncSuffix = '_truncOn';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
else
    numberConcPermutations = 15;
    paramKeyword = 'gauss';
    truncSuffix = '_truncOff';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
end

if truncOn
    numberConcPermutations = 15;
    paramKeyword = 'em';
    truncSuffix = '_truncOn';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
else
    numberConcPermutations = 15;
    paramKeyword = 'em';
    truncSuffix = '_truncOff';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
end

if truncOn
    numberConcPermutations = 25;
    paramKeyword = 'noise';
    truncSuffix = '_truncOn';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
else
    numberConcPermutations = 25;
    paramKeyword = 'noise';
    truncSuffix = '_truncOff';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
end

if truncOn
    numberConcPermutations = 16;
    paramKeyword = 'baseline';
    truncSuffix = '_truncOn';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
else
    numberConcPermutations = 16;
    paramKeyword = 'baseline';
    truncSuffix = '_truncOff';
    fileNameBase = ['Simulated_', paramKeyword, '_'];
    actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
        truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal);
end
end

function actualFitting(numberConcPermutations, paramKeyword, dataExportPathBase, TE, TR, fileNameBase, ...
    truncSuffix, fitLCModel, LCModelCallerInstance, pathBaseExportFiles, copyFiles, outputFilesPathLocal)
%% do the actual fitting
dataExportPath = [dataExportPathBase, paramKeyword, '/'];
concentrations = cell(1,numberConcPermutations);
if ~fitLCModel
    diff_conc = cell(1,numberConcPermutations);
    diff_em   = cell(1,numberConcPermutations);
    diff_gm   = cell(1,numberConcPermutations);
    diff_pc0  = cell(1,numberConcPermutations);
    diff_df2  = cell(1,numberConcPermutations);
    diff_pc12 = cell(1,numberConcPermutations);
    diff_T2   = cell(1,numberConcPermutations);
end
for index = 1:numberConcPermutations
    
    %% reconstruct data
                
    TE_string = ['TE', num2str(TE)];
    filenameData = [fileNameBase, num2str(index), truncSuffix];
    
    if fitLCModel
        fittingLCModelWithTransfer(LCModelCallerInstance, pathBaseExportFiles, ...
            filenameData, dataExportPath, '', '', TE_string, copyFiles);
        movefile([outputFilesPathLocal filenameData '.*'], dataExportPath);
        concentrations{index} = ...
            importConcentrationLCModelTable([filenameData '.table'],dataExportPath, TE_string, 'Met moiety');
        close all;
        plot_All_Fit_Metabolites([filenameData, '.coord'],dataExportPath, 'Met moiety', true, false);
        
    else
        refFreqPpm = 0;
        fittingProFit(filenameData, dataExportPath, '', '', TE, TR, dataExportPath,refFreqPpm);
        [concentrations{index}, activeMetabolites] = plot_ProFit_Metabolites_Fit([filenameData, '_profit.mat'],dataExportPath, 'Met', true, false);
        [diff_conc{index}, diff_em{index}, diff_gm{index}, diff_pc0{index}, diff_df2{index}, diff_pc12{index}, diff_T2{index}] = ...
            compareFitInput_Output(dataExportPath, filenameData, 3);
%         close all;
    end
end


if fitLCModel
    save([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix '.mat'], 'concentrations');
else 
    save([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix '_profit.mat'], 'concentrations', 'activeMetabolites');
    diff_conc_mat = reshape(cell2mat(diff_conc), length(diff_conc{1}), numberConcPermutations);
    diff_em_mat   = reshape(cell2mat(diff_em),   length(diff_em{1}), numberConcPermutations);
    diff_gm_mat   = reshape(cell2mat(diff_gm),   length(diff_gm{1}), numberConcPermutations);
    diff_pc0_mat  = reshape(cell2mat(diff_pc0),  length(diff_pc0{1}), numberConcPermutations);
    diff_df2_mat  = reshape(cell2mat(diff_df2),  length(diff_df2{1}), numberConcPermutations);
    diff_pc12_mat = reshape(cell2mat(diff_pc12), length(diff_pc12{1}), numberConcPermutations);
    diff_T2_mat   = reshape(cell2mat(diff_T2),   length(diff_T2{1}), numberConcPermutations);
    save([dataExportPathBase, paramKeyword, '\Diff_Summaries', truncSuffix, '_no_BL_df3.mat'], 'diff_conc_mat', 'diff_em_mat', 'diff_gm_mat', ...
        'diff_pc0_mat', 'diff_df2_mat', 'diff_pc12_mat', 'diff_T2_mat');
end

close all
end

