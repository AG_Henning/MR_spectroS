function tissueFractions = extractTissueFractionsFromCsv(path, csvFileName)

M = readtable([path csvFileName]);
strfind(M.Properties.VariableNames, 'GM')
indexGM = ~cellfun('isempty', strfind(M.Properties.VariableNames, 'GM'));
tissueFractions.fv_GM=M{1,indexGM};
indexWM = ~cellfun('isempty', strfind(M.Properties.VariableNames, 'WM'));
tissueFractions.fv_WM=M{1,indexWM};
indexCSF = ~cellfun('isempty', strfind(M.Properties.VariableNames, 'CSF'));
tissueFractions.fv_CSF=M{1,indexCSF};
end