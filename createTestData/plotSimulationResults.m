function plotSimulationResults()

%% path to the export of the spectra
exportFolder = {'11 Simulations'};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolder);
pathBaseExportFiles = pathBaseExportFiles(1:end-1);
pathBaseExportFiles = [pathBaseExportFiles ' - R3_new\'];
dataExportPathBase = strcat(pathBaseExportFiles, exportFolder{1}, '\');
truncSuffix = '_truncOn';
scalingOn = true;

% name of metabolites in LCModel
metabolitesLCModel     = {'Asp', 'Cr_CH2', 'Cr', 'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA_as', 'NAA_ac', 'NAAG', 'tCho_P',  'Scyllo',    'Tau', 'Leu'};

% name of metabolites in ProFit
metabolitesProFit     = {'Asp', 'Cr_CH2', 'Cr', 'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA_as', 'NAA_ac', 'NAAG', 'tCho_P',  'Scyllo',    'Tau', 'Leu'};

% name of metabolites to display
metabolitesToDisplay  = {'Asp', 'tCr(CH_2)', 'tCr(CH_3)', 'GABA', 'Gln', 'Glu', 'Gly', 'GSH', 'Lac', 'mI', 'NAA(CH_2)', 'NAA(CH_3)', 'NAAG', 'tCho+', 'sI', 'Tau', 'MM spectrum'};
metabolitesLabels     = {'Asp', 'tCr(CH2)', 'tCr(CH3)', 'GABA', 'Gln', 'Glu', 'Gly', 'GSH', 'Lac', 'mI', 'NAA(CH2)', 'NAA(CH3)', 'NAAG', 'tCho+', 'sI', 'Tau', 'MM spectrum'};

numberOfMet = length(metabolitesLabels);
indexesMainMet = zeros(1,numberOfMet);
mainMetabolites = { 'tCr(CH3)', 'tCr(CH2)', 'Glu', 'mI', 'NAA(CH2)', 'NAA(CH3)', 'tCho+'};
for indexMet = 1:numberOfMet
    indexesMainMet(indexMet) = ~isempty(find(strcmp(mainMetabolites,metabolitesLabels{indexMet}), 1));
end
indexesMainMet = logical(indexesMainMet);

numOfParameters = 4;
[metaboliteTable, indexTable] = setupTable(numberOfMet, numOfParameters, metabolitesLabels);

sumAllMeanProFit     = 0;
sumAllStdProFit      = 0;
sumAllMeanProFitMain = 0;
sumAllStdProFitMain  = 0;

plotColors = {[0 0 1], [1 0 0], [0 0 0], [1 0.5 0.3], [0.49 0.18 0.56], [0.0 0.4 0.0]};

plotCorrelations = false;
plotPrecision = true;

figIds = [];
secondaryFigures =0;
%%
paramKeyword = 'pc0';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = -0.45;
indexTable = indexTable +2;
metaboliteTable{1,indexTable} = paramKeyword;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, metaboliteTable, figIds, ~, ...
    allMeanProFit, allStdProFit,allMeanProFitMain, allStdProFitMain] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, metabolitesToDisplay, metabolitesLabels, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors{1}, offsetPlot, figIds, [], metaboliteTable, indexTable, indexesMainMet, secondaryFigures);
sumAllMeanProFit     = sumAllMeanProFit     + allMeanProFit;
sumAllStdProFit      = sumAllStdProFit      + allStdProFit;
sumAllMeanProFitMain = sumAllMeanProFitMain + allMeanProFitMain;
sumAllStdProFitMain  = sumAllStdProFitMain  + allStdProFitMain;
%%
paramKeyword = 'pc1';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = -0.15;
indexTable = indexTable +2;
metaboliteTable{1,indexTable} = paramKeyword;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, metaboliteTable, figIds, ~,...
    allMeanProFit, allStdProFit,allMeanProFitMain, allStdProFitMain] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, metabolitesToDisplay, metabolitesLabels, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors{2}, offsetPlot, figIds, [], metaboliteTable, indexTable, indexesMainMet, secondaryFigures);
sumAllMeanProFit     = sumAllMeanProFit     + allMeanProFit;
sumAllStdProFit      = sumAllStdProFit      + allStdProFit;
sumAllMeanProFitMain = sumAllMeanProFitMain + allMeanProFitMain;
sumAllStdProFitMain  = sumAllStdProFitMain  + allStdProFitMain;
%%
paramKeyword = 'df2_global';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = 0.15;
indexTable = indexTable +2;
metaboliteTable{1,indexTable} = paramKeyword;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, metaboliteTable, figIds, ~,...
    allMeanProFit, allStdProFit,allMeanProFitMain, allStdProFitMain] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, metabolitesToDisplay, metabolitesLabels, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors{3}, offsetPlot, figIds, [], metaboliteTable, indexTable, indexesMainMet, secondaryFigures);
sumAllMeanProFit     = sumAllMeanProFit     + allMeanProFit;
sumAllStdProFit      = sumAllStdProFit      + allStdProFit;
sumAllMeanProFitMain = sumAllMeanProFitMain + allMeanProFitMain;
sumAllStdProFitMain  = sumAllStdProFitMain  + allStdProFitMain;
%%
paramKeyword = 'df2_local';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = 0.45;
indexTable = indexTable +2;
metaboliteTable{1,indexTable} = paramKeyword;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, metaboliteTable, figIds, ~,...
    allMeanProFit, allStdProFit,allMeanProFitMain, allStdProFitMain] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, metabolitesToDisplay, metabolitesLabels, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors{4}, offsetPlot, figIds, [], metaboliteTable, indexTable, indexesMainMet, secondaryFigures);
sumAllMeanProFit     = sumAllMeanProFit     + allMeanProFit;
sumAllStdProFit      = sumAllStdProFit      + allStdProFit;
sumAllMeanProFitMain = sumAllMeanProFitMain + allMeanProFitMain;
sumAllStdProFitMain  = sumAllStdProFitMain  + allStdProFitMain;
%% -----------------------------------------------------------------------------------------------------------
axes(figIds{1})
str  = {'$$\varphi_0$$', '$$\varphi_1$$',  '$$\omega_{global}$$', '{\boldmath$\omega_{local}$}'};
% xOffset = 0.65;
xOffset = 0.38; %Revision 1
yOffsetRect = 0.080; %0.085 - submission 1 %0.075; -for two subfigures
for indexSim = 1:4
%     yOffset = 0.84;% 4 subfigs%0.80; - Two figs placed lower%0.86 -Two figs
    yOffset = 0.87;% Revision 1
    annotation('textbox',[xOffset yOffset 0.1 0.1],'String',str{indexSim},'FitBoxToText','On', ...
        'EdgeColor','none', 'FontSize', 12, 'Interpreter','latex','Color',plotColors{indexSim})
    annotation('rectangle',[xOffset-0.012 yOffset+yOffsetRect 0.01 0.01],'EdgeColor','none', 'FaceColor',plotColors{indexSim})
%     yOffset = 0.612;% 4 subfigs%0.37; - Two figs placed lower%0.37 -Two figs
%     annotation('textbox',[xOffset yOffset 0.1 0.1],'String',str{indexSim},'FitBoxToText','On', ...
%         'EdgeColor','none', 'FontSize', 12, 'FontWeight','bold', 'Interpreter','latex','Color',plotColors{indexSim})
%     annotation('rectangle',[xOffset-0.012 yOffset+yOffsetRect 0.01 0.01],'EdgeColor','none', 'FaceColor',plotColors{indexSim})
    xOffset = xOffset + 0.004*length(str{indexSim});
end

% save Table
xlswrite([dataExportPathBase, 'SimulationsTable1.xlsx'],metaboliteTable);
numOfParameters2 = 5;
[metaboliteTable, indexTable] = setupTable(numberOfMet, numOfParameters2, metabolitesLabels);

figIds = [];
secondaryFigures = 2;
secondaryFigures = 0;
%% -----------------------------------------------------------------------------------------------------------
%%
paramKeyword = 'gauss';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = -0.45;
indexTable = indexTable +2;
metaboliteTable{1,indexTable} = paramKeyword;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, metaboliteTable, figIds, ~,...
    allMeanProFit, allStdProFit,allMeanProFitMain, allStdProFitMain] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, metabolitesToDisplay, metabolitesLabels, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors{1}, offsetPlot, figIds, [], metaboliteTable, indexTable, indexesMainMet, secondaryFigures);
%%
paramKeyword = 'em';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = -0.15;
indexTable = indexTable +2;
metaboliteTable{1,indexTable} = paramKeyword;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, metaboliteTable, figIds, ~,...
    allMeanProFit, allStdProFit,allMeanProFitMain, allStdProFitMain] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, metabolitesToDisplay, metabolitesLabels, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors{2}, offsetPlot, figIds, [], metaboliteTable, indexTable, indexesMainMet, secondaryFigures);
sumAllMeanProFit     = sumAllMeanProFit     + allMeanProFit;
sumAllStdProFit      = sumAllStdProFit      + allStdProFit;
sumAllMeanProFitMain = sumAllMeanProFitMain + allMeanProFitMain;
sumAllStdProFitMain  = sumAllStdProFitMain  + allStdProFitMain;
%%
paramKeyword = 'noise';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = 0.15;
indexTable = indexTable +2;
metaboliteTable{1,indexTable} = paramKeyword;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, metaboliteTable, figIds, ~,...
    allMeanProFit, allStdProFit,allMeanProFitMain, allStdProFitMain] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, metabolitesToDisplay, metabolitesLabels, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors{3}, offsetPlot, figIds, [], metaboliteTable, indexTable, indexesMainMet, secondaryFigures);
sumAllMeanProFit     = sumAllMeanProFit     + allMeanProFit;
sumAllStdProFit      = sumAllStdProFit      + allStdProFit;
sumAllMeanProFitMain = sumAllMeanProFitMain + allMeanProFitMain;
sumAllStdProFitMain  = sumAllStdProFitMain  + allStdProFitMain;
%%
paramKeyword = 'baseline';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;
offsetPlot = 0.45;
indexTable = indexTable +2;
metaboliteTable{1,indexTable} = paramKeyword;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, metaboliteTable, figIds, ~,...
    allMeanProFit, allStdProFit,allMeanProFitMain, allStdProFitMain] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, metabolitesToDisplay, metabolitesLabels, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors{4}, offsetPlot, figIds, [], metaboliteTable, indexTable, indexesMainMet, secondaryFigures);
sumAllMeanProFit     = sumAllMeanProFit     + allMeanProFit;
sumAllStdProFit      = sumAllStdProFit      + allStdProFit;
sumAllMeanProFitMain = sumAllMeanProFitMain + allMeanProFitMain;
sumAllStdProFitMain  = sumAllStdProFitMain  + allStdProFitMain;

%%
paramKeyword = 'conc';
% load simulated data
load([dataExportPathBase, 'concentrations_simulated_', paramKeyword, '.mat'], 'concentrationsRm', 'metabolites', 'metaboliteNames');
% load fitted data LCModel
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '.mat'], 'concentrations');
concentrationsLCModel = concentrations;
% load fitted data ProFit
load([dataExportPathBase, 'concentrations_', paramKeyword, truncSuffix, '_profit.mat'], 'concentrations', 'activeMetabolites');
concentrationsProFit = concentrations;

plotCorrelations = true;
offsetPlot = 0.;
plotPrecision = false;
indexTable = indexTable +2;
metaboliteTable{1,indexTable} = paramKeyword;
[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, metaboliteTable, ~, figIdsCorr, ...
    allMeanProFit, allStdProFit,allMeanProFitMain, allStdProFitMain] = getConcentrations(...
    metabolites, metabolitesLCModel, metabolitesProFit, metabolitesToDisplay, metabolitesLabels, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors{1}, -offsetPlot, [], [], metaboliteTable, indexTable, indexesMainMet, secondaryFigures);
sumAllMeanProFit     = sumAllMeanProFit     + allMeanProFit;
sumAllStdProFit      = sumAllStdProFit      + allStdProFit;
sumAllMeanProFitMain = sumAllMeanProFitMain + allMeanProFitMain;
sumAllStdProFitMain  = sumAllStdProFitMain  + allStdProFitMain;

%%
sumAllMeanProFit     = sumAllMeanProFit     / (numOfParameters + numOfParameters2)
sumAllStdProFit      = sumAllStdProFit      / (numOfParameters + numOfParameters2)
sumAllMeanProFitMain = sumAllMeanProFitMain / (numOfParameters + numOfParameters2)
sumAllStdProFitMain  = sumAllStdProFitMain  / (numOfParameters + numOfParameters2)
%%
%% -----------------------------------------------------------------------------------------------------------
axes(figIds{1})
str  = {'$$\nu_g$$', '{\boldmath$\nu_{e}$}', '{\boldmath$noise$}', '{\boldmath$baseline$}'};
stringLenghts = {13, 13, 19, 27};
% xOffset = 0.63;
xOffset = 0.38; %Revision 1
yOffsetRect = 0.080; %0.085 - submission 1 %0.075; -for two subfigures
for indexSim = 1:4
%     yOffset = 0.392;% 4 subfigs%0.80; - Two figs placed lower%0.86 -Two figs
    yOffset = 0.87;% Revision 1
    annotation('textbox',[xOffset yOffset 0.1 0.1],'String',str{indexSim},'FitBoxToText','On', ...
        'EdgeColor','none', 'FontSize', 12, 'Interpreter','latex','Color',plotColors{indexSim})
    annotation('rectangle',[xOffset-0.012 yOffset+yOffsetRect 0.01 0.01],'EdgeColor','none', 'FaceColor',plotColors{indexSim})
%     yOffset = 0.172;% 4 subfigs%0.37; - Two figs placed lower%0.37 -Two figs
%     annotation('textbox',[xOffset yOffset 0.1 0.1],'String',str{indexSim},'FitBoxToText','On', ...
%         'EdgeColor','none', 'FontSize', 12, 'FontWeight','bold', 'Interpreter','latex','Color',plotColors{indexSim})
%     annotation('rectangle',[xOffset-0.012 yOffset+yOffsetRect 0.01 0.01],'EdgeColor','none', 'FaceColor',plotColors{indexSim})
    xOffset = xOffset + 0.004*stringLenghts{indexSim};
end

xlswrite([dataExportPathBase, 'SimulationsTable2.xlsx'],metaboliteTable);
end

%% get metabolite concentrations for a given paramKeyword and create the requested plots
function [allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev, metaboliteTable, figIds, figIdsCorr,...
    allMeanProFit, allStdProFit,allMeanProFitMain, allStdProFitMain] = ...
    getConcentrations(metabolites, metabolitesLCModel, metabolitesProFit, metabolitesToDisplay, metabolitesLabels, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, ...
    scalingOn, plotCorrelations, dataExportPathBase, paramKeyword, truncSuffix, ...
    plotPrecision, plotColors, offsetPlot, figIds, figIdsCorr, metaboliteTable, indexTable, indexesMainMet, secondaryFigures)
numberOfMet = length(metabolites);

[allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev] = ...
    reorganizeMetConcentrationsToMatrices(metabolites, metabolitesLCModel, metabolitesProFit, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, scalingOn);
if plotCorrelations
    figIdsCorr = plotCorrelationFigs(concentrationsRm, allLCModelMetConc, allProFitMetConc, metabolitesToDisplay, numberOfMet, ...
    dataExportPathBase, paramKeyword, truncSuffix, figIdsCorr);
end
if plotPrecision
    figIds = plotPrecisionFigs(figIds, numberOfMet, offsetPlot, secondaryFigures, plotColors, ...
    allLCModelMetConcDev, allProFitMetConcDev, metabolitesLabels);
end

display([paramKeyword, ' NANs:', num2str(sum(isnan(allLCModelMetConcDev)))]);
meanConcDevLCModel = mean(allLCModelMetConcDev,'omitnan');
stdConcDevLCModel  = std(allLCModelMetConcDev,'omitnan');
display([paramKeyword, ' NANs ProFit:', num2str(sum(isnan(allProFitMetConcDev)))]);
meanConcDevProFit  = mean(allProFitMetConcDev,'omitnan');
stdConcDevProFit   = std(allProFitMetConcDev,'omitnan');

allMeanLCModel = mean(abs(meanConcDevLCModel));
allStdLCModel  = mean(abs(stdConcDevLCModel));
allMeanProFit  = mean(abs(meanConcDevProFit));
allStdProFit   = mean(abs(stdConcDevProFit));

allMeanLCModelMain = mean(abs(meanConcDevLCModel(indexesMainMet)));
allStdLCModelMain  = mean(abs(stdConcDevLCModel(indexesMainMet)));
allMeanProFitMain  = mean(abs(meanConcDevProFit(indexesMainMet)));
allStdProFitMain   = mean(abs(stdConcDevProFit(indexesMainMet)));

allMeanLCModelSmall = mean(abs(meanConcDevLCModel(~indexesMainMet)));
allStdLCModelSmall  = mean(abs(stdConcDevLCModel(~indexesMainMet)));
allMeanProFitSmall  = mean(abs(meanConcDevProFit(~indexesMainMet)));
allStdProFitSmall   = mean(abs(stdConcDevProFit(~indexesMainMet)));

plusMinusSign = char(177);
metaboliteTable{2,indexTable}   = 'ProFit';
metaboliteTable{2,indexTable+1} = 'LCModel';
for indexMet = 1:numberOfMet
    metaboliteTable{indexMet+2,indexTable}   = [num2str(meanConcDevProFit(indexMet),'%.1f'),  plusMinusSign, num2str(stdConcDevProFit(indexMet),'%.1f')];
    metaboliteTable{indexMet+2,indexTable+1} = [num2str(meanConcDevLCModel(indexMet),'%.1f'), plusMinusSign, num2str(stdConcDevLCModel(indexMet),'%.1f')];
end

metaboliteTable{numberOfMet+3,indexTable}   = [num2str(allMeanProFit,'%.1f'),  plusMinusSign, num2str(allStdProFit,'%.1f')];
metaboliteTable{numberOfMet+3,indexTable+1} = [num2str(allMeanLCModel,'%.1f'),  plusMinusSign, num2str(allStdLCModel,'%.1f')];

metaboliteTable{numberOfMet+4,indexTable}   = [num2str(allMeanProFitMain,'%.1f'),  plusMinusSign, num2str(allStdProFitMain,'%.1f')];
metaboliteTable{numberOfMet+4,indexTable+1} = [num2str(allMeanLCModelMain,'%.1f'),  plusMinusSign, num2str(allStdLCModelMain,'%.1f')];

metaboliteTable{numberOfMet+5,indexTable}   = [num2str(allMeanProFitSmall,'%.1f'),  plusMinusSign, num2str(allStdProFitSmall,'%.1f')];
metaboliteTable{numberOfMet+5,indexTable+1} = [num2str(allMeanLCModelSmall,'%.1f'),  plusMinusSign, num2str(allStdLCModelSmall,'%.1f')];
end

%% reorganize the fitted concentrations to 2D tables
function [allSimulMetConc, allLCModelMetConc, allProFitMetConc, allLCModelMetConcDev, allProFitMetConcDev] = ...
    reorganizeMetConcentrationsToMatrices(metabolites, metabolitesLCModel, metabolitesProFit, ...
    concentrationsRm, concentrationsLCModel, concentrationsProFit, activeMetabolites, scalingOn)

iterationProFit = 4;
numberOfMet = length(metabolites);
allSimulMetConc = concentrationsRm;
allLCModelMetConc = zeros(size(allSimulMetConc));
allProFitMetConc = zeros(size(allSimulMetConc));
allLCModelMetConcDev = zeros(size(allSimulMetConc));
allProFitMetConcDev = zeros(size(allSimulMetConc));
for indexMetabolite = 1:numberOfMet
    simulatedMetConc = concentrationsRm(:, indexMetabolite);
    indexMetSim = find(strcmp(metabolites,'tCr(CH3)')); %'MMB'
    fittedMetConcLCModel = zeros(size(simulatedMetConc));
    for indexFit = 1:length(concentrationsLCModel)
        currentConcentrationsLCModel = concentrationsLCModel{indexFit};
        indexMetLCModel = find(strcmp(currentConcentrationsLCModel(1,:),'Cr')); %'Leu'
        RefConc = currentConcentrationsLCModel{2,indexMetLCModel};
        RefConcSim = concentrationsRm(indexFit,indexMetSim);
        if scalingOn
            scaling = RefConcSim/RefConc;
        else
            scaling = 1;
        end
        indexMetLCModel = find(strcmp(currentConcentrationsLCModel(1,:),metabolitesLCModel(indexMetabolite)));
        fittedMetConcLCModel(indexFit) = currentConcentrationsLCModel{2,indexMetLCModel}*scaling;
    end
    fittedMetConcProFit = zeros(size(simulatedMetConc));
    for indexFit = 1:length(concentrationsProFit)
        currentConcentrationsProFit = concentrationsProFit{indexFit}{iterationProFit};
        indexMetProFit = find(strcmp(activeMetabolites{iterationProFit},'Cr')); %'Leu'
        RefConc = currentConcentrationsProFit(indexMetProFit);
        RefConcSim = concentrationsRm(indexFit,indexMetSim);
        if scalingOn
            scaling = RefConcSim/RefConc;
        else
            scaling = 1;
        end
        indexMetProFit = find(strcmp(activeMetabolites{iterationProFit},metabolitesProFit(indexMetabolite)));
        fittedMetConcProFit(indexFit) = currentConcentrationsProFit(indexMetProFit)*scaling;
    end
    if strcmp(metabolitesProFit(indexMetabolite),'Leu')
        fittedMetConcProFit = fittedMetConcProFit *1e8;
    end
    allLCModelMetConc(:, indexMetabolite) = fittedMetConcLCModel;
    allProFitMetConc(:, indexMetabolite)  = fittedMetConcProFit;
    allLCModelMetConcDev(:, indexMetabolite) = (simulatedMetConc-fittedMetConcLCModel)./simulatedMetConc.*100;
    allProFitMetConcDev(:, indexMetabolite) = (simulatedMetConc-fittedMetConcProFit)./simulatedMetConc.*100;
end
end

%% plot Correlation plots between the fitted ProFit, LCModel concentrations and the true simulated concentrations
function figIdsCorr = plotCorrelationFigs(concentrationsRm, allLCModelMetConc, allProFitMetConc,...
    metabolitesToDisplay, numberOfMet, dataExportPathBase, paramKeyword, truncSuffix, figIdsCorr)

%TODO should I have this here? I don't know
plotCorrelationsGlx(concentrationsRm, allLCModelMetConc, allProFitMetConc,...
    metabolitesToDisplay,1:100);
plotCorrelationsGlx(concentrationsRm, allLCModelMetConc, allProFitMetConc,...
    metabolitesToDisplay,1:5:100);

if isempty(figIdsCorr)
    emptyFigureIdsCorr = true;
    figIdsCorr = cell(1,numberOfMet);
else
    emptyFigureIdsCorr = false;
end

% Main Figure = 12 subplots
figureMainRows = 4;
figureMainCols = 3;
metabolitesFigureMain = {'NAA(CH_2)', 'tCr(CH_2)', 'tCho+','Gly','Glu','Gln',...
    'Asp', 'GABA', 'Lac', 'GSH', 'NAAG', 'MM spectrum'};
figID_Main = figure();
mainFigureRunningIndex = 0;
% Supporting Figure = 5 subplots
figureSupportingRows = 3;
figureSupportingCols = 4;
metabolitesFigureSupporting = {'tCr(CH_3)', 'mI', 'sI', 'NAA(CH_3)','Tau'};
figID_Supporting = figure();
supportingFigureRunningIndex = 0;

colorOriginal = [0 0 0];
colorProFit = [0.49 0.18 0.56];
colorLCModel = [0.85 0.33 0.1];

for indexMetabolite = 1:numberOfMet  
    simulatedMetConc     = concentrationsRm(:, indexMetabolite);
    fittedMetConcLCModel = allLCModelMetConc(:, indexMetabolite);
    fittedMetConcProFit  = allProFitMetConc(:, indexMetabolite);
%     if emptyFigureIdsCorr
%         figIdsCorr{indexMetabolite} = figure;
%     end
%     figure(figIdsCorr{indexMetabolite});
    if sum(contains(metabolitesFigureMain, metabolitesToDisplay{indexMetabolite}))
        figure(figID_Main);
        mainFigureRunningIndex = mainFigureRunningIndex + 1;
        subplot(figureMainRows, figureMainCols, mainFigureRunningIndex);
%         if mod(mainFigureRunningIndex,figureMainCols)== 1 %first submission. Label on every row
        if mainFigureRunningIndex == 7 % Revision 1: Just one label
            plotYLabel = true;
        else
            plotYLabel = false;
        end
%         if round((mainFigureRunningIndex+1)/figureMainCols)== figureMainRows %first submission. Label on every column
        if mainFigureRunningIndex == 11 %Revision 1: Just one label
            plotXLabel = true;
        else
            plotXLabel = false;
        end
        if mainFigureRunningIndex == 3 %Revision 1: Just one legend
            plotLegend = true;
        else
            plotLegend = false;
        end
    elseif sum(contains(metabolitesFigureSupporting, metabolitesToDisplay{indexMetabolite}))
        figure(figID_Supporting);
        supportingFigureRunningIndex = supportingFigureRunningIndex + 2;
        if round((supportingFigureRunningIndex+1)/figureSupportingCols)== figureSupportingRows % offset plot
            supportingFigureRunningIndex = supportingFigureRunningIndex + 1;
        end
        subplot(figureSupportingRows, figureSupportingCols, supportingFigureRunningIndex-1:supportingFigureRunningIndex)
        if mod(supportingFigureRunningIndex-1,figureSupportingCols)<= 2
            plotYLabel = true;
        else
            plotYLabel = false;
        end
        if round((supportingFigureRunningIndex+1)/figureSupportingCols)>= figureSupportingRows-1
            plotXLabel = true;
        else
            plotXLabel = false;
        end
        if supportingFigureRunningIndex == 4 %Revision 1: Just one legend
            plotLegend = true;
        else
            plotLegend = false;
        end
    else
        warning('Metabolite not associated with either main or supporting figure: ', metabolitesToDisplay{indexMetabolite});
        figure();
        plotYLabel = true;
        plotXLabel = true;
    end
    
    if strcmp(metabolitesToDisplay(indexMetabolite),'MM spectrum')
        simulatedMetConc     = simulatedMetConc *1e-7;
        fittedMetConcLCModel = fittedMetConcLCModel *1e-7;
        fittedMetConcProFit  = fittedMetConcProFit *1e-7;
    end
    hold on
    scatter(simulatedMetConc, fittedMetConcLCModel, 15, 'MarkerEdgeColor', colorLCModel);
    scatter(simulatedMetConc, fittedMetConcProFit, 15, 'x', 'MarkerEdgeColor', colorProFit);
    offSetEnds = max(simulatedMetConc)*0.05;
    identityLine = min(simulatedMetConc)-offSetEnds:0.01:max(simulatedMetConc)+offSetEnds;
    plot(identityLine,identityLine,'color',colorOriginal);
    title(metabolitesToDisplay(indexMetabolite))
    
    if plotXLabel
        xlabel('$\bf c_{k}^{simulated} \,\, (mmol/kg)$', 'Interpreter', 'latex','FontSize', 11)
    end
    if plotYLabel
        ylabel('$\bf c_k^{fitted} \,\, (mmol/kg)$', 'Interpreter', 'latex','FontSize', 11)
    end
    if plotLegend
        handleLegend = legend('$LCModel \,\, c_k^{fitted}$', '$ProFit \,\, c_k^{fitted}$', '$Identity \, Line$', 'Location', 'best', 'Interpreter', 'latex', 'FontSize', 10);
        title(handleLegend,'Legend', 'FontSize',10);
    end

    xlim([min(identityLine),max(identityLine)])
    ylim([min(identityLine),max(identityLine)])
    
%     pathName = [dataExportPathBase, paramKeyword, '/correlations/'];
%     if ~exist(pathName, 'dir')
%         mkdir(pathName);
%     end
%     fileName = [metabolitesToDisplay{indexMetabolite}, '_', paramKeyword, truncSuffix];
%     saveas(figIdsCorr{indexMetabolite}, [pathName, fileName, '.tif']);
%     savefig(figIdsCorr{indexMetabolite}, [pathName,fileName, '.fig'],'compact');
end
end

%% plot Precision plots with boxplots showing percentages
function figIds = plotPrecisionFigs(figIds, numberOfMet, offsetPlot, secondaryFigures, plotColors, ...
    allLCModelMetConcDev, allProFitMetConcDev, metabolitesLabels)

    yLimValue = 100;
    if isempty(figIds)
%         figure('Position',[612,509,1132,469],'OuterPosition',[604,501,1148,562], 'Units','pixels')
%         figure('Position',[100,275,890,680],'Units','pixels')
%                     figIds{1} = subplot(2,1,2);
%                     figIds{2} = subplot(2,1,1);%, 'Position', [0.58,0.16,0.35,0.76], 'Position', [0.13,0.16,0.35,0.76]
%%Revision 1
%         figure('Position',[612,509,1132,469],'OuterPosition',[604,501,1148,562], 'Units','pixels')
        figure('Position',[9,10,1033,986],'Units','pixels')
        figIds{1} = subplot(1,2,2);
        figIds{2} = subplot(1,2,1);%, 'Position', [0.58,0.16,0.35,0.76], 'Position', [0.13,0.16,0.35,0.76]
        
%         figure('Position',[-908,-589,890,1400],'Units','pixels')
%         figIds{1} = subplot(4,1,2);
%         figIds{2} = subplot(4,1,1);
%         figIds{3} = subplot(4,1,4);
%         figIds{4} = subplot(4,1,3);
    end
    positionsTicks = [2.2:1.5:1.5*numberOfMet+1.5];
    positions = positionsTicks + offsetPlot;
    axes(figIds{1 + secondaryFigures})
    hold on
    boxPlotLCModel = boxplot(allLCModelMetConcDev, metabolitesLabels,'colors',plotColors,'symbol','+', 'positions',positions,'width',0.18,'orientation', 'horizontal');
    yticks(positionsTicks)
    yticklabels(metabolitesLabels)
    set(gca,'Ydir','reverse')
%     xtickangle(45)
    title('LCModel')
    xlim([-yLimValue,yLimValue]);
    ylim([positionsTicks(1)-1, positionsTicks(end)+1])
    set(gca, 'FontSize', 12)
    set(gca, 'YAxisLocation', 'right')
    xlabel('$Concentration\,\,change\,\,c^\%_{k}\,(\%)$', 'FontSize', 13, 'Interpreter', 'latex')
    set(boxPlotLCModel(:,:),'linewidth',1);
    xline(0,':', 'Color',[0.5 0.5 0.5]);
    %%
    axes(figIds{2 + secondaryFigures})
    hold on
    boxPlotProFit = boxplot(allProFitMetConcDev, metabolitesLabels,'colors',plotColors,'symbol','+', 'positions',positions,'width',0.18,'orientation', 'horizontal');
    yticks(positionsTicks)
    yticklabels(metabolitesLabels)
    set(gca,'Ydir','reverse')
%     ytickangle(45)
    ylim([1 27.5])
    title('ProFit')
    xlim([-yLimValue,yLimValue]);
    set(gca, 'FontSize', 12)
    set(gca, 'YAxisLocation', 'left')
    xlabel('$Concentration\,\,change\,\,c^\%_{k}\,(\%)$', 'FontSize', 13, 'Interpreter', 'latex')
    set(boxPlotProFit(:,:),'linewidth',1);
    xline(0,':', 'Color',[0.5 0.5 0.5]);
end

%% setup the table to show metabolite concentration changes
function [metaboliteTable, indexTable] = setupTable(numberOfMet, numOfParameters, metabolitesLabels)
metaboliteTable = cell(numberOfMet+4,numOfParameters*2+1);
metaboliteTable(3:numberOfMet+2,1) = metabolitesLabels;
metaboliteTable{numberOfMet+3,1}   = 'Mean';
metaboliteTable{numberOfMet+4,1}   = 'Mean Main Metabolites';
metaboliteTable{numberOfMet+5,1}   = 'Mean Small Metabolites';
indexTable = 0;
end

function plotCorrelationsGlx(concentrationsRm, allLCModelMetConc, allProFitMetConc,...
    metabolitesToDisplay, subsample)

colorOriginal = [0 0 0];
colorProFit = [0.49 0.18 0.56];
colorLCModel = [0.85 0.33 0.1];

indexGlu = find(strcmp(metabolitesToDisplay, 'Glu'));
indexGln = find(strcmp(metabolitesToDisplay, 'Gln'));

simulatedGluConc     = concentrationsRm(:, indexGlu);
simulatedGlnConc     = concentrationsRm(:, indexGln);
fittedGluConcLCModel = allLCModelMetConc(:, indexGlu);
fittedGlnConcLCModel = allLCModelMetConc(:, indexGln);
fittedGluConcProFit  = allProFitMetConc(:, indexGlu);
fittedGlnConcProFit  = allProFitMetConc(:, indexGln);

% subsample = 1:5:100;

figure();
plotYLabel = true;
plotXLabel = true;
plotLegend = true;

scatterSize = 50;
fontSize = 14;
hold on
scatter(fittedGluConcLCModel(subsample), fittedGlnConcLCModel(subsample), scatterSize, 'd', 'MarkerEdgeColor', colorLCModel);
scatter(simulatedGluConc(subsample), simulatedGlnConc(subsample), scatterSize, 'o', 'MarkerEdgeColor', colorOriginal);
scatter(fittedGluConcProFit(subsample), fittedGlnConcProFit(subsample), scatterSize, 'x', 'MarkerEdgeColor', colorProFit);
for index = subsample
    plot([fittedGluConcLCModel(index), simulatedGluConc(index), fittedGluConcProFit(index)],[fittedGlnConcLCModel(index), simulatedGlnConc(index), fittedGlnConcProFit(index)], 'color', colorOriginal);
end

offSetEnds = max(simulatedGluConc)*0.05;
identityLineGlu = min(simulatedGluConc)-offSetEnds:0.01:max(simulatedGluConc)+offSetEnds;
offSetEnds = max(simulatedGlnConc)*0.05;
identityLineGln = min(simulatedGlnConc)-offSetEnds:0.01:max(simulatedGlnConc)+offSetEnds;

set(gca, 'FontSize', fontSize)
if plotXLabel
    xlabel('$c_{Glu} \,\, (mmol/kg)$', 'Interpreter', 'latex','FontSize', fontSize+2)
end
if plotYLabel
    ylabel('$c_{Gln} \,\, (mmol/kg)$', 'Interpreter', 'latex','FontSize', fontSize+2)
end
if plotLegend
    handleLegend = legend('$LCModel$', '$Simulated$', '$ProFit$', 'Location', 'best', 'Interpreter', 'latex', 'FontSize', fontSize-2);
    title(handleLegend,'Legend');
end

xlim([min(identityLineGlu),max(identityLineGlu)])
ylim([min(identityLineGln),max(identityLineGln)])

end