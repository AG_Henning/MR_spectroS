function [processed_data, initial_data]=process(handles,data)


initial_data=data;

if ~data.Parameter.ReconFlags.isdcoffsetcorr
    if get(handles.checkbox_DcOffset,'Value');
    
        display('Data processing: DcOffset Correction');
        data.Parameter.Recon.DcOffsetCorrection='Yes';
        data.DcOffsetCorrection
    
    end
end

if ~data.Parameter.ReconFlags.isecc
    if get(handles.checkbox_EddyCurrents, 'Value');
    
        data.Parameter.Recon.EddyCurrentCorrection ='Yes';
        data.EddyCurrentCorrection;
    
    end
    
end


if ~data.Parameter.ReconFlags.iscombined
    if get(handles.checkbox_CombCoils,'Value');
    
        display('Data processing: Coil Combination');
        pause(2)
        selected_algorithm = get(handles.popupmenu_CombCoils,'String');
        selected_algorithm = selected_algorithm{ get(handles.popupmenu_CombCoils,'Value') }; 
    
        data.Parameter.Recon.CoilCombination.algorithm=selected_algorithm;
        data.CombineCoils;
    end
end


if get (handles.checkbox_Downsampling,'Value');
  
  display('Data processing: Downsampling');   
  pause(2)  
 selected_algorithm=   get(handles.popupmenu_Downsampling_alg,'String');
 selected_algorithm=  selected_algorithm{  get(handles.popupmenu_Downsampling_alg,'Value') };
 data.Parameter.DownsampleSettings.algorithm=selected_algorithm;
 
 selected_final_s=  get(handles.popupmenu_Downsampling_times,'String');
 selected_final_s = str2double (selected_final_s{ get(handles.popupmenu_Downsampling_times,'Value') });
 data.Parameter.ReconFlags.isoversampled=true;
 
 n_timepoints= size(data.Data{1},data.kx_dim)

 data.Parameter.DownsampleSettings.final_samples= n_timepoints/selected_final_s;
 n_timepoints/selected_final_s

 data.DownSample;
 
 n_timepoints= size(data.Data{1},data.kx_dim)
end

processed_data=data;
end

