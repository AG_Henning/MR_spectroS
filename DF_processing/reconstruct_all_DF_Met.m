function reconstruct_all_DF_Met()

pathName = 'DF data path';
subjects = { ...
    '1658' ...
    '1717' ...
    '2017' ...
    '3373' ...
    '3490' ...
    '5771' ...
    '6249' ...
    '6971' ... 
    '7338' ...    '7782' ...
    '9810' ...
    };

pathBase = pathToDataFolder(pathName, subjects);

TE = 24;

numberOfSubjects = length(subjects);
paths = cell(1,numberOfSubjects);
for indexSubj = 1:numberOfSubjects
    paths{indexSubj} = [pathBase subjects{indexSubj} '\\'];
end

files_TE24 = cell(1, numberOfSubjects);
water_files = cell(1, numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    files_TE24{indexSubject} = ls([paths{indexSubject}, '*_scout_FID*.dat']);
    water_files{indexSubject} = ls([paths{indexSubject}, '*_scout_wref_*.dat']);
end

addSinglet0ppm = 'Yes';

filesPath = cell(1,numberOfSubjects);
waterFilesPath = cell(1,numberOfSubjects);

for indexSubj = 1:numberOfSubjects
    filesPath{1, indexSubj} = [paths{indexSubj} files_TE24{1, indexSubj}];
    waterFilesPath{indexSubj} = [paths{indexSubj} water_files{indexSubj}];
end

indexSubjTEs = 0;
for indexSubj = 1:numberOfSubjects
    %% water data
    [a] = reconstructWater(waterFilesPath{indexSubj});
    filename = [paths{indexSubj} subjects{indexSubj} '_water.mat'];
    save(filename, 'a');
    a.ExportLcmRaw(paths{indexSubj}, strcat(subjects{indexSubj}, '_water'), addSinglet0ppm);
    %calculate scaling factor the summedSpectra individually according to water peak
    waterData = a.Data{1};
    maxWaterPeak = max(real(fftshift(fft(waterData))));     
    %scale the data for equalizing between subjects
    a.Data{1} = a.Data{1}./ maxWaterPeak;
    %create summed spectra
    if(indexSubj == 1)
        summedWater = a;
        newDataWater= zeros(length(a.Data{1}),1,1,1,1,1,1,1,1,1,1,numberOfSubjects);
    else
        oldDataWater = summedWater.Data{1};
        if (ndims(oldDataWater) ~= 12)
            newDataWater(:,1,1,1,1,1,1,1,1,1,1,1) = oldDataWater;
        else
            newDataWater(:,1,1,1,1,1,1,1,1,1,1,:) = oldDataWater;
        end
        newDataWater(:,1,1,1,1,1,1,1,1,1,1,indexSubj) = a.Data{1};
        summedWater.Data{1} = newDataWater;
    end 
    
    %% reconstruct Metabolite data
    a = reconstruct(filesPath{1, indexSubj}, true, true, [], false);
    filename = [paths{indexSubj} subjects{indexSubj} '_Metabolite_TE' num2str(TE) '.mat'];
    save(filename, 'a');
    a.ExportLcmRaw(paths{indexSubj}, strcat(subjects{indexSubj}, '_Metabolite_TE', num2str(TE)), addSinglet0ppm);
    %scale the data for equalizing between subjects
    a.Data{1} = a.Data{1}./ maxWaterPeak;
    
    % eliminate data with too many deleted averages from the summedSpectra
    
    deletedAverages = a.Parameter.DeleteMovedAveragesSettings.deletedAverages;
    if ~isempty(deletedAverages)
        numDeletedAverages = length(deletedAverages);
    else
        numDeletedAverages = 0;
    end
    
    if numDeletedAverages <= 2
        indexSubjTEs  = indexSubjTEs + 1;
        %create summed spectra
        if(indexSubjTEs == 1)
            summedSpectra = a;
            newData= zeros(length(a.Data{1}),1,1,1,1,1,1,1,2,1,1,numberOfSubjects);
        else
            oldData = summedSpectra.Data{1};
            if (ndims(oldData) ~= 12)
                newData(:,1,1,1,1,1,1,1,:,1,1,1) = oldData;
            else
                newData(:,1,1,1,1,1,1,1,:,1,1,:) = oldData;
            end
            newData(:,1,1,1,1,1,1,1,:,1,1,indexSubjTEs) = a.Data{1};
            summedSpectra.Data{1} = newData;
        end
    end    
end

%%

filename = [pathBase, 'Summed_water.mat'];
save(filename, 'summedWater');
summedWater = summedWater.AverageData;
save([pathBase, 'Summed_Averaged_water.mat'], 'summedWater');
summedWater.ExportLcmRaw(pathBase, 'Summed_Averaged_water', addSinglet0ppm);

filename = [pathBase, 'Summed_spectra_Metabolite_TE' num2str(TE) '.mat'];
summedSpectraTE = summedSpectra;
%store only the data, which was kept after eliminating the deleted ones
actualData = summedSpectraTE.Data{1}./indexSubjTEs;
summedSpectraTE.Data{1} = actualData(:,1,1,1,1,1,1,1,:,1,1,1:indexSubjTEs);
save(filename, 'summedSpectraTE');
%average across subjects
summedSpectraTE = summedSpectraTE.AverageData;
filename = [pathBase, 'Summed_Averaged_Metabolite_TE' num2str(TE) '.mat'];
save(filename, 'summedSpectraTE');
summedSpectraTE.ExportLcmRaw(pathBase, strcat('Summed_Averaged_Metabolite_TE', num2str(TE)), addSinglet0ppm);


summarize_Deleted_Averages(subjects, pathBase, TE, 'Metabolite')
close all
