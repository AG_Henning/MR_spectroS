function [absoluteConcentrations, metaboliteNames, subjectsAbs, data_path] = doAbsoluteConcentrationProcessing(compare, doMolal_Molar, TE_step)
% - TE_step: tells which of the TE steps we take from the file

switch compare
    case 'DF'
        outputFolder = 'Output\FWHMBA Results 7.5e-3\';
        pathName = 'DF data path';
    case 'MM'
        outputFolder = 'OutputMM\';
        pathName = 'DF data path';
    case 'Met'
        outputFolder = 'OutputMet\';
        pathName = 'DF data path';
    case 'DF_UF'
        outputFolder = 'Output_UF\';
        pathName = 'DF data path';
    case 'MM_UF'
        outputFolder = 'Output\';
        pathName = 'MM data path';
    case 'MM_Met'
        outputFolder = 'OutputMet\';
        pathName = 'MM data path';
end

sampleCriteria = {outputFolder};
data_path = pathToDataFolder(pathName, sampleCriteria);

TEs = [24; 32; 40; 52; 60];

doComparisonWithNoT2 = true;

tableConcentrationsFileName = ['tableConcentrationsSubjects.mat'];

if ~exist([data_path outputFolder tableConcentrationsFileName],'file')
    error('The required file does not exist!\n%s\nQuitting!', [data_path outputFolder tableConcentrationsFileName]);
end

tableConcentrationsSubjects = open([data_path outputFolder tableConcentrationsFileName]);

if strcmp(compare,'DF_UF')
    [relativeConcentrations, subjects, metaboliteNames]= extractConcentrations(tableConcentrationsSubjects.tableConcentrationsSubjects_abs_quantif, TE_step);
else
    [relativeConcentrations, subjects, metaboliteNames]= extractConcentrations(tableConcentrationsSubjects.tableConcentrationsSubjects, TE_step);
end

numOfSubjects = length(subjects);

NEX_H2O = 16;

switch compare
    case 'DF'
        load([data_path, 'tableRescaleFactorsDownfield'], 'tableRescaleFactors');
        NEX_signal = 96;
        metaboliteNames{1} = 'Leu';
%         % scale Cr to number of 1H
%         indexCr = find(strcmp(metaboliteNames, 'Cr'));
%         relativeConcentrations(:,indexCr) = relativeConcentrations(:,indexCr) * 3;
        % add NAADF with NAAB, and reshuffle them to be the last ones
        indexNAA = find(strcmp(metaboliteNames, 'NAA_DF'));
        indexNAAB = find(strcmp(metaboliteNames, 'NAAB'));
        if indexNAA < indexNAAB
            indexNAAB = indexNAAB - 1;
        end
        concNAA = relativeConcentrations(:, indexNAA);
        concNAAB = relativeConcentrations(:, indexNAAB);
        relativeConcentrations(:,indexNAA:end-1) = relativeConcentrations(:,indexNAA+1:end);
        metaboliteNames(indexNAA:end-1) = metaboliteNames(indexNAA+1:end);
        relativeConcentrations(:,indexNAAB:end-1) = relativeConcentrations(:,indexNAAB+1:end);
        metaboliteNames(indexNAAB:end-1) = metaboliteNames(indexNAAB+1:end);
        relativeConcentrations(:,end-1) = concNAAB;
        metaboliteNames{end-1} = 'NAA Broad';
        relativeConcentrations(:,end) = concNAA;
        metaboliteNames{end} = 'NAA';
%         relativeConcentrations(:,end+1) = nansum([concNAA,concNAAB],2);
%         metaboliteNames{end+1} = 'total NAA';
        metaboliteNames = strrep(metaboliteNames,'Cr','Cr-CH3');
        metaboliteNames = strrep(metaboliteNames,'Cr-CH3_CH2','Cr-CH2');
        metaboliteNames = strrep(metaboliteNames,'NAA_as','NAA-asp');
        metaboliteNames = strrep(metaboliteNames,'NAA_ac','NAA-ace');
        metaboliteNames = strrep(metaboliteNames,'tCho_P','tCho_PE');
        metaboliteNames = strrep(metaboliteNames,'Glu+Gln','Glx');
        metaboliteNames = strrep(metaboliteNames,'NAAB+NAA_DF','total NAA');
    case 'MM'
        load([data_path, 'tableRescaleFactorsMMBaseline'], 'tableRescaleFactors');
        NEX_signal = 64;
    case 'Met'
        load([data_path, 'tableRescaleFactorsMetabolite'], 'tableRescaleFactors');
        NEX_signal = 32;
    case 'DF_UF'
        load([data_path, 'tableRescaleFactorsDownfield'], 'tableRescaleFactors');
        NEX_signal = 96;        
    case 'MM_UF'
        load([data_path, 'tableRescaleFactors'], 'tableRescaleFactors');
        NEX_signal = 32;        
    case 'MM_Met'
        load([data_path, 'tableRescaleFactorsMetabolite'], 'tableRescaleFactors');
        NEX_signal = 32;
end

[T1_met, T2_met, metaboliteNames] = getRelaxationTimes(compare, metaboliteNames);

for indexSubject = 1:numOfSubjects
    subjects{indexSubject} = subjects{indexSubject}(1:4);
end

[absoluteConcentrations, subjectsAbs] = getAbsoluteConcentrations(data_path, relativeConcentrations, ...
    subjects, tableRescaleFactors, TE_step, doMolal_Molar, TEs(TE_step), NEX_H2O, NEX_signal, T2_met, T1_met);

if doComparisonWithNoT2
    [absoluteConcentrations2, ~] = getAbsoluteConcentrations(data_path, relativeConcentrations, ...
        subjects, tableRescaleFactors, TE_step, doMolal_Molar, TEs(TE_step), NEX_H2O, NEX_signal, NaN, T1_met);
end

eliminateMetabolites = {'Lac', 'Leu','Cho+GPC+PCh', 'mI+Glyc', 'Glu+Gln', 'Lip13a', 'Lip13b', 'Lip13c', 'Lip13d','Lip20', 'Lip13a+Lip13b'};

if strcmp(compare,'DF')
    eliminateMetabolites = {'NAD', 'Asp', 'Cr-CH3', 'Cr-CH2', 'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'mI', 'NAA-asp', 'NAA-ace', 'NAAG', 'tCho_PE', 'Scyllo', 'Tau', 'Glx', 'Lac', 'Leu','Cho+GPC+PCh', 'mI+Glyc', 'Glu+Gln', 'Lip13a', 'Lip13b', 'Lip13c', 'Lip13d','Lip20', 'Lip13a+Lip13b'};
    metaboliteNameOrder = {'DF58';'DF60';'DF61';'DF68';'DF70';'DF73';'DF75';'DF82';'DF83';'DF835';'DF85';'hCs';'NAA Broad';'NAA';'total NAA';};
    metaboliteNamesDisplay = {'DF5.75';'DF5.97';'DF6.12';'DF6.83';'DF7.04';'DF7.30';'DF7.48';'DF8.18';'DF8.24';'DF8.37';'DF8.49';'hCs';'NAA Broad';'NAA';'total NAA'};
    numberOfMet = length(metaboliteNameOrder);
    absoluteConcentrationsOrdered = zeros(size(absoluteConcentrations,1), numberOfMet);
    absoluteConcentrationsOrdered2 = zeros(size(absoluteConcentrations,1), numberOfMet);
    for indexMetabolite = 1:numberOfMet
        index = find(strcmp(metaboliteNameOrder(indexMetabolite), metaboliteNames));
        absoluteConcentrationsOrdered(:,indexMetabolite) = absoluteConcentrations(:,index);
        absoluteConcentrationsOrdered2(:,indexMetabolite) = absoluteConcentrations2(:,index);
    end
    absoluteConcentrations = absoluteConcentrationsOrdered;
    absoluteConcentrations2 = absoluteConcentrationsOrdered2;
    metaboliteNames = metaboliteNamesDisplay;
end
plotColor = {[0 0 1], [0 0.5 0]};
offsetXAxis = 0.166;
if doComparisonWithNoT2 
    [figureID] = plotConcentrations(absoluteConcentrations2, metaboliteNames, eliminateMetabolites, doMolal_Molar, plotColor{2}, +offsetXAxis);
    [figureID] = plotConcentrations(absoluteConcentrations, metaboliteNames, eliminateMetabolites, doMolal_Molar, plotColor{1}, -offsetXAxis, figureID);
else
    [figureID] = plotConcentrations(absoluteConcentrations, metaboliteNames, eliminateMetabolites, doMolal_Molar, plotColor{1}, offsetXAxis);
end

plusMinusSign = [' ' char(177) ' '];
microSign = [char(181) ];
meanAbsoluteConcentrations = mean(absoluteConcentrations, 'omitnan');
stdAbsoluteConcentrations = std(absoluteConcentrations, 'omitnan');
meanAbsoluteConcentrations2 = mean(absoluteConcentrations2, 'omitnan');
stdAbsoluteConcentrations2 = std(absoluteConcentrations2, 'omitnan');

numOfMetabolites = length(metaboliteNames);
table = cell(numOfMetabolites+1,2);
table2 = cell(numOfMetabolites+1,2);
if strcmp(doMolal_Molar,'Molar')
    table{1,2} = ['mmol/L' plusMinusSign 'std'];
    table2{1,2} = ['mmol/L' plusMinusSign 'std'];
else
%     table{1,2} = [microSign 'mol/g' plusMinusSign 'std'];
    table{1,2} = ['mmol/kg' plusMinusSign 'std'];
    table2{1,2} = ['mmol/kg' plusMinusSign 'std'];
end
table(2:end,1) = metaboliteNames';
table2(2:end,1) = metaboliteNames';
for index = 1:numOfMetabolites
    table{index+1,2} = [num2str(meanAbsoluteConcentrations(index), '%.2f') plusMinusSign  num2str(stdAbsoluteConcentrations(index), '%.2f')];
    table2{index+1,2} = [num2str(meanAbsoluteConcentrations2(index), '%.2f') plusMinusSign  num2str(stdAbsoluteConcentrations2(index), '%.2f')];
end

fileNameConcentrations = [data_path outputFolder 'Concentrations_' doMolal_Molar '.xlsx'];
xlswrite(fileNameConcentrations, table);
fileNameConcentrations2 = [data_path outputFolder 'Concentrations_' doMolal_Molar '_no_T2_correction.xlsx'];
xlswrite(fileNameConcentrations2, table2);
fclose all;
end


function [absoluteConcentrations, subjectsAbs] = getAbsoluteConcentrations(data_path, relativeConcentrations, ...
    subjects, tableRescaleFactors, TE_step, doMolal_Molar, TE, NEX_H2O, NEX_signal, T2_met, T1_met)
tissueConcentrationsPath = [data_path 'Tissue concentrations\'];
absoluteConcentrations = zeros(size(relativeConcentrations));
subjectsAbs = cell(size(subjects));
numOfSubjects = length(subjects);
indexSubjectsAbs = 0;
TR = 6000;
TE_water = TE;
TR_water = 6000;
for indexSubject = 1:numOfSubjects
    currentSubject = subjects{indexSubject};
    indexRescaleFactor = find(strcmp(tableRescaleFactors(:,1), currentSubject));
    if isempty(indexRescaleFactor)
        error('The tableRescaleFactors does not contain the subject %s', currentSubject);
    end
    if ~strcmp(tableRescaleFactors(indexRescaleFactor,2), 'TE24')
        error('The tableRescaleFactors does not contain TE24 at subject %s', currentSubject);
    end
    rescaleFactorMC = tableRescaleFactors{indexRescaleFactor+TE_step-1,3};
    
    tissueCsvFile = ls([tissueConcentrationsPath '*' currentSubject '.csv']);
    
    if ~isempty(tissueCsvFile)
        indexSubjectsAbs = indexSubjectsAbs + 1;
        subjectsAbs{indexSubjectsAbs} = currentSubject;
        
        tissueFractions = extractTissueFractionsFromCsv(tissueConcentrationsPath, tissueCsvFile);
        if exist('T2_met','var') && exist('T1_met','var')
            [absoluteConcentrations(indexSubjectsAbs, :)] = calculateMolal_MolarConcentrations(relativeConcentrations(indexSubject,:), tissueFractions, doMolal_Molar, TR, TE, TR_water, TE_water, ...
                NEX_H2O, NEX_signal, rescaleFactorMC, 40873, 1, T2_met, T1_met);
        else
            [absoluteConcentrations(indexSubjectsAbs, :)] = calculateMolal_MolarConcentrations(relativeConcentrations(indexSubject,:), tissueFractions, doMolal_Molar, TR, TE, TR_water, TE_water, ...
                NEX_H2O, NEX_signal, rescaleFactorMC, 40873, 1);
        end
        fclose all;
    else
        warning('No tissue percentage file found for subject %s', currentSubject)
    end
end

%resize the tables
subjectsAbs = subjectsAbs(1:indexSubjectsAbs);
absoluteConcentrations = absoluteConcentrations(1:indexSubjectsAbs, :);
end