classdef MR_spectroS 
    
    properties (Hidden, Constant)
        kx_dim  = 1;
        ky_dim  = 2;
        kz_dim  = 3;
        coil_dim = 4;
        dyn_dim = 10;   
        cardiac_ph_dim = 6;
        echo_dim = 7;
        loc_dim = 8;
        mix_dim = 9;
        extr2_dim = 11;
        meas_dim = 12;
             
    end 
 
     properties
         
         Data
         Parameter
       
     end
  
    
     methods
         
%//////////////////////////////////////////////////////////////////////////
%                                CONSTRUCTOR 
%//////////////////////////////////////////////////////////////////////////


        function this = MR_spectroS (midName,type)
            %midName : The pathname of the fid data or the MID #
            %        : 0 : "zero" constructor
            %        : 1 : Metabolite cycled data
            %        : 2 : Water data without receiver phase
            %        : 3 : water signal with receiver phase
            %        : 4 : JPRESS data
           
            
             
            this.Parameter           = Parameter(); 
            this.Parameter.Headers   = Headers();
            
              if ~exist('type','var') 
                  type=0;
              end
            
        
            if (~exist('midName','var') || isempty(midName) )  
                [fileName,pathName]  = uigetfile({'*.dat;*.DAT','DAT files (*.dat)'},'Please select the SIEMENS .dat file');
                midName                  = strcat(pathName, fileName);
                this                     = this.DataReArrange(midName,type);          
                     
            else
                 
                 if midName ~=0 
                     this                     =  this.DataReArrange(midName,type);   
                 end    
            end
            
            
          

            
            
        end

%//////////////////////////////////////////////////////////////////////////
%                             METHODS-FUNCTIONS    
%//////////////////////////////////////////////////////////////////////////        
        
        
        %Methods-Functions
        this                      = CombineCoils( this , weights);
        this                      = RemoveExtraPoints( this );
        this                      = ZeroFilling( this, zerroFillFactor );
        this                      = DcOffsetCorrection( this );
        this                      = Filtering( this );
        this                      = Truncate( this, truncPoint)
        this                      = RemoveFiltering( this );
        this                      = ExportLcmRaw( this, path, name, advoigdSinglet, makeBasisExport, referencePeakPpm, scalingFactorRefPeak, MC_mix);
        this                      = ExportMruiText(this, path, name, nucleus);
        this                      = AverageData( this );
        this                      = DeleteCoilChannels( this, coilChannels2Delete );
        this                      = SeparateWaterSpectra( this, afterNAverages );
        this                      = FrequencyAlign( this );
        this                      = PhaseCorrection( this );
        [thisFittedData, this]    = Hsvd (this);
        this                      = Normalisation( this );
        this                      = Rescale( this );
        this                      = ReconData( this );
        this                      = Sorting( this )
        this                      = ShiftTimePoints(this, pointStop);
        this                      = UndoPhaseCycling( this );
        this                      = fMRSAverage( this );
        this                      = fMRSMovingAverage( this );
        this                      = fMRSDifferenceGUI( this );
        this                      = fMRSDifference( this );
        this                      = Denoise( this );
        this                      = ScaleData(this,scalingFactor);
        this                      = ApplyFrequencyShift(this, frequencyShifts);
        this                      = FrequencyAlignFreqDomain(this);
        this                      = AlignWaterToPpm (this);
        this                      = SplineSmoothing(this, smoothingFactor, doPlotting);
        this                      = DeleteMovedAverages(this);
        this                      = CorrectMCSubtractionArtifacts(this);
        this                      = ImportMruiText(this, path, fileName);
        stats                     = Stats(this,list);
        y                         = convertPPMtoIndex(this, x, zeroFillFactor)
        ppm                       = getPpmVector(this, zeroFillFactor)
        this                      = Phase0_correction_31P(this);
        this                      = MissingPointPrediction(this);
        this                      = ISIS_Averaging(this);
        this                      = ISIS_Scheme(this);
        this                      = ReverseMC(this);
        [this, this2, weights]    = ISIS_main_processing(this,verbose);
        this                      = ISIS_prep_freqAlign(this, this2, weights);
        this                      = FrequencyAlignISIS(this,freqShiftsISISBlock,freqShiftsISISSteps);
        this                      = Anonimize_MR_spectroS(this, subjectNumber);
        
        
%//////////////////////////////////////////////////////////////////////////
%                       function this=DataReArrange(this,fid, type)  
%//////////////////////////////////////////////////////////////////////////        
        function this=DataReArrange(this,fid,type)

              tmp      = mapVBVD(fid);
              mrprot   = tmp.hdr;
              filename = tmp.image.filename;
            
             %############################################################
             % Order of raw data:
             %  1) Columns
             %  2) Channels/Coils
             %  3) Lines
             %  4) Partitions
             %  5) Slices
             %  6) Averages
             %  7) (Cardiac-) Phases
             %  8) Contrasts/Echoes
             %  9) Repetitions
             % 10) Sets
             %#############################################################
             
             [pathName,name,ext] = fileparts(filename);
             fileName = strcat(name,ext);
             this.Parameter.Filepath  = pathName;
             this.Parameter.Filename  = fileName;
             
             data   =tmp.image(''); %Read the data
             
 
              
              
            
             
             
             %% New dimensions
             % 1) Columns
             % 2) Lines
             % 3) Partitions
             % 4) Coils
             % 5) Slices
             % 6) Cardiac Phases
             % 7) Echos
             % 8) NaN
             % 9) NaN (New Mixes)
             % 10) Repetitions
             % 11) Sets
             % 12) Averages 
         
            permData            = double(permute(data,[1 3 4 2 5 7 8 11 12 9 10 6]));
            this.Data{1}        = permData;
            clear data;
            
                            
            
            if isfield(tmp,'phasestab') %water reference is acquired
                waterData                                     = tmp.phasestab('');
                waterData                                     = double(permute(waterData,[1 3 4 2 5 7 8 11 12 9 10 6]));
                waterData                                     = repmat(waterData, [1 1 1 1 1 1 1 1 2 1 1 1]);
                waterData(:, :, :, :, :, :, :, :, 1, :, :, :) = permData(:, :, :, :, :, :, :, :, 1, :, :, :);
                this.Data{1}                                  = waterData;
            end
            
            clear permData waterData;
             

%//////////////////////////////////////////////////////////////////////////
%                              READ HEADERS
%//////////////////////////////////////////////////////////////////////////


    this.Parameter.Headers   = this.Parameter.Headers.readHeaders(mrprot);


%//////////////////////////////////////////////////////////////////////////
%                              ADDITIONAL PARAMETERS 
%//////////////////////////////////////////////////////////////////////////       
             %1)
             %samplesBeforeEcho is saved in the FreeParameter[0]. The
             %acquition starts a number of sample points before the echo to
             %let the filter settle
             this.Parameter.Headers.SamplesBeforeEcho    =   tmp.image.freeParam(1);
             
             
             %2)
             %In the modified version of the STEAM sequency the user can
             %set after how many acquititions he want to aquire a spectrum
             %without water suppresion for EDC
             this.Parameter.Headers.AfterHowManyAverages =  tmp.image.freeParam (2);
             
             %3)
             %In some spectroscopy sequences the oversampling samples is
             %not exact the double of the set number since also extrapoints
             %are aquired helping for downsampling afterwards and letting 
             % filter settle 
             
             if size(this.Data{1},this.kx_dim)~= this.Parameter.Headers.VectorSize_OS
                 
                 warning(strcat('NCha ~= this.Parameter.Headers.VectorSize_OS.',...
                     ' Seems that extra points were acquired !'));
                 
                 this.Parameter.ReconFlags.isContainsExtraPoints= true;
             
             end
             
             
            if isfield(tmp,'phasestab') %water reference is acquired
              this.Parameter.Headers.WaterSpectra = true;  
            end
             
             
             %4)
             % If NRep == 1 then the data does not have to be sorted
             if size(this.Data{1},this.dyn_dim)==1;
                 this.Parameter.ReconFlags.isSorted = true;
     
             end
             

         
            switch type
                  case 1 %MC + Detector Phase Cycling
                    this.Parameter.ReconFlags.isMetaboliteCycling=1;
                    this = this.UndoPhaseCycling;
                 case  2 %water signal without receiver phase
                    this.Data{1} = cat(this.mix_dim,this.Data{1},this.Data{1});
                    this.Parameter.EddyCurrSettings.numberOfWaterSignals = size(this.Data{1},this.meas_dim);
                 case  3 %water signal with receiver phase
                    this.Data{1} = cat(this.mix_dim,this.Data{1},this.Data{1});
                    this.Parameter.EddyCurrSettings.numberOfWaterSignals = size(this.Data{1},this.meas_dim);
                    this = this.UndoPhaseCycling;
                case 4 %J resolved spectra
                    this.Parameter.ReconFlags.isJresolved = 1;
                    this = this.Sorting;
                    
                    
             end   
        end
          
             
             
             
     end
     
     methods(Static)
             %functions for interactive actions
        smoothedArray             = smoothArray(data, smoothingFactor, doPlotting);
        missingPointPredArrray    = missingPointPredictionArray(data, imiss, T_factor, polyOrder);
     end
end
          


       
        
   