function [psi corr_mtx_dB cov_mtx R] = noise_correlation(noise)
% computes noise correlation matrix
% input is 3D multichannel complex noise (no RF) (Nsamp,channel)
% steen code

 %% CMRR
% nCha = size(noise,2);
% psi = zeros(nCha);
% for i1=1:nCha % number of channels
%     for i2=1:nCha % number of channels
%         psi(i1,i2) = mean(noise(:,i1).*conj(noise(:,i2)));
%     end
% end
psi = cov(noise).';


cov_mtx = abs(psi)-diag(diag(psi));    % Noise covariance matrix

scaling2 = sqrt(diag(psi));
% scaling = scaling2 .\ scaling2(1);
psi2 = diag(1./scaling2)*psi*diag(1./scaling2);

corr_mtx = abs(psi2)-diag(diag(psi2));   % Noise correlation matrix

corr_mtx_dB = (20*log10(corr_mtx));

figure
imagesc(abs(psi))
title('Covariance matrix');
axis square
colorbar


figure
imagesc(corr_mtx_dB)
title('Correlation matrix [dB]');
axis square
colorbar


% pehses:
R = cov(bsxfun(@rdivide,bsxfun(@minus,noise,mean(noise,1)),std(noise,[],1))).';

figure
imagesc(abs(R))
title('Correlation matrix (scaled to 1)');
axis square
colorbar

