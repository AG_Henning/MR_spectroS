function varargout = noiseSettings(varargin)
% noiseSettings MATLAB code for noiseSettings.fig
%      noiseSettings, by itself, creates a new noiseSettings or raises the existing
%      singleton*.
%
%      H = noiseSettings returns the handle to a new noiseSettings or the handle to
%      the existing singleton*.
%
%      noiseSettings('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in noiseSettings.M with the given input arguments.
%
%      noiseSettings('Property','Value',...) creates a new noiseSettings or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before settings_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to settings_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help noiseSettings

% Last Modified by GUIDE v2.5 09-Oct-2014 15:25:57


%##########################################################################

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @settings_OpeningFcn, ...
                   'gui_OutputFcn',  @settings_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before noiseSettings is made visible.
function settings_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to noiseSettings (see VARARGIN)
if nargin == 4
    handles.hInput = varargin{1};
end
% Choose default command line output for noiseSettings
handles.output = hObject;



%---------------Initialise the shown values, sliders etc-------------------

main_handles    = handles.hInput.SelectedProcessingValues;
PeakRange       = get(main_handles,'PeakRange');
NoiseRangeFD      = get(main_handles,'NoiseRangeFD');
NoiseRangeTD    = get(main_handles,'NoiseRangeTD');
flagTimeDomain  = get(main_handles,'NoiseTimeDomain');

set(handles.edit_peak, 'String', num2str(PeakRange));

set(handles.edit_noise, 'String', num2str(NoiseRangeFD));
set(handles.edit_NoiseRangeTD, 'String', num2str(NoiseRangeTD));
set(handles.checkbox_timeDomain, 'Value', flagTimeDomain);


if flagTimeDomain
    set(handles.edit_NoiseRangeTD,'Enable','on')
else
    set(handles.edit_NoiseRangeTD,'Enable','off')
end

%--------------------------------------------------------------------------



% Update handles structure
guidata(hObject, handles);


% UIWAIT makes noiseSettings wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = settings_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



%##########################################################################


function edit_peak_Callback(hObject, eventdata, handles)
 
main_handles = handles.hInput.SelectedProcessingValues;

current_value = get(hObject,'String');
current_value = str2num(current_value);
current_value = convertPPMtoIndex(current_value,handles);
set(main_handles,'PeakRange',current_value); 
displaySpect(handles.hInput)



function edit_noise_Callback(hObject, eventdata, handles)



main_handles   = handles.hInput.SelectedProcessingValues;

current_value = get(hObject,'String');
current_value = str2num(current_value);

%current_value = convertPPMtoIndex(current_value,handles);
%current_value(2)-current_value(1)
set(main_handles,'NoiseRangeFD',current_value); 
displaySpect(handles.hInput);



function edit_NoiseRangeTD_Callback(hObject, eventdata, handles)

current_value= get(hObject,'String');

main_handles = handles.hInput.SelectedProcessingValues;
set(main_handles,'NoiseRangeTD',str2num(current_value)); 
displaySpect(handles.hInput);




% --- Executes on button press in checkbox_timeDomain.
function checkbox_timeDomain_Callback(hObject, eventdata, handles)

current_value= get(hObject,'Value');


if current_value
    set(handles.edit_NoiseRangeTD,'Enable','on')
else
    set(handles.edit_NoiseRangeTD,'Enable','off')
end

main_handles = handles.hInput.SelectedProcessingValues;
set(main_handles,'NoiseTimeDomain',current_value);
displaySpect(handles.hInput);


% --- Executes on button press in pushbutton_ok.
function pushbutton_ok_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1);


function y = convertPPMtoIndex(x,handles)
   
   this    = handles.hInput.MRSpectroMatrix.getCurrent;
   zeroPPM = handles.hInput.DisplayProperties.zeroPoint;
   
   txFreq = this.Parameter.Headers.ScanFrequency;
   bw     = this.Parameter.Headers.Bandwidth_Hz;
   bw     = linspace(-bw/2,bw/2,size(this.Data{1},this.kx_dim));
   ppm    = bw/(txFreq*1e-6)+zeroPPM;
   index1 = ppm >= x(1);
   index2 = ppm <= x(2);
   y      = index1 & index2;
   y =[find(y==1,1,'first') find(y==1,1,'last')];
   




%##########################################################################
%##########################################################################

% --- Executes during object creation, after setting all properties.
function edit_noise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit_peak_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_peak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes during object creation, after setting all properties.
function edit_NoiseRangeTD_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_NoiseRangeTD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
