function doFittingAndT2Calculations_MM(fitAllSubjects)

pathName = 'MM T1 data path';
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
fitAllSubjects = true;
orderTI1s = {'2360' '2150' '2000' '1900' '1800' '1300' '1300' '1300' '1250' '1200' '1050'};
orderTI2s = {'625' '600' '575' '550' '525' '80' '60' '20' '20' '20' '238'};
subjects = {'1717';'2016';'3490';'9810'};

extensionTable = '.table';
extensionCoord = '.coord';
defaultTI = 'TI1_2360_TI2_625';
defaultSubject = 'XXXX';
defaultLCModelUser = 'tborbath';

if fitAllSubjects
    controlFilesBase = 'fitsettings_Subject_XXXX_';
    controlFilesBaseTI1 = strcat(controlFilesBase, 'TI1_');
    controlFilesBaseSuffix = '_Cre.control';
    outputFileNameBase = strcat(defaultSubject, '_TI1_');
else
    controlFilesBase = 'fitsettings_Summed_Averaged_';
    controlFilesBaseTI1 = strcat(controlFilesBase, 'TI1_');
      %     controlFilesBaseSuffix = '_Cre.control';
    %     controlFilesBaseSuffix = '_Cre_PpmPfeuffer.control';
    controlFilesBaseSuffix = '_Cre.control';
    outputFileNameBase = 'Summed_Averaged_TI1_';
    
end

%% file paths setup
controlFilesPathRemote = '/Desktop/MM_T1/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/MM_T1/Output/';
LCModelOutputPath = [localFilePathBase, 'Output/'];
defaultControlFile = strcat(controlFilesBase, 'Default', controlFilesBaseSuffix);


if fitAllSubjects
    subjectsPath = strcat(subjects, '/');
else
    subjects = {'Summed'};
    subjectsPath = {''};
end

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath);
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath);
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath);
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath);

%% basic configurations
LCModelTableFits = strcat(outputFileNameBase, orderTI1s,  '_TI2_',orderTI2s, extensionTable);
LCModelTableFitsCoord = strcat(outputFileNameBase, orderTI1s, '_TI2_', orderTI2s, extensionCoord);
LCModelOutputFiles = strcat(outputFileNameBase, orderTI1s, '_TI2_', orderTI2s);
LCModelControlFiles = strcat(controlFilesBaseTI1, orderTI1s, '_TI2_', orderTI2s, controlFilesBaseSuffix);

numberOfTIs = length(orderTI1s);
numberOfSubjects = length(subjects);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

%% create the control file series
if fitAllSubjects
    createLCModelConfigs_MMT1(controlFilesPathLocal, defaultControlFile, orderTI1s, orderTI2s, defaultTI, defaultLCModelUser, LCModelCallerInstance.LCModelUser, subjects, defaultSubject);
else
    % call the Configuration file creator without subject configuration
    createLCModelConfigs_MMT1(controlFilesPathLocal, defaultControlFile, orderTI1s, orderTI2s, defaultTI, defaultLCModelUser, LCModelCallerInstance.LCModelUser);
end

%% load the table with the deleted averages
tableDeletedAverages = zeros(2);
% load([localFilePathBase, 'tableDeletedAverages.mat'], 'tableDeletedAverages');

%% do the actual LCModel fitting and T2 calculation
for indexCurrentSubject = 1:numberOfSubjects
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    if fitAllSubjects
        currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
        currentLCModelTableFits = strrep(LCModelTableFits, defaultSubject, subjects{indexCurrentSubject});
        currentLCModelTableFitsCoord = strrep(LCModelTableFitsCoord, defaultSubject, subjects{indexCurrentSubject});
    else
        currentControlFiles = LCModelControlFiles;
        currentOutputFiles = LCModelOutputFiles;
        currentLCModelTableFits = LCModelTableFits;
        currentLCModelTableFitsCoord = LCModelTableFitsCoord;
    end
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
    
    %%extract the TEs to eliminate
    eliminateTIs = {};
    indexTableSubject = find(strcmp(subjects{indexCurrentSubject}, tableDeletedAverages(:,1)), 1);
    %if the index is empty, then we don't eliminate any TE
    if (~isempty(indexTableSubject))
        % we assume that
        for indexTableDeleteTI = 1:numberOfTIs
            indexCurrentTI = tableDeletedAverages{indexTableSubject+indexTableDeleteTI-1,2};
            if strcmp(indexCurrentTI,['TI' orderTI1s{indexTableDeleteTI}]) == 1
                deletedAverages = tableDeletedAverages(indexTableSubject+indexTableDeleteTI-1,3:end);
                deletedAveragesNum = cell2mat(deletedAverages);
                if (length(deletedAveragesNum) > 2) %TODO check what is best 2 or 0
                    eliminateTIs{end+1} = orderTI1s{indexTableDeleteTI};
                end
            else
                warning('Subject %s does not have the matching TEs in the table of deleted averages.',subjects{indexCurrentSubject});
            end
        end
    else
        warning('Subject %s not found in the table of deleted averages.',subjects{indexCurrentSubject});
    end

    %% calculate the available TEs
    availableTIs = {};
    availableLCModelTableFits = {};
    availableLCModelTableFitsCoord = {};
    indexTI = 0;
    for indexCurrentTI = 1:numberOfTIs
        foundTI  = find(strcmp(orderTI1s{indexCurrentTI}, eliminateTIs), 1);
        if isempty(foundTI)
            indexTI = indexTI +1;
            availableTIs{indexTI} = orderTI1s{indexCurrentTI};
            availableLCModelTableFits{indexTI} = currentLCModelTableFits{indexCurrentTI};
            availableLCModelTableFitsCoord{indexTI} = currentLCModelTableFitsCoord{indexCurrentTI};
        end
    end
    numOfAvailableTIs = length(availableTIs);
%     %% do the T2 exponential fits
%     [tableConcentrations{indexCurrentSubject}, tableT2s{indexCurrentSubject}] = calculate_T2(availableLCModelTableFits, ...
%         subjectsLCModelOutputPath{indexCurrentSubject}, false, availableTIs, 'MM');
%     
%     %% mean Rsquared
%     Rsquares = str2double(tableT2s{indexCurrentSubject}(3,2:end));
%     meanRsquared =  mean(Rsquares);
%     
%     %% mean CRLB
%     CRLBs = cell2mat((tableConcentrations{indexCurrentSubject}(2:numberOfTes + 1,3:3:end)));
%     meanCrlbTEs = mean(CRLBs,2, 'omitnan');
%     meanCrlb = mean(meanCrlbTEs);
%     
%     %% T2 of Cr
%     metNames = tableT2s{indexCurrentSubject}(1,2:end);
%     indexes = strfind(metNames,'Cr');
%     indexCr = find(not(cellfun('isempty', indexes))) + 1;
%     T2Cr = tableT2s{indexCurrentSubject}(2,indexCr);
%     
%     %% display means
%     table(1,1) = {'TE'};
%     table(1,2:numOfAvailableTIs+1) = availableTIs(1:end);
%     table(2,1) = {'CRLB'};
%     table(2,2:numOfAvailableTIs+1) = num2cell(meanCrlbTEs(1:end));
%     table(3,1) = {'meanCRLB'};
%     table(3,2) = num2cell(meanCrlb);
%     table(4,1) = {'meanR2'};
%     table(4,2) = num2cell(meanRsquared);
%     table(5,1) = {'T2_Cr'};
%     table(5,2) = T2Cr;
%     display(table)
%     
%     %% save table to file
%     exportFileXlsx = [LCModelOutputPath ,'Quantification_Summaries.xlsx'];
%     startIndex = 1+ 7 *(indexCurrentSubject-1);
%     cellPositionTitle = ['A' num2str(startIndex)];
%     xlswrite(exportFileXlsx, {['Subject: ' subjects{indexCurrentSubject}]}, 1, cellPositionTitle);
%     cellPosition = ['A' num2str(startIndex + 1)];
%     xlswrite(exportFileXlsx, table, 1, cellPosition);
%     
%     %% calculate FWHMs
%     if indexCurrentSubject == 1
%         [fwhms, orderMetFWHM] =calculate_FWHM(currentLCModelTableFitsCoord, subjectsLCModelOutputPath{indexCurrentSubject}, false, 'MM');
%         tableFWHM = zeros(numberOfSubjects, size(fwhms,1), size(fwhms,2));
%         tableFWHM(indexCurrentSubject,:,:) = fwhms;
%     else
%         tableFWHM(indexCurrentSubject,:,:) = calculate_FWHM(currentLCModelTableFitsCoord, subjectsLCModelOutputPath{indexCurrentSubject}, false, 'MM');
%     end
end

% if fitAllSubjects
%     fileNameSuffix = 'Subjects';
% else
%     fileNameSuffix = 'Summed';
% end
% tableFWHM_name = 'tableFWHM';
% tableT2s_name = 'tableT2s';
% orderMet_name = 'orderMetFWHM';
% tableConcentrations_name = 'tableConcentrations';
% tableFWHM_nameSuffixed = [tableFWHM_name fileNameSuffix];
% tableT2s_nameSuffixed = [tableT2s_name fileNameSuffix];
% orderMet_nameSuffixed = [orderMet_name fileNameSuffix];
% tableConcentrations_nameSuffixed = [tableConcentrations_name fileNameSuffix];
% eval( [tableFWHM_nameSuffixed, '=',  tableFWHM_name]);
% eval( [tableT2s_nameSuffixed, '=',  tableT2s_name]);
% eval( [orderMet_nameSuffixed, '=',  orderMet_name]);
% eval( [tableConcentrations_nameSuffixed, '=',  tableConcentrations_name]);
% save([LCModelOutputPath, tableFWHM_nameSuffixed, '.mat'], tableFWHM_nameSuffixed, orderMet_nameSuffixed);
% save([LCModelOutputPath, tableT2s_nameSuffixed, '.mat'], tableT2s_nameSuffixed)
% save([LCModelOutputPath, tableConcentrations_nameSuffixed, '.mat'], tableConcentrations_nameSuffixed)
end