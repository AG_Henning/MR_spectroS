function  this  = UndoPhaseCycling( this )

if this.Parameter.ReconFlags.isUndoDetectorCycling
    warning('The detector phase cycling has been already removed!!! ')
end

 oldData        = this.Data{1};
 oldData(:,:,:,:,:,:,:,:,:,:,:,2:2:end)         =   -oldData(:,:,:,:,:,:,:,:,:,:,:,2:2:end);
 this.Data{1}   = oldData ;
 
 
 
 this.Parameter.ReconFlags.isUndoDetectorCycling = true;

end

