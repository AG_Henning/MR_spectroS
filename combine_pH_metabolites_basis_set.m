function combine_pH_metabolites_basis_set()

plotBasis = true;

pH_values = [6.9:0.01:7.15]';
pH_valuesSting = cellstr(num2str(pH_values, '%.2f'));

pH_metabolites_bases = {'hCs_imidazole'; 'hist_imidazole'};
metabolitesToMix = { {'hCs_alanine_deGraaf'; 'hCs_GABA_deGraaf'}; ...
    {'histidine_alanine_de_Graaf'}};
combined_metabolite_names = {'hCs'; 'hist'};
% contains the scaling factors of 'metabolitesToMix' first , followed by
% the factor 'pH_metabolites_bases'
factorsToMix = {{1;1;1};...
    {1; 1}};


pathName = 'DF data path';
sampleCriteria = {};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
filePathBase = [localFilePathBase 'Basis_sets\sLASER_new_DF_pH_sweep\TE'];
orderTEs = {'24' '32' '40' '52' '60'};
filePaths = strcat(filePathBase, orderTEs , '\');

for indexTE = 1 : length(orderTEs)
    filePath = filePaths{indexTE};
    for indexMetabolite = 1 : length(pH_metabolites_bases)
        pH_metabolites = strcat(pH_metabolites_bases{indexMetabolite}, '_pH_', pH_valuesSting);
        filenames_pH = strcat(combined_metabolite_names{indexMetabolite}, '_pH_', pH_valuesSting);
        %
        dataMix           = cell(1,length(factorsToMix{indexMetabolite}));
        metaboliteNameMix = cell(1,length(factorsToMix{indexMetabolite}));
        singletPresentMix = cell(1,length(factorsToMix{indexMetabolite}));
        %read in non pH dependent metabolite parts
        for indexMetabolitesToMix =  1:length(metabolitesToMix{indexMetabolite})
            [dataMix{indexMetabolitesToMix}, ~, ~, metaboliteNameMix{indexMetabolitesToMix}, ...
                singletPresentMix{indexMetabolitesToMix}] = ...
                ImportLCModelBasis(filePath, metabolitesToMix{indexMetabolite}{indexMetabolitesToMix}, ...
                plotBasis);
        end
        %combine
        combinedData = factorsToMix{indexMetabolite}{1} * dataMix{1};
        for indexMetabolitesToMix =  2:length(metabolitesToMix{indexMetabolite})
            % average the contributions. Don't worry about the
            % ref. singlet, it does not influence the fitting
            combinedData = combinedData + factorsToMix{indexMetabolite}{indexMetabolitesToMix} * ...
                dataMix{indexMetabolitesToMix};
        end
        
        numberOfMetabolitesToMix = length(metabolitesToMix{indexMetabolite});
        %read in pH dependent metabolite parts
        for index_pH = 1 : length(pH_values)
            [dataComplex, bandwidth, scanFrequency, ~, ~] = ...
                ImportLCModelBasis(filePath, pH_metabolites{index_pH}, plotBasis);
            %combine
            combinedData_pH = combinedData + factorsToMix{indexMetabolite}{numberOfMetabolitesToMix} * ...
                dataComplex;
            
            dwellTimeMs = 1/bandwidth * 1e3;
            filename_pH = filenames_pH{index_pH};
            metaboliteName = filenames_pH{index_pH};
            addSinglet = false;
            %export
            ExportLCModelBasis(combinedData_pH', dwellTimeMs, scanFrequency, filePath, ...
                filename_pH, metaboliteName, addSinglet);
        end
    end
end
