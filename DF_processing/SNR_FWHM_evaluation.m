

load('D:\Software\Spectro Data\DATA_df\stats.mat', 'stats');
load('D:\Software\Spectro Data\DATA_df\statsWater.mat', 'statsWater');
sample = stats{1,1};
indexNAA_DF = find(strcmp(sample(:,1),'NAA_DF'));
indexH2O = find(strcmp(sample(:,1),'H2O'));
SNR_NAA_DF = sample{indexNAA_DF, 2};
FWHM_H2O = sample{indexH2O, 3};

SNR_NAA_DFs = zeros(size(stats));
FWHM_H2Os = zeros(size(stats));
for index1 = 1 : size(stats,1)
    for index2 = 1 : size(stats,2)
        sample = stats{index1,index2};
        SNR_NAA_DFs(index1, index2) = sample{indexNAA_DF, 2};
        FWHM_H2Os(index1, index2) = sample{indexH2O, 3};
    end
end

sample = statsWater{1,1};
indexH2O = find(strcmp(sample(:,1),'H2O'));
FWHM_H2O_ref = sample{indexH2O, 3};
FWHM_H2Os_ref = zeros(size(statsWater));
for index1 = 1 : size(stats,1)
    sample = statsWater{index1};
    FWHM_H2Os_ref(index1) = sample{indexH2O, 3};
end

meanSNR_NAA_DF_TE24 = mean(SNR_NAA_DFs(:,1))
stdSNR_NAA_DF_TE24 = std(SNR_NAA_DFs(:,1))
meanSNR_NAA_DF_TE60 = mean(SNR_NAA_DFs(:,5))
stdSNR_NAA_DF_TE60 = std(SNR_NAA_DFs(:,5))


meanFWHM_H2O_ref = mean(FWHM_H2Os_ref(:))
stdFWHM_H2O_ref = std(FWHM_H2Os_ref(:))
meanFWHM_H2O = mean(FWHM_H2Os(:))
stdFWHM_H2O = std(FWHM_H2Os(:))