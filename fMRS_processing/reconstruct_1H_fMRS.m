function [a] = reconstruct_1H_fMRS(currentFolder,fid_id, isMC, isInVivo,...
    nBlocks, nOnAverages, PCSteps, weights, saveResults, visualType,BOLDType,modulus)

%modulus variable for Lenkinski correction. Does not work for the fMRS data

nsamplesBeforeEcho = 4;

if ~exist('isInVivo', 'var')
    isInVivo = true;
end

if ~exist('saveResults', 'var')
    saveResults = true;
end

% if ~exist('modulus', 'var')
%     error('choose modulus type true or false');
% end

if nBlocks == 5
    nAvg2save = '#64';
elseif nBlocks == 10
    nAvg2save = '#32';
end

if isMC
    a = MR_spectroS(fid_id,1);
else
    a = MR_spectroS(fid_id);
end

%%
if nsamplesBeforeEcho ~= 0
    startpoint = nsamplesBeforeEcho + 1;
    endpoint   = size(a.Data{1},a.kx_dim)-(32-nsamplesBeforeEcho);
    data = squeeze(a.Data{1});
    data = data(startpoint:endpoint,:,:);
    dataNew(:,1,1,:,1,1,1,1,1,1,1,:) = data;
    a.Data{1} = dataNew;
    clear data dataNew
end

%% sort averages of fMRS in case that ON block does not contain whole phase
%cycle
if ~exist('nOnAverages', 'var')
    nOnAverages = false;
end

if ~exist('PCSteps', 'var')
    PCSteps = double(16);
end


if nOnAverages
    nSamples      = size(a.Data{1},a.kx_dim);
    nCoils        = size(a.Data{1},a.coil_dim);
    nAverages     = size(a.Data{1},a.meas_dim);
    data = squeeze(a.Data{1});
    PCSorted = zeros(nSamples,nCoils,nAverages);
    for k = 1:nCoils
        n = 1;
        for i = 1:(2*nOnAverages):nAverages
            for m = 0:(nOnAverages-1)
                PCSorted(:,k,n+m) = data(:,k,i+m);
            end
            if (n + m) == PCSteps * 2
                n = n + m + 2*PCSteps + 1;
            else
                n = n+nOnAverages;
            end
        end
        n = PCSteps * 2 + 1;
        for i = (nOnAverages+1):(2*nOnAverages):nAverages
            for m = 0:(nOnAverages-1)
                PCSorted(:,k,n+m) = data(:,k,i+m);
            end
            if (n + m) == PCSteps * 4
                n = n + m + 2*PCSteps + 1;
            else
                n = n+nOnAverages;
            end
        end
    end
    dataNew(:,1,1,:,1,1,1,1,1,1,1,:) = PCSorted;
    a.Data{1} = dataNew;
end


%%
if isInVivo
    %% truncate before anything
%     if exist('modulus','var') && (modulus == 1)
%         trunc_ms = 200;
%     else
        trunc_ms = 250;
%     end
    truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
    a = a.Truncate(truncPoint);
    
    a = a.FrequencyAlign;
end
if isMC
    if isInVivo
        a = a.Rescale;
    end
    a = a.ReconData;
end


%% %average data block-wise
if ~exist('nBlocks', 'var')
    a.Parameter.AverageSettings.nBlocks = 1;
else
    a.Parameter.AverageSettings.nBlocks = nBlocks;
end

% if exist('modulus','var') && (modulus == 0)
    a = a.AverageData;
% end

%eddy current correction
if isMC
    a = a.EddyCurrentCorrection;        %CARFUL: take old eddycorrection for fMRS data
else
    [FileName, PathName] = uigetfile('*.dat',['Select water reference for :' a.Parameter.Filename]);
    path = strcat(PathName,FileName);
    tmpData      = mapVBVD(path);
    tmpWaterReferenceData = tmpData.image(''); %Read the data
    waterReferenceData = double(permute(tmpWaterReferenceData,[1 3 4 2 5 7 8 11 12 9 10 6]));
    a = a.EddyCurrentCorrection(waterReferenceData);
end

%combine coils using SVD
if exist('weights','var')
    if ~isempty(weights)
        a = a.CombineCoils(weights);
    else
        a = a.CombineCoils;
    end
else
    a = a.CombineCoils;
end

% %%
% %prepare Lenkinski correction, correct only spectra, not water
% if exist('modulus','var') && (modulus == 1)
%     % setup the settings for the FrequencyAlignFreqDomain
%     a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
%     a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;
%     
%     %indices of Interest: given in ppm
%     % NAA peak
%     a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = 2.008;
%     
%     %set zeroFillingParameter to get smooth approximations
%     a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;
%     
%     %search area (+-) in ppm
%     if isInVivo
%         a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.1;
%     else
%         a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.2;
%     end
%     %spline filtering is not needed if the spectra are not too noisy.
%     %flag to do spline filtering
%     a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
%     %spline filtering coefficient
%     a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;
%     % do the actual Frequency Alignment
%     a = a.FrequencyAlignFreqDomain;
%     
%     ppmvector = a.getPpmVector();
%     [~, idx5] = min(abs(ppmvector-5.55));
%     idxend = length(ppmvector);
%     for idxAv = 1:size(a.Data{1},12)
%         currentDataFID = squeeze(a.Data{1}(:,1,1,1,1,1,1,1,1,1,1,idxAv));
%         currentData_spec = fftshift(fft(currentDataFID(:,1)));
%         
%         x = 0:50;
%         g = exp(-((x-0)./15) .^(2));
%         [~, idxHalf] = min(abs(g-0.5*max(g)));
%         g2 = g(idxHalf+2:end);
%         g2 = fliplr(g2)*(-1)+max(g);
%         filter = [ones(1,2241-45),g2,g(idxHalf:end)];
%         filter = [filter, zeros(1,length(currentData_spec)-length(filter),1)];
%         currentData_spec_mod = currentData_spec.*filter';
% %         figure
% %         plot(ppmvector,filter.*max(real(currentData_spec))); hold on
% %         plot(ppmvector,real(currentData_spec_mod))
%         
% %         currentData_fid_mod = ifft(ifftshift(abs(currentData_spec_mod));
%         currentData_fid_mod = ifft(ifftshift(currentData_spec_mod));
% %         currentData_fid_mod_trunc = currentData_fid_mod;
% %         currentData_fid_mod_trunc(truncPoint:end) = 0;
%         
% %         figure
% %         plot(ppmvector,real(fftshift(fft(currentDataFID))))
% %         hold on
% %         plot(ppmvector,real(fftshift(fft(currentData_fid_mod))))
% %         plot(ppmvector,real(fftshift(fft(currentData_fid_mod_trunc))))
% %         plot(ppmvector,real(fftshift(fft(currentDataFID-currentData_fid_mod_trunc))))
% %         
% %         figure
% %         plot(ppmvector,real(fftshift(fft((currentDataFID./2)))))
% %         hold on
% %         plot(ppmvector,real(fftshift(fft(real(currentData_fid_mod)))))
% %         plot(ppmvector,real(fftshift(fft(real(currentData_fid_mod_trunc)))))
% %         plot(ppmvector,real(fftshift(fft(real(currentData_fid_mod)-real(currentData_fid_mod_trunc)))))
% %         plot(ppmvector,real(fftshift(fft((currentDataFID./2)-real(currentData_fid_mod_trunc)))))
% %         set(gca, 'XDir','reverse')
% %         xlim([0.5 4.2]);
% %         
% %         figure
% %         plot(ppmvector,real(fftshift(fft((currentDataFID./2)))))
% %         hold on
% %         plot(ppmvector,real(fftshift(fft(abs(currentData_fid_mod)))))
% %         plot(ppmvector,real(fftshift(fft(abs(currentData_fid_mod_trunc)))))
% %         plot(ppmvector,real(fftshift(fft(abs(currentData_fid_mod)-abs(currentData_fid_mod_trunc)))))
% %         plot(ppmvector,real(fftshift(fft((currentDataFID./2)-abs(currentData_fid_mod_trunc)))))
% %         set(gca, 'XDir','reverse')
% %         xlim([0.5 4.2]);
%         
%         
%         
% %         val5 = currentData_spec(idx5);
% %         valend = currentData_spec(idxend);
% %         yval = linspace(val5,valend,(idxend-idx5));
% %         currentData_spec_mod = [currentData_spec(1:idx5); yval'];
% %         %         currentData_spec_mod = currentData_spec_mod-valend;
% %         currentData_fid_mod = ifft(ifftshift(currentData_spec_mod));
%         %flip tail 
%         tail = flipud(currentData_fid_mod(truncPoint:end,1));
%         tail = [tail; zeros(4096-length(tail),1)];
%         summedFIDs = tail + currentData_fid_mod;
%         %truncate fid to remove tail (LCModel doesn't like the tail)
%         currentData_fid_mod_trunc = summedFIDs;
%         currentData_fid_mod_trunc(truncPoint:end) = 0;
% %         currentData_fid_mod_trunc_Lenkinski = complex(real(currentData_fid_mod_trunc),zeros(size(currentData_fid_mod_trunc)));
% %         currentData_fid_mod_Lenkinski = complex(real(currentData_fid_mod),zeros(size(currentData_fid_mod)));
% %         %add tail of FID to FID, then truncate
% %         tail = flipud(currentData_fid_mod_Lenkinski(1600:end,1));
% %         tail = [tail; zeros(4096-length(tail),1)];
% %         tail = complex(tail,zeros(size(currentData_fid_mod_Lenkinski)));
% %         summedFIDs = tail + currentData_fid_mod_Lenkinski;
% %         summedFIDsTrunc = summedFIDs;
% %         summedFIDsTrunc(truncPoint:end) = 0;
%         Data_Mod(:,1,1,1,1,1,1,1,1,1,1,idxAv) = currentData_fid_mod_trunc;
%     end
%     a.Data{1}(:,1,1,1,1,1,1,1,1,1,1,:) = Data_Mod;
%     
%     
%     %average data block-wise
%     if ~exist('nBlocks', 'var')
%         a.Parameter.AverageSettings.nBlocks = 1;
%     else
%         a.Parameter.AverageSettings.nBlocks = nBlocks;
%     end
%     
%     a = a.AverageData;
% end

%% setup the settings for the FrequencyAlignFreqDomain
% if exist('modulus','var') && (modulus == 0)
    a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
    a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;
    
    %indices of Interest: given in ppm
    % NAA peak
    a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = 2.008;
    
    %set zeroFillingParameter to get smooth approximations
    a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;
    
    %search area (+-) in ppm
    if isInVivo
        a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.1;
    else
        a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.2;
    end
    %spline filtering is not needed if the spectra are not too noisy.
    %flag to do spline filtering
    a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
    %spline filtering coefficient
    a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;
    % do the actual Frequency Alignment
    a = a.FrequencyAlignFreqDomain;
% end

%% water suppression
if isInVivo
    a.Parameter.HsvdSettings.bound = [-80 80];
else
    a.Parameter.HsvdSettings.bound = [-100 100];
end
a.Parameter.HsvdSettings.n = size(a.Data{1},1);
a.Parameter.HsvdSettings.p = 25;
[~, a] = a.Hsvd;
[~, a] = a.Hsvd;
[~, a] = a.Hsvd;
% 
if isMC
    %% Reverse MC
%     a = a.ReverseMC;
%     [~, a] = a.Hsvd;
end

%truncate data
% if exist('modulus','var') && (modulus == 0)
    if isInVivo
        trunc_ms = 150;
        %     trunc_ms = 100;     %MM
        truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
        a = a.Truncate(truncPoint);
    end
% end

%% fMRS processing

%set stimulus Paradigm, starting with off block, ending with on block
if nBlocks == 5
    paradigm = ones(1,2*nBlocks);
    for i = 1:4:2*nBlocks
        paradigm(i) = 0;
    end
elseif nBlocks == 10
    paradigm = ones(1,nBlocks);
    for i = 1:4:nBlocks
        paradigm(i) = 0;
    end
    for i = 2:2:nBlocks
        paradigm(i) = 2;
    end
else
    sprintf('nBlocks is not 5 or 10')
end
a.Parameter.fMRSAveSettings.paradigm = paradigm;

close all;
%BOLD Effect
SelProcValues = SelectedProcessingValues();
%Water
SelProcValues.PeakRange = [4 5];    %ppm
SelProcValues.NoiseRangeFD = [-5.3 -0.3];    %ppm
SelProcValues.NoiseTimeDomain = 0;     %SNR in Frequency Domain (=1 in Time Domain, but here: data are truncated)
currentValueH20 = SelProcValues.PeakRange;
currentValueH20 = convertPPMtoIndex(a, currentValueH20,1);
currentValueH20 =[find(currentValueH20==1,1,'first') find(currentValueH20==1,1,'last')];
SelProcValues.PeakRange = [currentValueH20(1) currentValueH20(2)];
currentValueNoise = SelProcValues.NoiseRangeFD;
currentValueNoise = convertPPMtoIndex(a, currentValueNoise,1);
currentValueNoise =[find(currentValueNoise==1,1,'first') find(currentValueNoise==1,1,'last')];
SelProcValues.NoiseRangeFD = [currentValueNoise(1) currentValueNoise(2)];
boldEffect( a, SelProcValues, 1, 2)

subplot(2,1,1)
boxoff = []; boxon = []; boxyoff = [];  boxyon = []; boxy2on = []; boxy2off = []; n = 1;
yLimits = get(gca,'YLim');
if nBlocks == 5
    for i = 1:nBlocks
        if mod(i,2)     %odd
            boxoff  = [boxoff (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
            boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else            %even
            boxon   = [boxon (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
            boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
    end
elseif nBlocks == 10
    for i = 1:2:nBlocks
        if mod(n,2)     %odd
            boxoff  = [boxoff (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
            boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else            %even
            boxon   = [boxon (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
            boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
        n = n+1;
    end
else
    sprintf('nBlocks is not 5 or 10')
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
subplot(2,1,2)
yLimits = get(gca,'YLim');
if nBlocks == 5
    for i = 1:nBlocks
        if mod(i,2)     %odd
            boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else
            boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
    end
elseif nBlocks == 10
    n = 1;
    for i = 1:2:nBlocks
        if mod(n,2)     %odd
            boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else
            boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
        n = n+1;
    end
else
    sprintf('nBlocks is not 5 or 10')
end

patch(boxoff,boxy2off,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxy2on,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);

if saveResults
    fig2save_BOLDH2O = fullfile(currentFolder, [num2str(visualType) '_BOLD_H2O' num2str(nAvg2save) '.fig']);
    savefig(fig2save_BOLDH2O)
    close all
end

%NAA
SelProcValues.PeakRange = [1.5 2.5];    %ppm
currentValueNAA = SelProcValues.PeakRange;
currentValueNAA = convertPPMtoIndex(a, currentValueNAA,1);
currentValueNAA =[find(currentValueNAA==1,1,'first') find(currentValueNAA==1,1,'last')];
SelProcValues.PeakRange = [currentValueNAA(1) currentValueNAA(2)];
boldEffect( a, SelProcValues, 1, 1)

subplot(2,1,1)
boxoff = []; boxon = []; boxyoff = [];  boxyon = []; boxy2on = []; boxy2off = []; n = 1;
yLimits = get(gca,'YLim');
if nBlocks == 5
    for i = 1:nBlocks
        if mod(i,2)     %odd
            boxoff  = [boxoff (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
            boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else            %even
            boxon   = [boxon (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
            boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
    end
elseif nBlocks == 10
    for i = 1:2:nBlocks
        if mod(n,2)     %odd
            boxoff  = [boxoff (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
            boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else            %even
            boxon   = [boxon (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
            boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
        n = n+1;
    end
else
    sprintf('nBlocks is not 5 or 10')
end

patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
subplot(2,1,2)
yLimits = get(gca,'YLim');
if nBlocks == 5
    for i = 1:nBlocks
        if mod(i,2)     %odd
            boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else
            boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
    end
elseif nBlocks == 10
    n = 1;
    for i = 1:2:nBlocks
        if mod(n,2)     %odd
            boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else
            boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
        n = n+1;
    end
else
    sprintf('nBlocks is not 5 or 10')
end
patch(boxoff,boxy2off,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxy2on,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);

if saveResults
    fig2save_BOLDNAA = fullfile(currentFolder, [num2str(visualType) '_BOLD_NAA' num2str(nAvg2save) '.fig']);
    savefig(fig2save_BOLDNAA)
    close all;
end

%Cr
SelProcValues.PeakRange = [2.9 3.2];    %ppm
currentValueCr = SelProcValues.PeakRange;
currentValueCr = convertPPMtoIndex(a, currentValueCr,1);
currentValueCr =[find(currentValueCr==1,1,'first') find(currentValueCr==1,1,'last')];
SelProcValues.PeakRange = [currentValueCr(1) currentValueCr(2)];
boldEffect( a, SelProcValues, 1, 1)

subplot(2,1,1)
boxoff = []; boxon = []; boxyoff = [];  boxyon = []; boxy2on = []; boxy2off = []; n = 1;
yLimits = get(gca,'YLim');
if nBlocks == 5
    for i = 1:nBlocks
        if mod(i,2)     %odd
            boxoff  = [boxoff (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
            boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else            %even
            boxon   = [boxon (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
            boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
    end
elseif nBlocks == 10
    for i = 1:2:nBlocks
        if mod(n,2)     %odd
            boxoff  = [boxoff (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
            boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else            %even
            boxon   = [boxon (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
            boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
        n = n+1;
    end
else
    sprintf('nBlocks is not 5 or 10')
end
patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);
subplot(2,1,2)
yLimits = get(gca,'YLim');
if nBlocks == 5
    for i = 1:nBlocks
        if mod(i,2)     %odd
            boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else
            boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
    end
elseif nBlocks == 10
    n = 1;
    for i = 1:2:nBlocks
        if mod(n,2)     %odd
            boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        else
            boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
        end
        n = n+1;
    end
else
    sprintf('nBlocks is not 5 or 10')
end
patch(boxoff,boxy2off,[0 0 1],'FaceAlpha',0.2)
patch(boxon,boxy2on,[1 0 0],'FaceAlpha',0.2)
ylim([yLimits(1) yLimits(2)]);

if saveResults
    fig2save_BOLDCr = fullfile(currentFolder, [num2str(visualType) '_BOLD_Cr' num2str(nAvg2save) '.fig']);
    savefig(fig2save_BOLDCr)
    close all;
end

%average on and off blocks
if nBlocks == 5         %size(a.Data{1},a.meas_dim)
    paradigm(1) = -1;
elseif nBlocks == 10
    if ~isempty(strfind(BOLDType,'64#1')) %take first 32 averages of each ON/OFF block
        paradigm = ones(1,2*nBlocks);
        for i = 1:2:2*nBlocks
            paradigm(i) = -1;
            if (i == 9) || (i == 17)
                paradigm(i) = 0;
            elseif (i == 5) || (i == 13)
                paradigm(i) = 1;
            end
        end
    else                                %take second 32 averages of each ON/OFF block
        paradigm = ones(1,2*nBlocks);
        for i = 1:2:2*nBlocks
            paradigm(i) = -1;
            if (i == 11) || (i == 19)
                paradigm(i) = 0;
            elseif (i == 7) || (i == 15)
                paradigm(i) = 1;
            end
        end
    end
end
a.Parameter.fMRSAveSettings.paradigm = paradigm;
a = a.fMRSAverage;      %now dim 1=ON, 2=OFF
figure; hold on;
data = squeeze(a.Data{1});
ppmVector = a.getPpmVector;
plot(ppmVector,real(fftshift(fft(data(:,1,1)))))
plot(ppmVector,real(fftshift(fft(data(:,1,2)))))
plot(ppmVector,real(fftshift(fft(data(:,1,1)-data(:,1,2)))))
if exist('modulus','var') && (modulus == 1)
    plot(ppmVector,real(fftshift(fft(real(data(:,1,1)-data(:,1,2)).*2))))
end
% plot(ppmVector,real(fftshift(fft(abs(data(:,1,1)-data(:,1,2)).*2))))
set(gca, 'XDir','reverse')
if exist('modulus','var') && (modulus == 1)
    xlim([0.5 4.5]); legend('On','Off','Residual')
else
    xlim([0.5 4.5]); legend('On','Off','Residual','Real Residual')
end

if saveResults
    if nBlocks == 5
        fig2save_ONOFF = fullfile(currentFolder, [num2str(visualType) '_ONOFF_128.fig']);
    elseif nBlocks == 10
        if ~isempty(strfind(BOLDType,'64#1'))
            fig2save_ONOFF = fullfile(currentFolder, [num2str(visualType) '_ONOFF_64#1.fig']);
        else
            fig2save_ONOFF = fullfile(currentFolder, [num2str(visualType) '_ONOFF_64.fig']);
        end
    end
    savefig(fig2save_ONOFF)
    close all;
end

%%
if saveResults
    data = (a.Data{1});
    %saving the file Stimulus ON
    aON = a;
    aON.Data{1} = data(:,:,:,:,:,:,:,:,:,:,:,1);
    filename = sprintf('%d',a.Parameter.Headers.PatientName);
    if nBlocks == 5
        save([filename '_' num2str(visualType) 'ON_woBOLDCorr128.mat'], 'aON')
    elseif nBlocks == 10
        if ~isempty(strfind(BOLDType,'64#1'))
            data2save_ONOFF = [filename '_' num2str(visualType) 'ON_woBOLDCorr64#1.mat'];
        else
            data2save_ONOFF = [filename '_' num2str(visualType) 'ON_woBOLDCorr64.mat'];
        end
        save(data2save_ONOFF, 'aON')
    end
    %export to LCModel Stimulus ON
    addSinglet0ppm = true;
    if exist('modulus','var') && (modulus == 1)
        aON = a;
        aON.Data{1} = complex(real(data(:,:,:,:,:,:,:,:,:,:,:,1).*2),zeros(size(data(:,:,:,:,:,:,:,:,:,:,:,1))));
    end
    if nBlocks == 5
        aON.ExportLcmRaw('',[filename '_' num2str(visualType) 'ON_woBOLDCorr128'], addSinglet0ppm);
    elseif nBlocks == 10
        if ~isempty(strfind(BOLDType,'64#1'))
            aON.ExportLcmRaw('',[filename '_' num2str(visualType) 'ON_woBOLDCorr64#1'], addSinglet0ppm);
        else
            aON.ExportLcmRaw('',[filename '_' num2str(visualType) 'ON_woBOLDCorr64'], addSinglet0ppm);
        end
    end
    
    %saving the file Stimulus OFF
    aOFF = a;
    aOFF.Data{1} = data(:,:,:,:,:,:,:,:,:,:,:,2);
    if nBlocks == 5
        save([filename '_' num2str(visualType) 'OFF_woBOLDCorr128.mat'], 'aOFF')
    elseif nBlocks == 10
        if ~isempty(strfind(BOLDType,'64#1'))
            data2save_ONOFF = [filename '_' num2str(visualType) 'OFF_woBOLDCorr64#1.mat'];
        else
            data2save_ONOFF = [filename '_' num2str(visualType) 'OFF_woBOLDCorr64.mat'];
        end
        save(data2save_ONOFF, 'aOFF')
    end
    
    %export to LCModel Stimulus OFF
    addSinglet0ppm = true;
    if exist('modulus','var') && (modulus == 1)
        aOFF = a;
        aOFF.Data{1} = complex(real(data(:,:,:,:,:,:,:,:,:,:,:,2).*2),zeros(size(data(:,:,:,:,:,:,:,:,:,:,:,2))));
    end
    if nBlocks == 5
        aOFF.ExportLcmRaw('',[filename '_' num2str(visualType) 'OFF_woBOLDCorr128'], addSinglet0ppm);
    elseif nBlocks == 10
        if ~isempty(strfind(BOLDType,'64#1'))
            aOFF.ExportLcmRaw('',[filename '_' num2str(visualType) 'OFF_woBOLDCorr64#1'], addSinglet0ppm);
        else
            aOFF.ExportLcmRaw('',[filename '_' num2str(visualType) 'OFF_woBOLDCorr64'], addSinglet0ppm);
        end
    end
end

%% Do BOLD lineshape correction
a.Parameter.fMRSDiffSettings.PhaseCorrection = 0;
a.Parameter.fMRSDiffSettings.frequencyShift = 0;
a.Parameter.fMRSDiffSettings.OffsetCorrection = 0;
a.Parameter.fMRSDiffSettings.Automatic = true;
a.Parameter.fMRSDiffSettings.indecesRegion = [1480 1510]; %NAA

a = a.fMRSDifference;

figure; hold on;
data = squeeze(a.Data{1});
ppmVector = a.getPpmVector;
plot(ppmVector,real(fftshift(fft(data(:,1,1)))))
plot(ppmVector,real(fftshift(fft(data(:,1,2)))))
plot(ppmVector,real(fftshift(fft(data(:,1,3)))))
% if exist('modulus','var') && (modulus == 1)
%     plot(ppmVector,real(fftshift(fft(real(data(:,1,3).*2)))))
% end
set(gca, 'XDir','reverse')
if exist('modulus','var') && (modulus == 1)
    xlim([0.5 4.5]); legend('On','Off','Residual')
else
    xlim([0.5 4.5]); legend('On','Off','Residual','Real Residual')
end

if saveResults
    if nBlocks == 5
        fig2save_ONOFF_BOLDCorr = fullfile(currentFolder, [num2str(visualType) '_ONOFF_BOLDCorr128.fig']);
    elseif nBlocks == 10
        if ~isempty(strfind(BOLDType,'64#1'))
            fig2save_ONOFF_BOLDCorr = fullfile(currentFolder, [num2str(visualType) '_ONOFF_BOLDCorr64#1.fig']);
        else
            fig2save_ONOFF_BOLDCorr = fullfile(currentFolder, [num2str(visualType) '_ONOFF_BOLDCorr64.fig']);
        end
    end
    savefig(fig2save_ONOFF_BOLDCorr)
    close all;
end

if saveResults
    data = (a.Data{1});
    %saving the file Stimulus ON
    aON = a;
    aON.Data{1} = data(:,:,:,:,:,:,:,:,:,:,:,1);
    filename = sprintf('%d',a.Parameter.Headers.PatientName);
    if nBlocks == 5
        save([filename '_' num2str(visualType) 'ON_withBOLDCorr128.mat'], 'aON')
    elseif nBlocks == 10
        if ~isempty(strfind(BOLDType,'64#1'))
            data2save_ONOFF = [filename '_' num2str(visualType) 'ON_withBOLDCorr64#1.mat'];
        else
            data2save_ONOFF = [filename '_' num2str(visualType) 'ON_withBOLDCorr64.mat'];
        end
        save(data2save_ONOFF, 'aON')
    end
    
    %export to LCModel Stimulus ON
    addSinglet0ppm = true;
%     if exist('modulus','var') && (modulus == 1)
%         aON = a;
%         aON.Data{1} = complex(real(data(:,:,:,:,:,:,:,:,:,:,:,1).*2),zeros(size(data(:,:,:,:,:,:,:,:,:,:,:,1))));
% %         aON.Data{1} = real(data(:,:,:,:,:,:,:,:,:,:,:,1).*2);
%     end
    if nBlocks == 5
        aON.ExportLcmRaw('',[filename '_' num2str(visualType) 'ON_withBOLDCorr128'], addSinglet0ppm);
    elseif nBlocks == 10
        if ~isempty(strfind(BOLDType,'64#1'))
            aON.ExportLcmRaw('',[filename '_' num2str(visualType) 'ON_withBOLDCorr64#1'], addSinglet0ppm);
        else
            aON.ExportLcmRaw('',[filename '_' num2str(visualType) 'ON_withBOLDCorr64'], addSinglet0ppm);
        end
    end
    
    %saving the file Stimulus OFF
    aOFF = a;
    aOFF.Data{1} = data(:,:,:,:,:,:,:,:,:,:,:,2);
    if nBlocks == 5
        save([filename '_' num2str(visualType) 'OFF_withBOLDCorr128.mat'], 'aOFF')
    elseif nBlocks == 10
        if ~isempty(strfind(BOLDType,'64#1'))
            data2save_ONOFF = [filename '_' num2str(visualType) 'OFF_withBOLDCorr64#1.mat'];
        else
            data2save_ONOFF = [filename '_' num2str(visualType) 'OFF_withBOLDCorr64.mat'];
        end
        save(data2save_ONOFF, 'aOFF')
    end    
    
    %export to LCModel Stimulus OFF
    addSinglet0ppm = true;
%     if exist('modulus','var') && (modulus == 1)
%         aOFF = a;
%         aOFF.Data{1} = complex(real(data(:,:,:,:,:,:,:,:,:,:,:,2).*2),zeros(size(data(:,:,:,:,:,:,:,:,:,:,:,2))));
% %         aOFF.Data{1} = real(data(:,:,:,:,:,:,:,:,:,:,:,1).*2);
%     end
    if nBlocks == 5
        aOFF.ExportLcmRaw('',[filename '_' num2str(visualType) 'OFF_withBOLDCorr128'], addSinglet0ppm);
    elseif nBlocks == 10
        if ~isempty(strfind(BOLDType,'64#1'))
            aOFF.ExportLcmRaw('',[filename '_' num2str(visualType) 'OFF_withBOLDCorr64#1'], addSinglet0ppm);
        else
            aOFF.ExportLcmRaw('',[filename '_' num2str(visualType) 'OFF_withBOLDCorr64'], addSinglet0ppm);
        end
    end
    
end

end