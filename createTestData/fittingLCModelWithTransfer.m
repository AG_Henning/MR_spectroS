function fittingLCModelWithTransfer(LCModelCallerInstance, localFilePath, filenameData, dataPath, ...
    filenameWater, waterPath, currentTEString, copyFiles, fitWater)

if ~exist('fitWater','var')
    fitWater = false;
end
    
extensionTable = '.table';
extensionCoord = '.coord';
defaultTE = 'TE24';
defaultLCModelUser = 'tborbath';

defaultSpectraName = 'XXXX';
defaultWaterRefName = 'YYYY';
controlFilesBase = 'fitsettings_';
if fitWater
    controlFilesBaseSuffix = '_MC_Water.control';
else %normal case
    controlFilesBaseSuffix = '_Metabolite.control';
end

%% file paths setup
controlFilesPathLocal =  [localFilePath, 'LCModelConfig/'];
dataFilesPathRemote = '/Desktop/TestDataFitting/Data/';
defaultControlFile = strcat(controlFilesBase, defaultSpectraName, controlFilesBaseSuffix);

%% basic configurations
LCModelTableFits = strcat(filenameData, extensionTable);
LCModelTableFitsCoord = strcat(filenameData, extensionCoord);
LCModelOutputFiles = filenameData;

%% create the control file series
% call the Configuration file creator
LCModelControlFile = createLCModelConfigFile(controlFilesPathLocal, defaultControlFile, defaultTE, currentTEString, ...
    defaultLCModelUser, LCModelCallerInstance.LCModelUser, defaultSpectraName, filenameData, ...
    defaultWaterRefName, filenameWater);

if copyFiles
    %% do the actual LCModel fitting
    filenameDataFull = [filenameData '.RAW'];
    % copy the .RAW LCModel file to the LCModel computer
    LCModelCallerInstance.CopyDataToRemote(dataPath, dataFilesPathRemote, filenameDataFull);
    if ~isempty(filenameWater)
        % copy the .RAW LCModel water reference file to the LCModel computer
        filenameWaterFull = [filenameWater '.RAW'];
        LCModelCallerInstance.CopyDataToRemote(waterPath, dataFilesPathRemote, filenameWaterFull);
    end
else
    % don't copy anything
    controlFilesPathLocal = [];
end

% do the fitting with the file transfers there and back from the remote computer
fittingLCModel(LCModelCallerInstance, {LCModelControlFile}, {LCModelOutputFiles}, ...
    LCModelCallerInstance.controlFilesPathRemote, controlFilesPathLocal, [], copyFiles);

if copyFiles
    % delete files from remote
    fileExtensions = {'.coord', '.csv', '.ps', '.table'};
    outputFilesToDelete = strcat(LCModelOutputFiles, fileExtensions);
    LCModelCallerInstance.DeleteFilesFromRemote(dataFilesPathRemote, {filenameDataFull});
    if ~isempty(filenameWater)
        LCModelCallerInstance.DeleteFilesFromRemote(dataFilesPathRemote, {filenameWaterFull});
    end
    LCModelCallerInstance.DeleteFilesFromRemote(LCModelCallerInstance.controlFilesPathRemote, {LCModelControlFile});
    LCModelCallerInstance.DeleteFilesFromRemote(LCModelCallerInstance.outputFilesPathRemote, outputFilesToDelete);
    
    delete([controlFilesPathLocal LCModelControlFile]);
else
    % don't delete anything
end

     
end