function doFitting_fMRS_MM()
clear; clc; close all;
pathName = 'fMRS data path';
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
% subjects = {'2823';'5166';};
% prefixes = {'2020-02-18_';'2020-02-21_';};
subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
    '4085';'3333';'4012';'4085';'6524'};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
    '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
    '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
suffixFolder = '_fMRS';

extensionTable = '.table';
extensionCoord = '.coord';
extensionRAW   = '.RAW';
extensionMat   = '.mat';
defaultSubject = 'XXXX';
defaultSubjectsPath = 'YYYY';
defaultLCModelUser = 'jdorst';

controlFilesBase = 'fitsettings_MMBaseline_XXXX';
controlFilesBaseSuffix = '.control';
outputFileNameBase = strcat(defaultSubject, '_MMBaseline');
outputFileNameBaseWithoutMetabolite = strcat(defaultSubject, '_MMBaseline_wo_Cr');
outputFileNameBaseWithoutMetaboliteFiltered = strcat(defaultSubject, '_MMBaseline_wo_Cr_Filtered');

%% file paths setup
controlFilesPathRemote = '/Desktop/1H_fMRS/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/1H_fMRS/Output/';
LCModelOutputPath = [localFilePathBase, 'Output/'];
LCModelMMBFilesPathRemote = '/Desktop/1H_fMRS/Basis_sets/MMBaseline/';
defaultControlFile = strcat(controlFilesBase, controlFilesBaseSuffix);
subjectsPath = strcat(prefixes, subjects, suffixFolder, '/');
subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath);
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath);
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath);
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath);

%% process MMBaseline data and save .RAW file
dataPath = strcat(localFilePathBase,subjectsPath);
for i = 1:length(dataPath)
    currentFolder = num2str(cell2mat(dataPath(i)));
    cd (currentFolder);
    subFolderName = dir(dataPath{i});
    dataFile = dir('*MMBaseline*.dat');
    reconstruct_1H_MMBaseline(dataFile.name,1,1);
    close(1); close(2); close(3); cd ..;
end

%% basic configurations
LCModelTableFits = strcat(outputFileNameBase, extensionTable);
LCModelTableFitsCoord = strcat(outputFileNameBase, extensionCoord);
LCModelTableFitsMat   = strcat(outputFileNameBase, extensionMat);
LCModelOutputFiles = strcat(outputFileNameBase);
LCModelControlFiles = strcat(controlFilesBase, controlFilesBaseSuffix);

numberOfSubjects = length(subjects);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

%% do the actual LCModel fitting
for indexCurrentSubject = 1:numberOfSubjects
    %% do the LCModel fitting
    %     LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
    
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
    currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
    %% create the control file series
    currentControlFiles = cellstr(currentControlFiles);
    currentOutputFiles  = cellstr(currentOutputFiles);
    LCModelOutputFiles  = cellstr(LCModelOutputFiles);
    for indexFMRS = 1:length(currentControlFiles)
        currentOutputFile = currentOutputFiles{indexFMRS};
        currentLCModelOutputFile = LCModelOutputFiles{indexFMRS};
        createLCModelConfigMMBaseline(controlFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, defaultControlFile, ...
            defaultLCModelUser, LCModelCallerInstance.LCModelUser, subjects{indexCurrentSubject}, defaultSubject,...
            subjectsPath{indexCurrentSubject}, defaultSubjectsPath);
    end
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
    
    % subtract Cr from MMBaseline
    currentLCModelMatFit = strrep(LCModelTableFitsMat,defaultSubject, subjects{indexCurrentSubject});
    currentLCModelCoordFit = strrep(LCModelTableFitsCoord,defaultSubject, subjects{indexCurrentSubject});
    currentLCModelFitWithoutMetabolite = strrep(outputFileNameBaseWithoutMetabolite,defaultSubject,subjects{indexCurrentSubject});
    currentDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject});
    subtractMetabolitesFromMMB_fMRS(currentDataPath,subjectsLCModelOutputPath{indexCurrentSubject},currentLCModelMatFit,currentLCModelCoordFit,currentLCModelFitWithoutMetabolite);
    
    
    % save MMBaseline w/o Cr in LCModel Basis_sets Folder
    fileName = strrep(strcat(outputFileNameBaseWithoutMetabolite,extensionRAW),defaultSubject,subjects{indexCurrentSubject});
    remoteDirectory = strcat(LCModelMMBFilesPathRemote,subjectsPath{indexCurrentSubject});
    LCModelCallerInstance.CopyDataToRemote(currentDataPath, remoteDirectory, fileName)
    
    % filter MMBaseline w/o Cr and save it to local path and LC Model
    data_nonFiltered = load(strrep(strcat(currentDataPath,outputFileNameBaseWithoutMetabolite,extensionMat),defaultSubject,subjects{indexCurrentSubject}));
    dwellTime = data_nonFiltered.MMBaseline_wo_Cr.Parameter.Headers.DwellTimeSig_ns*1E-9;
    t = (0:(size(data_nonFiltered.MMBaseline_wo_Cr.Data{1}, data_nonFiltered.MMBaseline_wo_Cr.kx_dim))-1)*dwellTime;
    data_nonFiltered = data_nonFiltered.MMBaseline_wo_Cr.Data{1};
    Fgau = 5;   %parameter that sets the decay rate in Hz
    L = -1;     %linewidth in Hz, L positive: line broadening, L negative: resolution enhancement
    W = exp(-t'*pi*L).*exp(- t'.^2*(pi*Fgau)^2);
    data_Filtered = data_nonFiltered.*W;
    currentLCModelFitWithoutMetaboliteFiltered = strrep(outputFileNameBaseWithoutMetaboliteFiltered,defaultSubject,subjects{indexCurrentSubject});
    reconstructedSpectraWithoutMetaboliteFiltered = strcat(currentDataPath, currentLCModelFitWithoutMetaboliteFiltered);
    load(strrep(strcat(currentDataPath,currentLCModelFitWithoutMetabolite),defaultSubject,subjects{indexCurrentSubject}));
    MMBaseline_wo_Cr_Filtered = MMBaseline_wo_Cr;
    MMBaseline_wo_Cr_Filtered.Data{1} = data_Filtered;
    save(reconstructedSpectraWithoutMetaboliteFiltered,'MMBaseline_wo_Cr_Filtered');
    MMBaseline_wo_Cr_Filtered.ExportLcmRaw(currentDataPath, currentLCModelFitWithoutMetaboliteFiltered, 'Yes', 'Yes');
    fileNameFiltered = strrep(strcat(outputFileNameBaseWithoutMetaboliteFiltered,extensionRAW),defaultSubject,subjects{indexCurrentSubject});
    LCModelCallerInstance.CopyDataToRemote(currentDataPath, remoteDirectory, fileNameFiltered)
    
end

%% sum MMBaselines, weighted with water fit (need to run doFitting_fMRS_Water.m first)
WaterReferenceTable = (readtable(strcat(LCModelOutputPath,'WaterReference_Fit.xlsx')));
WaterReferenceID = table2cell(WaterReferenceTable(2:end,1));
WaterReference = str2double(table2array(WaterReferenceTable(2:end,2:end)));
MMBScaled = zeros(8192,numberOfSubjects);
for indexCurrentSubject = 1:numberOfSubjects
    idx = find(ismember(WaterReferenceID, strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject})));
    currentScalingFactor = WaterReference(idx,1);
    currentMMBPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject});
    load(strrep(strcat(currentMMBPath,outputFileNameBaseWithoutMetabolite,extensionMat),defaultSubject,subjects{indexCurrentSubject}));
    currentMMB = squeeze(MMBaseline_wo_Cr.Data{1});
    currentMMBScaled = currentMMB./currentScalingFactor;
    MMBScaled(:,indexCurrentSubject) = currentMMBScaled;
end
MMBaseline_Summed = sum(MMBScaled,2);
%save to local Path and LCModel
MMBaseline_Summed_Path = strcat(localFilePathBase,'Basis_sets\');
save(strcat(MMBaseline_Summed_Path,'MMBaseline_Summed'),'MMBaseline_Summed');
    load(strrep(strcat(currentMMBPath,outputFileNameBaseWithoutMetabolite,extensionMat),defaultSubject,subjects{indexCurrentSubject}));
    MMBaseline_Summed_MR_spectroS = MMBaseline_wo_Cr;
    MMBaseline_Summed_MR_spectroS.Data{1} = MMBaseline_Summed;
    MMBaseline_Summed_MR_spectroS.ExportLcmRaw(MMBaseline_Summed_Path, 'MMBaseline_Summed', 'Yes', 'Yes');
    LCModelCallerInstance.CopyDataToRemote(MMBaseline_Summed_Path, LCModelMMBFilesPathRemote, strcat('MMBaseline_Summed',extensionRAW))

end
