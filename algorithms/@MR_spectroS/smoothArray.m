
function smoothedArray = smoothArray(data, smoothingFactor, doPlotting)
x = [1:length(data)]'  ;
y = fftshift(fft(data));

%Real part
[xDataRe, yDataRe] = prepareCurveData( x, real(y) );

%Imaginary part
[xDataIm, yDataIm] = prepareCurveData( x, imag(y) );

ft = fittype( 'smoothingspline' );
opts = fitoptions( ft );
opts.SmoothingParam = smoothingFactor;


% Fit model to data.

 %Real part
[fitresultRe, gof] = fit( xDataRe, yDataRe, ft, opts );

%Imaginary part
[fitresultIm, gof] = fit( xDataIm, yDataIm, ft, opts );

yReal = fitresultRe(xDataRe);
yImag = fitresultIm(xDataIm);


% Convert it to time domain and fft again!
smoothedArray = ifft(ifftshift(yReal + 1j* yImag));

if doPlotting
    yReal  = real(fftshift(fft(smoothedArray)));
    yImag  = imag(fftshift(fft(smoothedArray)));
    figure 
    
    subplot( 2, 2, 1 );
    
    h = plot(x, yReal, x, real(y));
    legend( h, 'Real vs. x', 'splines', 'Location', 'NorthEast' );
    % Label axes
    xlabel( 'x' );
    ylabel( 'yDataR' );
    grid on
    
    subplot( 2, 2, 3 );
    
    h = plot( x,real(y)- yReal);
    legend( h, 'splines - residuals', 'Location', 'NorthEast' );
    % Label axes
    xlabel( 'x' );
    ylabel( 'yDataR' );
    grid on
    
    subplot( 2, 2, 2 );
    
    h = plot(x, yImag, x, imag(y));
    legend( h, 'Imaginary vs. x', 'splines', 'Location', 'NorthEast' );
    % Label axes
    xlabel( 'x' );
    ylabel( 'yDataIm' );
    grid on
    
    subplot( 2, 2, 4 );
    
    h = plot( x,imag(y)- yImag);
    legend( h, 'splines - residuals', 'Location', 'NorthEast' );
    % Label axes
    xlabel( 'x' );
    ylabel( 'yDataIm' );
    grid on
end
        
end