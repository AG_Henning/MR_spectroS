function this = FrequencyAlign(this)

if ~this.Parameter.ReconFlags.isSorted
    error('The data has to be sorted first')
end

if this.Parameter.ReconFlags.isFrequencyAligned
    warning('The data are already frequency aligned')
end

oldData                = this.Data{1};
nCoils                 = size(this.Data{1}, this.coil_dim);
nAverages              = size(this.Data{1}, this. meas_dim);
nMix                   = size(this.Data{1}, this.mix_dim);

domain                 = this.Parameter.FreqAlignSettings.domain;
indecesOfInterest      = this.Parameter.FreqAlignSettings.indecesOfInterest;
displayPlots           = this.Parameter.FreqAlignSettings.displayPlots;


if (isempty(indecesOfInterest) && strcmp(domain,'frequency') )
   error('First, you should specify the indeces of interest in which the optimization will be performed!')
end

   
fprintf('Frequency alignment starts... Please wait\n');


newData                            = zeros(size(oldData));
dwellTime                          = this.Parameter.Headers.DwellTimeSig_ns*1e-9;
xFreAlgnMatrix                     = [];


if nMix  == 2 
   selectedMix                     =  2;
else
   selectedMix                    =  1;  
end

tmpData                            =  squeeze(oldData(:,1,1,:,1,1,1,1,selectedMix,1,1,:));
tmpData                            =  permute(tmpData,[1 3 2]); %permure data in order to order them average-wise
tmpData                            =  reshape(tmpData,size(tmpData,1),nAverages*nCoils); %reshape and find the max
[~, maxPosition]                   =  max(max(tmpData(1:30,:,:)));



referenceFID                       =  tmpData(:,maxPosition);
selectedCoil                       =  floor(maxPosition/nAverages)+1;
selectedAveraged                   =  mod(maxPosition,nAverages);
if selectedAveraged == 0
    selectedAveraged = 1;
end


clc
fprintf('Selected #Average: %d and #Coil: %d\n',selectedAveraged, selectedCoil)

%//////////////////////////////////////////////////////////////////////////
   
    xFreAlgnMatrix                     =  [];
    tmpData                            =  squeeze(oldData(:,1,1,selectedCoil,1,1,1,1,selectedMix,1,1,:));
    
    
    
         %f0         = fminbnd(@(f)    sumOfSquaresTD1( f, referenceFID, tmpData(:,selectedAveraged),dwellTime ),-30,30)
         f          = fminsearch(@(f) sumOfSquaresTD2( f, referenceFID, tmpData(:,selectedAveraged),dwellTime),[0 0])
  
    for indexOfAverage=1:nAverages
        
        if strcmp(domain,'frequency')
       
           %function d = sumOfSquaresFD1( f, refSignal, signal, dwellTime, indeces )
           f0      = fminbnd(@(f) sumOfSquaresFD1( f, referenceFID, tmpData(:,indexOfAverage), dwellTime, indecesOfInterest ),-30,30);
           
           %function d = sumOfSquaresFD2( f, refSignal1, signal,dwellTime, indeces )
           f        = fminsearch(@(f) sumOfSquaresFD2( f, referenceFID, tmpData(:,indexOfAverage), dwellTime, indecesOfInterest),[0 f0]);
           
          xFreAlgnMatrix{indexOfAverage}= f;

        else %time domain   
        %function [d, signalAligned]  = sumOfSquaresTD1( f, refSignal, signal, dwellTime ) 
        %f0          = fminbnd(@(f)    sumOfSquaresTD1( f, referenceFID, tmpData(:,indexOfAverage),dwellTime ),-30,30);
       
        %function [d, signalAligned] = sumOfSquaresTD2( f, refSignal, signal, dwellTime )
        f          = fminsearch(@(f) sumOfSquaresTD2( f, referenceFID, tmpData(:,indexOfAverage), dwellTime),[0 0]);
          xFreAlgnMatrix{indexOfAverage}= f;
        end 
    
    end
    
 
  
 %/////////////////////////////////////////////////////////////////////////


 
%//////////////////////////////////////////////////////////////////////////
%                   Reconstruction of the data
%//////////////////////////////////////////////////////////////////////////
clc
 fprintf('Reconstrustion of the data. Please wait...')
for indexOfAverage=1:nAverages
  for indexOfChannel=1:nCoils
      for selectedMix = 1:nMix
      selectedData         = oldData(:, 1, 1, indexOfChannel, 1, 1, 1, 1, selectedMix, 1, 1, indexOfAverage);
      [~, tmpFID]          = sumOfSquaresTD2(xFreAlgnMatrix{indexOfAverage}, referenceFID, selectedData,dwellTime);
       %1:NCol, 1, 1, 1:NCha, 1, 1, 1, 1, 1, 1:NRep, 1, 1:NSet
      newData(:, 1, 1, indexOfChannel, 1, 1, 1, 1, selectedMix, 1, 1, indexOfAverage)...
                           =tmpFID;
      end
  end
end
 
  
%//////////////////////////////////////////////////////////////////////////
%                             Display results
%//////////////////////////////////////////////////////////////////////////
if displayPlots
    
    figure %fig 1
    f  = [xFreAlgnMatrix{:}];
    f0 = f(1:2:end);
    f1 = f(2:2:end);
    subplot(2,1,1)
    %plot(1:nAverages,f0, 1:nAverages,smooth(f0,25));
    plot(1:nAverages,f0);
    title('Phase0')
    xlabel('#Average')
    ylabel('rads')
    subplot(2,1,2)
    %plot(1:nAverages,f1, 1:nAverages,smooth(f1,25));
    plot(1:nAverages,f1);
    title('Frequency Shift (Hz)')
    xlabel('#Average')
    ylabel('Hz')
    
    
    
    figure %fig 2 
    
    displayMix = selectedMix;
    preData = squeeze(oldData(:,1,1,selectedCoil,1,1,1,1,displayMix,1,1,:));
    preFFTS = fftshift(fft(preData));
    preFFTS = preFFTS(length(preFFTS)/2-100:length(preFFTS)/2+100,:);
    
    postData = squeeze(newData(:,1,1,selectedCoil,1,1,1,1,displayMix,1,1,:));
    postFFTS = fftshift(fft(postData));
    postFFTS = postFFTS(length(postFFTS)/2-100:length(postFFTS)/2+100,:);
    
    
    subplot(2,2,1)
    plot(real(preData))
    title('Before, TD')
    
    subplot(2,2,2)
    plot(real(preFFTS));
    title('Before, FD')
    
    subplot(2,2,3)
    plot(real(postData))
    title('After, TD')
    
    subplot(2,2,4)
    plot(real(postFFTS));
    title('After, FD')
    
end

%//////////////////////////////////////////////////////////////////////////
%                            Replace new data
%//////////////////////////////////////////////////////////////////////////

this.Data{1}                                    = newData;
this.Parameter.ReconFlags.isFrequencyAligned    = true;

clc
fprintf('Frequency alignment...FINISHED')     



%//////////////////////////////////////////////////////////////////////////
%                            COST FUNCTIONS
%//////////////////////////////////////////////////////////////////////////

    function [d, signalAligned]  = sumOfSquaresTD1( f, refSignal, signal, dwellTime )


    timeVector    = (0:length(signal)-1)*dwellTime;
    signalAligned       = signal.* exp(1i*2*pi*f*timeVector)';


    d            = refSignal -signalAligned ;
    d            = sum(abs(d).^2);


    end


    function [d, signalAligned] = sumOfSquaresTD2(f, refSignal, signal, dwellTime )


    timeVector    = (0:length(signal)-1)*dwellTime;
    signalAligned       = signal.* exp(1i*(f(1)+ 2*pi*f(2)*timeVector ))';


    d            = refSignal - signalAligned;
    d            = sum(abs(d).^2)*1000;


    end

    function [d,signalAligned]= sumOfSquaresFD1( f, refSignal, signal, dwellTime, indeces )


    timeVector           = (0:length(refSignal)-1)*dwellTime;
    signalAligned        = signal.* exp(1i*2*pi*f*timeVector)';


    ffts1        = fftshift(fft(refSignal));
    ffts2        = fftshift(fft(signalAligned));

    d            = ffts1(indeces)-ffts2(indeces);
    d            = sum(abs(d).^2);

    end


    function [d, signalAligned] = sumOfSquaresFD2( f, refSignal1, signal,dwellTime, indeces )

    timeVector     = (0:length(signal)-1)*dwellTime;
    signalAligned  = signal.* exp(1i*(f(1) + 2*pi*f(2)*timeVector))';


    ffts1        = fftshift(fft(refSignal1));
    ffts2        = fftshift(fft(signalAligned));

    d            = ffts1(indeces)-ffts2(indeces);
    d            = sum(abs(d).^2);


    end







end

