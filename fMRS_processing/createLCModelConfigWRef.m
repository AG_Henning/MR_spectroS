function outputFile = createLCModelConfigWRef(controlFilesPath, outputControlFilesPath, defaultControlFile, ...
    defaultLCModelUser, currentLCModelUser, spectrumFileName, defaultSpectrum,...
    subjectsPath, defaultSubjectsPath)
% Creates the control files, given a default control file.
% the newly generated files will be placed in the same path as indicated by
% the controlFilesPath.
% A sample input:
% controlFilesPath = 'Fitsettings/MMocc_noExtraMM_06knots/';
% defaultControlFile = 'fitsettingsDefault.control'

controlFileSuffix = '.control';

fitSettingsDefaultFileID = fopen([controlFilesPath defaultControlFile], 'r');

mkdir(outputControlFilesPath);
outputFile = strcat(outputControlFilesPath, 'fitsettings_WRef_', spectrumFileName, controlFileSuffix);

fitSettingsFileID = fopen(outputFile, 'w');
display(outputFile);

while(~feof(fitSettingsDefaultFileID))
    s = fgetl(fitSettingsDefaultFileID);
    s = strrep(s, defaultLCModelUser, currentLCModelUser);
%     s = strrep(s, defaultfMRS, currentfMRS);
    s = strrep(s, defaultSubjectsPath, subjectsPath);
    s = strrep(s, defaultSpectrum, spectrumFileName);
%     s = strrep(s, defaultWaterRef, waterRefFileName);

    fprintf(fitSettingsFileID,'%s\n', s);
end
fclose(fitSettingsFileID);
fclose(fitSettingsDefaultFileID);
end