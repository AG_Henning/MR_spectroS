function makeBasis_fMRS
clear; clc; close all;
pathName = 'fMRS data path';
sampleCriteria = {'Basis_sets'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
% subjects = {'3333';};
subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
    '4085';'3333';'4012';'4085';'6524'};
% prefixes = {'2020-03-10_';};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
    '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
    '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
suffixFolder = '_fMRS';

extensionIn = '.in';
defaultSubject = 'XXXX';
defaultSubjectsPath = 'YYYY';
defaultLCModelUser = 'jdorst';

%% file paths setup

makeBasisFilesPathRemote = '/Desktop/1H_fMRS/Basis_sets/';
makeBasisFilesPathLocal =  [localFilePathBase, 'Basis_sets/'];
LCModelBasisOutputFilesPathRemote = '/Desktop/1H_fMRS/Basis_sets/';
LCModelBasisOutputPath = [localFilePathBase, 'Basis_sets/'];
% makeBasisFilesBase = 'makeBasis_MMBIndFiltered_XXXX';
% makeBasisFilesBase = 'makeBasis_MMBInd_XXXX';
% makeBasisFilesBase = 'makeBasis_MMBSummed_XXXX';
makeBasisFilesBase = 'makeBasis_MMBSummed_v2_XXXX';
% makeBasisFilesBase = 'makeBasis_MMBSimulated_XXXX';
subjectsPath = strcat(prefixes, subjects, suffixFolder, '/');
subjectsControlFilesPathRemote = strcat(makeBasisFilesPathRemote, subjectsPath);
subjectsControlFilesPathLocal = strcat(makeBasisFilesPathLocal, subjectsPath);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath);
subjectsLCModelOutputFilesRemote = strcat(LCModelBasisOutputFilesPathRemote, subjectsPath);
subjectsLCModelOutputPath = strcat(LCModelBasisOutputPath, subjectsPath);

numberOfSubjects = length(subjects);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(makeBasisFilesPathRemote, LCModelBasisOutputFilesPathRemote, LCModelBasisOutputPath);

%% create the make basis file series
for indexCurrentSubject = 1:numberOfSubjects
    %give subject specific paths to LCModelCallerInstance
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    
    %create makeBasisFilesBase.in and save them locally
    createLCModelMakeBasisfMRS(makeBasisFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, ...
        strcat(makeBasisFilesBase,extensionIn), defaultLCModelUser, LCModelCallerInstance.LCModelUser, subjects{indexCurrentSubject}, defaultSubject,...
        subjectsPath{indexCurrentSubject}, defaultSubjectsPath);
    
    % save makeBasis in LCModel Basis_sets Folder, create Basis sets and
    % save created .basis, .ps files locally
    copyFileLocally = true;
    currentMakeBasisFile = strrep(strcat(makeBasisFilesBase,extensionIn),defaultSubject,subjects{indexCurrentSubject});
    currentBasisFileName = strrep(makeBasisFilesBase,defaultSubject,subjects{indexCurrentSubject});
    LCModelCallerInstance.CreateBasisSet(subjectsControlFilesPathLocal{indexCurrentSubject}, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        currentMakeBasisFile, copyFileLocally, currentBasisFileName);
    
end

fclose all;
end