function [this] = FrequencyAlignSPECIAL(this,freqShiftsSPECIALBlock,freqShiftsSPECIALSteps)
nAverages     = size(this.Data{1},this.meas_dim);   

    nSPECIAL  = 2;
    nSpec  = nAverages/nSPECIAL;

%check if nSpec == length(freqShiftsSPECIALBlock)
if nSpec ~= length(freqShiftsSPECIALBlock)
    error('nSpec ~= length(freqShiftsSPECIALBlock)');
end
%check if nSPECIAL == length(freqShiftsSPECIALSteps)
if nSPECIAL ~= length(freqShiftsSPECIALSteps)
    error('nSPECIAL ~= length(freqShiftsSPECIALSteps)');
end


freqShiftsISISBlockExpanded = zeros(1,nAverages);
for idxOfSpec = 1:nSpec
    offSet = (idxOfSpec-1) * nSPECIAL + 1;
    freqShiftsISISBlockExpanded(offSet:offSet+nSPECIAL-1) = freqShiftsSPECIALBlock(idxOfSpec);
end

freqShiftsISISStepsExpanded = zeros(1,nAverages);
for idxOfSpec = 1:nSPECIAL
    freqShiftsISISStepsExpanded(idxOfSpec:nSPECIAL:nAverages) = freqShiftsSPECIALSteps(idxOfSpec);
end

this = this.ApplyFrequencyShift(freqShiftsISISBlockExpanded);
this = this.ApplyFrequencyShift(freqShiftsISISStepsExpanded);

end
