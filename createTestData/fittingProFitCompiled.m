function fittingProFitCompiled(filenameData, varargin)

defaultPath = './';
defaultFilenameWater = [];
defaultWaterPath = [];
defaultTE = 24;%ms
defaultTR = 6000;%ms

p = inputParser;
validScalarPosNum = @(x) isnumeric(x) && isscalar(x) && (x > 0);
addRequired(p,'filenameData',@ischar);
addParameter(p,'dataPath',defaultPath,@ischar);
addParameter(p,'filenameWater',defaultFilenameWater,@ischar);
addParameter(p,'waterPath',defaultWaterPath, @ischar);
addParameter(p,'TE',defaultTE, validScalarPosNum);
addParameter(p,'TR',defaultTR, validScalarPosNum);
parse(p,filenameData,varargin{:});

filenameData = p.Results.filenameData;
dataPath = p.Results.dataPath;
filenameWater = p.Results.filenameWater;
waterPath = p.Results.waterPath;
TE = p.Results.TE;
TR = p.Results.TR;

fittingProFit(filenameData, dataPath, filenameWater, waterPath, TE, TR, dataPath);

[concentrations, activeMetabolites] = plot_ProFit_Metabolites_Fit([filenameData, '_profit.mat'],dataPath, 'Met', true);

save([dataPath, 'concentrations', '_profit.mat'], 'concentrations', 'activeMetabolites');
