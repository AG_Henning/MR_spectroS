function doFitting_fMRS_Met_Summed()

%check StimType, BOLDType, MMBType

clear; clc; close all;
% pathName = 'fMRS data path';    %first version, no modulus correction for gradient sideband removal
pathName = 'fMRS data path modulus';    %second version, with modulus correction for gradient sideband, only UF can be processed
sampleCriteria = {'Summed_fMRSData'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
StimType = {'StimON', 'StimOFF'};   %'RestON', 'RestOFF', 'StimON', 'StimOFF'
if ~isempty(strfind(StimType{1},'Stim'))
    visualType = 'Stim';
elseif ~isempty(strfind(StimType{1},'Rest'))
    visualType = 'Rest';
else
    error('StimType needs to be Stim or Rest');
end
BOLDType = {'_woBOLDCorr128'};  %_woBOLDCorr128, _withBOLDCorr128, _woBOLDCorr64, _withBOLDCorr64, _woBOLDCorr64#1
orderFMRS = strcat(StimType,BOLDType);
MMBType = '_MMBSummed_v2';     %_MMBSummed_v2, only useful with summed MMB
subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
    '4085';'3333';'4012';'4085';'6524'};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
    '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
    '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
excludeSubjects = {'5166';'9810';'5269'};
suffixFolder = '_fMRS';


if exist('excludeSubjects')
    n = length(subjects);
    m = length(excludeSubjects);
    nstep = 1;
    for j = 1:m
        for i = 1:n
            xx = find(~cellfun(@isempty,strfind(subjects(i,1),excludeSubjects{j})));
            if ~isempty(xx)
                excludeIdx(nstep) = i;
                nstep = nstep + 1;
            end
        end
    end
    subjects([excludeIdx],:) = [];
    prefixes([excludeIdx],:) = [];
end



extensionTable = '.table';
extensionCoord = '.coord';
extensionRAW   = '.RAW';
extensionMat   = '.mat';
defaultfMRS = 'StimOFF';
defaultSubject = 'XXXX';
defaultSubjectsPath = 'YYYY';
if ~isempty(strfind(MMBType,'_v2'))
    defaultMMB = '_MMBSummed_v2';
else
    defaultMMB = '_MMBSummed';
end
defaultLCModelUser = 'jdorst';
outputFileNameBase_OFF = strcat(defaultSubject, '_', num2str(visualType), 'OFF', BOLDType{1});
outputFileNameBase_ON  = strcat(defaultSubject, '_', num2str(visualType), 'ON', BOLDType{1});
outputFileNameBase_WRef = strcat(defaultSubject, '_WRef');

if ~isempty(strfind(MMBType,'_v2'))
    controlFilesBase = 'fitsettings_fMRS_v2_XXXX_';
else
    controlFilesBase = 'fitsettings_fMRS_XXXX_';
end
% controlFilesBaseDiff = 'fitsettings_fMRS_Difference_XXXX_';
% if visualType == 'Rest'
%     controlFilesBaseDiff = 'fitsettings+_fMRS_Difference_XXXX_';
% elseif visualTupe == 'Stim'
    controlFilesBaseDiff = 'fitsettings+shif_fMRS_Difference_XXXX_';
% end
controlFilesBaseSuffix = '.control';
outputFileNameBase = strcat(defaultSubject, '_');

LCModelOutputFiles = strcat(outputFileNameBase, orderFMRS, MMBType);
LCModelControlFiles = strcat(controlFilesBase, orderFMRS, MMBType, controlFilesBaseSuffix);
defaultWaterRef = strcat(outputFileNameBase, 'WRef');

%
numberOfFMRS = length(orderFMRS);
numberOfSubjects = length(subjects);

%% file paths setup
controlFilesPathRemote = '/Desktop/1H_fMRS/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/1H_fMRS/Output/';
if ~isempty(strfind(MMBType,'_v2'))
    if ~isempty(strfind(localFilePathBase,'Modulus'))
        LCModelOutputPath = [localFilePathBase, 'Output_v2_MMBSummed_Modulus/'];
    else
        LCModelOutputPath = [localFilePathBase, 'Output_v2_MMBSummed/'];
    end
else
    LCModelOutputPath = [localFilePathBase, 'Output/'];
end
defaultControlFile = strcat(controlFilesBase, defaultfMRS, controlFilesBaseSuffix);
defaultControlFileDiff = strcat(controlFilesBaseDiff, defaultfMRS, controlFilesBaseSuffix);

subjectsPath = strcat(prefixes, subjects, suffixFolder);
summedPath = 'Summed_fMRSData';

summedControlFilesPathRemote = strcat(controlFilesPathRemote, summedPath, '/');
summedControlFilesPathLocal = strcat(controlFilesPathLocal, summedPath, '/');
preprocessedFilesPathLocal = strcat(localFilePathBase, summedPath, '/');
summedLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, summedPath, '/');
summedLCModelOutputPath = strcat(LCModelOutputPath, summedPath, '/');

%% do the LCModel fitting
% LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);


%% sum fMRS spectra of all volunteers, weighted with water fit (need to run doFitting_fMRS_Water.m first)
WaterReferenceTable = (readtable(strcat(LCModelOutputPath,'WaterReference_Fit.xlsx')));
WaterReferenceID = table2cell(WaterReferenceTable(2:end,1));
WaterReference = str2double(table2array(WaterReferenceTable(2:end,2:end)));
fMRSDataScaled_OFF = zeros(4096,numberOfSubjects,2);    %samples, subjects, data/water
fMRSDataScaled_ON = zeros(4096,numberOfSubjects,2);
for indexCurrentSubject = 1:numberOfSubjects
    idx = find(ismember(WaterReferenceID, strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject})));
    currentScalingFactor = WaterReference(idx,1);
    currentfMRSDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject});
    load(strrep(strcat(currentfMRSDataPath, '\',outputFileNameBase_OFF,extensionMat),defaultSubject,subjects{indexCurrentSubject}));
    currentfMRSData_OFF = squeeze(aOFF.Data{1});
    currentfMRSDataScaled_OFF = currentfMRSData_OFF./currentScalingFactor;
    fMRSDataScaled_OFF(:,indexCurrentSubject,:) = currentfMRSDataScaled_OFF;
    load(strrep(strcat(currentfMRSDataPath, '\',outputFileNameBase_ON,extensionMat),defaultSubject,subjects{indexCurrentSubject}));
    currentfMRSData_ON = squeeze(aON.Data{1});
    currentfMRSDataScaled_ON = currentfMRSData_ON./currentScalingFactor;
    fMRSDataScaled_ON(:,indexCurrentSubject,:) = currentfMRSDataScaled_ON;
end
fMRSData_Summed_ON = squeeze(sum(fMRSDataScaled_ON,2));
fMRSData_Summed_ON = permute(fMRSData_Summed_ON,[1 3 4 5 6 7 8 9 2]);
if exist('excludeSubjects')
    fMRSData_Summed_ON_BOLD = strcat('Summed_', num2str(visualType), 'ON',strrep(BOLDType{1},'#','_'),MMBType,'_Excluded');
else
    fMRSData_Summed_ON_BOLD = strcat('Summed_', num2str(visualType), 'ON',strrep(BOLDType{1},'#','_'),MMBType);
end
eval([fMRSData_Summed_ON_BOLD '=fMRSData_Summed_ON']);
fMRSData_Summed_OFF = squeeze(sum(fMRSDataScaled_OFF,2));
fMRSData_Summed_OFF = permute(fMRSData_Summed_OFF,[1 3 4 5 6 7 8 9 2]);
if exist('excludeSubjects')
    fMRSData_Summed_OFF_BOLD = strcat('Summed_', num2str(visualType), 'OFF',strrep(BOLDType{1},'#','_'),MMBType,'_Excluded');
else
    fMRSData_Summed_OFF_BOLD = strcat('Summed_', num2str(visualType), 'OFF',strrep(BOLDType{1},'#','_'),MMBType);
end
eval([fMRSData_Summed_OFF_BOLD '=fMRSData_Summed_OFF']);

% save to local Path and LCModel
LCModelfMRSDataPathRemote = '/Desktop/1H_fMRS/Summed_fMRSData/';
fMRSData_Summed_Path = strcat(localFilePathBase,summedPath,'\');
exportFileMatON = [fMRSData_Summed_Path, strcat(fMRSData_Summed_ON_BOLD,'.mat')];
% % % % save(exportFileMatON,fMRSData_Summed_ON_BOLD);
exportFileMatOFF = [fMRSData_Summed_Path, strcat(fMRSData_Summed_OFF_BOLD,'.mat')];
% % % % save(exportFileMatOFF,fMRSData_Summed_OFF_BOLD);
load(strrep(strcat(currentfMRSDataPath, '\',outputFileNameBase_OFF,extensionMat),defaultSubject,subjects{indexCurrentSubject}));
fMRSData_Summed_OFF_MR_spectroS = aOFF;
fMRSData_Summed_OFF_MR_spectroS.Data{1} = fMRSData_Summed_OFF;
save(exportFileMatOFF,'fMRSData_Summed_OFF_MR_spectroS')
if ~isempty(strfind(localFilePathBase,'Modulus'))
    fMRSData_Summed_OFF = real(fMRSData_Summed_OFF).*2;
    fMRSData_Summed_OFF_MR_spectroS.Data{1} = fMRSData_Summed_OFF;
end
fMRSData_Summed_OFF_MR_spectroS.ExportLcmRaw(fMRSData_Summed_Path, fMRSData_Summed_OFF_BOLD, 'Yes', 'Yes');

load(strrep(strcat(currentfMRSDataPath, '\',outputFileNameBase_ON,extensionMat),defaultSubject,subjects{indexCurrentSubject}));
fMRSData_Summed_ON_MR_spectroS = aON;
fMRSData_Summed_ON_MR_spectroS.Data{1} = fMRSData_Summed_ON;
save(exportFileMatON,'fMRSData_Summed_ON_MR_spectroS')
if ~isempty(strfind(localFilePathBase,'Modulus'))
    fMRSData_Summed_ON = real(fMRSData_Summed_ON).*2;
    fMRSData_Summed_ON_MR_spectroS.Data{1} = fMRSData_Summed_ON;
end
fMRSData_Summed_ON_MR_spectroS.ExportLcmRaw(fMRSData_Summed_Path, fMRSData_Summed_ON_BOLD, 'Yes', 'Yes');


%% create and save difference spectrum from summed indiviually BOLD corrected spectra
fMRSData_Summed_ON = squeeze(fMRSData_Summed_ON);
fMRSData_Summed_OFF = squeeze(fMRSData_Summed_OFF);
fMRSData_Difference = fMRSData_Summed_ON - fMRSData_Summed_OFF;
fMRSData_Difference = permute(fMRSData_Difference,[1 3 4 5 6 7 8 9 2]);
if exist('excludeSubjects')
    fMRSData_Difference_BOLD = strcat('Diff_',visualType,strrep(BOLDType{1},'#','_'),MMBType,'_Excluded');
else
    fMRSData_Difference_BOLD = strcat('Diff_',visualType,strrep(BOLDType{1},'#','_'),MMBType);
end
eval([fMRSData_Difference_BOLD '=fMRSData_Difference']);
exportFileMatDifference = [fMRSData_Summed_Path, strcat(fMRSData_Difference_BOLD,'.mat')];
% % % % save(exportFileMatDifference,fMRSData_Difference_BOLD);
fMRSData_Difference_MR_spectroS = fMRSData_Summed_ON_MR_spectroS;
fMRSData_Difference_MR_spectroS.Data{1} = fMRSData_Difference;
fMRSData_Difference_MR_spectroS.ExportLcmRaw(fMRSData_Summed_Path, fMRSData_Difference_BOLD, 'Yes', 'Yes');
save(exportFileMatDifference,'fMRSData_Difference_MR_spectroS')
% % % % LCModelCallerInstance.CopyDataToRemote(fMRSData_Summed_Path, LCModelfMRSDataPathRemote, strcat(fMRSData_Difference_BOLD,extensionRAW))

%% create and save figure
ppmVector = fMRSData_Difference_MR_spectroS.getPpmVector;
figure
plot(ppmVector,real(fftshift(fft(fMRSData_Summed_ON(:,1))))); hold on
plot(ppmVector,real(fftshift(fft(fMRSData_Summed_OFF(:,1)))))
plot(ppmVector,real(fftshift(fft(fMRSData_Difference(:,1)))))
legend('ON','OFF','Difference')
set(gca,'xDir','reverse')
xlim([0 4.2])
fig2save_Difference = fullfile(fMRSData_Summed_Path, strcat(fMRSData_Difference_BOLD,'.fig'));
savefig(fig2save_Difference)
close all;

%% filter difference spectrum from summed indiviually BOLD corrected spectra
% dwellTime = fMRSData_Difference_MR_spectroS.Parameter.Headers.DwellTimeSig_ns*1E-9;
% t = (0:(size(fMRSData_Difference_MR_spectroS.Data{1},fMRSData_Difference_MR_spectroS.kx_dim))-1)*dwellTime;
% data_nonFiltered = squeeze(fMRSData_Difference_MR_spectroS.Data{1});
% Fgau = 10;  %parameter that sets the decay rate in Hz
% L = 0;      %linewidth in Hz, L positive: line broadening, L negative: resolution enhancement
% W = exp(-t'*pi*L).*exp(- t'.^2*(pi*Fgau)^2);
% data_Filtered = data_nonFiltered(:,1).*W;
% fMRSData_Difference_Filtered_BOLD = strcat('Diff_Filtered',BOLDType{1},MMBType);
% eval([fMRSData_Difference_Filtered_BOLD '=data_Filtered']);
% exportFileMatDifferenceFiltered = [fMRSData_Summed_Path, strcat(fMRSData_Difference_Filtered_BOLD,'.mat')];
% % % % save(exportFileMatDifferenceFiltered,fMRSData_Difference_Filtered_BOLD);
% fMRSData_Difference_Filtered_MR_spectroS = fMRSData_Difference_MR_spectroS;
% fMRSData_Difference_Filtered_MR_spectroS.Data{1} = data_Filtered;
% fMRSData_Difference_Filtered_MR_spectroS.ExportLcmRaw(fMRSData_Summed_Path, fMRSData_Difference_Filtered_BOLD, 'Yes', 'Yes');
% save(exportFileMatDifferenceFiltered,'fMRSData_Difference_Filtered_MR_spectroS')
% % % % % LCModelCallerInstance.CopyDataToRemote(fMRSData_Summed_Path, LCModelfMRSDataPathRemote, strcat(fMRSData_Difference_Filtered_BOLD,extensionRAW))


%% create and save difference spectrum from summed and afterwards BOLD corrected spectra
if ~isempty(strfind(BOLDType{1},'woBOLDCorr'))
    fMRSData_Summed_ON_noBOLD = load([fMRSData_Summed_Path, fMRSData_Summed_ON_BOLD]);
    fields = fieldnames(fMRSData_Summed_ON_noBOLD);
    fMRSData_Summed_ON_noBOLD = fMRSData_Summed_ON_noBOLD.(fields{1});
    fMRSData_Summed_OFF_noBOLD = load([fMRSData_Summed_Path, fMRSData_Summed_OFF_BOLD]);
    fields = fieldnames(fMRSData_Summed_OFF_noBOLD);
    fMRSData_Summed_OFF_noBOLD = fMRSData_Summed_OFF_noBOLD.(fields{1});
    % Do BOLD lineshape correction
    fMRSData_Summed_noBOLD = fMRSData_Summed_OFF_noBOLD;
    fMRSData_Summed_noBOLD_DataOFF = squeeze(fMRSData_Summed_OFF_noBOLD.Data{1});
    fMRSData_Summed_noBOLD_DataON = squeeze(fMRSData_Summed_ON_noBOLD.Data{1});
    fMRSData_Summed_noBOLD_DataOFFON(:,:,2) = fMRSData_Summed_noBOLD_DataOFF;
    fMRSData_Summed_noBOLD_DataOFFON(:,:,1) = fMRSData_Summed_noBOLD_DataON;
    fMRSData_Summed_noBOLD_DataOFFON = permute(fMRSData_Summed_noBOLD_DataOFFON,[1 4 5 6 7 8 9 10 2 11 12 3]);
    fMRSData_Summed_noBOLD.Data{1} = fMRSData_Summed_noBOLD_DataOFFON;
    
    fMRSData_Summed_noBOLD.Parameter.fMRSDiffSettings.PhaseCorrection = 0;
    fMRSData_Summed_noBOLD.Parameter.fMRSDiffSettings.frequencyShift = 0;
    fMRSData_Summed_noBOLD.Parameter.fMRSDiffSettings.OffsetCorrection = 0;
    fMRSData_Summed_noBOLD.Parameter.fMRSDiffSettings.Automatic = true;
    fMRSData_Summed_noBOLD.Parameter.fMRSDiffSettings.indecesRegion = [1480 1510]; %NAA
    fMRSData_Summed_noBOLD.Parameter.fMRSAveSettings.paradigm = [0 1 1 1];
    
    fMRSData_Summed_noBOLD.Parameter.ReconFlags.isfMRSDifference = 0;
    fMRSData_Summed_noBOLD = fMRSData_Summed_noBOLD.fMRSDifference;
    
    %save ON OFF
    %ON
    temp = squeeze(fMRSData_Summed_noBOLD.Data{1});
    temp = temp(:,:,1);
    fMRSData_Summed_ON_afterSummation = fMRSData_Summed_noBOLD;
    fMRSData_Summed_ON_afterSummation.Data{1} = permute(temp,[1 4 5 6 7 8 9 10 2 11 12 3]);
    if exist('excludeSubjects')
        fMRSData_Summed_ON_BOLDafterSummation = strcat('Summed_', num2str(visualType), 'ON',strrep(BOLDType{1},'#','_'),MMBType,'_Excluded_BOLDafterSum');
    else
        fMRSData_Summed_ON_BOLDafterSummation = strcat('Summed_', num2str(visualType), 'ON',strrep(BOLDType{1},'#','_'),MMBType,'_BOLDafterSum');
    end
    eval([fMRSData_Summed_ON_BOLDafterSummation '=fMRSData_Summed_ON_afterSummation']);
    exportFileMatON = [fMRSData_Summed_Path, strcat(fMRSData_Summed_ON_BOLDafterSummation,'.mat')];
    save(exportFileMatON,'fMRSData_Summed_ON_afterSummation')
    if ~isempty(strfind(localFilePathBase,'Modulus'))
        temp = real(temp).*2;
        fMRSData_Summed_ON_afterSummation.Data{1} = temp;
    end
    fMRSData_Summed_ON_afterSummation.ExportLcmRaw(fMRSData_Summed_Path, fMRSData_Summed_ON_BOLDafterSummation, 'Yes', 'Yes');
    %OFF
    temp = squeeze(fMRSData_Summed_noBOLD.Data{1});
    temp = temp(:,:,2);
    fMRSData_Summed_OFF_afterSummation = fMRSData_Summed_noBOLD;
    fMRSData_Summed_OFF_afterSummation.Data{1} = permute(temp,[1 4 5 6 7 8 9 10 2 11 12 3]);
    if exist('excludeSubjects')
        fMRSData_Summed_OFF_BOLDafterSummation = strcat('Summed_', num2str(visualType), 'OFF',strrep(BOLDType{1},'#','_'),MMBType,'_Excluded_BOLDafterSum');
    else
        fMRSData_Summed_OFF_BOLDafterSummation = strcat('Summed_', num2str(visualType), 'OFF',strrep(BOLDType{1},'#','_'),MMBType,'_BOLDafterSum');
    end
    eval([fMRSData_Summed_OFF_BOLDafterSummation '=fMRSData_Summed_OFF_afterSummation']);
    exportFileMatON = [fMRSData_Summed_Path, strcat(fMRSData_Summed_OFF_BOLDafterSummation,'.mat')];
    save(exportFileMatON,'fMRSData_Summed_OFF_afterSummation')
    if ~isempty(strfind(localFilePathBase,'Modulus'))
        temp = real(temp).*2;
        fMRSData_Summed_OFF_afterSummation.Data{1} = temp;
    end
    fMRSData_Summed_OFF_afterSummation.ExportLcmRaw(fMRSData_Summed_Path, fMRSData_Summed_OFF_BOLDafterSummation, 'Yes', 'Yes');
    
    % create and save difference spectrum
    %save only residual
    temp = squeeze(fMRSData_Summed_noBOLD.Data{1});
    temp = temp(:,:,3);
    fMRSData_Difference_BOLDafterSummation_MRSpectro = fMRSData_Summed_noBOLD;
    fMRSData_Difference_BOLDafterSummation_MRSpectro.Data{1} = permute(temp,[1 3 4 5 6 7 8 9 2]);
    if exist('excludeSubjects')
        fMRSData_Difference_BOLDafterSummation = strcat('Diff_',visualType,strrep(BOLDType{1},'#','_'),MMBType,'_Excluded_BOLDafterSum');
        fig2save_fMRSData_Summed_noBOLD = fullfile(fMRSData_Summed_Path, strcat('Diff_',visualType,strrep(BOLDType{1},'#','_'),MMBType,'_Excluded_BOLDafterSum.fig'));
    else
        fMRSData_Difference_BOLDafterSummation = strcat('Diff_',visualType,strrep(BOLDType{1},'#','_'),MMBType,'_BOLDafterSum');
        fig2save_fMRSData_Summed_noBOLD = fullfile(fMRSData_Summed_Path, strcat('Diff_',visualType,strrep(BOLDType{1},'#','_'),MMBType,'_BOLDafterSum.fig'));
    end
    eval([fMRSData_Difference_BOLDafterSummation '=fig2save_fMRSData_Summed_noBOLD']);
    exportFileMatDifferenceBOLDafterSummation = [fMRSData_Summed_Path, strcat(fMRSData_Difference_BOLDafterSummation, '.mat')];
    save(exportFileMatDifferenceBOLDafterSummation,'fMRSData_Difference_BOLDafterSummation_MRSpectro')
    if ~isempty(strfind(localFilePathBase,'Modulus'))
        temp = real(temp).*2;
        fMRSData_Difference_BOLDafterSummation_MRSpectro.Data{1} = temp;
    end
    fMRSData_Difference_BOLDafterSummation_MRSpectro.ExportLcmRaw(fMRSData_Summed_Path, fMRSData_Difference_BOLDafterSummation, 'Yes', 'Yes');
    clear temp
    
    % create and save figure
    %     timePoints      = size(fMRSData_Summed_noBOLD.Data{1},1);
    %     dwellTime       = fMRSData_Summed_noBOLD.Parameter.Headers.DwellTimeSig_ns * 1e-9; %in secs
    %     timeVector      = linspace(0,(timePoints-1)*dwellTime,timePoints)';
    %     plot(real(fftshift(fft(data(:,1,1).*exp(-pi*0.3*timeVector).*exp(1i*2*pi*0.09*timeVector))))-real(fftshift(fft(data(:,1,2)))))
    
    figure; hold on;
    data = squeeze(fMRSData_Summed_noBOLD.Data{1});
    ppmVector = fMRSData_Summed_noBOLD.getPpmVector;
    plot(ppmVector,real(fftshift(fft(real(data(:,1,1))))))
    plot(ppmVector,real(fftshift(fft(real(data(:,1,2))))))
    plot(ppmVector,real(fftshift(fft(real(data(:,1,3))))))
    set(gca, 'XDir','reverse')
    xlim([0.5 4.5]); legend('On','Off','Residual')
    
    if exist('excludeSubjects')
        fig2save_fMRSData_Summed_noBOLD = fullfile(fMRSData_Summed_Path, strcat('Diff_',visualType,strrep(BOLDType{1},'#','_'),MMBType,'_Excluded_BOLDafterSum.fig'));
    else
        fig2save_fMRSData_Summed_noBOLD = fullfile(fMRSData_Summed_Path, strcat('Diff_',visualType,strrep(BOLDType{1},'#','_'),MMBType,'_BOLDafterSum.fig'));
    end
    savefig(fig2save_fMRSData_Summed_noBOLD)
    close all;
    
    
end
%% sum Water reference spectra of all volunteers, weighted with water fit (need to run doFitting_fMRS_Water.m first)
WaterReferenceTable = (readtable(strcat(LCModelOutputPath,'WaterReference_Fit.xlsx')));
WaterReferenceID = table2cell(WaterReferenceTable(2:end,1));
WaterReference = str2double(table2array(WaterReferenceTable(2:end,2:end)));
WRefDataScaled = zeros(4096,numberOfSubjects);    %samples, subjects, data/water
for indexCurrentSubject = 1:numberOfSubjects
    idx = find(ismember(WaterReferenceID, strcat(prefixes{indexCurrentSubject}, subjects{indexCurrentSubject})));
    currentScalingFactor = WaterReference(idx,1);
    currentWRefDataPath = strcat(localFilePathBase,subjectsPath{indexCurrentSubject});
    load(strrep(strcat(currentWRefDataPath, '\',outputFileNameBase_WRef,extensionMat),defaultSubject,subjects{indexCurrentSubject}));
    currentfMRSData_WRef = squeeze(a.Data{1});
    currentfMRSDataScaled_WRef = currentfMRSData_WRef./currentScalingFactor;
    WRefDataScaled(:,indexCurrentSubject) = currentfMRSDataScaled_WRef;
end
fMRSData_Summed_WRef = squeeze(sum(WRefDataScaled,2));

%save to local Path and LCModel
LCModelfMRSDataPathRemote = '/Desktop/1H_fMRS/Summed_fMRSData/';
fMRSData_Summed_Path = strcat(localFilePathBase,summedPath,'\');
if exist('excludeSubjects')
    exportFileMatWRef = [fMRSData_Summed_Path, strcat('fMRSData_Summed_WRef_Excluded.mat')];
else
    exportFileMatWRef = [fMRSData_Summed_Path, strcat('fMRSData_Summed_WRef.mat')];
end
% % % save(exportFileMatWRef,'fMRSData_Summed_WRef');
load(strrep(strcat(currentWRefDataPath, '\',outputFileNameBase_WRef,extensionMat),defaultSubject,subjects{indexCurrentSubject}));
fMRSData_Summed_WRef_MR_spectroS = a;
fMRSData_Summed_WRef_MR_spectroS.Data{1} = fMRSData_Summed_WRef;
if exist('excludeSubjects')
    fMRSData_Summed_WRef_MR_spectroS.ExportLcmRaw(fMRSData_Summed_Path, 'fMRSData_Summed_WRef_Excluded', 'Yes', 'Yes');
else
    fMRSData_Summed_WRef_MR_spectroS.ExportLcmRaw(fMRSData_Summed_Path, 'fMRSData_Summed_WRef', 'Yes', 'Yes');
end
save(exportFileMatWRef,'fMRSData_Summed_WRef_MR_spectroS')
% % LCModelCallerInstance.CopyDataToRemote(fMRSData_Summed_Path, LCModelfMRSDataPathRemote, strcat('fMRSData_Summed_WRef',extensionRAW))


%% do fitting
LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(summedControlFilesPathRemote, summedLCModelOutputFilesRemote, ...
    summedLCModelOutputPath);
%for Stim ON / OFF
if exist('excludeSubjects')
    waterRefFileName = strrep(defaultWaterRef, defaultSubject, 'fMRSData_Summed');
    waterRefFileName = strcat(waterRefFileName,'_Excluded');
    currentControlFiles = strrep(LCModelControlFiles, defaultSubject, 'Summed');
    currentControlFiles = strrep(currentControlFiles,'.control','_Excluded.control');
    currentControlFiles = strrep(currentControlFiles,'fMRS_v2_','fMRS_');
    currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, 'Summed');
    currentOutputFiles = strcat(currentOutputFiles,'_Excluded');
else
    waterRefFileName = strrep(defaultWaterRef, defaultSubject, 'fMRSData_Summed');
    currentControlFiles = strrep(LCModelControlFiles, defaultSubject, 'Summed');
    currentControlFiles = strrep(currentControlFiles,'fMRS_v2_','fMRS_');
    currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, 'Summed');
end

for indexFMRS = 1:length(currentControlFiles)
    currentOutputFile = currentOutputFiles{indexFMRS};
    currentFMRS = orderFMRS{indexFMRS};
    currentLCModelOutputFile = LCModelOutputFiles{indexFMRS};
    %     if exist('excludeSubjects')
    %     createLCModelConfigfMRSDiff('fMRS', controlFilesPathLocal, summedControlFilesPathLocal, defaultControlFile, ...
    %         currentFMRS, defaultfMRS, defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFile, currentLCModelOutputFile,...
    %         waterRefFileName, defaultWaterRef, summedPath, defaultSubjectsPath, 'fMRSData_Summed',...
    %         defaultSubject, strcat(MMBType,'_Excluded'), defaultMMB,'');
    %     else
    createLCModelConfigfMRSDiff('fMRS', controlFilesPathLocal, summedControlFilesPathLocal, defaultControlFile, ...
        currentFMRS, defaultfMRS, defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFile, currentLCModelOutputFile,...
        waterRefFileName, defaultWaterRef, summedPath, defaultSubjectsPath, 'fMRSData_Summed',...
        defaultSubject, MMBType, defaultMMB,'');
    %     end
end
% % % fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, summedControlFilesPathRemote, ...
% % %     summedControlFilesPathLocal, preprocessedFilesPathLocal);

% don't copy the .RAW LCModel file to the LCModel computer, it's already
% done earlier
if exist('excludeSubjects')
    currentOutputFiles = strcat('Summed_',orderFMRS, MMBType,'_Excluded');
else
    currentOutputFiles = strcat('Summed_',orderFMRS, MMBType);
end
fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, summedControlFilesPathRemote, ...
    summedControlFilesPathLocal, preprocessedFilesPathLocal);

%for Difference spectrum from summed indiviually BOLD corrected spectra
LCModelOutputFilesDiff = strcat(outputFileNameBase(1:end-1), BOLDType{1}, MMBType);
LCModelControlFilesDiff = strcat(controlFilesBaseDiff(1:end-1), BOLDType{1}, MMBType, controlFilesBaseSuffix);
if exist('excludeSubjects')
    currentControlFilesDiff = strrep(LCModelControlFilesDiff, strcat('Difference_',defaultSubject), strcat('Diff_',visualType));
    currentControlFilesDiff = strrep(currentControlFilesDiff,'.control','_Excluded.control');
    currentOutputFilesDiff = strrep(LCModelOutputFilesDiff, defaultSubject, strcat('Diff_',visualType));
    currentOutputFilesDiff = strcat(currentOutputFilesDiff,'_Excluded');
    waterRefFileName = strrep(defaultWaterRef, defaultSubject, 'fMRSData_Summed');
    waterRefFileName = strcat(waterRefFileName,'_Excluded');
else
    currentControlFilesDiff = strrep(LCModelControlFilesDiff, strcat('Difference_',defaultSubject), strcat('Diff_',visualType));
    currentOutputFilesDiff = strrep(LCModelOutputFilesDiff, defaultSubject, strcat('Diff_',visualType));
    waterRefFileName = strrep(defaultWaterRef, defaultSubject, 'fMRSData_Summed');
end

% if exist('excludeSubjects')
%     createLCModelConfigfMRSDiff('fMRS', controlFilesPathLocal, summedControlFilesPathLocal, defaultControlFileDiff, ...
%         BOLDType{1}, strcat('_',defaultfMRS), defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFilesDiff, LCModelOutputFilesDiff,...
%         waterRefFileName, defaultWaterRef, summedPath, defaultSubjectsPath, 'fMRSData_Diff',...
%         defaultSubject, strcat(MMBType,'_Excluded'), defaultMMB,'');
% else
createLCModelConfigfMRSDiff('fMRS', controlFilesPathLocal, summedControlFilesPathLocal, defaultControlFileDiff, ...
    BOLDType{1}, strcat('_',defaultfMRS), defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFilesDiff, LCModelOutputFilesDiff,...
    waterRefFileName, defaultWaterRef, summedPath, defaultSubjectsPath, 'fMRSData_Diff',...
    defaultSubject, MMBType, defaultMMB,'');
% end

%fittingLCModel needs cell arrays
currentControlFilesDiff = cellstr(currentControlFilesDiff);
if exist('excludeSubjects')
    currentOutputFilesDiff = strcat('Diff_',visualType,BOLDType, MMBType,'_Excluded');
    currentControlFilesDiffwoBaseline = strcat(controlFilesBaseDiff(1:end-1), BOLDType, MMBType, '_Excluded_woBaseline', controlFilesBaseSuffix);
    currentControlFilesDiffwoBaseline = strrep(currentControlFilesDiffwoBaseline, strcat('Difference_',defaultSubject), strcat('Diff_',visualType));
else
    currentOutputFilesDiff = strcat('Diff_',visualType,BOLDType, MMBType);
    currentControlFilesDiffwoBaseline = strcat(controlFilesBaseDiff(1:end-1), BOLDType, MMBType, '_woBaseline', controlFilesBaseSuffix);
    currentControlFilesDiffwoBaseline = strrep(currentControlFilesDiffwoBaseline, strcat('Difference_',defaultSubject), strcat('Diff_',visualType));
end
fittingLCModel(LCModelCallerInstance, currentControlFilesDiff, currentOutputFilesDiff, summedControlFilesPathRemote, ...
    summedControlFilesPathLocal, preprocessedFilesPathLocal);
% subtract baseline from difference spectrum and fit again
currentLCModelFitwoBaseline = strcat(currentOutputFilesDiff, '_woBaseline');

%check save option for .mat file in subtractBaseline_fMRS
subtractBaseline_fMRS(fMRSData_Summed_Path,summedLCModelOutputPath,currentOutputFilesDiff{1},currentLCModelFitwoBaseline{1}, 'Lac')
if exist('excludeSubjects')
    currentOutputFilesDiff = strrep(strcat(outputFileNameBase(1:end-1),'_',visualType, BOLDType{1}, MMBType, '_Excluded_woBaseline'), defaultSubject, 'Diff');
    LCModelOutputFilesDiffwoBaseline = strcat(outputFileNameBase(1:end-1),BOLDType{1}, MMBType, '_woBaseline');
else
    currentOutputFilesDiff = strrep(strcat(outputFileNameBase(1:end-1),'_',visualType, BOLDType{1}, MMBType, '_woBaseline'), defaultSubject, 'Diff');
    LCModelOutputFilesDiffwoBaseline = strcat(outputFileNameBase(1:end-1),BOLDType{1}, MMBType, '_woBaseline');
end
% currentOutputFilesDiff = strrep(LCModelOutputFilesDiffwoBaseline, defaultSubject, 'Diff');
createLCModelConfigfMRSDiff('fMRS', controlFilesPathLocal, summedControlFilesPathLocal, defaultControlFileDiff, ...
    BOLDType{1}, strcat('_',defaultfMRS), defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFilesDiff, LCModelOutputFilesDiffwoBaseline,...
    waterRefFileName, defaultWaterRef, summedPath, defaultSubjectsPath, 'fMRSData_Diff',...
    defaultSubject, strcat(MMBType,'_woBaseline'), '','');
fittingLCModel(LCModelCallerInstance, currentControlFilesDiffwoBaseline, currentLCModelFitwoBaseline, summedControlFilesPathRemote, ...
    summedControlFilesPathLocal,preprocessedFilesPathLocal);


% %for filtered Difference spectrum from summed indiviually BOLD corrected spectra
% LCModelOutputFilesDiff = strcat(outputFileNameBase(1:end-1), BOLDType{1}, MMBType);
% LCModelControlFilesDiff = strcat(controlFilesBaseDiff(1:end-1), BOLDType{1}, MMBType, controlFilesBaseSuffix);
% currentControlFilesDiff = strrep(LCModelControlFilesDiff, strcat('Difference_',defaultSubject), 'Diff_Filtered');
% currentOutputFilesDiff = strrep(LCModelOutputFilesDiff, defaultSubject, 'Diff_Filtered');
% waterRefFileName = strrep(defaultWaterRef, defaultSubject, 'fMRSData_Summed');
%
% createLCModelConfigfMRSDiff('fMRS', controlFilesPathLocal, summedControlFilesPathLocal, defaultControlFileDiff, ...
%     BOLDType{1}, strcat('_',defaultfMRS), defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFilesDiff, LCModelOutputFilesDiff,...
%     waterRefFileName, defaultWaterRef, summedPath, defaultSubjectsPath, 'fMRSData_Diff_Filtered',...
%     defaultSubject, MMBType, defaultMMB,'');
%
% %fittingLCModel needs cell arrays
% currentControlFilesDiff = cellstr(currentControlFilesDiff);
% currentOutputFilesDiff = strcat('Diff_Filtered',BOLDType, MMBType);
% fittingLCModel(LCModelCallerInstance, currentControlFilesDiff, currentOutputFilesDiff, summedControlFilesPathRemote, ...
%     summedControlFilesPathLocal, preprocessedFilesPathLocal);
%
% currentLCModelFitwoBaseline = strcat(currentOutputFilesDiff, '_woBaseline');
% currentControlFilesDiffwoBaseline = strcat(controlFilesBaseDiff(1:end-1), BOLDType, MMBType, '_woBaseline', controlFilesBaseSuffix);
% currentControlFilesDiffwoBaseline = strrep(currentControlFilesDiffwoBaseline, strcat('Difference_',defaultSubject), 'Diff_Filtered');
% %check save option for .mat file in subtractBaseline_fMRS
% subtractBaseline_fMRS(fMRSData_Summed_Path,summedLCModelOutputPath,currentOutputFilesDiff{1},currentLCModelFitwoBaseline{1}, 'Lac')
% %subtract baseline from difference spectrum and fit again
% LCModelOutputFilesDiffwoBaseline = strcat(outputFileNameBase(1:end-1), BOLDType{1}, MMBType, '_woBaseline');
% currentOutputFilesDiff = strrep(LCModelOutputFilesDiffwoBaseline, defaultSubject, 'Diff_Filtered');
% createLCModelConfigfMRSDiff('fMRS', controlFilesPathLocal, summedControlFilesPathLocal, defaultControlFileDiff, ...
%     BOLDType{1}, strcat('_',defaultfMRS), defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFilesDiff, LCModelOutputFilesDiffwoBaseline,...
%     waterRefFileName, defaultWaterRef, summedPath, defaultSubjectsPath, 'fMRSData_Diff_Filtered',...
%     defaultSubject, strcat(MMBType,'_woBaseline'), '','');
% fittingLCModel(LCModelCallerInstance, currentControlFilesDiffwoBaseline, currentLCModelFitwoBaseline, summedControlFilesPathRemote, ...
%     summedControlFilesPathLocal,preprocessedFilesPathLocal);

% for spectra from summed and afterwards BOLD corrected spectra
if ~isempty(strfind(BOLDType{1},'woBOLDCorr'))
    %for Stim ON / OFF
    if exist('excludeSubjects')
        waterRefFileName = strrep(defaultWaterRef, defaultSubject, 'fMRSData_Summed');
        waterRefFileName = strcat(waterRefFileName,'_Excluded');
        currentControlFiles = strrep(LCModelControlFiles, defaultSubject, 'Summed');
        currentControlFiles = strrep(currentControlFiles,'.control','_Excluded_BOLDafterSum.control');
        currentControlFiles = strrep(currentControlFiles,'fMRS_v2_','fMRS_');
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, 'Summed');
        currentOutputFiles = strcat(currentOutputFiles,'_Excluded_BOLDafterSum');
    else
        waterRefFileName = strrep(defaultWaterRef, defaultSubject, 'fMRSData_Summed');
        currentControlFiles = strrep(LCModelControlFiles, defaultSubject, 'Summed');
        currentControlFiles = strrep(currentControlFiles,'.control','_BOLDafterSum.control');
        currentControlFiles = strrep(currentControlFiles,'fMRS_v2_','fMRS_');
        currentOutputFiles = strrep(strcat(LCModelOutputFiles,'_BOLDafterSum'), defaultSubject, 'Summed');
    end
    
    
    for indexFMRS = 1:length(currentControlFiles)
        currentOutputFile = currentOutputFiles{indexFMRS};
        currentFMRS = orderFMRS{indexFMRS};
        currentLCModelOutputFile = strcat(LCModelOutputFiles{indexFMRS},'_BOLDafterSum');
        createLCModelConfigfMRSDiff('fMRS', controlFilesPathLocal, summedControlFilesPathLocal, defaultControlFile, ...
            currentFMRS, defaultfMRS, defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFile, currentLCModelOutputFile,...
            waterRefFileName, defaultWaterRef, summedPath, defaultSubjectsPath, 'fMRSData_Summed',...
            defaultSubject, strcat(MMBType,'_BOLDafterSum'), strcat(defaultMMB,'_BOLDafterSum'),'');
    end
    
    if exist('excludeSubjects')
        currentOutputFiles = strcat('Summed_',orderFMRS, MMBType,'_Excluded_BOLDafterSum');
    else
        currentOutputFiles = strcat('Summed_',orderFMRS, MMBType,'_BOLDafterSum');
    end
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, summedControlFilesPathRemote, ...
        summedControlFilesPathLocal, preprocessedFilesPathLocal);
    
    %for difference spectrum
    
    LCModelOutputFilesDiff = strcat(outputFileNameBase(1:end-1), BOLDType{1}, MMBType, '_BOLDafterSum');
    LCModelControlFilesDiff = strcat(controlFilesBaseDiff(1:end-1), BOLDType, MMBType, '_BOLDafterSum', controlFilesBaseSuffix);
    if exist('excludeSubjects')
        currentControlFilesDiff = strrep(LCModelControlFilesDiff, strcat('Difference_',defaultSubject), strcat('Diff_',visualType));
        currentControlFilesDiff = strrep(currentControlFilesDiff,'_BOLDafterSum','_Excluded_BOLDafterSum');
        currentOutputFilesDiff = strrep(LCModelOutputFilesDiff, defaultSubject, strcat('Diff_',visualType));
        currentOutputFilesDiff = strrep(currentOutputFilesDiff,'_BOLDafterSum','_Excluded_BOLDafterSum');
        waterRefFileName = strrep(defaultWaterRef, defaultSubject, 'fMRSData_Summed');
        waterRefFileName = strcat(waterRefFileName,'_Excluded');
    else
        currentControlFilesDiff = strrep(LCModelControlFilesDiff, strcat('Difference_',defaultSubject), strcat('Diff_',visualType));
        currentOutputFilesDiff = strrep(LCModelOutputFilesDiff, defaultSubject, strcat('Diff_',visualType));
        waterRefFileName = strrep(defaultWaterRef, defaultSubject, 'fMRSData_Summed');
    end
    
    createLCModelConfigfMRSDiff('fMRS', controlFilesPathLocal, summedControlFilesPathLocal, defaultControlFileDiff, ...
        BOLDType{1}, strcat('_',defaultfMRS), defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFilesDiff, LCModelOutputFilesDiff,...
        waterRefFileName, defaultWaterRef, summedPath, defaultSubjectsPath, 'fMRSData_Diff',...
        defaultSubject, strcat(MMBType,'_BOLDafterSum'), '','');
    
    %fittingLCModel needs cell arrays
    
    currentControlFilesDiff = cellstr(currentControlFilesDiff);
    if exist('excludeSubjects')
        currentOutputFilesDiff = strcat('Diff_',visualType,BOLDType, MMBType,'_Excluded_BOLDafterSum');
        %     currentControlFilesDiffwoBaseline = strcat(controlFilesBaseDiff(1:end-1), BOLDType, MMBType, '_Excluded_woBaseline', controlFilesBaseSuffix);
        %     currentControlFilesDiffwoBaseline = strrep(currentControlFilesDiffwoBaseline, strcat('Difference_',defaultSubject), strcat('Diff_',visualType));
    else
        currentOutputFilesDiff = strcat('Diff_',visualType,BOLDType, MMBType,'_BOLDafterSum');
        %     currentControlFilesDiffwoBaseline = strcat(controlFilesBaseDiff(1:end-1), BOLDType, MMBType, '_woBaseline', controlFilesBaseSuffix);
        %     currentControlFilesDiffwoBaseline = strrep(currentControlFilesDiffwoBaseline, strcat('Difference_',defaultSubject), strcat('Diff_',visualType));
    end
    
    fittingLCModel(LCModelCallerInstance, currentControlFilesDiff, currentOutputFilesDiff, summedControlFilesPathRemote, ...
        summedControlFilesPathLocal,preprocessedFilesPathLocal);
    %     subtract baseline from difference spectrum and fit again
    currentLCModelFitwoBaseline = strcat(currentOutputFilesDiff, '_woBaseline');
    if exist('excludeSubjects')
        %         currentOutputFilesDiff = strcat('Diff_',visualType,BOLDType, MMBType,'_Excluded');
        currentControlFilesDiffwoBaseline = strcat(controlFilesBaseDiff(1:end-1), BOLDType, MMBType, '_Excluded_BOLDafterSum_woBaseline', controlFilesBaseSuffix);
        currentControlFilesDiffwoBaseline = strrep(currentControlFilesDiffwoBaseline, strcat('Difference_',defaultSubject), strcat('Diff_',visualType));
    else
        %         currentOutputFilesDiff = strcat('Diff_',visualType,BOLDType, MMBType);
        currentControlFilesDiffwoBaseline = strcat(controlFilesBaseDiff(1:end-1), BOLDType, MMBType, '_BOLDafterSum_woBaseline', controlFilesBaseSuffix);
        currentControlFilesDiffwoBaseline = strrep(currentControlFilesDiffwoBaseline, strcat('Difference_',defaultSubject), strcat('Diff_',visualType));
    end
    %check save option for .mat file in subtractBaseline_fMRS
    subtractBaseline_fMRS(fMRSData_Summed_Path,summedLCModelOutputPath,currentOutputFilesDiff{1},currentLCModelFitwoBaseline{1}, 'Lac')
    if exist('excludeSubjects')
        currentOutputFilesDiff = strrep(strcat(outputFileNameBase(1:end-1),'_',visualType, BOLDType{1}, MMBType, '_Excluded_BOLDafterSum_woBaseline'), defaultSubject, 'Diff');
        LCModelOutputFilesDiffwoBaseline = strcat(outputFileNameBase(1:end-1),BOLDType{1}, MMBType, '_BOLDafterSum_woBaseline');
    else
        currentOutputFilesDiff = strrep(strcat(outputFileNameBase(1:end-1),'_',visualType, BOLDType{1}, MMBType, '_BOLDafterSum_woBaseline'), defaultSubject, 'Diff');
        LCModelOutputFilesDiffwoBaseline = strcat(outputFileNameBase(1:end-1),BOLDType{1}, MMBType, '_BOLDafterSum_woBaseline');
    end
    createLCModelConfigfMRSDiff('fMRS', controlFilesPathLocal, summedControlFilesPathLocal, defaultControlFileDiff, ...
        BOLDType{1}, strcat('_',defaultfMRS), defaultLCModelUser, LCModelCallerInstance.LCModelUser, currentOutputFilesDiff, LCModelOutputFilesDiffwoBaseline,...
        waterRefFileName, defaultWaterRef, summedPath, defaultSubjectsPath, 'fMRSData_Diff',...
        defaultSubject, strcat(MMBType,'_BOLDafterSum_woBaseline'), '','');
    fittingLCModel(LCModelCallerInstance, currentControlFilesDiffwoBaseline, currentLCModelFitwoBaseline, summedControlFilesPathRemote, ...
        summedControlFilesPathLocal,preprocessedFilesPathLocal);
    
end
%% create concentration table for Stim ON / OFF
tableConcentrations = [];
isFirstIter = 1;
s = 1;
for indexorderFMRS = 1:length(orderFMRS)
    % retrieve parameters
    if exist('excludeSubjects')
        currentOutputFiles = strrep(strcat(LCModelOutputFiles,'_Excluded'), defaultSubject, 'Summed');
    else
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, 'Summed');
    end
    currentPath = strcat(LCModelOutputPath,summedPath,'/',currentOutputFiles,'.table');
    [c1 c2 c3 c4] = textread(currentPath{indexorderFMRS},'%s %s %s %s');
    
    if isFirstIter
        start  = find(strcmp(c1,'Conc.'));
        start  = start + 1;
        finish = find(strcmpi(c1,'$$MISC')) - 1;
        tableConcentrations{1,1} = 'ID';
        
        s = 2;
        for j=start:finish
            if(strcmp(c4{j},'')==1)
                c3_split = strsplit(c3{j},'+');
                if length(c3_split) == 1
                    c3_split = strsplit(c3{j},'-');
                end
                tableConcentrations{1,s} = c3_split{2};
            else
                tableConcentrations{1,s} = c4{j};
                if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                    tableConcentrations{1,s} = 'MMB';
                end
            end
            tableConcentrations{1,s+1} = 'CRLB %';
            s          = s + 2;
        end
        tableConcentrations{1,s} = 'water concentration';
    end
    
    % add ID and Water conc to table
    tableConcentrations{indexorderFMRS+1,1} = orderFMRS{indexorderFMRS};
    index_wconc = find(strcmp(c1,'wconc='));
    wconc_LCModel = str2num(c2{index_wconc});
    tableConcentrations{indexorderFMRS+1,end} = wconc_LCModel;
    % add metabolites to table
    s = 2;
    for j=start:finish
        %add quantified metabolite
        tableConcentrations{indexorderFMRS+1,s} = str2num( c1{j});
        %add CRLB
        tableConcentrations{indexorderFMRS+1,s+1} = str2num(  c2{j}(1:end-1));
        s = s+2;
    end
    
    isFirstIter = false;
    
end


% save table to file
if exist('excludeSubjects')
    ExpName = strrep(strcat('LCModelFit_Summed_', num2str(visualType), BOLDType, MMBType,'_Excluded'),'#','_');
else
    ExpName = strrep(strcat('LCModelFit_Summed_', num2str(visualType), BOLDType, MMBType),'#','_');
end
ExpName = ExpName{1};   %convert 1x1cell to string
exportFileXlsx = [LCModelOutputPath ,strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)

%% calculate percent changes Stim OFF vs Stim ON, save result tables
load(exportFileMat)
[~, rawFile, ~] = fileparts(exportFileMat);
LCModelFitTable = eval(rawFile);
metaboliteNames = LCModelFitTable(1,4:2:end-2);
metaboliteNames = strrep(metaboliteNames,'+','_');

ON = cell2mat(LCModelFitTable(2:2:end,4:2:end-2));
OFF = cell2mat(LCModelFitTable(3:2:end,4:2:end-2));
PercDiff = 100./OFF.*ON-100;
PercDiffMean = mean(PercDiff);
PercDiffStd = std(PercDiff);

rowNames = {'PercDiff'};
if exist('excludeSubjects')
    ExpNameDiff = strrep(strcat('ConcDiff_Summed_', num2str(visualType),BOLDType, MMBType,'_Excluded'),'#','_');
else
    ExpNameDiff = strrep(strcat('ConcDiff_Summed_', num2str(visualType),BOLDType, MMBType),'#','_');
end
ExpNameDiff = ExpNameDiff{1};   %convert 1x1cell to string
ConcDiff_Summed = array2table(PercDiff,'rowNames',rowNames,'VariableNames',metaboliteNames);
eval([ExpNameDiff '=ConcDiff_Summed']);
exportFileMatDiff = [LCModelOutputPath ,strcat(ExpNameDiff,'.mat')];
save(exportFileMatDiff,ExpNameDiff)

%% create concentration table for Stim ON / OFF BOLD Corr after summation
if ~isempty(strfind(BOLDType{1},'woBOLDCorr'))
    tableConcentrations = [];
    isFirstIter = 1;
    s = 1;
    for indexorderFMRS = 1:length(orderFMRS)
        % retrieve parameters
        if exist('excludeSubjects')
            currentOutputFiles = strrep(strcat(LCModelOutputFiles,'_Excluded_BOLDafterSum'), defaultSubject, 'Summed');
        else
            currentOutputFiles = strrep(strcat(LCModelOutputFiles,'_BOLDafterSum'), defaultSubject, 'Summed');
        end
        currentPath = strcat(LCModelOutputPath,summedPath,'/',currentOutputFiles,'.table');
        [c1 c2 c3 c4] = textread(currentPath{indexorderFMRS},'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
            tableConcentrations{1,s} = 'water concentration';
        end
        
        % add ID and Water conc to table
        tableConcentrations{indexorderFMRS+1,1} = orderFMRS{indexorderFMRS};
        index_wconc = find(strcmp(c1,'wconc='));
        wconc_LCModel = str2num(c2{index_wconc});
        tableConcentrations{indexorderFMRS+1,end} = wconc_LCModel;
        % add metabolites to table
        s = 2;
        for j=start:finish
            %add quantified metabolite
            tableConcentrations{indexorderFMRS+1,s} = str2num( c1{j});
            %add CRLB
            tableConcentrations{indexorderFMRS+1,s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end
    
    
    % save table to file
    if exist('excludeSubjects')
        ExpName = strrep(strcat('LCModelFit_Summed_', num2str(visualType),BOLDType, MMBType, '_Exc_BOLDaftS'),'#','_');
    else
        ExpName = strrep(strcat('LCModelFit_Summed_', num2str(visualType),BOLDType, MMBType, '_BOLDaftS'),'#','_');
    end
    ExpName = ExpName{1};   %convert 1x1cell to string
    exportFileXlsx = [LCModelOutputPath ,strcat(ExpName,'.xlsx')];
    xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
    cellPosition = 'A2';
    xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);
    
    exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
    eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
    save(exportFileMat, ExpName)
    
    % calculate percent changes Stim OFF vs Stim ON, save result tables
    load(exportFileMat)
    [~, rawFile, ~] = fileparts(exportFileMat);
    LCModelFitTable = eval(rawFile);
    metaboliteNames = LCModelFitTable(1,4:2:end-2);
    metaboliteNames = strrep(metaboliteNames,'+','_');
    
    ON = cell2mat(LCModelFitTable(2:2:end,4:2:end-2));
    OFF = cell2mat(LCModelFitTable(3:2:end,4:2:end-2));
    PercDiff = 100./OFF.*ON-100;
    PercDiffMean = mean(PercDiff);
    PercDiffStd = std(PercDiff);
    
    rowNames = {'PercDiff'};
    if exist('excludeSubjects')
        ExpNameDiff = strrep(strcat('ConcDiff_Summed_', num2str(visualType),BOLDType, MMBType,'_Excl_BOLDaftS'),'#','_');
    else
        ExpNameDiff = strrep(strcat('ConcDiff_Summed_', num2str(visualType),BOLDType, MMBType,'_BOLDaftS'),'#','_');
    end
    ExpNameDiff = ExpNameDiff{1};   %convert 1x1cell to string
    ConcDiff_Summed = array2table(PercDiff,'rowNames',rowNames,'VariableNames',metaboliteNames);
    eval([ExpNameDiff '=ConcDiff_Summed']);
    exportFileMatDiff = [LCModelOutputPath ,strcat(ExpNameDiff,'.mat')];
    save(exportFileMatDiff,ExpNameDiff)
end
end