function [dataComplex, bandwidth, scanFrequency, metaboliteName, singletPresent] = ImportLCModelBasis(filepath, filename, plotBasis, nuclues)

if ~exist('plotBasis', 'var')
    plotBasis = false;
end

if ~exist('nuclues','var')
    nuclues = '1H';
end

if ~(strcmp(filepath(end),'\') || strcmp(filepath(end),'/'))
    filepath = [filepath '/'];
end

if ~contains(filename,'.raw', 'IgnoreCase',true)
    filename = strcat(filename, '.RAW');
end
%open the .RAW data 
filename = strcat(filepath, filename);
%
fileString      =fileread(filename);
fileSplitString = strsplit(fileString);
%
index=find(strcmp(fileSplitString, 'Sweep'));
if ~isempty(index)
    bandwidth = str2double(fileSplitString{index+3});
else
    bandwidth = 8000;
    warning('Bandwidth could not be read. Assuming 8000Hz.');
end
%
index=find(strcmp(fileSplitString, 'Vector'));
if ~isempty(index)
    vectorSize = str2double(fileSplitString{index+3});
else
    vectorSize = NaN;
    warning('Vector size could not be read');
end
%
index=find(strcmp(fileSplitString, 'B0'));
if ~isempty(index)
    scanFrequency = str2double(fileSplitString{index+3});
else
    scanFrequency = 399;
    warning('Scan frequency could not be read. Assuming 399Hz');
end
%
index=find(strcmp(fileSplitString, 'Singlet'));
if ~isempty(index)
    singletPresent = strcmpi(fileSplitString{index+2}, 'true');
else
    singletPresent = NaN;
    warning('Vector size could not be read');
end

%
indexStartLCModelParameters=find(strcmp(fileSplitString, '$NMID'));
metaboliteName = fileSplitString{indexStartLCModelParameters+1};
if strcmp(metaboliteName(1:3),'ID=')
    metaboliteName = metaboliteName(5:end-1);
else
    error('Metabolite name should start with "ID=". Read in value is: %s', metaboliteName)
end
%
index=find(strcmp(fileSplitString, 'BRUKER='));
if ~isempty(index)
    brukerparam = fileSplitString{index+1};
    if strcmpi(brukerparam,'T')
        bruker = true;
    else
        bruker = false;
    end
else
    bruker = false;
end
%
indexEndLCModelParameters=find(strcmp(fileSplitString, '$END'));
indexEndLCModelParameters = indexEndLCModelParameters(end);
%
if isnan(vectorSize)
    data = str2double(fileSplitString(indexEndLCModelParameters+1:end));
    vectorSize = floor(length(data)/2);
end
%
data = str2double(fileSplitString(indexEndLCModelParameters+1:indexEndLCModelParameters+2*vectorSize));
if bruker
    dataComplex = complex(data(1:2:end),data(2:2:end));
else
    dataComplex = complex(data(1:2:end),data(2:2:end))';
end


if plotBasis
    bw     = linspace(-bandwidth/2,bandwidth/2,length(dataComplex));
    switch nuclues
        case '1H'
            ppmVector    = bw/(scanFrequency) + 4.7;
        case '31P'
            ppmVector    = bw/(scanFrequency);
        otherwise
                error('Nucleus not known');
    end     
    
    figure;
    plot(ppmVector,real(fftshift(fft(dataComplex))))
    xlabel('ppm');
    xlim([min(ppmVector) max(ppmVector)])
    set(gca,'xDir','reverse');
    title(metaboliteName,'Interpreter','none');
%     figure;
%     timeVector = linspace(0,1/bandwidth*length(dataComplex),length(dataComplex))*1000;
%     plot(timeVector,real(dataComplex));
%     xlabel('time[ms]');
%     xlim([min(timeVector) max(timeVector)])
end
display('Import...FINISH.');
end

