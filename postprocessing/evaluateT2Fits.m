function [figureIds, sortedIndeces] = evaluateT2Fits(downField_MM_Met, offsetPlot, figureIds, sortedIndeces)

if ~exist('downField_MM_Met','var')
    downField_MM_Met = 'Met';
end

if ~exist('offsetPlot','var')
    offsetPlot = 0;
end
if ~exist('figureIds','var')
    figureIds = {};
end

doMetaboliteFWHMs = false;

if offsetPlot <= 0
    plotColors = {[0 0 1], [1 0 0]};
    plotColors2 = {[0.0 0.4 0.0], [1 0.5 0.3]};
    plotColors2 = {[0.0 0.4 0.0], [0 0 0]};
%     plotColors2 = {[0.3 0.3 0.1], [0.2 0.5 0.05]};
else
    plotColors = {[0.0 0.7 0.9], [1 0 1]};
    plotColors2 = {[0.1 0.9 0.1], [0.9 0.6 0]};
    plotColors2 = {[0.1 0.9 0.1], [0.5 0.5 0.5]};
end

sortT2sDescending = true;
usePpm = true;

if isempty(figureIds)
    emptyFigureIds = true;
else
    emptyFigureIds = false;
end

switch downField_MM_Met
    case 'DF'
        pathNameMM = 'DF data path';
        outputFolder = 'Output\';
    case 'Met'
        pathNameMM = 'DF data path';
        outputFolder = 'Output_UF\';
    case 'MM'
        pathNameMM = 'MM data path';
        outputFolder = 'Output\';
    case 'MM WM'
        pathNameMM = 'MM WM data path';
        outputFolder = 'Output\';
end
sampleCriteriaMM = {outputFolder};
data_path = pathToDataFolder(pathNameMM, sampleCriteriaMM);

load([data_path outputFolder 'tableT2sSubjects.mat'],'tableT2sSubjects')
load([data_path outputFolder 'tableT2sSummed.mat'],'tableT2sSummed')
if exist([data_path outputFolder 'tableFWHMSubjects.mat'],'file')
    doFWHMevalutation = true;
else
    doFWHMevalutation = false;
end
if doFWHMevalutation
    load([data_path outputFolder 'tableFWHMSubjects.mat'],'tableFWHMSubjects')
    load([data_path outputFolder 'tableFWHMSummed.mat'],'tableFWHMSummed')
    load([data_path outputFolder 'tableFWHMSubjects.mat'],'orderMetFWHMSubjects')
    load([data_path outputFolder 'tableFWHMSummed.mat'],'orderMetFWHMSummed')
end

switch downField_MM_Met
    case 'DF'
        metaboliteNameOrder = {'Cr';'DF58';'DF60';'DF68';'DF73';'DF75';'DF82';'DF83';'DF835';'hCs';'ATP';'hist';'NAD';'NAA Broad';'NAA';};
        metaboliteNameOrderFWHM = metaboliteNameOrder;
        subjects = {'1658';'1717';'2017';'3373';'3490';'5771';'6249';'6971';'7338';'7782';'9810'};
        metaboliteNamesDisplay = metaboliteNameOrder;
    case 'Met'
        metaboliteNameOrder = {'Leu';'Asp';'Cr-CH3';'Cr-CH2';'GABA';'Glu';'Gln';'GSH_no';'Glyc';'GPC';'mI';'NAA-asp';'NAA-ace';'NAAG';'PCh';'PE';'Scyllo';'Tau';'tCho';'tNAA';'mI+Glyc';'Glx'};
        metaboliteNameOrder = {'Leu';'Asp';'Cr-CH3';'Cr-CH2';'GABA';'Glu';'Gln';'GSH_no';'Glyc';'mI';'NAA-asp';'NAA-ace';'NAAG';'Scyllo';'Tau';'tCho_PE';'tNAA';'mI+Glyc';'Glx'; 'Lac'; 'NAA+NAAG'};
        %         metaboliteNameOrder = {'Leu';'Asp';'Cr-CH3';'PCr';'GABA';'Glu';'Gln';'GSH_no';'Glyc';'mI';'NAA-asp';'NAA-ace';'NAAG';'Scyllo';'Tau';'tCho';'PE';'tNAA';'mI+Glyc';'Glx'; 'Lac'; 'Cr+PCr'};
        
        %actual values to plot in final version
        metaboliteNameOrder = {'Asp';'Cr-CH3';'Cr-CH2';'Glu';'Gln';'GSH';'Glyc';'mI';'NAA-asp';'NAA-ace';'NAAG';'tCho_PE';'tNAA+';'mI+Glyc';'Glx';};%'Leu';'GABA';'Scyllo';'Tau'};'tNAA';
        metaboliteNameOrderFWHM = metaboliteNameOrder;
        metaboliteNamesDisplay = {'Asp';'tCr(CH3)';'tCr(CH2)';'Glu';'Gln';'GSH';'Glyc';'mI';'NAA(CH2)';'NAA(CH3)';'NAAG';'tCho+';'tNAA+';'mI+Glyc';'Glx';};%'Leu';'GABA';'Scyllo';'Tau'};'tNAA';
        
        
        if doMetaboliteFWHMs
            metaboliteNameOrder = {'Cr-CH3' 'Cr-CH2' 'NAA-ace' 'tCho_PE'};
            metaboliteNameOrderFWHM = metaboliteNameOrder;
            metaboliteNamesDisplay = {'tCr(CH3)';'tCr(CH2)';'NAA(CH3)';'tCho+';};
        else
            doFWHMevalutation = false;
        end
        
        subjects = {'1658';'1706';'1717';'2017';'3373';'3490';'5771';'6249';'6971';'7338';'7782';'9810'};
    case 'MM'
        metaboliteNameOrder = {'M0.92';'M1.21';'M1.39';'M1.67';'M2.04';'M2.26';'M2.56';'M2.70';'M2.99';'M3.21';'M3.62';'M3.75';'M3.86';'M4.03'; 'tCr(CH2)'};
        metaboliteNameOrderFWHM = {'M_{0.92}';'M_{1.21}';'M_{1.39}';'M_{1.67}';'M_{2.04}';'M_{2.26}';'M_{2.56}';'M_{2.70}';'M_{2.99}';'M_{3.21}';'M_{3.62}';'M_{3.75}';'M_{3.86}';'M_{4.03}'; 'tCr(CH_2)'};
        subjects = {'1658';'1706';'1717';'2017';'3373';'3490';'5771';'6249';'6971';'7338';'7782';'9810'};
        metaboliteNamesDisplay = metaboliteNameOrder;
    case 'MM WM'
        metaboliteNameOrder = {'M0.92';'M1.21';'M1.39';'M1.67';'M2.04';'M2.26';'M2.56';'M2.70';'M2.99';'M3.21';'M3.62';'M3.75';'M3.86';'M4.03'; 'tCr(CH2)'};
        metaboliteNameOrderFWHM = {'M_{0.92}';'M_{1.21}';'M_{1.39}';'M_{1.67}';'M_{2.04}';'M_{2.26}';'M_{2.56}';'M_{2.70}';'M_{2.99}';'M_{3.21}';'M_{3.62}';'M_{3.75}';'M_{3.86}';'M_{4.03}'; 'tCr(CH_2)'};
        subjects = {'1658';'1717';'2016';'2020';'3490'};
        metaboliteNamesDisplay = metaboliteNameOrder;
        metaboliteNamesDisplay{end} = 'tCr(CH2)';
end

if doFWHMevalutation
    if sum(strcmp(orderMetFWHMSubjects, orderMetFWHMSummed)) ~= length(orderMetFWHMSubjects)
        error('The order of the metabolites in the FWHM tables have to be identical')
    end
end

switch downField_MM_Met
    case 'DF'
        yAxisLim = 115;
    case 'MM'
        yAxisLim = 85;
    case 'MM WM'
        yAxisLim = 85;
    case 'Met'
        yAxisLim = 11;
end

numberOfMet = length(metaboliteNameOrder);

scanFrequency = 399.719;
numOfSubjects = length(tableT2sSubjects);
T2s = cell(numOfSubjects,numberOfMet);
R2s = cell(numOfSubjects,numberOfMet);
for indexSubject = 1:numOfSubjects
    for indexMetabolite = 1:numberOfMet
        index = find(strcmp(metaboliteNameOrder(indexMetabolite), tableT2sSubjects{indexSubject}(1,2:end)));
        T2times = tableT2sSubjects{indexSubject}(2,2:end);
        R2times = tableT2sSubjects{indexSubject}(3,2:end);
        T2s(indexSubject,indexMetabolite) = T2times(index);
        R2s(indexSubject,indexMetabolite) = R2times(index);
    end
end


T2sSummed = cell(1,numberOfMet);
R2sSummed = cell(1,numberOfMet);
for indexMetabolite = 1:numberOfMet
    index = find(strcmp(metaboliteNameOrder(indexMetabolite), tableT2sSummed{1}(1,2:end)));
    T2times = tableT2sSummed{1}(2,2:end);
    R2times = tableT2sSummed{1}(3,2:end);
    T2sSummed(1,indexMetabolite) = T2times(index);
    R2sSummed(1,indexMetabolite) = R2times(index);
end

T2sNumeric = str2double(T2s);
R2sNumeric = str2double(R2s);
nonNegT2s = T2sNumeric;
nonNegT2s(nonNegT2s<0) = NaN;
reshape(nonNegT2s,numberOfMet, numOfSubjects);
meanT2s = mean(nonNegT2s,1,'omitNaN')
stdT2s = std(nonNegT2s,1,'omitNaN')

acceptableT2s = nonNegT2s;
acceptableR2s = R2sNumeric;
T2SummedNumeric = str2double(T2sSummed);
R2SummedNumeric = str2double(R2sSummed);
switch downField_MM_Met
    case 'DF'
        acceptableT2s(R2sNumeric<=0.1) = NaN;
        T2SummedNumeric(R2SummedNumeric<=0.1) = NaN;
    case 'MM'
        acceptableT2s(R2sNumeric<=0.5) = NaN;
        acceptableR2s(R2sNumeric<=0.5) = NaN;
        T2SummedNumeric(R2SummedNumeric<=0.5) = NaN;
        R2SummedNumeric(R2SummedNumeric<=0.5) = NaN;
    case 'MM WM'
        %TODO manually set the R2sNumeric and R2SummedNumeric of M4.03 to 0
        acceptableT2s(R2sNumeric<=0.5) = NaN;
        acceptableR2s(R2sNumeric<=0.5) = NaN;
        T2SummedNumeric(R2SummedNumeric<=0.5) = NaN;
        R2SummedNumeric(R2SummedNumeric<=0.5) = NaN;
    case 'Met'
        acceptableT2s(R2sNumeric<=0.5) = NaN;
        acceptableR2s(R2sNumeric<=0.5) = NaN;
        T2SummedNumeric(R2SummedNumeric<=0.5) = NaN;
        R2SummedNumeric(R2SummedNumeric<=0.5) = NaN;
end
%write the metabolites/subjects with issues
metaboliteNameOrder(sum(isnan(acceptableT2s))>0)
sum(isnan(acceptableT2s))
subjects(sum(isnan(acceptableT2s),2)>0)
sum(isnan(acceptableT2s),2)

reshape(acceptableT2s,numberOfMet, numOfSubjects);
acceptableMeanT2s = mean(acceptableT2s,1,'omitNaN');
acceptableStdT2s = std(acceptableT2s,1,'omitNaN');
acceptableMeanR2s = mean(acceptableR2s,1,'omitNaN');
acceptableStdR2s = std(acceptableR2s,1,'omitNaN');
numberOfNaNs = sum(sum(isnan(acceptableT2s)))
plusMinusSign = [' ' char(177) ' '];
tableT2Summarized = cell(numberOfMet+1,4);
tableT2Summarized(2:end,1) = metaboliteNamesDisplay;
% tableT2Summarized(1,3) = {'Std'};
% tableT2Summarized(1,4) = {['T2' plusMinusSign 'Std']};
% tableT2Summarized(1,5) = {['R2' plusMinusSign 'Std']};
% tableT2Summarized(2:end,2) = cellstr(num2str(acceptableMeanT2s','%.2f'));
% tableT2Summarized(2:end,3) = cellstr(num2str(acceptableStdT2s','%.2f'));
tableT2Summarized(1,2) = {'T2 Summed'};
tableT2Summarized(1,3) = {['T2' plusMinusSign 'Std']};
tableT2Summarized(1,4) = {['R2' plusMinusSign 'Std']};
for index=1:numberOfMet
    tableT2Summarized{index+1,2} = num2str(T2SummedNumeric(index), '%.1f');
%     tableT2Summarized{index+1,4} = [num2str(acceptableMeanT2s(index), '%.2f') plusMinusSign  num2str(acceptableStdT2s(index), '%.2f')];
%     tableT2Summarized{index+1,5} = [num2str(acceptableMeanR2s(index), '%.2f') plusMinusSign  num2str(acceptableStdR2s(index), '%.2f')];
    tableT2Summarized{index+1,3} = [num2str(acceptableMeanT2s(index), '%.1f') plusMinusSign  num2str(acceptableStdT2s(index), '%.1f')];
    tableT2Summarized{index+1,4} = [num2str(acceptableMeanR2s(index), '%.2f') plusMinusSign  num2str(acceptableStdR2s(index), '%.2f')];
end

save([data_path , outputFolder, 'meanT2s.mat'],'acceptableMeanT2s', 'metaboliteNamesDisplay');
exportFileXlsx = [data_path , outputFolder, 'T2_Results_Mean_std.xlsx'];
xlswrite(exportFileXlsx, tableT2Summarized, 1, 'A1');

if sortT2sDescending
    if ~exist('sortedIndeces','var')
        [acceptableMeanT2Sorted, sortedIndeces] = sort(acceptableMeanT2s,  'descend');
    else
        acceptableMeanT2Sorted = acceptableMeanT2s(sortedIndeces);
    end
    metaboliteNameOrder = metaboliteNameOrder(sortedIndeces);
    metaboliteNameOrderFWHM = metaboliteNameOrderFWHM(sortedIndeces);
    acceptableStdT2s = acceptableStdT2s(sortedIndeces);
    T2SummedNumeric = T2SummedNumeric(sortedIndeces);
    acceptableT2s = acceptableT2s(:, sortedIndeces);
    R2sNumeric = R2sNumeric(:, sortedIndeces);
    nonNegT2s = nonNegT2s(:, sortedIndeces);
    metaboliteNamesDisplay = metaboliteNamesDisplay(sortedIndeces);
end

%% plot the T2 Box plots
if emptyFigureIds
    figureIds{1} = figure;
else
    figure(figureIds{1});
end
    

positionsTicks = [2.2:1.5:1.5*numberOfMet+1.5];
positions1 = positionsTicks + offsetPlot;
boxPlotT2 = boxplot(acceptableT2s, metaboliteNamesDisplay,'colors',plotColors{1},'symbol','+','positions',positions1,'width',0.18);
ylabel('T_2   [ms]');
axT2 = get(figureIds{1},'CurrentAxes');
axT2.XAxis.TickLength = [0,0];
axT2.YColor = 'k';

set(boxPlotT2(:,:),'linewidth',1);
switch downField_MM_Met
    case 'DF'
        set(gca, 'YLim', [-5,90]);
        set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
    case 'MM'
%         set(gca, 'YLim', [-5,150]);
%         set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
        set(gca, 'YLim', [-5,95]);
        set(gca, 'XLim', [positionsTicks(2)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
    case 'MM WM'
%         set(gca, 'YLim', [-5,150]);
%         set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
        set(gca, 'YLim', [-5,95]);
        set(gca, 'XLim', [positionsTicks(2)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
    case 'Met'
        set(gca, 'YLim', [-5,200]);
        set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
end

set(gca, 'YGrid', 'on');
% set(gca, 'YTickMode', 'manual');
% set(gca, 'YTickLabel', [0.1, 1, 10, 100]);
set(gca, 'XTickMode', 'manual');
set(gca, 'XTick', positionsTicks);
% set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
set(gca, 'XTickLabelRotation', 90.0);
set(gca, 'FontSize', 16);
% title('T_2^\rho Relaxation Times');
title('T_2 Relaxation Times');

%% summed T2s
hold on
scatter(positions1,T2SummedNumeric, 120, 'X','MarkerEdgeColor',plotColors{2}, 'LineWidth',2);

%% FWHM
if doFWHMevalutation
    fwhmReshaped = zeros(size(tableFWHMSubjects,1)*size(tableFWHMSubjects,2), numberOfMet);
    for indexMetabolite = 1 :numberOfMet
        
        indexTable = find(strcmp(orderMetFWHMSubjects(:), metaboliteNameOrderFWHM(indexMetabolite)));
        
        fwhms = tableFWHMSubjects(:,:,indexTable);
        fwhmReshaped(:,indexMetabolite) = fwhms(:);
        %     figure;
        %     plot(fwhms(:));
        %     title(metaboliteNameOrder{indexMetabolite})
    end
    
    %% summed FWHM
    FWHMSummedNumeric = squeeze(median(tableFWHMSummed,2, 'omitnan'))';
    
    if sortT2sDescending
        % Attention! "fwhmReshaped" is sorted already in the code above!
        % fwhmReshaped = fwhmReshaped(:,sortedIndeces);
        FWHMSummedNumeric = FWHMSummedNumeric(sortedIndeces);
    end
    
    %% T2 corrected FWHM
    for i = 1:size(fwhmReshaped,1)
        %     fwhmReshapedT2corrected(i,:) = fwhmReshaped(i,:) - 1./(pi*acceptableT2s(floor(i./5)+1)*1e-3);
        
        fwhmReshaped3(i,:) = fwhmReshaped(i,:) - 1./(pi*acceptableMeanT2s*1e-3);
    end
    FWHMSummedNumeric3 = FWHMSummedNumeric - 1./(pi*acceptableMeanT2s*1e-3);
    if sortT2sDescending
        fwhmReshaped3 = fwhmReshaped3 - fwhmReshaped3(:,1);
        FWHMSummedNumeric3 = FWHMSummedNumeric3 - FWHMSummedNumeric3(:,1);
    else
        fwhmReshaped3 = fwhmReshaped3 - fwhmReshaped3(:,end);
        FWHMSummedNumeric3 = FWHMSummedNumeric3 - FWHMSummedNumeric3(:,end);
    end
    T2RelaxationLinewidth = 1./(pi*acceptableT2s*1e-3);
    T2RelaxationLinewidthSummed = 1./(pi*T2SummedNumeric*1e-3);
    
    
    
    
    %% plotting of FWHM
    if emptyFigureIds
        figureIds{2} = plotFWHMs(fwhmReshaped, FWHMSummedNumeric, metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors, 'Full Width Half Maxima', usePpm, yAxisLim, offsetPlot);
        figureIds{3} = plotFWHMs(fwhmReshaped3, FWHMSummedNumeric3, metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, 'Full-Width-Half-Maxima T_2 and B_0 corrected', usePpm, yAxisLim, offsetPlot);
    else
        figureIds{2} = plotFWHMs(fwhmReshaped, FWHMSummedNumeric, metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors, 'Full Width Half Maxima', usePpm, yAxisLim, offsetPlot, figureIds{2});
        figureIds{3} = plotFWHMs(fwhmReshaped3, FWHMSummedNumeric3, metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, 'Full-Width-Half-Maxima T_2 and B_0 corrected', usePpm, yAxisLim, offsetPlot, figureIds{3});
    end
    
    %%
    if emptyFigureIds
        figureIds{4} = plotFWHMs(T2RelaxationLinewidth, T2RelaxationLinewidthSummed, metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, 'Full-Width-Half-Maxima vs. Linewidth Component from T_2', usePpm, yAxisLim, offsetPlot);
    else
        figureIds{4} = plotFWHMs(T2RelaxationLinewidth, T2RelaxationLinewidthSummed, metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors2, 'Full-Width-Half-Maxima vs. Linewidth Component from T_2', usePpm, yAxisLim, offsetPlot, figureIds{4});
    end
    % plotFWHMs(fwhmReshaped, FWHMSummedNumeric, metaboliteNameOrder, numberOfMet, scanFrequency, plotColors, 'Full-Width-Half-Maxima vs. Linewidth from T_2^\rho', usePpm, yAxisLim, offsetPlot, figureIds{4});
    figureIds{4} = plotFWHMs(fwhmReshaped, FWHMSummedNumeric, metaboliteNamesDisplay, numberOfMet, scanFrequency, plotColors, 'Full-Width-Half-Maxima vs. Linewidth Component from T_2', usePpm, yAxisLim, offsetPlot, figureIds{4});

    
    tableFWHMSummarized = cell(numberOfMet+1,4);
    tableFWHMSummarized(2:end,1) = metaboliteNamesDisplay;
    tableFWHMSummarized(1,2) = {['FWHM' plusMinusSign 'Std']};
    if usePpm
        meanFwhmReshaped = mean(fwhmReshaped./scanFrequency, 'omitnan');
        stdFwhmReshaped = std(fwhmReshaped./scanFrequency, 'omitnan');
        writePrecision = '%.3f';
    else
        meanFwhmReshaped = mean(fwhmReshaped, 'omitnan');
        stdFwhmReshaped = std(fwhmReshaped, 'omitnan');
        writePrecision = '%.2f';
    end
    for index=1:numberOfMet
        tableFWHMSummarized{index+1,2} = [num2str(meanFwhmReshaped(index), writePrecision) plusMinusSign  num2str(stdFwhmReshaped(index), writePrecision)];
    end
    exportFileXlsx = [data_path ,'Output\FWHM_Results_Mean_std.xlsx'];
    xlswrite(exportFileXlsx, tableFWHMSummarized, 1, 'A1');
end

end

function [figId] = plotFWHMs(fwhmReshaped, FWHMSummedNumeric, metaboliteNames, numberOfMet, scanFrequency, ...
    plotColors, titleName, usePpm, yAxisLim, offsetPlot, figId)
%% plot the FWHM Box plots
if ~exist('figId', 'var')
    figId = figure;
else
    figure(figId);
end
positionsTicks = 2.2:1.5:1.5*numberOfMet+1.5;
positions = positionsTicks + offsetPlot;
fwhm_plot_limit = 150;

if (usePpm)
    fwhmReshapedPpm = fwhmReshaped ./ scanFrequency;
    FWHMSummedNumericPpm = FWHMSummedNumeric ./ scanFrequency;
    fwhm_plot_limit_ppm = fwhm_plot_limit ./ scanFrequency;
    %set yyaxis left active, hence both
    yyaxis left
end

boxPlotFWHM = boxplot(fwhmReshaped, metaboliteNames,'colors',plotColors{1},'symbol','+','positions',positions,'width',0.18);

if (usePpm)
    yyaxis right
    boxPlotFWHMPpm = boxplot(fwhmReshapedPpm, metaboliteNames,'colors',plotColors{1},'symbol','+','positions',positions,'width',0.18);
    set(gca,'YColor','k');
    yyaxis left
end

% if isDownField
%     yyaxis left
%     set(gca, 'YLim', [0,fwhm_plot_limit]);
%     yyaxis right
%     set(gca, 'YLim', [0,fwhm_plot_limit_ppm]);
% end

ylabel('FWHM [Hz]')
if usePpm
    yyaxis right
    ylabel('FWHM [ppm]', 'Color', 'k')
    yyaxis left
end

ax = get(figId,'CurrentAxes');
ax.XAxis.TickLength = [0,0];
ax.YColor = 'k';

set(boxPlotFWHM(:,:),'linewidth',1);
set(gca, 'YGrid', 'on');
set(gca, 'XTickMode', 'manual');
set(gca, 'XTick', positionsTicks);
set(gca, 'XLim', [positionsTicks(1)-abs(offsetPlot)-0.8 positionsTicks(end)+abs(offsetPlot)+0.8]);
set(gca, 'XTickLabelRotation', 90.0);
set(gca, 'FontSize', 16);
title(titleName);

%% summed FWHM
hold on
scatter(positions,FWHMSummedNumeric, 120, 'X','MarkerEdgeColor',plotColors{2}, 'LineWidth',2)
if usePpm
    yyaxis right
    scatter(positions,FWHMSummedNumericPpm, 120, 'X','MarkerEdgeColor',plotColors{2}, 'LineWidth',2)
    yyaxis left
end

if usePpm
%     yLimLower = 0;
    yLimLower = -3; %Hz, negative to add the 0 line :)
    set(gca, 'YLim', [yLimLower,yAxisLim]);
    yyaxis right
    set(gca, 'YLim', [yLimLower/scanFrequency,yAxisLim./scanFrequency]);
else
    set(gca, 'YLim', [0,yAxisLim]);
end
end
