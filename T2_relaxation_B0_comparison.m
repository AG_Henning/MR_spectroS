function T2_relaxation_B0_comparison()

% 3T T2 relaxation times and std from Wyss 2018 MRM, Occipital lobe
% GSH taken as GSH2; NAAG as NAAG; tCho as tCho (all other moieties were ignored
metaboliteNames =    {'Asp','tCr(CH3)','tCr(CH2)', 'GABA', 'Gln',  'Glu', 'Gly', 'GSH', 'Lac', 'mI', 'NAA(CH3)', 'NAA(CH2)', 'NAAG', 'PE', 'Scy', 'Tau', 'tCho' };
relaxation_times_3T =[ 90,    144,      112,        75,     99,     122,    81,    76,  99,     229,    263,        229,        107,  86,   107,    102,  213   ];
relax_times_std_3T = [ 27,    17,       18,         25,     21,     32,     27,    21,  19,     105,    43,         38,         19,   26,   17,     18,   56    ];

% 4T T2 relaxation times and std from PRESS Michaeli 2002 MRM, Occipital lobe
metaboliteNames =     {'Asp','tCr(CH3)','tCr(CH2)', 'GABA', 'Gln',  'Glu', 'Gly', 'GSH', 'Lac', 'mI', 'NAA(CH3)', 'NAA(CH2)', 'NAAG', 'PE', 'Scy', 'Tau', 'tCho' };
relax_4T_Michaeli   = [ NaN,    150,      NaN,        NaN,  NaN,    NaN,   NaN,   NaN,  NaN,   NaN,     239,        NaN,       NaN,   NaN,   NaN,    NaN,   NaN  ];
relax_4T__std_Micha = [ NaN,     11,      NaN,        NaN,  NaN,    NaN,   NaN,   NaN,  NaN,   NaN,      22,        NaN,       NaN,   NaN,   NaN,    NaN,   NaN  ];

% 7T T2 relaxation times and std from PRESS Michaeli 2002 MRM, Occipital lobe
metaboliteNames =     {'Asp','tCr(CH3)','tCr(CH2)', 'GABA', 'Gln',  'Glu', 'Gly', 'GSH', 'Lac', 'mI', 'NAA(CH3)', 'NAA(CH2)', 'NAAG', 'PE', 'Scy', 'Tau', 'tCho' };
relax_7T_Michaeli   = [ NaN,    109,      NaN,        NaN,  NaN,    NaN,   NaN,   NaN,  NaN,   NaN,     158,        NaN,       NaN,   NaN,   NaN,    NaN,   NaN  ];
relax_7T__std_Micha = [ NaN,     12,      NaN,        NaN,  NaN,    NaN,   NaN,   NaN,  NaN,   NaN,      22,        NaN,       NaN,   NaN,   NaN,    NaN,   NaN  ];

% 7T T2 relaxation times and std from Marjanska 2011 NMRB, Occipital lobe
metaboliteNames =    {'Asp','tCr(CH3)','tCr(CH2)', 'GABA', 'Gln',  'Glu', 'Gly', 'GSH', 'Lac', 'mI', 'NAA(CH3)', 'NAA(CH2)', 'NAAG', 'PE', 'Scy', 'Tau', 'tCho' };
relaxation_times_7T =[ NaN,    95,       84,        NaN,    NaN,     93,   NaN,    61,   NaN,   95,    132,         90,        NaN,  NaN,   96,     93,   152   ];
relax_times_std_7T = [ NaN,    3,        2,         NaN,    NaN,     4,    NaN,    3,    NaN,    2,      6,         11,        NaN,  NaN,    8,      7,     3   ];

% 9.4T T2 relaxation times and std from Deelchand 2010 MRM, Occipital lobe
metaboliteNames =     {'Asp','tCr(CH3)','tCr(CH2)', 'GABA', 'Gln',  'Glu', 'Gly', 'GSH', 'Lac', 'mI', 'NAA(CH3)', 'NAA(CH2)', 'NAAG', 'PE', 'Scy', 'Tau', 'tCho' };
relax_94T_Deelchand = [ NaN,    71.9,      68.3,        NaN,  NaN,    NaN,   NaN,   NaN,  NaN,   NaN,     98,        NaN,       NaN,   NaN,   NaN,    NaN,  70.7  ];
relax_94T__std_Deelch=[ NaN,     5,         6.4,        NaN,  NaN,    NaN,   NaN,   NaN,  NaN,   NaN,    8.4,        NaN,       NaN,   NaN,   NaN,    NaN,   7.1  ];
% 9.4T T2 relaxation times and std from Murali Manohar & Borbath 2019 MRM, Occipital lobe
metaboliteNames =    {'Asp','tCr(CH3)','tCr(CH2)', 'GABA', 'Gln',  'Glu', 'Gly', 'GSH', 'Lac', 'mI', 'NAA(CH3)', 'NAA(CH2)', 'NAAG', 'PE', 'Scy', 'Tau', 'tCho' };
relaxation_times_94T=[ 73,    100,        82,        NaN,   50,     87,    62,    74,    NaN,   90,    110,        102,        43,   NaN,   NaN,    NaN,   90   ];
relax_times_std_94T =[ 38,     16,        11,        NaN,   11,     23,    11,    32,    NaN,   18,     28,         34,        16,   NaN,   NaN,    NaN,   24   ];
relaxation_times_94T=[ 50,    101,        84,        NaN,   41,     90,    58,    69,    NaN,   98,    110,        103,        43,   NaN,   NaN,    NaN,   92   ];
relax_times_std_94T =[ 15,     16,        10,        NaN,   11,     26,     7,    24,    NaN,   21,     29,         26,        17,   NaN,   NaN,    NaN,   25   ];
relaxation_times_94T=[ 50,    101,        84,        NaN,   41,     90,    58,    69,    NaN,   88,    110,        103,        43,   NaN,   NaN,    NaN,   92   ];
relax_times_std_94T =[ 15,     16,        10,        NaN,   11,     26,     7,    24,    NaN,   18,     29,         26,        17,   NaN,   NaN,    NaN,   25   ];

field_strengths = [3, 4, 7, 7, 9.4, 9.41];

relaxationTimes    =[relaxation_times_3T; relax_4T_Michaeli; relax_7T_Michaeli; relaxation_times_7T; ...
    relaxation_times_94T; relax_94T_Deelchand];
relaxationTimesStd =[relax_times_std_3T;  relax_4T__std_Micha; relax_7T__std_Micha; relax_times_std_7T;  ...
    relax_times_std_94T; relax_94T__std_Deelch];

for indexMet = 1:length(metaboliteNames)
    relaxationTimesMet = relaxationTimes(:,indexMet);
    relaxationTimesStdMet = relaxationTimesStd(:,indexMet);
    if sum(isnan(relaxationTimesMet)) <= 3
        figure
        for index_B0 = 1:length(field_strengths)
            hold on
            errorbar(field_strengths(index_B0), relaxationTimesMet(index_B0), relaxationTimesStdMet(index_B0))
        end
        title(metaboliteNames{indexMet})
        xlim([1,10])
        xlabel('B_0 [T]')
        ylabel('T_2 [ms]')
        savefig(['D:\Software\Spectro Data\MM\paper_T2_Met_MM\Revision1\T2_vs_B0\' metaboliteNames{indexMet} '.fig'])
        export_fig(['D:\Software\Spectro Data\MM\paper_T2_Met_MM\Revision1\T2_vs_B0\' metaboliteNames{indexMet} '.tif'])
    end
end

