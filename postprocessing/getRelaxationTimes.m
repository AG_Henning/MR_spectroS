function [T1_met, T2_met, metaboliteNames] = getRelaxationTimes(compare, metaboliteNames)
%% setting up data paths
switch compare
    case 'DF'
        outputFolder = 'Output\FWHMBA Results 7.5e-3\';
        pathName = 'DF data path';
    case 'MM'
        outputFolder = 'OutputMM\';
        pathName = 'DF data path';
    case 'Met'
        outputFolder = 'OutputMet\';
        pathName = 'DF data path';
    case 'DF_UF'
        outputFolder = 'Output_UF\';
        pathName = 'DF data path';
    case 'MM_UF'
        outputFolder = 'Output\';
        pathName = 'MM data path';
    case 'MM_Met'
        outputFolder = 'OutputMet\';
        pathName = 'MM data path';
        pathNameDF = 'DF data path';
        outputFolderDF = 'Output_UF\';
        data_pathDF = pathToDataFolder(pathNameDF,{outputFolderDF});
end
sampleCriteria = {outputFolder};
data_path = pathToDataFolder(pathName, sampleCriteria);
%% extracting the T2 and T1 relaxation times
switch compare
    case 'DF'
        %%
        load([data_path outputFolder 'tableT2sSummed.mat'], 'tableT2sSummed');
        load([data_path outputFolder 'tableT2sSubjects.mat'], 'tableT2sSubjects');
        numberOfMet = length(metaboliteNames);
        T1_met = ones(1,numberOfMet) * 500; %ms, averaged from Nicole 9.4T paper
        index = find(strcmp('NAD',metaboliteNames));
        T1_met(index) = 299; %ms for NAD from de Graaf MRM 2014
        T2sSummed = zeros(1,numberOfMet-1);
        T2times = tableT2sSummed{1}(2,2:end);
        numberOfSubjects = length(tableT2sSubjects);
        T2times = cell(numberOfSubjects, size(tableT2sSubjects{1}(2,2:end),2));
        for indexSubject = 1 : numberOfSubjects
            T2times(indexSubject,:) = tableT2sSubjects{indexSubject}(2,2:end);
        end
        tableT2sSubjects{1}{1,2} = 'Leu';
        for indexMetabolite = 1:numberOfMet
%             index = find(strcmp(metaboliteNames(indexMetabolite), tableT2sSummed{1}(1,2:end)));
            index = find(strcmp(metaboliteNames(indexMetabolite), tableT2sSubjects{1}(1,2:end)));
%             T2sSummed(1,indexMetabolite) = T2times(index);
            T2sSummed(1,indexMetabolite) = median(str2double(T2times(:,index)),'omitnan');
        end
        T2_met = T2sSummed;
    case 'MM'
        T1_met = 360; %ms a rough estimation TODO change it!
        T2_met = 25; %ms a rough estimation TODO change it!
    case 'DF_UF'
        [mapMet_Names, mapMet_NamesInverse, mapMetNamesDisplay, mapMetNamesDisplayInverse] = getMetaboliteNames('UF');
        %values taken from A.Wright ISMRM 2019 abstract
        metaboliteNamesT1 = {'NAA-asp'; 'NAA-ace'; 'tCho'; 'mI'; 'Gln'; 'Glu'; 'Cr-CH2'; 'Cr-CH3'; 'Tau'; 'GSH'; 'Glx'; 'tNAA'}; %'GABA'
        T1_met_known =      [1158     ;  1602    ;  1111; 1309 ;  1831; 1405 ; 1162    ;  1565   ; 1961 ; 1310 ; 1618 ; 1380  ]; %'966' (Gaba)
        
        defaultT1 = round(mean(T1_met_known(1:end-1)),2); %average of the MM T1 times
        [T1_met, metaboliteNames] = matchRelaxationTimes(T1_met_known, metaboliteNamesT1,  defaultT1, metaboliteNames, true, mapMet_Names);

        %get T2 values
%         load(['D:\Software\Spectro Data\DATA_df\Output_UF\meanT2s.mat'], 'acceptableMeanT2s', 'metaboliteNamesDisplay');

        load([data_path outputFolder 'meanT2s.mat'], 'acceptableMeanT2s', 'metaboliteNamesDisplay');

        defaultT2 = round(mean(acceptableMeanT2s),2);
        [T2_met, metaboliteNames] = matchRelaxationTimes(acceptableMeanT2s, metaboliteNamesDisplay,  defaultT2, metaboliteNames, true, mapMetNamesDisplay);
        Cr_names = {'Cr_CH2', 'Cr'};
        Cr_to_replace = {'Cr', 'PCr', 'Cr+PCr'};
        [T2_met] = replaceRelaxationTimes(acceptableMeanT2s, metaboliteNamesDisplay,  defaultT2, Cr_names, true, mapMetNamesDisplay, metaboliteNames,  T2_met, Cr_to_replace);
        
        NAA_names = {'NAA_as', 'NAA_ac'};
        NAA_to_replace = {'NAA'};
        [T2_met] = replaceRelaxationTimes(acceptableMeanT2s, metaboliteNamesDisplay,  defaultT2, NAA_names, true, mapMetNamesDisplay, metaboliteNames,  T2_met, NAA_to_replace);
        Cho_names = {'tCho_P'};
        Cho_to_replace = {'tCho', 'PE'};
        [T2_met] = replaceRelaxationTimes(acceptableMeanT2s, metaboliteNamesDisplay,  defaultT2, Cho_names, true, mapMetNamesDisplay, metaboliteNames,  T2_met, Cho_to_replace);
        
    case 'MM_UF'
        [mapMet_Names, ~] = getMetaboliteNames('MM');
        %get T1 values
        %values based on S.Manohar ISMRM 2019 abstract; Updated based on
        %values calculated on 16.01.2019
        metaboliteNamesT1 = {'MM09'; 'MM12'; 'MM14'; 'MM17'; 'MM20'; 'MM22'; 'MM26'; 'MM27'; 'MM30'; 'MM32'; 'MM36'; 'MM37'; 'MM38'; 'Cre'}; 
        T1_met_known =      [ 293  ; 289   ; 337   ; 249   ; 358   ; 322   ; 522   ; 580   ; 450   ; 284   ; 795   ; 461   ; 195   ; 1162]';
        
        defaultT1 = round(mean(T1_met_known(1:end-1)),2); %average of the MM T1 times
        [T1_met, metaboliteNames] = matchRelaxationTimes(T1_met_known, metaboliteNamesT1,  defaultT1, metaboliteNames, true);
        
        %get T2 values
        load([data_path outputFolder 'meanT2s.mat'], 'acceptableMeanT2s', 'metaboliteNamesDisplay');

        defaultT2 = round(mean(acceptableMeanT2s),2);
        [T2_met, metaboliteNames] = matchRelaxationTimes(acceptableMeanT2s, metaboliteNamesDisplay,  defaultT2, metaboliteNames, true, mapMet_Names);
        
    case 'MM_Met'
        [mapMet_Names, mapMet_NamesInverse, mapMetNamesDisplay, mapMetNamesDisplayInverse] = getMetaboliteNames('UF');
        %values taken from A.Wright ISMRM 2019 abstract
        metaboliteNamesT1 = {'NAA-asp'; 'NAA-ace'; 'tCho'; 'mI'; 'Gln'; 'Glu'; 'Cr-CH2'; 'Cr-CH3'; 'Tau'; 'GSH'; 'Glx'; 'tNAA'}; %'GABA'
        T1_met_known =      [1158     ;  1602    ;  1111; 1309 ;  1831; 1405 ; 1162    ;  1565   ; 1961 ; 1310 ; 1618 ; 1380  ]; %'966' (Gaba)
        %values taken from A.Wright MRM 2021 paper (revision 2)
        metaboliteNamesT1 = {'NAA-asp'; 'NAA-ace'; 'tCho'; 'mI'; 'Gln'; 'Glu'; 'Cr-CH2'; 'Cr-CH3'; 'Tau'; 'GSH'; 'Asp'; 'GABA'; 'Glyc'; 'NAAG'; 'PE'; 'Scyllo'; 'Glx';  'tNAA'; 'tCr'; 'tCho+'; 'mI+Glyc'}; 
        T1_met_known =      [1137     ;  1701    ;  1161;  1499;  1214; 1343 ;  1118    ;  1678  ; 2063;  1505;  1300;  1240;    739;    1185;  1305;   1632;    1366;   1385;   1528;    1241;  1540]; 
        
        defaultT1 = round(mean(T1_met_known(1:end-1)),2); %average of the MM T1 times
        [T1_met, metaboliteNames] = matchRelaxationTimes(T1_met_known, metaboliteNamesT1,  defaultT1, metaboliteNames, true, mapMet_Names);%CHECK ME

        Cr_names = {'Cr+PCr'};
        Cr_to_replace = {'Cr', 'PCr', 'Cr+PCr'};
        [T1_met] = replaceRelaxationTimes(T1_met_known, metaboliteNamesT1,  defaultT1, Cr_names, true, mapMetNamesDisplay, metaboliteNames,  T1_met, Cr_to_replace);
        
        NAA_names = {'NAA+NAAG'};%trick to use tNAA - for T1 the name was used as NAA(CH2)+NAA(CH3), while normally it means NAA+NAAG
        NAA_to_replace = {'NAA','NAA+NAAG'};
        [T1_met] = replaceRelaxationTimes(T1_met_known, metaboliteNamesT1,  defaultT1, NAA_names, true, mapMetNamesDisplay, metaboliteNames,  T1_met, NAA_to_replace);

        %get T2 values
%         load(['D:\Software\Spectro Data\DATA_df\Output_UF\meanT2s.mat'], 'acceptableMeanT2s', 'metaboliteNamesDisplay');

        load([data_pathDF outputFolderDF 'meanT2s.mat'], 'acceptableMeanT2s', 'metaboliteNamesDisplay');

        defaultT2 = round(mean(acceptableMeanT2s),2);
        [T2_met, metaboliteNames] = matchRelaxationTimes(acceptableMeanT2s, metaboliteNamesDisplay,  defaultT2, metaboliteNames, true, mapMetNamesDisplay);
        Cr_names = {'Cr_CH2', 'Cr'};
        Cr_to_replace = {'Cr', 'PCr', 'Cr+PCr'};
        [T2_met] = replaceRelaxationTimes(acceptableMeanT2s, metaboliteNamesDisplay,  defaultT2, Cr_names, true, mapMetNamesDisplay, metaboliteNames,  T2_met, Cr_to_replace);
        
        NAA_names = {'NAA_as', 'NAA_ac'};
        NAA_to_replace = {'NAA'};
        [T2_met] = replaceRelaxationTimes(acceptableMeanT2s, metaboliteNamesDisplay,  defaultT2, NAA_names, true, mapMetNamesDisplay, metaboliteNames,  T2_met, NAA_to_replace);
        Cho_names = {'tCho_P'};
        Cho_to_replace = {'tCho', 'PE'};
        [T2_met] = replaceRelaxationTimes(acceptableMeanT2s, metaboliteNamesDisplay,  defaultT2, Cho_names, true, mapMetNamesDisplay, metaboliteNames,  T2_met, Cho_to_replace);

end
end



function [desiredRelaxationTimes, desiredMetaboliteNames] = matchRelaxationTimes(knownRelaxationTimes, knownMetaboliteNames,  defaultRelaxationTime, desiredMetaboliteNames, verbose, mapMet_Names)
if ~exist('verbose','var')
    verbose = false;
end

numberOfDesiredMetabolites = length(desiredMetaboliteNames);
desiredRelaxationTimes = zeros(1,numberOfDesiredMetabolites);
for index = 1:numberOfDesiredMetabolites
    if exist('mapMet_Names','var')
        desiredMetaboliteName = mapMet_Names(desiredMetaboliteNames{index});
    else
        desiredMetaboliteName = desiredMetaboliteNames{index};        
    end
    indexMetabolite = find(strcmp(desiredMetaboliteName, knownMetaboliteNames));
    if isempty(indexMetabolite)
        desiredRelaxationTimes(index) = defaultRelaxationTime;
        if verbose
            sprintf('Metabolite name %s not found. Setting relaxation time to default: %.2d', desiredMetaboliteNames{index}, defaultRelaxationTime)
        end
    else
        desiredRelaxationTimes(index) = knownRelaxationTimes(indexMetabolite);
    end
end
end

function [desiredRelaxationTimes] = replaceRelaxationTimes(knownRelaxationTimes, knownMetaboliteNames,  defaultRelaxationTime, metabolitesToReplaceNames, verbose, mapMet_Names, desiredMetaboliteNames, desiredRelaxationTimes, desiredMetabolitesReplace)
        [RelaxationTime_replace, ~] = matchRelaxationTimes(knownRelaxationTimes, knownMetaboliteNames,  defaultRelaxationTime, metabolitesToReplaceNames, verbose, mapMet_Names);
        meanRelaxationTime_replace = mean(RelaxationTime_replace);
        for index = 1:length(desiredMetabolitesReplace)
            indexMet = find(strcmp(desiredMetabolitesReplace{index},desiredMetaboliteNames));
            if ~isempty(indexMet)
                desiredRelaxationTimes(indexMet) = meanRelaxationTime_replace;
            end
        end
end