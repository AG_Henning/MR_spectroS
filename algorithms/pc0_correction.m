function [pc0,pc1] = pc0_correction(spec, maxPhasePc0Deg, maxPhasePc1, pc1RefPpm, bw,zp2, singlets,detect, verbose)
% JPCS Automatic detection of phase for JPRESS (area+maximum)
%   [pc0,pc1] = jpcs(spec,maxPhasePc0Deg,maxPhasePc1, pc1RefPpm, bw,zp2,singlets,detect, verbose)
%
%            pc0: zeroth order phase
%            pc1: linear phase
%           spec: 2D JPRESS
% maxPhasePc0Deg: maximal possible 0th order phase 
%    maxPhasePc1: maximal possible 1st order phase 
%      pc1RefPpm: reference pivot point for the 1st order phase correction
%             bw: bandwidths (divided by fo)[ppm]
%            zp2: zero point in f2          [ppm]
%       singlets: position of singlets      [ppm]
%         detect: 1=phantom
%                 2=in vivo
%        verbose: specifies if function should show output
%
% See also JPRESS, JZPS.

if nargin<1, help(mfilename); return; end
narginchk(8,9);
if ~exist('verbose','var')
    verbose = false;
end

switch detect
    case 1,
        wi_roi = 0.4;			% width [ppm] around peak
    case 2,
        wi_roi = 0.2;			% width [ppm] around peak
end

sing = singlets{detect};
si = size(spec);

ax1 = [-si(1)/2:(si(1)-1)/2]/si(1);
ax2 = [-si(2)/2:(si(2)-1)/2]/si(2);
numberOfPhaseSteps = 50;
maxPhasePc0Rad = maxPhasePc0Deg / 180 * pi;
phaseStep = maxPhasePc0Rad/numberOfPhaseSteps * 2;
if phaseStep < 0.005 %smaller resolution does not make sense from experience
    phaseStep = 0.005;
end
%zero order phases to check for
phis = [-maxPhasePc0Rad:phaseStep:maxPhasePc0Rad];
l_phis = length(phis);		% length of index (phis) list
ls = length(sing);

if verbose
    plt = true;
    fprintf('Automatic 0 order phase correction\n');
    fprintf('ppm\t\tmax\t\tint\t\tint neg\t\tmin neg\t\tmean\n');
else
    plt = false;
end

for l=1:ls %run for all singlets
    %select centrum of spectral indices in dim 1
    ind1 = find(((ax1*bw(1))>-wi_roi/2) & ((ax1*bw(1))<wi_roi/2));
    i1h = (ind1(end)-ind1(1))/2; %middle index in dim1
    
    ind2 = find(((ax2*bw(2)+zp2)>sing(l)-wi_roi/2) & ...
        ((ax2*bw(2)+zp2)<sing(l)+wi_roi/2));
    spec_roi = spec(ind1,ind2);
    maxRealAmpl = [];
    integralRealAmpl = [];
    
    abs_h_ampl = 0.0;
    %set min_sum_im to a ridiculously high value
    min_sum_im = max(spec(:)) * si(2);
    %difference between max and min peak in the dispersion spectra: search
    %for minimum, start with a ridiculously high value
    min_diff_ampl=max(spec(:)) * si(2);
    
    index = 1;
    for phi=phis % phi = pc0/180*pi
        % iterating is unelegant methods for some:
        %  could be calculated through combintation of real and
        %  imaginary part
        spec_roi_phased = spec_roi*exp(-1i*phi);
        spec_roi_phased_real = real(spec_roi_phased);
        spec_roi_phased_imag = imag(spec_roi_phased);
        % 1: determine maximum
        maxRealAmpl = [maxRealAmpl max(spec_roi_phased_real(:))];
        % 2: determine integral of whole peak
        integralRealAmpl = [integralRealAmpl sum(spec_roi_phased_real(:))];
        
        % Alex's methods
        abs_h_ampl = max(abs(spec_roi_phased_real));
        h_ampl_imag = max(spec_roi_phased_imag);
        l_ampl_imag = min(spec_roi_phased_imag);
        
        %find phase representing the minimal imaginary integral
        abs_sum_im = abs(sum(spec_roi_phased_imag));
        if(abs_sum_im < min_sum_im) && (maxRealAmpl(index) == abs_h_ampl)
            min_sum_im = abs_sum_im;
            phase_sum_im = phi;
        end
        
        %find phase representing the minimal imaginary integral
        diff_ampl = abs(abs(h_ampl_imag)-abs(l_ampl_imag));
        if(diff_ampl < min_diff_ampl) && (maxRealAmpl(index) == abs_h_ampl)
            min_diff_ampl = diff_ampl;
            phase_diff_ampl = phi;
        end
        
        index = index + 1;
    end
    % Alex's methods
    [max_sum_re, index_max_sum_re] = max(integralRealAmpl);
    phase_sum_re = phis(index_max_sum_re);
    
    h_ampl = maxRealAmpl;
    cg_phi = phis;
    [min_h_ampl, min_h_ampl_idx] = min(h_ampl);
    if (min_h_ampl_idx < l_phis/2)
        cg_phi(1:min_h_ampl_idx) = cg_phi(1:min_h_ampl_idx) + 360;
    else
        cg_phi(min_h_ampl_idx:end) = cg_phi(min_h_ampl_idx:end) -360;
    end
    amp_phi = cg_phi.*h_ampl;
    sum_amp_phi = sum(amp_phi);
    sum_amp=sum(h_ampl);
    phase_max_ampl = sum_amp_phi/sum_amp;
 
    
    if verbose
        fprintf('Niklaus: %g\t%0.3g\t\t%0.3g\t\t%0.3g\n',...
            sing(l),phase_max_ampl*180/pi,phase_sum_re*180/pi,...
            phase_sum_im*180/pi, phase_diff_ampl*180/pi);
    end
    
    if (abs(phase_max_ampl)+abs(phase_sum_re)+abs(phase_sum_im)+abs(phase_diff_ampl)> abs(phase_max_ampl+phase_sum_re+phase_sum_im+phase_diff_ampl))
        if (phase_max_ampl<-180+15)
            phase_max_ampl=phase_max_ampl+360;
        end
        if  (phase_sum_re<-180+15)
            phase_sum_re=phase_sum_re+360;
        end
        if  (phase_sum_im<-180+15)
            phase_sum_im=phase_sum_im+360;
        end
        if  (phase_diff_ampl<-180+15)
            phase_diff_ampl=phase_diff_ampl+360;
        end
    end
    wanted_phase = -(phase_max_ampl+phase_sum_re+phase_sum_im+phase_diff_ampl)/4 %Minus ist correct!
    
    % end Alex's methods
    
    % [yy_max,ii_max] = max(ma);
    
    % centre of gravity of maxima (much more reliable)
    [yy_mami,ii_mami] = min(maxRealAmpl);
    maxRealAmpl = maxRealAmpl-yy_mami;
    if ii_mami<l_phis/2
        cg_phis = phis + ([1:l_phis]<ii_mami)*2*pi;
    else
        cg_phis = phis - ([1:l_phis]>=ii_mami)*2*pi;
    end
    
    pc_max = sum(maxRealAmpl.*cg_phis)/sum(maxRealAmpl);
    
    [yy,ii_int] = max(integralRealAmpl);
    
    % determine the closer aread to search
    % ia: index list around
    
    iawi = l_phis/10;		% widths of indices around
    if ii_int<=iawi,
        ia = round([1:(ii_int+iawi) (l_phis-iawi+ii_int):l_phis]);
    else
        if ii_int>l_phis-iawi
            ia = round([1:(ii_int-l_phis+iawi) (ii_int-iawi:l_phis)]);
        else
            ia = round([ii_int-iawi:ii_int+iawi]);
        end
    end
    
    % different phases in [rad]
    pc_all = [pc_max phis(ii_int)];
    pcs(l) = mean(unwrap(sort(pc_all)));
    
    if verbose
        fprintf('%g\t%0.3g\t\t%0.3g\t\t%0.3g\n',...
            sing(l),pc_all(1)*180/pi,pc_all(2)*180/pi,pcs(l)*180/pi);
    end
    
    % plotting
    if plt
        figure(30);
        cmax = log(max(maxRealAmpl)+1)/log(2);
        siroi = size(spec_roi);
        
        subplot(length(sing)+1,2,2*l-1);
        plot2d_log(real(spec_roi*exp(-1i*pcs(l))));
        hold on;
        plot([1 siroi(2)],[siroi(1)/2 siroi(1)/2],'g:',...
            [siroi(2)/2 siroi(2)/2],[1 siroi(1)],'g:');
        hold off;
        caxis([-cmax cmax]); 
        colorbar; 
        colormap(cmap_dark);
        title([num2str(sing(l)) ' ppm']);
        
        subplot(length(sing)+1,2,2*l);
        plot(phis*180/pi,maxRealAmpl/max(maxRealAmpl),'b',...
            phis*180/pi,integralRealAmpl/max(integralRealAmpl),'r');
        xlabel('b: max; r: int');
        axis tight;
    end
end
pcs_tmp = pcs;
pcs = unwrap(pcs)*180/pi;

if verbose
    if any(pcs_tmp~=unwrap(pcs_tmp)),
        fprintf('Unwrapping\n');
        display(pcs);
    end
end

% switch detect
switch 2
    case 1,		% phantom
        p = polyfit((zp2-sing)/bw(2),pcs,1);
        pc0 = p(2);
        pc1 = -p(1);
    case 2,		% in vivo
        pc0 = mean(pcs);
        pc1 = 0;
    otherwise, error('nothing detected');
end
if pc0>180,
    pc0 = pc0-360;
end
if pc0<-180,
    pc0 = pc0+360;
end

[pc1] = phaseCorrect1Order(spec*exp(-1i*pc0/180*pi), maxPhasePc1, pc1RefPpm, bw,zp2, singlets,detect, verbose);

