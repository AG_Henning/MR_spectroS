function basisSetApplyLineWidth()

plotBasis = false;

%ISIS data
trunc_ms = 0.36;
%MRSI data
% trunc_ms = 1.34;

metaboliteFileNames = {'phosphocreatine_31P'   'ATP_31P_short_gamma' 'ATP_31P_short_alpha'  'ATP_31P_short_beta' 'GPC_31P_short' 'GPE_31P_short' 'inorganic_phosphate_intra_31P'...
    'inorganic_phosphate_extra_31P'   'phosphocholine-31P_short' 'phosphoethanolamine_31P_short' 'NADH_31P' 'NAD+_31P'  'UDPG_31P'};
metaboliteLabels =     {'PCr' 'ATP-gamma' 'ATP-alpha' 'ATP-beta' 'GPC' 'GPE' 'Pi intra' 'Pi extra' 'PC' 'PE' 'NADH' 'NAD+' 'UDPG'};
metaboliteNames =      {'Cr'  'ATP-g'     'ATP-a'     'ATP-b'    'GPC' 'GPE'   'Pi_in'    'Pi_ext'  'PC' 'PE' 'NADH' 'NAD'  'UDPG'};
metaboliteLineWidths = [  3     80           15        15         12     11      40          10      11   20     4     12     10  ];% Hz
metaboliteLineWidths = [  9.5   51           38        45         18     18.5    26          35      18   19     10    10     10  ];% Hz new Linewidths measured by Johanna (email 8.07.2019)
metaboliteLineWidths = [  9.5   51           38        45         18     18.5    26          35      18   19     10    10     10  ];% Hz new Linewidths measured by Johanna (email 8.07.2019)
metaboliteLineWidths = [  10    60           38        45         18     18.5    30          26      25   30     10    10     10  ];% Hz new Linewidths measured by Johanna (email 8.07.2019)
metaboliteLineWidths = [  10    70           45        70         18     18.5    40          26      30   40     10    10     10  ];% Hz new Linewidths measured by Johanna (email 8.07.2019)
metaboliteLineWidths = [  9.5   120          60        160        18     18.5    80          80      90   40     30    30     30  ];% Hz new Linewidths measured by Johanna (email 8.07.2019)

minLineWidth =  min(metaboliteLineWidths);
adjustedLineWidth = metaboliteLineWidths - minLineWidth; %Hz

lineWidthType = 'lorentz'; %'lorentz' 'gauss'

pathName = '31P data path';
sampleCriteria = {};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
filePathImport = [localFilePathBase 'Basis_sets\ISIS\ISIS_noRef\'];

for indexMetabolite = 1 : length(metaboliteFileNames)
    filtered_metabolite_name = [metaboliteFileNames{indexMetabolite} '_LW'];
    %
    %read in the metabolites
    [oldData, bandwidth, scanFrequency, metaboliteName, singletPresentMix] = ...
        ImportLCModelBasis(filePathImport, metaboliteFileNames{indexMetabolite}, plotBasis, '31P');    

    dwellTime = 1/bandwidth;
    addSinglet = true;
    %adjust linewidth
    
    t = (0:length(oldData)-1)*dwellTime;

    switch lineWidthType
        case 'gauss'
            lineWidthFilter = exp ( - t'.^2*(adjustedLineWidth(indexMetabolite))^2 );
        case 'lorentz' %exponential
            lineWidthFilter = exp(-t'*adjustedLineWidth(indexMetabolite));
        otherwise
                error('Line width type not defined');
    end
    lineWidthData = oldData .* lineWidthFilter;

        
%     if strcmp(metaboliteFileNames{indexMetabolite}, 'ATP_31P_short') == 1
%         spectrum = fftshift(fft(lineWidthData));
%         alphaIndeces = 3000:3200;
%         betaIndeces = 1850:2050;
%         gammaIndeces = 3660:3860;
%         neutralArea = 1500:1700;
%         switch metaboliteNames{indexMetabolite}
%             case 'ATP-g'
%                 filtered_metabolite_name = [metaboliteFileNames{indexMetabolite} '_gamma_LW'];
%                 spectrum(alphaIndeces) = spectrum(neutralArea);
%                 spectrum(betaIndeces) = spectrum(neutralArea);
%             case 'ATP-a'
%                 filtered_metabolite_name = [metaboliteFileNames{indexMetabolite} '_alpha_LW'];
%                 spectrum(gammaIndeces) = spectrum(neutralArea);
%                 spectrum(betaIndeces) = spectrum(neutralArea);
%         end
%         lineWidthData = ifft(ifftshift(spectrum));
%         lineWidthData(7000:end) = lineWidthData(7000:end).*0;
%     end

    dwellTimeMs = dwellTime * 1e3;
    truncPoint = round(trunc_ms/dwellTimeMs);
    realTruncTime_ms = truncPoint*dwellTimeMs;
    trunc_ms_residue = 0; %trunc_ms - realTruncTime_ms;
    truncData = [lineWidthData(truncPoint:end); complex(zeros(truncPoint-1,1))];
    
    nTimepoints = length(oldData);
    frequencyCentre = 0;
    frequencyRange  = linspace(-bandwidth/2,bandwidth/2,4096);

    phase1 = trunc_ms_residue*(2e-3) * 180;
    phase1_rad_Hz         = phase1 * pi/180  ; %in rads/Hz
    phase1Vector = exp(1i*(frequencyRange-frequencyCentre)*phase1_rad_Hz)';
    ffts    = fftshift(fft(truncData,4096));
    ffts    = ffts.*phase1Vector;
    phasedData = ifft(fftshift(ffts));
    filePathExport = [localFilePathBase 'Basis_sets\ISIS\Trunc' num2str(trunc_ms) '\'];
    mkdir(filePathExport)
    
    ExportLCModelBasis(phasedData', dwellTimeMs, scanFrequency, filePathExport, ...
        filtered_metabolite_name, metaboliteNames{indexMetabolite}, addSinglet, ...
        false, -15, 0.05);
end
end
