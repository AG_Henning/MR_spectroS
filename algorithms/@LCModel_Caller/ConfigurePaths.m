function this = ConfigurePaths( this, controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal)
% Configuring the class with the different folders
% - controlFilesPathRemote: specifies the folder in the remote server 
% containing the *.control LCModel files
% - outputFilesPathRemote:  specifies the folder in the remote server where 
% the data will be written by the aforementioned .control file 
% 
% The paths on the remote server are built up on the 'homePath' parameter
% of the class, which is the bash folder for the given user.
% 
% - outputFilesPathLocal: specifies the folder where the data should be
% copied on the local computer
%
% % Sample declarations would be
% controlFilesPathRemote = '/Desktop/MM/LCModelConfig/';
% outputFilesPathRemote = '/Desktop/MM/Output/';
% outputFilesPathLocal = 'D:/Software/Spectro Data/MM/Output/';
%
% Author: Tamas Borbath
% version: 20 July 2018

if(nargin  ~=  4)
    error('Error: ConfigurePaths requires 3 input arguments: controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal .');
end

this.controlFilesPathRemote = controlFilesPathRemote;
this.CreateRemoteFolder(this.controlFilesPathRemote);

this.outputFilesPathRemote = outputFilesPathRemote;
this.CreateRemoteFolder(this.outputFilesPathRemote);

this.outputFilesPathLocal = outputFilesPathLocal;
mkdir(this.outputFilesPathLocal);
end

