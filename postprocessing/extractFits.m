function [nOfPoints, ppmVector, phasedData, fitData, baselineData, residualData, ...
    metaboliteNames, metaboliteData, tableConcentrations, ppmStart, ppmEnd, ...
    scanFrequency, frequencyShift, ppmGap, phase0, phase1] = ...
    extractFits(PathName, FileName)

c          = textread(strcat(PathName,FileName),'%s');
nOfPoints  = find(strcmp(c ,'points'),1);
nOfPoints  = str2num (c{nOfPoints-1});

%ppm axis
indexOfPpm  = find(strcmp(c,'ppm-axis'))+3;
endOfMet    = nOfPoints+indexOfPpm-1;
ppmVector   = str2double( c(indexOfPpm:endOfMet,1));

%phaseData
indexOfMet  = find(strcmp(c,'phased'))+4;
endOfMet    = nOfPoints+indexOfMet-1;
phasedData  = str2double( c(indexOfMet:endOfMet,1));

%fit data
indexOfMet  = find(strcmp(c,'fit'))+5;
endOfMet    = nOfPoints+indexOfMet-1;
fitData     = str2double( c(indexOfMet:endOfMet,1));

%background
indexOfMet      = find(strcmp(c,'background'))+3;
endOfMet        = nOfPoints+indexOfMet-1;
baselineData    = str2double( c(indexOfMet:endOfMet,1));

%residual
residualData    = phasedData - fitData;

%metabolite names
[c1 c2 c3 c4] = textread(strcat(PathName,FileName),'%s %s %s %s');

start  = find(strcmp(c1,'Conc.'));
start  = start + 1;
finish = find(strcmpi(c1,'FWHM')) - 1;
tableConcentrations{1,1} = 'ID';


while isempty(strfind(c2{finish},'%')) && (finish > start)
    finish = finish - 1;
end
numberOfMetabolites = finish-start+1;
metaboliteNames = cell(numberOfMetabolites,1);
tableConcentrations{1,1} = 'Metabolite';
tableConcentrations{2,1} = 'Conc.';
tableConcentrations{3,1} = 'CRLB';
tableConcentrations{4,1} = '/Cr';
for j=2:numberOfMetabolites+1
    indexC = j+start-2;
    if(strcmp(c4{indexC},'')==1)
        c3_split = strsplit(c3{indexC},'+');
        if length(c3_split) == 1
            c3_split = strsplit(c3{indexC},'-');
        end
        tableConcentrations{1,j} = c3_split{2};
        tableConcentrations{2,j} = c1{indexC};
        tableConcentrations{3,j} = c2{indexC};
        tableConcentrations{4,j} = c3_split{1};
        metaboliteNames{j-1} = c3_split{2};
    else
        tableConcentrations{1,j} = c4{indexC};
        tableConcentrations{2,j} = c1{indexC};
        tableConcentrations{3,j} = c2{indexC};
        tableConcentrations{4,j} = c3{indexC};
        metaboliteNames{j-1} = c4{indexC};
    end
end

%metabolite spectra
metaboliteData = cell(numberOfMetabolites,1);
for indexMetabolite = 1:numberOfMetabolites
    %evaluate each metabolite
    individualMet = strsplit(metaboliteNames{indexMetabolite},'+');
    metaboliteSpectrum = zeros(nOfPoints,1);
    for indexIndividualMet=1:length(individualMet)
        %sum up the individual components
        indexOfMet   = find(strcmp(c,individualMet{indexIndividualMet}),1,'last')+3;
        endOfMet     = nOfPoints+indexOfMet-1;
        if (indexOfMet > indexOfPpm) %make sure that we indeed have a metabolite quantification spectrum
            metaboliteSpectrum = metaboliteSpectrum + str2double(c(indexOfMet:endOfMet,1)) - baselineData;
        end
    end
    metaboliteData{indexMetabolite}.metaboliteName = metaboliteNames{indexMetabolite};
    metaboliteData{indexMetabolite}.metaboliteSpectrum = metaboliteSpectrum;
end

%other information
indexOfValue = find(strcmpi(c,'ppmst='))+1;
if isempty(indexOfValue)
    indexOfValue = find(strcmpi(c,'ppmst'))+2;
end
ppmStart     = str2double( c(indexOfValue,1));
indexOfValue = find(strcmpi(c,'ppmend='))+1;
if isempty(indexOfValue)
    indexOfValue = find(strcmpi(c,'ppmend'))+2;
end
ppmEnd      = str2double( c(indexOfValue,1));
indexOfValue = find(strcmpi(c,'hzpppm='))+1;
if isempty(indexOfValue)
    indexOfValue = find(strcmp(c,'hzpppm'))+2;
end
scanFrequency = str2double( c(indexOfValue,1));
indexOfValue = find(strcmpi(c,'shift'))+2; % 'Data shift = x.xx ppm'
frequencyShift = str2double( c(indexOfValue,1));
indexOfValue = find(strcmpi(c,'Ph:'))+1; % 'Ph:  xx deg      xx.x deg/ppm'
phase0 = str2double( c(indexOfValue,1));
phase1 = str2double( c(indexOfValue+2,1));


tempX = strfind(c,'ppmgap(2,1)=');
indexOfValue = find(~cellfun(@isempty,tempX));

if isempty(indexOfValue)
    ppmGap = [];
else
    ppmGap = str2double( c{indexOfValue,1}(13:end));
end
end