function [a] = reconstruct_1H_MMBaseline(fid_id, isMC, isInVivo, nBlocks, weights, saveResults)

nsamplesBeforeEcho = 4;

if ~exist('isInVivo', 'var')
    isInVivo = true;
end

if ~exist('saveResults', 'var')
    saveResults = true;
end

if isMC
    a = MR_spectroS(fid_id,1);
else
    a = MR_spectroS(fid_id);
end

%%
if nsamplesBeforeEcho ~= 0
    startpoint = nsamplesBeforeEcho + 1;
    endpoint   = size(a.Data{1},a.kx_dim)-(32-nsamplesBeforeEcho);
    data = squeeze(a.Data{1});
    data = data(startpoint:endpoint,:,:);
    dataNew(:,1,1,:,1,1,1,1,1,1,1,:) = data;
    a.Data{1} = dataNew;
    clear data dataNew
end

%%
if isInVivo
    %% truncate before anything
    trunc_ms = 250;
    truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
    a = a.Truncate(truncPoint);

    a = a.FrequencyAlign;
end
if isMC
    if isInVivo
        a = a.Rescale;
    end
    a = a.ReconData;
end

%average data block-wise
if ~exist('nBlocks', 'var')
    a.Parameter.AverageSettings.nBlocks = 1;
else
    a.Parameter.AverageSettings.nBlocks = nBlocks;
end

a = a.AverageData;

%eddy current correction
if isMC
    a = a.EddyCurrentCorrection;
else
    [FileName, PathName] = uigetfile('*.dat',['Select water reference for :' a.Parameter.Filename]);
    path = strcat(PathName,FileName);
    tmpData      = mapVBVD(path);
    tmpWaterReferenceData = tmpData.image(''); %Read the data
    waterReferenceData = double(permute(tmpWaterReferenceData,[1 3 4 2 5 7 8 11 12 9 10 6]));
    a = a.EddyCurrentCorrection(waterReferenceData);
end

%combine coils using SVD
if exist('weights','var')
    if ~isempty(weights)
        a = a.CombineCoils(weights);
    else
        a = a.CombineCoils;
    end
else
    a = a.CombineCoils;
end

%% setup the settings for the FrequencyAlignFreqDomain
a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;

%indices of Interest: given in ppm
% water peak
a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = 2.008;

%set zeroFillingParameter to get smooth approximations
a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;

%search area (+-) in ppm
if isInVivo
    a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.1;
else
    a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.2;
end
%spline filtering is not needed if the spectra are not too noisy.
%flag to do spline filtering
a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
%spline filtering coefficient
a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;
% do the actual Frequency Alignment
a = a.FrequencyAlignFreqDomain;

%% water suppression
if isInVivo
    a.Parameter.HsvdSettings.bound = [-100 100];
else
    a.Parameter.HsvdSettings.bound = [-100 100];
end
a.Parameter.HsvdSettings.n = size(a.Data{1},1);
a.Parameter.HsvdSettings.p = 25;
[~, a] = a.Hsvd;
[~, a] = a.Hsvd;
[~, a] = a.Hsvd;
% 
if isMC
    %% Reverse MC
    a = a.ReverseMC;
%     [~, a] = a.Hsvd;
end

%truncate data
if isInVivo
%     trunc_ms = 150;
    trunc_ms = 100;     %MM
    truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
    a = a.Truncate(truncPoint);
end

%%
if saveResults
    data = (a.Data{1});
    %saving the file
    a.Data{1} = data(:,:,:,:,:,:,:,:,:,:,:,1);
    filename = sprintf('%d',a.Parameter.Headers.PatientName);
    save([filename '_MMBaseline.mat'], 'a')
    %export to LCModel
    addSinglet0ppm = 'Yes';
    a.ExportLcmRaw('',strcat(filename,'_MMBaseline'), addSinglet0ppm);
end

%%
figure; hold on;
data = squeeze(a.Data{1});
ppmVector = a.getPpmVector;
plot(ppmVector,real(fftshift(fft(data(:,1,1)))))
set(gca, 'XDir','reverse')
xlim([0 4.5]); 

end