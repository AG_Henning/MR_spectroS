function howIsFittingDone()

%% Concentrations and T2 values taken from the Murali Manohar MRM 2019 paper
% Concentration values were down or upscaled a bit to match better the literature comparison, if they were needed (Table 3)
% Values were rounded, standard deviations increased for values where possible mismatch with literature could be assumed. Both for concentrations and T2s
% Default T2 value taken for the metabolites, where these were not determined in the paper: GABA, Lac, Scyllo, Tau were taken as 75+-35ms
metabolites     = {'Asp', 'tCr(CH2)', 'tCr(CH3)', 'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA(CH3)', 'NAA(CH2)', 'NAAG', 'tCho+',  'Scy',    'Tau', 'MMB'};
% filenames       = {'Asp', 'Cr_CH2',   'Cr',       'GABA', 'Gln', 'Glu', 'Glyc', 'GSH', 'Lac', 'mI', 'NAA_ac',   'NAA_as',   'NAAG', 'tCho_P', 'Scyllo', 'Tau', 'Leu'};
concentrations  = [  3,     8.5,        8.5,       1.8,     3,     9,      1.2,    1.4,   0.7,   5.5,   12,          12,        1.4,    1.5,      0.4,     1.5,   1.5e7]; %mmol/kg
conc_std        = [0.8,     0.7,        0.7,       0.5,   0.8,     1,     0.3,   0.2,   0.2,   0.5,    1,           1,        0.3,    0.4,      0.1,     0.3,   0.2e7]; %mmol/kg
T2s             = [ 54,     82,         100,        75,    50,    85,     60,     75,    75,   90,    110,         100,        45,     90,       75,      75,   25]; %ms
T2_std          = [ 12,     10,          16,        35,    20,    25,     10,     35,    35,   20,     30,          25,        20,     25,       35,      35,   10]; %ms

TE = 24 * 1e-3;
numMetabolites = length(metabolites);

saveData = false;

%% path to the export of the spectra
exportFolder = {'11 Simulations'};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolder);
dataExportPathBase = strcat(pathBaseExportFiles, exportFolder{1}, '\');

%%
metaboliteFileNames = metabolites;
% replace the weirdos 
metaboliteFileNames = strrep(metaboliteFileNames, 'tCr(CH2)', 'Cr_singlet_tot_3.925');
metaboliteFileNames = strrep(metaboliteFileNames, 'tCr(CH3)', 'Cr_singlet_tot_3.028');
metaboliteFileNames = strrep(metaboliteFileNames, 'NAA(CH3)', 'NAA_2');
metaboliteFileNames = strrep(metaboliteFileNames, 'NAA(CH2)', 'NAA_1');
metaboliteFileNames = strrep(metaboliteFileNames, 'tCho+',    'tCho_PE');
metaboliteFileNames = strrep(metaboliteFileNames, 'Scy',      'Scyllo');
metaboliteFileNames = strrep(metaboliteFileNames, 'MMB',      'MMB_without_metabolite_TE24');

%% paths to the basis set files
pathName = 'DF data path';
sampleCriteria = {'Basis_sets'};
[filePath] = pathToDataFolder(pathName, sampleCriteria);


filePathBasis = strcat(filePath,  'Basis_sets/final_T2_met_MM_paper/');
pathMM = 'sLASER_MM/';
pathMetabolites = 'sLASER_new_UF_7ppm/TE24/';

fids = cell(1, numMetabolites);
plotBasis = false;

for indexMetabolite = 1:numMetabolites
    fileName = metaboliteFileNames{indexMetabolite};
    if strcmp(metabolites{indexMetabolite},'MMB')
        filePathMetabolite = strcat(filePathBasis, pathMM);
    else
        filePathMetabolite = strcat(filePathBasis, pathMetabolites);
    end
    [fid, bandwidth, scanFrequency, metaboliteName, singletPresent] = ...
        ImportLCModelBasis(filePathMetabolite, fileName, plotBasis, '1H');
    if strcmp(metabolites{indexMetabolite},'NAA_ac')
%         fid = fid * 3;
    end
    if strcmp(metabolites{indexMetabolite},'MMB')
        fids{indexMetabolite}(1,:)= fid(1:4096); % stupid change to make all metabolites 4096 long!
    else
        fids{indexMetabolite}(1,:)= fid;
    end
end

water_ppm = 4.66; % ppm
nTimepoints = length(fids{1});
dwellTime = 1/bandwidth;
timePointsSampling =(0:nTimepoints - 1) * dwellTime;
scalingFactorRefPeak = 0.05;

refFreqPpm = -0; % the reference Frequency for the excitation pulse, given in ppm compared to the water (minus is upfield, + downfield)

phasePivot_ppm = water_ppm + refFreqPpm;%in ppm
ppmVector = ppmVectorFunction(scanFrequency*1e6, bandwidth, nTimepoints, '1H');
phase_1_f2_mx = ppmVector-phasePivot_ppm;

ppmMask = (ppmVector>0.6) & (ppmVector<4.1);

%I didn't like the DSS amplitude, some stupid code for adjusting it.
        FWHM      =  1.75; %Hz
        T2        =  1/(pi*FWHM)*1e+3; %ms
        f         =  -4.7076 *scanFrequency; %Hz
        singlet   =  scalingFactorRefPeak*0.042*exp(1i*2*pi*f*timePointsSampling).*exp(-timePointsSampling*1e3/T2);
        FWHM      =  3; %Hz
        T2        =  1/(pi*FWHM)*1e+3; %ms
        f         =  -4.7077 *scanFrequency; %Hz
        singlet2  =  scalingFactorRefPeak*0.0278*exp(1i*2*pi*f*timePointsSampling).*exp(-timePointsSampling*1e3/T2);


%% set default values for the parameters
default_noiseLevel = 3;
noiseVector = wgn(1,nTimepoints, default_noiseLevel);
trunc_ms = 200;
truncPoint = floor( trunc_ms/(dwellTime*1e+3));

%% create the actual spectra
current_pc0 = 0;
current_pc1 = 0;
current_df2 = zeros(1,numMetabolites);
current_gm = 12;
current_em_std_factor = zeros(1,numMetabolites);
conc_std_factor = zeros(1,numMetabolites); 

indexNAA_ac = find(strcmp('NAA(CH3)',metabolites));
indexNAA_as = find(strcmp('NAA(CH2)',metabolites));
fid_NAA_ac = fids{indexNAA_ac};
fid_NAA_as = fids{indexNAA_as};
fids_NAA = {fid_NAA_ac, fid_NAA_as};
concentrations_NAA = [concentrations(indexNAA_ac), concentrations(indexNAA_as)];
conc_std_NAA = [0 0];
current_df2_NAA = [0 0];
conc_std_factor_NAA = [0 0];
T2s_NAA = [T2s(indexNAA_ac), T2s(indexNAA_as)];
T2_std_NAA = [0 0];
current_em_std_factor_NAA = [0 0];
metabolites_NAA = {'NAA(CH3)', 'NAA(CH2)'};
metaboliteFileNames_NAA = {metaboliteFileNames{indexNAA_ac}, metaboliteFileNames{indexNAA_as}};
%%
[simulatedNAA, defaultSummedFidWithNoise, defaultCurrent_em, defaultCurrentConcentrationRm] = ...
    createSummedSpectrum(...
    fids_NAA, concentrations_NAA, conc_std_NAA, [1000 1000], T2_std_NAA, TE, metabolites_NAA, metaboliteFileNames_NAA, timePointsSampling, phase_1_f2_mx, ppmMask, ...
    current_pc0, current_pc1, current_df2_NAA, current_gm*0, current_em_std_factor_NAA, conc_std_factor_NAA, noiseVector, ...
    truncPoint, saveData, 'meanConc', [], dwellTime * 1e3, scanFrequency, dataExportPathBase, water_ppm, scalingFactorRefPeak);
scalingFactor = 3 * (1 / max(real(fftshift(fft(simulatedNAA)))));
concentrations_NAA = concentrations_NAA * scalingFactor;
[simulatedNAA, defaultSummedFidWithNoise, defaultCurrent_em, defaultCurrentConcentrationRm] = ...
    createSummedSpectrum(...
    fids_NAA, concentrations_NAA, conc_std_NAA, [1000 1000], T2_std_NAA, TE, metabolites_NAA, metaboliteFileNames_NAA, timePointsSampling, phase_1_f2_mx, ppmMask, ...
    current_pc0, current_pc1, current_df2_NAA, current_gm*0, current_em_std_factor_NAA, conc_std_factor_NAA, noiseVector, ...
    truncPoint, saveData, 'meanConc', [], dwellTime * 1e3, scanFrequency, dataExportPathBase, water_ppm, scalingFactorRefPeak);
figure;
hold on
plot(ppmVector, real(fftshift(fft(simulatedNAA-singlet+singlet2))));%I didn't like the DSS amplitude
% plot(ppmVector, real(fftshift(fft(simulatedNAA))));
plotSetup();
ylim([-0.11; 3.1])
title('Simulated NAA')
legend('$$NAA$$', 'Interpreter','latex','Location','NorthWest')
xlim([-0.2 8.2])

[simulatedNAA_T2, defaultSummedFidWithNoise, defaultCurrent_em, defaultCurrentConcentrationRm] = ...
    createSummedSpectrum(...
    fids_NAA, concentrations_NAA, conc_std_NAA, T2s_NAA, T2_std_NAA, TE, metabolites_NAA, metaboliteFileNames_NAA, timePointsSampling, phase_1_f2_mx, ppmMask, ...
    current_pc0, current_pc1, current_df2_NAA, current_gm*0, current_em_std_factor_NAA, conc_std_factor_NAA, noiseVector, ...
    truncPoint, saveData, 'meanConc', [], dwellTime * 1e3, scanFrequency, dataExportPathBase, water_ppm, scalingFactorRefPeak);
figure;
plot(ppmVector, real(fftshift(fft(simulatedNAA))));
hold on;
plot(ppmVector, real(fftshift(fft(simulatedNAA_T2))),'r');
plotSetup();
ylim([-0.11; 3.1])
title('Simulated NAA')
legend('$$NAA$$', [10, '$$NAA*exp(-\nu_{e,k}\pi(TE+\overrightarrow{t}))$$'],...
    'Interpreter','latex','Location','NorthWest')
figure;
plot(ppmVector, real(fftshift(fft(simulatedNAA))));
hold on;
plot(ppmVector, real(fftshift(fft(simulatedNAA_T2))),'r');
plotSetup();
ylim([-0.11; 1.2])
title('Simulated NAA')
legend('$$NAA$$', [10, '$$NAA*exp(-\nu_{e,k}\pi(TE+\overrightarrow{t}))$$'],...
    'Interpreter','latex','Location','NorthWest')

[simulatedNAA_T2_gm, defaultSummedFidWithNoise, defaultCurrent_em, defaultCurrentConcentrationRm] = ...
    createSummedSpectrum(...
    fids_NAA, concentrations_NAA, conc_std_NAA, T2s_NAA, T2_std_NAA, TE, metabolites_NAA, metaboliteFileNames_NAA, timePointsSampling, phase_1_f2_mx, ppmMask, ...
    current_pc0, current_pc1, current_df2_NAA, current_gm, current_em_std_factor_NAA, conc_std_factor_NAA, noiseVector, ...
    truncPoint, saveData, 'meanConc', [], dwellTime * 1e3, scanFrequency, dataExportPathBase, water_ppm, scalingFactorRefPeak);

figure;
plot(ppmVector, real(fftshift(fft(simulatedNAA))));
hold on;
plot(ppmVector, real(fftshift(fft(simulatedNAA_T2))),'r');
plot(ppmVector, real(fftshift(fft(simulatedNAA_T2_gm))),'k');
plotSetup();
ylim([-0.11; 1.2])

legend('$$NAA$$', [10, '$$NAA \cdot exp(-\nu_{e,k}\pi(TE+\overrightarrow{t}))$$'],...
    [10,10, '$$NAA \cdot exp(-\nu_{e,k}\pi(TE+\overrightarrow{t}))\cdot$$',10, '$$\exp\left(\frac{\left(\nu_g\pi\,\vec{t}\right)^2}{4ln\left(2\right)}\right)$$'], ...
    'Interpreter','latex','Location','NorthWest')
title('Simulated NAA')

figure;
plot(ppmVector, real(fftshift(fft(simulatedNAA))));
hold on;
plot(ppmVector, real(fftshift(fft(simulatedNAA_T2))),'r');
plot(ppmVector, real(fftshift(fft(simulatedNAA_T2_gm))),'k');
plotSetup();
ylim([-0.11; 0.48])

legend('$$NAA$$', [10, '$$NAA \cdot exp(-\nu_{e,k}\pi(TE+\overrightarrow{t}))$$'],...
    [10,10, '$$NAA \cdot exp(-\nu_{e,k}\pi(TE+\overrightarrow{t}))\cdot$$',10, '$$\exp\left(\frac{\left(\nu_g\pi\,\vec{t}\right)^2}{4ln\left(2\right)}\right)$$'], ...
    'Interpreter','latex','Location','NorthWest')
title('Simulated NAA')


% generate the default spectrum
[simulatedSpectrum, defaultSummedFidWithNoise, defaultCurrent_em, defaultCurrentConcentrationRm] = ...
    createSummedSpectrum(...
    fids(1:end-1), concentrations(1:end-1), conc_std(1:end-1), T2s(1:end-1), T2_std(1:end-1), TE, metabolites(1:end-1), metaboliteFileNames(1:end-1), timePointsSampling, phase_1_f2_mx, ppmMask, ...
    current_pc0, current_pc1, current_df2(1:end-1), current_gm, current_em_std_factor(1:end-1), conc_std_factor(1:end-1), noiseVector, ...
    truncPoint, saveData, 'meanConc', [], dwellTime * 1e3, scanFrequency, dataExportPathBase, water_ppm, scalingFactorRefPeak);

figure;
plot(ppmVector,real(fftshift(fft(simulatedSpectrum))));
plotSetup();
xlim([0.6 4.1])
ylim([-130; 7000])
set(gca,'ytick',[]);
legend('Metabolites','Interpreter','latex','Location','NorthWest')
title('Simulated Metabolite Spectra')

% generate the default spectrum
[simulatedSpectrum, defaultSummedFidWithNoise, defaultCurrent_em, defaultCurrentConcentrationRm] = ...
    createSummedSpectrum(...
    fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
    current_pc0, current_pc1, current_df2, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
    truncPoint, saveData, 'meanConc', [], dwellTime * 1e3, scanFrequency, dataExportPathBase, water_ppm, scalingFactorRefPeak);

figure;
plot(ppmVector,real(fftshift(fft(simulatedSpectrum))));
hold on
plot(ppmVector,real(fftshift(fft(fids{end}.*exp(-pi* abs(TE + timePointsSampling)).*concentrations(end)))))
plotSetup();
xlim([0.6 4.1])
ylim([-130; 7000])
set(gca,'ytick',[]);

legend('Metabolites and MMs', [10, 'MM spectrum'],'Interpreter','latex','Location','NorthWest')
title('Simulated Metabolite and MM Spectra')

% generate the realistic spectrum
current_pc0 = -15;
current_pc1 = 8;
current_df2_ = current_df2 + -20;
[simulatedSpectrumWithAll, summedFidWithNoiseAndAll, defaultCurrent_em, defaultCurrentConcentrationRm] = ...
    createSummedSpectrum(...
    fids, concentrations, conc_std, T2s, T2_std, TE, metabolites, metaboliteFileNames, timePointsSampling, phase_1_f2_mx, ppmMask, ...
    current_pc0, current_pc1, current_df2_, current_gm, current_em_std_factor, conc_std_factor, noiseVector, ...
    truncPoint, saveData, 'meanConc', [], dwellTime * 1e3, scanFrequency, dataExportPathBase, water_ppm, scalingFactorRefPeak);

figure;
plot(ppmVector,real(fftshift(fft(simulatedSpectrum))));
hold on
plot(ppmVector,real(fftshift(fft(summedFidWithNoiseAndAll))),'Color',[0 114/255 104/255]);
plotSetup();
xlim([0.6 4.1])
ylim([-130; 7000])
set(gca,'ytick',[]);

legend('Metabolites and MMs', [10, 'Actual in vivo quality', 10, '$$with \; \varphi_{0}, \varphi_{1}, \omega_{global}, \: noise$$'],'Interpreter','latex','Location','NorthWest')
title('Simulated in vivo quality spectra')
end

function plotSetup()
FontSize = 12;
LineWidth = 1.5;
xlim([1.5, 3.0])
xlabel('\delta (ppm)')
ylabel('Signal (arb. u.)')
set(gca,'xDir','reverse')
% set(gca,'ytick',[]);
set(gca,'fontsize',FontSize);
set(gca,'FontWeight','bold');
h = findobj(gca,'Type','line');
for plots = h
    set(plots,'LineWidth',LineWidth)
end
end
