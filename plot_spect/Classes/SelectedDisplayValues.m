classdef SelectedDisplayValues < hgsetget
    %SELECTEDDISPLAYVALUES Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
       selectedCoil
       selectedDyn
       selectedAverage
       selectedSlice
       selectedZeroPoint
       selectedType
       selectedMode
       selectedMix
       selectedAveraging
       selectedAveRange
     
    end
    
    methods
        function this = SelectedDisplayValues()
            
           this.selectedCoil           =  1;
           this.selectedDyn            =  1;
           this.selectedAverage        =  1;
           selectedSlice               =  1;
           this.selectedZeroPoint      =  4.7;
           this.selectedType           =  'ppm';
           this.selectedMode           =  'Real';
           this.selectedMix            =  1;
           this.selectedAveraging      =  false;
           this.selectedAveRange       = []; 
           
        end  
           
            
        
    end
    
end

