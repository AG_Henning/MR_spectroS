function ExportLCModelBasis(data, dwellTimeMs, scanFrequency, filepath, filename, metaboliteName, addSinglet, ...
    brukerFormat, referencePeakPpm, scalingFactorRefPeak, makeBasis, TE, sequenceName)

% Define the fileptath where the .RAW files will be saved
if nargin < 6
    error('Please give parameters: data, dwellTimeMs, scanFrequency, filepath, filename, metaboliteName');
end

if ~exist('brukerFormat', 'var')
    brukerFormat = false;
end

if ~exist('referencePeakPpm','var')
    referencePeakPpm = -4.7;
end

if ~exist('scalingFactorRefPeak','var')
    scalingFactorRefPeak = 0.05;
end

if ~exist('makeBasis', 'var')
    makeBasis = 'Yes';
    TE = 24;
    sequenceName = 'STEAM';
end

if exist('addSinglet','var')
    choice = addSinglet;
    if choice == true
        choice = 'Yes';
    else
        if choice == false
            choice ='No';
        end
    end
else
    choice = 'NotDefined';
end

if strcmp(choice,'NotDefined')
    %Question dialog box
    choice = questdlg('Add signlet at 0 PPM ?', ...
        'LCModel Extract', ...
        'Yes','No','No');
end

% Read the data
NSamples =  length(data);   % # timepoints
bandwidth = 1/dwellTimeMs*1e3;
flagY = false;
switch choice
    case 'Yes'      
        flagY     =  true;
        time      =  linspace(0,dwellTimeMs*NSamples,NSamples); % calculation in milliseconds!
        FWHM      =  3; %Hz
        T2        =  1/(pi*FWHM)*1e+3; %ms
        %       Mo        =  4.9*(1+T2)/T2; %in order the peak to be 4.9 in the FD-real part
        f         =  referencePeakPpm *scanFrequency; %Hz
        singlet   =  scalingFactorRefPeak*abs((data(1,1,1,1,1,1,1,1,1,1,1,1)))*exp(1i*2*pi*f*time*1e-3).*exp(-time/T2);
    case 'No'
        flagY = false;
end

filename = strcat(filepath, filename, '.RAW');
fileID=     fopen(filename,'w');

%SEQPAR Namelist
fprintf(fileID,' Sweep Width = %d Hz\n', bandwidth);
fprintf(fileID,' Vector Size = %d points\n', NSamples);
fprintf(fileID,' B0 Field = %d MHz\n', scanFrequency);
if strcmp(choice,'Yes')
    fprintf(fileID,' Ref Singlet = True\n');
else
    fprintf(fileID,' Ref Singlet = False\n');
end

fprintf(fileID,' %s\n', '$SEQPAR');
fprintf(fileID,' %s = %d.\n', 'ECHOT', TE);
fprintf(fileID,' %s= %3.4f\n', 'HZPPM', scanFrequency);
fprintf(fileID,' %s = %c%s%c\n','SEQ', char(39), sequenceName, char(39));
fprintf(fileID,' %s\n', '$END');

switch makeBasis
    case 'No'
        %NMID Namelist
        fprintf(fileID,' %s\n', '$NMID');
        fprintf(fileID,' ID=%c%s%c\n', char(39), metaboliteName, char(39));
        fprintf(fileID,' FMTDAT=%c(2E16.6)%c\n',char(39),char(39));%should be (2E16.6), was (2E15.6)
        if brukerFormat
            fprintf(fileID,' BRUKER= T\n'); % I spent 1 whole day to find that this parameter makes difference for the inverse ppm scaling in my raw data...grrrr
        end
        fprintf(fileID,' %s\n', '$END');
    case 'Yes'
        %NMID Namelist
        fprintf(fileID,' %s\n', '$NMID');
        fprintf(fileID,' ID=%c%s%c\n', char(39), metaboliteName, char(39));
        fprintf(fileID,' FMTDAT=%c(2E16.6)%c\n',char(39),char(39));%should be (2E16.6), was (2E15.6)
        fprintf(fileID,' VOLUME= 1.0\n');
        fprintf(fileID,' TRAMP= 1.0\n');
        if brukerFormat
            fprintf(fileID,' BRUKER= T\n'); % I spent 1 whole day to find that this parameter makes difference for the inverse ppm scaling in my raw data...grrrr
        end
        fprintf(fileID,' %s\n', '$END');
end

%write the data in real, imaginary pairs
for n_point=1:NSamples
    if flagY
        TimePoint= data(n_point)+ singlet(n_point);
    else
        TimePoint= data(n_point);
    end
    fprintf(fileID,'%15.6E%15.6E\n', real(TimePoint), imag(TimePoint));
end
clc
fclose(fileID);
display('Export...FINISH.');
end

