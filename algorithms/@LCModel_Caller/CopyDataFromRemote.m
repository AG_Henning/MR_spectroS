function this = CopyDataFromRemote( this, remoteDirectory, localPath, fileName)
% Copy a given file from the remote LCModel computer to the local computer
% - remoteDirectory: the path on the remote computer where the desired file is,
% relative to the 'homePath'
% - localPath:  the path on the local computer where the desired
% file should be stored
% - fileName: the file which should be moved. Indicate also the extension.
%
% % Sample declarations would be
% fileName = 'fitsettings_Summed_Averaged_TE24_Cre.control';
% localPath =  'D:/Software/Spectro Data/MM/LCModelConfig/';
% remoteDirectory = '/Desktop/MM/LCModelConfig/';
%
% Author: Tamas Borbath
% version: 20 July 2018
if(nargin  ~=  4)
    error('Error: FitSpectrum requires 3 input arguments: remoteDirectory, localPath, fileName .');
end

scptomatlab(this.LCModelUser, this.LCModelIP, this.LCModelPassword, localPath, [this.homePath remoteDirectory fileName])

end

