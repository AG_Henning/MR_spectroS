function calculate_Mean_31P_T2()
clear; clc; close all;

%%%%%%%%%
%Check T2TableOutputFileBase
%%%%%%%%%


pathName = 'T2 31P Paper Data';
% sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName);
outputPath = 'Output\';
% outputPath = 'Output_Real_Vespa_Original\';
% outputPath = 'Output_Real_Vespa_Phased\';
% outputPath = 'Output_Real_Vespa_Phased_NormAbs400\';
% outputPath = 'Output_Real_Vespa_Phased_NormReal400\';
subjects = {'9873';'1090';'3490';'9810';'1658';'2017';'6524';'3009';...
    '6260';'2823';'2455';'4085'};
prefixes = {'2019-09-30_';'2019-10-10_';'2019-10-14_';'2019-10-14_';...
    '2019-10-22_';'2019-11-07_';'2019-11-07_';'2019-11-20_';...
    '2019-11-20_';'2019-12-05_';'2020-07-22_';'2020-07-23_';};
suffixFolder = '_SurfCoil';

threshold_Rsquare = 0.5;

numberOfSubjects = length(subjects);
paths = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    paths{indexSubject} = [localFilePathBase outputPath prefixes{indexSubject} subjects{indexSubject} suffixFolder '\'];
end

%%%%%%check, 'noTrunc_Ideal'
TruncToFit = 'noTrunc';        %'noTrunc_Real'; 'noTrunc_Ideal';   %'Trunc90ms';'noTrunc_noAGCoupl'   'noTrunc_noBCoupl'  'noTrunc_noBCoupl_noAGCoupl

defaultSubject ='ZZZZ';
defaultTrunc = 'Trunc90ms';
% T2TableOutputFileBase = 'T2_Results_ZZZZ_STEAM_Ideal_JD_LW_Trunc90ms.mat';
% T2TableOutputFileBase = 'T2_Results_ZZZZ_STEAM_Ideal_LW_Trunc90ms.mat';
T2TableOutputFileBase = 'T2_Results_ZZZZ_STEAM_Real__LW_Trunc90ms.mat';

TestFileToLoad = [paths{1} strrep(strrep(T2TableOutputFileBase,defaultSubject,subjects{1}),defaultTrunc,TruncToFit)];
load(TestFileToLoad)    %named tableT2toSave
metaboliteNames = tableT2toSave.Properties.VariableNames;
rowNames = tableT2toSave.Properties.RowNames;
rowNamesT2_CRLBsmaller900 = strcat(subjects,'_',rowNames{1}); rowNamesR2_CRLBsmaller900 = strcat(subjects,'_',rowNames{2});
rowNames_CRLBsmaller900 = [rowNamesT2_CRLBsmaller900 rowNamesR2_CRLBsmaller900]'; rowNames_CRLBsmaller900 = rowNames_CRLBsmaller900(:);
rowNames_CRLBsmaller900 = [rowNames_CRLBsmaller900; {'T2_Mean'}; {'T2_Std'}; {'R2_Mean'}; {'R2_Std'}];
rowNamesT2_ManualTE = strcat(subjects,'_',rowNames{3}); rowNamesR2_ManualTE = strcat(subjects,'_',rowNames{4});
rowNames_ManualTE = [rowNamesT2_ManualTE rowNamesR2_ManualTE]'; rowNames_ManualTE = rowNames_ManualTE(:);
rowNames_ManualTE = [rowNames_ManualTE; {'T2_Mean'}; {'T2_Std'}; {'R2_Mean'}; {'R2_Std'}];

clearvars TestFileToLoad tableT2toSave

T2_R2_CRLBsmaller900 = zeros(2*numberOfSubjects+4,length(metaboliteNames)); 
T2_R2_ManualTE = zeros(2*numberOfSubjects,length(metaboliteNames));
for indexSubject = 1:numberOfSubjects
    FileToLoad = [paths{indexSubject} strrep(strrep(T2TableOutputFileBase,defaultSubject,subjects{indexSubject}),defaultTrunc,TruncToFit)];
    load(FileToLoad)    %named tableT2toSave
    T2_R2_CRLBsmaller900((indexSubject*2-1):indexSubject*2,:) = table2array(tableT2toSave(1:2,:));
    %check for reasonable R square values
    for indexMetabolite = 1:length(metaboliteNames)
        if (T2_R2_CRLBsmaller900((indexSubject*2),indexMetabolite)<threshold_Rsquare)
            T2_R2_CRLBsmaller900((indexSubject*2),indexMetabolite)= NaN;
            T2_R2_CRLBsmaller900((indexSubject*2-1),indexMetabolite)= NaN;
        elseif (T2_R2_CRLBsmaller900((indexSubject*2-1),indexMetabolite)>250)
            T2_R2_CRLBsmaller900((indexSubject*2),indexMetabolite)= NaN;
            T2_R2_CRLBsmaller900((indexSubject*2-1),indexMetabolite)= NaN;
        end
    end
    T2_R2_ManualTE((indexSubject*2-1):indexSubject*2,:) = table2array(tableT2toSave(3:4,:));
    %check for reasonable R square values
    for indexMetabolite = 1:length(metaboliteNames)
        if (T2_R2_ManualTE((indexSubject*2),indexMetabolite)<threshold_Rsquare)
            T2_R2_ManualTE((indexSubject*2),indexMetabolite)= NaN;
            T2_R2_ManualTE((indexSubject*2-1),indexMetabolite)= NaN;
        elseif (T2_R2_ManualTE((indexSubject*2-1),indexMetabolite)>250)
            T2_R2_ManualTE((indexSubject*2),indexMetabolite)= NaN;
            T2_R2_ManualTE((indexSubject*2-1),indexMetabolite)= NaN;
        end
    end
end
%calculate mean and std of T2 and R2 of CRLB < 900, save new table
T2_R2_CRLBsmaller900_MeanT2 = nanmean(T2_R2_CRLBsmaller900(1:2:2*numberOfSubjects,:)); T2_R2_CRLBsmaller900_StdT2 = nanstd(T2_R2_CRLBsmaller900(1:2:2*numberOfSubjects,:));
T2_R2_CRLBsmaller900_MeanR2 = nanmean(T2_R2_CRLBsmaller900(2:2:2*numberOfSubjects,:)); T2_R2_CRLBsmaller900_StdR2 = nanstd(T2_R2_CRLBsmaller900(2:2:2*numberOfSubjects,:));
T2_R2_CRLBsmaller900(2*numberOfSubjects+1,:) = T2_R2_CRLBsmaller900_MeanT2; T2_R2_CRLBsmaller900(2*numberOfSubjects+2,:) = T2_R2_CRLBsmaller900_StdT2;
T2_R2_CRLBsmaller900(2*numberOfSubjects+3,:) = T2_R2_CRLBsmaller900_MeanR2; T2_R2_CRLBsmaller900(2*numberOfSubjects+4,:) = T2_R2_CRLBsmaller900_StdR2;

if ~isempty(strfind(T2TableOutputFileBase,'JD'))
    exportFileXlsxCRLBsmaller900 = [localFilePathBase outputPath ,strcat('T2_R2_CRLBsmaller900_allVolunteers_',TruncToFit,'_JD.xlsx')];
else
    exportFileXlsxCRLBsmaller900 = [localFilePathBase outputPath ,strcat('T2_R2_CRLBsmaller900_allVolunteers_',TruncToFit,'.xlsx')];
end
xlswrite(exportFileXlsxCRLBsmaller900, {[localFilePathBase outputPath]}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsxCRLBsmaller900, T2_R2_CRLBsmaller900, 1, cellPosition);

tableT2toSaveCRLBsmaller900 = array2table(T2_R2_CRLBsmaller900,'RowNames',rowNames_CRLBsmaller900,'VariableNames',metaboliteNames)
if ~isempty(strfind(T2TableOutputFileBase,'JD'))
    fileName = [localFilePathBase outputPath ,strcat('T2_R2_CRLBsmaller900_allVolunteers_',TruncToFit,'_JD')];
else
    fileName = [localFilePathBase outputPath ,strcat('T2_R2_CRLBsmaller900_allVolunteers_',TruncToFit)];
end
save(fileName, 'tableT2toSaveCRLBsmaller900');

%calculate mean and std of T2 and R2 of manual TE, save new table
T2_R2_ManualTE_MeanT2 = nanmean(T2_R2_ManualTE(1:2:2*numberOfSubjects,:)); T2_R2_ManualTE_StdT2 = nanstd(T2_R2_ManualTE(1:2:2*numberOfSubjects,:));
T2_R2_ManualTE_MeanR2 = nanmean(T2_R2_ManualTE(2:2:2*numberOfSubjects,:)); T2_R2_ManualTE_StdR2 = nanstd(T2_R2_ManualTE(2:2:2*numberOfSubjects,:));
T2_R2_ManualTE(2*numberOfSubjects+1,:) = T2_R2_ManualTE_MeanT2; T2_R2_ManualTE(2*numberOfSubjects+2,:) = T2_R2_ManualTE_StdT2;
T2_R2_ManualTE(2*numberOfSubjects+3,:) = T2_R2_ManualTE_MeanR2; T2_R2_ManualTE(2*numberOfSubjects+4,:) = T2_R2_ManualTE_StdR2;

if ~isempty(strfind(T2TableOutputFileBase,'JD'))
    exportFileXlsxManualTE = [localFilePathBase outputPath ,strcat('T2_R2_ManualTE_allVolunteers_',TruncToFit,'_JD.xlsx')];
else
    exportFileXlsxManualTE = [localFilePathBase outputPath ,strcat('T2_R2_ManualTE_allVolunteers_',TruncToFit,'.xlsx')];
end
xlswrite(exportFileXlsxManualTE, {[localFilePathBase outputPath]}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsxManualTE, T2_R2_ManualTE, 1, cellPosition);

tableT2toSaveManualTE = array2table(T2_R2_ManualTE,'RowNames',rowNames_ManualTE,'VariableNames',metaboliteNames)
if ~isempty(strfind(T2TableOutputFileBase,'JD'))
    fileName = [localFilePathBase outputPath ,strcat('T2_R2_ManualTE_allVolunteers_',TruncToFit,'_JD')];
else
    fileName = [localFilePathBase outputPath ,strcat('T2_R2_ManualTE_allVolunteers_',TruncToFit)];
end
save(fileName, 'tableT2toSaveManualTE');

end