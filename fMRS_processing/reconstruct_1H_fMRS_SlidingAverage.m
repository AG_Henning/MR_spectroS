function [a] = reconstruct_1H_fMRS_SlidingAverage(currentFolder,fid_id, isMC, isInVivo, weights, saveResults, visualType, nOfSpectra, nslidingAv)

nsamplesBeforeEcho = 4;

if ~exist('isInVivo', 'var')
    isInVivo = true;
end

if ~exist('saveResults', 'var')
    saveResults = true;
end

% if nBlocks == 5
%     nAvg2save = '#64';
% elseif nBlocks == 10
%     nAvg2save = '#32';
% end

if isMC
    a = MR_spectroS(fid_id,1);
else
    a = MR_spectroS(fid_id);
end

%%
if nsamplesBeforeEcho ~= 0
    startpoint = nsamplesBeforeEcho + 1;
    endpoint   = size(a.Data{1},a.kx_dim)-(32-nsamplesBeforeEcho);
    data = squeeze(a.Data{1});
    data = data(startpoint:endpoint,:,:);
    dataNew(:,1,1,:,1,1,1,1,1,1,1,:) = data;
    a.Data{1} = dataNew;
    clear data dataNew startpoint endpoint
end

%%
if isInVivo
    %% truncate before anything
    trunc_ms = 250;
    truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
    a = a.Truncate(truncPoint);
    
    a = a.FrequencyAlign;
end
if isMC
    if isInVivo
        a = a.Rescale;
    end
    a = a.ReconData;
end

%sliding average
oldData         = a.Data{1};
nAverages       = size(oldData,a.meas_dim);
if isMC
    nOfSpectra = nOfSpectra/2;    %already MC combined
    nslidingAvNew = nslidingAv/2;
end

if (a.Parameter.ReconFlags.isfMRSMovingAveraged)
    error('The data are already moving-averaged!!!')
end

nSlidSpec = (nAverages - nOfSpectra)/nslidingAvNew + 1;    %

newSize                = size(oldData);
newSize(a.meas_dim) = nSlidSpec;
newData                = zeros(newSize);
for n=1:nSlidSpec
    startPoint =  1 + (n-1)* nslidingAvNew;
    endPoint   = (n-1)*nslidingAvNew + nOfSpectra;
    newData(:,:,:,:,:,:,:,:,:,:,:,n) = sum(oldData(:,:,:,:,:,:,:,:,:,:,:,startPoint:endPoint),a.meas_dim);
end

a.Data{1}                                     = newData;
a.Parameter.ReconFlags.isfMRSMovingAveraged   = true;
a.Parameter.fMRSAveSettings.numberOfNewPoints = nSlidSpec;
a.Parameter.EddyCurrSettings.numberOfWaterSignals = nSlidSpec;
clear oldData nAverages newSize newData startPoint endPoint

%eddy current correction
if isMC
    a = a.EddyCurrentCorrection;
else
    [FileName, PathName] = uigetfile('*.dat',['Select water reference for :' a.Parameter.Filename]);
    path = strcat(PathName,FileName);
    tmpData      = mapVBVD(path);
    tmpWaterReferenceData = tmpData.image(''); %Read the data
    waterReferenceData = double(permute(tmpWaterReferenceData,[1 3 4 2 5 7 8 11 12 9 10 6]));
    a = a.EddyCurrentCorrection(waterReferenceData);
end

%combine coils using SVD
if exist('weights','var')
    if ~isempty(weights)
        a = a.CombineCoils(weights);
    else
        a = a.CombineCoils;
    end
else
    a = a.CombineCoils;
end

%% setup the settings for the FrequencyAlignFreqDomain
a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;

%indices of Interest: given in ppm
% water peak
a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = 2.008;

%set zeroFillingParameter to get smooth approximations
a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;

%search area (+-) in ppm
if isInVivo
    a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.1;
else
    a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.2;
end
%spline filtering is not needed if the spectra are not too noisy.
%flag to do spline filtering
a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
%spline filtering coefficient
a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;
% do the actual Frequency Alignment
a = a.FrequencyAlignFreqDomain;

%% water suppression
if isInVivo
    a.Parameter.HsvdSettings.bound = [-100 100];
else
    a.Parameter.HsvdSettings.bound = [-100 100];
end
a.Parameter.HsvdSettings.n = size(a.Data{1},1);
a.Parameter.HsvdSettings.p = 25;
[~, a] = a.Hsvd;
[~, a] = a.Hsvd;
[~, a] = a.Hsvd;
%
if isMC
    %% Reverse MC
    %     a = a.ReverseMC;
    %     [~, a] = a.Hsvd;
end

%truncate data
if isInVivo
    trunc_ms = 150;
    %     trunc_ms = 100;     %MM
    truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
    a = a.Truncate(truncPoint);
end

%% fMRS processing

%set stimulus Paradigm, starting with off block, ending with on block
% if nBlocks == 5
%     paradigm = ones(1,2*nBlocks);
%     for i = 1:4:2*nBlocks
%         paradigm(i) = 0;
%     end
% elseif nBlocks == 10
%     paradigm = ones(1,nBlocks);
%     for i = 1:4:nBlocks
%         paradigm(i) = 0;
%     end
%     for i = 2:2:nBlocks
%         paradigm(i) = 2;
%     end
% else
%     sprintf('nBlocks is not 5 or 10')
% end
% a.Parameter.fMRSAveSettings.paradigm = paradigm;

close all;
% %BOLD Effect
% SelProcValues = SelectedProcessingValues();
% %Water
% SelProcValues.PeakRange = [4 5];    %ppm
% SelProcValues.NoiseRangeFD = [-5.3 -0.3];    %ppm
% SelProcValues.NoiseTimeDomain = 0;     %SNR in Frequency Domain (=1 in Time Domain, but here: data are truncated)
% currentValueH20 = SelProcValues.PeakRange;
% currentValueH20 = convertPPMtoIndex(a, currentValueH20,1);
% currentValueH20 =[find(currentValueH20==1,1,'first') find(currentValueH20==1,1,'last')];
% SelProcValues.PeakRange = [currentValueH20(1) currentValueH20(2)];
% currentValueNoise = SelProcValues.NoiseRangeFD;
% currentValueNoise = convertPPMtoIndex(a, currentValueNoise,1);
% currentValueNoise =[find(currentValueNoise==1,1,'first') find(currentValueNoise==1,1,'last')];
% SelProcValues.NoiseRangeFD = [currentValueNoise(1) currentValueNoise(2)];
% boldEffect( a, SelProcValues, 1, 2)
%
% subplot(2,1,1)
% boxoff = []; boxon = []; boxyoff = [];  boxyon = []; boxy2on = []; boxy2off = []; n = 1;
% yLimits = get(gca,'YLim');
% if nBlocks == 5
%     for i = 1:nBlocks
%         if mod(i,2)     %odd
%             boxoff  = [boxoff (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
%             boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else            %even
%             boxon   = [boxon (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
%             boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%     end
% elseif nBlocks == 10
%     for i = 1:2:nBlocks
%         if mod(n,2)     %odd
%             boxoff  = [boxoff (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
%             boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else            %even
%             boxon   = [boxon (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
%             boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%         n = n+1;
%     end
% else
%     sprintf('nBlocks is not 5 or 10')
% end
% patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
% patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
% ylim([yLimits(1) yLimits(2)]);
% subplot(2,1,2)
% yLimits = get(gca,'YLim');
% if nBlocks == 5
%     for i = 1:nBlocks
%         if mod(i,2)     %odd
%             boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else
%             boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%     end
% elseif nBlocks == 10
%     n = 1;
%     for i = 1:2:nBlocks
%         if mod(n,2)     %odd
%             boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else
%             boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%         n = n+1;
%     end
% else
%     sprintf('nBlocks is not 5 or 10')
% end
%
% patch(boxoff,boxy2off,[0 0 1],'FaceAlpha',0.2)
% patch(boxon,boxy2on,[1 0 0],'FaceAlpha',0.2)
% ylim([yLimits(1) yLimits(2)]);
%
% if saveResults
%     fig2save_BOLDH2O = fullfile(currentFolder, [num2str(visualType) '_BOLD_H2O' num2str(nAvg2save) '.fig']);
%     savefig(fig2save_BOLDH2O)
%     close all
% end
%
% %NAA
% SelProcValues.PeakRange = [1.5 2.5];    %ppm
% currentValueNAA = SelProcValues.PeakRange;
% currentValueNAA = convertPPMtoIndex(a, currentValueNAA,1);
% currentValueNAA =[find(currentValueNAA==1,1,'first') find(currentValueNAA==1,1,'last')];
% SelProcValues.PeakRange = [currentValueNAA(1) currentValueNAA(2)];
% boldEffect( a, SelProcValues, 1, 1)
%
% subplot(2,1,1)
% boxoff = []; boxon = []; boxyoff = [];  boxyon = []; boxy2on = []; boxy2off = []; n = 1;
% yLimits = get(gca,'YLim');
% if nBlocks == 5
%     for i = 1:nBlocks
%         if mod(i,2)     %odd
%             boxoff  = [boxoff (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
%             boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else            %even
%             boxon   = [boxon (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
%             boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%     end
% elseif nBlocks == 10
%     for i = 1:2:nBlocks
%         if mod(n,2)     %odd
%             boxoff  = [boxoff (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
%             boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else            %even
%             boxon   = [boxon (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
%             boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%         n = n+1;
%     end
% else
%     sprintf('nBlocks is not 5 or 10')
% end
%
% patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
% patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
% ylim([yLimits(1) yLimits(2)]);
% subplot(2,1,2)
% yLimits = get(gca,'YLim');
% if nBlocks == 5
%     for i = 1:nBlocks
%         if mod(i,2)     %odd
%             boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else
%             boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%     end
% elseif nBlocks == 10
%     n = 1;
%     for i = 1:2:nBlocks
%         if mod(n,2)     %odd
%             boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else
%             boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%         n = n+1;
%     end
% else
%     sprintf('nBlocks is not 5 or 10')
% end
% patch(boxoff,boxy2off,[0 0 1],'FaceAlpha',0.2)
% patch(boxon,boxy2on,[1 0 0],'FaceAlpha',0.2)
% ylim([yLimits(1) yLimits(2)]);
%
% if saveResults
%     fig2save_BOLDNAA = fullfile(currentFolder, [num2str(visualType) '_BOLD_NAA' num2str(nAvg2save) '.fig']);
%     savefig(fig2save_BOLDNAA)
%     close all;
% end
%
% %Cr
% SelProcValues.PeakRange = [2.9 3.2];    %ppm
% currentValueCr = SelProcValues.PeakRange;
% currentValueCr = convertPPMtoIndex(a, currentValueCr,1);
% currentValueCr =[find(currentValueCr==1,1,'first') find(currentValueCr==1,1,'last')];
% SelProcValues.PeakRange = [currentValueCr(1) currentValueCr(2)];
% boldEffect( a, SelProcValues, 1, 1)
%
% subplot(2,1,1)
% boxoff = []; boxon = []; boxyoff = [];  boxyon = []; boxy2on = []; boxy2off = []; n = 1;
% yLimits = get(gca,'YLim');
% if nBlocks == 5
%     for i = 1:nBlocks
%         if mod(i,2)     %odd
%             boxoff  = [boxoff (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
%             boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else            %even
%             boxon   = [boxon (i-0.5) (i-0.5) (i+0.5) (i+0.5)];
%             boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%     end
% elseif nBlocks == 10
%     for i = 1:2:nBlocks
%         if mod(n,2)     %odd
%             boxoff  = [boxoff (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
%             boxyoff = [boxyoff yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else            %even
%             boxon   = [boxon (i-0.5) (i-0.5) (i+1.5) (i+1.5)];
%             boxyon  = [boxyon yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%         n = n+1;
%     end
% else
%     sprintf('nBlocks is not 5 or 10')
% end
% patch(boxoff,boxyoff,[0 0 1],'FaceAlpha',0.2)
% patch(boxon,boxyon,[1 0 0],'FaceAlpha',0.2)
% ylim([yLimits(1) yLimits(2)]);
% subplot(2,1,2)
% yLimits = get(gca,'YLim');
% if nBlocks == 5
%     for i = 1:nBlocks
%         if mod(i,2)     %odd
%             boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else
%             boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%     end
% elseif nBlocks == 10
%     n = 1;
%     for i = 1:2:nBlocks
%         if mod(n,2)     %odd
%             boxy2off = [boxy2off yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         else
%             boxy2on  = [boxy2on yLimits(1) yLimits(2) yLimits(2) yLimits(1)];
%         end
%         n = n+1;
%     end
% else
%     sprintf('nBlocks is not 5 or 10')
% end
% patch(boxoff,boxy2off,[0 0 1],'FaceAlpha',0.2)
% patch(boxon,boxy2on,[1 0 0],'FaceAlpha',0.2)
% ylim([yLimits(1) yLimits(2)]);
%
% if saveResults
%     fig2save_BOLDCr = fullfile(currentFolder, [num2str(visualType) '_BOLD_Cr' num2str(nAvg2save) '.fig']);
%     savefig(fig2save_BOLDCr)
%     close all;
% end

% %average on and off blocks
% if nBlocks == 5         %size(a.Data{1},a.meas_dim)
%     paradigm(1) = -1;
% elseif nBlocks == 10
%     paradigm = ones(1,2*nBlocks);
%     for i = 1:2:2*nBlocks
%         paradigm(i) = -1;
%         if (i == 11) || (i == 19)
%             paradigm(i) = 0;
%         elseif (i == 7) || (i == 15)
%             paradigm(i) = 1;
%         end
%     end
% end
% a.Parameter.fMRSAveSettings.paradigm = paradigm;

if saveResults
    %all blocks in one MR_Spectro file
    filename = sprintf('%d',a.Parameter.Headers.PatientName);
    save([filename '_' num2str(visualType) '_woBOLDCorr_slAv' num2str(nslidingAv) '.mat'], 'a')
    %every block in a seperate MR_Spectro and RAW file
    addSinglet0ppm = true;
    data = a.Data{1};
    for nBl = 1:nSlidSpec
        this = a;
        this.Data{1} = data(:,:,:,:,:,:,:,:,:,:,:,nBl);
        this.ExportLcmRaw('',[filename '_' num2str(visualType) '_woBOLDCorr_Av' num2str(nBl)], addSinglet0ppm);
        save([filename '_' num2str(visualType) '_woBOLDCorr_Av' num2str(nBl) '.mat'], 'this')
    end
end



end