function fitting_all_test_data_profit()

fitLCModel = false; %false fits profit
use_water_references =  true;

%TODO change
TR = 6000;%ms

reconOption = 2;
truncOn = true;

exportFolders = {'0 Water References',...
    '1 Normal recon', '2 Removed averages ','3 Removed coils',...
    '4 Removed averages and coils', '5 Water Supp Off', '6 Frequency Alignment Off', ...
    '7 Rescale off', '8 Rescale Water Supp Off'};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolders);
waterExportPathBase = strcat(pathBaseExportFiles, exportFolders{1}, '\');
dataExportPathBase = strcat(pathBaseExportFiles, exportFolders{reconOption+1}, '\');

if truncOn == false
    truncSuffix = '_truncOff';
else
    truncSuffix = '';
end
%% configurations for averages and coils

if  (reconOption == 2) || (reconOption == 4)
    namingSuffixesAve = {...
        '_16ave_1', '_16ave_2', '_16ave_3', ...
        '_32ave_1', '_32ave_2', '_32ave_3'};
else
    namingSuffixesAve = {''};
end

% 8/16 Channel 1H coil has been used.
if  (reconOption == 3) || (reconOption == 4)
    namingSuffixesCoil = {...
        '_8coil_vert', '_8coil_horiz', ...
        '_8coil_1', '_8coil_2', ...
        '_12coil_vert_1', '_12coil_vert_2', '_12coil_horiz_1', '_12coil_horiz_2', ...
        '_12coil_1', '_12coil_2','_12coil_3', '_12coil_4',...
        '_4coil_vert_1', '_4coil_vert_2', '_4coil_horiz_1', '_4coil_horiz_2', ...
        '_4coil_1', '_4coil_2','_4coil_3', '_4coil_4'};
else
    namingSuffixesCoil = {''};
end

subjects = { ...
%     'Summed' ...
    'Subj_1' ...
    'Subj_2' ...
    'Subj_3' ...
    'Subj_4' ...
    'Subj_5' ...
    'Subj_6' ...
    'Subj_7' ...
    'Subj_8' ...
    'Subj_9' ...
    'Subj_10' ...
    'Subj_11' ...
    };

TEs = [24; ];%32; 40; 52; 60];

numberOfSubjects = length(subjects);
waterExportPath = cell(1,numberOfSubjects);
dataExportPath = cell(1,numberOfSubjects);
for indexSubj = 1:numberOfSubjects
    waterExportPath{indexSubj} = [waterExportPathBase subjects{indexSubj} '\\'];
    dataExportPath{indexSubj} = [dataExportPathBase subjects{indexSubj} '\\'];
end

numberOfTEs = length(TEs);

if fitLCModel
    %% create LCModel instance
    controlFilesPathRemote = '/Desktop/TestDataFitting/LCModelConfig/';
    outputFilesPathRemote = '/Desktop/TestDataFitting/Output/';
    outputFilesPathLocal = [pathBaseExportFiles, 'Output/'];
    LCModelCallerInstance = startLCModel(controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal);
    if reconOption == 4 % don't copy files, there are too many
        warning('Files are not copied, and should be available on the server!');
        copyFiles = false;
    else
        copyFiles = true;
    end
end

%% do the actual fitting

for indexSubj = 1:numberOfSubjects
    waterPath = waterExportPath{indexSubj};
    filenameWater = [ subjects{indexSubj} '_water'];
    % use the followind info if needed
    %     load([filenameWater '.mat'], 'aWater', 'maxWaterPeak');
    
    concentrations = cell(numberOfTEs,length(namingSuffixesAve));
    concentrations_H2O = cell(numberOfTEs,length(namingSuffixesAve));
    concentrations_MC_H2O = cell(numberOfTEs,length(namingSuffixesAve));
    if ~fitLCModel
        FQNs = cell(numberOfTEs,length(namingSuffixesAve));
        FQNs2 = cell(numberOfTEs,length(namingSuffixesAve));
    end
    %% reconstruct data
    for indexTE = 1:1%numberOfTEs
        for indexAverageDelete = 1: length(namingSuffixesAve)
            namingSuffixAve = namingSuffixesAve{indexAverageDelete};
            for indexCoilDelete = 1: length(namingSuffixesCoil)
                namingSuffixCoil = namingSuffixesCoil{indexCoilDelete};
                
                TE_string = ['TE', num2str(TEs(indexTE))];
                filenameData = [ subjects{indexSubj}, '_', TE_string namingSuffixAve namingSuffixCoil truncSuffix];
                
                dataExportPathTE = [dataExportPath{indexSubj}, '\\', TE_string, '\\'];
                load([dataExportPathTE, filenameData, '.mat'],'a')
                MC_mix =2;
                filenameData_MC_water = [filenameData, '_MC_water'];
                a.ExportLcmRaw(dataExportPathTE, filenameData_MC_water, false, 'No', -4.7, 0.05, MC_mix);
                if fitLCModel
                    if use_water_references
                        try
                            fittingLCModelWithTransfer(LCModelCallerInstance, pathBaseExportFiles, ...
                                filenameData, dataExportPathTE, filenameWater, waterPath, TE_string, copyFiles);
                            movefile([outputFilesPathLocal filenameData '.*'], dataExportPathTE);
                            concentrations{indexTE,indexAverageDelete, indexCoilDelete} = ...
                                importConcentrationLCModelTable([filenameData '.table'],dataExportPathTE, TE_string, 'Met moiety');
                            %MC_water
%                             fittingLCModelWithTransfer(LCModelCallerInstance, pathBaseExportFiles, ...
%                                 filenameData_MC_water, dataExportPathTE, filenameWater, waterPath, TE_string, copyFiles, true);
%                             movefile([outputFilesPathLocal filenameData_MC_water '.*'], dataExportPathTE);
%                             concentrations_MC_H2O{indexTE,indexAverageDelete, indexCoilDelete} = ...
%                                 importConcentrationLCModelTable([filenameData_MC_water '.table'],dataExportPathTE, TE_string, 'Met moiety');
                        catch
                            concentrations{indexTE,indexAverageDelete, indexCoilDelete} = {};
                            continue;
                        end
                    else
                        fittingLCModelWithTransfer(LCModelCallerInstance, pathBaseExportFiles, ...
                            filenameData, dataExportPathTE, [], [], TE_string, copyFiles);
                    end
                    plot_All_Fit_Metabolites([filenameData, '.coord'],dataExportPathTE, 'Met moiety', true, true);
                else %TODO
                    if use_water_references
                        fittingProFit(filenameData, dataExportPathTE, filenameWater, waterPath, TEs(indexTE), TR, dataExportPathTE);
                        fitting_MC_water(filenameData_MC_water, dataExportPathTE, TEs(indexTE), TR, dataExportPathTE)
                    else
                        fittingProFit(filenameData, dataExportPathTE, [], [], TEs(indexTE), TR, dataExportPathTE);
                    end
                    [concentrations{indexTE,indexAverageDelete, indexCoilDelete}, activeMetabolites, FQNs{indexTE,indexAverageDelete, indexCoilDelete}, FQNs2{indexTE,indexAverageDelete, indexCoilDelete}] = ...
                        plot_ProFit_Metabolites_Fit([filenameData, '_profit.mat'],dataExportPathTE, 'Met', true);
                    load([dataExportPathTE, filenameData, '_profit_H2O.mat'],'concH2O');
                    concentrations_H2O{indexTE,indexAverageDelete, indexCoilDelete}=concH2O;
                    load([dataExportPathTE, filenameData_MC_water, '_profit.mat'],'concH2O');
                    concentrations_MC_H2O{indexTE,indexAverageDelete, indexCoilDelete}=concH2O;
                end
                close all
            end
        end
    end
    if fitLCModel
        save([dataExportPath{indexSubj}, 'concentrations' truncSuffix '.mat'], 'concentrations');
        save([dataExportPath{indexSubj}, 'concentrations', truncSuffix '_MC_H2O.mat'], 'concentrations_MC_H2O');
    else
        save([dataExportPath{indexSubj}, 'concentrations', truncSuffix '_profit.mat'], 'concentrations', 'activeMetabolites', 'FQNs', 'FQNs2');
        save([dataExportPath{indexSubj}, 'concentrations', truncSuffix '_H2O_profit.mat'], 'concentrations_H2O');
        save([dataExportPath{indexSubj}, 'concentrations', truncSuffix '_MC_H2O_profit.mat'], 'concentrations_MC_H2O');
    end
end

close all
end

