function [makeBasisFiles, basisFileNames] = createLCModelMakeBasisTEseries(makeBasisFilesPath, defaultMakeBasisFile, orderTEs, defaultTE, defaultLCModelUser, currentLCModelUser)
% Creates the make basis files for the TE series, given a default make basis file.
% the newly generated files will be placed in the same path as indicated by
% the makeBasisFilesPath.
% The defaultMakeBasisFile has to include the keyword "Default" in the file name.
% A sample input:

if(nargin  <  6)
    error('Error: createLCModelConfigsTEseries requires minimum 6 input arguments: makeBasisFilesPath, defaultMakeBasisFile, orderTEs, defaultTE, defaultLCModelUser, currentLCModelUser .');
end
orderTEsLong = strcat('TE', orderTEs);
defaultTELong = strcat('TE', defaultTE);
orderEchot = strcat('echot=', orderTEs);
defaultEchot = strcat('echot=', defaultTE);
defaultKeyword = 'Default';
indexFind  = strfind(defaultMakeBasisFile, defaultKeyword);
if isempty(indexFind)
    error('The default make basis file should have the keyword "Default" in its file name.');
end

makeBasisFileBase = defaultMakeBasisFile(1:indexFind-1);
makeBasisFileSuffix = defaultMakeBasisFile(indexFind+length(defaultKeyword):end);

makeBasisDefaultFileID = fopen([makeBasisFilesPath defaultMakeBasisFile], 'r');

if makeBasisDefaultFileID == -1
    error('Invalid file identifier. The following file does not exist: \n %s', [makeBasisFilesPath defaultMakeBasisFile]);
end
%iterate over all TEs
makeBasisFiles = cell(length(orderTEsLong),1);
basisFileNames = cell(length(orderTEsLong),1);
for index = 1:length(orderTEsLong)
    makeBasisFiles{index} = strcat(makeBasisFileBase, orderTEsLong{index}, makeBasisFileSuffix);
    makeBasisFileFull = strcat(makeBasisFilesPath, makeBasisFiles{index});
    makeBasisFileID = fopen(makeBasisFileFull, 'w');
    display(makeBasisFileFull);
    
    while(~feof(makeBasisDefaultFileID))
        s = fgetl(makeBasisDefaultFileID);
        s = strrep(s, defaultLCModelUser, currentLCModelUser);
        s = strrep(s, defaultTELong, orderTEsLong{index});
        s = strrep(s, defaultEchot, orderEchot{index});
        if ~isempty(strfind(s,'FILBAS')) || ~isempty(strfind(s,'filbas'))
            s_split= strsplit(s,'/');
            basisFileName= strsplit(s_split{end},'.basis');
            basisFileNames{index} = basisFileName{1};
        end
        fprintf(makeBasisFileID,'%s\n', s);
    end
    frewind(makeBasisDefaultFileID);
    fclose(makeBasisFileID);
end
fclose(makeBasisDefaultFileID);
end