function calculate_Mean_FWHM_pH_31P_T2()
clear; clc; close all;

%%% change fileName and exportFileXlsx  for Real/Ideal

pathName = 'T2 31P Paper Data';
[localFilePathBase] = pathToDataFolder(pathName);

subjects = {'9873';'1090';'3490';'9810';'1658';'2017';'6524';'3009';...
    '6260';'2823';'2455';'4085'};
prefixes = {'2019-09-30_';'2019-10-10_';'2019-10-14_';'2019-10-14_';...
    '2019-10-22_';'2019-11-07_';'2019-11-07_';'2019-11-20_';...
    '2019-11-20_';'2019-12-05_';'2020-07-22_';'2020-07-23_';};
suffixFolder = '_SurfCoil';

numberOfSubjects = length(subjects);
paths = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    paths{indexSubject} = [localFilePathBase 'Output\' prefixes{indexSubject} subjects{indexSubject} suffixFolder '\'];
end

defaultSubject ='ZZZZ';
FWHMTableOutputFileBase = 'ZZZZ_STEAM_Ideal_noTrunc_FWHM_Freq_Fit.mat';
FWHMTableOutputFileBase = 'ZZZZ_STEAM_Real__noTrunc_FWHM_Freq_Fit.mat';
FWHMTableOutputFileBase = 'ZZZZ_STEAM_Ideal_JD_noTrunc_FWHM_Freq_Fit.mat';

TestFileToLoad = [paths{1} strrep(FWHMTableOutputFileBase,defaultSubject,subjects{1})];
load(TestFileToLoad)    %named FWHM
metaboliteNames = FWHM.Properties.VariableNames;
rowNames = FWHM.Properties.RowNames;
rowNamesFWHM_ManualTE = strcat(subjects,'_',rowNames{1}); rowNamesFreq_ManualTE = strcat(subjects,'_',rowNames{2}); rowNamespH_ManualTE = strcat(subjects,'_',rowNames{3});
rowNames_ManualTE = [rowNamesFWHM_ManualTE rowNamesFreq_ManualTE rowNamespH_ManualTE]'; rowNames_ManualTE = rowNames_ManualTE(:);
rowNames_ManualTE = [rowNames_ManualTE; {'FWHM_Mean'}; {'FWHM_Std'}; {'Freq_Mean'}; {'Freq_Std'}; {'pH_Mean'}; {'pH_Std'}];

clearvars TestFileToLoad FWHM

FWHM_Freq_pH = zeros(3*numberOfSubjects+6,length(metaboliteNames)); 
for indexSubject = 1:numberOfSubjects
    FileToLoad = [paths{indexSubject} strrep(FWHMTableOutputFileBase,defaultSubject,subjects{indexSubject})];
    load(FileToLoad)    %named FWHM
    FWHM_Freq_pH((indexSubject*2-1+(indexSubject-1)):indexSubject*3,:) = table2array(FWHM(1:3,:));
end
%find FWHM > 100, set to NaN
FWHM_Freq_pH(FWHM_Freq_pH > 100) = nan;
%calculate mean and std of FWHM, Freq, pH; save new table
FWHM_Freq_pH_MeanFWHM = nanmean(FWHM_Freq_pH(1:3:3*numberOfSubjects,:)); FWHM_Freq_pH_StdFWHM = nanstd(FWHM_Freq_pH(1:3:3*numberOfSubjects,:));
FWHM_Freq_pH_MeanFreq = nanmean(FWHM_Freq_pH(2:3:3*numberOfSubjects,:)); FWHM_Freq_pH_StdFreq = nanstd(FWHM_Freq_pH(2:3:3*numberOfSubjects,:));
FWHM_Freq_pH_MeanpH = nanmean(FWHM_Freq_pH(3:3:3*numberOfSubjects,:));   FWHM_Freq_pH_StdpH = nanstd(FWHM_Freq_pH(3:3:3*numberOfSubjects,:));
FWHM_Freq_pH(3*numberOfSubjects+1,:) = FWHM_Freq_pH_MeanFWHM; FWHM_Freq_pH(3*numberOfSubjects+2,:) = FWHM_Freq_pH_StdFWHM;
FWHM_Freq_pH(3*numberOfSubjects+3,:) = FWHM_Freq_pH_MeanFreq; FWHM_Freq_pH(3*numberOfSubjects+4,:) = FWHM_Freq_pH_StdFreq;
FWHM_Freq_pH(3*numberOfSubjects+5,:) = FWHM_Freq_pH_MeanpH;   FWHM_Freq_pH(3*numberOfSubjects+6,:) = FWHM_Freq_pH_StdpH;

% exportFileXlsx = [localFilePathBase 'Output\' ,strcat('FWHM_Freq_pH_allVolunteers_noTrunc_Ideal.xlsx')];
% exportFileXlsx = [localFilePathBase 'Output\' ,strcat('FWHM_Freq_pH_allVolunteers_noTrunc_Ideal_JD.xlsx')];
exportFileXlsx = [localFilePathBase 'Output\' ,strcat('FWHM_Freq_pH_allVolunteers_noTrunc_Real.xlsx')];
xlswrite(exportFileXlsx, {[localFilePathBase 'Output\']}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, FWHM_Freq_pH, 1, cellPosition);

FWHMMean = array2table(FWHM_Freq_pH,'RowNames',rowNames_ManualTE,'VariableNames',metaboliteNames)
% fileName = [localFilePathBase 'Output\' ,strcat('FWHM_Freq_pH_allVolunteers_noTrunc_Ideal')];
% fileName = [localFilePathBase 'Output\' ,strcat('FWHM_Freq_pH_allVolunteers_noTrunc_Ideal_JD')];
fileName = [localFilePathBase 'Output\' ,strcat('FWHM_Freq_pH_allVolunteers_noTrunc_Real')];
save(fileName, 'FWHMMean');







end