function this = ScaleData(this,scalingFactor)

%//////////////////////////////////////////////////////
%
% function this = RescaleData(this,rescaleFactor)
% Rescale the data with the give rescaleFactor 
% Use rescaling for normilised spectra 
% i.e rescaleFactor = NAA peak
%
%//////////////////////////////////////////////////////

% if ~this.Parameter.ReconFlags.isCoilsCombined
%     warning('Coils are not combined...skipped')
%     return
% end

% if ~this.Parameter.ReconFlags.isAveraged
%     warning('The data should be averaged fisrt...skipped')
%     return
% end;


 if this.Parameter.ReconFlags.isScaledData
    warning('Data are already scaled!')
 end
 
 
 
 if ~exist('scalingFactor','var')
    x = inputdlg('Enter scaling factor:',...
             'ScaleData', [1 20]);
         
         if isempty(x)   
            return; %exit
         end
         
    scalingFactor = str2double(x{:});  
    clear x
 end

 
 %
 data = this.Data{1};
 
 data = data/scalingFactor;
 
 
 this.Data{1}= data;
 
  if this.Parameter.ReconFlags.isScaledData
      
      
    this.Parameter.ReconFlags.isScaledData = this.Parameter.ReconFlags.isScaledData*...
        scalingFactor;  
  else
      
     this.Parameter.ReconFlags.isScaledData = scalingFactor;     
 end
 
end

