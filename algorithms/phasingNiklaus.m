function wanted_phase = phasingNiklaus(fid, currentBandwidth, f0)

wanted_phase = 0.0;

NUM_ELEMENTS_OUT = size(fid,1);
%Apodisation with Hamming
seriesOfSize = 0:NUM_ELEMENTS_OUT-1;
HammingFilterFactor = pi/NUM_ELEMENTS_OUT;
hammingFilter = 0.54+0.46*cos(seriesOfSize * HammingFilterFactor);
apodizedFid = fid .* hammingFilter';
%zerofilling and fft
zeroFillFactor = 2;
spectrum = fftshift(fft(apodizedFid, zeroFillFactor * NUM_ELEMENTS_OUT));

bandwidth = [0 zeroFillFactor * currentBandwidth];

% set shifts for zp and phase determination
singlets = {[8.44 0.0],...			% phantom: DSS/Na formate
                [ 2.008 3.027 3.21 3.913]};		% in vivo: NAA/Cre/Cho 2.008 3.027 3.21 
detect = 2;            
zp2 = 4.72; %water frequency
pc1RefPpm = 2.8; %editable input: center frequency for acquisition

displayData = false;
verbose = true;
maxPhasePc1 = 1800;
maxPhasePc0Deg = 180;
[pc0,pc1] = pc0_correction(spectrum', maxPhasePc0Deg, maxPhasePc1, pc1RefPpm, bandwidth/f0,zp2, singlets,detect, verbose)