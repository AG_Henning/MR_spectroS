function reconstruct_all_MetT1()

pathName = 'Met T1 WM data path';
subjects = { ...
    '1405' ...
    '1706' ...
    '1717' ...
    '2016' ...
    '2017' ...
    '3373' ...
    '3490' ...
    '6971' ...
    '7338' ...
    '7782' ...
    '9810' ...
    };

pathBase = pathToDataFolder(pathName, subjects);

TIs = [20; 100; 400; 700; 1000; 1400; 2500];
%TIs = [1000];
numberOfSubjects = length(subjects);
paths = cell(1,numberOfSubjects);
for indexSubj = 1:numberOfSubjects
    paths{indexSubj} = [pathBase subjects{indexSubj} '\\'];
end

files_TI20 = cell(1, numberOfSubjects);
files_TI100 = cell(1, numberOfSubjects);
files_TI400 = cell(1, numberOfSubjects);
files_TI700 = cell(1, numberOfSubjects);
files_TI1000 = cell(1, numberOfSubjects);
files_TI1400 = cell(1, numberOfSubjects);
files_TI2500 = cell(1, numberOfSubjects);
% water_files = cell(1, numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    files_TI20{indexSubject} = ls([paths{indexSubject}, '*_inv20_*.dat']);
    files_TI100{indexSubject} = ls([paths{indexSubject}, '*_inv100_*.dat']);
    files_TI400{indexSubject} = ls([paths{indexSubject}, '*_inv400_*.dat']);
    files_TI700{indexSubject} = ls([paths{indexSubject}, '*_inv700_*.dat']);
    files_TI1000{indexSubject} = ls([paths{indexSubject}, '*_inv1000_*.dat']);
    files_TI1400{indexSubject} = ls([paths{indexSubject}, '*_inv1400_*.dat']);
    files_TI2500{indexSubject} = ls([paths{indexSubject}, '*_inv2500_*.dat']);
   % water_files{indexSubject} = ls([paths{indexSubject}, '*_scout_wref_*.dat']);
end

addSinglet0ppm = 'Yes';
numberOfTIs = length(TIs);

filesPath = cell(numberOfTIs,numberOfSubjects);
% waterFilesPath = cell(1,numberOfSubjects);
files = [files_TI20; files_TI100; files_TI400; files_TI700; files_TI1000; files_TI1400; files_TI2500];
for indexSubj = 1:numberOfSubjects
    for indexTI = 1:numberOfTIs
        filesPath{indexTI, indexSubj} = [paths{indexSubj} files{indexTI, indexSubj}];
    end
    %waterFilesPath{indexSubj} = [paths{indexSubj} water_files{indexSubj}];
end

%% load phase correction factors
fileNamePhaseCorrection = 'PhaseCorrectionFromLCM.xlsx';
[~, ~, phaseCorrectionFactors] = xlsread([pathBase fileNamePhaseCorrection]);
% check the phase correction data
for indexSubj = 1:numberOfSubjects
    if (phaseCorrectionFactors{indexSubj+1,1} ~= str2num(subjects{indexSubj}))
        error('The file %s does not contain the subjects and the phase correction elements in the order specified', fileNamePhaseCorrection);
    end
end
    
%% do the actual reconstruction

summedSpectra = cell(numberOfTIs,1);
indexSubjTIs = zeros(numberOfTIs,1);
for indexSubj = 1:numberOfSubjects
    %% water data
%     [a] = reconstructWater(waterFilesPath{indexSubj});
%     filename = [paths{indexSubj} subjects{indexSubj} '_water.mat'];
%     save(filename, 'a');
%     a.ExportLcmRaw(paths{indexSubj}, strcat(subjects{indexSubj}, '_water'), addSinglet0ppm);
%     %calculate scaling factor the summedSpectra individually according to water peak
%     waterData = a.Data{1};
%     maxWaterPeak = max(real(fftshift(fft(waterData))));     
%     %scale the data for equalizing between subjects
%     a.Data{1} = a.Data{1}./ maxWaterPeak;
%     %create summed spectra
%     if(indexSubj == 1)
%         summedWater = a;
%         newDataWater= zeros(length(a.Data{1}),1,1,1,1,1,1,1,1,1,1,numberOfSubjects);
%     else
%         oldDataWater = summedWater.Data{1};
%         if (ndims(oldDataWater) ~= 12)
%             newDataWater(:,1,1,1,1,1,1,1,1,1,1,1) = oldDataWater;
%         else
%             newDataWater(:,1,1,1,1,1,1,1,1,1,1,:) = oldDataWater;
%         end
%         newDataWater(:,1,1,1,1,1,1,1,1,1,1,indexSubj) = a.Data{1};
%         summedWater.Data{1} = newDataWater;
%     end
%     

    %% get the phase values from the table
    phase0 = phaseCorrectionFactors{indexSubj+1,2};
    phase1Hz = phaseCorrectionFactors{indexSubj+1,4}; 
    %% reconstruct Macromolecule data
    for indexTI = 1:numberOfTIs
        a = reconstruct_DF(filesPath{indexTI, indexSubj}, phase0, phase1Hz);
        filename = [paths{indexSubj} subjects{indexSubj} '_TE' num2str(TIs(indexTI)) '.mat'];
        save(filename, 'a');
        a.ExportLcmRaw(paths{indexSubj}, strcat(subjects{indexSubj}, '_TE', num2str(TIs(indexTI))), addSinglet0ppm);
        
        %% create the TE series data
        if(indexTI == 1)
            mruiSpectra = a;
            newDataMrui= zeros(length(a.Data{1}),1,1,1,1,1,1,1,2,1,1,numberOfTIs);
        else
            oldData = mruiSpectra.Data{1};
            if (ndims(oldData) ~= 12)
                newDataMrui(:,1,1,1,1,1,1,1,:,1,1,1) = oldData;
            else
                newDataMrui(:,1,1,1,1,1,1,1,:,1,1,:) = oldData;
            end
            newDataMrui(:,1,1,1,1,1,1,1,:,1,1,indexTI) = a.Data{1};
            mruiSpectra.Data{1} = newDataMrui;
        end
            
        %% calculations for subjects summed spectra
        %scale the data for equalizing between subjects
       % a.Data{1} = a.Data{1}./ maxWaterPeak;
        
        % eliminate data with too many deleted averages from the summedSpectra
        deletedAverages = a.Parameter.DeleteMovedAveragesSettings.deletedAverages;
        if ~isempty(deletedAverages)
            numDeletedAverages = length(deletedAverages);
        else
            numDeletedAverages = 0;
        end
        
        if numDeletedAverages <= 2 
            indexSubjTIs(indexTI) = indexSubjTIs(indexTI) + 1;
            %create summed spectra
            if(indexSubjTIs(indexTI) == 1)
                summedSpectra{indexTI} = a;
                newData= zeros(length(a.Data{1}),1,1,1,1,1,1,1,2,1,1,numberOfSubjects);
            else
                oldData = summedSpectra{indexTI}.Data{1};
                if (ndims(oldData) ~= 12)
                    newData(:,1,1,1,1,1,1,1,:,1,1,1) = oldData;
                else
                    newData(:,1,1,1,1,1,1,1,:,1,1,:) = oldData;
                end
                newData(:,1,1,1,1,1,1,1,:,1,1,indexSubjTIs(indexTI)) = a.Data{1};
                summedSpectra{indexTI}.Data{1} = newData;
            end
        end
    end

    fileNameMRUI = [subjects{indexSubj} '_mrui'];
    mruiSpectra.ExportMruiText(paths{indexSubj}, fileNameMRUI);
end

%%

% filename = [pathBase, 'Summed_water.mat'];
% save(filename, 'summedWater');
% summedWater = summedWater.AverageData;
% save([pathBase, 'Summed_Averaged_water.mat'], 'summedWater');
% summedWater.ExportLcmRaw(pathBase, 'Summed_Averaged_water', addSinglet0ppm);
for indexTI = 1:numberOfTIs
    filename = [pathBase, 'Summed_spectra_TE' num2str(TIs(indexTI)) '.mat'];
    summedSpectraTE = summedSpectra{indexTI};
    %store only the data, which was kept after eliminating the deleted ones
    actualData = summedSpectraTE.Data{1}./indexSubjTIs(indexTI);
    summedSpectraTE.Data{1} = actualData(:,1,1,1,1,1,1,1,:,1,1,1:indexSubjTIs(indexTI));
    save(filename, 'summedSpectraTE');
    %average across subjects
    summedSpectraTE = summedSpectraTE.AverageData;
    filename = [pathBase, 'Summed_Averaged_TE' num2str(TIs(indexTI)) '.mat'];
    save(filename, 'summedSpectraTE');   
    summedSpectraTE.ExportLcmRaw(pathBase, strcat('Summed_Averaged_TE', num2str(TIs(indexTI))), addSinglet0ppm);
    
    %% create the TE series data
    if(indexTI == 1)
        mruiSpectra = summedSpectraTE;
        newDataMrui= zeros(length(summedSpectraTE.Data{1}),1,1,1,1,1,1,1,2,1,1,numberOfTIs);
    else
        oldData = mruiSpectra.Data{1};
        if (ndims(oldData) ~= 12)
            newDataMrui(:,1,1,1,1,1,1,1,:,1,1,1) = oldData;
        else
            newDataMrui(:,1,1,1,1,1,1,1,:,1,1,:) = oldData;
        end
        newDataMrui(:,1,1,1,1,1,1,1,:,1,1,indexTI) = summedSpectraTE.Data{1};
        mruiSpectra.Data{1} = newDataMrui;
    end
end
%% save MRUI file of summed spectra
% fileNameMRUI = 'SummedAveraged_mrui';
% mruiSpectra.ExportMruiText(pathBase, fileNameMRUI);

summarize_Deleted_Averages(subjects, pathBase, TIs, 'Downfield')
close all
