function this = Sorting( this )

if this.Parameter.ReconFlags.isSorted
    
    error('The data are already sorted!!!');  
else 
    
    NDyns                           = size(this.Data{1},this.dyn_dim);
    nAverages                       = size(this.Data{1},this.meas_dim);
    newDataSize                     = size(this.Data{1});
    newDataSize(this.dyn_dim)       = 1;
    newDataSize(this.extr2_dim)       = 1;
    newDataSize(this.meas_dim)      = NDyns*nAverages;
    newData                         = zeros(newDataSize);
    oldData                         = this.Data{1};
    
    for i = 1:NDyns
       for j = 1: nAverages
           newData(:,:,:,:,:,:,:,:,:,1,:,j+(i-1)*nAverages)  =  oldData(:,:,:,:,:,:,:,:,:,i,:,j);
       end
    end
    
    this.Data{1}                       = newData;
    this.Parameter.ReconFlags.isSorted = true;
end

end

