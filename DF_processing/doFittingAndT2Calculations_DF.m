function doFittingAndT2Calculations_DF(fitAllSubjects)

pathName = 'DF data path';
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
fitAllSubjects = true;
orderTEs = {'24' '32' '40' '52' '60'};
% orderTEs = {'40'};
subjects = {'1658';'1717';'2017';'3373';'3490';'5771';'6249';'6971'; '7338'; '7782';'9810'};

extensionTable = '.table';
extensionCoord = '.coord';
defaultTE = 'TE24';
defaultSubject = 'XXXX';
pH_default_Name = '_pH_XXX';
defaultLCModelUser = 'tborbath';

if fitAllSubjects
    controlFilesBase = 'fitsettings_Subject_newNAA_XXXX_';
    controlFilesBaseTE = strcat(controlFilesBase, 'TE');
    controlFilesBaseSuffix = '_df_withUF_chosen_pH_XXX.control';
    outputFileNameBase = strcat(defaultSubject, pH_default_Name, '_TE');
else
    controlFilesBase = 'fitsettings_Summed_Averaged_';
    controlFilesBaseTE = strcat(controlFilesBase, 'TE');
    controlFilesBaseSuffix = '_df_withUF.control';
    controlFilesBaseSuffix = '_df_pH_XXX.control';
    outputFileNameBase = 'Summed_Averaged_TE';
end


if fitAllSubjects
    %pH_Values per subject
    [~, ~, pH_table] = xlsread([localFilePathBase, 'Output/pH_sweep_final_settings/pH_Results_Subjects.xlsx']);
    index_pH_value = 8;
    if pH_table{1,index_pH_value}~=40
        error('I thought you want to use the pH value of the TE 40 for fitting');
    end
    pH_valuesString = cellstr(num2str(cell2mat(pH_table(3:end,index_pH_value)), '%.2f'));
else
    pH_valuesString = {'7.09'};
end

pH_valuesName = strcat('_pH_', pH_valuesString);

%% file paths setup
controlFilesPathRemote = '/Desktop/DATA_df/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/DATA_df/Output/';
LCModelOutputPath = [localFilePathBase, 'Output/'];
defaultControlFile = strcat(controlFilesBase, 'Default', controlFilesBaseSuffix);

if fitAllSubjects
    subjectsPath = strcat(subjects, '/');
else
    subjects = {'Summed'};
    subjectsPath = {''};
end

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath);
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath);
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath);
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath);

%% basic configurations
LCModelTableFits = strcat(outputFileNameBase, orderTEs, extensionTable);
LCModelTableFitsCoord = strcat(outputFileNameBase, orderTEs, extensionCoord);
LCModelOutputFiles = strcat(outputFileNameBase, orderTEs);
LCModelControlFiles = strcat(controlFilesBaseTE, orderTEs, controlFilesBaseSuffix);

numberOfTes = length(orderTEs);
numberOfSubjects = length(subjects);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

%% create the control file series
if fitAllSubjects
    createLCModelConfigsTEseriesChosen_pHs(controlFilesPathLocal, defaultControlFile, orderTEs, defaultTE, ...
        defaultLCModelUser, LCModelCallerInstance.LCModelUser, subjects, defaultSubject, pH_valuesName, pH_default_Name);
else
    % call the Configuration file creator without subject configuration
    createLCModelConfigsTEseriesChosen_pHs(controlFilesPathLocal, defaultControlFile, orderTEs, defaultTE, ...
        defaultLCModelUser, LCModelCallerInstance.LCModelUser, [], [], pH_valuesName, pH_default_Name);
end

%% load the table with the deleted averages
% load([localFilePathBase, 'tableDeletedAverages.mat'], 'tableDeletedAverages');
tableDeletedAverages = [];

%% do the actual LCModel fitting and T2 calculation
parfor indexCurrentSubject = 1:numberOfSubjects
    %% do the LCModel fitting
    LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);
    
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    if fitAllSubjects
        % replace the name of the subjects
        currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
        % replace also the chosen pH values
        currentControlFiles = strrep(currentControlFiles, pH_default_Name, pH_valuesName{indexCurrentSubject});
        currentOutputFiles = strrep(currentOutputFiles, pH_default_Name, pH_valuesName{indexCurrentSubject});
    else
        currentControlFiles = LCModelControlFiles;
        currentOutputFiles = LCModelOutputFiles;
        % replace also the chosen pH values
        currentControlFiles = strrep(currentControlFiles, pH_default_Name, pH_valuesName{indexCurrentSubject});
        currentOutputFiles = strrep(currentOutputFiles, pH_default_Name, pH_valuesName{indexCurrentSubject});
    end
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
end
for indexCurrentSubject = 1:numberOfSubjects
    if fitAllSubjects
        % replace the name of the subjects
        currentLCModelTableFits = strrep(LCModelTableFits, defaultSubject, subjects{indexCurrentSubject});
        currentLCModelTableFitsCoord = strrep(LCModelTableFitsCoord, defaultSubject, subjects{indexCurrentSubject});
        % replace also the chosen pH values
        currentLCModelTableFits = strrep(currentLCModelTableFits, pH_default_Name, pH_valuesName{indexCurrentSubject});
        currentLCModelTableFitsCoord = strrep(currentLCModelTableFitsCoord, pH_default_Name, pH_valuesName{indexCurrentSubject});
    else
        currentLCModelTableFits = LCModelTableFits;
        currentLCModelTableFitsCoord = LCModelTableFitsCoord;
        % replace also the chosen pH values
        currentLCModelTableFits = strrep(currentLCModelTableFits, pH_default_Name, pH_valuesName{indexCurrentSubject});
        currentLCModelTableFitsCoord = strrep(currentLCModelTableFitsCoord, pH_default_Name, pH_valuesName{indexCurrentSubject});
    end
    %%extract the TEs to eliminate
    eliminateTEs = {};
    if ~isempty(tableDeletedAverages)
        indexTableSubject = find(strcmp(subjects{indexCurrentSubject}, tableDeletedAverages(:,1)), 1);
        %if the index is empty, then we don't eliminate any TE
        if (~isempty(indexTableSubject))
            % we assume that
            for indexTableDeleteTE = 1:numberOfTes
                indexCurrentTE = tableDeletedAverages{indexTableSubject+indexTableDeleteTE-1,2};
                if strcmp(indexCurrentTE,['TE' orderTEs{indexTableDeleteTE}]) == 1
                    deletedAverages = tableDeletedAverages(indexTableSubject+indexTableDeleteTE-1,3:end);
                    deletedAveragesNum = cell2mat(deletedAverages);
                    if (length(deletedAveragesNum) > 2) %TODO check what is best 2 or 0
                        eliminateTEs{end+1} = orderTEs{indexTableDeleteTE};
                    end
                else
                    warning('Subject %s does not have the matching TEs in the table of deleted averages.',subjects{indexCurrentSubject});
                end
            end
        else
            warning('Subject %s not found in the table of deleted averages.',subjects{indexCurrentSubject});
        end
    end
    
    %% calculate the available TEs
    availableTEs = {};
    availableLCModelTableFits = {};
    availableLCModelTableFitsCoord = {};
    indexTE = 0;
    for indexCurrentTE = 1:numberOfTes
        foundTE  = find(strcmp(orderTEs{indexCurrentTE}, eliminateTEs), 1);
        if isempty(foundTE)
            indexTE = indexTE +1;
            availableTEs{indexTE} = orderTEs{indexCurrentTE};
            availableLCModelTableFits{indexTE} = currentLCModelTableFits{indexCurrentTE};
            availableLCModelTableFitsCoord{indexTE} = currentLCModelTableFitsCoord{indexCurrentTE};
        end
    end
    numOfAvailableTEs = length(availableTEs);
    %% do the T2 exponential fits
    [tableConcentrations{indexCurrentSubject}, tableT2s{indexCurrentSubject}] = calculate_T2(availableLCModelTableFits, ...
        subjectsLCModelOutputPath{indexCurrentSubject}, true, availableTEs, 'DF');
    
    %% mean Rsquared
    Rsquares = str2double(tableT2s{indexCurrentSubject}(3,2:end));
    meanRsquared =  mean(Rsquares);
    
    %% mean CRLB
    CRLBs = cell2mat((tableConcentrations{indexCurrentSubject}(2:numberOfTes + 1,3:3:end)));
    meanCrlbTEs = mean(CRLBs,2, 'omitnan');
    meanCrlb = mean(meanCrlbTEs);
    
    %% T2 of Cr
    metNames = tableT2s{indexCurrentSubject}(1,2:end);
    indexes = strfind(metNames,'NAA Broad');
    indexCr = find(not(cellfun('isempty', indexes))) + 1;
    T2Cr = tableT2s{indexCurrentSubject}(2,indexCr);
    
    %% display means
    table(1,1) = {'TE'};
    table(1,2:numOfAvailableTEs+1) = availableTEs(1:end);
    table(2,1) = {'CRLB'};
    table(2,2:numOfAvailableTEs+1) = num2cell(meanCrlbTEs(1:end));
    table(3,1) = {'meanCRLB'};
    table(3,2) = num2cell(meanCrlb);
    table(4,1) = {'meanR2'};
    table(4,2) = num2cell(meanRsquared);
    table(5,1) = {'T2_NAA'};
    table(5,2) = T2Cr;
    display(table)
    
    %% save table to file
    exportFileXlsx = [LCModelOutputPath ,'Quantification_Summaries.xlsx'];
    startIndex = 1+ 7 *(indexCurrentSubject-1);
    cellPositionTitle = ['A' num2str(startIndex)];
    xlswrite(exportFileXlsx, {['Subject: ' subjects{indexCurrentSubject}]}, 1, cellPositionTitle);
    cellPosition = ['A' num2str(startIndex + 1)];
    xlswrite(exportFileXlsx, table, 1, cellPosition);
    
    %% calculate FWHMs
    if indexCurrentSubject == 1
        [fwhms, orderMetFWHM] =calculate_FWHM(currentLCModelTableFitsCoord, subjectsLCModelOutputPath{indexCurrentSubject}, false, 'DF');
        tableFWHM = zeros(numberOfSubjects, size(fwhms,1), size(fwhms,2));
        tableFWHM(indexCurrentSubject,:,:) = fwhms;
    else
        tableFWHM(indexCurrentSubject,:,:) = calculate_FWHM(currentLCModelTableFitsCoord, subjectsLCModelOutputPath{indexCurrentSubject}, false, 'DF');
    end
end

if fitAllSubjects
    fileNameSuffix = 'Subjects';
else
    fileNameSuffix = 'Summed';
end
tableFWHM_name = 'tableFWHM';
tableT2s_name = 'tableT2s';
orderMet_name = 'orderMetFWHM';
tableConcentrations_name = 'tableConcentrations';
tableFWHM_nameSuffixed = [tableFWHM_name fileNameSuffix];
tableT2s_nameSuffixed = [tableT2s_name fileNameSuffix];
orderMet_nameSuffixed = [orderMet_name fileNameSuffix];
tableConcentrations_nameSuffixed = [tableConcentrations_name fileNameSuffix];
eval( [tableFWHM_nameSuffixed, '=',  tableFWHM_name]);
eval( [tableT2s_nameSuffixed, '=',  tableT2s_name]);
eval( [orderMet_nameSuffixed, '=',  orderMet_name]);
eval( [tableConcentrations_nameSuffixed, '=',  tableConcentrations_name]);
save([LCModelOutputPath, tableFWHM_nameSuffixed, '.mat'], tableFWHM_nameSuffixed, orderMet_nameSuffixed);
save([LCModelOutputPath, tableT2s_nameSuffixed, '.mat'], tableT2s_nameSuffixed)
save([LCModelOutputPath, tableConcentrations_nameSuffixed, '.mat'], tableConcentrations_nameSuffixed)
end