function [ppm] = freq_ppm(peakSpectrum, ppmStart, ppmEnd, dataPoints, scanFrequency, interpFactor)
warningFlag = false;
if nargin < 3
    error('The function fwhm_Hz requires minimum 2 input parameters. The parameters are: peakSpectrum, bandwidth, dataPoints, interpFactor')
end
if (~exist('interpFactor', 'var'))
    interpFactor  = 20; %default value
end

indeces       = [];
i = 1;

while (length(indeces)<2 && i <4)
    %create a temporary interpolated peak
    peakTmp         = interp(peakSpectrum,interpFactor*i);
    %get the position of the maximum
    [~,  maxPos] = max(abs(peakTmp));
    %iterate again if not enough precise
    i            = i+1;
end

interpFactor    = interpFactor*(i-1);

factor = (ppmStart - ppmEnd) / (dataPoints * interpFactor) ;

if maxPos < length(peakTmp)/2
    ppm = (length(peakTmp)/2-maxPos)*factor;
elseif maxPos > length(peakTmp)/2
    ppm = -(maxPos-length(peakTmp)/2)*factor;
end
end
    
