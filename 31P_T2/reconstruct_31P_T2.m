function reconstruct_31P_T2(currentFile,TEs,prefixes,subjects,paths,metaboliteNames,centerPpm,trunc_ms,indexTE,indexSubject)
currentData   = MR_spectroS(currentFile);
currentData.Parameter.PhaseCorr31PSettings.sing = [0];

% somehow only 11-D complex double, make it 12-D that it is consistent with MR_spectroS
if length(size(currentData.Data{1})) == 11
    oldData             = currentData.Data{1};
    permData            = double(permute(oldData,[1 2 3 4 5 6 7 8 9 10 12 11]));
    currentData.Data{1} = permData;
    clear oldData permData
end

% remove 1H channels
currentData = currentData.DeleteCoilChannels(1:10);

% average data
currentData = currentData.AverageData();

% coil combination (strong Gaussian filtering to gain enough signal
% for svd on PCr
oldData = currentData.Data{1};
oldData = squeeze(oldData);
dwellTime   = currentData.Parameter.Headers.DwellTimeSig_ns * 1e-9; %in sec
t = (0:length(oldData)-1)*dwellTime;
nCoils        = size(currentData.Data{1},currentData.coil_dim);
Fgau = 100;
GauFilter = exp ( - t'.^2*(Fgau)^2 );
for idxOfCh = 1:nCoils
    oldDataFiltered(:,idxOfCh) = oldData(:,idxOfCh).*GauFilter;
end
[~,~,V] = svd(oldDataFiltered(:,:)'*oldDataFiltered(:,:));
weights = V(:,1);
comb = oldData(:,:)*weights;
currentData.Data{1}                                = comb;
currentData.Parameter.ReconFlags.isCoilsCombined = true;

% phase zero correction
currentData = currentData.Phase0_correction_31P;


%% needed?
% apply the previously calculated values
% this.Parameter.MissingPointPredictionSettings = this2.Parameter.MissingPointPredictionSettings;
%         this.Parameter.MissingPointPredictionSettings.imiss = 8;
%         this.Parameter.MissingPointPredictionSettings.T_factor = 2;
%         this.Parameter.MissingPointPredictionSettings.polyOrder = 18;
%         % this.Parameter.Phase0RemovalSettings.phase0 = this2.Parameter.Phase0RemovalSettings.phase0;
%         if this.Parameter.Phase0RemovalSettings.phase0 ~= 0
%             phase0 = this.Parameter.Phase0RemovalSettings.phase0;
%             this.Data{1} = this.Data{1}* exp(-1i*phase0);
%         end
%         % this.Parameter.PhaseCorrSettings.phase0 = this2.Parameter.PhaseCorrSettings.phase0;
%         this = this.PhaseCorrection;
%         this_noP1corr = this;
%         % save noP1corr, just p0
%         fileName = [this_noP1corr.Parameter.Filename(1:end-4) '_noP1corr.mat'];
%         %     save(fileName, 'this_noP1corr');
%         this.Parameter.PhaseCorrSettings.phase0 = 0;
%         this.Parameter.PhaseCorrSettings.phase1 = 0.117;
%         this = this.PhaseCorrection;
%         if this.Parameter.MissingPointPredictionSettings.polyOrder ~= 60
%             this = this.MissingPointPrediction;
%         end



% FrequencyAlign PCr to 0
currentData.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = [0];
currentData = currentData.FrequencyAlignFreqDomain;


%% quick look at data, no truncation
nTimepoints     = size(currentData.Data{1},currentData.kx_dim);
bandwidth       = currentData.Parameter.Headers.Bandwidth_Hz;
freq    = bandwidth/nTimepoints*(-(nTimepoints/2-1):1:nTimepoints/2)';
time    = 1/bandwidth*(0:nTimepoints-1)'*1000;      %ms
scanFreq_MHz = currentData.Parameter.Headers.ScanFrequency/1E6;

figure
%         plot(freq/scanFreq_MHz,(real(fftshift(fft(currentData.Data{1}))))/max(real(fftshift(fft(currentData.Data{1})))))
plot(freq/scanFreq_MHz,(real(fftshift(fft(currentData.Data{1})))))
axis([-20 8 -inf inf])
set(gca,'xdir','reverse')
xlabel 'f / ppm'
title (['TE ', num2str(TEs{indexTE}), ' ms  ', prefixes{indexSubject}, subjects{indexSubject}, ' no trunc'], 'Interpreter', 'none')
fig2save = fullfile(paths{indexSubject}, strcat('TE', num2str(TEs{indexTE}), 'ms_noTrunc', '.fig'));
savefig(fig2save)

% save this file
data2save_noTrunc = fullfile(paths{indexSubject}, strcat(prefixes{indexSubject}, subjects{indexSubject}, '_TE', num2str(TEs{indexTE}), 'ms_noTrunc.mat'));
save(data2save_noTrunc, 'currentData')

% SNR in frequency domain, no filtering or truncation
Data = currentData.Data{1};
Data = fftshift(fft(Data));
searchArea = 0.2; %ppm
snrValue = zeros(length(metaboliteNames),1);
for iMetabolite = 1:length(metaboliteNames)
    ppmStartMet = round(nTimepoints/2 + (centerPpm(iMetabolite) + searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
    ppmEndMet = round(nTimepoints/2 + (centerPpm(iMetabolite) - searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
    snrValue(iMetabolite) = max( abs(Data(ppmEndMet:ppmStartMet)))/std(Data(((size(Data))-size(Data)*1/4):end));
end

% FWHM, no filtering or truncation
ppmStart = bandwidth/scanFreq_MHz/2; ppmEnd = -bandwidth/scanFreq_MHz/2;
searchArea = 0.2; %ppm
ppmVector = freq/scanFreq_MHz;
fwhm = zeros(length(metaboliteNames),1);
for iMetabolite = 1:length(metaboliteNames)
    ppmStartMet = centerPpm(iMetabolite) + searchArea;
    ppmEndMet = centerPpm(iMetabolite) - searchArea;
    spectrumPeak = Data;
    spectrumPeak(ppmVector>ppmStartMet | ppmVector < ppmEndMet) = 0;
    [fwhm(iMetabolite), ~] = fwhm_Hz(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
end

% LCModel Export
currentData.Parameter.ReconFlags.isCoilsCombined = 1;
currentData.Parameter.ReconFlags.isAveraged = 1;
filename = fullfile(paths{indexSubject}, strcat(prefixes{indexSubject}, subjects{indexSubject}, '_TE', num2str(TEs{indexTE}), 'ms_noTrunc'));
currentData.ExportLcmRaw('',filename, true, 'Yes', 15, 0.05);
%% truncation after trunc_ms
truncPoint = floor( trunc_ms/(currentData.Parameter.Headers.DwellTimeSig_ns*1e-6));
currentDataTrunc = currentData.Truncate(truncPoint);
% phase zero correction again
currentDataTrunc = currentDataTrunc.Phase0_correction_31P;

figure
plot(freq/scanFreq_MHz,(real(fftshift(fft(currentDataTrunc.Data{1})))))
axis([-20 8 -inf inf])
set(gca,'xdir','reverse')
xlabel 'f / ppm'
title (['TE ', num2str(TEs{indexTE}), ' ms  ', prefixes{indexSubject}, subjects{indexSubject}, ' trunc ', num2str(trunc_ms), ' ms'], 'Interpreter', 'none')
fig2save_Trunc = fullfile(paths{indexSubject}, strcat('TE', num2str(TEs{indexTE}), 'ms_Trunc', num2str(trunc_ms), 'ms', '.fig'));
savefig(fig2save_Trunc)

% save this file
data2save_Trunc = fullfile(paths{indexSubject}, strcat(prefixes{indexSubject}, subjects{indexSubject}, '_TE', num2str(TEs{indexTE}), 'ms_Trunc',num2str(trunc_ms),'ms.mat'));
save(data2save_Trunc, 'currentDataTrunc')

% SNR in frequency domain, truncation after trunc_ms
DataTrunc = currentDataTrunc.Data{1};
DataTrunc = fftshift(fft(DataTrunc));
searchArea = 0.2; %ppm
snrValueTrunc = zeros(length(metaboliteNames),1);
for iMetabolite = 1:length(metaboliteNames)
    ppmStartMet = round(nTimepoints/2 + (centerPpm(iMetabolite) + searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
    ppmEndMet = round(nTimepoints/2 + (centerPpm(iMetabolite) - searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
    snrValueTrunc(iMetabolite) = max( abs(DataTrunc(ppmEndMet:ppmStartMet)))/std(DataTrunc(((size(DataTrunc))-size(DataTrunc)*1/4):end));
end

% FWHM, truncation after trunc_ms
ppmStart = bandwidth/scanFreq_MHz/2; ppmEnd = -bandwidth/scanFreq_MHz/2;
searchArea = 0.2; %ppm
ppmVector = freq/scanFreq_MHz;
fwhmTrunc = zeros(length(metaboliteNames),1);
for iMetabolite = 1:length(metaboliteNames)
    ppmStartMet = centerPpm(iMetabolite) + searchArea;
    ppmEndMet = centerPpm(iMetabolite) - searchArea;
    spectrumPeak = DataTrunc;
    spectrumPeak(ppmVector>ppmStartMet | ppmVector < ppmEndMet) = 0;
    [fwhmTrunc(iMetabolite), ~] = fwhm_Hz(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
end

% LCModel Export
currentDataTrunc.Parameter.ReconFlags.isCoilsCombined = 1;
currentDataTrunc.Parameter.ReconFlags.isAveraged = 1;
filenameTrunc = fullfile(paths{indexSubject}, strcat(prefixes{indexSubject}, subjects{indexSubject}, '_TE', num2str(TEs{indexTE}), 'ms_Trunc', num2str(trunc_ms), 'ms'));
currentDataTrunc.ExportLcmRaw('',filenameTrunc, true, 'Yes', 15, 0.05);
%% save SNR table
snrValues = [snrValue(1) snrValue(2) snrValue(3) snrValue(4) snrValue(5) snrValue(6) snrValue(7) snrValue(8) snrValue(9) ;...
    fwhm(1) fwhm(2) fwhm(3) fwhm(4) fwhm(5) fwhm(6) fwhm(7) fwhm(8) fwhm(9) ;...
    snrValueTrunc(1) snrValueTrunc(2) snrValueTrunc(3) snrValueTrunc(4) snrValueTrunc(5) snrValueTrunc(6) snrValueTrunc(7) snrValueTrunc(8) snrValueTrunc(9);...
    fwhmTrunc(1) fwhmTrunc(2) fwhmTrunc(3) fwhmTrunc(4) fwhmTrunc(5) fwhmTrunc(6) fwhmTrunc(7) fwhmTrunc(8) fwhmTrunc(9) ];
rowNames = {'SNR','FWHM','SNR Truncated','FWHM Truncated'};
colNames = {'PE','PC','Piint','GPE','GPC','PCr','g_ATP','a_ATP','NADH'};
SNR = array2table(snrValues,'RowNames',rowNames,'VariableNames',colNames)

fileName = fullfile(paths{indexSubject}, strcat('TE', num2str(TEs{indexTE}), 'ms_SNR'));
save(fileName, 'SNR');

end