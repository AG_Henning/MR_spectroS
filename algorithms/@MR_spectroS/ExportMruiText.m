function ExportMruiText(this, path, name, nucleus)

% Define the fileptath where the MRUI .txt file will be saved
if nargin > 1
    filepath=path;
else
    filepath=pwd;
end
if nargin > 2
    filename = name;
else
    filename = '';
end
if nargin > 3
    selNuc = nucleus;
else
    selNuc = '1H';
end


% Read the data
data             =  this.Data{1};
NCol             =  size( data, this.kx_dim);   % # timepoints
NMeas            =  size( data, this.meas_dim); % # Averages
%NCha            =  size( data, this.coil_dim); % # Channels
%NRep            =  size( data, this.dyn_dim);  % # Repetitions
%NMix            =  size( data, this.mix_dim);  % # Mixes
isCoilsCombined =  this.Parameter.ReconFlags.isCoilsCombined;

if ~isCoilsCombined
    error('The data has to be combined first!!!')
end


%open the file where the .RAW data will be exported

PatientName = get(this.Parameter.Headers,'PatientName');
PatientID   = get(this.Parameter.Headers,'PatientID');
PatientID  = num2str(PatientID);
if strfind(PatientID,':') % if the ID is not something usable, replace it
    PatientID = PatientName;
    PatientID  = num2str(PatientID);
end
Sequence    = get(this.Parameter.Headers,'SequenceDescription');

dyn_char=   strcat( '_',num2str(NMeas),'Averages');
if isunix
    slash='/';
else
    slash='\';
end
if strcmp(filename,'')
    filename = strcat(PatientID, '_', Sequence, dyn_char , '.txt');
    filenameFull = strcat(filepath, slash, filename);
else
    filenameFull = strcat(filepath, filename, '.txt');
end
fileID=     fopen(filenameFull,'w');

dwellTime =  this.Parameter.Headers.DwellTimeSig_ns *1e-6; %ms



%Read the values will be later written in the .txt namelists
TE=          get(this.Parameter.Headers,'TE_us')/1E3; %in ms
transmitterFreq = get(this.Parameter.Headers,'ScanFrequency');
sequenceDescription = get(this.Parameter.Headers, 'SequenceDescription');
if strcmp(selNuc,'1H')
    GyroMagneticRatio = 42.57747892;
elseif strcmp(selNuc,'31P')
    GyroMagneticRatio = 17.235;
end

% Write Header
fprintf(fileID, 'jMRUI Data Textfile\n\n');
fprintf(fileID, 'Filename: %s\n\n', filename);
fprintf(fileID, 'PointsInDataset: %d\n', NCol);
fprintf(fileID, 'DatasetsInFile: %d\n', NMeas); 
printExponentialMRUI(fileID, 'SamplingInterval: ', dwellTime);
printExponentialMRUI(fileID, 'ZeroOrderPhase: ', 0); %%
printExponentialMRUI(fileID, 'BeginTime: ', 0); %%
printExponentialMRUI(fileID, 'TransmitterFrequency: ', transmitterFreq);
printExponentialMRUI(fileID, 'MagneticField: ', transmitterFreq/GyroMagneticRatio * 1e-6);
printExponentialMRUI(fileID, 'TypeOfNucleus: ', 0E0); %should stand for proton
fprintf(fileID, 'NameOfPatient: %s\n', PatientID);
fprintf(fileID, 'DateOfExperiment: %s\n', 20010101); %%
%     fprintf(fileID, 'Spectrometer: %s\n', ''); %%
fprintf(fileID, 'AdditionalInfo: %s\n', '');
fprintf(fileID, 'SignalNames: %s\n', sequenceDescription);
fprintf(fileID, '\n\n');

fprintf(fileID, 'Signal and FFT\n');
fprintf(fileID, 'sig(real)	sig(imag)	fft(real)	fft(imag)\n');


%%write the data in real, imaginary pairs
for indexOfMeas=1:NMeas %create one RAW file for each dynamic... will be changed in the next days
    fprintf(fileID, 'Signal %d out of %d in file\n', indexOfMeas, NMeas);
    spectrum = fftshift(fft(data(:,1,1,1,1,1,1,1,1,1,1,indexOfMeas)));
    for n_point=1:NCol
        
        %data(:,1,1,ncha,1,1,1,1,nmix,nrep,1,nmeas)=data(:,1,1,ncha,1,1,1,1,nmix,nrep,1,nmeas)
        timePoint= data(n_point,1,1,1,1,1,1,1,1,1,1,indexOfMeas);
        spectralPoint = spectrum(n_point);
        fprintf(fileID,'%.4E\t%.4E\t%.4E\t%.4E\n', real(timePoint), imag(timePoint), real(spectralPoint), imag(spectralPoint));
    end
end
clc
fclose(fileID);
display('Export...FINISH.');

end


function printExponentialMRUI(fileID, description, value)
    tmpString =  sprintf('%.3E\n',value);
    tmpString = strrep(tmpString, 'E+0','E');
    tmpString = strrep(tmpString, 'E-0','E-');
    fprintf(fileID, [description, tmpString]);
end