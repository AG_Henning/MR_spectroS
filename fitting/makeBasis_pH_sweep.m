function makeBasis_pH_sweep
pathName = 'DF data path';
sampleCriteria = {'Basis_sets'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
orderTEs = {'24' '32' '40' '52' '60'};

defaultTE = '24';
defaultLCModelUser = 'tborbath';


pH_values = [6.9:0.01:7.1]';
pH_values = [7.1:0.01:7.15]';
pH_values = [7.0:0.01:7.09]';
% pH_values = [7.0:0.01:7.15]';
pH_values = [6.9:0.01:7.15]';
% pH_values = [7.09]';
pH_valuesSting = cellstr(num2str(pH_values, '%.2f'));
pH_default = '7.00';
pH_default_Name = '_pH_XXX';

moietyFit = true;
imidazoleFit = true;
if moietyFit
    if imidazoleFit
        %DF
        makeBasisFilesBase = 'sLASER_7ppm_moiety_imidazole_';
        makeBasisFilesBase = 'sLASER_7ppm_moiety_imidazole_newNAA_';
%         makeBasisFilesBase = 'deGraaf_sLASER_7ppm_moiety_imidazole_';
    else
        %DF
        makeBasisFilesBase = 'sLASER_7ppm_moiety_full_';
    end
else
    if imidazoleFit
        makeBasisFilesBase = 'sLASER_7ppm_abs_quantif_imidazole_';
    else
        makeBasisFilesBase = 'sLASER_7ppm_abs_quantif_';
    end
end

%%UF fitsettings
makeBasisFilesBaseSuffix = '_make_basis.in';

%% file paths setup

%DF
makeBasisFilesPathRemote = '/Desktop/DATA_df/Basis_sets/pH_sweep/';
makeBasisFilesPathLocal =  [localFilePathBase, 'Basis_sets/pH_sweep/'];

if imidazoleFit
LCModelBasisOutputFilesPathRemote = '/Desktop/DATA_df/Basis_sets/pH_sweep/NewBasisImidazole_deGraaf/';
LCModelBasisOutputPath = [localFilePathBase, 'Basis_sets/pH_sweep/NewBasisImidazole_deGraaf/'];
else
LCModelBasisOutputFilesPathRemote = '/Desktop/DATA_df/Basis_sets/pH_sweep/BasisFull/';
LCModelBasisOutputPath = [localFilePathBase, 'Basis_sets/pH_sweep/BasisFull/'];
end

defaultMakeBasisFile = strcat(makeBasisFilesBase, 'Default', pH_default_Name, makeBasisFilesBaseSuffix);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(makeBasisFilesPathRemote, LCModelBasisOutputFilesPathRemote, LCModelBasisOutputPath);

%% create the make basis file series
[makeBasisFilesTE, basisFileNamesTE] = createLCModelMakeBasisTEseries(makeBasisFilesPathLocal, defaultMakeBasisFile, orderTEs, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser);

numberOfTEs = length(orderTEs);
numberOf_pH = length(pH_values);
makeBasisFilesTE_pH = cell(1,numberOfTEs);
basisFileNamesTE_pH = cell(1,numberOfTEs);
for indexTE = 1:numberOfTEs
    [makeBasisFilesTE_pH{indexTE}, basisFileNamesTE_pH{indexTE}] = createLCModelMakeBasis_pH_sweep(...
        makeBasisFilesPathLocal, makeBasisFilesTE{indexTE}, pH_valuesSting, pH_default, ...
        defaultLCModelUser, LCModelCallerInstance.LCModelUser);
end

% do LCModel stuff
copyFileLocally = true;
parfor indexTE = 1:numberOfTEs
    %% do the LCModel fitting
    LCModelCallerInstance = startLCModel(makeBasisFilesPathRemote, LCModelBasisOutputFilesPathRemote, LCModelBasisOutputPath);

    for index_pH = 1:numberOf_pH
        currentMakeBasisFile = makeBasisFilesTE_pH{indexTE}{index_pH};
        currentBasisFileName = basisFileNamesTE_pH{indexTE}{index_pH};
        LCModelCallerInstance.CreateBasisSet(makeBasisFilesPathLocal, makeBasisFilesPathRemote, currentMakeBasisFile, copyFileLocally, currentBasisFileName);
    end
end

fclose all;