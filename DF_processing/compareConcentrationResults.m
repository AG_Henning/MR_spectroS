function compareConcentrationResults()

outputFolder = 'Output\pH_sweep\';
outputFolderRef = 'Output_UF\';
pathName = 'DF data path';
sampleCriteria = {outputFolder, outputFolderRef};
data_path = pathToDataFolder(pathName, sampleCriteria);
pH_values = [6.9:0.01:7.1]';
doSubjects = true;

if doSubjects
    subjects = {'1658';'1717';'2017';'3373';'3490';'5771';'6249';'6971'; '7338'; '7782';'9810'};
else
    subjects = {'Summed'};
end

echoTimes = {'24', '32', '40', '52', '60'};
%metaboliteNameOrder = {'Asp';'Cr-CH3';'Cr-CH2';'Glu';'Gln';'GSH';'Glyc';'mI';'NAA-asp';'NAA-ace';'NAAG';'tCho_PE';'tNAA+';'mI+Glyc';'Glx';};%'Leu';'GABA';'Scyllo';'Tau'};'tNAA';
metaboliteNameOrder = {'Asp';'Cr';'Cr_CH2';'Glu';'Gln';'GSH';'mI';'NAA_as';'NAA_ac';'tCho_P';'mI+Glyc';'Glu+Gln';};%;'Glyc';'NAAG';'GABA';'Scyllo';'Tau'
        
numberOfTEs = length(echoTimes);
numberOfSubjects =  length(subjects);
numberOfMetabolites = length(metaboliteNameOrder);

index_pH_value = 16; %7.05;
current_pH_value = 7.05;% pH_values(index_pH_value);

concentrationDifferences = zeros(numberOfSubjects, numberOfTEs);
concentrationNans = zeros(numberOfSubjects, numberOfTEs);
concentrationDifferenceMean  = 0;
concentrations = zeros(numberOfSubjects * numberOfTEs, numberOfMetabolites);
concentrationsRef = zeros(numberOfSubjects * numberOfTEs, numberOfMetabolites);

for indexSubject = 1:numberOfSubjects
    currentSubject = subjects{indexSubject};
    load([data_path, outputFolder, currentSubject, '\tableConcentrations_', currentSubject, '.mat'], 'tableConcentrations', 'pH_values');
    load([data_path, outputFolderRef, 'tableConcentrationsSubjects.mat'], 'tableConcentrationsSubjects');
    index_pH_value = find(pH_values==current_pH_value);
    currentConcentrations = tableConcentrations{indexSubject,index_pH_value};
    currentConcentrationsRef = tableConcentrationsSubjects{indexSubject};
    for indexMetabolite = 1:numberOfMetabolites
        currentMetabolite = metaboliteNameOrder(indexMetabolite);
        indexMet    = find(strcmp(currentConcentrations(1,:), currentMetabolite), 1, 'last');
        indexMetRef = find(strcmp(currentConcentrationsRef(1,:), currentMetabolite), 1);
        for indexTE = 1:numberOfTEs
            metConcDifferencePercent = (currentConcentrations{indexTE+1, indexMet} - currentConcentrationsRef{indexTE+1, indexMetRef})./currentConcentrations{indexTE+1, indexMet} * 100;
            currentIndex = (indexSubject-1) * numberOfTEs + indexTE;
            concentrations(currentIndex, indexMetabolite) = currentConcentrations{indexTE+1, indexMet};
            concentrationsRef(currentIndex, indexMetabolite) = currentConcentrationsRef{indexTE+1, indexMetRef};
            if ~isnan(metConcDifferencePercent)
                concentrationDifferences(indexSubject,indexTE) = concentrationDifferences(indexSubject,indexTE) + abs(metConcDifferencePercent);
            else
                concentrationNans(indexSubject,indexTE) = concentrationNans(indexSubject,indexTE) + 1;
            end
        end
    end
end

concentrationDifferenceMean = mean(mean(concentrationDifferences./(numberOfMetabolites-concentrationNans)))

tablePValues = {};
for indexMetabolite = 1:numberOfMetabolites
    currentMetabolite = metaboliteNameOrder(indexMetabolite);
    pValue = signrank(concentrations(:,indexMetabolite), concentrationsRef(:,indexMetabolite));
%     pValue = ranksum(concentrations(:,indexMetabolite), concentrationsRef(:,indexMetabolite));
    tablePValues{1,1+indexMetabolite}=currentMetabolite{1};
    tablePValues{2,1+indexMetabolite}=pValue;
    figure
    boxplot([concentrations(:,indexMetabolite), concentrationsRef(:,indexMetabolite)])
%     hold on
%     for index = 1:size(concentrations, 1)
%         plot([concentrations(index,indexMetabolite), concentrationsRef(index,indexMetabolite)])
%     end
    title(currentMetabolite)
end
tablePValues
