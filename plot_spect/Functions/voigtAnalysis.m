function voigtAnalysis(handles)

% Current MR_SpectroS object
this = handles.MRSpectroMatrix.getCurrent;
data = this.Data{1};
SelectedProcessingValues = handles.SelectedProcessingValues; 




%Selected Display Values
selectedCoil      = get(handles.SelectedDisplayValues, 'selectedCoil')       ;
selectedDyn       = get(handles.SelectedDisplayValues, 'selectedDyn')        ;
selectedMix       = get(handles.SelectedDisplayValues, 'selectedMix')        ;
selectedAveraging = get(handles.SelectedDisplayValues, 'selectedAveraging')  ;
selectedAverage   = get(handles.SelectedDisplayValues, 'selectedAverage')    ;
range             = get(handles.SelectedDisplayValues, 'selectedAveRange')   ;
bandwidth         = this.Parameter.Headers.Bandwidth_Hz; 
if ~isempty(range)
 range             = str2num(range)                                          ;
end

clear this  handles; 

% Selected displayed data
if selectedAveraging
   
    data = squeeze(sum(data(:,1,1,selectedCoil,1,1,1,1,selectedMix,selectedDyn,1,range(1):range(2)),12));
else
    
    data = squeeze(data(:,1,1,selectedCoil,1,1,1,1,selectedMix,selectedDyn,1,selectedAverage));
end

clear selectedDyn selectedCoil selectedMix selectedAverage range selectedAveraging



 lineWidthValueInitial   = linewidth(data, SelectedProcessingValues,bandwidth); 
 % snr calculation
 %snrValueInitial         = snr(data, SelectedProcessingValues)*lineWidthValueInitial;
 snrValueInitial          = (snr(data, SelectedProcessingValues));
 
 
 % Iterative calculation of SNR and Linewidth for different Voigt filters
 % range for calculations
 gaussRange      = 50:100; 
 lorenzRange     = 0:0.2:8;
 
 
 SNRMtx       =  zeros(length(gaussRange),length(lorenzRange));
 LineWidthMtx =  zeros( size(SNRMtx ) );
 
k = 1;
 for i = 1: length(gaussRange)
     for j = 1: length(lorenzRange)
          clc
          display(strcat( num2str(100*k/(length(gaussRange)*length(lorenzRange)))),'%')
          
          gaussFilter  = gaussRange(i)/(1/bandwidth*size(data,1)*1e+3);
          gaussPart=(exp(-linspace(0,1,length(data)).^2/(2*gaussFilter^2))');

          expFilter    = lorenzRange(j)*pi;
          expPart=(exp(((0:length(data)-1)*(1/bandwidth))*expFilter)');
          
          
          tmpData           = data.* expPart.* gaussPart;
          LineWidthMtx(i,j) = linewidth(tmpData, SelectedProcessingValues,bandwidth);  
          %SNRMtx(i,j)       = snr(tmpData, SelectedProcessingValues) *LineWidthMtx(i,j);
          SNRMtx(i,j)      = (snr(tmpData, SelectedProcessingValues));
          k=k+1;
     end
 end
 
 LineWidthMtx = LineWidthMtx - lineWidthValueInitial;
 SNRMtx       = (SNRMtx - snrValueInitial)/snrValueInitial * 100; % in (%)
%  SNRMtx(find (SNRMtx> 40))   = 50;
%  SNRMtx(find (SNRMtx < -10)) = -15;
 
 figure 
 
 
 subplot(2,2,1)
 %surf(gaussRange,lorenzRange,LineWidthMtx')
 imagesc(gaussRange,lorenzRange,LineWidthMtx')
 xlabel('Gauss Filter(ms)' );
 ylabel('Lorenz Filter (Hz)');
 title('Linewidth change')
 shading interp;
 colorbar

subplot(2,2,2)
 imagesc(gaussRange,lorenzRange,SNRMtx')
 xlabel('Gauss Filter(ms)' );
 ylabel('Lorenz Filter (Hz)');
 title('SNR change (%)')
 shading interp;
 colorbar
 


%% 
 

 findOptimum = (SNRMtx > 15).* (SNRMtx <100 ).* (LineWidthMtx <= -2 ) ;

subplot(2,2,[3 4])
imagesc(gaussRange,lorenzRange,findOptimum'); 
 xlabel('Gauss Filter(ms)' );
 ylabel('Lorenz Filter (Hz)');
 title('Optimum')
 

