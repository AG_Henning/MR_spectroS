function this    = DeleteFilesFromRemote( this, remoteDirectory, fileNames)
% delete the given files (fileNames should be a cell array. 
for index = 1:length(fileNames)
    [this.connection, res] = sshfrommatlabissue(this.connection, ['rm ' this.homePath remoteDirectory fileNames{index}]);
end

end