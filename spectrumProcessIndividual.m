function spectrumProcessIndividual()

%% --------------------- Preconfigure ----------------------------------------------------------------------------------------------------------- 
pathName = 'Individual data path';
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);

individualPath = 'Individual/';
preprocessedFilesPathLocal = strcat(localFilePathBase, individualPath);

%% --------------------- Select Raw data --------------------------------------------------------------------------------------------------------
[fileNameSpectrum, path] = uigetfile('*.dat', 'Select spectrum');
[~, fileBaseSpectrum, ~] = fileparts(fileNameSpectrum);
waterFileName = ls([path, '*_scout_wref_*.dat']);
[~, fileBaseWater, ~] = fileparts(waterFileName);
addSinglet0ppm = 'Yes';
filesPath = [path fileNameSpectrum];
waterFilesPath = [path waterFileName];

%% --------------------- Reconstruct data ------------------------------------------------------------------------------------------------------
%% water data
[a_water] = reconstructWater(waterFilesPath);
filename = [preprocessedFilesPathLocal fileBaseWater '.mat'];
save(filename, 'a_water');
a_water.ExportLcmRaw(preprocessedFilesPathLocal, fileBaseWater, addSinglet0ppm);
%% reconstruct spectral data
isMC = true;
isInVivo = true;
% a=MR_spectroS(filesPath);
% plot_spect(a)
a = reconstruct(filesPath,isMC, isInVivo);
% plot_spect(a)
filename = [preprocessedFilesPathLocal fileBaseSpectrum '.mat'];
save(filename, 'a');
a.ExportLcmRaw(preprocessedFilesPathLocal, strcat(fileBaseSpectrum), addSinglet0ppm);

close all
%% --------------------- Fitting --------------------------------------------------------------------------------------------------------------- 
TE_ms = a.Parameter.Headers.TE_us * 1e-3;
isSemiLASER = ~isempty(strfind(a.Parameter.Headers.SequenceDescription, 'semiLASER'));
isMMBaseline = ~isempty(strfind(a.Parameter.Headers.SequenceDescription, 'MMBaseline'));
if isSemiLASER
	sequence = 'sLASER';
else
	error('sequence not known');
end
currentTE = num2str(TE_ms);

extensionTable = '.table';
extensionCoord = '.coord';
defaultTE = 'TE24';
defaultLCModelUser = 'tborbath@mpi.localnet';

defaultSpectrum = 'Spectrum';
defaultWaterRef = 'Water_Ref';

%% file paths setup
remotePathBase = '/Desktop/IndividualFits/';
controlFilesPathRemote = [remotePathBase 'LCModelConfig/'];
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = [remotePathBase 'Output/'];
LCModelOutputPath = [localFilePathBase, 'Output/'];
if isSemiLASER 
    if isMMBaseline
        defaultControlFile = 'fitsettings_MMBaseline_Lopez_all_linewidths.control';
    else
        defaultControlFile = 'fitsettings_Individual_abs_quantif.control';
    end
else
   error('Sequence not defined yet');
end

individualControlFilesPathRemote = strcat(controlFilesPathRemote, individualPath);
individualControlFilesPathLocal = strcat(controlFilesPathLocal, individualPath);
preprocessedFilesPathRemote = strcat(remotePathBase, individualPath);

%% basic configurations
LCModelTableFits = strcat(fileBaseSpectrum, extensionTable);
LCModelTableFitsCoord = strcat(fileBaseSpectrum, extensionCoord);
LCModelOutputFiles = fileBaseSpectrum;

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(individualControlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

LCModelCallerInstance.CopyDataToRemote(preprocessedFilesPathLocal, preprocessedFilesPathRemote, [fileBaseSpectrum '.RAW']);
LCModelCallerInstance.CopyDataToRemote(preprocessedFilesPathLocal, preprocessedFilesPathRemote, [fileBaseWater '.RAW']);
%% create the control file
LCModelControlFile = createLCModelConfig('Met', controlFilesPathLocal, individualControlFilesPathLocal, defaultControlFile, ...
    currentTE, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser, fileBaseSpectrum, defaultSpectrum,...
    fileBaseWater, defaultWaterRef, sequence, 'sLASER');

[~, currentControlFile, extension] = fileparts(LCModelControlFile);
fittingLCModel(LCModelCallerInstance, {[currentControlFile, extension]}, {LCModelOutputFiles}, individualControlFilesPathRemote, ...
    individualControlFilesPathLocal, preprocessedFilesPathLocal);

plot_All_Fit_Metabolites(LCModelTableFitsCoord, LCModelOutputPath);


%% --------------------- Absolute Quantification ---------------------------------------------------------------------------------------------
%% get fitted concentrations
[tableConcentrations, wconc_LCModel, atth2o_LCModel] = importConcentrationLCModelTable({LCModelTableFits},LCModelOutputPath, currentTE, 'Met');
[relativeConcentrations, subjects, metaboliteNames]= extractConcentrations({tableConcentrations}, 1);

%% read tissue fractions
tissueCsvFile = ls([path '*.csv']);
if ~isempty(tissueCsvFile)
    tissueFractions = extractTissueFractionsFromCsv(path, tissueCsvFile);
else
    warning('Assuming tissue fractions: 70% GM, 25% WM, 5% CSF!');
    tissueFractions.fv_GM = 0.70;
    tissueFractions.fv_WM = 0.25;
    tissueFractions.fv_CSF = 0.05;
end
doMolal_Molar = 'Molal';
TR_ms = a.Parameter.Headers.TR_us * 1e-3;
TE_ms_water = a_water.Parameter.Headers.TE_us * 1e-3;
TR_ms_water = a_water.Parameter.Headers.TR_us * 1e-3;
NEX_H2O = a_water.Parameter.Headers.NAveMeas;
NEX_signal = a.Parameter.Headers.NAveMeas;
rescaleFactorMC = a.Parameter.ReconFlags.isRescaled;
%% get Relaxation times
[T1_met, T2_met, metaboliteNames] = getRelaxationTimes('DF_UF', metaboliteNames);
%% calculate absolute concentrations
[absoluteConcentrations] = calculateMolal_MolarConcentrations(relativeConcentrations, tissueFractions, doMolal_Molar, TR_ms, TE_ms, TR_ms_water, TE_ms_water, ...
    NEX_H2O, NEX_signal, rescaleFactorMC, wconc_LCModel, atth2o_LCModel, T2_met, T1_met);

%% plot concentrations
eliminateMetabolites = {'Leu','Cho+GPC+PCh', 'mI+Glyc', 'Glu+Gln', 'Lip13a', 'Lip13b', 'Lip13c', 'Lip13d','Lip20', 'Lip13a+Lip13b'};

plotColor = {[0 0 1], [0 0.5 0]};
offsetXAxis = 0.166;
[figureID] = plotConcentrations(absoluteConcentrations, metaboliteNames, eliminateMetabolites, doMolal_Molar, plotColor{1}, offsetXAxis);
