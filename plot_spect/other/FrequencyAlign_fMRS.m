 function FrequencyAlign_fMRS(this,range,ref_mix,is_plotted)
 %#########################################################################
 %function FrequencyAlign_fMRS(this,range,ref_mix,isplotted)
 %
 %
 %
 %
 %
 %
 %
 %
 %#########################################################################
 
if ~this.Parameter.ReconFlags.isread
    error('Please read the data first' );
end
if ~this.Parameter.ReconFlags.issorted
    error('expecting already sorted data');
end


NAA_range= 1000:1500; %fftshift(fft())!
residual_water= 2040:2060;

%Define the initial values if they are not given from the user
if nargin<2
    range= NAA_range;
    ref_mix=1;
    is_plotted= true;
end

%---------------------------------------------------------------

n_coils=size(this.Data{1},this.coil_dim);

if n_coils~=1 
    error(' The coils should be combined first!')
end

n_dyns=size(this.Data{1}, this.dyn_dim);
n_means=size(this.Data{1},this.meas_dim);

shift_freq=zeros(n_means, n_dyns);

%% Determine the reference fid!
method=1;
switch method
    %______________________________________________________________________
    case 1

        %find the fid with the highest SNR in the first dynamic
        position=0;
        SNR_max=0;
        dynamic=1;
            
        for mean=1:n_means
            
            fid= squeeze(this.Data{1}(:,1,1,1,1,1,1,1,ref_mix,dynamic,1,mean));
            ffts= fftshift( fft(fid) );
            
            SNR= max( abs(real (ffts(range)) ) )/std(real(ffts(end-1500:end)));
            
            if SNR> SNR_max
                SNR_max=SNR;
                position=mean;
            end
        end
        
        ref_fid= squeeze(this.Data{1}(:,1,1,1,1,1,1,1,ref_mix,dynamic,1,position));
               
    %______________________________________________________________________    
    case 2
        
        %Sum all the fids in the first dynamic
        
        dynamic=1;
        ref_fid= squeeze(sum( this.Data{1}(:,1,1,1,1,1,1,1,ref_mix,dynamic,1,:), this.meas_dim));
         
    %______________________________________________________________________
    case 3
       
       %find the find with the highest peak in the given range in the first dynamic
       dynamic=1;
       position=0;
       peak_max=0;
       
       for mean=1:n_means
           
          fid= squeeze(this.Data{1}(:,1,1,1,1,1,1,1,ref_mix,dynamic,1,mean));
          ffts= fftshift( fft(fid) );
          peak= max( abs(real ( ffts(range) ) ) );
          
          if  peak> peak_max
              peak_max=peak;
              position=mean;
          end
       end
       ref_fid= squeeze(this.Data{1}(:,1,1,1,1,1,1,1,ref_mix,dynamic,1,position));
       % this could be also performed using max function but I prefer this
       % way
end
   

%%
%##############################for fmincom#################################
options = optimset('Display', 'off', ...
                   'Algorithm', 'interior-point', ...
                   'DerivativeCheck', 'off', ...
                   'GradObj', 'on');

x0 = 0;
A = [];
b = [];
Aeq = [];
beq = [];
lb = -500;
ub = 500;
nonlcon = [];
%##########################################################################




%%
%find the shifted frequencies
for dyn=1:n_dyns
    for mean=1:n_means
        
    fid=squeeze(this.Data{1}(:,1,1,1,1,1,1,1,ref_mix,dyn,1,mean));
    %fid=fid; %This is the required format for the this.align_fid
    x=fmincon(@(x) this.align_fid(x, ref_fid, fid, range), x0, A, b, Aeq, beq, lb, ub, nonlcon, options);
    shift_freq(mean,dyn)=x ;
    
    
    end
end

%% Calculate the new fids
old_fids=this.Data{1}(:,1,1,1,1,1,1,1,ref_mix,:,1,:);

for dyn=1:n_dyns
    for mean=1:n_means
        this.Data{1}(:,1,1,1,1,1,1,1,ref_mix,dyn,1,mean)= this.Data{1}(:,1,1,1,1,1,1,1,ref_mix,dyn,1,mean).*...
            exp(1i*shift_freq(mean,dyn)*2*pi*linspace(0,1,length(fid))).';
    end
end

%%
if is_plotted 
    
    total_n_fids=n_dyns*n_means;
    
    new=zeros(total_n_fids,length(range));
    old=zeros(total_n_fids,length(range));
    
    i=0;
    for dyn=1:n_dyns
        for mean=1:n_means
            
        i=i+1;
        
        tmp_new= squeeze( fftshift( fft(this.Data{1}(:,1,1,1,1,1,1,1,ref_mix,dyn,1,mean))));
        tmp_old= squeeze( fftshift( fft(old_fids(:,1,1,1,1,1,1,1,ref_mix,dyn,1,mean))));
        
        new(i,:)= tmp_new(range)';
        old(i,:)= tmp_old(range)';
        
        end
    end
    
end




