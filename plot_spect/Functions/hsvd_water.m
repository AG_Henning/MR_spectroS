function varargout = hsvd_water(varargin)
% HSVD_WATER MATLAB code for hsvd_water.fig
%      HSVD_WATER, by itself, creates a new HSVD_WATER or raises the existing
%      singleton*.
%
%      H = HSVD_WATER returns the handle to a new HSVD_WATER or the handle to
%      the existing singleton*.
%
%      HSVD_WATER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HSVD_WATER.M with the given input arguments.
%
%      HSVD_WATER('Property','Value',...) creates a new HSVD_WATER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before hsvd_water_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to hsvd_water_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help hsvd_water

% Last Modified by GUIDE v2.5 04-Mar-2014 10:03:05





% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @hsvd_water_OpeningFcn, ...
                   'gui_OutputFcn',  @hsvd_water_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before hsvd_water is made visible.
function hsvd_water_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to hsvd_water (see VARARGIN)

% Choose default command line output for hsvd_water
handles.output = hObject;


%---------------Initialise the shown values, sliders etc-------------------
if nargin == 4
    handles.hInput = varargin{1};
   
end


% nSinusoids
% matrixSize
% boundaries
% hsvd
% hOriginalV
% hFittedV
% hSuppressedV

flagHsvd       = get(handles.hInput.SelectedProcessingValues,'hsvd');
n_sins         = get(handles.hInput.SelectedProcessingValues,'nSinusoids');
n_matrix       = get(handles.hInput.SelectedProcessingValues,'matrixSize'); 
bounds         = get(handles.hInput.SelectedProcessingValues,'boundaries');
flagOriginal   = get(handles.hInput.SelectedProcessingValues,'hOriginalV');
flagFitted     = get(handles.hInput.SelectedProcessingValues,'hFittedV');
flagSuppressed = get(handles.hInput.SelectedProcessingValues,'hSuppressedV');

% max of data points for the SVD matrix should be #timepoints--------------


this  = varargin{1}.MRSpectroMatrix.getCurrent;
n_matrix_max   = size(this.Data{1},this.kx_dim); 
slider_step    = [1/(log2(n_matrix_max) - 9) 1/(log2(n_matrix_max) - 9)]; % power of 2!

%--------------------------------------------------------------------------

set(handles.edit_sinusoids,'String',n_sins);
set(handles.slider_sinusoids,'Value',n_sins);

set(handles.edit_matrixSize,'String',n_matrix);
set(handles.slider_matrixSize,'Value',log2(n_matrix)); % power of 2!
set(handles.slider_matrixSize,'Max',log2(n_matrix_max));
set(handles.slider_matrixSize,'SliderStep',slider_step);

set(handles.edit_boundaries,'String',num2str(bounds));

set(handles.checkbox_hsvd,'Value',flagHsvd);

%Checkboxes
set(handles.checkbox_original,'Value',flagOriginal)
set(handles.checkbox_fitted,'Value',flagFitted)
set(handles.checkbox_suppressed,'Value',flagSuppressed)

%--------------------------------------------------------------------------


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes hsvd_water wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = hsvd_water_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




%#########################################################################
%                                 Sinusoids
%#########################################################################

function edit_sinusoids_Callback(hObject, eventdata, handles)
% hObject    handle to edit_sinusoids (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
current_value= str2double(get(hObject,'String'));
set(handles.slider_sinusoids,'Value',current_value);
set(handles.hInput.SelectedProcessingValues,'nSinusoids',current_value);
displaySpect(handles.hInput);




% --- Executes on slider movement.
function slider_sinusoids_Callback(hObject, eventdata, handles)
% hObject    handle to slider_sinusoids (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
current_value= get(hObject,'Value');
set(handles.edit_sinusoids,'String',current_value);
set(handles.hInput.SelectedProcessingValues,'nSinusoids',current_value);
displaySpect(handles.hInput);




%#########################################################################
%                            matrixSize
%#########################################################################


function edit_matrixSize_Callback(hObject, eventdata, handles)
% hObject    handle to edit_matrixSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value= get(hObject,'String');
current_value= str2double( current_value );
set(handles.slider_matrixSize,'Value',log2(current_value)); %power of 2!
set(handles.hInput.SelectedProcessingValues,'matrixSize',current_value);
displaySpect(handles.hInput);



% --- Executes on slider movement.
function slider_matrixSize_Callback(hObject, eventdata, handles)
% hObject    handle to slider_matrixSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value= get(hObject,'Value');
set(handles.edit_matrixSize,'String',pow2( current_value ));%power of 2!
set(handles.hInput.SelectedProcessingValues,'matrixSize',pow2(current_value)); %power of 2!
displaySpect(handles.hInput);


%##########################################################################
%                          Boundaries
%##########################################################################

function edit_boundaries_Callback(hObject, eventdata, handles)
% hObject    handle to edit_boundaries (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value= get(hObject,'String');
current_value= str2num(current_value); % str2double does not work in this case
set(handles.hInput.SelectedProcessingValues,'boundaries',current_value); 
displaySpect(handles.hInput);



%##########################################################################
%                           CHECKBOXES
%##########################################################################

% --- Executes on button press in checkbox_hsvd.
function checkbox_hsvd_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_hsvd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value= get( hObject,'Value');
set(handles.hInput.SelectedProcessingValues,'hsvd',current_value);
displaySpect(handles.hInput);


% hOriginalV
% hFittedV
% hSuppressedV

% --- Executes on button press in checkbox_original.
function checkbox_original_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_original (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value= get( hObject,'Value');
set(handles.hInput.SelectedProcessingValues,'hOriginalV',current_value);
displaySpect(handles.hInput);


% --- Executes on button press in checkbox_suppressed.
function checkbox_suppressed_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_suppressed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value= get( hObject,'Value');
set(handles.hInput.SelectedProcessingValues,'hSuppressedV',current_value);
displaySpect(handles.hInput);



% --- Executes on button press in checkbox_fitted.
function checkbox_fitted_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_fitted (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value= get( hObject,'Value');

set(handles.hInput.SelectedProcessingValues,'hFittedV',current_value);
displaySpect(handles.hInput);



%##########################################################################
%                             OK BUTTON
%##########################################################################

% --- Executes on button press in pushbutton_ok.
function pushbutton_ok_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close();



% --- Executes on button press in pushbuttonApply.
function pushbuttonApply_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonApply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 
this = handles.hInput.MRSpectroMatrix.getCurrent();

this.Parameter.HsvdSettings.p     = str2num(get(handles.edit_sinusoids,'String'));
this.Parameter.HsvdSettings.n     = str2num(get(handles.edit_matrixSize,'String'));
this.Parameter.HsvdSettings.bound = str2num(get(handles.edit_boundaries,'String'));

[~, this]  = this.Hsvd;

handles.hInput.MRSpectroMatrix.Save(this);
displaySpect(handles.hInput);



%##########################################################################
%##########################################################################

% --- Executes during object creation, after setting all properties.
function edit_sinusoids_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_sinusoids (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_boundaries_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_boundaries (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function slider_matrixSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_matrixSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function edit_matrixSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_matrixSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function slider_sinusoids_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_sinusoids (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

