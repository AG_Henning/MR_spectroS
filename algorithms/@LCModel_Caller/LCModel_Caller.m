classdef LCModel_Caller
    properties (Hidden)
        LCModelPassword
        LCModelPath = '/.lcmodel/bin/lcmodel';
        MakeBasisPath = '/.lcmodel/bin/makebasis';
        homePath
        LCModelUserDomain
        
        connection
        currentControlFilePath
    end
    
    properties
        % configuration of connection
        LCModelUser
        LCModelIP
        % file paths setup
        controlFilesPathRemote
        outputFilesPathRemote
        outputFilesPathLocal
        % current files used for fitting
        currentControlFile
        currentOutputFiles
    end
    
    methods
        %//////////////////////////////////////////////////////////////////////////
        %                                CONSTRUCTOR
        %//////////////////////////////////////////////////////////////////////////
        function this = LCModel_Caller(LCModelUser,LCModelPassword, LCModelIP, controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal)
            % Constructor of the LCModel Caller
            % - LCModelUser: the username on the LCModel computer
            % - LCModelPassword: Password of the user for the LCModel computer
            % - LCModelIP: IP address of LCModel computer
            % - controlFilesPathRemote: specifies the folder in the remote server
            % containing the *.control LCModel files
            % - outputFilesPathRemote:  specifies the folder in the remote server where
            % the data will be written by the aforementioned .control file
            %
            % The paths on the remote server are built up on the 'homePath' parameter
            % of the class, which is the bash folder for the given user.
            %
            % - outputFilesPathLocal: specifies the folder where the data should be
            % copied on the local computer
            %
            % % Sample declarations would be
            % LCModelUser = 'tborbath';
            % LCModelPassword = 'Password';
            % LCModelIP = '10.41.60.215';
            % controlFilesPathRemote = '/Desktop/MM/LCModelConfig/';
            % outputFilesPathRemote = '/Desktop/MM/Output/';
            % outputFilesPathLocal = 'D:/Software/Spectro Data/MM/Output/';
            %
            % Author: Tamas Borbath
            % version: 20 July 2018
            
            if(nargin  <  3)
                error('Error: LCModel_Caller constructor requires minimum 3 input arguments: LCModelUser,LCModelPassword, LCModelIP, controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal .');
            end
            this.LCModelUser = LCModelUser;
            this.LCModelUserDomain = [LCModelUser '@mpi.localnet'];
            this.LCModelPassword = LCModelPassword;
            this.LCModelIP = LCModelIP;
            if exist('controlFilesPathRemote','var')
                this.controlFilesPathRemote = controlFilesPathRemote;
            end
            if exist('outputFilesPathRemote','var')
                this.outputFilesPathRemote = outputFilesPathRemote;
            end
            
            if exist('outputFilesPathLocal','var')
                this.outputFilesPathLocal = outputFilesPathLocal;
            end
            %% install the ssh before every start
            sshfrommatlabinstall;
            %% establish ssh connection
            this.homePath = ['/kyb/agks/' LCModelUser];
%             try
%             this.connection = sshfrommatlab(this.LCModelUser, this.LCModelIP, 'Wrong Password');
%             catch exception
%             end
            this.connection = sshfrommatlab(this.LCModelUser, this.LCModelIP, this.LCModelPassword);
            
            %% Check if ssh connection is as expected
            [this.connection, res] = sshfrommatlabissue(this.connection,'pwd');
            if ~ strcmp(res{1}, this.homePath)
                error('It seems that you could not start the batch properly. Pwd shows: \n%s \n Quitting!', res{1});
            end
        end
        
        %//////////////////////////////////////////////////////////////////////////
        %                             METHODS-FUNCTIONS
        %//////////////////////////////////////////////////////////////////////////
        this    = ConfigurePaths( this, controlFilesPathRemote, outputFilesPathRemote, outputFilesPathLocal);
        this    = CreateBasisSet( this, makeBasisFilePathLocal, makeBasisFilePathRemote, makeBasisFile, copyFileLocally, basisFileName);
        this    = FitSpectrum( this, currentControlFile, currentOutputFiles, copyFileLocally);
        this    = CopyDataToRemote( this, localPath, remoteDirectory, fileName);
        this    = CopyDataFromRemote( this, remoteDirectory, localPath, fileName);
        this    = CreateRemoteFolder( this, remoteDirectory);
        this    = DeleteFilesFromRemote( this, remoteDirectory, fileNames);
        
        function delete(this)
            c = class(this);
            sshfrommatlabclose(this.connection);
            disp(['Object destructor called for class ',c])
        end
    end
end