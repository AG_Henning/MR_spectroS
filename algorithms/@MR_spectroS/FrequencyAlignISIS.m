function [this] = FrequencyAlignISIS(this,freqShiftsISISBlock,freqShiftsISISSteps)
nAverages     = size(this.Data{1},this.meas_dim);   
scheme        = this.Parameter.ISISSettings.scheme;

if strcmp(scheme,'ISIS-orig')
    nISIS  = 8;
    nSpec  = nAverages/nISIS;
elseif strcmp(scheme,'EISIS-full')
    nISIS  = 36;
    nSpec  = nAverages/nISIS;
end

%check if nSpec == length(freqShiftsISISBlock)
if nSpec ~= length(freqShiftsISISBlock)
    error('nSpec ~= length(freqShiftsISISBlock)');
end
%check if nISIS == length(freqShiftsISISSteps)
if nISIS ~= length(freqShiftsISISSteps)
    error('nISIS ~= length(freqShiftsISISSteps)');
end


freqShiftsISISBlockExpanded = zeros(1,nAverages);
for idxOfSpec = 1:nSpec
    offSet = (idxOfSpec-1) * nISIS + 1;
    freqShiftsISISBlockExpanded(offSet:offSet+nISIS-1) = freqShiftsISISBlock(idxOfSpec);
end

freqShiftsISISStepsExpanded = zeros(1,nAverages);
for idxOfSpec = 1:nISIS
    freqShiftsISISStepsExpanded(idxOfSpec:nISIS:nAverages) = freqShiftsISISSteps(idxOfSpec);
end

this = this.ApplyFrequencyShift(freqShiftsISISBlockExpanded);
this = this.ApplyFrequencyShift(freqShiftsISISStepsExpanded);

end
