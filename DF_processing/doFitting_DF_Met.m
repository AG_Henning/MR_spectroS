function doFitting_DF_Met(fitAllSubjects)

pathName = 'DF data path';
sampleCriteria = {'OutputMet', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
fitAllSubjects = true;
orderTEs = {'24'};
subjects = {'1658'; '1717';'2017';'3373';'3490';'5771';'6249';'6971'; '7338'; '9810'};

extensionTable = '.table';
extensionCoord = '.coord';
defaultTE = 'TE24';
defaultSubject = 'XXXX';
defaultLCModelUser = 'tborbath';

if fitAllSubjects
    controlFilesBase = 'fitsettings_Subject_XXXX_';
    controlFilesBaseTE = strcat(controlFilesBase, 'TE');
    controlFilesBaseSuffix = '_Metabolite.control';
    controlFilesBaseSuffix = '_Metabolite_deGraaf_abs_quantif.control';
    outputFileNameBase = strcat(defaultSubject, '_Metabolite_TE');
else
    controlFilesBase = 'fitsettings_Summed_Averaged_';
    controlFilesBaseTE = strcat(controlFilesBase, 'TE');
    controlFilesBaseSuffix = '_Metabolite.control';
    controlFilesBaseSuffix = '_Metabolite_deGraaf_abs_quantif.control';
    outputFileNameBase = 'Summed_Averaged_Metabolite_TE';
end

%% file paths setup
controlFilesPathRemote = '/Desktop/DATA_df/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/DATA_df/OutputMet/';
LCModelOutputPath = [localFilePathBase, 'OutputMet/'];
defaultControlFile = strcat(controlFilesBase, 'Default', controlFilesBaseSuffix);


if fitAllSubjects
    subjectsPath = strcat(subjects, '/');
else
    subjects = {'Summed'};
    subjectsPath = {''};
end

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath);
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath);
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath);
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath);

%% basic configurations
LCModelTableFits = strcat(outputFileNameBase, orderTEs, extensionTable);
LCModelTableFitsCoord = strcat(outputFileNameBase, orderTEs, extensionCoord);
LCModelOutputFiles = strcat(outputFileNameBase, orderTEs);
LCModelControlFiles = strcat(controlFilesBaseTE, orderTEs, controlFilesBaseSuffix);

numberOfTes = length(orderTEs);
numberOfSubjects = length(subjects);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

%% create the control file series
if fitAllSubjects
    createLCModelConfigsTEseries('Met', controlFilesPathLocal, defaultControlFile, orderTEs, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser, subjects, defaultSubject);
else
    % call the Configuration file creator without subject configuration
    createLCModelConfigsTEseries('Met', controlFilesPathLocal, defaultControlFile, orderTEs, defaultTE, defaultLCModelUser, LCModelCallerInstance.LCModelUser);
end

%% do the actual LCModel fitting and T2 calculation
parfor indexCurrentSubject = 1:numberOfSubjects
    %% do the LCModel fitting
    LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    if fitAllSubjects
        currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
    else
        currentControlFiles = LCModelControlFiles;
        currentOutputFiles = LCModelOutputFiles;
    end
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
end

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

for indexCurrentSubject = 1:numberOfSubjects
    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
    if fitAllSubjects
        currentLCModelTableFits = strrep(LCModelTableFits, defaultSubject, subjects{indexCurrentSubject});
        currentLCModelTableFitsCoord = strrep(LCModelTableFitsCoord, defaultSubject, subjects{indexCurrentSubject});
    else
        currentLCModelTableFits = LCModelTableFits;
        currentLCModelTableFitsCoord = LCModelTableFitsCoord;
    end
    
    numOfAvailableTEs = length(orderTEs);
    %% get fitted concentrations
    tableConcentrations{indexCurrentSubject} = importConcentrationLCModelTable(currentLCModelTableFits, ...
        subjectsLCModelOutputPath{indexCurrentSubject}, orderTEs, 'Met');
        
    %% mean CRLB
    CRLBs = cell2mat((tableConcentrations{indexCurrentSubject}(2:numberOfTes + 1,3:3:end)));
    meanCrlbTEs = mean(CRLBs,2, 'omitnan');
    meanCrlb = mean(meanCrlbTEs);
        
    %% display means
    table(1,1) = {'TE'};
    table(1,2:numOfAvailableTEs+1) = orderTEs(1:end);
    table(2,1) = {'CRLB'};
    table(2,2:numOfAvailableTEs+1) = num2cell(meanCrlbTEs(1:end));
    table(3,1) = {'meanCRLB'};
    table(3,2) = num2cell(meanCrlb);
    display(table)
    
    %% save table to file
    exportFileXlsx = [LCModelOutputPath ,'Quantification_Summaries_Metabolite.xlsx'];
    startIndex = 1+ 5 *(indexCurrentSubject-1);
    cellPositionTitle = ['A' num2str(startIndex)];
    xlswrite(exportFileXlsx, {['Subject: ' subjects{indexCurrentSubject}]}, 1, cellPositionTitle);
    cellPosition = ['A' num2str(startIndex + 1)];
    xlswrite(exportFileXlsx, table, 1, cellPosition);
end

if fitAllSubjects
    fileNameSuffix = 'Subjects';
else
    fileNameSuffix = 'Summed';
end
tableConcentrations_name = 'tableConcentrations';
tableConcentrations_nameSuffixed = [tableConcentrations_name fileNameSuffix];
eval( [tableConcentrations_nameSuffixed, '=',  tableConcentrations_name]);
save([LCModelOutputPath, tableConcentrations_nameSuffixed, '.mat'], tableConcentrations_nameSuffixed)
end