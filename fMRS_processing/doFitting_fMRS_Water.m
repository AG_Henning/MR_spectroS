function doFitting_fMRS_Water()
clear; clc; close all;
pathName = 'fMRS data path';
sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
subjects = {'2823';'5166';'9810';'2774';'1658';'5269';'1706';'1004';...
            '4085';'3333';'4012';'4085';'6524'};
prefixes = {'2020-02-18_';'2020-02-21_';'2020-02-21_';'2020-02-26_';...
            '2020-02-27_';'2020-02-27_';'2020-02-28_';'2020-03-02_';....
            '2020-03-02_';'2020-03-10_';'2020-03-10_';'2020-03-13_';'2020-03-13_';};
suffixFolder = '_fMRS';

extensionTable = '.table';
extensionCoord = '.coord';
defaultSubject = 'XXXX';
defaultSubjectsPath = 'YYYY';
defaultLCModelUser = 'jdorst';

    controlFilesBase = 'fitsettings_WRef_XXXX';
    controlFilesBaseSuffix = '.control';
    outputFileNameBase = strcat(defaultSubject, '_WRef');

%% file paths setup
controlFilesPathRemote = '/Desktop/1H_fMRS/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/1H_fMRS/Output/';
LCModelOutputPath = [localFilePathBase, 'Output/'];
defaultControlFile = strcat(controlFilesBase, controlFilesBaseSuffix);
subjectsPath = strcat(prefixes, subjects, suffixFolder, '/');
subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, subjectsPath);
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, subjectsPath);
preprocessedFilesPathLocal = strcat(localFilePathBase, subjectsPath);
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, subjectsPath);
subjectsLCModelOutputPath = strcat(LCModelOutputPath, subjectsPath);

%% process WRef data and save .RAW file
dataPath = strcat(localFilePathBase,subjectsPath);
for i = 1:length(dataPath)
    currentFolder = num2str(cell2mat(dataPath(i)));
    cd (currentFolder);
    subFolderName = dir(dataPath{i});
    dataFile = dir('*WRef*.dat');
    reconstructWater_sbe(dataFile.name,1,1);
    close(1); close(2); cd ..;
end

%% basic configurations
LCModelTableFits = strcat(outputFileNameBase, extensionTable);
LCModelTableFitsCoord = strcat(outputFileNameBase, extensionCoord);
LCModelOutputFiles = strcat(outputFileNameBase);
LCModelControlFiles = strcat(controlFilesBase, controlFilesBaseSuffix);

numberOfSubjects = length(subjects);

%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

%% do the actual LCModel fitting 
for indexCurrentSubject = 1:numberOfSubjects
    %% do the LCModel fitting
%     LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

    LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote{indexCurrentSubject}, subjectsLCModelOutputFilesRemote{indexCurrentSubject}, ...
        subjectsLCModelOutputPath{indexCurrentSubject});
        currentControlFiles = strrep(LCModelControlFiles, defaultSubject, subjects{indexCurrentSubject});
        currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{indexCurrentSubject});
    %% create the control file series
        currentControlFiles = cellstr(currentControlFiles);
        currentOutputFiles  = cellstr(currentOutputFiles);
        LCModelOutputFiles  = cellstr(LCModelOutputFiles);
        for indexFMRS = 1:length(currentControlFiles)
            currentOutputFile = currentOutputFiles{indexFMRS};
            currentLCModelOutputFile = LCModelOutputFiles{indexFMRS};
            createLCModelConfigWRef(controlFilesPathLocal, subjectsControlFilesPathLocal{indexCurrentSubject}, defaultControlFile, ...
                defaultLCModelUser, LCModelCallerInstance.LCModelUser, subjects{indexCurrentSubject}, defaultSubject,...
                subjectsPath{indexCurrentSubject}, defaultSubjectsPath);
        end
    fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote{indexCurrentSubject}, ...
        subjectsControlFilesPathLocal{indexCurrentSubject}, preprocessedFilesPathLocal{indexCurrentSubject});
end

%% Write H2O concentrations in table
tableConcentrations = [];
isFirstIter = 1;
s = 1;
for index =1:numberOfSubjects
    %% retrieve parameters
     currentOutputFiles = strrep(LCModelOutputFiles, defaultSubject, subjects{index});
     currentPath = strcat(LCModelOutputPath,subjectsPath{index},currentOutputFiles,'.table');
    [c1 c2 c3 c4] = textread(currentPath{1},'%s %s %s %s');
    
    if isFirstIter
        start  = find(strcmp(c1,'Conc.'));
        start  = start + 1;
        finish = find(strcmpi(c1,'$$MISC')) - 1;
        start2  = find(strcmp(c1,'FWHM'));
        finish2 = start2;
        tableConcentrations{1,1} = 'ID';
        
        tableConcentrations{1,s+1} = 'Conc_H2O';
        tableConcentrations{1,s+2} = '%SD';
        tableConcentrations{1,s+3} = 'FWHM';
        tableConcentrations{1,s+4} = 'S/N';
    end
    
    %% add each metabolites to the table
    for j=start:finish
        %add quantified metabolite 
        tableConcentrations{index+1,s} = strcat(prefixes{index}, subjects{index});
        tableConcentrations{index+1,s+1} = str2num( c1{j});
        %add CRLB
        tableConcentrations{index+1,s+2} = str2num(  c2{j}(1:end-1));
    end
    
    for j = start2:finish2
        tableConcentrations{index+1,s+3} = str2num(  c3{j});
        tableConcentrations{index+1,s+4} = str2num(  regexprep(c2{j+1},'=',''));
    end
        
    isFirstIter = false;
end


% save table to file
exportFileXlsx = [LCModelOutputPath ,'WaterReference_Fit.xlsx'];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

end
