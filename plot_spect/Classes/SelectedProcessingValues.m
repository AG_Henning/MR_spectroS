classdef SelectedProcessingValues < hgsetget
    
    properties
        
        Gaussian
        Exponential
        Phase0
        Phase1
        RefPhaseFrequency
        nSinusoids
        matrixSize
        boundaries
        hsvd
        hOriginalV
        hFittedV
        hSuppressedV
        PeakRange
        NoiseRangeFD
        NoiseTimeDomain
        NoiseRangeTD
        boldLinewidth
        boldPhase
        boldFrequency
        boldOffset
        MissingPointPred_imiss
        MissingPointPred_T_factor
        MissingPointPred_polyOrder
        
    end
    
    
    methods
        
        function this = SelectedProcessingValues()
            
            % Voigt
            this.Gaussian=0;
            this.Exponential=0;
            
            % Phase Correction
            this.Phase0            = 0;
            this.Phase1            = 0;
            this.RefPhaseFrequency = 0;
            
            % HSVD 
            this.nSinusoids=25;
            this.matrixSize=4096;
            this.boundaries=[-160 160];
            this.hsvd= false;
            this.hOriginalV= true;
            this.hFittedV= true;
            this.hSuppressedV= true;
            
            %Noise settings
            this.PeakRange= [1 10 ];
            this.NoiseRangeFD= [1 1000];
            this.NoiseRangeTD= [3096 4096];
            this.NoiseTimeDomain= false;
            
            %fMRS BOLD Effect Correction
            
            this.boldLinewidth  = 0;
            this.boldPhase      = 0;
            this.boldFrequency  = 0;
            this.boldOffset     = 0;
            
            %Missing Point Prediction
            this.MissingPointPred_imiss = 0;
            this.MissingPointPred_T_factor = 2;
            this.MissingPointPred_polyOrder = 60;
           
        end
        
       
  
        
    end
    
   
end
 