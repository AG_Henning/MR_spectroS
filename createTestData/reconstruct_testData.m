function [a, stats] = reconstruct_testData(fid_id, flagsReconstruct, averages2Keep, coilChannels2Delete)
%TODO save results outside

%all data is assumed to be using metabolite cycling
a = MR_spectroS(fid_id,1);

if flagsReconstruct.truncStep1
    %% truncate before anything
    trunc_ms = 250;
    truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
    a = a.Truncate(truncPoint);
end

if flagsReconstruct.frequencyAlign
    a = a.FrequencyAlign;
end

if flagsReconstruct.rescale
    a = a.Rescale;
end

%since all data is metabolite cycled we use MC Recon on all of them
a = a.ReconData;

%phase cycling steps 16:(sSpecPara.lPhaseCyclingType = 1)
% step to delete averages (mind phase cycling!)
if flagsReconstruct.averageAll == false
    if exist('averages2Keep','var')
        a.Data{1} = a.Data{1}( :, 1, 1, :, 1, 1, 1, 1, :, 1, 1, averages2Keep);
    else
        error('averages2Keep variable missing');
    end
end

% after potential selection of some averages to delete, averaging is performed
a = a.AverageData;

if flagsReconstruct.eddyCurrentCorrection
    a = a.EddyCurrentCorrection;
end

% delete coils
if flagsReconstruct.coilCombineAll == false
    if exist('coilChannels2Delete','var')
        a = a.DeleteCoilChannels(coilChannels2Delete);
    else
        error('coilChannels2Delete variable missing')
    end
end

a = a.CombineCoils;


if flagsReconstruct.frequencyAlignFreqDomain
    %% setup the settings for the FrequencyAlignFreqDomain
    a.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
    a.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;
    
    %indices of Interest: given in ppm
    % water peak
    a.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = [2.008];
    
    %set zeroFillingParameter to get smooth approximations
    a.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;
    
    %search area (+-) in ppm
    a.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.1;
    
    %spline filtering is not needed if the spectra are not too noisy.
    %flag to do spline filtering
    a.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
    %spline filtering coefficient
    a.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;
    % do the actual Frequency Alignment
    a = a.FrequencyAlignFreqDomain;
end

if flagsReconstruct.waterSuppress
    a.Parameter.HsvdSettings.bound = [-160 160];
    a.Parameter.HsvdSettings.n = size(a.Data{1},1);
    a.Parameter.HsvdSettings.p = 25;
    [~, a] = a.Hsvd;
end

if flagsReconstruct.waterSuppress2
    [~, a] = a.Hsvd;
    [~, a] = a.Hsvd;
end

if flagsReconstruct.reverseMC
    %% Reverse MC
    a = a.ReverseMC;
    [~, a] = a.Hsvd;
end

%
if flagsReconstruct.truncStepLast
    trunc_ms = 200;
    truncPoint = floor( trunc_ms/(a.Parameter.Headers.DwellTimeSig_ns*1e-6));
    a = a.Truncate(truncPoint);
end

% retrieve SNR values for NAA, Cr(CH3), Glu (2.4ppm), tCho
% the noise is calculated from the noise window of the first 500 points (-5 to -3 ppm)
% subtracting a 1st order polynomial from the points
% The peak amplitude is calculated from the real spectra
stats = a.Stats;

end