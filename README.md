## Description
This library provides extensive functionality to reconstruct magnetic resonance spectroscopy data.
These include:
1. preprocess raw data including:
    - metabolite cycling
    - frequency alignment both in time and frequency domain
    - Averaging
    - Eddy current correction
    - coil combination
    - HSVD water suppression
    - etc. See functions such as `reconstruct`, `reconstruct_all_...`
2. call LCModel spectral fitting via ssh
3. calculate mmol/kg concentrations
4. all above functionalities are adapted to:
    - upfield spectra
    - downfield spectra
    - macromolecular spectra
    - macromolecular spectra fitted to amino acids models
    - creating and evaluating simulated spectra
    - functional MRS data

## Dependencies

For reading in raw scanner data files, the `mapVBVD` function by Philipp Ehses was used. This and other functions to read in raw scanner data can be found in the Gannet2.0 repository: https://github.com/cjohnevans/Gannet2.0 
Additional MATLAB packages used by the code include:
https://de.mathworks.com/matlabcentral/fileexchange/27999-ssh-from-matlab-updated-sftp-scp
https://de.mathworks.com/matlabcentral/fileexchange/19729-passwordentrydialog
https://de.mathworks.com/matlabcentral/fileexchange/27418-fdr_bh
https://de.mathworks.com/matlabcentral/fileexchange/28303-bonferroni-holm-correction-for-multiple-comparisons
https://de.mathworks.com/matlabcentral/fileexchange/71052-blandaltmanplot
https://de.mathworks.com/matlabcentral/fileexchange/13634-axescoord2figurecoord
https://de.mathworks.com/matlabcentral/fileexchange/5782-eps2pdf

## Publications

Following noteable publications used this library:

Giapitzakis, I. A., Shao, T., Avdievich, N., Mekle, R., Kreis, R., & Henning, A. (2018). Metabolite‐cycled STEAM and semi‐LASER localization for MR spectroscopy of the human brain at 9.4 T. Magnetic resonance in medicine, 79(4), 1841-1850.

Giapitzakis, I. A., Borbath, T., Murali‐Manohar, S., Avdievich, N., & Henning, A. (2019). Investigation of the influence of macromolecules and spline baseline in the fitting model of human brain spectra at 9.4 T. Magnetic resonance in medicine, 81(2), 746-758.

Giapitzakis, I. A., Avdievich, N., & Henning, A. (2018). Characterization of macromolecular baseline of human brain using metabolite cycled semi‐LASER at 9.4 T. Magnetic resonance in medicine, 80(2), 462-473.

Avdievich, N. I., Giapitzakis, I. A., Pfrommer, A., Borbath, T., & Henning, A. (2018). Combination of surface and ‘vertical’loop elements improves receive performance of a human head transceiver array at 9.4 T. NMR in Biomedicine, 31(2), e3878.

Murali‐Manohar, S., Borbath, T., Wright, A. M., Soher, B., Mekle, R., & Henning, A. (2020). T2 relaxation times of macromolecules and metabolites in the human brain at 9.4 T. Magnetic resonance in medicine, 84(2), 542-558.

Murali‐Manohar, S., Wright, A. M., Borbath, T., Avdievich, N. I., & Henning, A. (2021). A novel method to measure T1‐relaxation times of macromolecules and quantification of the macromolecular resonances. Magnetic resonance in medicine, 85(2), 601-614.

Borbath, T., Murali‐Manohar, S., Wright, A. M., & Henning, A. (2021). In vivo characterization of downfield peaks at 9.4 T: T2 relaxation times, quantification, pH estimation, and assignments. Magnetic resonance in medicine, 85(2), 587-600.

Dorst, J., Ruhm, L., Avdievich, N., Bogner, W., & Henning, A. (2021). Comparison of four 31P single‐voxel MRS sequences in the human brain at 9.4 T. Magnetic resonance in medicine, 85(6), 3010-3026.

Borbath, T., Murali‐Manohar, S., Dorst, J., Wright, A. M., & Henning, A. (2021). ProFit‐1D—A 1D fitting software and open‐source validation data sets. Magnetic resonance in medicine
