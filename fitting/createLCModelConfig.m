function outputFile = createLCModelConfig(downField_MM_Met, controlFilesPath, outputControlFilesPath, defaultControlFile, ...
    currentTE, defaultTE, defaultLCModelUser, currentLCModelUser, spectrumFileName, defaultSpectrum,...
    waterRefFileName, defaultWaterRef, sequence, defaultSequence)
% Intro comments need to be rewritten
% Creates the control files for the TE series, given a default control file.
% the newly generated files will be placed in the same path as indicated by
% the controlFilesPath.
% downField_MM_Met can be: 'DF' 'MM', 'Met' or 'pH'
% A sample input:
% controlFilesPath = 'Fitsettings/MMocc_noExtraMM_06knots/';
% defaultControlFile = 'fitsettingsDefault.control'

currentTELong = strcat('TE', currentTE);
controlFileSuffix = '.control';

fitSettingsDefaultFileID = fopen([controlFilesPath defaultControlFile], 'r');

outputFile = strcat(outputControlFilesPath, spectrumFileName, controlFileSuffix);

fitSettingsFileID = fopen(outputFile, 'w');
display(outputFile);

while(~feof(fitSettingsDefaultFileID))
    s = fgetl(fitSettingsDefaultFileID);
    s = strrep(s, defaultLCModelUser, currentLCModelUser);
    s = strrep(s, defaultTE, currentTELong);
    s = strrep(s, defaultSequence, sequence);
    s = strrep(s, defaultSpectrum, spectrumFileName);
    s = strrep(s, defaultWaterRef, waterRefFileName);
    
    switch downField_MM_Met
        case 'MM'
            if (str2num(currentTE) >= 40)
                if (~isempty(strfind(s,'MM27')))
                    s = strrep(s, 'AMP= 1', 'AMP= -1');
                end
            end
        otherwise
            % DO nothing
    end
    fprintf(fitSettingsFileID,'%s\n', s);
end
fclose(fitSettingsFileID);
fclose(fitSettingsDefaultFileID);
end