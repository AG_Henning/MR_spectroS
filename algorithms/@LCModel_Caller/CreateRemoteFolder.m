function this    = CreateRemoteFolder( this, remoteDirectory)
% create the folder if it does not exist. 
% The command does nothing if the folder exists.
[this.connection, res] = sshfrommatlabissue(this.connection, ['mkdir ' this.homePath remoteDirectory]);

end