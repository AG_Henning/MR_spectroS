function varargout = phase_correction(varargin)
% PHASE_CORRECTION MATLAB code for phase_correction.fig
%      PHASE_CORRECTION, by itself, creates a new PHASE_CORRECTION or raises the existing
%      singleton*.
%
%      H = PHASE_CORRECTION returns the handle to a new PHASE_CORRECTION or the handle to
%      the existing singleton*.
%
%      PHASE_CORRECTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PHASE_CORRECTION.M with the given input arguments.
%
%      PHASE_CORRECTION('Property','Value',...) creates a new PHASE_CORRECTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before phase_correction_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to phase_correction_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help phase_correction

% Last Modified by GUIDE v2.5 20-Aug-2018 17:43:27



% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @phase_correction_OpeningFcn, ...
                   'gui_OutputFcn',  @phase_correction_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before phase_correction is made visible.
function phase_correction_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to phase_correction (see VARARGIN)

% Choose default command line output for phase_correction
handles.output = hObject;



%---------------Initialise the shown values, sliders etc-------------------
if nargin == 4
    handles.hInput= varargin{1};
end

phase0_n     = get(handles.hInput.SelectedProcessingValues,'Phase0');
phase1_n     = get(handles.hInput.SelectedProcessingValues,'Phase1');
refPhaseFreq = get(handles.hInput.SelectedProcessingValues,'RefPhaseFrequency'); 

phase1_ms = phase1_n/(2e-3) / 180;

set(handles.edit_phase0,'String',phase0_n);
set(handles.slider_phase0,'Value',phase0_n);
set(handles.edit_phase1,'String',phase1_n);
set(handles.edit_phase1_ms,'String',phase1_ms);
set(handles.slider_phase1,'Value',phase1_n);

set(handles.slider_phase1,'Max',1.500); %0.142 was the legacy value
set(handles.slider_phase1,'Min',-1.500); %-0.142 was the legacy value
set(handles.slider_phase1,'SliderStep',[0.001 0.001]);

set(handles.edit_refFreq,'String',refPhaseFreq);

%--------------------------------------------------------------------------





% Update handles structure
guidata(hObject, handles);

% UIWAIT makes phase_correction wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = phase_correction_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




%##########################################################################
%                             PHASE_0
%##########################################################################

% --- Executes on slider movement.
function slider_phase0_Callback(hObject, eventdata, handles)
% hObject    handle to slider_phase0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value=get(hObject,'Value');
set(handles.edit_phase0,'String',current_value);
set(handles.hInput.SelectedProcessingValues,'Phase0',current_value);
displaySpect(handles.hInput)



function edit_phase0_Callback(hObject, eventdata, handles)
% hObject    handle to edit_phase0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value=get(hObject,'String');
current_value=str2double(current_value);
set(handles.slider_phase0,'Value',current_value);
set(handles.hInput.SelectedProcessingValues,'Phase0',current_value);
displaySpect(handles.hInput)



%##########################################################################
%                          REF FREQ FOR PHASE CORR.
%##########################################################################


function edit_refFreq_Callback(hObject, eventdata, handles)
% hObject    handle to edit_refFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value =  get(hObject,'String')      ;
current_value =   str2double(current_value) ;

set(handles.hInput.SelectedProcessingValues,'RefPhaseFrequency',current_value);
displaySpect(handles.hInput)


%##########################################################################
%                           PHASE_1
%##########################################################################


% --- Executes on slider movement.
function slider_phase1_Callback(hObject, eventdata, handles)
% hObject    handle to slider_phase1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value=get(hObject,'Value');
phase1_ms = current_value/(2e-3) / 180;
set(handles.edit_phase1,'String',current_value);
set(handles.edit_phase1_ms,'String',phase1_ms);

set(handles.hInput.SelectedProcessingValues,'Phase1',current_value);
displaySpect(handles.hInput)


function edit_phase1_Callback(hObject, eventdata, handles)
% hObject    handle to edit_phase1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value=get(hObject,'String');


current_value=str2double(current_value);
phase1_ms = current_value/(2e-3) / 180;
set(handles.edit_phase1_ms,'String',phase1_ms);
set(handles.slider_phase1,'Value',current_value);


set(handles.hInput.SelectedProcessingValues,'Phase1',current_value);
displaySpect(handles.hInput)
 


function edit_phase1_ms_Callback(hObject, eventdata, handles)
% hObject    handle to edit_phase1_ms (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_phase1_ms as text
%        str2double(get(hObject,'String')) returns contents of edit_phase1_ms as a double

current_value=get(hObject,'String');


current_value=str2double(current_value);
phase1_n = current_value*(2e-3) * 180;
set(handles.edit_phase1,'String',phase1_n);
set(handles.slider_phase1,'Value',phase1_n);


set(handles.hInput.SelectedProcessingValues,'Phase1',phase1_n);
displaySpect(handles.hInput)



%##########################################################################
%                              BUTTONS
%##########################################################################


% --- Executes on button press in pushbutton_ok.
function pushbutton_ok_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close;


% --- Executes on button press in pushbuttonApply.
function pushbuttonApply_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonApply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


this                                        =  handles.hInput.MRSpectroMatrix.getCurrent();
this.Parameter.PhaseCorrSettings.phase0     =  get(handles.slider_phase0,'Value');
this.Parameter.PhaseCorrSettings.phase1     =  get(handles.slider_phase1,'Value');
this.Parameter.PhaseCorrSettings.freqCentre =  str2double( get(handles.edit_refFreq,'String'));
this                                         = this.PhaseCorrection;
phase0_n = 0;
phase1_n = 0;
phase1_ms = 0;
set(handles.edit_phase0,'String',phase0_n);
set(handles.slider_phase0,'Value',phase0_n);
set(handles.hInput.SelectedProcessingValues,'Phase0',phase0_n);
set(handles.edit_phase1,'String',phase1_n);
set(handles.slider_phase1,'Value',phase1_n);
set(handles.hInput.SelectedProcessingValues,'Phase1',phase1_n);
set(handles.edit_phase1_ms,'String',phase1_ms);
handles.hInput.MRSpectroMatrix.Save(this);
displaySpect(handles.hInput);





%##########################################################################
%##########################################################################







% --- Executes during object creation, after setting all properties.
function edit_phase0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_phase0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes during object creation, after setting all properties.
function slider_phase1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_phase1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end




% --- Executes during object creation, after setting all properties.
function edit_phase1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_phase1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function slider_phase0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_phase0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes during object creation, after setting all properties.
function edit_refFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_refFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes during object creation, after setting all properties.
function edit_phase1_ms_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_phase1_ms (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
