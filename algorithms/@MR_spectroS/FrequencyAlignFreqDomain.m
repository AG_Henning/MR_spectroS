function this = FrequencyAlignFreqDomain(this)

%retrieve general information from the dataset
oldData     = this.Data{1};
nAverages   = size(oldData,this.meas_dim);
samples     = size(oldData, this.kx_dim);
%TODO check how it works. Fails if nAverages =1, nCoils =1 
%select the coil with the highest SNR
% tmpData                            =  squeeze(oldData(:,1,1,:,1,1,1,1,selectedMix,1,1,:));
% tmpData                            =  permute(tmpData,[1 3 2]); %permure data in order to order them average-wise
% tmpData                            =  reshape(tmpData,size(tmpData,1),nAverages*nCoils); %reshape and find the max
% [~, maxPosition]                   =  max(max(tmpData(1:30,:,:)));
% selectedCoil                       =  floor(maxPosition/nAverages)+1;

selectedCoil    =  this.Parameter.FreqAlignFreqDomainSettings.selectedCoil;
selectedMix     = this.Parameter.FreqAlignFreqDomainSettings.selectedMix;
%set zeroFillingParameter to get smooth approximations
zeroFillFactor  = this.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor;
%indices of Interest: given in ppm
indices         = this.Parameter.FreqAlignFreqDomainSettings.peaksInPpm; 
%search area (+-) in ppm
searchArea      = this.Parameter.FreqAlignFreqDomainSettings.searchArea; 
%flag to do spline filtering
doSplineSmoothing      = this.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing;
%spline filtering coefficient
splineSmoothingCoeff      = this.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff;

numberOfIndices = length(indices);
%group of searchAreas
indicesSearchAreas = [indices-searchArea indices+searchArea];

%[ppm] vector 
[ppm] = this.getPpmVector(zeroFillFactor);
%ppm groups of indices to search for
metaboliteIndexes= zeros(numberOfIndices, samples * zeroFillFactor);
for index = 1: numberOfIndices
    metaboliteIndexes(index,:) = this.convertPPMtoIndex(indicesSearchAreas(index,:), zeroFillFactor);
end


%calculate for all averages
freqShifts = zeros(1,nAverages);
for iAverage = 1:nAverages
    %get the individual datasets
    dataMix = squeeze(oldData(:,1,1,selectedCoil,1,1,1,1,selectedMix,:,1,iAverage));
    %do spline smoothing if flag is set
    if doSplineSmoothing
        dataMix = MR_spectroS.smoothArray(dataMix, splineSmoothingCoeff, false);
    end
    %frequency Domain signal (zerofilled to get better resolution)
    fftsReal      = real(fftshift(fft(dataMix, samples*zeroFillFactor)));
    
    %define the ppmDifferences
    ppmMetaboliteDifference = zeros(1,numberOfIndices);
    % calculate index of peak with the max amplitude using the ppm masks
    for index = 1:numberOfIndices
        %define peak search area
        metaboliteIndexesToSearch = logical(metaboliteIndexes(index,:));
        peakMetabolite          = fftsReal(metaboliteIndexesToSearch);
        %get the position of the maximal amplitude
        [~,  maxPosMetabolite] = max(abs(peakMetabolite));
        %retrieve the ppmValue of the peak
        ppmMetaboliteIndexes = ppm(metaboliteIndexesToSearch);
        ppmMetaboliteDifference(index) = ppmMetaboliteIndexes(maxPosMetabolite) - indices(index);
    end
    %get mean value of the ppm differences
    meanPpmDifference = mean(ppmMetaboliteDifference);
    %transform this differences into Hz
    freqShifts(iAverage) = meanPpmDifference;
end


this = this.ApplyFrequencyShift(freqShifts);

this.Parameter.FreqAlignFreqDomainSettings.freqShifts = freqShifts;

end