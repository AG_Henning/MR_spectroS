classdef HsvdSettings 
   
    properties
        
     p %number of decaying sinusoid
     n %matrix size for SVD
     bound % boundaries for fitting [Hz units]
     
     
    
    end   
   
    methods
        
        function this= HsvdSettings()
            
            this.p=25;
            this.n=4096;
            this.bound= [-160 160]; %(default)
            
            
        end
        
    end
    
end

