function tableT2Summarized = calculateT2ExchangeCorrected()

pathName = 'DF data path';
sampleCriteria = {'Output'};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);

localFilePath = [localFilePathBase 'Output\FWHMBA Results 7.5e-3\'];
load([localFilePath 'meanT2s.mat'], 'acceptableMeanT2s', 'acceptableStdT2s', 'metaboliteNamesDisplay');

% exchangeTimes in s-1 from Fichtner 2017 Occipital ROI. 
% For not separated peaks in Nicole's work, both peaks from this work used the same exchange rate
metaboliteNamesExchange = {'DF5.75'; 'DF6.83'; 'DF8.18'; 'DF8.24'; 'DF8.37'; 'DF8.49'; 'NAA Broad'; 'NAA'; 'total NAA'};
exchangeRates =           [    6.76;     2.34;     9.32;     9.32;    13.8;      3.31;        0.74;  0.74;       0.74;];
exchangeRatesStd =        [    1.59;     0.44;     0.91;     0.91;    0.79;      0.06;        0.23;  0.23;       0.23;];

numberOfMetabolites = length(metaboliteNamesDisplay);

T2s_corrected = zeros(1,numberOfMetabolites);
T2_std_corrected = zeros(1,numberOfMetabolites);
tableT2Summarized = cell(numberOfMetabolites,2);
tableT2Summarized(2:end,1) = metaboliteNamesDisplay(2:end);
plusMinusSign = [' ' char(177) ' '];
% T2 and T2_app in (ms); k in (s-1) or (Hz)
% The formula:
% T2 = T2_app / (1-k*T2_app) * 1e3
% with the standard deviation calculated as:
% 
for indexMet = 1: numberOfMetabolites
    index = find(strcmp(metaboliteNamesDisplay{indexMet}, metaboliteNamesExchange));
    if isempty(index)
        T2s_corrected(indexMet)    = NaN;
        T2_std_corrected(indexMet)    = NaN;
        tableT2Summarized{indexMet,2} = '-';
    else
        T2_apparent = acceptableMeanT2s(indexMet) * 1e-3;
        T2_apparent_std = acceptableStdT2s(indexMet) * 1e-3;
        k = exchangeRates(index);
        k_std = exchangeRatesStd(index);
        T2s_corrected(indexMet)    = T2_apparent / (1-k*T2_apparent) * 1e3;
        T2s_corrected(indexMet)    = 1 / ((1/T2_apparent)-k) * 1e3;
        T2_std_corrected(indexMet) = 1/sqrt((1/T2_apparent_std)^2+k_std^2) * 1e3;
        % write format
        tableT2Summarized{indexMet,2} = [num2str(T2s_corrected(indexMet), '%.1f') plusMinusSign  num2str(T2_std_corrected(indexMet), '%.1f')];
    end
end
tableT2Summarized(1,1) = {''};
tableT2Summarized(1,2) = {'T2 Corrected'};

end


