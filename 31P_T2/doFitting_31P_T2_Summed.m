function doFitting_31P_T2_Summed()
clear; clc; close all;

%%%%%%%%%%
% Check controlFilesBase, STEAMVersion


pathName = 'T2 31P Paper Data';
% sampleCriteria = {'Output', 'LCModelConfig'};
[localFilePathBase] = pathToDataFolder(pathName);

subjects = {'9873';'1090';'3490';'9810';'1658';'2017';'6524';'3009';...
    '6260';'2823';'2455';'4085'};
prefixes = {'2019-09-30_';'2019-10-10_';'2019-10-14_';'2019-10-14_';...
    '2019-10-22_';'2019-11-07_';'2019-11-07_';'2019-11-20_';...
    '2019-11-20_';'2019-12-05_';'2020-07-22_';'2020-07-23_';};
suffixFolder = '_SurfCoil';
STEAMVersion = ''     %'Ideal_JD';

%be careful with Pi intra/extra, NADH and bATP!
metaboliteNames = {'PE', 'PC', 'Pi_int', 'GPE', 'GPC', 'PCr', 'gATP', 'aATP', 'NADH'};
centerPpm =       [ 6.77, 6.23, 4.83,     3.51,   2.96,  0,     -2.48, -7.62,  -8.16];

%%%%%%%%%%%%%%
%Select
trunc_ms = 90;
TruncToFit = 'noTrunc';        %'noTrunc';   %'Trunc90ms';

% % controlFilesBase = 'fitsettings_ZZZZ_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_Summed';
% controlFilesBase = 'fitsettings_ZZZZ_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms';
controlFilesBase = 'fitsettings_ZZZZ_STEAM_Real_XXXX_TM5ms_LW_Trunc90ms';
% % % % controlFilesBase = 'fitsettings_ZZZZ_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noBCoupl';
% % % % controlFilesBase = 'fitsettings_ZZZZ_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noAGCoupl';
% % % % controlFilesBase = 'fitsettings_ZZZZ_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noBCoupl_noAGCoupl';
%%%%%%%%%%%%%%

numberOfSubjects = length(subjects);
TEs = {'6'; '8'; '11'; '15'; '20'; '30'; '50'; '80'; '150'};  %ms
numberOfTEs = length(TEs);

paths = cell(1,numberOfSubjects);
for indexSubject = 1:numberOfSubjects
    paths{indexSubject} = [localFilePathBase prefixes{indexSubject} subjects{indexSubject} suffixFolder '\'];
end
outputPath = 'D:\PAPER\31P_T2_RelaxationTimes\Data\Summed';

%read MR_SpectroS files of all subjects and TEs into cell array FIDsTE
FIDsTE = cell(numberOfSubjects,numberOfTEs);    %subjects x TEs
for indexSubject = 1:numberOfSubjects
    files = ls([paths{indexSubject} '*_noTrunc.mat*']);
    for indexTE = 1:numberOfTEs
        temp = strfind(cellstr(files),strcat('TE',num2str(TEs{indexTE}),'ms'));
        indexFile = find(~cellfun(@isempty,temp));
        load([paths{indexSubject} '\' files(indexFile,:)]);     %MR_SpectroS files saved as currentData
        FIDsTE{indexSubject,indexTE} = currentData;
    end
end

dummy_MR_SpectroS = currentData;
dummy_MR_SpectroS.Parameter.Filepath = outputPath;

nTimepoints     = size(dummy_MR_SpectroS.Data{1},dummy_MR_SpectroS.kx_dim);
bandwidth       = dummy_MR_SpectroS.Parameter.Headers.Bandwidth_Hz;
freq    = bandwidth/nTimepoints*(-(nTimepoints/2-1):1:nTimepoints/2)';
time    = 1/bandwidth*(0:nTimepoints-1)'*1000;      %ms
scanFreq_MHz = dummy_MR_SpectroS.Parameter.Headers.ScanFrequency/1E6;

%sum FIDs, truncate them
FIDsTESummed = cell(1,numberOfTEs);    %1 x TEs
FIDsTESummed_noTrunc = cell(1,numberOfTEs);    %1 x TEs
FIDsTESummed_Trunc90ms = cell(1,numberOfTEs);    %1 x TEs
% for indexTE = 1:numberOfTEs
%     FIDsTESummed{1,indexTE} = zeros(4096,1);
%     for indexSubject = 1:numberOfSubjects
%         FIDsTESummed{1,indexTE} = FIDsTESummed{1,indexTE} + FIDsTE{indexSubject,indexTE}.Data{1};
%     end
%     FID_MR_SpectroS = dummy_MR_SpectroS;
%     FID_MR_SpectroS.Data{1} = FIDsTESummed{1,indexTE};
%     FID_MR_SpectroS.Parameter.Filename = strcat('Summed_TE',num2str(TEs{indexTE}),'ms_noTrunc');
%     FIDsTESummed_noTrunc{1,indexTE} = FID_MR_SpectroS;
%
%     figure
%     plot(freq/scanFreq_MHz,(real(fftshift(fft(FID_MR_SpectroS.Data{1})))))
%     axis([-20 8 -inf inf])
%     set(gca,'xdir','reverse')
%     xlabel 'f / ppm'
%     title (['TE ', num2str(TEs{indexTE}), ' ms  ', 'Summed ', 'no trunc '], 'Interpreter', 'none')
%     fig2save_Trunc = fullfile(outputPath, strcat('TE', num2str(TEs{indexTE}),'ms_', 'noTrunc', '.fig'));
%     savefig(fig2save_Trunc)
%
%     % SNR in frequency domain, no trunc or filtering
%     Data = FID_MR_SpectroS.Data{1};
%     Data = fftshift(fft(Data));
%     searchArea = 0.2; %ppm
%     snrValuenoTrunc = zeros(length(metaboliteNames),1);
%     for iMetabolite = 1:length(metaboliteNames)
%         ppmStartMet = round(nTimepoints/2 + (centerPpm(iMetabolite) + searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
%         ppmEndMet = round(nTimepoints/2 + (centerPpm(iMetabolite) - searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
%         snrValuenoTrunc(iMetabolite) = max( abs(Data(ppmEndMet:ppmStartMet)))/std(Data(((size(Data))-size(Data)*1/4):end));
%     end
%
%     % FWHM, no trunc or filtering
%     ppmStart = bandwidth/scanFreq_MHz/2; ppmEnd = -bandwidth/scanFreq_MHz/2;
%     searchArea = 0.2; %ppm
%     ppmVector = freq/scanFreq_MHz;
%     fwhmnoTrunc = zeros(length(metaboliteNames),1);
%     for iMetabolite = 1:length(metaboliteNames)
%         ppmStartMet = centerPpm(iMetabolite) + searchArea;
%         ppmEndMet = centerPpm(iMetabolite) - searchArea;
%         spectrumPeak = Data;
%         spectrumPeak(ppmVector>ppmStartMet | ppmVector < ppmEndMet) = 0;
%         [fwhmnoTrunc(iMetabolite), ~] = fwhm_Hz(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
%     end
%
%     % LCModel Export
%     FID_MR_SpectroS.Parameter.ReconFlags.isCoilsCombined = 1;
%     FID_MR_SpectroS.Parameter.ReconFlags.isAveraged = 1;
%     filenamenoTrunc = fullfile(outputPath, strcat('Summed_TE', num2str(TEs{indexTE}), 'ms_noTrunc'));
%     FID_MR_SpectroS.ExportLcmRaw('',filenamenoTrunc, true, 'Yes', 15, 0.05);
%
%     % save MR_SpectroS file, no trunc
%     data2save_noTrunc = fullfile(outputPath, strcat('Summed_TE', num2str(TEs{indexTE}), 'ms_noTrunc.mat'));
%     save(data2save_noTrunc, 'FID_MR_SpectroS')
%
%     %% truncate after trunc_ms
%     truncPoint = floor( trunc_ms/(FID_MR_SpectroS.Parameter.Headers.DwellTimeSig_ns*1e-6));
%     FID_MR_SpectroS_Trunc = FID_MR_SpectroS.Truncate(truncPoint);
%     % phase zero correction again
%     FID_MR_SpectroS_Trunc = FID_MR_SpectroS_Trunc.Phase0_correction_31P;
%     FIDsTESummed_Trunc90ms{1,indexTE} = FID_MR_SpectroS_Trunc;
%
%     figure
%     plot(freq/scanFreq_MHz,(real(fftshift(fft(FID_MR_SpectroS_Trunc.Data{1})))))
%     axis([-20 8 -inf inf])
%     set(gca,'xdir','reverse')
%     xlabel 'f / ppm'
%     title (['TE ', num2str(TEs{indexTE}), ' ms  ', 'Summed ', 'trunc ',num2str(trunc_ms),' ms'], 'Interpreter', 'none')
%     fig2save_Trunc = fullfile(outputPath, strcat('TE', num2str(TEs{indexTE}),'ms_', 'Trunc',num2str(trunc_ms),'ms', '.fig'));
%     savefig(fig2save_Trunc)
%
%     % SNR in frequency domain, no trunc or filtering
%     DataTrunc = FID_MR_SpectroS_Trunc.Data{1};
%     DataTrunc = fftshift(fft(DataTrunc));
%     searchArea = 0.2; %ppm
%     snrValueTrunc = zeros(length(metaboliteNames),1);
%     for iMetabolite = 1:length(metaboliteNames)
%         ppmStartMet = round(nTimepoints/2 + (centerPpm(iMetabolite) + searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
%         ppmEndMet = round(nTimepoints/2 + (centerPpm(iMetabolite) - searchArea)*scanFreq_MHz/bandwidth*nTimepoints);
%         snrValueTrunc(iMetabolite) = max( abs(DataTrunc(ppmEndMet:ppmStartMet)))/std(DataTrunc(((size(DataTrunc))-size(DataTrunc)*1/4):end));
%     end
%
%     % FWHM, no trunc or filtering
%     ppmStart = bandwidth/scanFreq_MHz/2; ppmEnd = -bandwidth/scanFreq_MHz/2;
%     searchArea = 0.2; %ppm
%     ppmVector = freq/scanFreq_MHz;
%     fwhmTrunc = zeros(length(metaboliteNames),1);
%     for iMetabolite = 1:length(metaboliteNames)
%         ppmStartMet = centerPpm(iMetabolite) + searchArea;
%         ppmEndMet = centerPpm(iMetabolite) - searchArea;
%         spectrumPeak = DataTrunc;
%         spectrumPeak(ppmVector>ppmStartMet | ppmVector < ppmEndMet) = 0;
%         [fwhmTrunc(iMetabolite), ~] = fwhm_Hz(spectrumPeak, ppmStart, ppmEnd, nTimepoints, scanFreq_MHz);
%     end
%
%     % LCModel Export
%     FID_MR_SpectroS_Trunc.Parameter.ReconFlags.isCoilsCombined = 1;
%     FID_MR_SpectroS_Trunc.Parameter.ReconFlags.isAveraged = 1;
%     filenameTrunc = fullfile(outputPath, strcat('Summed_TE', num2str(TEs{indexTE}), 'ms_Trunc',num2str(trunc_ms),'ms'));
%     FID_MR_SpectroS_Trunc.ExportLcmRaw('',filenameTrunc, true, 'Yes', 15, 0.05);
%
%     % save MR_SpectroS file, no trunc
%     data2save_Trunc = fullfile(outputPath, strcat('Summed_TE', num2str(TEs{indexTE}), 'ms_Trunc',num2str(trunc_ms),'ms.mat'));
%     save(data2save_Trunc, 'FID_MR_SpectroS_Trunc')
%
%     %% save SNR/FWHM table
%     snrValues = [snrValuenoTrunc(1) snrValuenoTrunc(2) snrValuenoTrunc(3) snrValuenoTrunc(4) snrValuenoTrunc(5) snrValuenoTrunc(6) snrValuenoTrunc(7) snrValuenoTrunc(8) snrValuenoTrunc(9) ;...
%         fwhmnoTrunc(1) fwhmnoTrunc(2) fwhmnoTrunc(3) fwhmnoTrunc(4) fwhmnoTrunc(5) fwhmnoTrunc(6) fwhmnoTrunc(7) fwhmnoTrunc(8) fwhmnoTrunc(9) ;...
%         snrValueTrunc(1) snrValueTrunc(2) snrValueTrunc(3) snrValueTrunc(4) snrValueTrunc(5) snrValueTrunc(6) snrValueTrunc(7) snrValueTrunc(8) snrValueTrunc(9);...
%         fwhmTrunc(1) fwhmTrunc(2) fwhmTrunc(3) fwhmTrunc(4) fwhmTrunc(5) fwhmTrunc(6) fwhmTrunc(7) fwhmTrunc(8) fwhmTrunc(9) ];
%     rowNames = {'SNR','FWHM','SNR Truncated','FWHM Truncated'};
%     colNames = {'PE','PC','Piint','GPE','GPC','PCr','g_ATP','a_ATP','NADH'};
%     SNR = array2table(snrValues,'RowNames',rowNames,'VariableNames',colNames)
%
%     fileName = fullfile(outputPath, strcat('TE', num2str(TEs{indexTE}), 'ms_SNR'));
% %     save(fileName, 'SNR');
% end

%% LCModel Fitting
extensionTable = '.table';
extensionCoord = '.coord';
defaultTE = 'XXXX';
defaultSubjectsPath = 'YYYY';
defaultSubject ='ZZZZ';
defaultTrunc = 'Trunc90ms';
defaultLCModelUser = 'jdorst';

controlFilesBaseSuffix = '.control';
outputFileNameBase = strcat(defaultTE, '_');

%% file paths setup
controlFilesPathRemote = '/Desktop/31P_T2_Paper/LCModelConfig/';
controlFilesPathLocal =  [localFilePathBase, 'LCModelConfig/'];
LCModelOutputFilesPathRemote = '/Desktop/31P_T2_Paper/Output/';
LCModelOutputPath = [localFilePathBase, 'Output/'];

defaultControlFile = strcat(controlFilesBase, controlFilesBaseSuffix);

summedPath = 'Summed';

subjectsControlFilesPathRemote = strcat(controlFilesPathRemote, summedPath, '/');
subjectsControlFilesPathLocal = strcat(controlFilesPathLocal, summedPath, '/');
preprocessedFilesPathLocal = strcat(localFilePathBase, summedPath, '/');
subjectsLCModelOutputFilesRemote = strcat(LCModelOutputFilesPathRemote, summedPath, '/');
subjectsLCModelOutputPath = strcat(LCModelOutputPath, summedPath, '/');

%% basic configurations
if ~isempty(strfind(controlFilesBase,'_noBCoupl')) && isempty(strfind(controlFilesBase,'_noAGCoupl'))
    LCModelOutputFiles = strcat('Summed_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noBCoupl');
elseif ~isempty(strfind(controlFilesBase,'_noAGCoupl')) && isempty(strfind(controlFilesBase,'_noBCoupl'))
    LCModelOutputFiles = strcat('Summed_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noAGCoupl');
elseif ~isempty(strfind(controlFilesBase,'_noBCoupl_noAGCoupl'))
    LCModelOutputFiles = strcat('Summed_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms_noBCoupl_noAGCoupl');
else
    if ~isempty(strfind(controlFilesBase,'_Ideal'))
        LCModelOutputFiles = strcat('Summed_STEAM_Ideal_XXXX_TM5ms_LW_Trunc90ms');
    elseif ~isempty(strfind(controlFilesBase,'_Real'))
        LCModelOutputFiles = strcat('Summed_STEAM_Real_XXXX_TM5ms_LW_Trunc90ms');
    end
end

%
numberOfSubjects = length(subjects);
% TEsCell = num2cell(TEs);
%% do the LCModel fitting
% % LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

% do the actual LCModel fitting
%% do the LCModel fitting
LCModelCallerInstance = startLCModel(controlFilesPathRemote, LCModelOutputFilesPathRemote, LCModelOutputPath);

LCModelCallerInstance = LCModelCallerInstance.ConfigurePaths(subjectsControlFilesPathRemote, subjectsLCModelOutputFilesRemote, ...
    subjectsLCModelOutputPath);

%% create the control file series
for indexCurrentTE = 1:numberOfTEs
    if ~isempty(strfind(STEAMVersion,'Ideal_JD'))
        currentTE = strcat('JD_TE',num2str((TEs{indexCurrentTE})),'ms');
    else
        currentTE = strcat('TE',num2str((TEs{indexCurrentTE})),'ms');
    end
    currentOutputFile = strrep(LCModelOutputFiles, defaultTE, currentTE);
    defaultOutputFile = 'defaultOutputFile';
    createLCModelConfig_31P_T2(controlFilesPathLocal, subjectsControlFilesPathLocal, defaultControlFile, ...
        summedPath, defaultSubjectsPath, 'Summed', defaultSubject,...
        currentTE,defaultTE,currentOutputFile,defaultOutputFile,TruncToFit,defaultTrunc);
end
if ~isempty(strfind(STEAMVersion,'Ideal_JD'))
    currentControlFiles = strrep(strrep(strrep(defaultControlFile,defaultTE,strcat('JD_TE',TEs,'ms')),defaultSubject,'Summed'),defaultTrunc,TruncToFit);
    currentOutputFiles  = strrep(strrep(LCModelOutputFiles, defaultTE, strcat('JD_TE',TEs,'ms')),defaultTrunc,TruncToFit);
else
    currentControlFiles = strrep(strrep(strrep(defaultControlFile,defaultTE,strcat('TE',TEs,'ms')),defaultSubject,'Summed'),defaultTrunc,TruncToFit);
    currentOutputFiles  = strrep(strrep(LCModelOutputFiles, defaultTE, strcat('TE',TEs,'ms')),defaultTrunc,TruncToFit);
end
fittingLCModel(LCModelCallerInstance, currentControlFiles, currentOutputFiles, subjectsControlFilesPathRemote, ...
    subjectsControlFilesPathLocal, preprocessedFilesPathLocal);


createConcTable(numberOfSubjects,LCModelOutputFiles,LCModelOutputPath,defaultTE,TEs,prefixes,subjects,summedPath,TruncToFit,defaultTrunc,STEAMVersion)

end

%% create concentration table
function createConcTable(numberOfSubjects,LCModelOutputFiles,LCModelOutputPath,defaultTE,TEs,prefixes,subjects,subjectsPath,TruncToFit,defaultTrunc,STEAMVersion)
tableConcentrations = [];
isFirstIter = 1;
s = 1;
for indexCurrentTE = 1:length(TEs)
    for indexCurrentSubject = 1:numberOfSubjects
        % retrieve parameters
        if ~isempty(strfind(STEAMVersion,'Ideal_JD'))
            currentTE = strcat('JD_TE',num2str((TEs{indexCurrentTE})),'ms');
        else
            currentTE = strcat('TE',num2str((TEs{indexCurrentTE})),'ms');
        end
        currentOutputFiles = strrep(strrep(LCModelOutputFiles, defaultTE, currentTE),defaultTrunc,TruncToFit);
        currentPath = strrep(strcat(LCModelOutputPath,subjectsPath,'/',currentOutputFiles,'.table'),defaultTrunc,TruncToFit);
        [c1 c2 c3 c4] = textread(currentPath,'%s %s %s %s');
        
        if isFirstIter
            start  = find(strcmp(c1,'Conc.'));
            start  = start + 1;
            finish = find(strcmpi(c1,'$$MISC')) - 1;
            tableConcentrations{1,1} = 'ID';
            
            s = 2;
            for j=start:finish
                if(strcmp(c4{j},'')==1)
                    c3_split = strsplit(c3{j},'+');
                    if length(c3_split) == 1
                        c3_split = strsplit(c3{j},'-');
                    end
                    tableConcentrations{1,s} = c3_split{2};
                else
                    tableConcentrations{1,s} = c4{j};
                    if length(tableConcentrations{1,s}) == 3 & tableConcentrations{1,s} == 'Leu'
                        tableConcentrations{1,s} = 'MMB';
                    end
                end
                tableConcentrations{1,s+1} = 'CRLB %';
                s          = s + 2;
            end
        end
        
        % add ID to table
        tableConcentrations{((indexCurrentTE-1)+2),1} = strcat('Summed_', strcat('TE',num2str((TEs{indexCurrentTE})),'ms'));
        % add metabolites to table
        s = 2;
        for j=start:finish
            %add quantified metabolite
            tableConcentrations{((indexCurrentTE-1)+2),s} = str2num( c1{j});
            %add CRLB
            tableConcentrations{((indexCurrentTE-1)+2),s+1} = str2num(  c2{j}(1:end-1));
            s = s+2;
        end
        
        isFirstIter = false;
        
    end
end

% save table to file
if ~isempty(strfind(STEAMVersion,'Ideal_JD'))
    ExpName = strrep(strcat('LCModelFit_', strrep(LCModelOutputFiles,strcat(defaultTE,'_'),'JD_')),defaultTrunc,TruncToFit);
else
    ExpName = strrep(strcat('LCModelFit_', strrep(LCModelOutputFiles,strcat(defaultTE,'_'),'')),defaultTrunc,TruncToFit);
end
% ExpName = strrep(strcat('LCModelFit_', strrep(LCModelOutputFiles,strcat(defaultTE,'_'),'')),defaultTrunc,TruncToFit);
exportFileXlsx = [LCModelOutputPath ,strcat(ExpName,'.xlsx')];
xlswrite(exportFileXlsx, {LCModelOutputPath}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsx, tableConcentrations, 1, cellPosition);

exportFileMat = [LCModelOutputPath ,strcat(ExpName,'.mat')];
eval([ExpName '= tableConcentrations']);        %use of eval not recommended, change it
save(exportFileMat, ExpName)

close all
end