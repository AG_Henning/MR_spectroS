function createLCModelConfigsTEseriesChosen_pHs(controlFilesPath, defaultControlFile, ...
    orderTEs, defaultTE, defaultLCModelUser, currentLCModelUser, subjects, defaultSubject,...
    pH_Values, default_pH)
% Creates the control files for the TE series, given a default control file.
% the newly generated files will be placed in the same path as indicated by
% the controlFilesPath.
% The defaultControlFile has to include the keyword "Default" in the file name.
% pH_Values have to be specified for each subject
% A sample input:
% controlFilesPath = 'Fitsettings/MMocc_noExtraMM_06knots/';
% defaultControlFile = 'fitsettingsDefault.control'

orderTEsLong = strcat('TE', orderTEs);
defaultKeyword = 'Default';
indexFind  = strfind(defaultControlFile, defaultKeyword);
if isempty(indexFind)
    error('The default control file should have the keyword "Default" in its file name.');
end

fitAllSubjects = false;
numOfSubjects = 1;
if  exist('subjects','var') && exist('defaultSubject','var')
    if ~isempty(subjects)
        if ~contains(defaultControlFile, defaultSubject)
            error('The default control file should have the keyword %s in its file name when using subject fitting.', defaultSubject);
        end
        subjectsControlFilesPath = strcat(controlFilesPath, subjects, '/');
        
        fitAllSubjects = true;
        numOfSubjects = length(subjects);
        % create the folders for the individual subjects
        for indexSubject = 1:numOfSubjects
            mkdir(subjectsControlFilesPath{indexSubject})
        end
    end
end

controlFileBase = defaultControlFile(1:indexFind-1);
controlFileSuffix = defaultControlFile(indexFind+length(defaultKeyword):end);

fitSettingsDefaultFileID = fopen([controlFilesPath defaultControlFile], 'r');

%iterate over all TEs
for index = 1:length(orderTEsLong)
    %iterate over all subjects, if we have it set. Otherwise the loop is executed only once
    for indexSubject = 1:numOfSubjects
        if fitAllSubjects
            % add the subject name to the output path, and the control file name
            subjectControlFileBase = strrep(controlFileBase, defaultSubject, subjects{indexSubject});
            outputFile = strcat(subjectsControlFilesPath{indexSubject}, subjectControlFileBase, orderTEsLong{index}, controlFileSuffix);
        else
            outputFile = strcat(controlFilesPath, controlFileBase, orderTEsLong{index}, controlFileSuffix);
        end
        % pH
        outputFile = strrep(outputFile, default_pH, pH_Values{indexSubject});
        
        fitSettingsFileID = fopen(outputFile, 'w');
        display(outputFile);
        
        while(~feof(fitSettingsDefaultFileID))
            s = fgetl(fitSettingsDefaultFileID);
            s = strrep(s, defaultLCModelUser, currentLCModelUser);
            s = strrep(s, defaultTE, orderTEsLong{index});
            if fitAllSubjects
                s = strrep(s, defaultSubject, subjects{indexSubject});
            end
            s = strrep(s, default_pH, pH_Values{indexSubject});
            
            fprintf(fitSettingsFileID,'%s\n', s);
        end
        frewind(fitSettingsDefaultFileID);
        fclose(fitSettingsFileID);
    end
end
fclose(fitSettingsDefaultFileID);
end