function this =  AlignWaterToPpm(this)

%keep in mind, that when you are displaying the 3rd digit precision might
%not be visible, due to not that high zerofilling in the plot

%% setup the settings for the FrequencyAlignFreqDomain
this.Parameter.FreqAlignFreqDomainSettings.selectedCoil = 1; % check for max SNR coil
this.Parameter.FreqAlignFreqDomainSettings.selectedMix = 1;

%indices of Interest: given in ppm
% water peak
this.Parameter.FreqAlignFreqDomainSettings.peaksInPpm  = [2.008];

%set zeroFillingParameter to get smooth approximations
this.Parameter.FreqAlignFreqDomainSettings.zeroFillFactor = 50;

%search area (+-) in ppm
this.Parameter.FreqAlignFreqDomainSettings.searchArea = 0.3;
%spline filtering is not needed if the spectra are not too noisy.
%flag to do spline filtering
this.Parameter.FreqAlignFreqDomainSettings.doSplineSmoothing = false;
%spline filtering coefficient
this.Parameter.FreqAlignFreqDomainSettings.splineSmoothingCoeff = 0.01;

%% do the actual Frequency Alignment
this = this.FrequencyAlignFreqDomain;

this.Parameter.ReconFlags.isAlignWaterToPpm  = true;