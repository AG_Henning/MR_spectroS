function [fwhm, warningFlag] = fwhm_Hz(peakSpectrum, ppmStart, ppmEnd, dataPoints, scanFrequency, interpFactor)
warningFlag = false;
if nargin < 3
    error('The function fwhm_Hz requires minimum 2 input parameters. The parameters are: peakSpectrum, bandwidth, dataPoints, interpFactor')
end
if (~exist('interpFactor', 'var'))
    interpFactor  = 20; %default value
end

indeces       = [];
i = 1;

while (length(indeces)<2 && i <4)
    %create a temporary interpolated peak
    peakTmp         = interp(peakSpectrum,interpFactor*i);
    %get the position of the maximum
    [~,  maxPos] = max(abs(peakTmp));
    halfMaxima = peakTmp(maxPos)/2;
    % normalize all the spectrum to the half maxima, to have that
    % be ~ 1.000
    normValues   = peakTmp / halfMaxima;
    %extract the indices which would match the half maxima
    indeces      = find(normValues> 0.995 & normValues< 1.005);
    %iterate again if not enough precise
    i            = i+1;
end

interpFactor    = interpFactor*(i-1);

factor = (ppmStart - ppmEnd)* scanFrequency / (dataPoints * interpFactor) ;
%find the two points being in the middle
lhs_point = mean(indeces(indeces<maxPos));
rhs_point = mean(indeces(indeces>maxPos));

%handling the different situations
if ~isnan(lhs_point) 
    if ~isnan(rhs_point)
        % peak well defined
        fwhm  = (rhs_point - lhs_point)*factor;
    else
        warning('rhs - is probably out of our spectrum; we estimate the fwhm with the lhs_point');
        warningFlag = true;
        fwhm = 2*(maxPos - lhs_point)*factor;
    end
else
    if ~isnan(rhs_point)
        warning('rhs - is probably out of our spectrum; we estimate the fwhm with the lhs_point');
        warningFlag = true;
        fwhm = 2*(rhs_point - maxPos)*factor;
    else
        % something went wrong and we don't have a peak
        warning('Something went wrong. FWHM of the peak could not be estimated');
        warningFlag = true;
        fwhm = NaN;
    end
end
    
