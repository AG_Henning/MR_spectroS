function basisSetApplyLineWidth_31P_T2_Normalized(TEs)

%%%%%%%%%%%
% Check filePathImport and filePathExport
%%%%%%%%%%%


plotBasis = false;

metaboliteFileNames = {'phosphocreatine_31P' 'ATP_31P_short_gamma_no_a-g-coupling' ...
    'ATP_31P_short_alpha_no_a-g-coupling' 'GPC_31P_short' 'GPE_31P_short' 'inorganic_phosphate_intra_31P'...
    'inorganic_phosphate_extra_31P' 'phosphocholine-31P_short' 'phosphoethanolamine_31P_short' 'NADH_31P' 'NAD+_31P'};
metaboliteLabels =     {'PCr' 'ATP-gamma' 'ATP-alpha' 'GPC' 'GPE' 'Pi intra' 'Pi extra' 'PC' 'PE' 'NADH' 'NAD+'};
metaboliteNames =      {'Cr'  'ATP-g'     'ATP-a'     'GPC' 'GPE' 'Pi_in'    'Pi_ext'   'PC' 'PE' 'NADH' 'NAD'};
metaboliteLineWidths = [  10    120         60          18     19     80          80      50    50     50    50];% Hz, for single volunteer basis sets
metaboliteLineWidths = [  10    100         80          20     20     50          30      50    30     20    40];% Hz, for single volunteer basis sets
Phases = {0; -2.5; 33.25; 3; 3.25; -2; -4.75; -15.25; -22.5; 42; 45; };     %tested with Vespa Singlet simulation
Phases = {0; -2.5; 33.25; 3; 3.25; -2; -4.75; -15.25; -22.5; 0; 0; };     %performs better w/o NAD correction

minLineWidth =  min(metaboliteLineWidths);
adjustedLineWidth = metaboliteLineWidths - minLineWidth; %Hz

lineWidthType = 'lorentz'; %'lorentz' 'gauss'

pathName = 'T2 31P Paper Basis_sets';
sampleCriteria = {};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);

for indexTE = 1:length(TEs)
%     filePathImport = [localFilePathBase, strcat('STEAM_Ideal_TE', num2str(TEs(indexTE)), 'ms_TM5ms')];
%     filePathImport = [localFilePathBase, strcat('STEAM_Ideal_JD_TE', num2str(TEs(indexTE)), 'ms_TM5ms')];
    filePathImport = [localFilePathBase, strcat('STEAM_Real_TE', num2str(TEs(indexTE)), 'ms_TM5ms')];
    
    for indexMetabolite = 1 : length(metaboliteFileNames)
        filtered_metabolite_name = [metaboliteFileNames{indexMetabolite} '_LW'];
        %
        %read in the metabolites
        [oldData, bandwidth, scanFrequency, metaboliteName, singletPresentMix] = ...
            ImportLCModelBasis(filePathImport, strcat(metaboliteFileNames{indexMetabolite},'.RAW'), plotBasis, '31P');
        
        dwellTime = 1/bandwidth;
        addSinglet = true;
        %adjust linewidth
        
        %correct strange Vespa phase
        tPhase = [-4096/2:(4096/2-1)]/4096;
        phaseData = oldData.*exp(-1i*Phases{indexMetabolite}*pi/180*tPhase');
        
        t = (0:length(oldData)-1)*dwellTime;
        
        %apply in vivo linewidth
        switch lineWidthType
            case 'gauss'
                lineWidthFilter = exp ( - t'.^2*(adjustedLineWidth(indexMetabolite))^2 );
            case 'lorentz' %exponential
                lineWidthFilter = exp(-t'*adjustedLineWidth(indexMetabolite));
            otherwise
                error('Line width type not defined');
        end
        lineWidthData = phaseData .* lineWidthFilter;
%         normFactor = sum((real(fftshift(fft(lineWidthData)))))/400;       %normalize real to 400
        normFactor = sum((abs(fftshift(fft(lineWidthData)))))/400;       %normalize abs to 400
        lineWidthDataNorm = lineWidthData./normFactor;
        
        dwellTimeMs = dwellTime * 1e3;
        
%         filePathExport = [localFilePathBase strcat('STEAM_Ideal_TE', num2str(TEs(indexTE)), 'ms_TM5ms\LW_Summed\')];
%         filePathExport = [localFilePathBase strcat('STEAM_Ideal_TE', num2str(TEs(indexTE)), 'ms_TM5ms\LW\')];
%         filePathExport = [localFilePathBase strcat('STEAM_Ideal_JD_TE', num2str(TEs(indexTE)), 'ms_TM5ms\LW\')];
        filePathExport = [localFilePathBase strcat('STEAM_Real_TE', num2str(TEs(indexTE)), 'ms_TM5ms\LW\')];
        mkdir(filePathExport)
        ExportLCModelBasis(lineWidthDataNorm', dwellTimeMs, scanFrequency, filePathExport, ...
            filtered_metabolite_name, metaboliteNames{indexMetabolite}, addSinglet, ...
            false, -15, 0.25);
    end
end
end
