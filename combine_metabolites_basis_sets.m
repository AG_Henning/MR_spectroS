function combine_metabolites_basis_sets()

plotBasis = true;

metabolitesToMix = { {'NAA_1'; 'NAA_2'}; ...
    {'GPC'; 'PCh'}};
combined_metabolite_names = {'NAA_comb'; 'tCho_comb'};
% contains the scaling factors of 'metabolitesToMix'
factorsToMix = {{1; 1};...
    {0.625; 0.375}};


pathName = 'DF data path';
sampleCriteria = {};
[localFilePathBase] = pathToDataFolder(pathName, sampleCriteria);
filePathBase = [localFilePathBase 'Basis_sets\final_T2_met_MM_paper\sLASER_new_UF_7ppm\TE'];
orderTEs = {'24' '32' '40' '52' '60'};
filePaths = strcat(filePathBase, orderTEs , '\');

for indexTE = 1 : length(orderTEs)
    filePath = filePaths{indexTE};
    for indexMetabolite = 1 : length(combined_metabolite_names)
        combined_metabolite_name = combined_metabolite_names{indexMetabolite};
        %
        dataMix           = cell(1,length(factorsToMix{indexMetabolite}));
        metaboliteNameMix = cell(1,length(factorsToMix{indexMetabolite}));
        singletPresentMix = cell(1,length(factorsToMix{indexMetabolite}));
        %read in non pH dependent metabolite parts
        for indexMetabolitesToMix =  1:length(metabolitesToMix{indexMetabolite})
            [dataMix{indexMetabolitesToMix}, bandwidth, scanFrequency, metaboliteNameMix{indexMetabolitesToMix}, ...
                singletPresentMix{indexMetabolitesToMix}] = ...
                ImportLCModelBasis(filePath, metabolitesToMix{indexMetabolite}{indexMetabolitesToMix}, ...
                plotBasis);
        end
        %combine
        combinedData = dataMix{1} * factorsToMix{indexMetabolite}{1};
        for indexMetabolitesToMix =  2:length(metabolitesToMix{indexMetabolite})
            % average the contributions. Don't worry about the
            % ref. singlet, it does not influence the fitting
            combinedData = combinedData + factorsToMix{indexMetabolite}{indexMetabolitesToMix} * ...
                dataMix{indexMetabolitesToMix};
        end
        
        dwellTimeMs = 1/bandwidth * 1e3;
        addSinglet = false;

        ExportLCModelBasis(combinedData', dwellTimeMs, scanFrequency, filePath, ...
                combined_metabolite_name, combined_metabolite_name, addSinglet);
    end
end
