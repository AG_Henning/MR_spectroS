function updateDisplayProperties(handles)

%//////////////////////////////////////////////////////////////////////////
%                  UPPER ROW TEXTS
%//////////////////////////////////////////////////////////////////////////

   

   this        = handles.DisplayProperties;
   
   filenameString = this.filename;
   set(handles.textNameData,'String',sprintf('Filename: %s',filenameString));
   
   nTimepoints = this.nTimepoints;
   set(handles.text_points,'String', sprintf('#Timepoints: %d',nTimepoints));
   
   nCoils      = this.nCoils;
   set(handles.text_coils,'String', sprintf('#Coils: %d',nCoils));
   
   nDyns       = this.nDyns;
   set(handles.text_dyns,'String', sprintf('#Dynamics: %d',nDyns));
   
   nAverages   = this.nAverages;
   
   set(handles.text_means,'String', sprintf('#Averages: %d',nAverages));
   
   nMixes      = this.nMixes;
   set(handles.text_mixes,'String', sprintf('#Mixes: %d',nMixes));
   set(handles.text_means2,'Visible','off');
   

   
   if nMixes>1
       %n_means2=obj.Parameter.Encoding.NrFids(2); I need to fix that!
       nAverages2= this.nAverages2;
       set(handles.text_means2,'String', sprintf('#Averages2: %d', nAverages2));
       set(handles.text_means2,'Visible','on');
       set(handles.popupmenu_mix,'Enable','on');
    
       
 %/////////////////////////////////////////////////////////////////////////
 %                     POPMENU STRING
 %/////////////////////////////////////////////////////////////////////////
       for i=1:nMixes
           s{i}=num2str(i);
       end
       set(handles.popupmenu_mix,'String',s)
       
   else
       set(handles.popupmenu_mix,'Enable','off');
      
   end

%//////////////////////////////////////////////////////////////////////////
%                             SLIDERS-EDITS
%//////////////////////////////////////////////////////////////////////////



   set(handles.slider_coils,'Max',nCoils);
   
   if nCoils <1
       set(handles.edit_coils,'Visible','off');
       set(handles.slider_coils,'Visible','off');
   elseif nCoils <2
       set(handles.slider_coils,'Visible','off');
   else 
       set(handles.slider_coils,'Visible','on');
       set(handles.edit_coils,'Visible','on');
       set(handles.slider_coils,'SliderStep',[1 1]/(nCoils-1));
       
   end
   
   
   
   
   set(handles.slider_dyns,'Max',nDyns);
   if nDyns <1
       set(handles.edit_dyns,'Visible','off');
       set(handles.slider_dyns,'Visible','off');
   elseif nDyns<2
       set(handles.slider_dyns,'Visible','off');
       set(handles.edit_dyns,'Visible','on');
       set(handles.edit_dyns,'Enable','off');
   else
       set(handles.edit_dyns,'Visible','on');
       set(handles.slider_dyns,'Visible','on');
       set(handles.slider_dyns,'SliderStep',[1 1]/(nDyns-1));
   end
    
   
   
   
   set(handles.slider_means,'Max',nAverages);
   if nAverages <1
       set(handles.edit_means,'Visible','off');
       set(handles.slider_means,'Visible','off');
   elseif nAverages<2
       set(handles.slider_means,'Visible','off');
       set(handles.edit_means,'Visible','on');
       set(handles.edit_means,'Enable','off');
   else
       set(handles.edit_means,'Visible','on');
       set(handles.slider_means,'Visible','on');
       set(handles.edit_means,'Enable','on');
       set(handles.slider_means,'SliderStep',[1 1]/(nAverages-1));
   end
   
   


%//////////////////////////////////////////////////////////////////////////
%                   ACQUISITION TIME + BANDWIDTH
%//////////////////////////////////////////////////////////////////////////
    aquisitionTime= this.acquisitionTime;
 
    set(handles.edit_readout,'String',num2str(aquisitionTime));
    set(handles.edit_readout,'Enable','off');
    
    bandwidth= this.bandwidth;
    set(handles.edit_bandwidth, 'String', num2str(bandwidth));
    
%     if bandwidth==0
%         set(handles.edit_bandwidth, 'Enable', 'off');
%     else
%          set(handles.edit_bandwidth, 'Enable', 'on');
%     end
    

%//////////////////////////////////////////////////////////////////////////
%                                BUTTONS
%//////////////////////////////////////////////////////////////////////////

     isDataRead = this.isDataRead;
     if ~isDataRead
        set(handles.button_plotsnr,'Enable','off');
        set(handles.pushbutton_noise_corr,'Enable','off');
     else
        set(handles.button_plotsnr,'Enable','on');
        set(handles.pushbutton_noise_corr,'Enable','on');
     end

%//////////////////////////////////////////////////////////////////////////
%                                POPUPMENUS
%//////////////////////////////////////////////////////////////////////////

     isDataRead = this.isDataRead;
     if ~isDataRead
        set(handles.popupmenu_mode,'Enable','off');
        set(handles.popupmenu_type,'Enable','off');
     else
        set(handles.popupmenu_mode,'Enable','on');
        set(handles.popupmenu_type,'Enable','on');
         
     end



end

