classdef FreqAlignFreqDomainSettings < hgsetget
    
    
    properties
        selectedCoil
        selectedMix
        peaksInPpm
        zeroFillFactor
        searchArea
        doSplineSmoothing
        splineSmoothingCoeff
        freqShifts
    end
    
    methods
        
        function this= FreqAlignFreqDomainSettings()
            this.selectedCoil = 1; % check for max SNR coil
            this.selectedMix = 1;

            %indices of Interest: given in ppm
            %NAA, tCr-CH3 (average of Cr [3.027], PCr [3.029])
            %tCho not taken, since Cho = 3.185, GPC = 3.212, PCho = 3.208
            this.peaksInPpm  = [2.008; 3.028];

            %set zeroFillingParameter to get smooth approximations
            this.zeroFillFactor = 100;

            %search area (+-) in ppm
            this.searchArea = 0.2;
            %flag to do spline filtering
            this.doSplineSmoothing = true;
            %spline filtering coefficient
            this.splineSmoothingCoeff = 0.01;
            this.freqShifts =[];
        end
        
    end
    
end