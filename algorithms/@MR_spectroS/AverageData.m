function this = AverageData( this )
% version: 26 Feb 2014 

if this.Parameter.ReconFlags.isAveraged 
    warning('The data is already averaged')
end


 nCoils        = size(this.Data{1},this.coil_dim);
 nDyns         = size(this.Data{1},this.dyn_dim);
 nMixes        = size(this.Data{1},this.mix_dim);
 nAverages     = size(this.Data{1},this.meas_dim);
 oldData       = this.Data{1};

 
 nBlocks       = this.Parameter.AverageSettings.nBlocks;
 
 if mod(nAverages,nBlocks)~=0
     error('The number of measurements should be multiple of the number of blocks')
 end
 

 averagesPerBlock            = nAverages/nBlocks;
 newDataSize                 = size(oldData);
 

 newDataSize(this.meas_dim)  = nBlocks;

     
 
 
 newData                     = zeros(newDataSize);

 
 for indexOfBlocks=1:nBlocks
    
     newData(:,1,1,:,1,1,1,1,1,1,1,indexOfBlocks)     =... 
         sum(oldData(:,1,1,:,1,1,1,1,1,1,1,(1:averagesPerBlock)+(indexOfBlocks-1)*averagesPerBlock),this.meas_dim);

     if size(oldData,this.mix_dim)~=1  %Average the mix 2 (water signals) as well
        newData(:,1,1,:,1,1,1,1,2,1,1,indexOfBlocks)  =...
         sum(oldData(:,1,1,:,1,1,1,1,2,1,1,(1:averagesPerBlock)+(indexOfBlocks-1)*averagesPerBlock),this.meas_dim);
     end
 
 end

 this.Parameter.ReconFlags.isAveraged                 = true;
 this.Data{1}                                         = newData;
 this.Parameter.EddyCurrSettings.numberOfWaterSignals = nBlocks; %TODO rename this to number of blocks and it shouldn't be here...
end

