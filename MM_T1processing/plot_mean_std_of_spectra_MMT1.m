function plot_mean_std_of_spectra_MM

pathName = 'MM data path';
[path] = pathToDataFolder(pathName);

fileNameBase = [path, 'Summed_spectra_'];

TEs = [24; 32; 40; 52; 60];
TI1 = [];
TI2 = [];

colors = {[0 0.7 0]; [1 0 0]; [0 0 1]; [0 0 0]; [0 0.5 0.5];};
numberOfTEs = length(TEs);
offsetStep = 6e-6;
offSetBase = offsetStep * (numberOfTEs + 1); 

for indexTE = 1:numberOfTEs
    fileName = [fileNameBase num2str(TEs(indexTE))];
    offset = offSetBase - indexTE * offsetStep;
    plotOneTE(fileName, TEs(indexTE), colors{indexTE}, offset);
end

end

function plotOneTE(fileName, TE, colorSpectrum, offset)
load([fileName '.mat'], 'summedSpectraTE')

zeroPPM = 4.67;
nTimepoints = size(summedSpectraTE.Data{1},summedSpectraTE.kx_dim);
nAverages = size(summedSpectraTE.Data{1},summedSpectraTE.meas_dim);
waterFreq  = summedSpectraTE.Parameter.Headers.ScanFrequency * 1E-6;
bandwidth  = summedSpectraTE.Parameter.Headers.Bandwidth_Hz;
frequencyRange    = linspace(-bandwidth/2,bandwidth/2,nTimepoints);

fidSpectra = summedSpectraTE.Data{1}(:,1,1,1,1,1,1,1,1,1,1,:);
fidWater = summedSpectraTE.Data{1}(:,1,1,1,1,1,1,1,2,1,1,:);
spectra = squeeze(fftshift(fft(fidSpectra)));
water = squeeze(fftshift(fft(fidWater)));
meanSpectrum = mean(spectra,2);
stdSpectrum = std(spectra,[],2);
relativeStd = stdSpectrum ./ max(meanSpectrum) * 100;

ppmScale = (frequencyRange)/waterFreq+zeroPPM;

figure(101)
plot(ppmScale,relativeStd)
xlim([0.5 4.2]);
set(gca,'xDir','reverse')


%%
scalingRange = (ppmScale < 1.8) & (ppmScale > 0);
for indexAverage = 1:nAverages
    [maxWater,indexMaxWater] = max(abs(real(water(:, indexAverage))));
    
    [~, indexMaxWaterInRange] = max(abs(real((water(scalingRange, indexAverage)-mean(water(scalingRange, indexAverage))))));
    
    [maxWaterInRange, ~] = max(real(water(scalingRange, indexAverage)));
    minWaterInRange = min(real(water(scalingRange, indexAverage)));
    
    maxSpectraInRange = max(real(spectra(scalingRange, indexAverage)));
    minSpectraInRange = min(real(spectra(scalingRange, indexAverage)));
    
    indexStart = find(scalingRange);
    indexMaxWaterInRange = indexMaxWaterInRange + indexStart(1);
    referenceLines = zeros(nTimepoints,1);
    referenceLines(:) = -maxWaterInRange*2;
    referenceLines(indexMaxWaterInRange:indexMaxWater + (indexMaxWater - indexMaxWaterInRange)) = maxWaterInRange*2;
    
    plotOffset = maxSpectraInRange;
%     figWaterSpectra = figure(2);
%     hold on
%     plot(ppmScale, real(water(:,indexAverage)) - minWaterInRange + plotOffset,'b')
%     plotScale = (maxWaterInRange-minWaterInRange);
%     ylim([minSpectraInRange-(plotOffset*0.3) plotScale*1.2+plotOffset]);
%     hold on
%     pSpectra = plot(ppmScale, real(spectra(:,indexAverage)),'r');
%     hold on
% %     figure
%     plot(ppmScale, referenceLines, 'k--')
%     xlim([0 8.5]);
%     set(gca,'xDir','reverse')
%     set(gca,'LineWidth',1);
%     % set(gca,'ytick',[])
%     xlabel('[ppm]');
%     makedatatip(pSpectra,[indexMaxWaterInRange])
%     
%     print(['WaterSpectraComparisonPlots\' num2str(indexAverage) '_' fileName],'-dpng')
%     close(figWaterSpectra)
end
%%
% figure
% plot(ppmScale, real(spectra))

% xlim([0.5 4.2]);
% set(gca,'xDir','reverse')
% set(gca,'LineWidth',1);
% % set(gca,'ytick',[])
% xlabel('[ppm]');
% title(['Individual Spectra TE ' num2str(TE) ' ms'])

%%
figure(1)
stdAlpha = 0.4;
hold on
h = area(ppmScale,[(meanSpectrum - stdSpectrum) + offset (stdSpectrum * 2)]);
h(1).FaceColor = 'none';
h(2).FaceColor = colorSpectrum;
h(2).FaceAlpha = stdAlpha;
h(1).EdgeColor = 'none';
h(2).EdgeColor = 'none';
hold on
pSpectrum = plot(ppmScale,meanSpectrum + offset, 'LineWidth',2, 'Color', colorSpectrum);
   
xlim([0.5 4.2]);
set(gca,'xDir','reverse')
set(gca,'LineWidth',1);
set(gca,'ytick',[])
xlabel('[ppm]');
title('Spectra mean and standard deviation')
% title(['Spectra TE ' num2str(TE) ' ms'])
end