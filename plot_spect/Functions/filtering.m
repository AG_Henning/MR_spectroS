function varargout = filtering(varargin)
% FILTERING MATLAB code for filtering.fig
%      FILTERING, by itself, creates a new FILTERING or raises the existing
%      singleton*.
%
%      H = FILTERING returns the handle to a new FILTERING or the handle to
%      the existing singleton*.
%
%      FILTERING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FILTERING.M with the given input arguments.
%
%      FILTERING('Property','Value',...) creates a new FILTERING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before filtering_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to filtering_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help filtering

% Last Modified by GUIDE v2.5 09-Oct-2014 18:38:29



% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @filtering_OpeningFcn, ...
                   'gui_OutputFcn',  @filtering_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before filtering is made visible.
function filtering_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to filtering (see VARARGIN)

% Choose default command line output for filtering
handles.output = hObject;

%---------------Initialise the shown values, sliders etc-------------------
if nargin == 4
    handles.hInput= varargin{1};
    %handles.hInput
end

exp_n= get(handles.hInput.SelectedProcessingValues,'Exponential');
gauss_n= get(handles.hInput.SelectedProcessingValues,'Gaussian');

set(handles.edit_exponential,'String',exp_n);
set(handles.slider_exponential,'Value',exp_n);


this       = varargin{1}.MRSpectroMatrix.getCurrent();

time_aquisition=this.Parameter.Headers.AcquisitionTime_ms;
set(handles.slider_gaussian,'Max',time_aquisition/2);
set(handles.slider_gaussian,'SliderStep',[2/time_aquisition 2/time_aquisition]);
set(handles.edit_gaussian,'String',gauss_n);
set(handles.slider_gaussian,'Value',gauss_n);

%--------------------------------------------------------------------------



% Update handles structure
guidata(hObject, handles);

% UIWAIT makes filtering wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = filtering_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;





%##########################################################################
%                          GAUSSIAN PART
%##########################################################################

function slider_gaussian_Callback(hObject, eventdata, handles)
% hObject    handle to slider_gaussian (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value=get(hObject,'Value');
set(handles.edit_gaussian,'String',current_value');
set(handles.hInput.SelectedProcessingValues,'Gaussian',current_value);
displaySpect(handles.hInput);


function edit_gaussian_Callback(hObject, eventdata, handles)
% hObject    handle to edit_gaussian (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value=get(hObject,'String');
current_value=str2double(current_value);
set(handles.slider_gaussian,'Value',current_value);
set(handles.hInput.SelectedProcessingValues,'Gaussian',current_value);
displaySpect(handles.hInput);



%##########################################################################
%                           EXPOTENTIAL PART
%##########################################################################


function edit_exponential_Callback(hObject, eventdata, handles)
% hObject    handle to edit_exponential (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

current_value=get(hObject,'String');
current_value=str2double(current_value);
set(handles.slider_exponential,'Value',current_value);
set(handles.hInput.SelectedProcessingValues,'Exponential',current_value);
displaySpect(handles.hInput);


function slider_exponential_Callback(hObject, eventdata, handles)
% hObject    handle to slider_exponential (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



current_value=get(hObject,'Value');
set(handles.edit_exponential,'String',current_value');

set(handles.hInput.SelectedProcessingValues,'Exponential',current_value);
displaySpect(handles.hInput);


%##########################################################################
%                              BUTTONS
%##########################################################################



% --- Executes on button press in pushbutton_ok.
function pushbutton_ok_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close;


% --- Executes on button press in pushbuttonApply.
function pushbuttonApply_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonApply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
expFilter   = get(handles.hInput.SelectedProcessingValues,'Exponential');
gaussFilter = get(handles.hInput.SelectedProcessingValues,'Gaussian');

this   = handles.hInput.MRSpectroMatrix.getCurrent();
set(this.Parameter.FilterSettings,'Gaussian',gaussFilter);
set(this.Parameter.FilterSettings,'Exponential',expFilter);

this = this.Filtering;
handles.hInput.MRSpectroMatrix.Save(this);
displaySpect(handles.hInput);

%##########################################################################
%##########################################################################

% --- Executes during object creation, after setting all properties.
function slider_gaussian_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_gaussian (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function slider_exponential_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_exponential (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function edit_exponential_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_exponential (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit_gaussian_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_gaussian (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
