classdef ReconFlags 
% version: 26 Feb 2014 

    properties
        isScaledData
        isEddyCurrentCorrected
        isFrequencyAligned
        isAveraged
        isfMRSAveraged
        isfMRSMovingAveraged
        isfMRSDifference
        isCoilsCombined
        isOversampledData
        isDCOffsetCorrected
        isZeroFilled
        isContainsExtraPoints
        isDataFiltered
        isMetaboliteCycling
        isDataRecon
        isDataPhased
        isSorted
        isCoilChannelsDeleted
        isHsvdFiltered
        isPhase0Removed
        isRescaled
        isPointsShifted
        isTruncated
        isUndoDetectorCycling
        isDenoised
        isSplinedSmoothed
        isJresolved
        isAlignWaterToPpm
        isDeletedMoved
        isISISAveraged
        isISISOrdered
        isSPECIALAveraged
        isSPECIALOrdered
    end
    
    
    
    methods
        
        function this=ReconFlags()
        
            
        this.isScaledData           = false;
        this.isEddyCurrentCorrected = false;
        this.isFrequencyAligned     = false;
        this.isAveraged             = false;
        this.isfMRSAveraged         = false;
        this.isfMRSMovingAveraged   = false;
        this.isfMRSDifference       = false;
        this.isCoilsCombined        = false;
        this.isOversampledData      = false;
        this.isDCOffsetCorrected    = false;
        this.isZeroFilled           = false;
        this.isContainsExtraPoints  = false;
        this.isDataFiltered         = false;
        this.isMetaboliteCycling    = false;
        this.isDataRecon            = false;
        this.isDataPhased           = false;
        this.isSorted               = false;
        this.isCoilChannelsDeleted  = [];
        this.isHsvdFiltered         = false;
        this.isPhase0Removed        = false;
        this.isRescaled             = false;
        this.isPointsShifted        = false;
        this.isTruncated            = false;
        this.isUndoDetectorCycling  = false;
        this.isDenoised             = false;
        this.isSplinedSmoothed      = false;
        this.isJresolved            = false;
        this.isAlignWaterToPpm      = false;
        this.isISISAveraged         = false;
        this.isISISOrdered          = false;
        end
        
    end
    
    
end