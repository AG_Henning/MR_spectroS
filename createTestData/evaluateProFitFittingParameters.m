function evaluateProFitFittingParameters()

folderName = 'final_simulations';
exportFolder = {'11 Simulations'};
pathNameExport = 'ProFit test data';
pathBaseExportFiles = pathToDataFolder(pathNameExport, exportFolder);
pathBaseExportFiles = pathBaseExportFiles(1:end-1);
pathBaseExportFiles = [pathBaseExportFiles ' - R3_new\'];% _only %+FID
dataExportPathBase = strcat(pathBaseExportFiles, exportFolder{1}, '\');
truncSuffix = '_truncOn';
folderFigures = [pathBaseExportFiles 'Output\' folderName, truncSuffix, '\'];
mkdir(folderFigures)

folders = {'conc', 'df2', 'pc1', 'pc0', 'gauss', 'conc2', 'conc3', 'noise'};
folders = {'df2', 'pc1', 'pc0', 'gauss'};
folders = {'df2_global', 'df2_local', 'pc1', 'pc0', 'gauss', 'em', 'noise', 'baseline'};
folders = {'pc0',                 'pc1',           'gauss',     'df2_global',         'df2_local',        ...
    'em',              'noise',    'baseline'};
xAxisLabels = {'$$\rm\varphi_0$$', '$$\rm\varphi_1$$', '$$\rm\nu_g$$', '$$\rm\omega_{global}$$', '{\rm\boldmath$\omega_{local}$}', ...
    '{\rm\boldmath$\nu_{e}$}', '{\rm\boldmath$noise$}', '{\rm\boldmath$baseline$}'};
legendLabels = {'\varphi_0 :\hspace{3em}', '\varphi_1 :\hspace{3.em}', '\nu_g :\hspace{3.em}', '\omega_{global} :\hspace{1.5em}', '${\boldmath$\omega_{local}$}$:\hspace{1.7em}', ...
    '${\boldmath$\nu_{e}$}$:\hspace{3.5em}', '${\boldmath$noise$}$:\hspace{1.5em}', '${\boldmath$baseline$}$:'};

parameters =      {'pc0',           'pc12',             'gm',                   'df2', ...
    'em',                                         'conc',                                  'T2'};
titleLabels = {'\Delta\varphi_0', '\Delta\varphi_1', '\Delta\nu_g', '${\boldmath$\overline{\mid\Delta\omega\mid}$}$', ...
    '${\boldmath$\overline{\mid\Delta\nu_{e}\mid}$}$', '${\boldmath$\overline{\mid\Delta c\mid}$}$', '${\boldmath$\overline{\mid\Delta T_{2}\mid}$}$'};
units = {          ' (degrees)',     ' (degrees/ppm)', ' (Hz)',         ' (Hz)',...
    ' (Hz)',                                   ' (mmol/kg)',                            ' (ms)'};
% titleOffset = {'\quad variations:\quad\quad','\quad variations:\quad', '\quad variations:\quad\quad\quad', '\quad variations:\quad\quad',...
%     '\quad variations:\quad\quad',     '\quad variations:\quad',        '\quad variations:\quad\quad'};
titleOffset = {'\quad variations:\quad','\quad variations:\,\,', '\quad variations:\quad\quad', '\quad variations:\quad',...
    '\quad variations:\quad',     '\quad variations:\,\,',        '\quad variations:\quad'};
% parameters = {'conc'};

useSubPlots =  true;
if useSubPlots
    numParam = length(parameters)-1;
    figure;
else
    numParam = length(parameters);
end

tableDeviations = cell(9,7);
for iFolder = 1:length(folders)
   tableDeviations{iFolder+1,1} = legendLabels{iFolder};
end    
for iParam = 1: numParam
    legendLabels_ = {};
    unitLabel = [titleLabels{iParam}, units{iParam}];
    tableDeviations{1,iParam+1} = unitLabel;
    for iFolder = 1:length(folders)
        load([dataExportPathBase, folders{iFolder}, '\Diff_Summaries', truncSuffix, '_no_BL_df3.mat'], 'diff_conc_mat', 'diff_em_mat', 'diff_gm_mat', ...
            'diff_pc0_mat', 'diff_df2_mat', 'diff_pc12_mat', 'diff_T2_mat');
        size1 = size(diff_conc_mat, 1);
        size2 = size(diff_conc_mat, 2);
        % think about these more
        diff_conc_mean = mean(mean(abs(diff_conc_mat)));
        diff_conc_std = std(abs(diff_conc_mat));
        
        eval(['diff_mat = diff_', parameters{iParam}, '_mat;']);
        
        diff_mean = mean(mean(abs(diff_mat), 'omitnan'));
        diff_std = std(mean(abs(diff_mat), 'omitnan'));
%         numberOfNaNs = sum(sum(isnan(diff_mat)))
%         numberOfNaNsV = sum((isnan(diff_mat)),2)'
        
        if useSubPlots
            subplot(3,2,iParam)
        else
            figure(iParam+10);
        end
        hold on
        if strcmp(parameters{iParam},'em') || strcmp(parameters{iParam},'conc') || ...
                strcmp(parameters{iParam},'df2') || strcmp(parameters{iParam},'T2') 
            scatter(zeros(1,size2)+iFolder, mean(abs(diff_mat)));
        else
            scatter(zeros(1,size2)+iFolder, mean(diff_mat));
        end
        title(['$$\rm', titleLabels{iParam}, '$$'],'Interpreter','latex');
        deviationsString = [ num2str(diff_mean,'%.2f'), ' \pm ', num2str(diff_std,'%.2f')];
        tableDeviations{iFolder+1,iParam+1} = deviationsString;
%         legendLabels_{iFolder}= ['$$\rm' legendLabels{iFolder}, deviationsString, '$$'];
%         lgd = legend(legendLabels_,'Interpreter','latex','Location','northwest');
        
%         title(lgd,{'$$\rm Spectra\,\,with\,\,induced$$',['$$\rm', titleOffset{iParam}, unitLabel,'$$']},'FontSize',8);
    end
    xlim([0, length(folders)+1]);
    xticks(1:length(folders));
    xticklabels(xAxisLabels);
%     xlabel('Simulations')
    xlabel('Spectra with induced variations in ')
%     set(gca,'YAxisLocation','right')
    ylabel(['$$\rm', unitLabel, '$$'],'Interpreter','latex');
    set(gca,'TickLabelInterpreter','latex')
    set(gca,'XTickLabelRotation',60)
    savefig([folderFigures, parameters{iParam}])
end

writecell(tableDeviations,[folderFigures, 'Params.xlsx'])