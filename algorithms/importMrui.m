function [data, fileNameNew, samplingInterval, transmitterFrequency, typeOfNucleus, nameOfPatient, ...
    signalNames] = importMrui(filePath, fileName)

if ~(strcmp(filePath(end),'\') || strcmp(filePath(end),'/'))
    filePath = [filePath '/'];
end

filenameFull = strcat(filePath, fileName);

fileID=     fopen(filenameFull,'r');

[firstLine,~] = textscan(fileID,'%s %s %s',1);
if ((strcmp(firstLine{1},'jMRUI') && strcmp(firstLine{2},'Data') && strcmp(firstLine{3},'Textfile')) ~= 1 )
    ME = MException('ImportMruiText:WrongFileFormat', ...
        '%s file header should start with: "jMRUI Data Textfile".', ...
        fileName);
    throw(ME)
end

% initialization of default values
fileNameNew = '';
dataSetsInFile = 0;
pointsInDataset = 0;
samplingInterval = 1;   
zeroOrderPhase = 0;       
beginTime = 0;       
transmitterFrequency = 0;
magneticField = 0;
typeOfNucleus = 0;       
nameOfPatient = '';
dateOfExperiment = ''; 
spectrometer = 0; 
additionalInfo = ''; 
signalNames = '';


% [line,pos] = textscan(fileID,'%s%s\n', 1, 'Delimiter', {':',' :',' : ',': '});
[line,~] = textscan(fileID,'%s', 1, 'Delimiter', {'\n'});

while ~contains(line{1}, 'Signal and FFT')
    [line,~] = textscan(fileID,'%s', 1, 'Delimiter', {'\n'});
    %% TODO add here the interogation for the parameters
    if (~isempty(line{1}{1}))
        [values,~] = textscan(line{1}{1},'%s%s\n', 1, 'Delimiter', {':',' :',' : ',': '});
        if (length(values)>1)
            if (~isempty(values{2}))
                switch values{1}{1}
                    case 'Filename'
                        fileNameNew = values{2}{1};
                    case 'PointsInDataset'
                        pointsInDataset = str2double(values{2}{1});
                    case 'DatasetsInFile'
                        dataSetsInFile = str2double(values{2}{1});
                    case 'SamplingInterval'
                        samplingInterval = str2double(values{2}{1});
                    case 'ZeroOrderPhase'
                        zeroOrderPhase = str2double(values{2}{1});
                    case 'BeginTime'
                        beginTime = str2double(values{2}{1});
                    case 'TransmitterFrequency'
                        transmitterFrequency = str2double(values{2}{1});
                    case 'MagneticField'
                        magneticField = str2double(values{2}{1});
                    case 'TypeOfNucleus'
                        typeOfNucleus = str2double(values{2}{1});
                    case 'NameOfPatient'
                        nameOfPatient = values{2}{1};
                    case 'DateOfExperiment'
                        dateOfExperiment = values{2}{1};
                    case 'Spectrometer'
                        spectrometer = values{2}{1};
                    case 'AdditionalInfo'
                        additionalInfo = values{2}{1};
                    case 'SignalNames'
                        signalNames = values{2}{1};
                    otherwise
                        warning('.JMRUI value not known: %s', values{1}{1});
                end
            end
        end
    end
%     line{1}
end
[line,~] = textscan(fileID,'%s', 1, 'Delimiter', {'\n'});

data = zeros(pointsInDataset,dataSetsInFile);
for dataSetIndex = 1:dataSetsInFile
    % read the line: "Signal X out of Y in file"
    [lineIndexes,~] = textscan(fileID,'Signal %d out of %d in file', 1, 'Delimiter', {'\n'});
    if ((lineIndexes{1} ~= dataSetIndex) || (lineIndexes{2} ~= dataSetsInFile))
        ME = MException('ImportMruiText:WrongFileFormat', ...
            '%s file does not contain the correct separators between the datasets: "Signal X out of Y in file".', ...
            fileName);
        throw(ME)
    end
    %read the actual data set values
    valuesCellArray = textscan(fileID,'%f %f %f %f','Delimiter',' ','MultipleDelimsAsOne', 1);
    if (pointsInDataset ~= size(valuesCellArray{1}))
        ME = MException('readMRScan:WrongFileFormat', ...
            '%s file contains an array that does not match the given size: %d.', ...
            fileName, pointsInDataset);
        throw(ME)
    else
        fid = complex(valuesCellArray{1}, valuesCellArray{2});
        spectrum = complex(valuesCellArray{3}, valuesCellArray{4});
    end
    data(:,dataSetIndex) = fid;
end

fclose(fileID);
end