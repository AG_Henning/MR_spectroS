function plot_mean_std_of_spectra

[fileNames, path] = uigetfile('*.mat','MultiSelect', 'on');

colors = {[0 0.7 0]; [1 0 0]; [0 0 1]; [0 0 0]; [0 0.5 0.5]; [0.5 0.5 0];};
numberOfFiles = length(fileNames);
offsetStep = 2e-3;
offSetBase = offsetStep * (numberOfFiles + 1); 

for indexFile = 1:2
    fileName = fileNames{indexFile};
    offset = offSetBase - indexFile * offsetStep;
    plotOneTE([path fileName], colors{indexFile}, offset);
end

end

function plotOneTE(fileName, colorSpectrum, offset)
load(fileName, 'a')

zeroPPM = 4.67;
nTimepoints = size(a.Data{1},a.kx_dim);
nAverages = size(a.Data{1},a.meas_dim);
waterFreq  = a.Parameter.Headers.ScanFrequency * 1E-6;
bandwidth  = a.Parameter.Headers.Bandwidth_Hz;
frequencyRange    = linspace(-bandwidth/2,bandwidth/2,nTimepoints);

fidSpectra = a.Data{1}(:,1,1,1,1,1,1,1,1,1,1,:);
fidWater = a.Data{1}(:,1,1,1,1,1,1,1,2,1,1,:);
spectra = squeeze(fftshift(fft(fidSpectra)));
meanSpectrum = mean(spectra,2);
stdSpectrum = std(spectra,[],2);
relativeStd = stdSpectrum ./ max(meanSpectrum) * 100;

pts0_7ppm = 1233;
pts1_8ppm = 1455;
pts4_1ppm = 1924;
stddevAll = mean(stdSpectrum(pts0_7ppm : pts4_1ppm));
stddevMet = mean(stdSpectrum(pts1_8ppm : pts4_1ppm));
stddevMM = mean(stdSpectrum(pts0_7ppm : pts1_8ppm));
stddev = [stddevAll stddevMet stddevMM]
ppmScale = (frequencyRange)/waterFreq+zeroPPM;

figure(101)
hold on
plot(ppmScale,real(relativeStd) + offset*500, 'Color', colorSpectrum)
xlim([0.5 4.2]);
set(gca,'xDir','reverse')

%
figure(1)
stdAlpha = 0.4;
hold on
h = area(ppmScale,[real((meanSpectrum - stdSpectrum) + offset) (stdSpectrum * 2)]);
h(1).FaceColor = 'none';
h(2).FaceColor = colorSpectrum;
h(2).FaceAlpha = stdAlpha;
h(1).EdgeColor = 'none';
h(2).EdgeColor = 'none';
hold on
pSpectrum = plot(ppmScale,real(meanSpectrum + offset), 'LineWidth',2, 'Color', colorSpectrum);
   
xlim([0.5 4.2]);
set(gca,'xDir','reverse')
set(gca,'LineWidth',1);
set(gca,'ytick',[])
xlabel('[ppm]');
title('Spectra mean and standard deviation')
end