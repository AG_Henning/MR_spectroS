function plotFitAminoAcidsWithOffset()

pathNameMM = 'MM data path';
sampleCriteriaMM = {'OutputAminoAcids'};
[localFilePathBaseMM] = pathToDataFolder(pathNameMM, sampleCriteriaMM);
pathNameDF = 'DF data path';
sampleCriteriaDF = {'Output'};
[localFilePathBaseDF] = pathToDataFolder(pathNameDF, sampleCriteriaDF);

filename= 'Summed_Averaged_TE24.coord';

fullPathAminoAcidFit = [localFilePathBaseMM 'OutputAminoAcids\New_AminoAcids\' filename];
fullPathAminoAcidFitVisible = [localFilePathBaseMM 'OutputAminoAcids\New_AminoAcids_visible\' filename];
fullPathDF = [localFilePathBaseDF 'Output\' filename];

% close all;
figure;
offset = 1.3;

plotId1 = plotSpectrum(fullPathAminoAcidFitVisible, 0, true);
plotId2 = plotSpectrum(fullPathAminoAcidFit, offset, true);
plotId3 = plotSpectrum(fullPathDF, offset*2-0.4, false);


xlim([0.5 8.8]);
ylim([-0.4 2.1+offset]);
set(gca,'xDir','reverse')

set(gca,'ytick',[])
% legend('TE = 24ms','TE = 32ms','TE = 40ms', 'TE = 52ms', 'TE = 60ms');
xlabel('[ppm]');
title('Spectra and fit')
end

function plotId = plotSpectrum(FileNameFull, offset, plotFit)
    %[c1 c2 c3 c4] = textread(strcat(PathName,FileName{i}),'%s %s %s %s');
    c          = textread(strcat(FileNameFull),'%s');
    nOfPoints  = find(strcmp(c ,'points'),1);
    nOfPoints  = str2num (c{nOfPoints-1});
    
    %ppm axis
    indexCoordinates   = find(strcmp(c,'ppm-axis'))+3;
    endOfCoordinates     = nOfPoints+indexCoordinates-1;
    ppmVector = str2double( c(indexCoordinates:endOfCoordinates,1));
    
    %phaseData
    indexCoordinates   = find(strcmp(c,'phased'))+4;
    endOfCoordinates     = nOfPoints+indexCoordinates-1;
    phasedData = str2double( c(indexCoordinates:endOfCoordinates,1));
    
    %fit data
    indexCoordinates   = find(strcmp(c,'fit'))+5;
    endOfCoordinates     = nOfPoints+indexCoordinates-1;
    fitData = str2double( c(indexCoordinates:endOfCoordinates,1));
    
    %background
    indexCoordinates   = find(strcmp(c,'background'))+3;
    endOfCoordinates     = nOfPoints+indexCoordinates-1;
    baselineData = str2double( c(indexCoordinates:endOfCoordinates,1));
    
    
    indexDF = find((ppmVector > 5.5), 1, 'last');
    % search only based on DF
    maxValueSample = max(phasedData(1:indexDF));
    
    %plotting
    hold on
    if plotFit
        plotId = plot(ppmVector, [phasedData / maxValueSample + offset fitData / maxValueSample + offset]);
    else
        plotId = plot(ppmVector(1:indexDF), phasedData(1:indexDF) / maxValueSample + offset);
    end
    
    set(plotId(1),'LineWidth',1);
    set(gca,'fontsize',14);
end