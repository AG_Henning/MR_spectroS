function plot_snr(handles)


this=handles.this;

% edit real-imaginary-absolute 
data=this.Data{1};


n_coils=size(data,this.coil_dim);
n_dyns=size(data,this.dyn_dim);
n_means=size(data,this.meas_dim);

n_fids=n_dyns*n_means;



% Arrange the plot figures 
if n_coils/4 <1
    n_rows=1;
    n_columns=n_coils;
else
    n_rows=n_coils/4;
    n_columns=4;
end;


figure
for i=1:n_coils
    
    
    s=1;
    snr=zeros(n_fids);
    ffts_all=0;
    snr_all=0;
    
    range1=[1000:1500];
    range2=[2000:2100];
    
    for j=1:n_dyns
        for k=1:n_means
            
            fid=data(:,1,1,i,1,1,1,1,1,j,1,k);
            
            ffts=fftshift(fft(fid));
            snr(s)=max(abs(ffts(range2)))/abs(std(ffts(end-1000:end))); %SNR= Ampl(NAA)/SD(Noise)
            
            ffts_all=ffts_all+ffts;
            snr_all(s)=max(abs(ffts_all(range2)))/abs(std(ffts_all(end-1000:end)));
            s=s+1;
        end
    end




mean_snr=max(mean(snr));
theoritical_snr=sqrt(1:n_fids)*mean_snr;

subplot(n_rows,n_columns,i)
plot(1:n_fids,mean_snr*ones(n_fids))
hold all
plot(1:n_fids,snr_all);
hold all
plot(1:n_fids,theoritical_snr);
hold all
plot(1:n_fids,snr);
xlabel('#mean');
title(sprintf('Coil %d',i));
end


end



    
    