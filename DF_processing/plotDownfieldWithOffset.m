function plotDownfieldWithOffset()

pathNameMM = 'MM data path';
sampleCriteriaMM = {};
[localFilePathBaseMM] = pathToDataFolder(pathNameMM, sampleCriteriaMM);
pathNameDF = 'DF data path';
sampleCriteriaDF = {};
[localFilePathBaseDF] = pathToDataFolder(pathNameDF, sampleCriteriaDF);

filename= 'Summed_Averaged_TE24.mat';

fullPathMM = [localFilePathBaseMM filename];
fullPathDF = [localFilePathBaseDF filename];

% close all;
figure;
offset = 1.3;


colors = {[0 0.7 0]; [1 0 0]; [0 0 1]; [0 0 0]; [0 0.5 0.5];};
plotId1 = plotSpectrum(fullPathMM, colors{1}, 0);
plotId2 = plotSpectrum(fullPathDF, colors{2}, offset);


xlim([5.5 8.8]);
ylim([-0.4 2.1+offset]);
set(gca,'xDir','reverse')

set(gca,'ytick',[])
xlabel('[ppm]');
title('Macromolecular spectra and fit')
end

function plotSpectraDF(fileName, colorSpectrum, offset)
load(fileName, 'summedSpectraTE')

zeroPPM = 4.67;
nTimepoints = size(summedSpectraTE.Data{1},summedSpectraTE.kx_dim);
waterFreq  = summedSpectraTE.Parameter.Headers.ScanFrequency * 1E-6;
bandwidth  = summedSpectraTE.Parameter.Headers.Bandwidth_Hz;
frequencyRange    = linspace(-bandwidth/2,bandwidth/2,nTimepoints);

fidSpectra = summedSpectraTE.Data{1}(:,1,1,1,1,1,1,1,1,1,1,:);


fidWater = summedSpectraTE.Data{1}(:,1,1,1,1,1,1,1,2,1,1,:);
spectra = squeeze(fftshift(fft(fidSpectra)));
water = squeeze(fftshift(fft(fidWater)));

ppmVector = (frequencyRange)/waterFreq+zeroPPM;

%%
scalingRange = (ppmVector < 1.8) & (ppmVector > 0);

%%
figure(1)
hold on
pSpectrum = plot(ppmVector,spectra + offset, 'LineWidth',2, 'Color', colorSpectrum);
    
xlim([5.5 9.4]);
set(gca,'xDir','reverse')
set(gca,'LineWidth',1);
set(gca,'ytick',[])
xlabel('[ppm]');
title('Spectra mean and standard deviation')
end