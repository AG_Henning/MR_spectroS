function [ tableConcentrations, tableT2s ] = calculate_31P_T2( FileNames,PathName, displaySilent, availableTEs)
%CALCULATE_T2 Summary of this function goes here
% select .table from LCModel output from all TEs
% (D:\PAPER\31P_T2_RelaxationTimes\Data\Output\2019-09-30_9873_SurfCoil)
% calculate T2 values from exponential fit either for all values with
% CRLB < 900, or till manual defined TE value. Plot data and fits, create
% output table with T2 values.

close all;

% CAREFUL: Give files in order of availableTEs!!!
if (~exist('FileNames', 'var'))
    [FileNames,PathName] = uigetfile('*.table','Please select .table files from LCModel','Multiselect','on');
end

if ~exist('displaySilent', 'var')
    displaySilent = false;
end

if ~exist('availableTEs', 'var')
    availableTEs = {'+6' '8' '11' '15' '20' '30' '50' '80' '150'};
end

numTEs = length(availableTEs);
%threshold for crlbs
threshold_CRLB = 900;

%% do the absolute quantification on all files
if ~iscell(FileNames)
    FileNames= {FileNames};
end

tableConcentrations = [];
isFirstIter = 1;
for index =1:length(FileNames)
    %% retrieve parameters
    [c1 c2 c3 c4] = textread(strcat(PathName,FileNames{index}),'%s %s %s %s');
    
    if isFirstIter
        start  = find(strcmp(c1,'Conc.'));
        start  = start + 1;
        finish = find(strcmpi(c1,'$$MISC')) - 1;
        tableConcentrations{1,1} = 'ID';
        
        s = 2;
        for j=start:finish
            if(strcmp(c4{j},'')==1)
                c3_split = strsplit(c3{j},'-');
                c3_split = strsplit(c3_split{end},'+');
                tableConcentrations{1,s} = c3_split{end};
            else
                tableConcentrations{1,s} = c4{j};
            end
            tableConcentrations{1,s+1} = 'CRLB %';
            tableConcentrations{1,s+2} = 'CRLB (rel)';
            s          = s +3;
        end
    end
    
    s = 2;
    loc = 1;
%     position = find(strcmpi(FileNames{index}(end-7:end-6),availableTEs));
    posTable = index + 1; %position in the table
    
    tableConcentrations{posTable,1}= FileNames{index}(1:end-6);
    
    %% get each metabolite concentration and add it to the table
    for j=start:finish
        %add quantified metabolite
        tableConcentrations{posTable,s} = str2num( c1{j});
        %add CRLB
        tableConcentrations{posTable,s+1} = str2num(  c2{j}(1:end-1));
        
        %check for reasonable CRLB values
        if (tableConcentrations{posTable,s+1}>threshold_CRLB) tableConcentrations{posTable,s}= NaN; end
        if (tableConcentrations{posTable,s+1}>threshold_CRLB) tableConcentrations{posTable,s+1}= NaN; end
        %add CRLB (rel)
        tableConcentrations{posTable,s+2} = tableConcentrations{posTable,s}*0.01*tableConcentrations{posTable,s+1};
        s          = s +3;
    end
    
    isFirstIter = false;
end

tableT2s = [];
x = str2double(availableTEs(:));
numOfMetabolite =  finish - start + 1;
tableConcentrations{11,1} = 'T2';       %first row ID, 9 TEs -> continue in 11th row
tableConcentrations{12,1} = 'RSquare';
tableConcentrations{13,1} = 'T2_ManualTE';       %first row ID, 9 TEs -> continue in 11th row
tableConcentrations{14,1} = 'RSquare_ManualTE';
tableT2s{2,1} = 'T2';
tableT2s{3,1} = 'RSquare';
tableT2s{4,1} = 'T2_ManualTE';
tableT2s{5,1} = 'RSquare_ManualTE';
if strfind(FileNames{1},'Summed')
    if ~isempty(strfind(FileNames{1},'noTrunc'))
        if ~isempty(strfind(FileNames{1},'_noAGCoupl')) && isempty(strfind(FileNames{1},'_noBCoupl'))
            volunteer = FileNames{1}([1:18,end-26:end-6]);
        elseif ~isempty(strfind(FileNames{1},'_noBCoupl')) && isempty(strfind(FileNames{1},'_noAGCoupl'))
            volunteer = FileNames{1}([1:18,end-25:end-6]);
        elseif ~isempty(strfind(FileNames{1},'_noBCoupl_noAGCoupl'))
            volunteer = FileNames{1}([1:18,end-35:end-6]);
        else
            if ~isempty(strfind(FileNames{1},'JD'))
                volunteer = FileNames{1}([1:21,end-16:end-6]);
            else
                volunteer = FileNames{1}([1:18,end-16:end-6]);
            end
        end
    else
        volunteer = FileNames{1}([1:19,end-17:end-6]);
    end
else
    if ~isempty(strfind(FileNames{1},'noTrunc'))
        if ~isempty(strfind(FileNames{1},'_noAGCoupl')) && isempty(strfind(FileNames{1},'_noBCoupl'))
            volunteer = FileNames{1}([1:16,end-26:end-6]);
        elseif ~isempty(strfind(FileNames{1},'_noBCoupl')) && isempty(strfind(FileNames{1},'_noAGCoupl'))
            volunteer = FileNames{1}([1:16,end-25:end-6]);
        elseif ~isempty(strfind(FileNames{1},'_noBCoupl_noAGCoupl'))
            volunteer = FileNames{1}([1:16,end-35:end-6]);
        else
            if ~isempty(strfind(FileNames{1},'JD'))
                volunteer = FileNames{1}([1:19,end-16:end-6]);
            else
                volunteer = FileNames{1}([1:16,end-16:end-6]);
            end
        end
    else
        volunteer = FileNames{1}([1:17,end-17:end-6]);
    end
end
for iMetabolite = 1:numOfMetabolite
    indexMetabolite = iMetabolite * 3 - 1;
    metaboliteName = tableConcentrations(1,indexMetabolite);
    y = cell2mat(tableConcentrations(2:numTEs+1,indexMetabolite));
    %normalize to PCr TE 8ms
    PCrConcTE8ms = cell2mat(tableConcentrations(3,29));
    y = y./PCrConcTE8ms;
    [calculatedT2, gof] = calculateT2Fit(x,y, iMetabolite, metaboliteName, displaySilent, PathName, volunteer);
    tableConcentrations{11,indexMetabolite} = calculatedT2;
    tableConcentrations{12,indexMetabolite} = gof.rsquare;
    [calculatedT2ManualTE, gofManualTE] = calculateT2FitManualTE(x,y, iMetabolite, metaboliteName, displaySilent, PathName, volunteer);
    tableConcentrations{13,indexMetabolite} = calculatedT2ManualTE;
    tableConcentrations{14,indexMetabolite} = gofManualTE.rsquare;
    %saving results
    metaboliteName{1};
    tableT2s{1,iMetabolite+1} = metaboliteName{1};
    tableT2s{2,iMetabolite+1} = num2str(calculatedT2,'%.2f');
    tableT2s{3,iMetabolite+1} = num2str(gof.rsquare,'%.2f');
    tableT2s{4,iMetabolite+1} = num2str(calculatedT2ManualTE,'%.2f');
    tableT2s{5,iMetabolite+1} = num2str(gofManualTE.rsquare,'%.2f');

end


%% save table to file
exportFileXlsxQuantification = [PathName ,'Quantification_Results_',volunteer,'.xlsx'];
xlswrite(exportFileXlsxQuantification, {PathName}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsxQuantification, tableConcentrations, 1, cellPosition);

exportFileXlsxT2 = [PathName ,'T2_Results_',volunteer,'.xlsx'];
xlswrite(exportFileXlsxT2, {PathName}, 1, 'A1');
cellPosition = 'A2';
xlswrite(exportFileXlsxT2, tableT2s, 1, cellPosition);


for i = 2:length(tableT2s)
    tableT2(1,i-1) = str2num(tableT2s{2,i});
    tableT2(2,i-1) = str2num(tableT2s{3,i});
    tableT2(3,i-1) = str2num(tableT2s{4,i});
    tableT2(4,i-1) = str2num(tableT2s{5,i});
end
rowNames = {'T2','Rsquare','T2_ManualTE','Rsquare_ManualTE'};
for i = 2:length(tableT2s)
    colNames{1,i-1} = tableT2s{1,i};
    colNames{1,i-1} = strrep(colNames{1,i-1},'-','_');
    colNames{1,i-1} = strrep(colNames{1,i-1},'+','_');
end
tableT2toSave = array2table(tableT2,'RowNames',rowNames,'VariableNames',colNames)

fileName = [PathName ,'T2_Results_',volunteer];
save(fileName, 'tableT2toSave');

end

function [calculatedT2, gof] = calculateT2Fit(x, y, iMetabolite, metaboliteName, displaySilent, PathName, volunteer)
    orderOfMagn = floor(log(abs(mean(y,'omitnan')))./log(10));
    tempX = x(~(isnan(y)));
%     tempY = y(~(isnan(y))) * 10^(orderOfMagn+1); %values scaled to have the same order of magnitude as the echo times (ms)
    tempY = y(~(isnan(y))) * 100; %best fit precision when y values between 10 and 100
%     if ~isempty(strfind(PathName,'1090'))
%         tempX_temp = tempX(2:end); tempX = tempX_temp;
%         tempY_temp = tempY(2:end); tempY = tempY_temp;
%     end
    if length(tempX) >= 3
%         % write equation y = a * x +b for 2nd and 3rd point to check for an
%         % improved starting value
%         a = ((tempY(3)-tempY(2))/tempX(3)) / (1 + tempX(2)/tempX(3));
%         b = tempY(2) - a * tempX(2);
%         firstValueOk = tempY(1) > (a * tempX(1) + b);
%         if (firstValueOk)
            options = fitoptions('exp1', 'Upper', [Inf 0]);
%         else
%             options = fitoptions('exp1', 'Upper', [Inf 0]);
%             [expofitTemp] = fit(tempX(2:end),tempY(2:end), 'exp1', options);
%             p0 = [expofitTemp.a * 0.5 expofitTemp.b * 1.5]; %it's a try to give a better starting value, but it actually doesn't really work
%             options = fitoptions('exp1', 'Upper', [Inf 0], 'StartPoint', p0);
%         end
        [expofit, gof] = fit(tempX,tempY, 'exp1', options);
        calculatedT2 = 1.0/(-1.0*expofit.b);
        if length(tempX) == 2
            if isnan(y(1)) || isnan(y(2))
                thisValue = [metaboliteName ' ' num2str(calculatedT2) ' ' num2str(gof.rsquare)]
                calculatedT2 = NaN;
                gof.rsquare = 0;
            end
        end
        if displaySilent == false
            % plotting
            figure(iMetabolite);
            plot(expofit,tempX,tempY);
%             set(gca, 'YScale', 'log')
            title({metaboliteName{1},volunteer},'Interpreter','none');
            xlabel 'TE / ms'; ylabel 'Signal Amp. / a.u.';
            annotation('textbox', [0.2, 0.2, 0.1, 0.1], 'String', {strcat('R2: ',num2str(gof.rsquare)),strcat('T2: ',num2str(calculatedT2),' ms')})
            mkdir([PathName 'T2_plots\'])
            savefig([PathName 'T2_plots\' metaboliteName{1} '_' volunteer '.fig'])
        end
    else
        calculatedT2 = NaN;
        gof.rsquare = 0;
    end
    close all
end



function [calculatedT2ManualTE, gofManualTE] = calculateT2FitManualTE(x, y, iMetabolite, metaboliteName, displaySilent, PathName, volunteer)
    orderOfMagn = floor(log(abs(mean(y,'omitnan')))./log(10));
    if strfind(PathName,'Summed')
        if (~isempty(strfind(metaboliteName{1},'PCr'))||~isempty(strfind(metaboliteName{1},'PE'))||~isempty(strfind(metaboliteName{1},'PC'))||...
                ~isempty(strfind(metaboliteName{1},'GPE'))||~isempty(strfind(metaboliteName{1},'GPC')))%TE 6-150ms
            tempX = x(1:end);
            tempY = y(1:end);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))) * 100; 
        elseif (~isempty(strfind(metaboliteName{1},'NAD')))    %TE 6-20ms
            tempX = x(1:5);
            tempY = y(1:5);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))) * 100; 
        elseif (~isempty(strfind(metaboliteName{1},'ext')))    %TE 6-15ms
            tempX = x(1:4);
            tempY = y(1:4);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))) * 100; 
       elseif ~isempty(strfind(metaboliteName{1},'ATP'))    %TE 6-50ms
            tempX = x(1:7);
            tempY = y(1:7);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))) * 100; 
        elseif ~isempty(strfind(metaboliteName{1},'in'))    %TE 6-80ms
            tempX = x(1:8);
            tempY = y(1:8);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))) * 100; 
        end
    else
        if ~isempty(strfind(metaboliteName{1},'PCr'))
            tempX = x(1:end);
            tempY = y(1:end);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))) * 100; 
        elseif (~isempty(strfind(metaboliteName{1},'NAD'))||~isempty(strfind(metaboliteName{1},'ext')))    %TE 6-15ms
            tempX = x(1:4);
            tempY = y(1:4);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))) * 100; 
        elseif ~isempty(strfind(metaboliteName{1},'ATP'))    %TE 6-30ms
            tempX = x(1:6);
            tempY = y(1:6);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))) * 100; 
        elseif ~isempty(strfind(metaboliteName{1},'in'))    %TE 6-50ms
            tempX = x(1:7);
            tempY = y(1:7);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))) * 100; 
        elseif ~isempty(strfind(metaboliteName{1},'PE'))||~isempty(strfind(metaboliteName{1},'PC'))    %TE 6-80ms
            tempX = x(1:8);
            tempY = y(1:8);
            tempX = tempX(~(isnan(tempY)));
            tempY = tempY(~(isnan(tempY))) * 100; 
        end
    end
%     tempY = tempY.*1E8;
%     if ~isempty(strfind(PathName,'1090'))
%         tempX_temp = tempX(2:end); tempX = tempX_temp;
%         tempY_temp = tempY(2:end); tempY = tempY_temp;
%     end
    if length(tempX) >= 3
        options = fitoptions('exp1', 'Upper', [Inf 0]);
        [expofit, gofManualTE] = fit(tempX,tempY, 'exp1', options);
        %identify outliers as points at a distance greater than 1.0 std
        %from the baseline model and refit the data with the outliers excluded
        fdata = feval(expofit,tempX);
        I = abs(fdata - tempY) > 2.0*std(tempY);
        outliers = excludedata(tempX,tempY,'indices',I);
        options = fitoptions('exp1', 'Upper', [Inf 0],'Exclude',outliers);
        if (length(tempX)-sum(outliers(:))) >= 3
            [expofit, gofManualTE] = fit(tempX,tempY, 'exp1', options);
            calculatedT2ManualTE = 1.0/(-1.0*expofit.b);
            if length(tempX) == 2
                if isnan(y(1)) || isnan(y(2))
                    thisValue = [metaboliteName ' ' num2str(calculatedT2ManualTE) ' ' num2str(gofManualTE.rsquare)]
                    calculatedT2ManualTE = NaN;
                    gofManualTE.rsquare = 0;
                end
            end
            if displaySilent == false
                % plotting
                figure(iMetabolite);
                plot(tempX(~outliers),tempY(~outliers),'.');hold on;
                plot(linspace(0,(max(tempX)+10),100),expofit.a*exp(expofit.b*linspace(0,(max(tempX)+10),100)));
                plot(tempX(outliers),tempY(outliers),'k*');
                title({metaboliteName{1},volunteer},'Interpreter','none');
                plots=get(gca, 'Children');
                if sum(outliers(:))~=0
                    legend(plots([3,1,2]), {'data','excluded data','fit'});
                else
                    legend('data','fit');
                end
                xlabel 'TE / ms'; ylabel 'Signal Amp. / a.u.';
                annotation('textbox', [0.2, 0.2, 0.1, 0.1], 'String', {strcat('R2: ',num2str(gofManualTE.rsquare)),strcat('T2: ',num2str(calculatedT2ManualTE),' ms')})
                mkdir([PathName 'T2_plots\'])
                savefig([PathName 'T2_plots\' metaboliteName{1} '_' volunteer '_ManualTE.fig'])
            end
        else
            calculatedT2ManualTE = NaN;
            gofManualTE.rsquare = 0;
        end
    else
        calculatedT2ManualTE = NaN;
        gofManualTE.rsquare = 0;
    end
    close all
end
